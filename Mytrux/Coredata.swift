////
////  Coredata.swift
////  Mytrux
////
////  Created by Mukta Bhuyar Punjabi on 21/05/19.
////  Copyright © 2019 Mytrux. All rights reserved.
////
//
//import Foundation
//
////
////  CoreDataManager.swift
////  Flooent
////
////  Created by Raj Singh on 13/04/17.
////  Copyright © 2017 Raj Singh. All rights reserved.
////
//
//import UIKit
//import CoreData
//import SwiftyJSON
//class CoreDataManager: NSObject {
//    
//    
//    private class func getMainContext() -> NSManagedObjectContext {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        return appDelegate.persistentContainer.viewContext
//    }
//    
//    private class func getBackgroundContext() -> NSManagedObjectContext {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        return appDelegate.persistentContainer.newBackgroundContext()
//    }
//    
//    class func setUserLoggedIn(isLoggedIn : Bool){
//        deleteLoginStatus()
//        // let context = NSManagedObjectContext (concurrencyType: .privateQueueConcurrencyType)
//        let context = getMainContext()
//        context.perform {
//            
//            let entity = NSEntityDescription.entity(forEntityName: "UserStatePrefs", in: context)
//            let managedObj = NSManagedObject(entity: entity!, insertInto: context)
//            
//            managedObj.setValue(isLoggedIn, forKey: "isLoggedIn")
//            
//            do {
//                try context.save()
//                print("setUserLoggedIn saved!")
//            } catch {
//                print(error.localizedDescription)
//            }
//        }
//    }
//    
//    class func isUserLoggedIn() -> Bool{
//        
//        let fetchRequest:NSFetchRequest<UserStatePrefs> = UserStatePrefs.fetchRequest()
//        
//        do {
//            let fetchResult = try getMainContext().fetch(fetchRequest)
//            if fetchResult.count == 0{
//                return false
//            }else{
//                let item = fetchResult.first
//                return item!.isLoggedIn
//            }
//        }catch {
//            print(error.localizedDescription)
//        }
//        return false
//    }
//    
//    class func deleteLoginStatus(){
//        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "UserStatePrefs")
//        let request = NSBatchDeleteRequest(fetchRequest: fetch)
//        
//        do{
//            _ = try getMainContext().execute(request)
//        }catch{
//            print("ChatMessagesDB Deleted!")
//        }
//    }
//    
//    class func setChatMessage(chatMessage : ChatMessage){
//        
//        //let context = NSManagedObjectContext (concurrencyType: .privateQueueConcurrencyType)
//        let context = getMainContext()
//        if #available(iOS 10.0, *) {
//            context.mergePolicy = NSMergePolicy.mergeByPropertyObjectTrump
//        } else {
//            // Fallback on earlier versions
//            context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
//        }
//        
//        context.perform {
//            
//            let entity = NSEntityDescription.entity(forEntityName: "ChatMessagesDB", in: context)
//            
//            let managedObj = NSManagedObject(entity: entity!, insertInto: context)
//            managedObj.setValue(chatMessage.id, forKey: "id")
//            managedObj.setValue(chatMessage.from, forKey: "from")
//            managedObj.setValue(chatMessage.to, forKey: "to")
//            managedObj.setValue(chatMessage.isFromSelf, forKey: "isFromSelf")
//            managedObj.setValue(chatMessage.primaryText, forKey: "message")
//            managedObj.setValue(chatMessage.secondaryText, forKey: "trans_msg")
//            managedObj.setValue(chatMessage.timestamp, forKey: "timestamp")
//            do {
//                try context.save()
//                
//                print("chatMessage.id = \(chatMessage.id)")
//                print("Int64(chatMessage.id)! = \(Int64(chatMessage.id))")
//                if Int64(UserDefaults.standard.integer(forKey: TextStats.lastKnownMsgID)) < Int64(chatMessage.id){
//                    UserDefaults.standard.set(Int64(chatMessage.id), forKey: TextStats.lastKnownMsgID)
//                    UserDefaults.standard.synchronize()
//                }
//                print("UserDefaults.standard.integer(forKey: TextStats.lastKnownMsgID) = \(UserDefaults.standard.integer(forKey: TextStats.lastKnownMsgID))");
//                print("Int64(chatMessage.id)! = \(Int64(chatMessage.id)))");
//                
//                print("setChatMessage saved ! chatMessage.id = \(chatMessage.id)")
//            } catch {
//                print(error.localizedDescription)
//            }
//            
//        }
//        
//    }
//    
//    class func setChatMessages(chatMessages : [ChatMessage],completion: @escaping () -> Void){
//        
//        //let context = NSManagedObjectContext (concurrencyType: .privateQueueConcurrencyType)
//        let context = getMainContext()
//        if #available(iOS 10.0, *) {
//            context.mergePolicy = NSMergePolicy.mergeByPropertyObjectTrump
//        } else {
//            // Fallback on earlier versions
//            context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
//        }
//        
//        context.perform {
//            
//            for chatMessage in chatMessages{
//                let entity = NSEntityDescription.entity(forEntityName: "ChatMessagesDB", in: context)
//                
//                let managedObj = NSManagedObject(entity: entity!, insertInto: context)
//                managedObj.setValue(chatMessage.id, forKey: "id")
//                managedObj.setValue(chatMessage.from, forKey: "from")
//                managedObj.setValue(chatMessage.to, forKey: "to")
//                managedObj.setValue(chatMessage.isFromSelf, forKey: "isFromSelf")
//                managedObj.setValue(chatMessage.primaryText, forKey: "message")
//                managedObj.setValue(chatMessage.secondaryText, forKey: "trans_msg")
//                managedObj.setValue(chatMessage.timestamp, forKey: "timestamp")
//                
//                if Int64(UserDefaults.standard.integer(forKey: TextStats.lastKnownMsgID)) < Int64(chatMessage.id){
//                    UserDefaults.standard.set(Int64(chatMessage.id), forKey: TextStats.lastKnownMsgID)
//                    UserDefaults.standard.synchronize()
//                }
//            }
//            do {
//                try context.save()
//                
//                
//            } catch {
//                print(error.localizedDescription)
//            }
//            
//        }
//        
//    }
//    
//    
//    class func setChatMessageArray(chatMessages : [ChatMessage],completion: @escaping () -> Void){
//        //  let privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
//        //   privateContext.persistentStoreCoordinator = getBackgroundContext().persistentStoreCoordinator
//        //  privateContext.performAndWait {
//        
//        let privateContext = getMainContext()
//        //             Code in here is now running "in the background" and can safely
//        //             do anything in privateContext.
//        //             This is where you will create your entities and save them.
//        
//        privateContext.perform {
//            
//            let syncQueue = DispatchQueue(label: "com.bzmeet.flooent.multipleChatHistory")
//            
//            for chatMessage in chatMessages{
//                syncQueue.sync {
//                    setChatMessage(chatMessage: chatMessage)
//                }
//            }
//            print("chatMessages.count \(chatMessages.count)")
//            if chatMessages.count > 0{
//                UserDefaults.standard.set(true, forKey: TextStats.downloadedChatHsitory)
//                UserDefaults.standard.synchronize()
//            }
//            completion()
//            NotificationCenter.default.post(name: .chatsUpdatedInDB, object: nil)
//        }
//        
//    }
//    
//    class func setMessage(chatMessage : Message){
//        
//        //let context = NSManagedObjectContext (concurrencyType: .privateQueueConcurrencyType)
//        let context = getMainContext()
//        if #available(iOS 10.0, *) {
//            context.mergePolicy = NSMergePolicy.mergeByPropertyObjectTrump
//        } else {
//            // Fallback on earlier versions
//            context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
//        }
//        
//        let entity = NSEntityDescription.entity(forEntityName: "MessagePreview", in: context)
//        
//        let managedObj = NSManagedObject(entity: entity!, insertInto: context)
//        managedObj.setValue(chatMessage.contactId, forKey: "contactId")
//        managedObj.setValue(chatMessage.contactName, forKey: "contactName")
//        managedObj.setValue(chatMessage.chatMessageId, forKey: "chatMessageId")
//        managedObj.setValue(chatMessage.messages, forKey: "message")
//        managedObj.setValue(chatMessage.timestamp, forKey: "timestamp")
//        managedObj.setValue(chatMessage.friend_status, forKey: "friend_status")
//        do {
//            try context.save()
//            
//            if Int64(UserDefaults.standard.integer(forKey: TextStats.lastKnownMsgID)) < Int64(chatMessage.chatMessageId)!{
//                UserDefaults.standard.set(Int64(chatMessage.chatMessageId)!, forKey: TextStats.lastKnownMsgID)
//                UserDefaults.standard.synchronize()
//            }
//        } catch {
//            print(error.localizedDescription)
//        }
//        
//    }
//    
//    class func setMessageArray(chatMessages : [Message],completion: @escaping () -> Void){
//        //    let privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
//        //    privateContext.persistentStoreCoordinator = getBackgroundContext().persistentStoreCoordinator
//        let privateContext = getMainContext()
//        
//        
//        //             Code in here is now running "in the background" and can safely
//        //             do anything in privateContext.
//        //             This is where you will create your entities and save them.
//        
//        let syncQueue = DispatchQueue(label: "com.bzmeet.flooent.multipleChatHistory")
//        
//        for chatMessage in chatMessages{
//            syncQueue.sync {
//                //deleteMessageByID(ID : chatMessage.chatMessageId)
//                setMessage(chatMessage: chatMessage)
//            }
//        }
//        print("chatMessages.count \(chatMessages.count)")
//        
//        completion()
//        // NotificationCenter.default.post(name: .chatsUpdatedInDB, object: nil)
//        
//        
//    }
//    
//    //    class func getLatestChatMessagesByIds(contactIds : [String]){
//    //        var formatString = ""
//    //        contactIds.forEach { contact_id in
//    //
//    //        }
//    //        let fromPredicate = NSPredicate(format: "from IN %@", contactIds)
//    //        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "timestamp", ascending: false)]
//    //        fetchRequest.fetchLimit = 1
//    //
//    //    }
//    
//    class func getChatMessages(contactId : Int64, chatId : Int64) -> [ChatMessage]{
//        let context = getMainContext()
//        var chatMessages = [ChatMessage]()
//        
//        let UserID = UserDefaults.standard.integer(forKey: User.ID)
//        
//        let fetchRequest:NSFetchRequest<ChatMessagesDB> = ChatMessagesDB.fetchRequest()
//        
//        //        let predicate = NSPredicate(format: "from == %@", contactId)
//        //            fetchRequest.predicate = predicate
//        let fromMePredicate = NSPredicate(format: "from = %i AND to = %i", UserID, contactId)
//        let toMePredicate = NSPredicate(format: "from = %i AND to = %i", contactId, UserID)
//        let beforePredicate = NSPredicate(format: "id < %i", chatId)
//        if (chatId != -1){
//            let orPredicate = NSCompoundPredicate.init(orPredicateWithSubpredicates: [fromMePredicate,toMePredicate])
//            let andPredicate = NSCompoundPredicate.init(andPredicateWithSubpredicates: [orPredicate,beforePredicate])
//            fetchRequest.predicate = andPredicate
//        } else {
//            let predicate = NSCompoundPredicate.init(orPredicateWithSubpredicates: [fromMePredicate,toMePredicate])
//            fetchRequest.predicate = predicate
//        }
//        
//        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "timestamp", ascending: false)]
//        fetchRequest.fetchLimit = 10
//        //   context.perform{
//        
//        do {
//            
//            let fetchResult = try context.fetch(fetchRequest)
//            
//            print("fetchResult.count = \(fetchResult.count)")
//            
//            for item in fetchResult {
//                print(item.isFromSelf)
//                
//                var transMsg = ""
//                if item.trans_msg != nil{
//                    transMsg = item.trans_msg!
//                }
//                //                print("{ id:\(item.id),  timestamp:\(item.timestamp),  from:\(item.from!),  message:\(item.message!),  to:\(item.to!), }")
//                
//                chatMessages.append(ChatMessage(primaryText: item.message!, secondaryText: item.trans_msg!, isFromSelf: item.isFromSelf, id: item.id, timestamp: item.timestamp, from: item.from, to: item.to))
//            }
//            
//        }catch {
//            print(error.localizedDescription)
//        }
//        chatMessages = chatMessages.sorted(by: { $0.timestamp > $1.timestamp })
//        
//        // }
//        return chatMessages
//    }
//    
//    
//    class func getRequestMessages(contactIds : [Int64]) -> [ChatMessage]{
//        let context = getMainContext()
//        var chatMessages = [ChatMessage]()
//        
//        let UserID = UserDefaults.standard.integer(forKey: User.ID)
//        contactIds.forEach { contactId in
//            
//            let fetchRequest:NSFetchRequest<ChatMessagesDB> = ChatMessagesDB.fetchRequest()
//            
//            let fromMePredicate = NSPredicate(format: "from = %i AND to = %i", UserID, contactId)
//            let toMePredicate = NSPredicate(format: "from = %i AND to = %i", contactId, UserID)
//            let predicate = NSCompoundPredicate.init(orPredicateWithSubpredicates: [fromMePredicate,toMePredicate])
//            
//            fetchRequest.predicate = predicate
//            fetchRequest.sortDescriptors = [NSSortDescriptor(key: "timestamp", ascending: false)]
//            fetchRequest.fetchLimit = 1
//            
//            //   context.perform{
//            
//            do {
//                
//                let fetchResult = try context.fetch(fetchRequest)
//                
//                print("fetchResult.count = \(fetchResult.count)")
//                
//                for item in fetchResult {
//                    
//                    chatMessages.append(ChatMessage(primaryText: item.message!, secondaryText: item.trans_msg!, isFromSelf: item.isFromSelf, id: item.id, timestamp: item.timestamp, from: item.from, to: item.to))
//                }
//                
//            }catch {
//                print(error.localizedDescription)
//            }
//        }
//        // }
//        return chatMessages
//    }
//    
//    
//    class func getLatestMessages() -> [Message]{
//        
//        
//        var messages = [Message]()
//        
//        
//        let context = getMainContext()
//        
//        
//        let fetchRequest:NSFetchRequest<MessagePreview> = MessagePreview.fetchRequest()
//        //        let sort = NSSortDescriptor(key: "timestamp", ascending: true)
//        //        fetchRequest.sortDescriptors = [sort]
//        
//        do {
//            
//            let fetchResult = try context.fetch(fetchRequest)
//            print("fetchResult.count = \(fetchResult.count)")
//            
//            for item in fetchResult {
//                
//                print(item.contactName!)
//                print(item.timestamp)
//                print(item.contactId)
//                print(item.message!)
//                print(item.chatMessageId!)
//                
//                
//                messages.append(Message(contactId: item.contactId, contactName: item.contactName!, chatMessageId: item.chatMessageId!, timestamp: item.timestamp, messages: item.message!, friend_status: item.friend_status!))
//            }
//            
//        }catch {
//            print(error.localizedDescription)
//        }
//        
//        messages = messages.sorted(by: { $0.timestamp > $1.timestamp })
//        return messages
//        
//    }
//    
//    
//    class func deleteAllChatMessage(){
//        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "ChatMessagesDB")
//        let request = NSBatchDeleteRequest(fetchRequest: fetch)
//        //        You can also add a predicate if you only wanted to delete instances that match the predicate. To run the request:
//        do{
//            _ = try getMainContext().execute(request)
//        }catch{
//            print("ChatMessagesDB Deleted!")
//        }
//        
//    }
//    
//    
//    
//    class func deleteAllMessage(){
//        
//        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "MessagePreview")
//        let request = NSBatchDeleteRequest(fetchRequest: fetch)
//        //        You can also add a predicate if you only wanted to delete instances that match the predicate. To run the request:
//        do{
//            _ = try getMainContext().execute(request)
//        }catch{
//            print("MessagePreview Deleted!")
//        }
//        
//        //        if messages.count > 0{
//        //
//        //
//        //        }else{
//        //            UserDefaults.standard.set(true, forKey: TextStats.downloadedChatHsitory)
//        //            UserDefaults.standard.synchronize()
//        //        }
//    }
//    
//    
//    
//    class func getMessages(conact_id : [Int64]) -> [Message]{
//        
//        let context = getMainContext()
//        var messages = [Message]()
//        
//        var friendStatusPredicates = [NSPredicate]()
//        for friendStatus in conact_id{
//            let predicate = NSPredicate(format: "contactId == %i ", friendStatus)
//            friendStatusPredicates.append(predicate)
//        }
//        
//        let fetchRequest:NSFetchRequest<MessagePreview> = MessagePreview.fetchRequest()
//        let predicate = NSCompoundPredicate.init(orPredicateWithSubpredicates: friendStatusPredicates)
//        fetchRequest.predicate = predicate
//        
//        do {
//            
//            let fetchResult = try context.fetch(fetchRequest)
//            print("fetchResult.count = \(fetchResult.count)")
//            
//            for item in fetchResult {
//                
//                //                    print(item.contactName!)
//                //                    print(item.timestamp)
//                //                    print(item.contactId!)
//                //                    print(item.message!)
//                print(item.chatMessageId!)
//                //  print(item.transMsg!)
//                
//                messages.append(Message(contactId: item.contactId, contactName: item.contactName!, chatMessageId: item.chatMessageId!, timestamp: item.timestamp, messages: item.message!, friend_status: item.friend_status!))
//            }
//            
//        }catch {
//            print(error.localizedDescription)
//        }
//        messages = messages.sorted(by: { $0.timestamp > $1.timestamp })
//        return messages
//        
//    }
//    
//    
//    
//    
//    
//    
//    
//    //    class func getMessages() -> [Message]{
//    //
//    //        let context = getMainContext()
//    //        var messages = [Message]()
//    //
//    //        var friendStatusPredicates = [NSPredicate]()
//    //        for friendStatus in conact_id{
//    //            let predicate = NSPredicate(format: "contactId == %i ", friendStatus)
//    //            friendStatusPredicates.append(predicate)
//    //        }
//    //
//    //        let fetchRequest:NSFetchRequest<MessagePreview> = MessagePreview.fetchRequest()
//    //        let predicate = NSCompoundPredicate.init(orPredicateWithSubpredicates: friendStatusPredicates)
//    //        fetchRequest.predicate = predicate
//    //
//    //        do {
//    //
//    //            let fetchResult = try context.fetch(fetchRequest)
//    //            print("fetchResult.count = \(fetchResult.count)")
//    //
//    //            for item in fetchResult {
//    //
//    //                //                    print(item.contactName!)
//    //                //                    print(item.timestamp)
//    //                //                    print(item.contactId!)
//    //                //                    print(item.message!)
//    //                print(item.chatMessageId!)
//    //                //  print(item.transMsg!)
//    //
//    //                messages.append(Message(contactId: item.contactId, contactName: item.contactName!, chatMessageId: item.chatMessageId!, timestamp: item.timestamp, messages: item.message!, friend_status: item.friend_status!))
//    //            }
//    //
//    //        }catch {
//    //            print(error.localizedDescription)
//    //        }
//    //        messages = messages.sorted(by: { $0.timestamp > $1.timestamp })
//    //        return messages
//    //
//    //    }
//    //
//    
//    
//    
//    
//    //    class func deleteAllChatMessage(){
//    //        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "ChatMessagesDB")
//    //        let request = NSBatchDeleteRequest(fetchRequest: fetch)
//    //        //        You can also add a predicate if you only wanted to delete instances that match the predicate. To run the request:
//    //        do{
//    //            _ = try getMainContext().execute(request)
//    //        }catch{
//    //            print("ChatMessagesDB Deleted!")
//    //        }
//    //    }
//    
//    //    class func deleteAllMinus1Messages(){
//    //        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "ChatMessagesDB")
//    //        let predicate = NSPredicate(format: "id = %@ ", "-1")
//    //        fetch.predicate = predicate
//    //
//    //        let request = NSBatchDeleteRequest(fetchRequest: fetch)
//    //        //        You can also add a predicate if you only wanted to delete instances that match the predicate. To run the request:
//    //        do{
//    //            _ = try getMainContext().execute(request)
//    //        }catch{
//    //            print("ChatMessagesDB Deleted!")
//    //        }
//    //    }
//    
//    class func deleteChatByID(ID : String){
//        
//        let context = getMainContext()
//        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "ChatMessagesDB")
//        let predicate = NSPredicate(format: "id == %@", ID)
//        fetch.predicate = predicate
//        
//        let request = NSBatchDeleteRequest(fetchRequest: fetch)
//        //        You can also add a predicate if you only wanted to delete instances that match the predicate. To run the request:
//        //context.perform {
//        
//        context.perform {
//            
//            
//            do{
//                _ = try context.execute(request)
//                print("deleteChatByID Deleted! ID = \(ID)")
//                
//            }catch{
//                print("deleteChatByID Failed!")
//            }
//        }
//    }
//    
//    
//    class func deleteMessageByID(ID : String){
//        
//        let context = getMainContext()
//        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "MessagePreview")
//        let predicate = NSPredicate(format: "chatMessageId == %@", ID)
//        fetch.predicate = predicate
//        
//        let request = NSBatchDeleteRequest(fetchRequest: fetch)
//        //        You can also add a predicate if you only wanted to delete instances that match the predicate. To run the request:
//        //context.perform {
//        
//        context.perform {
//            
//            
//            do{
//                _ = try context.execute(request)
//                print("deleteChatByID Deleted! ID = \(ID)")
//                
//            }catch{
//                print("deleteChatByID Failed!")
//            }
//        }
//    }
//    
//    
//    class func setSupportedLanguages(langs : [Langs]){
//        CoreDataManager.deleteSupportedLanguages()
//        
//        let context = getMainContext()
//        
//        let entity = NSEntityDescription.entity(forEntityName: "SupportedLanguages", in: context)
//        
//        for lang in langs{
//            let managedObj = NSManagedObject(entity: entity!, insertInto: context)
//            
//            managedObj.setValue(lang.codename, forKey: "codeName")
//            managedObj.setValue(lang.name, forKey: "name")
//            managedObj.setValue(lang.flagImage, forKey: "flagImageURL")
//            
//            do {
//                try context.save()
//                print("saved!")
//            } catch {
//                print(error.localizedDescription)
//            }
//        }
//    }
//    
//    class func getSupportedLanguages() -> [Langs]{
//        var langs = [Langs]()
//        
//        let fetchRequest:NSFetchRequest<SupportedLanguages> = SupportedLanguages.fetchRequest()
//        
//        do {
//            let fetchResult = try getMainContext().fetch(fetchRequest)
//            
//            print("fetchResult.count = \(fetchResult.count)")
//            
//            for item in fetchResult {
//                langs.append(Langs(name: item.name!, codeName: item.codeName!, languageName: "", flagImage : item.flagImageURL!))
//            }
//        }catch {
//            print(error.localizedDescription)
//        }
//        return langs
//    }
//    
//    class func deleteSupportedLanguages(){
//        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "SupportedLanguages")
//        let request = NSBatchDeleteRequest(fetchRequest: fetch)
//        //        You can also add a predicate if you only wanted to delete instances that match the predicate. To run the request:
//        do{
//            _ = try getMainContext().execute(request)
//        }catch{
//            print("saved!")
//        }
//    }
//    
//    /*
//     class func setTranslatedMessages(chatMessage : TranslatedMessage){
//     
//     let context = getMainContext()
//     
//     let entity = NSEntityDescription.entity(forEntityName: "TranslatedChatMessage", in: context)
//     
//     let managedObj = NSManagedObject(entity: entity!, insertInto: context)
//     
//     managedObj.setValue(chatMessage.id, forKey: "id")
//     managedObj.setValue(chatMessage.message, forKey: "message")
//     
//     do {
//     try context.save()
//     print("setChatMessage saved!")
//     } catch {
//     print(error.localizedDescription)
//     }
//     }
//     
//     class func getTranslatedMessages(messageID : String) -> TranslatedMessage?{
//     
//     let fetchRequest:NSFetchRequest<TranslatedChatMessage> = TranslatedChatMessage.fetchRequest()
//     
//     let predicate = NSPredicate(format: "id == %@", messageID)
//     fetchRequest.predicate = predicate
//     
//     
//     do {
//     let fetchResult = try getMainContext().fetch(fetchRequest)
//     
//     print("fetchResult.count = \(fetchResult.count)")
//     if fetchResult.count == 0{
//     return nil
//     }
//     let result = fetchResult.first
//     
//     return TranslatedMessage(id: (result?.id)!, message: (result?.message)!)
//     }catch {
//     print(error.localizedDescription)
//     }
//     
//     return nil
//     }
//     
//     class func deleteAllTranslatedMessages(){
//     let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "TranslatedChatMessage")
//     
//     let request = NSBatchDeleteRequest(fetchRequest: fetch)
//     //        You can also add a predicate if you only wanted to delete instances that match the predicate. To run the request:
//     do{
//     _ = try getMainContext().execute(request)
//     }catch{
//     print("TranslatedChatMessage Deleted!")
//     }
//     }
//     */
//    class func setUserProfileData(userProfileData : UserProfileData){
//        
//        let context = getMainContext()
//        
//        let entity = NSEntityDescription.entity(forEntityName: "UserProfile", in: context)
//        
//        let managedObj = NSManagedObject(entity: entity!, insertInto: context)
//        
//        
//        managedObj.setValue(userProfileData.id, forKey: "id")
//        managedObj.setValue(userProfileData.about, forKey: "about")
//        managedObj.setValue(userProfileData.websit, forKey: "website")
//        managedObj.setValue(userProfileData.email, forKey: "email")
//        managedObj.setValue(userProfileData.phone, forKey: "phone")
//        
//        print("OBJECT setUserProfileData")
//        print("setUserProfileData id = \(userProfileData.id)")
//        print("setUserProfileData aboutme = \(userProfileData.about)")
//        print("setUserProfileData contactemail = \(userProfileData.email)")
//        print("setUserProfileData mobile = \(userProfileData.phone)")
//        print("setUserProfileData website = \(userProfileData.websit)")
//        
//        //  CoreDataManager.deleteUserProfileDataByID(userID : userProfileData.id)
//        do {
//            try context.save()
//            print("userProfileData saved!")
//            
//        } catch {
//            print(error.localizedDescription)
//        }
//    }
//    
//    class func getUserProfileData(userID : String) -> UserProfileData?{
//        
//        let fetchRequest:NSFetchRequest<UserProfile> = UserProfile.fetchRequest()
//        
//        let predicate = NSPredicate(format: "id == %@", userID)
//        fetchRequest.predicate = predicate
//        
//        do {
//            let fetchResult = try getMainContext().fetch(fetchRequest)
//            
//            print("fetchResult.count = \(fetchResult.count)")
//            if fetchResult.count == 0{
//                return nil
//            }
//            let result = fetchResult.first
//            
//            var aboutTxt = ""
//            var emailTxt = ""
//            var phoneTxt = ""
//            var websiteTxt = ""
//            var idTxt = ""
//            
//            if result?.id != nil{
//                idTxt = (result?.id)!
//            }
//            if result?.about != nil{
//                aboutTxt = (result?.about)!
//            }
//            if result?.email != nil{
//                emailTxt = (result?.email)!
//            }
//            if result?.phone != nil{
//                phoneTxt = (result?.phone)!
//            }
//            if result?.website != nil{
//                websiteTxt = (result?.website)!
//            }
//            
//            let userProfileData = UserProfileData(id: idTxt, about: aboutTxt, email: emailTxt, phone: phoneTxt, websit: websiteTxt)
//            
//            print("OBJECT getUserProfileData")
//            print("getUserProfileData id = \(userProfileData.id)")
//            print("getUserProfileData aboutme = \(userProfileData.about)")
//            print("getUserProfileData contactemail = \(userProfileData.email)")
//            print("getUserProfileData mobile = \(userProfileData.phone)")
//            print("getUserProfileData website = \(userProfileData.websit)")
//            
//            return userProfileData
//        }catch {
//            print(error.localizedDescription)
//        }
//        
//        return nil
//    }
//    
//    class func deleteAllUserProfileData(){
//        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "UserProfile")
//        
//        let request = NSBatchDeleteRequest(fetchRequest: fetch)
//        //        You can also add a predicate if you only wanted to delete instances that match the predicate. To run the request:
//        do{
//            _ = try getMainContext().execute(request)
//        }catch{
//            print("UserProfiles Deleted!")
//        }
//    }
//    class func deleteUserProfileDataByID(userID : Int64){
//        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "UserProfile")
//        let predicate = NSPredicate(format: "id == %@", userID)
//        fetch.predicate = predicate
//        
//        let request = NSBatchDeleteRequest(fetchRequest: fetch)
//        //        You can also add a predicate if you only wanted to delete instances that match the predicate. To run the request:
//        do{
//            _ = try getMainContext().execute(request)
//        }catch{
//            print("UserProfiles Deleted!")
//        }
//    }
//    
//    
//    class func setFlooentContacts(contacts : [myContacts], isUser : Bool){
//        let privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
//        privateContext.persistentStoreCoordinator = getMainContext().persistentStoreCoordinator
//        privateContext.performAndWait {
//            
//            //             Code in here is now running "in the background" and can safely
//            //             do anything in privateContext.
//            //             This is where you will create your entities and save them.
//            //            let syncQueue = DispatchQueue(label: "com.bzmeet.flooent.multipleChatHistory")
//            let context = getMainContext()
//            
//            let entity = NSEntityDescription.entity(forEntityName: "FlooentContacts", in: context)
//            
//            for contact in contacts{
//                let contactsQueue = DispatchQueue(label: "com.bzmeet.flooent.deleteContFirst")
//                contactsQueue.sync {
//                    // CoreDataManager.deleteContactByID(userID: contact.id)
//                    let managedObj = NSManagedObject(entity: entity!, insertInto: context)
//                    
//                    managedObj.setValue(contact.id, forKey: "id")
//                    managedObj.setValue(contact.fullName, forKey: "name")
//                    managedObj.setValue(contact.display_name, forKey: "name")
//                    managedObj.setValue(contact.first_name, forKey: "first_name")
//                    managedObj.setValue(contact.last_name, forKey: "last_name")
//                    managedObj.setValue(contact.status_text, forKey: "status_text")
//                    managedObj.setValue(contact.lang, forKey: "lang")
//                    managedObj.setValue(contact.profile_pic, forKey: "profile_pic")
//                    if isUser{
//                        managedObj.setValue(FriendStatus.ME , forKey: "friend_status")
//                    }else{
//                        managedObj.setValue(contact.friend_status, forKey: "friend_status")
//                    }
//                    managedObj.setValue(contact.about, forKey: "about")
//                    managedObj.setValue(contact.website, forKey: "website")
//                    managedObj.setValue(contact.email, forKey: "email")
//                    managedObj.setValue(contact.phone, forKey: "phone")
//                    managedObj.setValue(contact.user_status, forKey: "user_status")
//                    
//                    do {
//                        try context.save()
//                        print("FlooentContact saved!")
//                    } catch {
//                        print(error.localizedDescription)
//                    }
//                }
//            }
//            if !isUser{
//                let contactDict:[String: [myContacts]] = ["contacts": contacts]
//                NotificationCenter.default.post(name: .contactsUpdatedInDB, object: nil, userInfo: contactDict)
//            }
//        }
//    }
//    
//    
//    class func getUserProfile() -> myContacts?{
//        var userID : Int64? = -1
//        if appDelegate.isKeyPresentInUserDefaults(key: User.ID){
//            userID = Int64(UserDefaults.standard.integer(forKey: User.ID))
//        }
//        
//        let fetchRequest:NSFetchRequest<FlooentContacts> = FlooentContacts.fetchRequest()
//        let predicate = NSPredicate(format: "id == %i", userID!)
//        fetchRequest.predicate = predicate
//        
//        do {
//            
//            let fetchResult = try getMainContext().fetch(fetchRequest)
//            
//            print("fetchResult.count = \(fetchResult.count)")
//            if fetchResult.count == 0{
//                return nil
//            }
//            let cont = fetchResult[0]
//            let contact = myContacts(id: cont.id, friend_status: cont.friend_status!, display_name: cont.name!, first_name: cont.first_name!, last_name: cont.last_name!, email: cont.email!, lang: cont.lang!, website: cont.website!, about: cont.about!, phone: cont.phone!, status_text: cont.status_text!, room_id: "", profile_pic: cont.profile_pic!, user_status: cont.user_status!)
//            print("contact.id = \(contact.id)")
//            return contact
//        }catch {
//            print(error.localizedDescription)
//        }
//        return nil
//    }
//    
//    
//    class func updateLanguage(language : String){
//        
//        let userProfile = getUserProfile()
//        userProfile?.lang = language
//        CoreDataManager.setFlooentContacts(contacts: [userProfile!], isUser: true)
//        
//    }
//    
//    class func getContacts() -> [myContacts]?{
//        let friendStatus = [FriendStatus.REQUEST_SENT_BY_ME,FriendStatus.FRIENDS,FriendStatus.IGNORED_BY_CONTACT, FriendStatus.DELETED_BY_ME]
//        return getContacts(friend_statuses: friendStatus)
//    }
//    
//    class func getRequest() -> [myContacts]?{
//        
//        return getContacts(friend_statuses: [FriendStatus.REQUEST_SENT_BY_CONTACT, FriendStatus.IGNORED_BY_ME])
//        
//    }
//    
//    class func getMessagesByFriendStatus() -> [myContacts]?{
//        
//        return getContacts(friend_statuses: [FriendStatus.NOT_IN_CONTACTS,FriendStatus.REQUEST_SENT_BY_CONTACT])
//        
//    }
//    
//    class func getContacts(friend_statuses : [String]) -> [myContacts]?{
//        var friendStatusPredicates = [NSPredicate]()
//        for friendStatus in friend_statuses{
//            let predicate = NSPredicate(format: "friend_status == %@ ", friendStatus)
//            friendStatusPredicates.append(predicate)
//        }
//        
//        let fetchRequest:NSFetchRequest<FlooentContacts> = FlooentContacts.fetchRequest()
//        let predicate = NSCompoundPredicate.init(orPredicateWithSubpredicates: friendStatusPredicates)
//        fetchRequest.predicate = predicate
//        
//        do {
//            
//            let fetchResult = try getMainContext().fetch(fetchRequest)
//            
//            print("fetchResult.count = \(fetchResult.count)")
//            if fetchResult.count == 0{
//                return nil
//            }
//            var contacts = [myContacts]()
//            for cont in fetchResult{
//                let contact = myContacts(id: cont.id, friend_status: cont.friend_status!, display_name: cont.name!, first_name: cont.first_name!, last_name: cont.last_name!, email: cont.email!, lang: cont.lang!, website: cont.website!, about: cont.about!, phone: cont.phone!, status_text: cont.status_text!, room_id: "", profile_pic: cont.profile_pic!, user_status: cont.user_status!)
//                contacts.append(contact)
//            }
//            return contacts
//        }catch {
//            print(error.localizedDescription)
//        }
//        
//        return nil
//    }
//    
//    class func getContactByID(userID : Int64) -> myContacts?{
//        
//        let fetchRequest:NSFetchRequest<FlooentContacts> = FlooentContacts.fetchRequest()
//        let predicate = NSPredicate(format: "id == %i", userID)
//        fetchRequest.predicate = predicate
//        
//        do {
//            
//            let fetchResult = try getMainContext().fetch(fetchRequest)
//            
//            print("fetchResult.count = \(fetchResult.count)")
//            if fetchResult.count == 0{
//                return nil
//            }
//            let cont = fetchResult[0]
//            let contact = myContacts(id: cont.id, friend_status: cont.friend_status!, display_name: cont.name!, first_name: cont.first_name!, last_name: "", email: cont.email!, lang: cont.lang!, website: cont.website!, about: cont.about!, phone: cont.phone!, status_text: cont.status_text!, room_id: "", profile_pic: cont.profile_pic!, user_status: cont.user_status!)
//            return contact
//        }catch {
//            print(error.localizedDescription)
//        }
//        
//        return nil
//    }
//    
//    class func deleteAllContacts(){
//        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "FlooentContacts")
//        
//        let request = NSBatchDeleteRequest(fetchRequest: fetch)
//        //        You can also add a predicate if you only wanted to delete instances that match the predicate. To run the request:
//        do{
//            _ = try getMainContext().execute(request)
//        }catch{
//            print("FlooentContacts Deleted!")
//        }
//    }
//    class func deleteContactByID(userID : Int64){
//        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "FlooentContacts")
//        let predicate = NSPredicate(format: "id == %i", userID)
//        fetch.predicate = predicate
//        
//        let request = NSBatchDeleteRequest(fetchRequest: fetch)
//        //        You can also add a predicate if you only wanted to delete instances that match the predicate. To run the request:
//        do{
//            _ = try getMainContext().execute(request)
//        }catch{
//            print("FlooentContact Deleted!")
//        }
//    }
//}
//
//
//
