//
//  ReverseLoadViewController.swift
//  Mytrux
//
//  Created by Mytrux on 19/11/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON

class ReverseLoadViewController: BaseViewController,GMSMapViewDelegate {
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var timeDistanceView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var currentLocationView: UIView!
    @IBOutlet weak var goHomeBtnView: UIView!
    @IBOutlet weak var statusView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topBarCard(topBar: topBar)
        buttonCardArray(arrView: [statusView,timeDistanceView,goHomeBtnView])
        cardViewArray(arrView: [currentLocationView])
        self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = true
        do {
                // Set the map style by passing the URL of the local file.
                if let styleURL = Bundle.main.url(forResource: "map_style", withExtension: "json") {
                    mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                } else {
                    print("Unable to find map_style.json")
                }
            } catch {
                print("One or more of the map styles failed to load. \(error)")
            }
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
