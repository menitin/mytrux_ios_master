//
//  RegisterViewController.swift
//  Mytrux
//
//  Created by Mukta Bhuyar Punjabi on 21/05/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import StepIndicator

class RegisterVCStep1: BaseViewController {

    @IBOutlet weak var backButton: UIView!
    @IBOutlet weak var uploadPan: UIView!
    @IBOutlet weak var uploadPhoto: UIView!
    @IBOutlet weak var nextBtn: UIView!
    @IBOutlet weak var textFieldWrapper: UIView!
    @IBOutlet weak var stepIndicatorView: StepIndicatorView!
    @IBOutlet weak var stepIndicatorWrapper: UIView!
    @IBOutlet weak var topBar: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let cardArr = [textFieldWrapper,uploadPan,uploadPhoto]
        let buttonCard = [nextBtn]
        cardViewArray(arrView: cardArr as! [UIView])
        topBarCard(topBar: topBar)
       buttonCardArray(arrView: buttonCard as! [UIView])
        let backtap = UITapGestureRecognizer(target: self, action: #selector(self.handleBackButtonTap(_:)))
        backButton.addGestureRecognizer(backtap)
        backButton.isUserInteractionEnabled = true
        self.topBar.addSubview(backButton)

        // Do any additional setup after loading the view.
    }
    @objc func handleBackButtonTap(_ sender: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"LoginVC")
        transitionVc(vc: viewController, duration: 0.5, type: .fromLeft)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
