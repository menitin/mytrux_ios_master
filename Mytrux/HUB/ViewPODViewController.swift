//
//  ViewPODViewController.swift
//  Mytrux
//
//  Created by Aboli on 15/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class ViewPODViewController: BaseViewController {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var podImage: UIImageView!
    @IBOutlet weak var okBtn: UIView!
    var photoId = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        mainView.layer.cornerRadius = 8
        mainView.layer.shadowColor = UIColor.gray.cgColor
        mainView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        mainView.layer.shadowRadius = 5
        mainView.layer.shadowOpacity = 0.4
        buttonCardView(button: okBtn)
      print(" photoId === \(photoId)")
    
    }
  
    override func viewWillAppear(_ animated: Bool) {
        Alamofire.request(URLStatics.vehicleImage+"\(photoId)").responseImage { response in
            //  print("response === \(String(describing: response.data))")
            if let imagee = response.result.value {
                self.podImage.image = imagee
                //print("fleetImg.count ======== \(self.fleetImg.count)")
            }
        }
    }
    
    @IBAction func okCLicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
