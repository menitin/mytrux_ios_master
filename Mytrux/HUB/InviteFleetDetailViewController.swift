//
//  InviteFleetDetailViewController.swift
//  Mytrux
//
//  Created by Mytrux on 24/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import SideMenuSwift

class InviteFleetDetailViewController: BaseViewController {

    @IBOutlet weak var mainVIew: UIView!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var remainingTime: UIView!
    @IBOutlet weak var confirmBtn: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topBarCard(topBar: topBar)
        cardViewArray(arrView: [mainVIew])
        remainingTime.layer.cornerRadius = remainingTime.frame.height / 2
        confirmBtn.layer.cornerRadius = 5
        // Do any additional setup after loading the view.
    }
    
    @IBAction func menuBtn(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    @IBAction func confirmAndInviteFleet(_ sender: Any) {
         SideMenuController.preferences.basic.defaultCacheKey = "default"
        sideMenuController?.setContentViewController(with: "default")
        NotificationCenter.default.post(name: .confirmSubmitQuote, object: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
