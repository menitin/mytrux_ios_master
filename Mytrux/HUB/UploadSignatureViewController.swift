//
//  UploadSignatureViewController.swift
//  Mytrux
//
//  Created by Mytrux on 19/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SideMenuSwift

class UploadSignatureViewController: BaseViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,YPSignatureDelegate{

    
    @IBOutlet weak var signatureView: YPDrawSignatureView!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var backbtn: UIView!
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var uploadSignatureButton: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    topBarCard(topBar: topBar)
    cardViewArray(arrView: [signatureView,commentView])
    buttonCardView(button: uploadSignatureButton)
         NotificationCenter.default.addObserver(self, selector: #selector(self.uploadSignature(notification:)), name: .uploadSignature, object: nil)
        signatureView.delegate = self
        // Do any additional setup after loading the view.
    }

    
    func didStart() {
        print("didStart")
    }
    
    func didFinish() {
       print("didFinish")
   
    }
    var jobID = String()
    var tripID = String()
    
    @objc func uploadSignature(notification: Notification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        let jobId = userInfo!["jobId"] as! String
         let tripId = userInfo!["tripId"] as! String
        self.jobID = jobId
        self.tripID = tripId
    }
    
    
    
    @IBAction func backBtnClicked(_ sender: Any) {
        sideMenuController?.setContentViewController(with: "19")
    }
  
    @IBAction func signatureViewClicked(_ sender: Any) {
 
    }

    @IBAction func uploadSignatureClicked(_ sender: Any) {
        if let signatureImage = self.signatureView.getSignature(scale: 10) {
            let imageStr = signatureImage.toBase64()
            print("imageStr ===== \(String(describing: imageStr!))")
            uploadSign(jobId: self.jobID, tripId: self.tripID, imageStr: imageStr!)
        }
        
    }
    
    
    @IBAction func clear(_ sender: Any) {
        self.signatureView.clear()
    }
    
    func uploadSign(jobId:String,tripId:String,imageStr:String){
        
        self.activityIndicatorBegin()
        
        var parameters = [String:Any]()
        parameters = [
            "jobId":jobId,
            "podString" : "gfggghghg",
            "podUploadMode" : "ONLINE"
        ]
        
        print("para ===== \(parameters)")
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
        let header = self.header()
        
        Alamofire.request(URLStatics.uploadPod + "\(jobId)" + "/" + "\(tripId)?" + "&"+"jobId="+jobId+"&"+"podString="+"sdsfd"+"&"+"podUploadMode="+"ONLINE", method: .put, encoding: JSONEncoding.default, headers: header)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                 print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                switch (response.result) {
                case .success:
                    _ = [String:Any]()
                    if let reply = response.result.value {
                        print("JSON: uploadPod ======== \(reply)")
                        let mainResponse = JSON(reply)
                      
                        if mainResponse["warning"].stringValue != "" {
                            let alertController = UIAlertController(title: "\(mainResponse["warning"].stringValue)", message:"", preferredStyle: .alert)
                            
                            // Create the actions
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                                
                            }
                            alertController.addAction(okAction)
                            
                            // Present the controller
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                       self.activityIndicatorEnd()
                        
                        
                    }
                    
                    break
                case .failure(let error):
                    
                    print("error \(error.localizedDescription)")
                    break
                }
        }
    }
}




