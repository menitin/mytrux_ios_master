//
//  RegisterViewController.swift
//  Mytrux
//
//  Created by Mukta Bhuyar Punjabi on 21/05/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import StepIndicator
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
class Step1RegisterVC: BaseViewController,UITextFieldDelegate,imageDelegate,passSelectedItem {
    func passCompanyType(companyType: String, id: Int, companyCategories: String) {
                self.userSubTypeTf.text = companyType
        TypeofCompany(id: id, companyType: companyType, companyCategories: companyCategories)
        userDefaults.set("\(companyType)", forKey: RegisterConstants.companyType)
        userDefaults.set("\(companyCategories)", forKey: RegisterConstants.companyCategories)
        userDefaults.set("\(String(id))", forKey: RegisterConstants.id)
        userDefaults.synchronize()
    }
 
    @IBAction func backClicked(_ sender: Any) {

        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let presentedViewController = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginViewController
        presentedViewController.modalPresentationStyle = .fullScreen
        transitionVc(vc: presentedViewController, duration: 0.5, type: .fromLeft)
    }
    
    func passState(state: String) {
        
    }
    
    func passCity(city: String) {
        
    }
    
    @IBOutlet weak var mainView: UIView!
   
   // @IBOutlet weak var activityIndicatorr: NVActivityIndicatorView!
    @IBOutlet weak var userSubTypeView: UIView!
    @IBOutlet weak var galleryImageView: UIImageView!
    @IBOutlet weak var cameraImageView: UIImageView!
    @IBOutlet weak var uploadStackView: UIStackView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIView!
    @IBOutlet weak var uploadPan: UIView!
    @IBOutlet weak var uploadPhoto: UIView!
    @IBOutlet weak var nextBtn: UIView!
    @IBOutlet weak var textFieldWrapper: UIView!
    @IBOutlet weak var stepIndicatorView: StepIndicatorView!
    @IBOutlet weak var mobiletf: UITextField!
    @IBOutlet weak var emailtf: UITextField!
    @IBOutlet weak var stepIndicatorWrapper: UIView!
    @IBOutlet weak var confirmPwdTf: UITextField!
    @IBOutlet weak var passwordtf: UITextField!
    @IBOutlet weak var userSubTypeTf: UITextField!
    @IBOutlet weak var fullnametf: UITextField!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var showBtn: UIButton!
    var imageArray = [UIImage]()
    var imagePicked = UIImage()
    var imageClicked = UIImage()
    var isImageHidden = true
    override func viewDidLoad() {
        super.viewDidLoad()

        let cardArr = [textFieldWrapper,uploadPan,uploadPhoto]
        let buttonCard = [nextBtn]
        let state = UserDefaults.standard.value(forKey: UserDefaultsContants.MTX_STATE)! as! String
        print("state = \(state)")
        self.passwordtf.isSecureTextEntry = true
        self.confirmPwdTf.isSecureTextEntry = true
        if state == "PRO"{
            self.titleLabel.text = "MTX PRO"
        }else{
            self.titleLabel.text = "MTX GO"
        }
        userdefaultsKey()
      //  activityIndicatorr.type = .ballTrianglePath
//self.backgroundView.isHidden = true
      //  self.activityIndicatorr.isHidden = true
    //activityIndicatorr.startAnimating()
        let textFieldArray = [mobiletf,fullnametf,emailtf,passwordtf,confirmPwdTf,userSubTypeTf] as! [UITextField]
        for textField in textFieldArray{
            textField.delegate = self
            }
        cardViewArray(arrView: cardArr as! [UIView])
        topBarCard(topBar: topBar)
        buttonCardArray(arrView: buttonCard as! [UIView])
//        let backtap = UITapGestureRecognizer(target: self, action: #selector(self.handleBackButtonTap(_:)))
//        backButton.addGestureRecognizer(backtap)
     backButton.isUserInteractionEnabled = true
        self.topBar.addSubview(backButton)
        let nextTap = UITapGestureRecognizer(target: self, action: #selector(self.handleNextButtonTap(_:)))
        nextBtn.addGestureRecognizer(nextTap)
        nextBtn.isUserInteractionEnabled = true
        self.bottomView.addSubview(nextBtn)
        let name = Notification.Name("userDocuments")
      NotificationCenter.default.addObserver(self, selector: #selector(self.uploadedPhoto(notification:)), name: name, object: nil)
      //  NotificationCenter.default.addObserver(self, selector: #selector(self.uploadedPhoto(_:)), name: name, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil);
       }
    var uploadClicked = 0
    var typeOfCompany = [TypeOfCompany]()
    var companyCategory = [String]()
    var companyType = [String]()
    var id = [Int]()
    @IBAction func showPassword(_ sender: Any) {
        if showBtn.currentImage == UIImage(named:"password_inactive") {
            (sender as AnyObject).setImage(UIImage(named:"password_active"), for: .normal)
            self.passwordtf.isSecureTextEntry = false
        }
        else if  showBtn.currentImage == UIImage(named:"password_active") {
            (sender as AnyObject).setImage( UIImage(named:"password_inactive"), for: .normal)
            self.passwordtf.isSecureTextEntry = true
        }
    }
    
    @IBAction func userSubTypeClicked(_ sender: Any) {
        print("type = \(String(describing: UserDefaults.standard.value(forKey: UserDefaultsContants.MTX_Load_STATE)!))")
      print("companyType = \(companyType.count)")
         print("companyCategory = \(companyCategory.count)")
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let presentedViewController = storyBoard.instantiateViewController(withIdentifier: "ListMV") as! ListModelViewController
        presentedViewController.providesPresentationContextTransitionStyle = true
        DispatchQueue.main.async {
            presentedViewController.getTypeOfCompany()
        }
        UserDefaults.standard.set(3, forKey: UserDefaultsContants.SELECTED_MODE)
        UserDefaults.standard.synchronize()
        presentedViewController.definesPresentationContext = true
        presentedViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
        presentedViewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        presentedViewController.delegate = self
        self.present(presentedViewController, animated: true, completion: nil)
    }
   
    func userdefaultsKey() {
        if isKeyPresentInUserDefaults(key: RegisterConstants.FirstName){
            self.fullnametf.text = (userDefaults.value(forKey: RegisterConstants.FirstName)! as! String) + (userDefaults.value(forKey: RegisterConstants.Surname)! as! String)}
        if isKeyPresentInUserDefaults(key: RegisterConstants.MobileNo){
            self.mobiletf.text = (userDefaults.value(forKey: RegisterConstants.MobileNo)! as! String)}
        if isKeyPresentInUserDefaults(key: RegisterConstants.Email){
            self.emailtf.text = (userDefaults.value(forKey: RegisterConstants.Email)! as! String)}
        if isKeyPresentInUserDefaults(key: RegisterConstants.Email){
            self.emailtf.text = (userDefaults.value(forKey: RegisterConstants.Email)! as! String)}
        if isKeyPresentInUserDefaults(key: RegisterConstants.passward){
            self.passwordtf.text = (userDefaults.value(forKey: RegisterConstants.passward)! as! String)}
        if isKeyPresentInUserDefaults(key: RegisterConstants.confirmPassword){
                self.confirmPwdTf.text = (userDefaults.value(forKey: RegisterConstants.confirmPassword)! as! String)}
        if isKeyPresentInUserDefaults(key: RegisterConstants.companyType){
            self.userSubTypeTf.text = (userDefaults.value(forKey: RegisterConstants.companyType)! as! String)}
      
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == emailtf{
            if emailtf.text!.isValidEmail() == false{
                let alert = UIAlertController(title: "", message: "Invalid Email", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
 
    }
    
    @objc func uploadedPhoto(notification: Notification){
        if self.loadImageFromDiskWith(fileName: "profilePhoto") != nil {
            self.cameraImageView.image = self.loadImageFromDiskWith(fileName: "profilePhoto")
        }
        if self.loadImageFromDiskWith(fileName: "panPhoto") != nil {
            self.galleryImageView.image = self.loadImageFromDiskWith(fileName: "panPhoto")
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == fullnametf) {
            let maxLength = 30
            let currentString: NSString = fullnametf.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        if(textField == mobiletf) {
            let maxLength = 10
            let currentString: NSString = mobiletf.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        if(textField == passwordtf){
            let maxLength = 30
            let currentString: NSString = passwordtf.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }else{
            return true
        }
    }
    @IBAction func uploadImageClicked(_ sender: UITapGestureRecognizer) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let presentedViewController = storyBoard.instantiateViewController(withIdentifier: "ModelVC") as! ModelViewController
        presentedViewController.providesPresentationContextTransitionStyle = true
        presentedViewController.definesPresentationContext = true
        presentedViewController.uploadState = 1
        presentedViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
        presentedViewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        presentedViewController.isSelectModalPresent = false
        presentedViewController.isCameraModalPresent = true
        self.uploadClicked = 1
        self.present(presentedViewController, animated: true, completion: nil)
    }
    
    @IBAction func uploadPanClick(_ sender: UITapGestureRecognizer) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let presentedViewController = storyBoard.instantiateViewController(withIdentifier: "ModelVC") as! ModelViewController
        presentedViewController.providesPresentationContextTransitionStyle = true
        presentedViewController.uploadState = 2
        presentedViewController.definesPresentationContext = true
        presentedViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
        presentedViewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        presentedViewController.isSelectModalPresent = false
        presentedViewController.isCameraModalPresent = true
        self.uploadClicked = 2
        self.present(presentedViewController, animated: true, completion: nil)
        
        
    }
    
    func loadImageFromDiskWith(fileName: String) -> UIImage? {
        
        let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
        
        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
        
        if let dirPath = paths.first {
            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image
            
        }
        
        return nil
    }
    override func viewDidAppear(_ animated: Bool) {
    
//        if isKeyPresentInUserDefaults(key: UserDefaultsContants.SELECTED_CAMERA_IMAGE) == true {
//            self.cameraImageView.isHidden = isImageHidden
//            self.cameraImageView.image = UserDefaults.standard.imageForKey(key: UserDefaultsContants.SELECTED_CAMERA_IMAGE)
//        }
//        if isKeyPresentInUserDefaults(key: UserDefaultsContants.SELECTED_GALLERY_IMAGE) == true{
//            self.galleryImageView.isHidden = isImageHidden
//            self.galleryImageView.image = UserDefaults.standard.imageForKey(key: UserDefaultsContants.SELECTED_GALLERY_IMAGE)
//        }
        
        self.galleryImageView.isHidden = isImageHidden
        self.cameraImageView.isHidden = isImageHidden
        DispatchQueue.main.async {
            if self.loadImageFromDiskWith(fileName: "profilePhoto") != nil {
                self.cameraImageView.image = self.loadImageFromDiskWith(fileName: "profilePhoto")
            }
            
            if self.loadImageFromDiskWith(fileName: "panPhoto") != nil {
                self.galleryImageView.image = self.loadImageFromDiskWith(fileName: "panPhoto")
            }

        }
      
        //        if imageClicked != nil{
//
//            self.galleryImageView.image = imageClicked
//
//        }
//        if imagePicked != nil {
//
//            self.cameraImageView.image = imagePicked
//        }
        
    }
  
    @objc func keyboardWillShow(notification: NSNotification) {
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        
      
        UIView.animate(withDuration: 0.2) {
            self.bottomView.transform = CGAffineTransform.identity.translatedBy(x: 0, y: -keyboardHeight)
        }
    }
    func imageData(imaage: UIImage) {
    print("data")
        self.imageClicked = imaage
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        _ = keyboardRectangle.height
        UIView.animate(withDuration: 0.2) {
            self.bottomView.transform = CGAffineTransform.identity
        }
    }
    
//    @objc func handleBackButtonTap(_ sender: UITapGestureRecognizer) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier :"SelectServiceVC")
//        transitionVc(vc: viewController, duration: 0.5, type: .fromLeft)
//    }

    @objc func handleNextButtonTap(_ sender: UITapGestureRecognizer) {
       
        let fullName    = fullnametf.text!
        var firstname = ""
        var surname = ""
        if fullName.contains(" ") {
            let fullNameArr = fullName.components(separatedBy:" ")
            firstname = fullNameArr[0]
            surname = fullNameArr[1]
        }else{
            firstname = fullnametf.text!
            surname = ""
        }
        userDefaults.set("\(firstname)", forKey: RegisterConstants.FirstName)
        userDefaults.set("\(surname)", forKey: RegisterConstants.Surname)
          userDefaults.set("\(mobiletf.text!)", forKey: RegisterConstants.MobileNo)
        userDefaults.set("\(emailtf.text!)", forKey: RegisterConstants.Email)
        userDefaults.set("\(passwordtf.text!)", forKey: RegisterConstants.passward)
        userDefaults.set("\(confirmPwdTf.text!)", forKey: RegisterConstants.confirmPassword)
       userDefaults.synchronize()
        
        
        print("1 \(UserDefaults.standard.value(forKey: UserDefaultsContants.MTX_STATE)! as! String)")
        print("2 \(UserDefaults.standard.value(forKey: UserDefaultsContants.SELECTED_SERVICE) as! String)")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"step2VC") as! Step2RegisterVC
        viewController.modalPresentationStyle = .fullScreen
        viewController.userData = user
        transitionVc(vc: viewController, duration: 0.5, type: .fromRight)
        
       
    }
    var user = [User]()
    
    func getUserData() -> [User]?{
        let fullName    = fullnametf.text!
        var firstname = ""
        var surname = ""
        if fullName.contains(" ") {
            let fullNameArr = fullName.components(separatedBy:" ")
            firstname = fullNameArr[0]
            surname = fullNameArr[1]
        }else{
            firstname = fullnametf.text!
            surname = ""
        }
        
        let us = User(firstName: firstname, surname: surname, designation: "", mobileNo: mobiletf.text!, email: emailtf.text!, serviceType: UserDefaults.standard.value(forKey: UserDefaultsContants.MTX_STATE)! as! String, appActivated: UserDefaults.standard.value(forKey: UserDefaultsContants.SELECTED_SERVICE) as! String)
            user.append(us)
        
        return user
    }
  

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    override func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension String {
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    func isPanNumberValid() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "[A-Z]{5}[0-9]{4}[A-Z]{1}", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
}
