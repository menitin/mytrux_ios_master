//
//  BidListTableViewCell.swift
//  Mytrux
//
//  Created by Aboli on 05/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class BidListTableViewCell: UITableViewCell {
   @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var bidCodeLAbel: UILabel!
    @IBOutlet weak var availableLocation: UILabel!
    @IBOutlet weak var vehicleTypeName: UILabel!
    @IBOutlet weak var destinationLocation: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainView.layer.cornerRadius = 5
        mainView.layer.shadowColor = UIColor.gray.cgColor
        mainView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        mainView.layer.shadowRadius = 5
        mainView.layer.shadowOpacity = 0.4
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
