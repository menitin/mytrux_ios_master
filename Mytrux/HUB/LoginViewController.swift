//
//  LoginViewController.swift
//  Mytrux
//
//  Created by Mukta Bhuyar Punjabi on 21/05/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SideMenuSwift


class LoginViewController: BaseViewController,UITextFieldDelegate {
    
    @IBOutlet weak var language: UITextField!
    @IBOutlet weak var country: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var mobileNumber: UITextField!
    @IBOutlet weak var registerStackView: UIStackView!
    @IBOutlet weak var registerBtn: UILabel!
    @IBOutlet weak var loginBtn: UIView!
    @IBOutlet weak var backBtn: UIView!
    @IBOutlet weak var logoLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var logoView: UIView!
    @IBOutlet weak var textFieldWrapper: UIView!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var selectCountryBtn: UIButton!
    @IBOutlet weak var selectLangBtn: UIButton!
    var image_logo = ""
    var label_title = ""
    
    
    @IBOutlet weak var serviceState: UILabel!
    var state = String()
    var loginData = [String:Any]()
    @IBOutlet weak var showBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        password.isSecureTextEntry = true
        state = UserDefaults.standard.value(forKey: UserDefaultsContants.MTX_STATE)! as! String
        self.serviceState.text! = UserDefaults.standard.value(forKey: UserDefaultsContants.SERVICE_STATE)! as! String
        print("state = \(state)")
        if state == "PRO"{
            logoImageView.image = UIImage.init(named: "Mtx_Pro")
            logoLabel.text = "MTX PRO"
        }else if state == "GO"{
            logoImageView.image = UIImage.init(named: "Mtx_Go")
            logoLabel.text = "MTX GO"
        }
        
      
        mobileNumber.delegate = self
        language.delegate = self
        country.delegate = self
        password.delegate = self
        country.isEnabled = false
        language.isEnabled = false
        showBtn.setImage( UIImage(named:"password_inactive"), for: .normal)
        topBarCard(topBar: topBar)
        let arr = [logoView,textFieldWrapper,loginBtn]
        cardViewArray(arrView: arr as! [UIView])
        
        let backtap = UITapGestureRecognizer(target: self, action: #selector(self.handleBackButtonTap(_:)))
        backBtn.addGestureRecognizer(backtap)
        backBtn.isUserInteractionEnabled = true
        self.topBar.addSubview(backBtn)
        
        let regBtn = UITapGestureRecognizer(target: self, action: #selector(self.handleRegisterButtonTap(_:)))
        registerBtn.addGestureRecognizer(regBtn)
        registerBtn.isUserInteractionEnabled = true
        self.registerStackView.addSubview(registerBtn)
        // Do any additional setup after loading the view.
    }
   
    func presentModal(title:String,item:String){
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let presentedViewController = storyBoard.instantiateViewController(withIdentifier: "ModelVC") as! ModelViewController
        
        presentedViewController.providesPresentationContextTransitionStyle = true
        presentedViewController.definesPresentationContext = true
        presentedViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
        presentedViewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        presentedViewController.isSelectModalPresent = true
        presentedViewController.isCameraModalPresent = false
        presentedViewController.titleLabel.text = title
        presentedViewController.itemLabel.text = item
        self.present(presentedViewController, animated: true, completion: nil)
    }
    
   
    
    func login(){
        let loginUserType = UserDefaults.standard.value(forKey: UserDefaultsContants.LOGIN_USER_TYPE)! as! String
        let login = Login(loginUserType:loginUserType,fcmToken:"test",mobileNo:"+91" + mobileNumber.text!,password:password.text!,appActivated: UserDefaults.standard.value(forKey: UserDefaultsContants.SELECTED_SERVICE) as! String,serviceType:UserDefaults.standard.value(forKey: UserDefaultsContants.MTX_STATE)! as! String)
        let jsonDataa = try! JSONEncoder().encode(login)
        let type = String(data: jsonDataa, encoding: .utf8)!
        do {
            self.loginData = try self.convertToDictionary(from: type)
            print(self.loginData)
        } catch {
            print(error)
        }
        let parameters : [String:Any] = loginData
        
        
        //+918669696004
        //asdfgf1234$
        //8888870909
        //asdfgf1234$
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
       
        self.activityIndicatorBegin()
        Alamofire.request(URLStatics.hubLogin, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: [:])
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                let resultNSString = NSString(data: response.request!.httpBody!, encoding: String.Encoding.utf8.rawValue)!
                print("response.request?.httpBody = \(resultNSString)")
                switch (response.result) {
                case .success:
                    
                    if let reply = response.result.value {
                        print("JSON: \(reply)")
                        self.activityIndicatorEnd()
                        let mainResponse = JSON(reply)
                        if mainResponse["warning"].stringValue != "" {
                            let alert = UIAlertController(title:"", message: mainResponse["warning"].stringValue, preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                        if mainResponse["token"] != ""{
                            
                            print("mainResponse = \(mainResponse["token"])")
                            self.userDefaults.set(mainResponse["token"].stringValue, forKey: LoginDataConstants.token)
                            self.userDefaults.set(mainResponse["appActivated"].stringValue, forKey: LoginDataConstants.appActivated)
                            self.userDefaults.set(mainResponse["companyCode"].stringValue, forKey: LoginDataConstants.companyCode)
                            self.userDefaults.set(mainResponse["companyLogo"].stringValue, forKey: LoginDataConstants.companyLogo)
                            self.userDefaults.set(mainResponse["designation"].stringValue, forKey: LoginDataConstants.designation)
                            self.userDefaults.set(mainResponse["emailId"].stringValue, forKey: LoginDataConstants.emailId)
                            self.userDefaults.set(mainResponse["erpStatus"].stringValue, forKey: LoginDataConstants.erpStatus)
                            self.userDefaults.set(mainResponse["financeYear"].stringValue, forKey: LoginDataConstants.financeYear)
                            self.userDefaults.set(mainResponse["firstName"].stringValue, forKey: LoginDataConstants.firstName)
                            self.userDefaults.set(mainResponse["fleetId"].stringValue, forKey: LoginDataConstants.fleetId)
                            self.userDefaults.set(mainResponse["fleetName"].stringValue, forKey: LoginDataConstants.fleetName)
                            self.userDefaults.set(mainResponse["hubStatus"].stringValue, forKey: LoginDataConstants.hubStatus)
                            self.userDefaults.set(mainResponse["id"].stringValue, forKey: LoginDataConstants.id)
                            self.userDefaults.set(mainResponse["lastName"].stringValue, forKey: LoginDataConstants.lastName)
                            self.userDefaults.set(mainResponse["mobileNo"].stringValue, forKey: LoginDataConstants.mobileNo)
                            self.userDefaults.set(mainResponse["profilePic"].stringValue, forKey: LoginDataConstants.profilePic)
                            self.userDefaults.set(mainResponse["serviceType"].stringValue, forKey: LoginDataConstants.serviceType)
                            self.userDefaults.set(mainResponse["userType"].stringValue, forKey: LoginDataConstants.userType)
                            self.userDefaults.synchronize()
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let viewController = storyboard.instantiateViewController(withIdentifier :"SideMenuController")
                            viewController.modalPresentationStyle = .fullScreen
                            self.transitionVc(vc: viewController, duration: 0.5, type: .fromLeft)
                            
                            
                        }
                        
                    }
                    
                    break
                case .failure(let error):
                    
                    print("error \(error.localizedDescription)")
                    break
                }
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == mobileNumber) {
            let maxLength = 10
            let currentString: NSString = mobileNumber.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        if(textField == password){
            let maxLength = 30
            let currentString: NSString = password.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }else{
            return true
        }
    }
    @IBAction func selectLanguageClicked(_ sender: UIButton) {
        self.presentModal(title: "Select Language", item: "English")
    }
    @IBAction func selectCountryClicked(_ sender: Any) {
        self.presentModal(title: "Select Country", item: "India")
    }
    @IBAction func showHidePasswordClicked(_ sender: UIButton) {
        if showBtn.currentImage == UIImage(named:"password_inactive") {
            (sender as AnyObject).setImage(UIImage(named:"password_active"), for: .normal)
            self.password.isSecureTextEntry = false
        }
        else if  showBtn.currentImage == UIImage(named:"password_active") {
            (sender as AnyObject).setImage( UIImage(named:"password_inactive"), for: .normal)
            self.password.isSecureTextEntry = true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func handleRegisterButtonTap(_ sender: UITapGestureRecognizer){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "step1VC")
        viewController.modalPresentationStyle = .fullScreen
        transitionVc(vc: viewController, duration: 0.5, type: .fromRight )
    }
    
    @objc func handleBackButtonTap(_ sender: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"SelectServiceVC")
        viewController.modalPresentationStyle = .fullScreen
        transitionVc(vc: viewController, duration: 0.5, type: .fromLeft)
    }
    @IBAction func login(_ sender: UITapGestureRecognizer) {
       login()
//          let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//                let viewController = storyBoard.instantiateViewController(withIdentifier: "SideMenuController")
//                transitionVc(vc: viewController, duration: 0.5, type: .fromRight )
        
       // sideMenuController?.setContentViewController(with: "default")
    }
    @IBAction func selectCountry(_ sender: UITapGestureRecognizer) {
        self.presentModal(title: "Select Country", item: "India")
    }
    
    @IBAction func selectLanguage(_ sender: UITapGestureRecognizer) {
        self.presentModal(title: "Select Language", item: "English")
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
