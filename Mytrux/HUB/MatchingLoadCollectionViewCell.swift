//
//  MatchingLoadCollectionViewCell.swift
//  Mytrux
//
//  Created by Aboli on 10/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class MatchingLoadCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var quotedView: UIView!
    @IBOutlet weak var loadVehicleType: UILabel!
    @IBOutlet weak var loadId: UILabel!
    @IBOutlet weak var loadTo: UILabel!
    @IBOutlet weak var loadFrom: UILabel!
    @IBOutlet weak var fleetId: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainView.layer.cornerRadius = 5
        mainView.layer.shadowColor = UIColor.gray.cgColor
        mainView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        mainView.layer.shadowRadius = 4
        mainView.layer.shadowOpacity = 0.4
        
    }
}
