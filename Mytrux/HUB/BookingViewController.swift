//
//  BookingViewController.swift
//  Mytrux
//
//  Created by Aboli on 27/06/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import SideMenuSwift
import Alamofire
import SwiftyJSON

class BookingViewController: BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    var imageArray = [UIImage]()
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var assignViewLabel: UILabel!
    @IBOutlet weak var completedView: UIView!
    @IBOutlet weak var assignView: UIView!
    @IBOutlet weak var inTransitView: UIView!
    @IBOutlet weak var myBookingView: UIView!
    @IBOutlet weak var collectionVIew: UICollectionView!
    @IBOutlet weak var myBookingCountLabel: UILabel!
    @IBOutlet weak var inTransitCountLAbel: UILabel!
    @IBOutlet weak var assignCounttLabel: UILabel!
    @IBOutlet weak var completedCountLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topBarCard(topBar: topBar)
        cardViewArray(arrView: [myBookingView,inTransitView,assignView,completedView])
        collectionVIew.delegate = self
        collectionVIew.dataSource = self
        imageArray = [UIImage(named: "Ticket1")!,UIImage(named: "Ticket2")!,UIImage(named: "Ticket3")!]
        if self.loginUserType == UserStates.SERVICE_USER{
            self.assignViewLabel.text = "New Booking"
        }else{
            self.assignViewLabel.text = "Assign"
        }
        bookingDashboardCount()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func myBookingClicked(_ sender: Any) {
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookingController") }, with: "tenth")
        
        sideMenuController?.setContentViewController(with: "tenth")
        
        NotificationCenter.default.post(name: Notification.Name("MyBooking"), object: nil,userInfo: ["status" : UserStates.ALL])
        
    }
    
    @IBAction func assignClicked(_ sender: Any) {
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookingController") }, with: "tenth")
        print("Assign")
        sideMenuController?.setContentViewController(with: "tenth")
        NotificationCenter.default.post(name: Notification.Name("Assign"), object: nil,userInfo: ["status" : UserStates.NEW])
    }
    
    @IBAction func inTransit(_ sender: Any) {
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookingController") }, with: "tenth")
        print("InTransit")
        sideMenuController?.setContentViewController(with: "tenth")
        NotificationCenter.default.post(name: Notification.Name("InTransit"), object: nil,userInfo: ["status" : UserStates.ASSIGN])
    }
    
    @IBAction func postFleetClicked(_ sender: Any) {
        if loginUserType == UserStates.SERVICE_USER{
            sideMenuController?.setContentViewController(with: "ninth")
        }else{
            sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "PostFleetVC") }, with: "fourth")
            sideMenuController?.setContentViewController(with: "fourth")
        }
    }
    
    
    func bookingDashboardCount(){
        self.activityIndicatorBegin()
       
        
        let headers = header()
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
        
        
        Alamofire.request(URLStatics.bookingDashboardCount+"financialYear="+self.currentFinancialYear!, method:.get,encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                
                switch (response.result) {
                case .success:
                    self.activityIndicatorEnd()
                    if let reply = response.result.value {
                        
                        let mainResponse = JSON(reply)
                        print("======= JSON: bookingDashboardCount ========  \(mainResponse)")
                        self.myBookingCountLabel.text = mainResponse["allBookingCount"].stringValue
                        self.assignCounttLabel.text = mainResponse["newBookingCount"].stringValue
                        self.inTransitCountLAbel.text = mainResponse["assignBookingCount"].stringValue
                        self.completedCountLabel.text = mainResponse["completedBookingCount"].stringValue
                    }
                    break
                case .failure(let error):
                    
                    print("== error == \(error.localizedDescription)")
                    break
                }
        }
        
        
        
        
    }
    
    
    
    @IBAction func completed(_ sender: Any) {
        
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookingController") }, with: "tenth")
        
        sideMenuController?.setContentViewController(with: "tenth")
        
        NotificationCenter.default.post(name: Notification.Name("Completed"), object: nil,userInfo: ["status" : UserStates.COMPLETE])
        print("Completed")
        
    }
    @IBAction func trackTransitClicked(_ sender: Any) {
         sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookingController") }, with: "tenth")
        sideMenuController?.setContentViewController(with: "tenth")
        NotificationCenter.default.post(name: Notification.Name("trackTransit"), object: nil,userInfo: ["status" : UserStates.WITHOUTNEW])
        print("trackTransit")
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionVIew.dequeueReusableCell(withReuseIdentifier: "bookingCell", for: indexPath) as! BookingCollectionViewCell
        let imageName = imageArray[indexPath.item]
        cell.imageView.image = imageName
        return cell
    }
    
    @IBAction func menuBtn(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
