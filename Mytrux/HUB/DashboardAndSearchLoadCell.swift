//
//  DashboardAndSearchLoadCell.swift
//  Mytrux
//
//  Created by Aboli on 11/06/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class DashboardAndSearchLoadCell: UITableViewCell {
    
   

    @IBOutlet weak var callBtn: UIView!
    @IBOutlet weak var fleetPostedwrapper: UIView!
   
  
    @IBOutlet weak var vehicleView: UIStackView!
    @IBOutlet weak var makeBookingBtn: UIView!
    @IBOutlet weak var bookingNumberStack: UIStackView!
 
    @IBOutlet weak var loadsPostedWrapper: UIView!
   
    @IBOutlet weak var loadsQuotedView: UIView!
    @IBOutlet weak var loadView1: UIView!
    @IBOutlet weak var view_1: UIView!
    @IBOutlet weak var loadView2: UIView!
    @IBOutlet weak var view_2: UIView!
    @IBOutlet weak var view_3: UIView!
    
    @IBOutlet weak var loadCameraView: UIView!
   
    @IBOutlet weak var mainView2: UIView!
  
    @IBOutlet weak var sealNoStack: UIStackView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var containerNoStack: UIStackView!
    @IBOutlet weak var assignButtonLabel: UILabel!
    @IBOutlet weak var fleetCargoWtOrNAView: UILabel!
    
    @IBOutlet weak var loadVIew3: UIView!
    @IBOutlet weak var viewBidBtn: UIView!
    @IBOutlet weak var messageImageLoadPosted: UIImageView!
    @IBOutlet weak var editView2: UIView!
    @IBOutlet weak var cameraView2: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var seperatorFleetView: UIImageView!
    @IBOutlet weak var freightViewFleet: UIView!
    
    //Assign and Loads Quoted
    @IBOutlet weak var bookingNo: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var loadId: UILabel!
    @IBOutlet weak var vehicleType: UILabel!
    @IBOutlet weak var fleetId: UILabel!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var rankView: UIView!
    @IBOutlet weak var freightAmount: UILabel!
    @IBOutlet weak var registrationNo: UILabel!
    @IBOutlet weak var loadType: UILabel!
    @IBOutlet weak var noOfVehicle: UILabel!
    @IBOutlet weak var bookingDate: UILabel!
    @IBOutlet weak var cargoType: UILabel!
    @IBOutlet weak var cargoWt: UILabel!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var camera: UIView!
    @IBOutlet weak var bookingTime: UILabel!
    @IBOutlet weak var callView: UIView!
    @IBOutlet weak var assignView: UIView!
    @IBOutlet weak var lowestBidLabel: UILabel!
    @IBOutlet weak var bidNowView: UIView!
    @IBOutlet weak var lowestBidView: UIView!
    @IBOutlet weak var rankCountLabel: UILabel!
    
    
    
    
    
    
    
    //  Fleet Posted //
      @IBOutlet weak var fleetView1: UIView!
      @IBOutlet weak var fleetView2: UIView!
    @IBOutlet weak var fleetView3: UIView!
    @IBOutlet weak var inviteFleetView: UIView!
    @IBOutlet weak var fleetPostedId: UILabel!
    @IBOutlet weak var fleetPostedVechType: UILabel!
      @IBOutlet weak var editView: UIView!
    @IBOutlet weak var fleetPostedFrom: UILabel!
    @IBOutlet weak var fleetPostedTo: UILabel!
    @IBOutlet weak var fleetPostedAmount: UILabel!
    @IBOutlet weak var fleetPostedRegistrationNo: UILabel!
    @IBOutlet weak var fleetPostedLoadType: UILabel!
    @IBOutlet weak var fleetPostedAvailFromDate: UILabel!
   
    @IBOutlet weak var fleetPostedAvailTillDate: UILabel!
    @IBOutlet weak var fleetPostedCargoWt: UILabel!
    @IBOutlet weak var fleetPOstedCargoType: UILabel!
    @IBOutlet weak var fleetPOstedAvailFromTime: UILabel!
    @IBOutlet weak var fleetPostedNoOfVech: UILabel!
    @IBOutlet weak var fleetPostedAvailTillTime: UILabel!
     @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var messageCountLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        messageImageLoadPosted.isHidden = false
        messageImageLoadPosted.image = UIImage(named: "messagepdf")
        messageImageLoadPosted.image = messageImageLoadPosted.image?.withRenderingMode(.alwaysTemplate)
        messageImageLoadPosted.tintColor = UIColor(hexString: ColorConstants.YELLOW)
        cardViewArray(viewArr: [mainView,mainView2,fleetPostedwrapper,loadsPostedWrapper])
        view2.isHidden = true
        view3.isHidden = true
        loadsQuotedView.isHidden = true
        fleetView2.isHidden = true
        fleetPostedwrapper.isHidden = true
        
    }
    
    func cardViewArray(viewArr:[UIView]){
        for view in viewArr{
            view.layer.cornerRadius = 4
            view.layer.shadowColor = UIColor.gray.cgColor
            view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            view.layer.shadowRadius = 5
            view.layer.shadowOpacity = 0.4
        }
    }
    
    func circularView(circleViewArr:[UIView]){
        for circleView in circleViewArr{
            circleView.layoutIfNeeded()
            circleView.layer.cornerRadius = circleView.frame.size.height / 2
            circleView.layer.masksToBounds = true
            circleView.clipsToBounds = true
        }
    }
    
    override func setNeedsLayout() {
        circularView(circleViewArr: [camera,cameraView2,loadCameraView,rankView,editView,editView2])
        fleetView2.layoutIfNeeded()
        fleetView1.layoutIfNeeded()
        loadView2.layoutIfNeeded()
        loadView1.layoutIfNeeded()
        loadsPostedWrapper.layoutIfNeeded()
        fleetPostedwrapper.layoutIfNeeded()
       
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
