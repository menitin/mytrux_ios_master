//
//  FleetPostCellTableViewCell.swift
//  Mytrux
//
//  Created by Aboli on 20/06/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class FleetPostCell: UITableViewCell {
    
    //Fleet
     @IBOutlet weak var fleetPostView: UIView!
     @IBOutlet weak var fleetView1: UIView!
     @IBOutlet weak var fleetView2: UIView!
     @IBOutlet weak var fleetView3: UIView!
     @IBOutlet weak var editView: UIView!
    
     @IBOutlet weak var msgLabel: UILabel!
     @IBOutlet weak var fleetVerticalImage: UIImageView!
     @IBOutlet weak var invitesBtn: UIView!
    @IBOutlet weak var fleetID: UILabel!
    @IBOutlet weak var vehType: UILabel!
    @IBOutlet weak var fleetFrom: UILabel!
    @IBOutlet weak var fleetTo: UILabel!
    @IBOutlet weak var freightView: UIView!
    @IBOutlet weak var freightAmountLabel: UILabel!
    @IBOutlet weak var fleetLoadType: UILabel!
    @IBOutlet weak var fleetNoOfVech: UILabel!
    @IBOutlet weak var fleetAvailFromdate: UILabel!
    @IBOutlet weak var fleetAvailTillDate: UILabel!
    @IBOutlet weak var fleetCargoType: UILabel!
    @IBOutlet weak var fleetCargoWt: UILabel!
    @IBOutlet weak var fleetAvailFromTime: UILabel!
    @IBOutlet weak var fleetAvailTillTime: UILabel!
    @IBOutlet weak var fleetMessageView: UIView!
    @IBOutlet weak var fleetRegNoLabel: UILabel!
    
    

    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var mainView2: UIView!
    @IBOutlet weak var loadsPostedView: UIView!
    @IBOutlet weak var loadView3: UIView!
    @IBOutlet weak var messageImageView: UIImageView!
    @IBOutlet weak var loadView2: UIView!
    @IBOutlet weak var editView2: UIView!
    @IBOutlet weak var loadView1: UIView!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var loadVerticalImage: UIImageView!
    @IBOutlet weak var viewBidBtn: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //        fleetPostView.layer.cornerRadius = 4
        //        fleetPostView.layer.shadowColor = UIColor.gray.cgColor
        //        fleetPostView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        //        fleetPostView.layer.shadowRadius = 5
        //        fleetPostView.layer.shadowOpacity = 0.4
       fleetMessageView.isHidden = true
        messageImageView.image = UIImage(named: "messagepdf")
        messageImageView.image = messageImageView.image?.withRenderingMode(.alwaysTemplate)
        messageImageView.tintColor = UIColor(hexString: ColorConstants.YELLOW)
       loadVerticalImage.image = UIImage.init(named: "redVerticalLine")
       loadVerticalImage.image = loadVerticalImage.image?.withRenderingMode(.alwaysTemplate)
       loadVerticalImage.tintColor = UIColor.init(hexString: ColorConstants.GREEN)
        fleetView2.isHidden = true
        fleetView3.isHidden = true
        cardViewArray(viewArr: [fleetPostView,loadsPostedView])
        circularView(circleViewArr: [editView2,editView,cameraView])
    }
    
    func cardViewArray(viewArr:[UIView]){
        
        for view in viewArr{
            view.layer.cornerRadius = 4
            view.layer.shadowColor = UIColor.gray.cgColor
            view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            view.layer.shadowRadius = 5
            view.layer.shadowOpacity = 0.4
        }
    }
    
    func circularView(circleViewArr:[UIView]){
        for circleView in circleViewArr{
            circleView.layoutIfNeeded()
            circleView.layer.cornerRadius = circleView.frame.size.height / 2
            circleView.layer.masksToBounds = true
            circleView.clipsToBounds = true
        }
    }
    override func setNeedsLayout() {
          
        
        //        editView.layoutIfNeeded()
        //        editView.layer.cornerRadius = editView.frame.size.height / 2
        //        editView.layer.masksToBounds = true
        //        editView.clipsToBounds = true
    }
//
    
//    override func prepareForReuse() {
//        super .prepareForReuse()
//        fleetID.text = ""
//        vehType.text = ""
//        fleetFrom.text = ""
//        fleetTo.text = ""
//    }
//    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
