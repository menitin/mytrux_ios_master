//
//  BookingDetailViewController.swift
//  Mytrux
//
//  Created by Aboli on 16/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class BookingDetailViewController: BaseViewController {
    
    @IBOutlet weak var backBtn: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var bookingNo: UILabel!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var pickupDate: UILabel!
    @IBOutlet weak var dropDate: UILabel!
    @IBOutlet weak var pickupAddress: UILabel!
    @IBOutlet weak var dropAddress: UILabel!
    @IBOutlet weak var consignmentLocation: UILabel!
    @IBOutlet weak var load: UILabel!
    @IBOutlet weak var pickupTime: UILabel!
    @IBOutlet weak var dropTime: UILabel!
    @IBOutlet weak var startReadingKm: UILabel!
    @IBOutlet weak var transporterNAme: UILabel!
    @IBOutlet weak var transpoterMobileNo: UILabel!
    @IBOutlet weak var transporterAddress: UILabel!
    @IBOutlet weak var registrationNo: UILabel!
    
    @IBOutlet weak var vehicleType: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cardViewArray(arrView: [mainView])
        buttonCardView(button: backBtn)
       
        NotificationCenter.default.addObserver(self, selector: #selector(self.bookingDetaill(notification:)), name: .bookingDetailController , object: nil)
        // Do any additional setup after loading the view.
    }
    var bookingDetail : BookingElement?
    @objc func bookingDetaill(notification: Notification) {
    let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        let booking = userInfo!["bookinng"] as! BookingElement
        print("booking no === \(String(describing: booking.bookingNo!))")
        bookingDetail = booking
        self.bookingNo.text! = booking.bookingNo!
        let pickUpTimestamp = Double(booking.pickupDateTime! / 1000)
        let deliveryTimestamp = Double(booking.deliveryDateTime! / 1000)
        self.pickupDate.text = epochDate(epochDate: pickUpTimestamp)
        self.dropDate.text = epochDate(epochDate: deliveryTimestamp)
        self.pickupAddress.text = booking.pickUpLocation!.uppercased()
        self.dropAddress.text = booking.destination!.uppercased()
        self.consignmentLocation.text = booking.consignee?.location!.uppercased()
        self.pickupTime.text = epochDayDate(epochDate: pickUpTimestamp)
        self.dropTime.text = epochDayDate(epochDate: deliveryTimestamp)
        self.fromLabel.text = booking.shipper?.location!.uppercased()
        self.toLabel.text = booking.deliveryCustomer?.location!.uppercased()
        self.load.text = booking.loadType?.loadType
        let jobVehicle = booking.jobVehiclesDrivers![0]
        self.vehicleType.text = jobVehicle.vehicleType?.name.uppercased()
        self.registrationNo.text = jobVehicle.vehicleRegistrationNo!
        self.transporterNAme.text = booking.shipper?.name!.uppercased()
        self.transpoterMobileNo.text = booking.shipper?.contactNo!
        self.transporterAddress.text = "\(String(describing: booking.shipper!.name!))" + "\(String(describing: booking.shipper!.address!))"
        if jobVehicle.startKM == nil{
            self.startReadingKm.text = "0.0" + " Kms"
        }else{
            self.startReadingKm.text = "\(String(describing: jobVehicle.startKM))" + "Kms"
        }
    }
    
    @IBAction func backClicked(_ sender: Any) {
        
          sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "TrackViewController") }, with: "twelve")
        sideMenuController?.setContentViewController(with: "twelve")
        NotificationCenter.default.post(name: .track ,object: nil,userInfo:["booking":self.bookingDetail!])
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
