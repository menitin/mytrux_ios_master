//
//  FleetDetailViewController.swift
//  Mytrux
//
//  Created by Mytrux on 26/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class FleetDetailViewController: BaseViewController {

    @IBOutlet weak var freightView: UIView!
    @IBOutlet weak var vehicleTypeLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var freightAmountLabel: UILabel!
    @IBOutlet weak var registrationLabel: UILabel!
    @IBOutlet weak var loadType: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var noOfVechLabel: UILabel!
    @IBOutlet weak var fleetPostedView: UIView!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var availableTillDate: UILabel!
    @IBOutlet weak var cargoTypeLabel: UILabel!
    @IBOutlet weak var availableFromDate: UILabel!
    @IBOutlet weak var availableFromTime: UILabel!
    @IBOutlet weak var availableTillTime: UILabel!
    @IBOutlet weak var cargoWtLabel: UILabel!
    @IBOutlet weak var commentsLabel: UILabel!
    
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        circularView(viewArray: [editView])
        mainView.layer.cornerRadius = 20
        fleetPostedView.layer.cornerRadius = 20
           NotificationCenter.default.addObserver(self, selector: #selector(self.fleetDetailsBottom(_:)), name: .fleetDetailsBottom, object: nil)
        // Do any additional setup after loading the view.
    }
    
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        scrollView.contentOffset.x = 0
    }
    
    
 @objc func fleetDetailsBottom(_ notification: NSNotification) {
       let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
      let fleetDetails = userInfo!["fleetDetails"] as! [FleetDatum]
    let bidCode = userInfo!["id"] as! String
    
    
    for fleet in fleetDetails {
        if bidCode == fleet.bidCode{
            self.idLabel.text = fleet.bidCode
            self.vehicleTypeLabel.text = fleet.vehicleType.name
            self.fromLabel.text = fleet.availableLocation
            self.toLabel.text = fleet.destinationLocation!
            self.freightAmountLabel.text = String(Int(truncating: fleet.expectedFreight!))
            self.loadType.text = fleet.loadType.loadType
            self.cargoTypeLabel.text = fleet.cargoType.cargoType
            self.noOfVechLabel.text = String(fleet.noOfVehicles)
            self.cargoWtLabel.text = fleet.capacityWeightUnit.name
            self.commentsLabel.text = fleet.comments!
            let timestap: Double = Double(fleet.availableDateTime / 1000)
            let availableFromtime = self.epochTimeSec(epochTimee: timestap)
            let availableFromdate = self.epochDate(epochDate: timestap)
            let timestamp: Double = Double(fleet.closedBidTime / 1000)
            let availableTilltime = self.epochTimeSec(epochTimee: timestamp)
            let availableTilldate = self.epochDate(epochDate: timestamp)
            self.availableFromDate.text = availableFromdate
            self.availableTillTime.text = availableTilltime
            self.availableFromTime.text = availableFromtime
            self.availableTillDate.text = availableTilldate
            
        }
    }
    
    
    
    
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
