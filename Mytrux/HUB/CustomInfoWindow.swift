//
//  CustomInfoWindow.swift
//  Mytrux
//
//  Created by Aboli on 14/06/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

protocol MapMarkerDelegate: class {
    func didTapMoreButton(data: String)
}
class CustomInfoWindow: UIView {
    @IBOutlet weak var circularView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var quotedImage: UIView!
    @IBOutlet weak var stackV: UIStackView!
      weak var delegate: MapMarkerDelegate?
    var str = "abcd"
    override init(frame: CGRect) {
        super.init(frame: frame)
    
    }
    @IBAction func moreButton(_ sender: Any) {
          print("viewMoreClicked")
        delegate?.didTapMoreButton(data:str)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func loadView() -> CustomInfoWindow{
        let customInfoWindow = Bundle.main.loadNibNamed("CustomInfoWindow", owner: self, options: nil)?[0] as! CustomInfoWindow
   
        return customInfoWindow
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
