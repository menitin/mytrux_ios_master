//
//  DashboardAndSearchLoad.swift
//  Mytrux
//
//  Created by Aboli on 11/06/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON
import SideMenuSwift


class DashboardAndSearchLoad: BaseViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate {
    
    // FLEET INFO VIEW
    @IBOutlet weak var fleetInfoId: UILabel!
    @IBOutlet weak var fleetInfoVehicleType: UILabel!
    @IBOutlet weak var fleetInfoCargoType: UILabel!
    @IBOutlet weak var fleetInfoLoadType: UILabel!
    @IBOutlet weak var fleetInfoTo: UILabel!
    @IBOutlet weak var fleetInfoFrom: UILabel!
    @IBOutlet weak var amountTF: UITextField!
    @IBOutlet weak var validDateTF: UITextField!
    @IBOutlet weak var validTimeTF: UITextField!
    @IBOutlet weak var timeLabel: UITextField!
    
    // *****
    
    @IBOutlet weak var lastBidLabel: UILabel!
    @IBOutlet weak var maniVIew: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchLoadView: UIView!
    @IBOutlet weak var dashboardView1: UIView!
    @IBOutlet weak var searchloadView1: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dashboardView: UIView!
    @IBOutlet weak var slider: StepSlider!
    @IBOutlet weak var assignVechImage: UIImageView!
    @IBOutlet weak var assignVechLabel: UILabel!
    @IBOutlet weak var fleetPostedImage: UIImageView!
    @IBOutlet weak var fleetPostLabel: UILabel!
    @IBOutlet weak var searchLabel: UILabel!
    @IBOutlet weak var loadsQuotedImage: UIImageView!
    @IBOutlet weak var loadsQuotedLabel: UILabel!
    @IBOutlet weak var assignVechLine: UIImageView!
    @IBOutlet weak var seperatorLineView: UIView!
    @IBOutlet weak var fleetPostLine: UIImageView!
    @IBOutlet weak var loadQuotedLine: UIImageView!
    @IBOutlet weak var containerLabel: UILabel!
    @IBOutlet weak var containerLine: UIImageView!
    @IBOutlet weak var FTLLabel: UILabel!
    @IBOutlet weak var ftlLine: UIImageView!
    @IBOutlet weak var ltlLabel: UILabel!
    @IBOutlet weak var ltlLine: UIImageView!
    @IBOutlet weak var tableViewWrapper: UIView!
    @IBOutlet weak var searchLoadMainView: UIView!
    @IBOutlet weak var availabelVechCollectionView: UICollectionView!
    @IBOutlet weak var submitQuoteBtn: UIView!
    @IBOutlet weak var availableLoadCollectionView: UICollectionView!
    @IBOutlet weak var inviteFleetBtn: UIView!
    @IBOutlet weak var availableLoadDetails: UIView!
    @IBOutlet weak var postLoadView: UIView!
    @IBOutlet weak var fleetInviteWraperView: UIView!
    @IBOutlet weak var submitQuoteWrraperView: UIView!
    @IBOutlet weak var postLoadTraillingConstraint: NSLayoutConstraint!
    @IBOutlet weak var postFleetTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var addPostFleetBtn: UIView!
    @IBOutlet weak var fleetInfoView: UIView!
    var isContainerClicked = false
    var isFTLClicked = false
    var isLTLClicked = false
    var FleetVehicleNameArray = [String]()
    var ContainerVehicleNameArray = [String]()
    var LTLVehicleNameArray = [String]()
    var vehicleTypeArray = [String]()
    var containerImageArray = [String]()
    var fleetImageArray = [String]()
    var ltlImageArray = [String]()
    var vehicleList = [VehicleList]()
    var containerImg = [Image]()
    var fleetImg = [Image]()
    var ltlImg = [Image]()
    var availableLoadBid = [Int]()
    var cellTapped:Bool = false
    var currentRow = 0;
    var checked = Set<IndexPath>()
    var collectionSelectedIndex = IndexPath()
    var selectedIndex = IndexPath()
    var array = [1,2,3,4,5,6,7,8]
    var indexx : Int?
    var vehicle_LoadDetails = [String:Any]()
    var isSubmitQuote = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        collectionView.delegate = self
        collectionView.dataSource = self
        availabelVechCollectionView.delegate = self
        availabelVechCollectionView.dataSource = self
        availableLoadCollectionView.delegate = self
        availableLoadCollectionView.dataSource = self
        submitQuoteBtn.layer.cornerRadius = 5
        inviteFleetBtn.layer.cornerRadius = 5
        slider.labels = ["10","","30"]
        isDragging = false
        
        submitQuoteWrraperView.isHidden = true
        isAvailableLoadBidQuotation = false
        //self.validTimeTF.inputView = self.timePicker
        self.validTimeTF.isUserInteractionEnabled = false
        amountTF.delegate = self
        validDateTF.delegate = self
        cardViewArray(arrView: [fleetInfoView,availableLoadDetails])
        assignVechImage.image = UIImage(named: "assignVech")
        assignVechLabel.textColor = UIColor.init(hexString: "#0F2541")
        slider.setIndex(2, animated: true)
        assignVechLine.isHidden = false
        fleetPostLine.isHidden = true
        loadQuotedLine.isHidden = true
        self.indexx = Int(self.slider.index)
        userDefaults.set(indexx, forKey: "index")
        self.availabelVechCollectionView.isHidden = false
        FTLLabel.textColor = UIColor(hexString: "#6F7179")
        ltlLabel.textColor = UIColor(hexString: "#6F7179")
        containerLabel.textColor = UIColor(hexString: "#0F2541")
        if vehicleName.count != 0{
            self.availabelVechCollectionView.isHidden = true
        }else{
            self.availabelVechCollectionView.isHidden = false
        }
        
        self.availableLoadCollectionView.isHidden = true
        self.postFleetTrailingConstraint.constant = (self.view.frame.width / 2) - (self.addPostFleetBtn.frame.width / 1.5)
        self.postLoadTraillingConstraint.constant = (self.view.frame.width / 2) - (self.postLoadView.frame.width / 1.5)
        self.ltlLine.isHidden = true
        self.ftlLine.isHidden = true
        self.containerLine.isHidden = false
        self.isContainerClicked = true
        self.isLTLClicked = false
        self.isFTLClicked = false
        self.loadType()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.availableLoadBidQuotation(notification:)), name: Notification.Name("AvailableLoadBidQuotation"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.fleet(notification:)), name: Notification.Name("fleet"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.selectedTime(notification:)), name: .selectedTime, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.selectedDate(notification:)), name: .selectedDate, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.pannelCollapsed(notification:)), name: Notification.Name("PannelCollapsed"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.dashboardCollapsed(notification:)), name: Notification.Name("dashboardCollapsed"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.searchLoadCollapsed(notification:)), name: Notification.Name("searchLoadCollapsed"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.searchLoadCollapsed1(notification:)), name: Notification.Name("searchLoadCollapsed1"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSumbitQuoteView(notification:)), name: Notification.Name("sumbitQuoteView"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.inviteFleetClicked(notification:)), name: Notification.Name("inviteFleetClicked"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.confirmSubmitQuote(notification:)), name: .confirmSubmitQuote, object: nil)
        getVehicleList()
        loginUserType = UserDefaults.standard.value(forKey: UserDefaultsContants.LOGIN_USER_TYPE)! as! String
        print("loginUserType == \(String(describing: loginUserType))")
        
    }
    
    var amount = Int()
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == amountTF) {
            let maxLength = 6
            let currentString: NSString = amountTF.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }else{
            return true
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        amount = Int(amountTF.text!)!
        if textField == amountTF{
            if amount <= 0 {
                let alert = UIAlertController(title: "", message: "Please enter valid amount", preferredStyle: UIAlertController.Style.alert)
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    @objc func confirmSubmitQuote(notification: Notification) {
        self.maniVIew.isHidden = false
        submitQuoteWrraperView.isHidden = true
        fleetInviteWraperView.isHidden = true
    }
    @objc func showSumbitQuoteView(notification: Notification) {
        self.maniVIew.isHidden = true
        submitQuoteWrraperView.isHidden = false
        fleetInfoView.isHidden = true
        fleetInviteWraperView.isHidden = true
    }
    
    @objc func inviteFleetClicked(notification: Notification) {
        self.maniVIew.isHidden = true
        submitQuoteWrraperView.isHidden = true
        fleetInviteWraperView.isHidden = false
    }
    
    
    @IBAction func submitQuoteClicked(_ sender: Any) {
        isSubmitQuote = true
        if isAvailableLoadBidQuotation == false && cellSelected == false{
            let alert = UIAlertController(title: "", message: "Please Select fleet to continue", preferredStyle: UIAlertController.Style.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        
        if amountTF.text != "" && validDateTF.text != "" && validTimeTF.text != ""{
            sideMenuController?.setContentViewController(with: "15")
            
            self.vehicle_LoadDetails = ["fleetId": self.fleetId,"vehicleType":self.vehicleType,"vehicleDetailsFrom": self.vehicleDetailsFrom,"vehicleDetailsTo":self.vehicleDetailsTo,"vehicleDetailsLoadType":self.vehicleDetailsLoadType,"vehicleDetailsCargoType":self.vehicleDetailsCargoType,"loadDetailsLoadId":self.loadDetailsLoadId,"loadDetailsLoadType":self.loadDetailsLoadType,"loadDetailsFrom":self.loadDetailsFrom,"loadDetailsTo":self.loadDetailsTo,"loadDetailsCargoWt":self.loadDetailsCargoWt,"loadDetailsNoOfVech":self.loadDetailsNoOfVech,"loadDetailsAvailDate":self.loadDetailsAvailDate,"loadDetailsAvailTime":self.loadDetailsAvailTime,"amount":self.amountTF.text!,"valid": self.validDateTF.text!,"range":self.validTimeTF.text!,"time":self.timeLabel.text!,"closedBidTime": self.closeBidTime,"loadBidId":self.loadBidId,"fleetBidId":self.fleetBidId,"isAvailableLoadBidQuotation":isAvailableLoadBidQuotation,"id":self.id]
            
            
            NotificationCenter.default.post(name: Notification.Name("SubmitQuoteClickedFleetInfo"),object: nil,userInfo: self.vehicle_LoadDetails)
            
        }else{
            return
        }
        
    }
    
    @IBAction func inviteFleetBtn(_ sender: Any) {
        sideMenuController?.setContentViewController(with: "27")
    }
    
    @IBAction func fleetInviteClicked(_ sender: Any) {
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        cellSelected = false
        self.collectionSelectedIndex = IndexPath()
        self.amountTF.text = ""
        self.validDateTF.text = ""
        self.validTimeTF.text = ""
        self.lastBidLabel.text = "NA"
        self.timeLabel.text = ""
        
        
        
        
        //        if isKeyPresentInUserDefaults(key: "selectedTime")
        //        {
        //            validTimeTF.text = (userDefaults.value(forKey: "selectedTime") as! String)
        //        }
        //
        //        if isKeyPresentInUserDefaults(key: "selectedTimeType"){
        //            timeLabel.text = (userDefaults.value(forKey: "selectedTimeType") as! String)
        //        }
        //
        if loginUserType == UserStates.SERVICE_USER{
            searchLabel.text = "Search Fleets"
            assignVechLabel.text = "Make Booking"
            fleetPostLabel.text = "Loads Posted"
            loadsQuotedLabel.text = "Fleets Invited"
            
        }else if loginUserType == UserStates.SERVICE_PROVIDER{
            searchLabel.text = "Search Loads"
            assignVechLabel.text = "Assign Vehicle"
            fleetPostLabel.text = "Fleets Posted"
            loadsQuotedLabel.text = "Loads Quoted"
        }
    }
    
    var loadTypee = [LoadType]()
    
    func loadType(){
        
        Alamofire.request(URLStatics.loadType,method: .get, parameters: nil).responseJSON { response in
            print("response.request=\(String(describing: response.request))")  // original URL request
            print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
            print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
            switch (response.result) {
            case .success:
                if let reply = response.result.value {
                    let mainResponse = JSON(reply)
                    print(" loadType JSON: \(mainResponse)")
                    var returnDict = [[String:Any]]()
                    if let returnObj = mainResponse.arrayObject as? [[String:Any]] {
                        
                        returnDict = returnObj
                    }
                    
                    for data in returnDict{
                        print(" code == \(String(describing: data["code"]!))")
                        print("id == \(String(describing: data["id"]!))")
                        print("loadType == \(String(describing: data["loadType"]!))")
                        let load = LoadType(id: data["id"] as! Int, code: data["code"]! as! String, loadType: data["loadType"]! as! String)
                        self.loadTypee.append(load)
                    }
                }
                break
                
            case .failure(let error):
                
                print("POST REQUEST ERROR  - \(error.localizedDescription)")
                print("=== REQUEST INFORMATION ===")
                break
            }
        }
        
    }
    
    @IBAction func addPostFleetClicked(_ sender: Any) {
        
        UIView.animate(withDuration: 0.6) {
            self.postFleetTrailingConstraint.constant = 10
        }
        self.availabelVechCollectionView.reloadData()
        self.availabelVechCollectionView.layoutIfNeeded()
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "PostFleetVC") }, with: "fourth")
        sideMenuController?.setContentViewController(with: "fourth")
    }
    
    @IBAction func addPostLoadClicked(_ sender: Any) {
        UIView.animate(withDuration: 0.6) {
            self.postLoadTraillingConstraint.constant = 10
        }
        self.availableLoadCollectionView.isHidden = false
        self.availableLoadCollectionView.reloadData()
        self.availableLoadCollectionView.layoutIfNeeded()
    }
    
    @IBAction func sliderValueChanged(_ sender: Any) {
        indexx = Int(slider.index)
        switch indexx! {
        case 0:
            userDefaults.set(10, forKey: "index")
        case 1:
            userDefaults.set(20, forKey: "index")
        case 2:
            userDefaults.set(30, forKey: "index")
        default:
            userDefaults.set(30, forKey: "index")
        }
        NotificationCenter.default.post(name: Notification.Name("sliderValueChanged"), object: nil)
    }
    func saveVehicleImage(){
        //        containerImg.removeAll()
        //        fleetImg.removeAll()
        //        ltlImg.removeAll()
        DispatchQueue.main.async {
            
            if self.containerImageArray.isEmpty == false{
                for img in self.containerImageArray{
                    //  print("containerImageArray = \(img)")
                    if img == ""{
                        self.containerImg.append(UIImage.init(named: "search")!)
                    }else{
                        Alamofire.request(URLStatics.vehicleImage+"\(img)").responseImage { response in
                            //      print("response === \(String(describing: response.data))")
                            if let imagee = response.result.value {
                                self.containerImg.append(imagee)
                                //     print("containerImg.count ======== \(self.containerImg.count)")
                            }
                        }
                    }
                }
                self.collectionView.reloadData()
            }
            if self.fleetImageArray.isEmpty == false{
                
                for img in self.fleetImageArray{
                    //  print("fleetImageArray = \(img)")
                    if img == ""{
                        self.fleetImg.append(UIImage.init(named: "search")!)
                    }else{
                        Alamofire.request(URLStatics.vehicleImage+"\(img)").responseImage { response in
                            //  print("response === \(String(describing: response.data))")
                            if let imagee = response.result.value {
                                self.fleetImg.append(imagee)
                                //print("fleetImg.count ======== \(self.fleetImg.count)")
                            }
                        }
                    }
                    
                }
                self.collectionView.reloadData()
            }
            if self.ltlImageArray.isEmpty == false{
                
                for img in self.ltlImageArray{
                    //    print("ltlImageArray = \(img)")
                    if img == ""{
                        self.ltlImg.append(UIImage.init(named: "search")!)
                    }else{
                        Alamofire.request(URLStatics.vehicleImage+"\(img)").responseImage { response in
                            //     print("response === \(String(describing: response.data))")
                            if let imagee = response.result.value {
                                self.ltlImg.append(imagee)
                                //   print("ltlImg.count ======== \(self.ltlImg.count)")
                            }
                        }
                    }
                }
                self.collectionView.reloadData()
            }
        }
    }
    
    
    @IBAction func validTimeTapped(_ sender: Any) {
        
        self.picker(state:"TimePicker")
        
        
    }
    
    
    @IBAction func validDateTapped(_ sender: Any) {
        self.picker(state:"DatePicker")
        
    }
    
    @IBAction func ampmTapped(_ sender: Any) {
        //  self.picker(state:"AMPMPicker")
    }
    
    
    var pageNo = 0
    
    
    func fleetPostedDashboard() {
        //    if isDragging == false{
        //      self.activityIndicatorEnd()
        //    }
        var bidlist : GetBidList?
        if isDragging == false{
            NotificationCenter.default.post(name: .dashboardActivityIndicator,object: nil,userInfo: ["active" : true])
        }
        bidlist = GetBidList(pageSize:"10",pageNo:"\(self.pageNo)",recordType:"",search:"",fromCity:"",fromState:"",toState:"",toCity:"",vehicleType:"",loadType:"",noOfVehicle:"",fromDate:"",toDate:"",currentLatitude:"",currentLongitude:"",km: "",financialYear: self.currentFinancialYear!,cargoType: "")
        
        
        
        
        let jsonDataa = try! JSONEncoder().encode(bidlist)
        let type = String(data: jsonDataa, encoding: .utf8)!
        do {
            self.getBidListPara = try self.convertToDictionary(from: type)
            print("listt = \(self.getBidListPara)")
        } catch {
            print(error)
        }
        
        let parameters : [String:Any] = getBidListPara
        
        let headers = header()
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
        
        Alamofire.request(URLStatics.fleetPosted + "OPEN" , method: .get, parameters: parameters,headers: headers)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                
                
                switch (response.result) {
                    
                case .success:
                    if self.isDragging == false{
                        NotificationCenter.default.post(name: .dashboardActivityIndicator,object: nil,userInfo: ["active" : false])
                    }
                    
                    if let reply = response.result.value {
                        print("======= JSON: Fleet Posted ========  \(reply)")
                        let mainResponse = JSON(reply)
                        
                        
                        var returnDict = [[String:Any]]()
                        // var datumArray = [FleetDatum]()
                        var datum : FleetDatum?
                        var vehicleType = [String:Any]()
                        var smallDocumentType = [String:Any]()
                        var documentType = [String:Any]()
                        var document : FleetLogo?
                        var smallDocument : FleetLogo?
                        var cargoType : FleetCargoType?
                        var cargo = [String:Any]()
                        var capacityWt = [String:Any]()
                        var capacityWtUnit : FleetCapacityWeightUnit?
                        var loadType = [String:Any]()
                        var fleetLoadType : FleetLoadType?
                        var currencyMaster = [String:Any]()
                        var fleetCurrencyMaster : FleetCurrencyMaster
                        
                        var fleetVehicleType : FleetVehicleType?
                        if let returnObj = mainResponse["data"].arrayObject as? [[String:Any]] {
                            returnDict = returnObj
                        }
                        
                        for data in returnDict{
                            vehicleType = data["vehicleType"] as! [String:Any]
                            smallDocumentType = vehicleType["documentSmall"] as! [String:Any]
                            documentType = vehicleType["document"] as! [String:Any]
                            cargo = data["cargoType"] as! [String:Any]
                            capacityWt = data["capacityWeightUnit"] as! [String:Any]
                            loadType = data["loadType"] as! [String:Any]
                            currencyMaster = data["currencyMaster"] as! [String:Any]
                            
                            document = FleetLogo(id: documentType["id"] as! Int, mimeType: documentType["mimeType"] as! String, type: documentType["type"] as! String)
                            
                            smallDocument = FleetLogo(id: smallDocumentType["id"] as! Int, mimeType: smallDocumentType["mimeType"] as! String, type: smallDocumentType["type"] as! String)
                            
                            fleetVehicleType = FleetVehicleType(id: Int(truncating: vehicleType["id"] as! NSNumber), name: vehicleType["name"] as! String, sequence: Int(truncating: vehicleType["sequence"] as! NSNumber), container: vehicleType["container"] as! Bool, type: vehicleType["type"] as! String, document: document!, documentSmall: smallDocument!, vehicleOrder: Int(truncating: vehicleType["vehicleOrder"] as! NSNumber), documentID: Int(truncating: vehicleType["documentId"] as! NSNumber), smallDocID: Int(truncating: vehicleType["smallDocId"] as! NSNumber))
                            
                            fleetLoadType = FleetLoadType(id: Int(truncating: loadType["id"] as! NSNumber), loadType: loadType["loadType"] as! String, code: loadType["code"] as! String)
                            
                            cargoType = FleetCargoType(id: cargo["id"] as! Int, cargoType: cargo["cargoType"] as! String)
                            
                            capacityWtUnit = FleetCapacityWeightUnit(id: capacityWt["id"] as! Int, name: capacityWt["name"] as! String, type: capacityWt["type"] as! String)
                            
                            fleetCurrencyMaster = FleetCurrencyMaster(id: currencyMaster["id"] as! Int, currencyName: currencyMaster["currencyName"] as! String, currencyCountry: currencyMaster["currencyCountry"] as! String, currencyCode: currencyMaster["currencyCode"] as! String)
                            
                            datum = FleetDatum(id: data["id"] as! Int,
                                               bidCode: data["bidCode"] as! String,
                                               creationDate: data["creationDate"] as! Int,
                                               closedBidTime: data["closedBidTime"] as! Int,
                                               availableLocation: data["availableLocation"] as! String,
                                               destinationLocation: data["destinationLocation"] as? String,
                                               availableLatitude: data["availableLatitude"] as? Double,
                                               availableLongitude: data["availableLongitude"] as? Double,
                                               destinationLatitude: data["destinationLatitude"] as? Double,
                                               destinationLongitude: data["destinationLongitude"] as? Double,
                                               count: data["count"] as? Int,
                                               vehicleType: fleetVehicleType!,
                                               noOfVehicles: data["noOfVehicles"] as! Int,
                                               availableDateTime: data["availableDateTime"] as! Int,
                                               cargoType: cargoType!,
                                               capacityWeight: data["capacityWeight"] as! Int,
                                               capacityWeightUnit: capacityWtUnit!,
                                               loadType: fleetLoadType!,
                                               expectedFreight: data["expectedFreight"] as? NSNumber,
                                               currencyMaster: fleetCurrencyMaster,
                                               isInvited: data["isInvited"] as! Bool,
                                               bidInviteCount: data["bidInviteCount"] as! Int,
                                               vehicleTypeMaster: data["vehicleTypeMaster"] as? String,
                                               loadBidCode: data["loadBidCode"] as? String,
                                               registrationNo: data["registrationNo"] as? String,
                                               comments: data["comments"] as? String, sequence: data["sequence"] as? Int, postType: data["postType"] as? String)
                            
                            if self.datumArray.contains(where: { bid in bid.bidCode == datum?.bidCode }) {
                                print(" exists in the array")
                            } else {
                                print(" does not exists in the array")
                                self.datumArray.append(datum!)
                            }
                            
                            
                        }
                        print( "datumArray.count ====== \(self.datumArray.count)")
                        self.tableView.reloadData()
                    }
                    break
                case .failure(let error):
                    
                    print("error \(error.localizedDescription)")
                    break
                }
        }
        
    }
    
    
    
    
    
    
    
    func getImages(id:String) -> Image {
        var image = Image()
        
        Alamofire.request(URLStatics.vehicleImage+"\(id)").responseImage { response in
            debugPrint(response)
            print("=========================")
            print(response.request)
            print(response.response)
            debugPrint(response.result)
            if let imagee = response.result.value {
                print("image downloaded: \(image)")
                image = imagee
                
            }
        }
        return image
        //        Alamofire.request(URLStatics.vehicleImage+"\(935)",method: .get, parameters: nil).responseJSON { response in
        //            print("response.request=\(String(describing: response.request))")  // original URL request
        //            print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
        //            print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
        //            switch (response.result) {
        //            case .success:
        //                    if let reply = response.result.value {
        //                    let mainResponse = JSON(reply)
        //                    print(" vehicle list JSON: \(mainResponse)")
        //                    var returnDict = [[String:Any]]()
        ////                    if let returnObj = mainResponse.arrayObject as? [[String:Any]] {
        ////                        returnDict = returnObj
        ////                    }
        ////
        ////                    for data in returnDict{
        ////
        ////                }
        //                }
        //                break
        //            case .failure(let error):
        //
        //                print("POST REQUEST ERROR  - \(error.localizedDescription)")
        //                print("=== REQUEST INFORMATION ===")
        //                break
        //            }
        //        }
    }
    
    func getVehicleList(){
        //+918669696004
        //asdfgf1234$
        Alamofire.request(URLStatics.vehicleList,method: .get, parameters: nil).responseJSON { response in
            //  print("response.request=\(String(describing: response.request))")  // original URL request
            //  print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
            //    print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
            switch (response.result) {
            case .success:
                
                if let reply = response.result.value {
                    let mainResponse = JSON(reply)
                    //    print(" vehicle list JSON: \(mainResponse)")
                    
                    var returnDict = [[String:Any]]()
                    
                    if let returnObj = mainResponse.arrayObject as? [[String:Any]] {
                        returnDict = returnObj
                    }
                    
                    for data in returnDict{
                        var vehicleOrder = ""
                        var documentId = ""
                        var id = ""
                        var sequence = ""
                        var smallDocId = ""
                        if let vehicleOrderr = data["vehicleOrder"] as? Int {
                            vehicleOrder = "\(vehicleOrderr)"
                        }else{
                            vehicleOrder = ""
                        }
                        if let documentIdd = data["documentId"] as? Int {
                            documentId = "\(documentIdd)"
                        }else{
                            documentId = ""
                        }
                        if let idd = data["id"] as? Int {
                            id = "\(idd)"
                        }else{
                            id = ""
                        }
                        if let sequencee = data["sequence"] as? Int {
                            sequence = "\(sequencee)"
                        }else{
                            sequence = ""
                        }
                        if let smallDocIdd = data["smallDocId"] as? Int {
                            smallDocId = "\(smallDocIdd)"
                        }else{
                            smallDocId = ""
                        }
                        
                        
                        let vechicleListt = VehicleList(name: data["name"] as! String, vehicleOrder: vehicleOrder, smallDocId: smallDocId, documentId:documentId, id: id, vehicleType: data["vehicleType"] as! String, container: String(data["container"] as! Bool), type: data["type"] as! String, sequence: sequence)
                        self.vehicleList.append(vechicleListt)
                    }
                    //                    for v in self.vehicleList{
                    //                        print(" smallDocId = \(v.smallDocId)")
                    //                    }
                    //                    self.ContainerVehicleNameArray.removeAll()
                    //                    self.FleetVehicleNameArray.removeAll()
                    //                    self.LTLVehicleNameArray.removeAll()
                    for v in self.vehicleList{
                        if v.vehicleType == "FLEET" || v.vehicleType == "CAR/BIKE CARRIER" {
                            self.FleetVehicleNameArray.append(v.name)
                            self.fleetImageArray.append(v.smallDocId)
                        }
                        if v.vehicleType == "CONTAINER"{
                            self.ContainerVehicleNameArray.append(v.name)
                            self.containerImageArray.append(v.smallDocId)
                        }
                        if v.name == "PICKUP" || v.name == "407" || v.name == "709" || v.name == "1109" {
                            self.LTLVehicleNameArray.append(v.name)
                            self.ltlImageArray.append(v.smallDocId)
                        }
                    }
                    DispatchQueue.main.async {
                        self.saveVehicleImage()
                    }
                    
                }
                break
            case .failure(let error):
                
                print("POST REQUEST ERROR  - \(error.localizedDescription)")
                print("=== REQUEST INFORMATION ===")
                break
            }
        }
    }
    
    //DBDFE3
    
    
    func collapse(){
        self.searchLoadView.isHidden = false
        self.searchloadView1.isHidden = false
        self.searchLoadMainView.isHidden = false
        self.seperatorLineView.isHidden = false
        self.dashboardView.isHidden = false
        self.dashboardView1.isHidden = false
        self.tableView.isHidden = false
        self.tableViewWrapper.isHidden = false
        if isSubmitQuote == false{
            self.submitQuoteWrraperView.isHidden = true
            self.maniVIew.isHidden = false
            fleetInviteWraperView.isHidden = true
        }
        
        self.datumArray.removeAll()
        self.fleet = nil
        self.vehicleName.removeAll()
    }
    
    var vehicleName = [String]()
    var vehicleImageArr = [String]()
    var loadDetailsLoadId = String()
    var closeBidTime = Int()
    var loadDetailsLoadType = String()
    var loadDetailsFrom = String()
    var loadDetailsTo = String()
    var loadDetailsCargoWt = String()
    var loadDetailsNoOfVech = String()
    var loadDetailsAvailDate = String()
    var loadDetailsAvailTime = String()
    var fleetId = String()
    var vehicleType = String()
    var vehicleDetailsFrom = String()
    var vehicleDetailsTo = String()
    var vehicleDetailsCargoType = String()
    var vehicleDetailsLoadType = String()
    var loadBidId = Int()
    var fleetBidId = Int()
    var id = Int()
    
    @objc func selectedTime(notification: Notification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        self.validTimeTF.text = (userInfo!["time"] as! String)
        self.timeLabel.text = (userInfo!["timeType"] as! String)
        
    }
    
    @objc func selectedDate(notification: Notification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        
        self.validDateTF.text = (userInfo!["date"] as! String)
    }
    
    
    func epochTime1(epochTimee:Double)->String{
        
        let timeResult:Double = epochTimee
        let date = NSDate(timeIntervalSince1970: timeResult)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm" //Set time style
        
        let timeZone = TimeZone.current.identifier as String
        dateFormatter.timeZone = TimeZone(identifier: timeZone)
        let localDate = dateFormatter.string(from: date as Date)
        return "\(localDate)"
        
    }
    func epochDate(epochdate:Double)->String{
        
        let timeResult:Double = epochdate
        let date = NSDate(timeIntervalSince1970: timeResult)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy" //Set time style
        
        let timeZone = TimeZone.current.identifier as String
        dateFormatter.timeZone = TimeZone(identifier: timeZone)
        let localDate = dateFormatter.string(from: date as Date)
        return "\(localDate)"
        
    }
    
    func epochTimeType(epochtimeType:Double)->String{
        
        let timeResult:Double = epochtimeType
        let date = NSDate(timeIntervalSince1970: timeResult)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "a" //Set time style
        
        let timeZone = TimeZone.current.identifier as String
        dateFormatter.timeZone = TimeZone(identifier: timeZone)
        let localDate = dateFormatter.string(from: date as Date)
        return "\(localDate)"
        
    }
    
    var cellSelected = Bool()
    var isAvailableLoadBidQuotation = Bool()
    var fleet : FleetDatum?
    
    @objc func fleet(notification: Notification) {
      
        
    }
    var status = String()
    @objc func availableLoadBidQuotation(notification: Notification) {
        
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        
        let loadObj = userInfo!["loadObj"] as! [String:Any]
        print("======== > loadObj[bidCode] ======== >  \(loadObj["bidCode"]!)")
        print("======== > loadObj[vehicleType] ======== >  \(loadObj["vehicleType"]!)")
        let vehicleTypeId = loadObj["vehicleTypeId"] as! Int
        let loadTypeId = loadObj["loadTypeId"] as! Int
        let availableBidQuotation = userInfo!["availableLoadBidQuotaionElement"] as! AvailableLoadBidQuotaionElement?
        let statuss = userInfo!["status"] as! String
        self.status = statuss
        let lat = loadObj["latitude"] as! Double
        let lng = loadObj["longitude"] as! Double
        loadBidId = userInfo!["loadBidId"] as! Int
        loadDetailsLoadId = loadObj["bidCode"]! as! String
        loadDetailsLoadType = loadObj["loadType"]! as! String
        loadDetailsFrom = loadObj["from"]! as! String
        loadDetailsTo = loadObj["to"]! as! String
        loadDetailsCargoWt = userInfo!["cargoWt"]! as! String
        loadDetailsNoOfVech = String(loadObj["noOfVech"]! as! Int)
        loadDetailsAvailDate = loadObj["availableFromdate"]! as! String
        loadDetailsAvailTime = loadObj["availableFromtime"]! as! String
        self.closeBidTime =
            userInfo!["closeBidTime"] as! Int
        
        print("availableBidQuotation ==== \(String(describing: availableBidQuotation?.id))")
        print("vehicleTypeId ==== \(vehicleTypeId)")
        self.vehicleName.removeAll()
        self.vehicleImageArr.removeAll()
        if availableBidQuotation?.id != nil {
            
            //                var image = getImageFromId(id: availableBidQuotation?.availableFleetBid.vehicleType.documentSmall.id!)
            print("availableLoadBidQuotaionElement +++++ \(String(describing: (availableBidQuotation?.availableFleetBid.vehicleType.name!)))");
            
            self.amountTF.text = String((availableBidQuotation?.quotationAmount!)!) + ".00"
            
            self.lastBidLabel.text = String((availableBidQuotation?.quotationAmount!)!) + ".00"
            
            let validDateTime = (availableBidQuotation?.validDateTime!)! / 1000
            print("validDateTime ====== >>>>>>> ====== \(validDateTime)")
            self.validDateTF.text = epochDate(epochdate: Double(validDateTime))
            self.validTimeTF.text = epochTime1(epochTimee: Double(validDateTime))
            print("epochTimeType ====== >>>>>>> ====== \(epochTimeType(epochtimeType: Double(validDateTime)))")
            self.timeLabel.text = epochTimeType(epochtimeType: Double(validDateTime))
            self.vehicleName.append((availableBidQuotation?.availableFleetBid.bidCode)!)
            self.vehicleImageArr.append("\((availableBidQuotation?.availableFleetBid.vehicleType.documentSmall?.id)!)")
            self.id = (availableBidQuotation?.id!)!
            self.fleetId = (availableBidQuotation?.availableFleetBid.bidCode!)!
            self.vehicleType = (availableBidQuotation?.availableFleetBid.vehicleType.name!)!
            self.vehicleDetailsFrom = (availableBidQuotation?.availableFleetBid.availableLocation!)!
            self.vehicleDetailsTo = (availableBidQuotation?.availableFleetBid.destinationLocation!)!
            self.vehicleDetailsLoadType = (availableBidQuotation?.availableFleetBid.loadType.loadType!)!
            self.vehicleDetailsCargoType = (availableBidQuotation?.availableFleetBid.cargoType.cargoType)!
            self.fleetBidId = (availableBidQuotation?.availableFleetBid.id!)!
            
            
            print("fleetBidId ====>>>>> \(self.fleetBidId)")
            
            isAvailableLoadBidQuotation = true

            self.availabelVechCollectionView.reloadData()
            
        }else if status == "MatchLoad" && availableBidQuotation?.id == nil{
            
            self.addPostFleetBtn.isHidden = true
            self.availabelVechCollectionView.isHidden = false
            cellSelected = true
            
            
          
            let fleeet = userInfo!["fleetDetail"] as? FleetDatum
            self.fleet = fleeet
           
            self.availabelVechCollectionView.reloadData()
            self.availabelVechCollectionView.setNeedsLayout()
            self.availabelVechCollectionView.layoutIfNeeded()
            self.vehicleName.append(fleet!.bidCode)
            self.vehicleImageArr.append("\(String(describing: fleet?.vehicleType.documentSmall.id))")
                self.id = (fleet?.id)!
                self.fleetId = (fleet!.bidCode)
            self.vehicleType = (fleet?.vehicleType.name)!
            self.vehicleDetailsFrom = (fleet?.availableLocation)!
                self.vehicleDetailsTo = (fleet?.destinationLocation)!
                self.vehicleDetailsLoadType = (fleet?.loadType.loadType)!
                self.vehicleDetailsCargoType = (fleet?.cargoType.cargoType)!
                self.fleetBidId = (fleet?.id)!
                

               isAvailableLoadBidQuotation = false
            self.availabelVechCollectionView.reloadData()
        }
        else{
            print("availableLoadBidQuotaionElement +++++ \(String(describing: availableBidQuotation?.id))")
            findavailablefleetforbidlist(lat: lat, lng: lng, vehicleTypeId: vehicleTypeId, loadTypeId: loadTypeId)
            isAvailableLoadBidQuotation = false
            
        }
        
        
        
        UIView.animate(withDuration: 0.6) {
            self.postFleetTrailingConstraint.constant = 10
        }
        
        if  availableBidQuotation?.id != nil {
            self.addPostFleetBtn.isHidden = true
            self.availabelVechCollectionView.isHidden = false
            cellSelected = true
            self.availabelVechCollectionView.reloadData()
            self.availabelVechCollectionView.layoutIfNeeded()
            isAvailableLoadBidQuotation = true
        }else if status == "MatchLoad" && availableBidQuotation?.id == nil{
            self.addPostFleetBtn.isHidden = true
            self.availabelVechCollectionView.isHidden = false
            cellSelected = true
            self.availabelVechCollectionView.reloadData()
            self.availabelVechCollectionView.layoutIfNeeded()
            isAvailableLoadBidQuotation = false
        }else{
            self.addPostFleetBtn.isHidden = false
            self.availabelVechCollectionView.isHidden = false
            self.availabelVechCollectionView.reloadData()
            self.availabelVechCollectionView.layoutIfNeeded()
            cellSelected = false
            isAvailableLoadBidQuotation = false
        }
        
        
    }
    
    
    //    func getImageFromId(id : Int) -> Image{
    //
    //        Alamofire.request(URLStatics.vehicleImage+"\(id)").responseImage { response in
    //            //     print("response === \(String(describing: response.data))")
    //            if let imagee = response.result.value {
    //
    //            }
    //            return imagee
    //        }
    //    }
    //
    var datumArray = [FleetDatum]()
    
    var getBidListPara = [String:Any]()
    
    func findavailablefleetforbidlist(lat:Double,lng:Double,vehicleTypeId:Int,loadTypeId:Int){
        let indexx = userDefaults.value(forKey: "index") as! Int
        self.datumArray.removeAll()
        let bidlist = GetBidList(pageSize:"40",pageNo:"0",recordType:"",search:"",fromCity:"",fromState:"",toState:"",toCity:"",vehicleType:String(vehicleTypeId),loadType:String(loadTypeId),noOfVehicle:"",fromDate:"",toDate:"",currentLatitude:String(lat),currentLongitude:String(lng),km: String(indexx),financialYear: self.currentFinancialYear!,cargoType: "")
        let jsonDataa = try! JSONEncoder().encode(bidlist)
        let type = String(data: jsonDataa, encoding: .utf8)!
        do {
            self.getBidListPara = try self.convertToDictionary(from: type)
            print("listt = \(self.getBidListPara)")
        } catch {
            print(error)
        }
        
        let parameters : [String:Any] = getBidListPara
        
        //+918669696004
        //asdfgf1234$
        //8888870909
        //asdfgf1234$
        let headers = header()
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
        
        Alamofire.request(URLStatics.findavailablefleetforbidlist, method: .get, parameters: parameters,headers: headers)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                //                let resultNSString = NSString(data: response.request!.httpBody!, encoding: String.Encoding.utf8.rawValue)!
                //                print("response.request?.httpBody = \(resultNSString)")
                switch (response.result) {
                case .success:
                    
                    if let reply = response.result.value {
                        print("======= JSON: findavailablefleetforbidlist ========  \(reply)")
                        let mainResponse = JSON(reply)
                        if mainResponse["status"].boolValue == true{
                            self.addPostFleetBtn.isHidden = false
                            UIView.animate(withDuration: 0.6) {
                                self.postFleetTrailingConstraint.constant = 10
                            }
                            self.availabelVechCollectionView.isHidden = false
                            self.availabelVechCollectionView.reloadData()
                            self.availabelVechCollectionView.layoutIfNeeded()
                        }else{
                            self.addPostFleetBtn.isHidden = false
                            self.postFleetTrailingConstraint.constant = (self.view.frame.width / 2) - (self.addPostFleetBtn.frame.width / 1.5)
                            self.postLoadTraillingConstraint.constant = (self.view.frame.width / 2) - (self.postLoadView.frame.width / 1.5)
                        }
                        var returnDict = [[String:Any]]()
                        // var datumArray = [FleetDatum]()
                        var datum : FleetDatum?
                        var vehicleType = [String:Any]()
                        var smallDocumentType = [String:Any]()
                        var documentType = [String:Any]()
                        var document : FleetLogo?
                        var smallDocument : FleetLogo?
                        var cargoType : FleetCargoType?
                        var cargo = [String:Any]()
                        var capacityWt = [String:Any]()
                        var capacityWtUnit : FleetCapacityWeightUnit?
                        var loadType = [String:Any]()
                        var fleetLoadType : FleetLoadType?
                        var currencyMaster = [String:Any]()
                        var fleetCurrencyMaster : FleetCurrencyMaster
                        
                        var fleetVehicleType : FleetVehicleType?
                        if let returnObj = mainResponse["data"].arrayObject as? [[String:Any]] {
                            returnDict = returnObj
                        }
                        for data in returnDict{
                            vehicleType = data["vehicleType"] as! [String:Any]
                            smallDocumentType = vehicleType["documentSmall"] as! [String:Any]
                            documentType = vehicleType["document"] as! [String:Any]
                            cargo = data["cargoType"] as! [String:Any]
                            capacityWt = data["capacityWeightUnit"] as! [String:Any]
                            loadType = data["loadType"] as! [String:Any]
                            currencyMaster = data["currencyMaster"] as! [String:Any]
                            
                            document = FleetLogo(id: documentType["id"] as! Int, mimeType: documentType["mimeType"] as! String, type: documentType["type"] as! String)
                            
                            smallDocument = FleetLogo(id: smallDocumentType["id"] as! Int, mimeType: smallDocumentType["mimeType"] as! String, type: smallDocumentType["type"] as! String)
                            
                            fleetVehicleType = FleetVehicleType(id: vehicleType["id"] as! Int, name: vehicleType["name"] as! String, sequence: vehicleType["sequence"] as! Int, container: vehicleType["container"] as! Bool, type: vehicleType["type"] as! String, document: document!, documentSmall: smallDocument!, vehicleOrder: vehicleType["vehicleOrder"] as! Int, documentID: vehicleType["documentId"] as! Int, smallDocID: vehicleType["smallDocId"] as! Int)
                            
                            fleetLoadType = FleetLoadType(id: loadType["id"] as! Int, loadType: loadType["loadType"] as! String, code: loadType["code"] as! String)
                            
                            cargoType = FleetCargoType(id: cargo["id"] as! Int, cargoType: cargo["cargoType"] as! String)
                            
                            capacityWtUnit = FleetCapacityWeightUnit(id: capacityWt["id"] as! Int, name: capacityWt["name"] as! String, type: capacityWt["type"] as! String)
                            
                            fleetCurrencyMaster = FleetCurrencyMaster(id: currencyMaster["id"] as! Int, currencyName: currencyMaster["currencyName"] as! String, currencyCountry: currencyMaster["currencyCountry"] as! String, currencyCode: currencyMaster["currencyCode"] as! String)
                            
                            datum = FleetDatum(id: data["id"] as! Int,
                                               bidCode: data["bidCode"] as! String,
                                               creationDate: data["creationDate"] as! Int,
                                               closedBidTime: data["closedBidTime"] as! Int,
                                               availableLocation: data["availableLocation"] as! String,
                                               destinationLocation: data["destinationLocation"] as? String,
                                               availableLatitude: data["availableLatitude"] as? Double,
                                               availableLongitude: data["availableLongitude"] as? Double,
                                               destinationLatitude: data["destinationLatitude"] as? Double,
                                               destinationLongitude: data["destinationLongitude"] as? Double,
                                               count: data["count"] as? Int,
                                               vehicleType: fleetVehicleType!,
                                               noOfVehicles: data["noOfVehicles"] as! Int,
                                               availableDateTime: data["availableDateTime"] as! Int,
                                               cargoType: cargoType!,
                                               capacityWeight: data["capacityWeight"] as! Int,
                                               capacityWeightUnit: capacityWtUnit!,
                                               loadType: fleetLoadType!,
                                               expectedFreight:  data["expectedFreight"] as? NSNumber,
                                               currencyMaster: fleetCurrencyMaster,
                                               isInvited: data["isInvited"] as! Bool,
                                               bidInviteCount: data["bidInviteCount"] as! Int,
                                               vehicleTypeMaster: data["vehicleTypeMaster"] as? String,
                                               loadBidCode: data["loadBidCode"] as? String, registrationNo: "", comments: "", sequence: 0, postType: "")
                            self.datumArray.append(datum!)
                            
                        }
                        print( "datumArray.count ====== \(self.datumArray.count)")
                        self.availabelVechCollectionView.reloadData()
                    }
                    break
                case .failure(let error):
                    
                    print("error \(error.localizedDescription)")
                    break
                }
        }
        
    }
    
    
    
    
    @objc func pannelCollapsed(notification: Notification) {
        self.collapse()
    }
    @objc func dashboardCollapsed(notification: Notification) {
        self.collapse()
    }
    @objc func searchLoadCollapsed(notification: Notification) {
        self.collapse()
    }
    @objc func searchLoadCollapsed1(notification: Notification) {
        
        self.dashboardView.isHidden = true
        self.dashboardView1.isHidden = true
        self.tableView.isHidden = true
        self.tableViewWrapper.isHidden = true
        self.seperatorLineView.isHidden = true
        NotificationCenter.default.post(name: .SearchLoad, object: nil)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == availableLoadCollectionView{
            return self.availableLoadBid.count
        }
        if collectionView == availabelVechCollectionView{
            if vehicleName.count != 0 {
                return vehicleName.count
            }else if self.fleet != nil{
                return 1
                
            }else{
                return self.datumArray.count
            }
        }
        if isContainerClicked == true{
            return ContainerVehicleNameArray.count
        }else if isFTLClicked{
            return FleetVehicleNameArray.count
        }else{
            return LTLVehicleNameArray.count
        }
    }
    override func viewDidLayoutSubviews() {
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == availableLoadCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "availableLoad", for: indexPath) as! AvailableLoadsCell
            
            if selectedIndex == indexPath
            {
                cell.circularView.backgroundColor = UIColor.init(hexString: ColorConstants.RED)
                
            }
            else
            {
                cell.circularView.backgroundColor = UIColor.clear
            }
            return cell
        }else if collectionView == availabelVechCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "availableVehicle", for: indexPath) as! AvailableVehicleCell
            cell.circularView.backgroundColor = UIColor.clear
            //            if self.availableLoadBid.count > 0 {
            //                Alamofire.request(URLStatics.vehicleImage+"\(self.availableLoadBid[indexPath.row])").responseData { (response) in
            //                    if response.error == nil {
            //                        print(response.result)
            //
            //                        // Show the downloaded image:
            //                        if let data = response.data {
            //                            cell.vehicleImage.image = UIImage(data: data)
            //                        }
            //                    }
            //                }
            //            }
            print( "datumArray.count ====== \(datumArray.count)")
            
            if vehicleName.count > 0  {
                if self.vehicleImageArr.count > 0 {
                    Alamofire.request(URLStatics.vehicleImage + "\(self.vehicleImageArr[indexPath.item])" ).responseImage { response in
                        debugPrint(response)
                        
                        print(response.request!)
                        print(response.response!)
                        debugPrint(response.result)
                        if let image = response.result.value {
                            print("image downloaded: \(image)")
                            cell.vehicleImage.image = image
                        }
                    }
                }else{
                        cell.vehicleImage.image = UIImage(named: "truck1")
                }
              
            
                cell.vehicleLabel.text = vehicleName[indexPath.item]
                cell.circularView.backgroundColor = UIColor.init(hexString: ColorConstants.RED)
                cell.circularView.isUserInteractionEnabled = false
            }
            else if self.fleet != nil{
                
                Alamofire.request(URLStatics.vehicleImage + "\(fleet!.vehicleType.documentSmall.id)" ).responseImage { response in
                    debugPrint(response)
                    
                    print(response.request!)
                    print(response.response!)
                    debugPrint(response.result)
                    if let image = response.result.value {
                        print("image downloaded: \(image)")
                        cell.vehicleImage.image = image
                    }
                }
                cell.vehicleLabel.text = fleet!.bidCode
                cell.circularView.backgroundColor = UIColor.init(hexString: ColorConstants.RED)
                cell.circularView.isUserInteractionEnabled = false
            }else{
                if datumArray.count != 0 {
                    if indexPath.item <= datumArray.count{
                        // let datumItem = datumArray[indexPath.item]
                        cell.circularView.isUserInteractionEnabled = true
                        print( "vehicleLabel ====== \(datumArray[indexPath.item].vehicleType.name)")
                        cell.vehicleLabel.text = datumArray[indexPath.item].bidCode
                        print( "documentSmall.id ======= \(datumArray[indexPath.item].vehicleType.documentSmall.id)")
                        
                        Alamofire.request(URLStatics.vehicleImage + "\(datumArray[indexPath.item].vehicleType.documentSmall.id)" ).responseImage { response in
                            debugPrint(response)
                            
                            print(response.request!)
                            print(response.response!)
                            debugPrint(response.result)
                            if let image = response.result.value {
                                print("image downloaded: \(image)")
                                cell.vehicleImage.image = image
                            }
                        }
                    }
                }
                cell.circularView.isUserInteractionEnabled = true
            }
            
            
            if collectionSelectedIndex == indexPath
            {
                cell.circularView.backgroundColor = UIColor.init(hexString: ColorConstants.RED)
                self.cellSelected = true
            }else
            {
                cell.circularView.backgroundColor = UIColor.clear
                self.cellSelected = false
            }
            if self.cellSelected == true || isAvailableLoadBidQuotation{
                cell.circularView.backgroundColor = UIColor.init(hexString: ColorConstants.RED)
                self.cellSelected = false
            }else{
                cell.circularView.backgroundColor = UIColor.clear
            }
            if self.status == "MatchLoad"{
                cell.circularView.backgroundColor = UIColor.init(hexString: ColorConstants.RED)
            }
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(availableVechClicked(sender:)))
            cell.circularView.tag = indexPath.item
            cell.circularView.addGestureRecognizer(tapGesture)
       
            return cell
            
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "searchLoadCell", for: indexPath) as! SearchLoadCell
            //   self.saveVehicleImage()
            if isContainerClicked == true && ContainerVehicleNameArray.isEmpty == false && containerImageArray.isEmpty == false && containerImg.isEmpty == false{
                
                //         print(" containerImg.count = \(containerImg.count)")
                //      print(" ContainerVehicleNameArray.count = \(ContainerVehicleNameArray.count)")
                cell.vehicleName.text = ContainerVehicleNameArray[indexPath.row]
                if containerImg.count == ContainerVehicleNameArray.count{
                    cell.vehicleImage.image = containerImg[indexPath.row]
                }
                
            }else if isFTLClicked == true && FleetVehicleNameArray.isEmpty == false && fleetImageArray.isEmpty == false && fleetImg.isEmpty == false{
                print(" fleetImg.count = \(fleetImg.count)")
                print(" FleetVehicleNameArray.count = \(FleetVehicleNameArray.count)")
                cell.vehicleName.text = FleetVehicleNameArray[indexPath.row]
                if fleetImg.count == ContainerVehicleNameArray.count{
                    cell.vehicleImage.image = self.fleetImg[indexPath.row]
                }
            }else if isLTLClicked == true && LTLVehicleNameArray.isEmpty == false && ltlImageArray.isEmpty == false && ltlImg.isEmpty == false{
                //    print(" ltlImg.count = \(ltlImg.count)")
                //      print(" LTLVehicleNameArray.count = \(LTLVehicleNameArray.count)")
                cell.vehicleName.text = LTLVehicleNameArray[indexPath.row]
                if ltlImg.count == LTLVehicleNameArray.count{
                    cell.vehicleImage.image = ltlImg[indexPath.row]
                }
            }
            if selectedIndex == indexPath
            {
                cell.innerView.backgroundColor = UIColor.init(hexString: ColorConstants.RED)
            }
            else
            {
                cell.innerView.backgroundColor = UIColor.clear
            }
            return cell
        }
    }
    
    
    @objc func availableVechClicked(sender: UITapGestureRecognizer) {
        self.collectionSelectedIndex = selectedIndex
        DispatchQueue.main.async {
            self.availabelVechCollectionView.reloadData()
        }
        self.fleetInfoView.isHidden = false
        print("===== availableVechClicked ======")
        
        let tag = sender.view?.tag
        print("sender.view ======= \(String(describing: sender.view?.tag))")
        fleetInfoId.text = self.datumArray[tag!].bidCode
        fleetInfoVehicleType.text = self.datumArray[tag!].vehicleType.name
        fleetInfoFrom.text = self.datumArray[tag!].availableLocation
        fleetInfoTo.text = self.datumArray[tag!].destinationLocation
        fleetInfoLoadType.text = self.datumArray[tag!].loadType.loadType
        fleetInfoCargoType.text = self.datumArray[tag!].cargoType.cargoType
        // self.cellSelected = true
        self.fleetId = self.datumArray[tag!].bidCode
        self.vehicleType = self.datumArray[tag!].vehicleType.name
        self.vehicleDetailsFrom = self.datumArray[tag!].availableLocation
        self.vehicleDetailsTo = self.datumArray[tag!].destinationLocation!
        self.vehicleDetailsCargoType = self.datumArray[tag!].cargoType.cargoType
        self.vehicleDetailsLoadType = self.datumArray[tag!].loadType.loadType
        self.closeBidTime = self.datumArray[tag!].closedBidTime
        self.fleetBidId = self.datumArray[tag!].id
        
        print("self.datumArray closeBidTime ====>>>>> \(self.closeBidTime)")
        self.availabelVechCollectionView.reloadData()
        NotificationCenter.default.post(name: .FleetInfoView, object: nil)
        
        
        //        self.vehicle_LoadDetails = ["fleetId": self.fleetId,"vehicleType":self.vehicleType,"vehicleDetailsFrom": self,vehicleDetailsFrom,"vehicleDetailsTo":self.vehicleDetailsTo,"vehicleDetailsLoadType":self.vehicleDetailsLoadType,"vehicleDetailsCargoType":self.datumArray[tag!].cargoType.cargoType,"loadDetailsLoadId":self.loadDetailsLoadId,"loadDetailsLoadType":self.loadDetailsLoadType,"loadDetailsFrom":self.loadDetailsFrom,"loadDetailsTo":self.loadDetailsTo,"loadDetailsCargoWt":self.loadDetailsCargoWt,"loadDetailsNoOfVech":self.loadDetailsNoOfVech,"loadDetailsAvailDate":self.loadDetailsAvailDate,"loadDetailsAvailTime":self.loadDetailsAvailTime,"amount":self.amountTF.text!,"valid": self.validDateTF.text!,"range":self.validTimeTF.text!,"time":self.timeLabel.text!]
        //
        //
        self.availabelVechCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == availableLoadCollectionView{
            let itemWidth: CGFloat = ((availableLoadCollectionView.bounds.size.width)  / CGFloat(3))
            return CGSize(width: itemWidth, height: availableLoadCollectionView.bounds.size.height)
        }else if collectionView == availabelVechCollectionView{
            let itemWidth: CGFloat = ((availabelVechCollectionView.bounds.size.width)  / CGFloat(3.22))
            return CGSize(width: itemWidth, height: availabelVechCollectionView.bounds.size.height)
        }else{
            let itemWidth: CGFloat = ((collectionView.bounds.size.width)  / CGFloat(4.0))
            return CGSize(width: itemWidth, height: collectionView.bounds.size.height)
        }
    }
    
    var selectedIndexx = IndexPath()
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndexx = indexPath
        selectedIndex = indexPath
        self.collectionView.reloadData()
        //   self.availabelVechCollectionView.reloadData()
        self.availableLoadCollectionView.reloadData()
    }
    
    @IBAction func dashboardClicked(_ sender: UITapGestureRecognizer) {
        print("dashboard")
        assignClicked = true
        
        self.searchLoadView.isHidden = true
        self.searchloadView1.isHidden = true
        self.searchLoadMainView.isHidden = true
        self.seperatorLineView.isHidden = true
        NotificationCenter.default.post(name: Notification.Name("Dashboard"), object: nil)
        self.assignBookingArray.removeAll()
        self.datumArray.removeAll()
        if assignClicked{
            self.assignBookingList(search: "")
        }
    }
    
    
    @IBAction func searchLoadClicked(_ sender: UITapGestureRecognizer) {
        print("searchload")
        self.dashboardView.isHidden = true
        self.dashboardView1.isHidden = true
        self.tableView.isHidden = true
        self.tableViewWrapper.isHidden = true
        self.seperatorLineView.isHidden = true
        NotificationCenter.default.post(name: .SearchLoad, object: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if assignClicked{
            return assignBookingArray.count
        }else if fleetClicked{
            return datumArray.count
        }else{
            return loadsQuotedArray.count
        }
        
    }
    
    var isDragging = false
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        // print("offsetY = \(offsetY) ======= contentHeight ==== \(contentHeight - scrollView.frame.size.height - 300)")
        if scrollView == tableView{
            if offsetY > contentHeight - scrollView.frame.size.height - 300 {
                
                pageNo += 1
                
                isDragging = true
                if fleetClicked {
                    if self.datumArray[0].count! != self.datumArray.count{
                        self.fleetPostedDashboard()
                    }
                    
                }else if assignClicked{
                    if self.assignBookingArray[0].count! != self.assignBookingArray.count{
                        self.assignBookingList(search: "")
                    }
                    
                }else{
                    if self.loadsQuotedArray[0].count! != self.loadsQuotedArray.count{
                        self.loadsQuotedList()
                    }
                }
            }else{
                isDragging = false
                
            }
        }else{
            
        }
        
        
    }
    
    var fleetDetails = [String:Any]()
    
    
    @objc func editFleetPosted(recognizer: UITapGestureRecognizer){
        let tag = recognizer.view?.tag
        self.fleetDetails  = ["fleetDetails":self.datumArray[tag!],"state" : "Dashboard"]
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "PostFleetVC") }, with: "fourth")
        sideMenuController?.setContentViewController(with: "fourth")
        NotificationCenter.default.post(name: .editFleetPosted,object: nil,userInfo: self.fleetDetails)
        
    }
    
    @objc func assignView(recognizer: UITapGestureRecognizer){
        let tag = recognizer.view?.tag
        let assignDetail  = ["assignDetail":self.assignBookingArray[tag!],"state" : "Dashboard"] as [String : Any]
        
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "AssignViewController") }, with: "18")
        sideMenuController?.setContentViewController(with: "18")
        
        NotificationCenter.default.post(name: .assign ,object: nil,userInfo: assignDetail)
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DSCell") as! DashboardAndSearchLoadCell
        let makeBooking = UITapGestureRecognizer(target: self, action: #selector(MakeBooking(recognizer:)))
        cell.makeBookingBtn.addGestureRecognizer(makeBooking)
        cell.mainView.isHidden = true
        cell.fleetPostedwrapper.isHidden = true
        cell.view2.isHidden = true
        cell.view3.isHidden = true
        cell.fleetView2.isHidden = true
        cell.loadCameraView.isHidden = true
        cell.mainView2.isHidden = true
        cell.loadsPostedWrapper.isHidden = true
        cell.view_2.isHidden = true
        cell.view_3.isHidden = true
        cell.loadView2.isHidden = true
        
        if loginUserType == UserStates.SERVICE_USER{
            cell.fleetPostedwrapper.isHidden = true
            cell.mainView.isHidden = true
            cell.mainView2.isHidden = false
            cell.loadsPostedWrapper.isHidden = false
        }else if loginUserType == UserStates.SERVICE_PROVIDER{
            cell.fleetPostedwrapper.isHidden = false
            cell.mainView.isHidden = false
            cell.mainView2.isHidden = true
            cell.loadsPostedWrapper.isHidden = true
        }
        
        if fleetClicked == true{
            
            let fleetDetails = self.datumArray[indexPath.row]
            cell.fleetPostedId.text = fleetDetails.bidCode
            cell.fleetPostedVechType.text = fleetDetails.vehicleType.name
            cell.fleetPostedFrom.text = fleetDetails.availableLocation
            cell.fleetPostedTo.text = fleetDetails.destinationLocation
            if fleetDetails.expectedFreight != nil {
                cell.fleetPostedAmount.text = "\(fleetDetails.expectedFreight!)" + ".00"
            }else{
                cell.fleetPostedAmount.text = "00.00"
            }
            
            cell.fleetPostedRegistrationNo.text = fleetDetails.registrationNo
            cell.fleetPostedLoadType.text = fleetDetails.loadType.loadType
            cell.fleetPOstedCargoType.text = fleetDetails.cargoType.cargoType
            let cargoWt = "\(fleetDetails.capacityWeight)" + " \(fleetDetails.capacityWeightUnit.name)"
            cell.fleetPostedCargoWt.text = cargoWt
            cell.fleetPostedNoOfVech.text = "\(fleetDetails.noOfVehicles)"
            let timestap: Double = Double(fleetDetails.availableDateTime / 1000)
            let availableFromtime = self.epochTime(epochTimee: timestap)
            let availableFromdate = self.epochDate(epochDate: timestap)
            let timestamp: Double = Double(fleetDetails.closedBidTime / 1000)
            let availableTilltime = self.epochTime(epochTimee: timestamp)
            let availableTilldate = self.epochDate(epochDate: timestamp)
            cell.fleetPostedAvailFromDate.text = availableFromdate
            cell.fleetPOstedAvailFromTime.text = availableFromtime
            cell.fleetPostedAvailTillDate.text = availableTilldate
            cell.fleetPostedAvailTillTime.text = availableTilltime
            
            let edit = UITapGestureRecognizer(target: self, action: #selector(editFleetPosted(recognizer:)))
            cell.editView.addGestureRecognizer(edit)
            cell.editView.tag = indexPath.row
            
            
            if fleetDetails.isInvited == true{
                cell.messageView.isHidden = false
                cell.messageCountLabel.text = String(fleetDetails.bidInviteCount)
                cell.fleetView3.isHidden = false
                let invite = UITapGestureRecognizer(target: self, action: #selector(invite(recognizer:)))
                cell.inviteFleetView.addGestureRecognizer(invite)
                cell.inviteFleetView.tag = indexPath.row
            }else{
                cell.messageView.isHidden = true
                cell.messageCountLabel.text = "00"
                cell.fleetView3.isHidden = true
            }
            
            
            if loginUserType == UserStates.SERVICE_USER{
                cell.fleetPostedwrapper.isHidden = true
                cell.mainView.isHidden = true
                cell.mainView2.isHidden = true
                cell.loadsPostedWrapper.isHidden = false
            }else if loginUserType == UserStates.SERVICE_PROVIDER{
                cell.mainView.isHidden = true
                cell.mainView2.isHidden = true
                cell.fleetPostedwrapper.isHidden = false
                cell.loadsPostedWrapper.isHidden = true
            }
        }else if assignClicked == true{
            let assign = assignBookingArray[indexPath.row]
            cell.bookingNo.text = assign.bookingNo
            cell.loadId.text = assign.loadBidCode
            cell.vehicleType.text = assign.vehicleContainerDetails!.vehicleType.name
            cell.fleetId.text = assign.fleetBidCode
            cell.fromLabel.text = assign.pickUpLocation
            cell.toLabel.text = assign.destination
            
            if assign.inviteAmount != nil {
                cell.freightAmount.text = "\(Int(assign.inviteAmount!))" + ".00"
            }else{
                cell.freightAmount.text = "00.00"
            }
            
            cell.bookingNumberStack.isHidden = false
            cell.rankView.isHidden = true
            cell.rankLabel.isHidden = true
            cell.registrationNo.text = assign.registrationNo
            cell.loadType.text = assign.loadType?.loadType
            cell.cargoType.text = assign.cargoType?.cargoType
            cell.noOfVehicle.text = "\(assign.vehicleContainerDetails!.noOfVehicles)"
            
            let assignn = UITapGestureRecognizer(target: self, action: #selector(assignView(recognizer:)))
            cell.assignView.addGestureRecognizer(assignn)
            cell.assignView.tag = indexPath.row
            
            
            if assign.packgs != nil{
                cell.cargoWt.text = "\(assign.vehicleContainerDetails!.chargeableWeight)" + ".0" + " \(assign.vehicleContainerDetails!.chargeableWeightUnit.name)" + " / " + "\(String(describing: assign.packgs!))"
            }else{
                cell.cargoWt.text = "\(assign.vehicleContainerDetails!.chargeableWeight)" + ".0" + " \(assign.vehicleContainerDetails!.chargeableWeightUnit.name)"
            }
            
            let bookingDatee = Double(assign.createDate!) / 1000
            cell.bookingDate.text = self.epochDate(epochdate: bookingDatee)
            cell.bookingTime.text = self.epochTimeSec(epochTimee: bookingDatee)
            
            
        }else if loadsQuotedClicked{
            let load = loadsQuotedArray[indexPath.row]
            cell.bookingNo.isHidden = true
            cell.loadId.text = load.availableLoadBid?.bidCode
            cell.vehicleType.text = load.availableLoadBid?.vehicleType.name
            cell.fleetId.text = load.availableFleetBid?.bidCode
            cell.fromLabel.text = load.availableLoadBid?.availableLocation
            cell.toLabel.text = load.availableLoadBid?.destinationLocation
            
            if load.quotationAmount != nil {
                cell.freightAmount.text = "\((load.quotationAmount!))" + "0"
            }else{
                cell.freightAmount.text = "00.00"
            }
            
            
            cell.rankView.isHidden = false
            cell.rankLabel.isHidden = false
            cell.registrationNo.text = load.availableFleetBid?.registrationNo
            cell.loadType.text = load.availableLoadBid?.loadType.loadType
            cell.cargoType.text = load.availableFleetBid?.cargoType.cargoType
            cell.noOfVehicle.text = "\(String(describing: load.availableFleetBid!.noOfVehicles))"
            
            if load.availableLoadBid?.packgs != nil{
                cell.cargoWt.text = "\(String(describing: load.availableLoadBid!.capacityWeight))" + ".0" + " \(String(describing: load.availableLoadBid!.capacityWeightUnit.name!))" + " / " + "\(String(describing: load.availableLoadBid!.packgs!))"
            }else{
                cell.cargoWt.text = "\(String(describing: load.availableLoadBid!.capacityWeight))" + ".0" + " \(String(describing: load.availableLoadBid?.capacityWeightUnit.name!))"
            }
            cell.bookingNumberStack.isHidden = true
            cell.lowestBidLabel.text = "\(load.lowestBidAmount!)" + "0"
            let availDate = Double((load.availableFleetBid?.availableDateTime)!) / 1000
            
            cell.bookingDate.text = self.epochDate(epochdate: availDate)
            cell.bookingTime.text = self.epochTimeSec(epochTimee: availDate)
            cell.rankCountLabel.text = "\(load.bidRank!)"
            let bidNow = UITapGestureRecognizer(target: self, action: #selector(bidNow(recognizer:)))
            cell.bidNowView.addGestureRecognizer(bidNow)
            cell.bidNowView.tag = indexPath.row
            
            
        }else{
            cell.fleetPostedwrapper.isHidden = true
            cell.mainView.isHidden = false
            cell.loadsPostedWrapper.isHidden = true
        }
        
        if indexPath.row == currentRow {
            if cellTapped == false {
                if loginUserType == UserStates.SERVICE_PROVIDER {
                    if fleetClicked == true{
                        cell.mainView.isHidden = true
                        cell.fleetView2.isHidden = true
                        cell.fleetView3.isHidden = true
                        self.tableView.rowHeight = 130
                    }else if loadsQuotedClicked == true{
                        cell.view2.isHidden = true
                        cell.view3.isHidden = true
                        self.tableView.rowHeight = 160
                        cell.loadsQuotedView.isHidden = true
                    }else{
                        cell.view2.isHidden = true
                        cell.view3.isHidden = true
                        self.tableView.rowHeight = 160
                        cell.loadsQuotedView.isHidden = true
                        
                    }
                }else if loginUserType == UserStates.SERVICE_USER{
                    if fleetClicked == true{
                        cell.mainView2.isHidden = true
                        cell.loadView2.isHidden = true
                        cell.loadVIew3.isHidden = true
                        self.tableView.rowHeight = 130
                    }else if loadsQuotedClicked == true{
                        cell.view_2.isHidden = true
                        cell.view_3.isHidden = true
                        self.tableView.rowHeight = 140
                        cell.loadsQuotedView.isHidden = true
                    }else{
                        cell.view2.isHidden = true
                        cell.view3.isHidden = true
                        self.tableView.rowHeight = 140
                        cell.loadsQuotedView.isHidden = true
                    }
                }
            } else {
                if loginUserType == UserStates.SERVICE_PROVIDER{
                    if fleetClicked == true{
                        cell.mainView.isHidden = true
                        cell.fleetView2.isHidden = false
                        let fleetDetails = self.datumArray[indexPath.row]
                        if fleetDetails.isInvited == true{
                            cell.messageView.isHidden = false
                            cell.messageCountLabel.text = String(fleetDetails.bidInviteCount)
                            cell.fleetView3.isHidden = false
                            self.tableView.rowHeight = 430
                            
                        }
                        else{
                            cell.messageView.isHidden = true
                            cell.messageCountLabel.text = "00"
                            cell.fleetView3.isHidden = true
                            self.tableView.rowHeight = 400
                        }
                        
                    }else if loadsQuotedClicked == true{
                        cell.view2.isHidden = false
                        cell.view3.isHidden = true
                        cell.loadsQuotedView.isHidden = false
                        cell.fleetView2.isHidden = true
                        self.tableView.rowHeight = 410
                    }else{
                        cell.view2.isHidden = false
                        cell.view3.isHidden = false
                        cell.loadsQuotedView.isHidden = true
                        cell.fleetView2.isHidden = true
                        self.tableView.rowHeight = 410
                    }
                }else if loginUserType == UserStates.SERVICE_USER{
                    if fleetClicked == true{
                        cell.mainView2.isHidden = true
                        cell.loadView2.isHidden = false
                        cell.loadVIew3.isHidden = false
                        cell.loadCameraView.isHidden = false
                        self.tableView.rowHeight = 400
                    }else if loadsQuotedClicked == true{
                        cell.view_2.isHidden = false
                        cell.view_3.isHidden = true
                        cell.loadView2.isHidden = true
                        self.tableView.rowHeight = 350
                    }else{
                        cell.view_2.isHidden = false
                        cell.view_3.isHidden = false
                        cell.loadView2.isHidden = true
                        self.tableView.rowHeight = 400
                    }
                }
            }
        }else{
            if fleetClicked == true{
                cell.mainView.isHidden = true
                cell.fleetView2.isHidden = true
                cell.fleetView3.isHidden = true
                self.tableView.rowHeight = 130
            }
        }
        
        
        return cell
    }
    var loadObject = [String: Any]()
    
    
    @objc func bidNow(recognizer: UITapGestureRecognizer){
        
        let tag = recognizer.view?.tag
        
        
        for data in loadsQuotedArray{
            
            print("markerBidCode == \(String(describing: self.loadsQuotedArray[tag!].availableLoadBid?.bidCode))")
            if (data.availableLoadBid?.bidCode == self.loadsQuotedArray[tag!].availableLoadBid?.bidCode){
                print("data.bidCode === \(String(describing: data.availableLoadBid?.bidCode))")
                let timestap: Double = Double(data.availableFleetBid!.availableDateTime / 1000)
                let availableFromtime = self.epochTime(epochTimee: timestap)
                let availableFromdate = self.epochDate(epochDate: timestap)
                let timestamp: Double = Double(data.availableFleetBid!.closedBidTime / 1000)
                let availableTilltime = self.epochTime(epochTimee: timestamp)
                let availableTilldate = self.epochDate(epochDate: timestamp)
                //  let lowestBid = data.availableLoadBidQuotaions[0].quotationAmount
                let ownLastThreeBids = data.loadBidQuotaionDetails
                
                for bid in ownLastThreeBids!{
                    print("====== bid.quotationAmount ======= \(String(describing: bid.quotationAmount))")
                }
                
                
                
                loadObject = ["id": data.id!,"bidCode": data.availableLoadBid!.bidCode,
                              "vehicleType": data.availableLoadBid!.vehicleType.name,
                              "from": data.availableLoadBid!.availableLocation,
                              "to": data.availableLoadBid!.destinationLocation as Any,
                              "loadType": data.availableLoadBid!.loadType.loadType, "noOfVech":data.availableFleetBid!.noOfVehicles,
                              "cargoType": data.availableLoadBid!.cargoType.cargoType!,
                              "cargoWt": data.availableLoadBid!.capacityWeight ,
                              "packgs": data.availableLoadBid!.packgs!,
                              "availableFromdate": availableFromdate,
                              "availableFromtime": availableFromtime,
                              "availableTilltime": availableTilltime,
                              "availableTilldate": availableTilldate,
                              "kgName":data.availableLoadBid!.capacityWeightUnit.name!,
                              "lowestBid": 0,"closedBidTime":data.availableFleetBid!.closedBidTime ,
                              "bidRank": data.bidRank! ,
                              "availableLoadQuotations": "",
                              "ownLastThreeBids": ownLastThreeBids!,
                              "availableLoadBidQuotaionElement":data.loadBidQuotaionDetails!,"latitude":0.0,"longitude":0.0,"vehicleTypeId":0,"loadTypeId":0,"status":"MapView"]
                
            }
        }
          sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "LoadDetailViewController") }, with: "seventh")
        sideMenuController?.setContentViewController(with: "seventh")
        NotificationCenter.default.post(name: Notification.Name("loadObj"),
                                        object:nil,userInfo: loadObject)
    }
    
    
    
    
    @objc func invite(recognizer: UITapGestureRecognizer){
        
        let tag = recognizer.view?.tag
        
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "FleetInvitedViewController") }, with: "29")
        sideMenuController?.setContentViewController(with: "29")
        
        NotificationCenter.default.post(name: .fleetInvited, object: nil,userInfo: ["bidId": self.datumArray[tag!].id])
        
    }
    
    
    
    
    @objc func MakeBooking(recognizer: UITapGestureRecognizer){
        sideMenuController?.setContentViewController(with: "25")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if loginUserType == UserStates.SERVICE_USER{
            if cellTapped{
                if indexPath as IndexPath == selectedIndex {
                    if fleetClicked == true{
                        
                        if self.datumArray.count != 0{
                            let fleetDetails = datumArray[indexPath.row]
                            if fleetDetails.isInvited == true{
                                return 430
                            }
                            else{
                                return 380
                            }
                        }
                        
                    }
                    if loadsQuotedClicked == true{
                        return 350
                    }
                    return 410
                }
            }
            if fleetClicked == true{
                return 130
            }
            return 140 // Default Size
        }else{
            if cellTapped{
                if indexPath as IndexPath == selectedIndex {
                    if fleetClicked == true{
                        
                        if self.datumArray.count != 0{
                            let fleetDetails = datumArray[indexPath.row]
                            if fleetDetails.isInvited == true{
                                return 430
                            }
                            else{
                                return 400
                            }
                        }
                        
                    }else{
                        return 400
                    }
                    //Size you want to increase to
                }
            }
            if fleetClicked == true{
                return 130
            }
            return 160 // Default Size
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedIndex = indexPath
        let selectedRowIndex = indexPath
        currentRow = selectedRowIndex.row
        cellTapped = !cellTapped
        self.tableView.reloadDataWithAutoSizingCellWorkAround()
    }
    
    @IBAction func assignVehicle(_ sender: Any) {
        fleetClicked = false
        loadsQuotedClicked = false
        assignClicked = true
        
        if assignVechImage.image == UIImage(named:"assignVech") {
            assignVechLine.isHidden = false
            fleetPostLine.isHidden = true
            loadQuotedLine.isHidden = true
        }else if  assignVechImage.image == UIImage(named:"assignVechUnselected") {
            toggleView(mainLabel: assignVechLabel, label1: fleetPostLabel, label2: loadsQuotedLabel)
            toggleImage(mainImage: assignVechImage, image1: fleetPostedImage, image2: loadsQuotedImage, mainImageString: "assignVech", string1: "fleetPostedUnselected", string2: "loadsQuotedUnselected")
            assignVechLine.isHidden = false
            fleetPostLine.isHidden = true
            loadQuotedLine.isHidden = true
        }
        self.isDragging = false
        pageNo = 0
        self.assignBookingArray.removeAll()
        self.datumArray.removeAll()
        self.loadsQuotedArray.removeAll()
        assignBookingList(search: "")
        self.tableView.reloadData()
        tableView.scrollsToTop = true
    }
    
    
    var assignBookingArray = [AssignDatum]()
    
    
    
    func assignBookingList(search:String){
        
        
        var parameters = [String:Any]()
        
        parameters = [
            "recordsPerPage" : 10,
            "pageNo" : "\(self.pageNo)",
            "search" : "\(search)",
            "financialYear": self.currentFinancialYear!,
            "status": "NEW"
        ]
        
        if isDragging == false{
            NotificationCenter.default.post(name: .dashboardActivityIndicator,object: nil,userInfo: ["active" : true])
        }
        
        let headers = header()
        
        Alamofire.request(URLStatics.assignBookingList , method: .get, parameters: parameters,headers: headers)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                
                
                switch (response.result) {
                    
                case .success:
                    
                    if self.isDragging == false{
                        NotificationCenter.default.post(name: .dashboardActivityIndicator,object: nil,userInfo: ["active" : false])
                    }
                    
                    
                    if let reply = response.result.value {
                        var returnDict = [[String:Any]]()
                        print("======= JSON: Assign Vehicle ========  \(reply)")
                        let mainResponse = JSON(reply)
                        if let returnObj = mainResponse["data"].arrayObject as? [[String:Any]] {
                            returnDict = returnObj
                        }
                        
                        var cargoType : CargoTypeClass
                        var jobStatusDict = [String:Any]()
                        var cargoo = [String:Any]()
                        var loadTypeDict = [String:Any]()
                        var vehicleTypeDict = [String:Any]()
                        var documentDict = [String:Any]()
                        var documentSmallDict = [String:Any]()
                        var document : FirstPicID
                        var doucmentSmall : FirstPicID
                        var vehicleType : AssignVehicleType1
                        var loadType : LoadTypeClass
                        var jobStatus : JobStatusClass
                        var vehicleDetails : VehicleContainerDetails1
                        var vehicleDetailsDict = [String:Any]()
                        var grossWeightUnit : WeightUnit
                        var chargeableWTUnit : WeightUnit
                        var netWeightUnit : WeightUnit
                        var grosWtUnit = [String:Any]()
                        var chargebleWtUnit = [String:Any]()
                        var netWtUnit = [String:Any]()
                        for data in returnDict{
                            
                            jobStatusDict = data["jobStatus"] as! [String:Any]
                            jobStatus = JobStatusClass(id: jobStatusDict["id"] as! Int, jobStatus: jobStatusDict["jobStatus"] as! String)
                            cargoo = data["cargoType"] as! [String:Any]
                            cargoType = CargoTypeClass(id: cargoo["id"] as! Int, cargoType: cargoo["cargoType"] as! String)
                            
                            loadTypeDict = data["loadType"] as! [String:Any]
                            
                            loadType = LoadTypeClass(id: loadTypeDict["id"] as! Int, loadType: loadTypeDict["loadType"] as! String, code: loadTypeDict["code"] as! String)
                            vehicleDetailsDict = data["vehicleContainerDetails"] as! [String:Any]
                            
                            vehicleTypeDict = vehicleDetailsDict["vehicleType"] as! [String:Any]
                            grosWtUnit = vehicleDetailsDict["grossWeightUnit"] as! [String:Any]
                            chargebleWtUnit = vehicleDetailsDict["chargeableWeightUnit"] as! [String:Any]
                            netWtUnit = vehicleDetailsDict["netWeightUnit"] as! [String:Any]
                            
                            documentDict = vehicleTypeDict["document"] as! [String:Any]
                            documentSmallDict = vehicleTypeDict["documentSmall"] as! [String:Any]
                            document = FirstPicID(id: documentDict["id"] as! Int, mimeType: documentDict["mimeType"] as! String, type: documentDict["type"] as! String)
                            doucmentSmall = FirstPicID(id: documentSmallDict["id"] as! Int, mimeType: documentSmallDict["mimeType"] as! String, type: documentSmallDict["type"] as! String)
                            
                            
                            grossWeightUnit = WeightUnit(id: grosWtUnit["id"] as! Int, name: grosWtUnit["name"] as! String, type: grosWtUnit["type"] as! String)
                            chargeableWTUnit = WeightUnit(id: chargebleWtUnit["id"] as! Int, name: chargebleWtUnit["name"] as! String, type: chargebleWtUnit["type"] as! String)
                            
                            netWeightUnit = WeightUnit(id: netWtUnit["id"] as! Int, name: netWtUnit["name"] as! String, type: netWtUnit["type"] as! String)
                            
                            vehicleType = AssignVehicleType1(id: vehicleTypeDict["id"] as! Int, name: vehicleTypeDict["name"] as! String, sequence: vehicleTypeDict["sequence"] as! Double, container: vehicleTypeDict["container"] as! Bool, type: vehicleTypeDict["type"] as! String, document: document, documentSmall: doucmentSmall, vehicleOrder: vehicleTypeDict["vehicleOrder"] as! Int, documentID: vehicleTypeDict["documentId"] as! Int, smallDocID: vehicleTypeDict["smallDocId"] as! Int, weightCapacity: vehicleTypeDict["weightCapacity"] as? Int)
                            
                            
                            vehicleDetails = VehicleContainerDetails1(id: vehicleDetailsDict["id"] as! Int, vehicleType: vehicleType, noOfVehicles: vehicleDetailsDict["noOfVehicles"] as! Int, grossWeight: vehicleDetailsDict["grossWeight"] as! Int, grossWeightUnit: grossWeightUnit, chargeableWeight: vehicleDetailsDict["chargeableWeight"] as! Int, chargeableWeightUnit: chargeableWTUnit, netWeight: vehicleDetailsDict["netWeight"] as! Int, netWeightUnit: netWeightUnit)
                            
                            
                            
                            let assignDatum = AssignDatum(id: data["id"] as? Int,
                                                          jobStatus: jobStatus,
                                                          loadType: loadType,
                                                          cargoType: cargoType,
                                                          pickupDateTime: data["pickupDateTime"] as? Int,
                                                          deliveryDateTime: data["deliveryDateTime"] as? Int,
                                                          bookingNo: data["bookingNo"] as? String,
                                                          pickUpLocation: data["pickUpLocation"] as? String,
                                                          destination: data["destination"] as? String,
                                                          createDate: data["createDate"] as? Int,
                                                          vehicleContainerDetails: vehicleDetails,
                                                          bookingCust: data["bookingCust"] as! Bool,
                                                          shiper: data["shiper"] as! Bool, consigneer: data["consigneer"] as! Bool,
                                                          deliverCust: data["deliverCust"] as! Bool,
                                                          salesBillFinal: data["salesBillFinal"] as! Bool,
                                                          purchaseBillFinal: data["purchaseBillFinal"] as! Bool,
                                                          count: data["count"] as? Int,
                                                          lrNo: data["lrNo"] as? Int,
                                                          fleetBidCode: data["fleetBidCode"] as? String,
                                                          loadBidCode: data["loadBidCode"] as? String,
                                                          inviteAmount: data["inviteAmount"] as? Double,
                                                          fleetBidID: data["fleetBidId"] as? Int,
                                                          isinvite: data["isinvite"] as! Bool,
                                                          registrationNo: data["registrationNo"] as? String,
                                                          telephone: data["telephone"] as? String,
                                                          packgs: data["packgs"] as? Int,
                                                          quoteAmount: data["quoteAmount"] as? Int)
                            
                            if self.assignBookingArray.contains(where: { bid in bid.loadBidCode == assignDatum.loadBidCode }) {
                                
                            } else {
                                
                                self.assignBookingArray.append(assignDatum)
                            }
                            
                            
                        }
                        
                        print("self.assignBookingArray.count ====== \(self.assignBookingArray.count)")
                        self.tableView.reloadData()
                    }
                    break
                case .failure(let error):
                    
                    print("error \(error.localizedDescription)")
                    break
                }
        }
        
        
        
        
        
    }
    
    
    var loadsQuotedArray = [LoadQuotationDatum]()
    
    
    func loadsQuotedList(){
        var parameters = [String:Any]()
        
        parameters = [
            "recordsPerPage" : 10,
            "pageNo" : "\(self.pageNo)",
            "search" : "",
            "status" : "OPEN",
            "financialYear": self.currentFinancialYear!
            
        ]
        
        if isDragging == false{
            NotificationCenter.default.post(name: .dashboardActivityIndicator,object: nil,userInfo: ["active" : true])
        }
        
        let headers = header()
        
        
        Alamofire.request(URLStatics.quotations , method: .get, parameters: parameters,headers: headers)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                
                
                switch (response.result) {
                    
                case .success:
                    
                    if self.isDragging == false{
                        NotificationCenter.default.post(name: .dashboardActivityIndicator,object: nil,userInfo: ["active" : false])
                    }
                    
                    
                    if let reply = response.result.value {
                        var returnDict = [[String:Any]]()
                        print("======= JSON: Load Quoted ========  \(reply)")
                        let mainResponse = JSON(reply)
                        if let returnObj = mainResponse["data"].arrayObject as? [[String:Any]] {
                            returnDict = returnObj
                        }
                        
                        var availbaleFleetBid : AvailableBid?
                        var availFleetBidDict = [String:Any]()
                        var capacityFleetWeightUnitArr = [String:Any]()
                        var vehicleTypeFleet = [String:Any]()
                        var vehicleTypeeFleet : VehicleType?
                        var documentFleet : Logo?
                        var docsFleet : Logo?
                        var cargoTypeFleet : CargoType?
                        var cargoTypeArrayFleet = [String:Any]()
                        var documentSmallTypeFleet : [String:Any]?
                        var docuTypeFleet = [String:Any]()
                        var capacityWeightLoadFleet : CapacityWeightUnit?
                        var loadType1Fleet : LoadType1?
                        var loadTypeDictFleet = [String:Any]()
                        var currencyMasterFleet : CurrencyMaster?
                        var currencyMasterTypeFleet = [String:Any]()
                        
                        
                        
                        var capacityWeightUnitArr = [String:Any]()
                        var availLoadBidDict = [String:Any]()
                        var vehicleType = [String:Any]()
                        var availableLoadBid : AvailableBid?
                        var vehicleTypee : VehicleType?
                        var document : Logo?
                        var docs : Logo?
                        var cargoType : CargoType?
                        var cargoTypeArray = [String:Any]()
                        var documentSmallType : [String:Any]?
                        var docuType = [String:Any]()
                        var capacityWeightLoad : CapacityWeightUnit?
                        var loadType1 : LoadType1?
                        var loadTypeDict = [String:Any]()
                        var currencyMaster : CurrencyMaster?
                        var currencyMasterType = [String:Any]()
                        var loadBid = [[String:Any]]()
                        var loadBidDetails : AvailableLoadBidQuotaion?
                        for data in returnDict{
                            
                            var loadBidQuotation = [AvailableLoadBidQuotaion]()
                            
                            
                            
                            //   availLoadBid
                            availLoadBidDict = data["availableLoadBid"] as! [String:Any]
                            vehicleType = availLoadBidDict["vehicleType"] as! [String:Any]
                            documentSmallType = vehicleType["documentSmall"] as? [String:Any]
                            docuType = vehicleType["document"] as! [String:Any]
                            
                            document = Logo(id: documentSmallType?["id"] as? Int,mimeType: documentSmallType?["mimeType"] as? String,type: ((documentSmallType?["type"] as? String)))
                            
                            docs = Logo(id: docuType["id"] as? Int, mimeType: docuType["mimeType"] as? String, type: ((docuType["type"] as? String)))
                            
                            
                            if let returnObject =
                                data["loadBidQuotaionDetails"] as? [[String:Any]] {
                                loadBid = returnObject
                            }
                            
                            for bid in loadBid{
                                print("quotationAmount ==== \( String(describing: bid["quotationAmount"] as? Double))")
                                loadBidDetails = AvailableLoadBidQuotaion(id: (bid["id"] as? Int)!, quotationAmount:Int(truncating: bid["quotationAmount"] as! NSNumber) , currencyMaster: nil, quotationEntryDate: (bid["quotationEntryDate"] as? Int)!)
                                loadBidQuotation.append(loadBidDetails!)
                            }
                            
                            print("loadBidQuotation?.count ====== \(String(describing: loadBidQuotation.count))")
                            
                            
                            
                            vehicleTypee = VehicleType(id: vehicleType["id"] as! Int,
                                                       name: vehicleType["name"] as! String,
                                                       sequence: Int(vehicleType["sequence"] as! Double),
                                                       container: vehicleType["container"] as! Bool,
                                                       type: vehicleType["type"] as! String,
                                                       document: docs!, documentSmall: document!,
                                                       vehicleOrder: vehicleType["vehicleOrder"] as! Int,
                                                       weightCapacity: vehicleType["weightCapacity"] as? Int,
                                                       documentID: vehicleType["documentId"] as? Int,
                                                       smallDocID: vehicleType["smallDocId"] as? Int)
                            
                            cargoTypeArray = availLoadBidDict["cargoType"] as! [String : Any]
                            
                            cargoType = CargoType(id: cargoTypeArray["id"] as? Int,
                                                  cargoType: cargoTypeArray["cargoType"] as? String)
                            
                            if let capacityWeight = availLoadBidDict["capacityWeightUnit"] as? [String:Any] {
                                capacityWeightUnitArr = capacityWeight
                            }
                            
                            capacityWeightLoad = CapacityWeightUnit(id: capacityWeightUnitArr["id"] as? Int, name: capacityWeightUnitArr["name"] as? String, type: capacityWeightUnitArr["type"] as? String)
                            loadTypeDict = availLoadBidDict["loadType"] as! [String:Any]
                            loadType1 = LoadType1(id: loadTypeDict["id"] as! Int, loadType: loadTypeDict["loadType"] as! String, code: loadTypeDict["code"] as! String)
                            
                            currencyMasterType = data["currencyMaster"] as! [String : Any]
                            currencyMaster = CurrencyMaster(id: currencyMasterType["id"] as? Int, currencyCode: currencyMasterType["currencyCode"] as? String)
                            
                            availableLoadBid = AvailableBid(id: availLoadBidDict["id"] as! Int,
                                                            bidCode: availLoadBidDict["bidCode"] as! String,
                                                            creationDate: availLoadBidDict["creationDate"] as! Int,
                                                            closedBidTime: availLoadBidDict["closedBidTime"] as! Int,
                                                            availableLocation: availLoadBidDict["availableLocation"] as! String,
                                                            destinationLocation: availLoadBidDict["destinationLocation"] as? String,
                                                            comments: availLoadBidDict["comments"] as? String,
                                                            availableLatitude: availLoadBidDict["availableLatitude"] as? Double,
                                                            availableLongitude: availLoadBidDict["availableLongitude"] as? Double,
                                                            destinationLatitude: availLoadBidDict["destinationLatitude"] as? Double,
                                                            destinationLongitude: availLoadBidDict["destinationLongitude"] as? Double,
                                                            bidInviteCount: availLoadBidDict["bidInviteCount"] as? Int,
                                                            vehicleType: vehicleTypee!,
                                                            noOfVehicles: availLoadBidDict["noOfVehicles"] as! Int,
                                                            availableDateTime:  availLoadBidDict["availableDateTime"] as! Int,
                                                            cargoType: cargoType!,
                                                            capacityWeight: Int(truncating: availLoadBidDict["capacityWeight"] as! NSNumber),
                                                            capacityWeightUnit: capacityWeightLoad!,
                                                            loadType: loadType1!,
                                                            expectedFreight: availLoadBidDict["expectedFreight"] as? Int,
                                                            currencyMaster: nil,
                                                            status: availLoadBidDict["status"] as! String,
                                                            registrationNo: availLoadBidDict["registrationNo"] as? String,
                                                            packgs: availLoadBidDict["packgs"] as? Int,
                                                            containerNo: availLoadBidDict["containerNo"] as? String)
                            
                            
                            //  availbaleFleetBid
                            
                            availFleetBidDict = data["availableFleetBid"] as! [String:Any]
                            
                            vehicleTypeFleet = availFleetBidDict["vehicleType"] as! [String:Any]
                            documentSmallTypeFleet = vehicleTypeFleet["documentSmall"] as? [String:Any]
                            docuTypeFleet = vehicleTypeFleet["document"] as! [String:Any]
                            
                            documentFleet = Logo(id: documentSmallTypeFleet?["id"] as? Int,mimeType: documentSmallTypeFleet?["mimeType"] as? String,type: ((documentSmallTypeFleet?["type"] as? String)))
                            
                            docsFleet = Logo(id: docuTypeFleet["id"] as? Int, mimeType: docuTypeFleet["mimeType"] as? String, type: ((docuTypeFleet["type"] as? String)))
                            
                            vehicleTypeeFleet = VehicleType(id: vehicleTypeFleet["id"] as! Int,
                                                            name: vehicleTypeFleet["name"] as! String,
                                                            sequence: Int(vehicleTypeFleet["sequence"] as! Double),
                                                            container: vehicleTypeFleet["container"] as! Bool,
                                                            type: vehicleTypeFleet["type"] as! String,
                                                            document: docsFleet!, documentSmall: documentFleet!,
                                                            vehicleOrder: vehicleTypeFleet["vehicleOrder"] as! Int,
                                                            weightCapacity: vehicleTypeFleet["weightCapacity"] as? Int,
                                                            documentID: vehicleTypeFleet["documentId"] as? Int,
                                                            smallDocID: vehicleTypeFleet["smallDocId"] as? Int)
                            
                            cargoTypeArrayFleet = availFleetBidDict["cargoType"] as! [String : Any]
                            
                            cargoTypeFleet = CargoType(id: cargoTypeArrayFleet["id"] as? Int,
                                                       cargoType: cargoTypeArrayFleet["cargoType"] as? String)
                            
                            if let capacityWeight = availFleetBidDict["capacityWeightUnit"] as? [String:Any] {
                                capacityFleetWeightUnitArr = capacityWeight
                            }
                            
                            capacityWeightLoadFleet = CapacityWeightUnit(id: capacityFleetWeightUnitArr["id"] as? Int, name: capacityFleetWeightUnitArr["name"] as? String, type: capacityFleetWeightUnitArr["type"] as? String)
                            
                            
                            loadTypeDictFleet = availFleetBidDict["loadType"] as! [String:Any]
                            
                            loadType1Fleet = LoadType1(id: loadTypeDictFleet["id"] as! Int, loadType: loadTypeDictFleet["loadType"] as! String, code: loadTypeDictFleet["code"] as! String)
                            
                            //    currencyMasterTypeFleet = availFleetBidDict["currencyMaster"] as! [String : Any]
                            //
                            //    currencyMasterFleet = CurrencyMaster(id: currencyMasterTypeFleet["id"] as? Int, currencyCode: currencyMasterTypeFleet["currencyCode"] as? String)
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            availbaleFleetBid = AvailableBid(id: availFleetBidDict["id"] as! Int,
                                                             bidCode: availFleetBidDict["bidCode"] as! String,
                                                             creationDate: availFleetBidDict["creationDate"] as! Int,
                                                             closedBidTime: availFleetBidDict["closedBidTime"] as! Int,
                                                             availableLocation: availFleetBidDict["availableLocation"] as! String,
                                                             destinationLocation: availFleetBidDict["destinationLocation"] as? String,
                                                             comments: availFleetBidDict["comments"] as? String,
                                                             availableLatitude: availFleetBidDict["availableLatitude"] as? Double,
                                                             availableLongitude: availFleetBidDict["availableLongitude"] as? Double,
                                                             destinationLatitude: availFleetBidDict["destinationLatitude"] as? Double,
                                                             destinationLongitude: availFleetBidDict["destinationLongitude"] as? Double,
                                                             bidInviteCount: availFleetBidDict["bidInviteCount"] as? Int,
                                                             vehicleType: vehicleTypeeFleet!,
                                                             noOfVehicles: availFleetBidDict["noOfVehicles"] as! Int,
                                                             availableDateTime:  availFleetBidDict["availableDateTime"] as! Int,
                                                             cargoType: cargoTypeFleet! ,
                                                             capacityWeight: availFleetBidDict["capacityWeight"] as! Int,
                                                             capacityWeightUnit: capacityWeightLoadFleet! ,
                                                             loadType: loadType1Fleet!,
                                                             expectedFreight: availFleetBidDict["expectedFreight"] as? Int,
                                                             currencyMaster: nil,
                                                             status: availFleetBidDict["status"] as! String,
                                                             registrationNo: availFleetBidDict["registrationNo"] as? String,
                                                             packgs: availFleetBidDict["packgs"] as? Int,
                                                             containerNo: availFleetBidDict["containerNo"] as? String)
                            
                            
                            
                            
                            let loadQUote = LoadQuotationDatum(id: data["id"] as? Int, availableLoadBid: availableLoadBid, fleetOwner: nil, quotationAmount: data["quotationAmount"] as? Double, currencyMaster: currencyMaster, validDateTime: data["validDateTime"] as? Int, quotationEntryDate: data["quotationEntryDate"] as? Int,
                                                               quoteID: data["quoteID"] as? String,
                                                               status: data["status"] as? String, availableFleetBid: availbaleFleetBid, updateCount: data["updateCount"] as? Int, loadBidQuotaionDetails: loadBidQuotation, count: data["count"] as? Int, bidRank: data["bidRank"] as? Int, loadOwner: nil, lowestBidAmount: data["lowestBidAmount"] as? Double, quotationUpdatedDate: data["quotationUpdatedDate"] as? Double)
                            
                            if self.loadsQuotedArray.contains(where: { bid in bid.availableLoadBid?.bidCode == loadQUote.availableLoadBid?.bidCode }) {
                                
                            } else {
                                
                                self.loadsQuotedArray.append(loadQUote)
                            }
                            
                            
                        }
                        
                        print("self.loadsQuotedArray.count ====== \(self.loadsQuotedArray.count)")
                        
                        
                        self.tableView.reloadData()
                    }
                    break
                case .failure(let error):
                    
                    print("error \(error.localizedDescription)")
                    break
                }
        }
        
        
    }
    
    
    
    
    
    
    var fleetClicked = false
    var loadsQuotedClicked = false
    var assignClicked = false
    
    @IBAction func fleetPosted(_ sender: Any) {
        fleetClicked = true
        loadsQuotedClicked = false
        assignClicked = false
        if fleetPostedImage.image == UIImage(named:"fleetsPosted") {
            assignVechLine.isHidden = true
            fleetPostLine.isHidden = false
            loadQuotedLine.isHidden = true
        }else if  fleetPostedImage.image == UIImage(named:"fleetPostedUnselected") {
            toggleView(mainLabel: fleetPostLabel, label1: assignVechLabel, label2: loadsQuotedLabel)
            toggleImage(mainImage: fleetPostedImage, image1: assignVechImage, image2: loadsQuotedImage, mainImageString: "fleetsPosted", string1: "assignVechUnselected", string2: "loadsQuotedUnselected")
            assignVechLine.isHidden = true
            fleetPostLine.isHidden = false
            loadQuotedLine.isHidden = true
        }
        isDragging = false
        pageNo = 0
        
        self.assignBookingArray.removeAll()
        self.datumArray.removeAll()
        self.loadsQuotedArray.removeAll()
        self.fleetPostedDashboard()
        
        self.tableView.reloadData()
        tableView.scrollsToTop = true
    }
    
    @IBAction func loadsQuoted(_ sender: Any) {
        
        fleetClicked = false
        loadsQuotedClicked = true
        assignClicked = false
        self.tableView.reloadData()
        if loadsQuotedImage.image == UIImage(named:"loadsQuoted") {
            assignVechLine.isHidden = true
            fleetPostLine.isHidden = true
            loadQuotedLine.isHidden = false
        }
        else if  loadsQuotedImage.image == UIImage(named:"loadsQuotedUnselected") {
            toggleView(mainLabel: loadsQuotedLabel, label1: assignVechLabel, label2: fleetPostLabel)
            toggleImage(mainImage: loadsQuotedImage, image1: assignVechImage, image2: fleetPostedImage, mainImageString: "loadsQuoted", string1: "assignVechUnselected", string2: "fleetPostedUnselected")
            assignVechLine.isHidden = true
            fleetPostLine.isHidden = true
            loadQuotedLine.isHidden = false
        }
        
        isDragging = false
        pageNo = 0
        self.assignBookingArray.removeAll()
        self.datumArray.removeAll()
        self.loadsQuotedArray.removeAll()
        self.loadsQuotedList()
        self.tableView.reloadData()
    }
    
    func toggleView(mainLabel:UILabel,label1:UILabel,label2:UILabel){
        mainLabel.textColor = UIColor.init(hexString: "#0F2541")
        label1.textColor = UIColor.init(hexString: "#C0C6CC")
        label2.textColor = UIColor.init(hexString: "#C0C6CC")
    }
    
    func toggleImage(mainImage:UIImageView,image1:UIImageView,image2:UIImageView,mainImageString:String,string1:String,string2:String){
        mainImage.image = UIImage(named: mainImageString)
        image1.image = UIImage(named: string1)
        image2.image = UIImage(named: string2)
    }
    
    @IBAction func conatinerClicked(_ sender: Any) {
        isContainerClicked = true
        isFTLClicked = false
        isLTLClicked = false
        
        if isContainerClicked == true{
            containerLabel.textColor = UIColor(hexString: "#0F2541")
            self.containerLine.isHidden = false
            self.ltlLine.isHidden = true
            self.ftlLine.isHidden = true
            self.FTLLabel.textColor = UIColor(hexString: "#6F7179")
            self.ltlLabel.textColor = UIColor(hexString: "#6F7179")
            //   self.saveVehicleImage()
        }else if isContainerClicked == false {
            
        }
        
        
        self.collectionView.reloadData()
    }
    
    @IBAction func FTLClicked(_ sender: Any) {
        isContainerClicked = false
        isFTLClicked = true
        isLTLClicked = false
        if isFTLClicked == true{
            FTLLabel.textColor = UIColor(hexString: "#0F2541")
            self.containerLabel.textColor = UIColor(hexString: "#6F7179")
            self.ltlLabel.textColor = UIColor(hexString: "#6F7179")
            self.containerLine.isHidden = true
            self.ltlLine.isHidden = true
            self.ftlLine.isHidden = false
            // self.saveVehicleImage()
        }else if isFTLClicked == false {
            //            self.containerLabel.textColor = UIColor(hexString: "#6F7179")
            //            self.ltlLabel.textColor = UIColor(hexString: "#6F7179")
            //            self.containerLine.isHidden = true
            //            self.ltlLine.isHidden = true
            //            self.ftlLine.isHidden = false
        }
        
        self.collectionView.reloadData()
    }
    @IBAction func LTLClicked(_ sender: Any) {
        isContainerClicked = false
        isFTLClicked = false
        isLTLClicked = true
        if isLTLClicked == true{
            ltlLabel.textColor = UIColor(hexString: "#0F2541")
            self.FTLLabel.textColor = UIColor(hexString: "#6F7179")
            self.containerLine.isHidden = true
            self.ltlLine.isHidden = false
            self.ftlLine.isHidden = true
            // self.saveVehicleImage()
            self.containerLabel.textColor = UIColor(hexString: "#6F7179")
        }else if isLTLClicked == false{
            
        }
        
        self.collectionView.reloadData()
    }
    
}




extension UIView {
    
    // Example use: myView.addBorder(toSide: .Left, withColor: UIColor.redColor().CGColor, andThickness: 1.0)
    
    enum ViewSide {
        case Left, Right, Top, Bottom
    }
    
    func addBorder(toSide side: ViewSide, withColor color: CGColor, andThickness thickness: CGFloat) {
        
        let border = CALayer()
        border.backgroundColor = color
        
        switch side {
        case .Left: border.frame = CGRect(x: frame.minX, y: frame.minY, width: thickness, height: frame.height); break
        case .Right: border.frame = CGRect(x: frame.maxX, y: frame.minY, width: thickness, height: frame.height); break
        case .Top: border.frame = CGRect(x: frame.minX, y: frame.minY, width: frame.width, height: thickness); break
        case .Bottom: border.frame = CGRect(x: frame.minX, y: frame.maxY, width: frame.width, height: thickness); break
        }
        
        layer.addSublayer(border)
    }
}
extension UITableView {
    func reloadDataWithAutoSizingCellWorkAround() {
        self.reloadData()
        self.setNeedsLayout()
        self.layoutIfNeeded()
        self.reloadData()
    }
}
