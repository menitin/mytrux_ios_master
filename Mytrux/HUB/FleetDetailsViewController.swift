//
//  FleetDetailsViewController.swift
//  Mytrux
//
//  Created by Mytrux on 24/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import SideMenuSwift

class FleetDetailsViewController: BaseViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var fleetId: UILabel!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var daysLabel: UILabel!
    @IBOutlet weak var vehicleType: UILabel!
    @IBOutlet weak var remainingTimeView: UIView!
    @IBOutlet weak var expectedFreightView: UIView!
    @IBOutlet weak var from: UILabel!
    @IBOutlet weak var cargoWt: UILabel!
    @IBOutlet weak var to: UILabel!
    @IBOutlet weak var cargoType: UILabel!
    @IBOutlet weak var loadType: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var availFromDate: UILabel!
    @IBOutlet weak var noOfVehicle: UILabel!
    @IBOutlet weak var inviteFleetBtn: UIView!
    @IBOutlet weak var availFromTime: UILabel!
    @IBOutlet weak var availTillDate: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var availTillTime: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var fleetBtnLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topBarCard(topBar: topBar)
        cardViewArray(arrView: [mainView])
         //    scrollView.delegate = self
        roundedCornerView(viewArray: [remainingTimeView,inviteFleetBtn,expectedFreightView])
        NotificationCenter.default.addObserver(forName: Notification.Name("fleetDetail"), object: nil, queue: nil, using: fleetDetails(notification:))
        self.daysLabel.text = ""
        // Do any additional setup after loading the view.
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        scrollView.contentOffset.x = 0
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        if state == "bookNow"{
             sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookNowViewController") }, with: "34")
             sideMenuController?.setContentViewController(with: "34")
        }else{
        if self.fleetBtnLabel.text == "BACK"{
            sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "MatchingLoadsVC") }, with: "22")
            sideMenuController?.setContentViewController(with: "22")
        }else{
            NotificationCenter.default.post(name: .inviteFleet, object: nil)
            SideMenuController.preferences.basic.defaultCacheKey = "default"
            sideMenuController?.setContentViewController(with: "default")
        }
          }
    }
    
     var timer = Timer()
    var fleetDataArray : FleetDatum?
    var state = String()
     var closeBidTime = Int()
    @objc func fleetDetails(notification: Notification) {
        let userInfo : [String:Any] = notification.userInfo as! [String:Any]
        let fleet = userInfo["fleetDetails"] as! FleetDatum
       let statee = userInfo["state"] as? String
        self.state = statee!
        fleetId.text = fleet.bidCode
        vehicleType.text = fleet.vehicleType.name
        to.text = fleet.destinationLocation
        from.text = fleet.availableLocation
        let timestap: Double = Double(fleet.availableDateTime / 1000)
        let availableFromtime = self.epochTime(epochTimee: timestap)
        let availableFromdate = self.epochDate(epochDate: timestap)
        let timestamp: Double = Double(fleet.closedBidTime / 1000)
        let availableTilltime = self.epochTime(epochTimee: timestamp)
        let availableTilldate = self.epochDate(epochDate: timestamp)
        availFromDate.text = availableFromdate
        availTillDate.text = availableTilldate
        availFromTime.text = availableFromtime
        availTillTime.text = availableTilltime
        let cargoWtString = "\(fleet.capacityWeight)" + ".0" + " " + "\( fleet.capacityWeightUnit.name )"
        cargoWt.text = cargoWtString
        loadType.text = fleet.loadType.loadType
        noOfVehicle.text = String(fleet.noOfVehicles)
        cargoType.text = fleet.cargoType.cargoType
        comment.text = fleet.comments
        
        if fleet.expectedFreight != nil {
            amountLabel.text = String(Double(truncating: fleet.expectedFreight!)) + "0"
        }else{
            amountLabel.text = "00.00"
        }
       
        
        closeBidTime = fleet.closedBidTime
   
        DispatchQueue.global().async(execute: {
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
            //self.seconds = time
            
            RunLoop.main.add(self.timer, forMode: .common)
            RunLoop.current.run()
        })
        fleetBtnLabel.text = "BACK"
        //self.fleetDataArray = fleet

        
    }

    
    @objc func updateTimer() {
        let time = Int64(closeBidTime) - Date().toMillis()
        
        let sec = time/1000
        let min = sec/60
        let hrs = min/60
        let days = hrs/24
        let finalSec = sec % 60
        let finalMin = min % 60
        let finalHrs = hrs % 24
        
        DispatchQueue.main.async {
            //            self.remainingHrsLabel.text = "\(days)" + " days " + "\(finalHrs):" + "\(finalMin): + "\(finalSec)" + "hrs"
            
            if (time > 0) {
                if (days >= 1) {
                    self.timeLabel.text = "\(days)" + " " + "days"
                        + " " + "\(finalHrs)" + ":" + "\(finalMin)"
                        + ":" + "\(finalSec)" + " " + "hrs"
                    
                }else if(days == 0 && finalHrs != 0) {
                    self.timeLabel.text = "\(finalHrs)" + ":" + "\(finalMin)"
                        + ":" + "\(finalSec)" + " " + "hrs"
                    self.timeLabel.textColor = UIColor(hexString: ColorConstants.RED)
                }else if(finalHrs == 0 && finalMin <= 59) {
                    self.timeLabel.text = "\(finalHrs)" + ":" + "\(finalMin)"
                        + ":" + "\(finalSec)" + " " + "min"
                    self.timeLabel.textColor = UIColor(hexString: ColorConstants.RED)
                }else if(finalMin == 0 && finalSec <= 59) {
                    self.timeLabel.text = "\(finalHrs)" + ":" + "\(finalMin)"
                        + ":" + "\(finalSec)" + " " + "sec"
                    self.timeLabel.textColor = UIColor(hexString: ColorConstants.RED)
                }
            } else {
                self.timeLabel.text = "CLOSED"
                self.timeLabel.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
            }
            
        }
        
    }
    
    
    
    @IBAction func fleetInvited(_ sender: Any) {
        
        
        if self.fleetBtnLabel.text == "BACK"{
            
            if  state == "bookNow" {
                 sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookNowViewController") }, with: "34")
                sideMenuController?.setContentViewController(with: "34")
            }else{
                sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "MatchingLoadsVC") }, with: "22")
                sideMenuController?.setContentViewController(with: "22")
            }
            
        }else{
            NotificationCenter.default.post(name: .inviteFleet, object: nil)
            SideMenuController.preferences.basic.defaultCacheKey = "default"
            sideMenuController?.setContentViewController(with: "default")
        }
        
  
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
