//
//  BookingNoteViewController.swift
//  Mytrux
//
//  Created by Mytrux on 24/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import WebKit
class BookingNoteViewController: BaseViewController,UIWebViewDelegate {

   
    @IBOutlet weak var webViewWrraper: UIView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var ewayBillView: UIView!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var titlee: UILabel!
    @IBOutlet weak var manualLRView: UIView!
    var state = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        topBarCard(topBar: topBar)
         NotificationCenter.default.addObserver(self, selector: #selector(self.eLR(_:)), name: .eLR, object: nil)
        webView.delegate = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backbtn(_ sender: Any) {
        if state == "payment"{
               sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "WalletViewController") }, with: "fifth")
            sideMenuController?.setContentViewController(with: "fifth")
        }else{
            sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookingController") }, with: "tenth")
                 sideMenuController?.setContentViewController(with: "tenth")
        }
        
    }
   
    
    @objc func eLR(_ notification: NSNotification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        
        let eLR_url = (userInfo!["url"] as! String)
        self.state = (userInfo!["state"] as! String)
        if state == "payment"{
            self.titlee.text = "Receipt"
            self.ewayBillView.isHidden = true
            self.manualLRView.isHidden = true
        }else{
            self.titlee.text = "Booking Note"
            self.ewayBillView.isHidden = false
            self.manualLRView.isHidden = false
        }
        print("eLR_url ====== \(eLR_url)")
        let url = URL(string: eLR_url)
        self.activityIndicatorBegin()
        let requestObj = URLRequest(url: url!)
        webView.loadRequest(requestObj)
        self.activityIndicatorEnd()
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if let request = webView.request {
            let response = URLCache.shared.cachedResponse(for: request)
            print("responseeese ------- \(String(describing: response))")
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
