//
//  SearchLoadCell.swift
//  Mytrux
//
//  Created by Aboli on 13/06/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class SearchLoadCell: UICollectionViewCell {
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var vehicleName: UILabel!
    @IBOutlet weak var vehicleImage: UIImageView!
   
    
    override func awakeFromNib() {
        innerView.layoutIfNeeded()
        innerView.layer.cornerRadius = innerView.frame.size.width / 2
        innerView.layer.borderWidth = 1
        innerView.layer.borderColor = UIColor(hexString: "#C0C6CC").cgColor
        innerView.layer.masksToBounds = true
        innerView.clipsToBounds = true
        vehicleImage.layoutIfNeeded()
        vehicleImage.layer.cornerRadius = vehicleName.frame.size.width / 2
        vehicleImage.layer.masksToBounds = true
        vehicleImage.clipsToBounds = true
        vehicleName.adjustsFontSizeToFitWidth = true
        vehicleName.minimumScaleFactor = 0.2
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        vehicleImage.image = UIImage.init(named: "truck4")
        vehicleName.text = ""
    }
}

