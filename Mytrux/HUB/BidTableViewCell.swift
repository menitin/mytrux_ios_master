//
//  BidTableViewCell.swift
//  Mytrux
//
//  Created by Aboli on 12/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class BidTableViewCell: UITableViewCell {

    @IBOutlet weak var callView: UIView!
    @IBOutlet weak var mainVIew: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainVIew.layer.cornerRadius = 5
        mainVIew.layer.shadowColor = UIColor.gray.cgColor
        mainVIew.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        mainVIew.layer.shadowRadius = 5
        mainVIew.layer.shadowOpacity = 0.4
        callView.layer.cornerRadius = 16
        view1.layer.cornerRadius = 5
        view4.layer.cornerRadius = 5
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
