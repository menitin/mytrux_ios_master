//
//  PostFleetCollectionViewCell.swift
//  Mytrux
//
//  Created by Aboli on 25/06/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class PostFleetCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        innerView.layoutIfNeeded()
        innerView.layer.cornerRadius = innerView.frame.size.width / 2
        innerView.layer.borderWidth = 1
        innerView.layer.borderColor = UIColor(hexString: "#C0C6CC").cgColor
        innerView.layer.masksToBounds = true
        innerView.clipsToBounds = true
        imageView.layoutIfNeeded()
        imageView.layer.cornerRadius = imageView.frame.size.width / 2
        imageView.layer.masksToBounds = true
        imageView.clipsToBounds = true
    }

    override func setNeedsLayout() {
        
    }
}
