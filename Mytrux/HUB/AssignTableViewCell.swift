//
//  AssignTableViewCell.swift
//  Mytrux
//
//  Created by Mytrux on 22/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class AssignTableViewCell: UITableViewCell {
    
    @IBOutlet weak var autoImageView: UIImageView!
    @IBOutlet weak var autoBtn: UILabel!
    @IBOutlet weak var manualBtn: UILabel!
    @IBOutlet weak var updateBtn: UIView!
    @IBOutlet weak var vehicleType: UITextField!
    @IBOutlet weak var manualImageView: UIImageView!
    @IBOutlet weak var vehicleNo: UITextField!
    @IBOutlet weak var driverName: UITextField!
    @IBOutlet weak var driverMobileNo: UITextField!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var startKmsView: UIView!
    @IBOutlet weak var containerImage: UIImageView!
    @IBOutlet weak var meter: UILabel!
    @IBOutlet weak var container: UILabel!
    @IBOutlet weak var endKms: UIView!
    @IBOutlet weak var startKmsTF: UITextField!
    @IBOutlet weak var meterImage: UIImageView!
    @IBOutlet weak var endKmsTF: UITextField!
    @IBOutlet weak var containerNoView: UIView!
    @IBOutlet weak var containerNoTF1: UITextField!
    @IBOutlet weak var containerNoTF2: UITextField!
    @IBOutlet weak var startEndKmsStack: UIStackView!
    @IBOutlet weak var updateLabel: UILabel!
    @IBOutlet weak var confirmBtn: UIView!

 
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        startKmsTF.text = ""
        endKmsTF.text = ""
        containerNoTF1.text = ""
        containerNoTF2.text = ""
        vehicleNo.text = ""
        vehicleType.text = ""
        driverMobileNo.text = ""
        driverName.text = ""
        
        mainView.layer.cornerRadius = 5
        mainView.layer.shadowColor = UIColor.gray.cgColor
        mainView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        mainView.layer.shadowRadius = 5
        mainView.layer.shadowOpacity = 0.4
      
        roundedCornerView(viewArr: [updateBtn,confirmBtn])
        
        
        
        // Initialization code
    }

    func roundedCornerView(viewArr:[UIView]){
        for view in viewArr{
            view.layer.cornerRadius = view.frame.height / 2
            view.layer.shadowColor = UIColor.gray.cgColor
            view.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
            view.layer.shadowRadius = 5
            view.layer.shadowOpacity = 0.7
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

