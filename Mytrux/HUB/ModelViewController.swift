//
//  ModelViewController.swift
//  Mytrux
//
//  Created by Mukta Bhuyar Punjabi on 23/05/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import CoreData
protocol imageDelegate {
    func imageData(imaage : UIImage)
}


class ModelViewController: BaseViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    
   
    @IBOutlet weak var cameraGalleryStack: UIStackView!
    @IBOutlet weak var labelWrapper: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cameraWrapperView: UIView!
    var delegate : imageDelegate?
    var isCameraModalPresent = false
    var isSelectModalPresent = false
    @IBOutlet weak var cancelBtn: UIView!
    @IBOutlet weak var galleryView: UIView!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var itemLabel: UILabel!
    var imagee = UIImage()
    var imageArray = [UIImage]()
  var uploadState = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.labelWrapper.isHidden = true
        self.cameraWrapperView.isHidden = true
        isSelectModalPresent = false
        isCameraModalPresent = false
       buttonCardView(button: cancelBtn)
        
        let dismiss = UITapGestureRecognizer(target: self, action: #selector(self.handleDissmiss(_:)))
        self.view.addGestureRecognizer(dismiss)
        self.view.isUserInteractionEnabled = true
        let select = UITapGestureRecognizer(target: self, action: #selector(self.selectTapped(_:)))
        self.itemLabel.addGestureRecognizer(select)
        self.labelWrapper.addSubview(itemLabel)
        self.itemLabel.isUserInteractionEnabled = true
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        if isCameraModalPresent == true {
            self.labelWrapper.isHidden = true
            self.cameraWrapperView.isHidden = false
        }
        else if isSelectModalPresent == true {
            self.labelWrapper.isHidden = false
            self.cameraWrapperView.isHidden = true
        }
    }
    
    @IBAction func galleryClicked(_ sender: Any) {
         openGallery()
    }
    @IBAction func cameraClicked(_ sender: UITapGestureRecognizer) {
      
         openCamera()
    }
    @objc func handleDissmiss(_ sender: UITapGestureRecognizer){
         self.dismiss(animated: true, completion: nil)
    }
    
    @objc func selectTapped(_ sender: UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)

    }
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
   
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
            picker.dismiss(animated:  true, completion: nil)
      
        self.imagee = chosenImage
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"step1VC") as! Step1RegisterVC
        if uploadState == 1{
            let image = chosenImage.resized(withPercentage: 0.1)
            saveImage(imageName: "profilePhoto", image: image!)
             viewController.imagePicked = chosenImage
        }else if uploadState == 2 {
            let image = chosenImage.resized(withPercentage: 0.1)
            saveImage(imageName: "panPhoto", image: image!)
            viewController.imageClicked = chosenImage
        }
        viewController.uploadClicked = uploadState
       
        viewController.imageArray = imageArray
        viewController.isImageHidden = false
       // NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userDocumentImages"), object: nil)
        

         self.dismiss(animated: true, completion: nil)
        self.present(viewController, animated: true, completion: nil)

    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  true, completion: nil)
    }
    
    func saveImage(imageName: String, image: UIImage) {
        
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        
        let fileName = imageName
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        guard let data = image.jpegData(compressionQuality: 1) else { return }
        
        //Checks if file exists, removes it if so.
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(atPath: fileURL.path)
                print("Removed old image")
            } catch let removeError {
                print("couldn't remove file at path", removeError)
            }
            
        }
        
        do {
            try data.write(to: fileURL)
        } catch let error {
            print("error saving file with error", error)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if segue.identifier == "step1" {
            
            let next = segue.destination as! Step1RegisterVC
            
            next.cameraImageView.image = imagee
            
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UserDefaults {
    func imageForKey(key: String) -> UIImage? {
        var image: UIImage?
        if let imageData = data(forKey: key) {
            image = NSKeyedUnarchiver.unarchiveObject(with: imageData) as? UIImage
        }
        return image
    }
    func setImage(image: UIImage?, forKey key: String) {
        var imageData: NSData?
        if let image = image {
            imageData = NSKeyedArchiver.archivedData(withRootObject: image) as NSData?
        }
        set(imageData, forKey: key)
    }
}


