//
//  LoadDetailViewController.swift
//  Mytrux
//
//  Created by Aboli on 09/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import SideMenuSwift

extension Date {
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}
class LoadDetailViewController: BaseViewController,UIScrollViewDelegate {
  
    @IBOutlet weak var instructionView: UIView!
    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var bidNumber3: UILabel!
    @IBOutlet weak var bidNumber2: UILabel!
    @IBOutlet weak var bid3Label: UILabel!
    @IBOutlet weak var bid1Label: UILabel!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var bidNumber1: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var rankView: UIView!
    @IBOutlet weak var bidCircle1: UIView!
    @IBOutlet weak var bidCircle2: UIView!
    @IBOutlet weak var bid2Label: UILabel!
    @IBOutlet weak var bidCircle3: UIView!
    @IBOutlet weak var remainingTimeView: UIView!
    @IBOutlet weak var lowestBidView: UIView!
    @IBOutlet weak var menuButton: UIView!
    @IBOutlet weak var submitQuoteButton: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var vehicleTypeLabel: UILabel!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var loadTypeLabel: UILabel!
    @IBOutlet weak var noOfVehicleLabel: UILabel!
    @IBOutlet weak var cargoTypeLabel: UILabel!
    @IBOutlet weak var cargoWeightLabel: UILabel!
    @IBOutlet weak var availableFromDateLabel: UILabel!
    @IBOutlet weak var availableFromTimeLabel: UILabel!
    @IBOutlet weak var availableTillDateLabel: UILabel!
    @IBOutlet weak var availableTillTimeLabel: UILabel!
    @IBOutlet weak var remainingDaysLabel: UILabel!
    @IBOutlet weak var remainingHrsLabel: UILabel!
    @IBOutlet weak var lowestBitLabel: UILabel!
    @IBOutlet weak var submitBtnLabel: UILabel!
    
    var timer = Timer()
    var loadArray = [Load]()
    var seconds : Int64 = 0
    var vcc : LoadDetails? = nil
    let vc = MapViewController()
    override func viewDidLoad() {
    super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //         NotificationCenter.default.addObserver(self, selector: #selector(self.loadObj(notification:)), name: Notification.Name("loadObj"), object: nil)
        sideMenuController?.clearCache(with: "seventh")
        NotificationCenter.default.addObserver(forName: Notification.Name("loadObj"), object: nil, queue: nil, using: loadObj(notification:))
        scrollView.delegate = self
        cardViewArray(arrView: [mainView])
        topBarCard(topBar: topBar)
        self.remainingTimeView.layer.cornerRadius = self.remainingTimeView.frame.height/2
        self.lowestBidView.layer.cornerRadius = self.lowestBidView.frame.height/2
        buttonCardView(button: submitQuoteButton)
        self.remainingDaysLabel.isHidden = true
//        self.idLabel.text = userDefaults.value(forKey: LoadDetails.id)! as? String
//        self.vehicleTypeLabel.text = userDefaults.value(forKey: LoadDetails.vehicleType)! as? String
//        self.fromLabel.text = userDefaults.value(forKey: LoadDetails.from)! as? String
//        self.toLabel.text = userDefaults.value(forKey: LoadDetails.to)! as? String
//        self.loadTypeLabel.text = userDefaults.value(forKey: LoadDetails.loadType)! as? String
//        self.noOfVehicleLabel.text = userDefaults.value(forKey: LoadDetails.noOfVehicles)! as? String
//        let lowestBid = userDefaults.value(forKey: LoadDetails.lowestBid)! as? String
//        self.lowestBitLabel.text = lowestBid!
//        self.bid1Label.text = lowestBid!
//        self.cargoTypeLabel.text = userDefaults.value(forKey: LoadDetails.cargoType)! as? String
//        self.cargoWeightLabel.text = "\((userDefaults.value(forKey: LoadDetails.cargoWeight)! as? String)! )" + " " + "\( (userDefaults.value(forKey: LoadDetails.kgName)! as? String)! )" + " / " + "\((userDefaults.value(forKey: LoadDetails.packgs)! as? String)!)"
//
//        self.availableFromDateLabel.text = userDefaults.value(forKey: LoadDetails.availablefromdate)! as? String
//        self.availableFromTimeLabel.text = userDefaults.value(forKey: LoadDetails.availablefromtime)! as? String
//        self.availableTillDateLabel.text = userDefaults.value(forKey: LoadDetails.availabletilldate)! as? String
//        self.availableTillTimeLabel.text = userDefaults.value(forKey: LoadDetails.availabletilltime)! as? String
//        self.loadArray = vc.loadDetailsArray
//
//        let closeBidTime = userDefaults.value(forKey: LoadDetails.closedBidTime)! as! String
//        let closeBidFinalTime = Int64(closeBidTime)! / 1000
//        let Timestamp = Date().toMillis()
//        print("closeBidTime = \(closeBidTime)")
//        print("timestamp = \(String(describing: Timestamp))")
//        let time = Timestamp! - closeBidFinalTime
//        print("time = \(time)")
//        let sec = time/1000
//        print("sec = \(sec)")
//
//        let newTime = epochTime(epochTimee: Double(time))
//        print("newTime = \(newTime)")
//
//        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
//        seconds = time
//        self.remainingHrsLabel.text = newTime + " hrs"
 
    }
  var noOfDays = 0
    @objc func updateTimer() {
        let time = Int64(closeBidTime) - Date().toMillis()
    
        let sec = time/1000
        let min = sec/60
        let hrs = min/60
        let days = hrs/24
        let finalSec = sec % 60
        let finalMin = min % 60
        let finalHrs = hrs % 24
        
        DispatchQueue.main.async {
//            self.remainingHrsLabel.text = "\(days)" + " days " + "\(finalHrs):" + "\(finalMin): + "\(finalSec)" + "hrs"
            
            if (time > 0) {
                if (days >= 1) {
                    self.remainingHrsLabel.text = "\(days)" + " " + "days"
                        + " " + "\(finalHrs)" + ":" + "\(finalMin)"
                        + ":" + "\(finalSec)" + " " + "hrs"
                
                }else if(days == 0 && finalHrs != 0) {
                    self.remainingHrsLabel.text = "\(finalHrs)" + ":" + "\(finalMin)"
                        + ":" + "\(finalSec)" + " " + "hrs"
                    self.remainingHrsLabel.textColor = UIColor(hexString: ColorConstants.RED)
                }else if(finalHrs == 0 && finalMin <= 59) {
                     self.remainingHrsLabel.text = "\(finalHrs)" + ":" + "\(finalMin)"
                        + ":" + "\(finalSec)" + " " + "min"
                   self.remainingHrsLabel.textColor = UIColor(hexString: ColorConstants.RED)
                }else if(finalMin == 0 && finalSec <= 59) {
                    self.remainingHrsLabel.text = "\(finalHrs)" + ":" + "\(finalMin)"
                        + ":" + "\(finalSec)" + " " + "sec"
            self.remainingHrsLabel.textColor = UIColor(hexString: ColorConstants.RED)
                }
            } else {
                self.remainingHrsLabel.text = "CLOSED"
                 self.remainingHrsLabel.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
            }
            
        }
        
    }
    
    func epochTime1(epochTimee:Double)->String{
        
        let timeResult:Double = epochTimee
        let date = NSDate(timeIntervalSince1970: timeResult)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm:s" //Set time style
        
        let timeZone = TimeZone.current.identifier as String
        dateFormatter.timeZone = TimeZone(identifier: timeZone)
        let localDate = dateFormatter.string(from: date as Date)
        return "\(localDate)"
        
    }
    func epochHrs(epochhr:Double)->String{
        
        let timeResult:Double = epochhr
        let date = NSDate(timeIntervalSince1970: timeResult)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h" //Set time style
        
        let timeZone = TimeZone.current.identifier as String
        dateFormatter.timeZone = TimeZone(identifier: timeZone)
        let localDate = dateFormatter.string(from: date as Date)
        return "\(localDate)"
        
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
       
            self.timer.invalidate()
      
    }
    var closeBidTime = Int()
   
    var fleet : FleetDatum?
    var status = String()
    
    @objc func loadObj(notification: Notification) {
        let userInfo : [String:Any] = notification.userInfo as! [String:Any]
        let bidCode = userInfo["bidCode"]
        let fleetDetail = userInfo["fleet"] as? FleetDatum
        if fleetDetail != nil{
            self.fleet = fleetDetail
        }
        let statuss = userInfo["status"] as! String
        print("status ======== > \(status)")
        self.status = statuss
        
      
        if statuss == "LoadsQuoted"{
          self.submitQuoteButton.isHidden = true
                    self.instructionView.isHidden = false
        }else if statuss == "bookNow"{
            self.submitBtnLabel.text = "BACK"
            self.submitQuoteButton.isHidden = false
            self.instructionView.isHidden = true
        }else if statuss == "book"{
              self.submitBtnLabel.text = "SUBMIT QUOTE"
                      self.submitQuoteButton.isHidden = false
                      self.instructionView.isHidden = true
        }else{
            self.submitBtnLabel.text = "SUBMIT QUOTE"
            self.submitQuoteButton.isHidden = false
            self.instructionView.isHidden = true
        }
        
        
        print("id = \(String(describing: bidCode!))")
        var quotationAmountArray = [Int]()
                 self.lowestBitLabel.text = "NA"
        let latitude = userInfo["latitude"] as! Double
        let longitude = userInfo["longitude"] as! Double
        let vehicleTypeId = userInfo["vehicleTypeId"] as! Int
        let loadTypeId = userInfo["loadTypeId"] as! Int
        
        print("vehicleTypeId ==== \(vehicleTypeId)")
        print("loadTypeId ==== \(loadTypeId)")
     let bidId = userInfo["id"] as! Int
         print("bidId ====>>>>> \(bidId)")
       
        self.bid1Label.text = "NA"
        self.bid2Label.text = "NA"
        self.bid3Label.text = "NA"
        self.bidCircle1.backgroundColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
        self.bidCircle2.backgroundColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
        self.bidCircle3.backgroundColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
        if let ownLastThreeBids = userInfo["ownLastThreeBids"] as! [AvailableLoadBidQuotaion]?{
            quotationAmountArray.removeAll()
            for bid in ownLastThreeBids{
                print("bid === \(bid.quotationAmount)")
                quotationAmountArray.append(bid.quotationAmount)
            }
            quotationAmountArray = quotationAmountArray.sorted()
            print("quotationAmountArray.count >>>>> === \(quotationAmountArray.count)")
            switch quotationAmountArray.count {
            case 0:
                self.bid1Label.text = "NA"
                self.bid2Label.text = "NA"
                self.bid3Label.text = "NA"
                self.bidCircle1.backgroundColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
                self.bidCircle2.backgroundColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
                self.bidCircle3.backgroundColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
                break
            case 1:
                self.bid1Label.text = String(quotationAmountArray[0]) + ".00"
                 self.lowestBitLabel.text = String(quotationAmountArray[0]) + ".00"
                  self.bidCircle1.backgroundColor = UIColor(hexString: ColorConstants.LIGHT_BLUE)
            case 2:
                self.bid1Label.text = String(quotationAmountArray[0]) + ".00"
                self.lowestBitLabel.text = String(quotationAmountArray[0]) + ".00"
                self.bidCircle1.backgroundColor = UIColor(hexString: ColorConstants.LIGHT_BLUE)
            self.bidCircle2.backgroundColor = UIColor(hexString: ColorConstants.LIGHT_BLUE)
                 self.bid2Label.text = String(quotationAmountArray[1]) + ".00"
            case 3:
                self.bid1Label.text = String(quotationAmountArray[0]) + ".00"
                self.lowestBitLabel.text = String(quotationAmountArray[0]) + ".00"
                self.bidCircle1.backgroundColor = UIColor(hexString: ColorConstants.LIGHT_BLUE)
                self.bidCircle2.backgroundColor = UIColor(hexString: ColorConstants.LIGHT_BLUE)
                self.bid2Label.text = String(quotationAmountArray[1]) + ".00"
                self.bidCircle3.backgroundColor = UIColor(hexString: ColorConstants.LIGHT_BLUE)
                 self.bid3Label.text = String(quotationAmountArray[2]) + ".00"
            default:
                 self.bid1Label.text = "NA"
                 self.bid2Label.text = "NA"
                 self.bid3Label.text = "NA"
            }
        }

       print("userInfo = \(userInfo)")
        self.idLabel.text = (bidCode as! String)
        self.vehicleTypeLabel.text = (userInfo["vehicleType"] as! String)
        self.fromLabel.text = (userInfo["from"] as! String)
        self.toLabel.text = (userInfo["to"] as! String)
        self.loadTypeLabel.text = (userInfo["loadType"] as! String)
        self.noOfVehicleLabel.text = String(userInfo["noOfVech"] as! Int)
       
        self.cargoTypeLabel.text = (userInfo["cargoType"] as! String)
        let cargoWtString = "\(String(describing: (userInfo["cargoWt"] as! Int)))" + ".0" + " " + "\( (userInfo["kgName"] as! String) )" + " / " + "\(userInfo["packgs"] as? Int)"
        self.cargoWeightLabel.text = cargoWtString
        self.rankLabel.text = String(userInfo["bidRank"] as! Int)
        self.availableFromDateLabel.text = (userInfo["availableFromdate"] as! String)
        self.availableFromTimeLabel.text = (userInfo["availableFromtime"] as! String)
        self.availableTillDateLabel.text = (userInfo["availableTilldate"] as! String)
        self.availableTillTimeLabel.text = (userInfo["availableTilltime"] as! String)
        self.loadArray = vc.loadDetailsArray
        closeBidTime = (userInfo["closedBidTime"] as! Int)
     //   let closeBidFinalTime = Int64(closeBidTime) / 1000
       
    
        
        
        DispatchQueue.global().async(execute: {
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
            //self.seconds = time
          
            RunLoop.main.add(self.timer, forMode: .common)
          RunLoop.current.run()
        })
       
       // self.remainingHrsLabel.text = newTime + " hrs"
        
        
        
        if let availableLoadBidQuotaionElement = userInfo["availableLoadBidQuotaionElement"] as? AvailableLoadBidQuotaionElement?{
            
            if availableLoadBidQuotaionElement?.id != nil{
                print("availableLoadBidQuotaionElement != nil")
                
//                self.availableLoadQuotation = ["availableLoadBidQuotaionElement": availableLoadBidQuotaionElement,"latitude":latitude,"longitude":longitude,"vehicleTypeId":vehicleTypeId,"loadTypeId":loadTypeId,"loadObj":userInfo,"cargoWt": cargoWtString, "noOfVech":userInfo["noOfVech"] as! Int,"availableFromDate":userInfo["availableFromdate"] as! String,"availableFromtime": userInfo["availableFromtime"] as! String,"loadBidId":bidId]
                
                
                self.availableLoadQuotation = ["availableLoadBidQuotaionElement": availableLoadBidQuotaionElement!,"loadObj":userInfo,"cargoWt": cargoWtString,"loadBidId":bidId,"closeBidTime":self.closeBidTime,"status":self.status,"fleetDetail":fleet]

                
            }else{
                print("availableLoadBidQuotaionElement = nil")
                
                self.availableLoadQuotation = ["availableLoadBidQuotaionElement": availableLoadBidQuotaionElement!,"loadObj":userInfo,"cargoWt": cargoWtString,"loadBidId":bidId,"closeBidTime":self.closeBidTime,"status":self.status,"fleetDetail":fleet]

                
                
//                self.availableLoadQuotation = ["availableLoadBidQuotaionElement": availableLoadBidQuotaionElement,"latitude":latitude,"longitude":longitude,"vehicleTypeId":vehicleTypeId,"loadTypeId":loadTypeId,"loadObj":userInfo, "cargoWt": cargoWtString, "noOfVech":userInfo["noOfVech"] as! Int,"availableFromDate":userInfo["availableFromdate"] as! String,"availableFromtime": userInfo["availableFromtime"] as! String,"loadBidId":bidId,"loadDetailsDict":userInfo] as [String : Any]
            }
        }
        
    }
    
    var availableLoadQuotation = [String:Any]()
    
    override func viewWillAppear(_ animated: Bool) {
        print("loadArray.count = \(loadArray.count)")
        for load in loadArray{
            let dataArr = load.data
            for data in dataArr{
                print("data.capacityWeightUnit.name = \(data.capacityWeightUnit.name)")
            }
            print("load.data.count = \(load.data.count)")
        }
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        scrollView.contentOffset.x = 0
    }
    
    @IBAction func menuClicked(_ sender: Any) {
       
        if status == "LoadsQuoted"{
            sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "loadsQuotedVC") }, with: "second")
            sideMenuController?.setContentViewController(with: "second")
            
        }else if status == "MatchLoad"{
            sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "MatchingLoadsVC") }, with: "22")
            sideMenuController?.setContentViewController(with: "22")
     
        }else if status == "book" {
             sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookNowViewController") }, with: "34")
            sideMenuController?.setContentViewController(with: "34")
        }else if status == "bookNow" {
             sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookNowViewController") }, with: "34")
            sideMenuController?.setContentViewController(with: "34")
            
        }else{
              SideMenuController.preferences.basic.defaultCacheKey = "default"
             sideMenuController?.setContentViewController(with: "default")
        }
       
    }
    
    override func viewDidLayoutSubviews() {
        self.circularView(viewArray: [cameraView,rankView,bidCircle1,bidCircle2,bidCircle3])
        NotificationCenter.default.post(name: .BackClicked, object: nil)
    }
    
    
    @IBAction func submitQuoteClicked(_ sender: Any) {
        if status == "bookNow" {
             sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookNowViewController") }, with: "34")
            sideMenuController?.setContentViewController(with: "34")
        }
        
         SideMenuController.preferences.basic.defaultCacheKey = "default"
         NotificationCenter.default.post(name: .submitQuote, object: nil)
        sideMenuController?.setContentViewController(with: "default")
//        if self.fleet != nil{
//             print("fleet id ++++++------- \(fleet!.bidCode)")
//            NotificationCenter.default.post(name: Notification.Name("fleet"),object: nil,userInfo: ["fleet":])
//        }else{
        
        print("AvailableLoadBidQuotation id ====----=== \(availableLoadQuotation["availableLoadBidQuotaionElement"])")
               NotificationCenter.default.post(name: Notification.Name("AvailableLoadBidQuotation"),object: nil,userInfo: self.availableLoadQuotation)
     //   }
       
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



