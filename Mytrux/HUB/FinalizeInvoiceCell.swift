//
//  FinalizeInvoiceCell.swift
//  Mytrux
//
//  Created by Mytrux on 14/10/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit


class FinalizeInvoiceCell: UITableViewCell {
    
    @IBOutlet weak var editImage: UIImageView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var billingDetailView: UIView!
    @IBOutlet weak var billDescription: UILabel!
    @IBOutlet weak var checkMarkView: UIView!
    @IBOutlet weak var unit: UILabel!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var rate: UILabel!
    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var taxCheckBox: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
