//
//  LegalViewController.swift
//  Mytrux
//
//  Created by Aboli on 09/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import SideMenuSwift

class LegalViewController: BaseViewController {

    @IBOutlet weak var disclaimerView: UIView!
    @IBOutlet weak var cookiesView: UIView!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var privacyPolicy: UIView!
    @IBOutlet weak var termsView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cardViewArray(arrView: [disclaimerView,cookiesView,termsView,privacyPolicy])
        topBarCard(topBar: topBar)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func cookiesClicked(_ sender: Any) {
        let url = URLStatics.cookies
       
        sideMenuController?.setContentViewController(with: "32")
         NotificationCenter.default.post(name: .legal, object: nil,userInfo: ["url" :url])
    }
    @IBAction func disclaimerClicked(_ sender: Any) {
          let url = URLStatics.disclaimer
          sideMenuController?.setContentViewController(with: "32")
                 NotificationCenter.default.post(name: .legal, object: nil,userInfo: ["url" : url])
    }
    @IBAction func menuButton(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    @IBAction func termsAndConditionClciked(_ sender: Any) {
        let url = URLStatics.termsCondition
          sideMenuController?.setContentViewController(with: "32")
               NotificationCenter.default.post(name: .legal, object: nil,userInfo: ["url" : url])
    }
    
    @IBAction func privacyPolicyClicked(_ sender: Any) {
        
        let url = URLStatics.privacyPolicy
          sideMenuController?.setContentViewController(with: "32")
               NotificationCenter.default.post(name: .legal, object: nil,userInfo: ["url" : url])
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
