//
//  SearchViewController.swift
//  Mytrux
//
//  Created by Mytrux on 24/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import SideMenuSwift
import Alamofire
import SwiftyJSON
import Speech

class SearchViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate, SFSpeechRecognizerDelegate{
    
    var  isDragging = false
    var pageNo = 0
    var bookingList = [BookingElement]()
    var status = String()
    var isTrackCLicked = Bool()
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var topbar: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var microPhoneBtn: UIButton!
    
    let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))
    override func viewDidLoad() {
        super.viewDidLoad()
        topBarCard(topBar: topbar)
        tableView.delegate = self
        tableView.dataSource = self
        searchTF.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.search(notification:)), name: .search , object: nil)
        self.tableView.isHidden = true
        self.noDataView.isHidden = false
        searchTF.addTarget(self, action: #selector(self.textFieldDidChange(_:)),
                           for: UIControl.Event.editingChanged)
        
        speechRecognizer!.delegate = self
        SFSpeechRecognizer.requestAuthorization { (authStatus) in  //4
            
            var isButtonEnabled = false
            
            switch authStatus {  //5
            case .authorized:
                isButtonEnabled = true
                
            case .denied:
                isButtonEnabled = false
                print("User denied access to speech recognition")
                
            case .restricted:
                isButtonEnabled = false
                print("Speech recognition restricted on this device")
                
            case .notDetermined:
                isButtonEnabled = false
                print("Speech recognition not yet authorized")
                
                OperationQueue.main.addOperation() {
                    self.microPhoneBtn.isUserInteractionEnabled = isButtonEnabled
                }
            }
            
            // Do any additional setup after loading the view.
        }
    }
    var loadTypeName = String()
    var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    var recognitionTask: SFSpeechRecognitionTask?
    let audioEngine = AVAudioEngine()
    
  
    
    @IBAction func microPhoneClicked(_ sender: Any) {
        if audioEngine.isRunning {
            audioEngine.stop()
            recognitionRequest?.endAudio()
            microPhoneBtn.isEnabled = false
           // microPhoneBtn.setTitle("Start Recording", for: .normal)
        } else {
            startRecording()
           // microPhoneBtn.setTitle("Stop Recording", for: .normal)
        }
    }
    
    
    func startRecording() {
        
        if recognitionTask != nil {
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.record)
            try audioSession.setMode(AVAudioSession.Mode.measurement)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        guard let inputNode = audioEngine.inputNode as? AVAudioInputNode else {
            fatalError("Audio engine has no input node")
        }
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }
        recognitionRequest.shouldReportPartialResults = true
        recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in
            var isFinal = false
            
            if result != nil {
                
                self.searchTF.text = result?.bestTranscription.formattedString
                isFinal = (result?.isFinal)!
                if self.searchTF.text != ""{
                    self.pageNo = 0
                          self.bookingList.removeAll()
                    self.isDragging = true
                    self.myBooking(search:self.searchTF.text!)
                      }else{
                          self.bookingList.removeAll()
                          self.tableView.isHidden = true
                          self.noDataView.isHidden = false
                      }
                
            }
            
            if error != nil || isFinal {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
                self.microPhoneBtn.isEnabled = true
            }
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        
        do {
            try audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
        
        searchTF.text = "Say something, I'm listening!"
        
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if searchTF.text != ""{
            pageNo = 0
            self.bookingList.removeAll()
            myBooking(search:searchTF.text!)
        }else{
            self.bookingList.removeAll()
            self.tableView.isHidden = true
            self.noDataView.isHidden = false
        }
    }
    
    @objc func search(notification: Notification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        let loadType = userInfo!["loadTypeName"] as! String
        self.loadTypeName = loadType
        let isTrack = userInfo!["isTractClicked"] as! Bool
        self.isTrackCLicked = isTrack
        
        if isTrackCLicked {
            status = UserStates.WITHOUTNEW
        }else{
            status = UserStates.ALL
        }
        if searchTF.text != ""{
            self.tableView.isHidden = false
            self.noDataView.isHidden = true
            myBooking(search: "")
        }else{
            self.tableView.isHidden = true
            self.noDataView.isHidden = false
        }
    }
    
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        // print("offsetY = \(offsetY) ======= contentHeight ==== \(contentHeight - scrollView.frame.size.height - 300)")
        
        if offsetY > contentHeight - scrollView.frame.size.height - 300 {
            
            pageNo += 1
            
            isDragging = true
            
            if self.bookingList[0].count! != self.bookingList.count{
                self.myBooking(search: self.searchTF.text!)
            }
            
        }else{
            isDragging = false
            
        }
        
    }
    
    //    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    //        if searchTF.text != ""{
    //            myBooking(search:searchTF.text!)
    //        }
    //
    //        return true
    //    }
    
    
    func myBooking(search:String){
        
//        if isDragging == false{
//            self.activityIndicatorBegin()
//        }
        let parameters : [String:Any]
        
        parameters = [
            "recordsPerPage" : 10,
            "pageNo" : "\(self.pageNo)",
            "search" : search,
            "status" : self.status,
            "financialYear": self.currentFinancialYear!,
            "loadTypeName": self.loadTypeName
        ]
        
        let headers = header()
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
        
        Alamofire.request(URLStatics.bookingList, method:.get,parameters: parameters, headers: headers)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                
                switch (response.result) {
                case .success:
//                    if self.isDragging == false{
//                        self.activityIndicatorEnd()
//                    }
                    if let reply = response.result.value {
                        var vehicleDetails : VehicleContainerDetails1
                        var vehicleDetailsDict = [String:Any]()
                        let mainResponse = JSON(reply)
                        var vehicleType : AssignVehicleType1
                        var documentDict : [String:Any]?
                        var documentSmallDict : [String:Any]?
                        var document : FirstPicID
                        var doucmentSmall : FirstPicID
                        var vehicleTypeDict = [String:Any]()
                        var grossWeightUnit : WeightUnit
                        var chargeableWTUnit : WeightUnit
                        var netWeightUnit : WeightUnit
                        var grosWtUnit = [String:Any]()
                        var chargebleWtUnit = [String:Any]()
                        var netWtUnit = [String:Any]()
                        var jobStatusDict = [String:Any]()
                        var jobStatus : JobStatusClass
                        var cargoType : CargoTypeClass
                        var cargoo = [String:Any]()
                        var loadType : LoadTypeClass
                        var loadTypeDict = [String:Any]()
                        var jobType : JobTypeClass
                        var jobTypeDict = [String:Any]()
                        var bookingType : BookingTypeClass
                        var bookingTypeDict = [String:Any]()
                        var availbaleFleetBid : AvailableBid1?
                        var availFleetBidDict = [String:Any]()
                        var availableLoadBid : AvailableBid1?
                        var availLoadBidDict = [String:Any]()
                        var jobVehiclesDict = [[String:Any]]()
                        var podId : PodID?
                        var podDict : [String:Any]?
                        var jobCVehicleDct : [String:Any]?
                        var jobVehicleTYpe : VehicleType?
                        var loadOwner : LoadOwner1?
                        var fleetOwner : FleetOwner1?
                        var loadOwnerDict : [String:Any]?
                        var fleetOwnerDict : [String:Any]?
                        var shipper : Shipper?
                        var loadOwnerShipperDict = [String:Any]()
                        var loadOwnerShipper : LoadOwner1?
                        var consignee : Consignee?
                        var loadOwnerConsignee : LoadOwner1?
                        var loadownerConsigneeDict = [String:Any]()
                        var shipperDict = [String:Any]()
                        var consigneeDict = [String:Any]()
                        var deliveryCustomerDict = [String:Any]()
                        var deliveryCustomer : Consignee?
                        var loadOwnerDeliveryCustomer : LoadOwner1?
                        var loadownerDeliveryCustomerDict = [String:Any]()
                        var freightTerm : FreightTermsClass?
                        let freightTermDict = [String:Any]()
                        var exTaxInfo : ExTaxInfo?
                        var taxGroupDict = [String:Any]()
                        var taxGroup: TaxGroup?
                        var taxDetailArrDict = [[String:Any]]()
                        var taxDict = [[String:Any]]()
                        print("======= JSON: bookingList ======== \(mainResponse)")
                        
                        for data in mainResponse.arrayValue{
                            
                            var jobVehiclesDrivers = [JobVehiclesDriver]()
                            var taxDetailArray = [TaxDetail]()
                            var tax = [Tax]()
                            
                            if let returnObject =
                                data["jobVehiclesDrivers"].arrayObject as? [[String:Any]] {
                                jobVehiclesDict = returnObject
                            }
                            
                            
                            
                            for job in jobVehiclesDict{
                                
                                podDict = job["podId"] as? [String:Any]
                                podId = PodID(id: podDict?["id"] as? Int, mimeType: podDict?["mimeType"] as? String)
                                let logo = Logo(id: nil, mimeType: nil, type: nil)
                                jobCVehicleDct = job["vehicleType"] as? [String:Any]
                                jobVehicleTYpe = VehicleType(id: jobCVehicleDct!["id"] as! Int,
                                                             name: jobCVehicleDct!["name"] as! String,
                                                             sequence: Int(jobCVehicleDct!["sequence"] as! NSNumber),
                                                             container: jobCVehicleDct!["container"] as! Bool,
                                                             type: jobCVehicleDct!["type"] as! String,
                                                             document: logo,
                                                             documentSmall: logo,
                                                             vehicleOrder: jobCVehicleDct!["vehicleOrder"] as! Int,
                                                             weightCapacity: jobCVehicleDct!["weightCapacity"] as? Int,
                                                             documentID: jobCVehicleDct!["documentID"] as? Int,
                                                             smallDocID: jobCVehicleDct!["smallDocID"] as? Int)
                                
                                let jobDriver = JobVehiclesDriver(id: job["id"] as? Int, availableFleetBid: nil, vehicleRegistrationNo: job["vehicleRegistrationNo"] as? String, driverFullName: job["driverFullName"] as? String, mobileNo: job["mobileNo"] as? String, startKM: job["startKM"] as? Int, endKM: job["endKM"] as? Int, user: nil, allocationType: job["allocationType"] as? String, createTime: job["createTime"] as? Int, vehicleType: jobVehicleTYpe, jobStatus: nil, lrNo: job["lrNo"] as? Int, uploadPodTime: job["uploadPodTime"] as? Int, podID: podId, exManualLrNo: job["exManualLrNo"] as? String, exManualLrDate: job["exManualLrDate"] as? Int)
                                
                                jobVehiclesDrivers.append(jobDriver)
                                print("podDict id ====== \(String(describing: podDict?["id"]))")
                                
                            }
                            
                            
                            let yourString = data["exTaxInfo"].stringValue
                            var exTaxInfojson: JSON
                            let dataToConvert = yourString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                            do {
                                exTaxInfojson = try JSON(data: dataToConvert!)
                            } catch _ {
                                exTaxInfojson = ""
                            }
                            
                            taxGroupDict = exTaxInfojson["taxGroup"].dictionaryObject as! [String:Any]
                            
                            taxGroup = TaxGroup(taxGroupDescription: taxGroupDict["taxGroupDescription"] as? String, tax: taxGroupDict["tax"] as? Int, id: taxGroupDict["id"] as? Int, status: taxGroupDict["status"] as? Bool, defaultType: taxGroupDict["defaultType"] as? String)
                            
                            
                            for taxx in exTaxInfojson["taxDetails"].arrayValue{
                                print("taxName \(taxx["taxName"].stringValue)")
                                let taxInfo = TaxDetail(taxAmount: taxx["taxAmount"].int, taxName: taxx["taxName"].string, taxRate: taxx["taxRate"].double, taxType: taxx["taxType"].string)
                                taxDetailArray.append(taxInfo)
                            }
                            for taxx in exTaxInfojson["taxs"].arrayValue{
                                var taxmasterdict = taxx["taxMaster"].dictionaryValue
                                let taxMaster = TaxMaster(id: taxmasterdict["id"]?.int, taxName: taxmasterdict["taxName"]?.string, taxType: taxmasterdict["taxType"]?.string)
                                let taxInfo = Tax(taxMaster: taxMaster, taxType: taxx["taxType"].string, id: taxx["id"].int, tax: taxx["tax"].double, status: taxx["status"].bool, taxName: taxx["taxName"].string)
                                tax.append(taxInfo)
                            }
                            
                            
                            print("exTaxInfojson string: " + String(describing: exTaxInfojson))
                            
                            print("id booking list ==== \(data["id"].stringValue)")
                            
                            shipperDict = data["shipper"].dictionaryObject as! [String:Any]
                            consigneeDict = data["consignee"].dictionaryObject as! [String:Any]
                            deliveryCustomerDict = data["deliveryCustomer"].dictionaryObject as! [String:Any]
                            loadOwnerShipperDict = shipperDict["loadOwner"] as! [String:Any]
                            loadownerConsigneeDict = consigneeDict["loadOwner"] as! [String:Any]
                            loadownerDeliveryCustomerDict = deliveryCustomerDict["loadOwner"] as! [String:Any]
                            fleetOwnerDict = data["fleetOwner"].dictionaryObject as! [String:Any]
                            
                            vehicleDetailsDict = data["vehicleContainerDetails"].dictionaryObject as! [String:Any]
                            loadOwnerDict = data["loadOwner"].dictionaryObject as! [String:Any]
                            fleetOwnerDict = data["fleetOwner"].dictionaryObject as! [String:Any]
                            availLoadBidDict = data["availableLoadBid"].dictionaryObject as! [String:Any]
                            availFleetBidDict = data["availableFleetBid"].dictionaryObject as! [String:Any]
                            vehicleTypeDict = vehicleDetailsDict["vehicleType"] as! [String:Any]
                            cargoo = data["cargoType"].dictionaryObject as! [String:Any]
                            cargoType = CargoTypeClass(id: cargoo["id"] as! Int, cargoType: cargoo["cargoType"] as! String)
                            
                            loadTypeDict = data["loadType"].dictionaryObject as! [String:Any]
                            jobTypeDict = data["jobType"].dictionaryObject as! [String:Any]
                            jobType = JobTypeClass(id: jobTypeDict["id"] as? Int, jobType: jobTypeDict["jobType"] as? String)
                            loadType = LoadTypeClass(id: loadTypeDict["id"] as! Int, loadType: loadTypeDict["loadType"] as! String, code: loadTypeDict["code"] as! String)
                            
                            bookingTypeDict = data["bookingType"].dictionaryObject as! [String:Any]
                            bookingType = BookingTypeClass(id: bookingTypeDict["id"] as? Int, bookingType: bookingTypeDict["bookingType"] as? String)
                            
                            
                            
                            
                            documentDict = vehicleTypeDict["document"] as? [String:Any]
                            documentSmallDict = vehicleTypeDict["documentSmall"] as? [String:Any]
                            document = FirstPicID(id: documentDict?["id"] as? Int, mimeType: documentDict?["mimeType"] as? String, type: documentDict?["type"] as? String)
                            doucmentSmall = FirstPicID(id: documentSmallDict?["id"] as? Int, mimeType: documentSmallDict?["mimeType"] as? String, type: documentSmallDict?["type"] as? String)
                            grosWtUnit = vehicleDetailsDict["grossWeightUnit"] as! [String:Any]
                            chargebleWtUnit = vehicleDetailsDict["chargeableWeightUnit"] as! [String:Any]
                            netWtUnit = vehicleDetailsDict["netWeightUnit"] as! [String:Any]
                            
                            grossWeightUnit = WeightUnit(id: grosWtUnit["id"] as! Int, name: grosWtUnit["name"] as! String, type: grosWtUnit["type"] as! String)
                            chargeableWTUnit = WeightUnit(id: chargebleWtUnit["id"] as! Int, name: chargebleWtUnit["name"] as! String, type: chargebleWtUnit["type"] as! String)
                            jobStatusDict = data["jobStatus"].dictionaryObject as! [String:Any]
                            jobStatus = JobStatusClass(id: jobStatusDict["id"] as! Int, jobStatus: jobStatusDict["jobStatus"] as! String)
                            
                            netWeightUnit = WeightUnit(id: netWtUnit["id"] as! Int, name: netWtUnit["name"] as! String, type: netWtUnit["type"] as! String)
                            vehicleType = AssignVehicleType1(id: vehicleTypeDict["id"] as? Int, name: vehicleTypeDict["name"] as? String, sequence: vehicleTypeDict["sequence"] as? Double, container: vehicleTypeDict["container"] as? Bool, type: vehicleTypeDict["type"] as? String, document: document, documentSmall: doucmentSmall, vehicleOrder: vehicleTypeDict["vehicleOrder"] as? Int, documentID: vehicleTypeDict["documentId"] as? Int, smallDocID: vehicleTypeDict["smallDocId"] as? Int, weightCapacity: vehicleTypeDict["weightCapacity"] as? Int)
                            
                            availableLoadBid = AvailableBid1(id: availLoadBidDict["id"] as? Int,
                                                             
                                                             bidCode: availLoadBidDict["bidCode"] as? String,
                                                             
                                                             creationDate: availLoadBidDict["creationDate"] as? Int,
                                                             
                                                             closedBidTime: availLoadBidDict["closedBidTime"] as? Int,
                                                             
                                                             availableLocation: availLoadBidDict["availableLocation"] as? String,
                                                             destinationLocation: availLoadBidDict["destinationLocation"] as? String,
                                                             comments: availLoadBidDict["comments"] as? String,
                                                             availableLatitude: availLoadBidDict["availableLatitude"] as? Double,
                                                             availableLongitude: availLoadBidDict["availableLongitude"] as? Double,
                                                             destinationLatitude: availLoadBidDict["destinationLatitude"] as? Double,
                                                             destinationLongitude: availLoadBidDict["destinationLongitude"] as? Double,
                                                             vehicleType:nil, noOfVehicles: availLoadBidDict["noOfVehicles"] as? Int,
                                                             availableDateTime: nil,
                                                             cargoType: nil,
                                                             capacityWeight:  Int(truncating: availLoadBidDict["capacityWeight"] as! NSNumber),
                                                             capacityWeightUnit:nil,
                                                             loadType: nil,
                                                             expectedFreight: availLoadBidDict["expectedFreight"] as? Int,
                                                             currencyMaster: nil,
                                                             status:availLoadBidDict["status"] as? String,
                                                             registrationNo: nil,
                                                             postType: nil,
                                                             bidInviteCount: nil, bidQuotationCount: nil,
                                                             containerNo: availLoadBidDict["containerNo"] as? String,
                                                             sealNo: availLoadBidDict["sealNo"] as? String, pinNo: nil, packgs: availLoadBidDict["packgs"] as? Int)
                            
                            
                            
                            
                            availbaleFleetBid = AvailableBid1(id: availFleetBidDict["id"] as? Int,
                                                              bidCode: availFleetBidDict["bidCode"] as? String,
                                                              creationDate: availFleetBidDict["creationDate"] as? Int,
                                                              closedBidTime: availFleetBidDict["closedBidTime"] as? Int,
                                                              availableLocation: availFleetBidDict["availableLocation"] as? String,
                                                              destinationLocation: availFleetBidDict["destinationLocation"] as? String,
                                                              comments: availFleetBidDict["comments"] as? String,
                                                              availableLatitude: availFleetBidDict["availableLatitude"] as? Double,
                                                              availableLongitude: availFleetBidDict["availableLongitude"] as? Double,
                                                              destinationLatitude: availFleetBidDict["destinationLatitude"] as? Double,
                                                              destinationLongitude: availFleetBidDict["destinationLongitude"] as? Double,
                                                              vehicleType: nil, noOfVehicles: availFleetBidDict["noOfVehicles"] as? Int,
                                                              availableDateTime: availFleetBidDict["availableDateTime"] as? Int,
                                                              cargoType: nil,
                                                              capacityWeight:  availFleetBidDict["capacityWeight"] as? Int,
                                                              capacityWeightUnit: nil ,
                                                              loadType: nil,
                                                              expectedFreight: nil ,
                                                              currencyMaster: nil,
                                                              status: availFleetBidDict["status"] as? String,
                                                              registrationNo: availFleetBidDict["registrationNo"] as? String,
                                                              postType: availFleetBidDict["postType"] as? String,
                                                              bidInviteCount: availFleetBidDict["bidInviteCount"] as? Int, bidQuotationCount: nil,
                                                              containerNo: availFleetBidDict["containerNo"] as? String,
                                                              sealNo: availFleetBidDict["sealNo"] as? String, pinNo: nil, packgs: availFleetBidDict["packgs"] as? Int)
                            
                            
                            
                            vehicleDetails = VehicleContainerDetails1(id: vehicleDetailsDict["id"] as! Int,
                                                                      vehicleType: vehicleType,
                                                                      noOfVehicles: vehicleDetailsDict["noOfVehicles"] as! Int,
                                                                      grossWeight: Int(truncating: vehicleDetailsDict["grossWeight"] as! NSNumber),
                                                                      grossWeightUnit: grossWeightUnit,
                                                                      chargeableWeight: Int(truncating: vehicleDetailsDict["chargeableWeight"] as! NSNumber),
                                                                      chargeableWeightUnit: chargeableWTUnit,
                                                                      netWeight: Int(truncating: vehicleDetailsDict["netWeight"] as! NSNumber),
                                                                      netWeightUnit: netWeightUnit)
                            
                            
                            loadOwner = LoadOwner1(id: loadOwnerDict!["id"] as? Int, name: loadOwnerDict!["name"] as? String, registrationNo: loadOwnerDict!["registrationNo"] as? String, companyCode: loadOwnerDict!["companyCode"] as? String, telephone: loadOwnerDict!["telephone"] as? String, fax: loadOwnerDict!["fax"] as? String, address: nil, typeOfCompany: nil, logo: nil, gstTax: loadOwnerDict!["gstTax"] as? Double, gstTaxRegNo: loadOwnerDict!["gstTaxRegNo"] as? String, jurisdiction: loadOwnerDict!["jurisdiction"] as? String, timeZone: loadOwnerDict!["timeZone"] as? String, panNo: loadOwnerDict!["panNo"] as? String, vatNo: loadOwnerDict!["vatNo"] as? String, iecNo: loadOwnerDict!["iecNo"] as? String, cinNo: loadOwnerDict!["cinNo"] as? String, cstNo: loadOwnerDict!["cstNo"] as? String, registrationDate: loadOwnerDict!["registrationDate"] as? Int, subscriptionStartDate: loadOwnerDict!["subscriptionStartDate"] as? Int, subscriptionEndDate: loadOwnerDict!["subscriptionEndDate"] as? Int, currentBalance: loadOwnerDict!["currentBalance"] as? Int, smsNotification: loadOwnerDict!["smsNotification"] as? String, companyAccess: nil, loadAdditionalInformation: nil, status: loadOwnerDict!["status"] as? String, expiredType: loadOwnerDict!["expiredType"] as? String, activeUserCount: loadOwnerDict!["activeUserCount"] as? Int, maxUser: loadOwnerDict!["maxUser"] as? Int, maxUserType: loadOwnerDict!["maxUserType"] as? String, exchangeBookingCount: loadOwnerDict!["exchangeBookingCount"] as? Int, exWalletValidFrom: loadOwnerDict!["exWalletValidFrom"] as? Int, exWalletValidTill: loadOwnerDict!["exWalletValidTill"] as? Int, exWalletValidDays: loadOwnerDict!["exWalletValidDays"] as? Int, exPlanType: loadOwnerDict!["exPlanType"] as? String, goCount: loadOwnerDict!["goCount"] as? Int, proCount: loadOwnerDict!["proCount"] as? Int, profileMailCount: loadOwnerDict!["profileMailCount"] as? Int, ewbFlagStr: loadOwnerDict!["ewbFlagStr"] as? String, planType: loadOwnerDict!["planType"] as? String, exSuwalletAmount: loadOwnerDict!["exSuwalletAmount"] as? Int, exSuhubMaxUser: loadOwnerDict!["exSuhubMaxUser"] as? Int, exSuallowTransaction: loadOwnerDict!["exSuallowTransaction"] as? Int, exSuremainsTr: loadOwnerDict!["exSuremainsTr"] as? Int, hubPlanID: loadOwnerDict!["hubPlanID"] as? Int, exSuhubTransactionCost: loadOwnerDict!["exSuhubTransactionCost"] as? Int, exPackType: loadOwnerDict!["exPackType"] as? String)
                            
                            
                            fleetOwner = FleetOwner1(id: fleetOwnerDict!["id"] as? Int, name: fleetOwnerDict!["name"] as? String, registrationNo: fleetOwnerDict!["registrationNo"] as? String, panNo: fleetOwnerDict!["panNo"] as? String, fax: fleetOwnerDict!["fax"] as? String, telephone: fleetOwnerDict!["telephone"] as? String, typeOfCompany: nil, fleetAdditionalInformation: nil, address: nil, userCount: fleetOwnerDict!["userCount"] as? Int, activeUserCount: fleetOwnerDict!["activeUserCount"] as? Int, companyCode: fleetOwnerDict!["companyCode"] as? String, logo: nil, gstTax: fleetOwnerDict!["gstTax"] as? Int, gstTaxRegNo: fleetOwnerDict!["gstTaxRegNo"] as? String, jurisdiction: fleetOwnerDict!["jurisdiction"] as? String, timeZone: fleetOwnerDict!["timeZone"] as? String, vatNo: fleetOwnerDict!["vatNo"] as? String, iecNo: fleetOwnerDict!["iecNo"] as? String, cinNo: fleetOwnerDict!["cinNo"] as? String, cstNo: fleetOwnerDict!["cstNo"] as? String, registrationDate: fleetOwnerDict!["registrationDate"] as? Int, expiredType: fleetOwnerDict!["expiredType"] as? String, subscriptionStartDate: fleetOwnerDict!["subscriptionStartDate"] as? Int, subscriptionEndDate: fleetOwnerDict!["subscriptionEndDate"] as? Int, perTransactionFee: fleetOwnerDict!["perTransactionFee"] as? Int, smsCharge: fleetOwnerDict!["smsCharge"] as? Int, currentBalance: fleetOwnerDict!["currentBalance"] as? Int, bookingCount: fleetOwnerDict!["bookingCount"] as? Int, purchasePaymentCount: fleetOwnerDict!["purchasePaymentCount"] as? Int, purchaseReceiptCount: fleetOwnerDict!["purchaseReceiptCount"] as? Int, routeCount: fleetOwnerDict!["routeCount"] as? Int, smsNotification: fleetOwnerDict!["smsNotification"] as? String, maxUser: fleetOwnerDict!["maxUser"] as? Int, maxUserType: fleetOwnerDict!["maxUserType"] as? String, status: fleetOwnerDict!["status"] as? String, contractMasterCount: fleetOwnerDict!["contractMasterCount"] as? Int, goCount: fleetOwnerDict!["goCount"] as? Int, proCount: fleetOwnerDict!["proCount"] as? Int, profileMailCount: fleetOwnerDict!["profileMailCount"] as? Int, ewbFlagStr: fleetOwnerDict!["ewbFlagStr"] as? String, walletAmount: fleetOwnerDict!["walletAmount"] as? Int, hubMaxUser: fleetOwnerDict!["hubMaxUser"] as? Int, allowTransaction: fleetOwnerDict!["allowTransaction"] as? Int, remainsTr: fleetOwnerDict!["remainsTr"] as? Int, hubPlanID: fleetOwnerDict!["hubPlanID"] as? Int, hubTransactionCost: fleetOwnerDict!["hubTransactionCost"] as? Int, thirdPartyAuthDateTime: fleetOwnerDict!["thirdPartyAuthDateTime"] as? Int, exPurchasePayCount: fleetOwnerDict!["exPurchasePayCount"] as? Int, exPurchaseReceiptCount: fleetOwnerDict!["exPurchaseReceiptCount"] as? Int, exWalletValidFrom: fleetOwnerDict!["exWalletValidFrom"] as? Int, exWalletValidTill: fleetOwnerDict!["exWalletValidTill"] as? Int, exWalletValidDays: fleetOwnerDict!["exWalletValidDays"] as? Int, exPlanType: fleetOwnerDict!["exPlanType"] as? String, exPackType: fleetOwnerDict!["exPackType"] as? String, userCounts: fleetOwnerDict!["userCounts"] as? Int)
                            
                            loadOwnerShipper = LoadOwner1(id: loadOwnerShipperDict["id"] as? Int, name: loadOwnerShipperDict["name"] as? String, registrationNo: loadOwnerShipperDict["registrationNo"] as? String, companyCode: loadOwnerShipperDict["companyCode"] as? String, telephone: loadOwnerShipperDict["telephone"] as? String, fax: loadOwnerShipperDict["fax"] as? String, address: nil, typeOfCompany: nil, logo: nil, gstTax: loadOwnerShipperDict["gstTax"] as? Double, gstTaxRegNo: loadOwnerShipperDict["gstTaxRegNo"] as? String, jurisdiction: loadOwnerShipperDict["jurisdiction"] as? String, timeZone: loadOwnerShipperDict["timeZone"] as? String, panNo: loadOwnerShipperDict["panNo"] as? String, vatNo: loadOwnerShipperDict["vatNo"] as? String, iecNo: loadOwnerShipperDict["iecNo"] as? String, cinNo: loadOwnerShipperDict["cinNo"] as? String, cstNo: loadOwnerShipperDict["cstNo"] as? String, registrationDate: loadOwnerShipperDict["registrationDate"] as? Int, subscriptionStartDate: loadOwnerShipperDict["subscriptionStartDate"] as? Int, subscriptionEndDate: loadOwnerShipperDict["subscriptionEndDate"] as? Int, currentBalance: loadOwnerShipperDict["currentBalance"] as? Int, smsNotification: loadOwnerShipperDict["smsNotification"] as? String, companyAccess: nil, loadAdditionalInformation: nil, status: loadOwnerShipperDict["status"] as? String, expiredType: loadOwnerShipperDict["expiredType"] as? String, activeUserCount: loadOwnerShipperDict["activeUserCount"] as? Int, maxUser: loadOwnerShipperDict["maxUser"] as? Int, maxUserType: loadOwnerShipperDict["maxUserType"] as? String, exchangeBookingCount: loadOwnerShipperDict["exchangeBookingCount"] as? Int, exWalletValidFrom: loadOwnerShipperDict["exWalletValidFrom"] as? Int, exWalletValidTill: loadOwnerShipperDict["exWalletValidTill"] as? Int, exWalletValidDays: loadOwnerShipperDict["exWalletValidDays"] as? Int, exPlanType: loadOwnerShipperDict["exPlanType"] as? String, goCount: loadOwnerShipperDict["goCount"] as? Int, proCount: loadOwnerShipperDict["proCount"] as? Int, profileMailCount: loadOwnerShipperDict["profileMailCount"] as? Int, ewbFlagStr: loadOwnerShipperDict["ewbFlagStr"] as? String, planType: loadOwnerShipperDict["planType"] as? String, exSuwalletAmount: loadOwnerShipperDict["exSuwalletAmount"] as? Int, exSuhubMaxUser: loadOwnerShipperDict["exSuhubMaxUser"] as? Int, exSuallowTransaction: loadOwnerShipperDict["exSuallowTransaction"] as? Int, exSuremainsTr: loadOwnerShipperDict["exSuremainsTr"] as? Int, hubPlanID: loadOwnerShipperDict["hubPlanID"] as? Int, exSuhubTransactionCost: loadOwnerShipperDict["exSuhubTransactionCost"] as? Int, exPackType: loadOwnerShipperDict["exPackType"] as? String)
                            
                            loadOwnerConsignee = LoadOwner1(id: loadownerConsigneeDict["id"] as? Int, name: loadownerConsigneeDict["name"] as? String, registrationNo: loadownerConsigneeDict["registrationNo"] as? String, companyCode: loadownerConsigneeDict["companyCode"] as? String, telephone: loadownerConsigneeDict["telephone"] as? String, fax: loadownerConsigneeDict["fax"] as? String, address: nil, typeOfCompany: nil, logo: nil, gstTax: loadownerConsigneeDict["gstTax"] as? Double, gstTaxRegNo: loadownerConsigneeDict["gstTaxRegNo"] as? String, jurisdiction: loadownerConsigneeDict["jurisdiction"] as? String, timeZone: loadownerConsigneeDict["timeZone"] as? String, panNo: loadownerConsigneeDict["panNo"] as? String, vatNo: loadownerConsigneeDict["vatNo"] as? String, iecNo: loadownerConsigneeDict["iecNo"] as? String, cinNo: loadownerConsigneeDict["cinNo"] as? String, cstNo: loadownerConsigneeDict["cstNo"] as? String, registrationDate: loadownerConsigneeDict["registrationDate"] as? Int, subscriptionStartDate: loadownerConsigneeDict["subscriptionStartDate"] as? Int, subscriptionEndDate: loadownerConsigneeDict["subscriptionEndDate"] as? Int, currentBalance: loadownerConsigneeDict["currentBalance"] as? Int, smsNotification: loadownerConsigneeDict["smsNotification"] as? String, companyAccess: nil, loadAdditionalInformation: nil, status:loadownerConsigneeDict["status"] as? String, expiredType: loadownerConsigneeDict["expiredType"] as? String, activeUserCount: loadownerConsigneeDict["activeUserCount"] as? Int, maxUser: loadownerConsigneeDict["maxUser"] as? Int, maxUserType: loadownerConsigneeDict["maxUserType"] as? String, exchangeBookingCount: loadownerConsigneeDict["exchangeBookingCount"] as? Int, exWalletValidFrom: loadownerConsigneeDict["exWalletValidFrom"] as? Int, exWalletValidTill: loadownerConsigneeDict["exWalletValidTill"] as? Int, exWalletValidDays: loadownerConsigneeDict["exWalletValidDays"] as? Int, exPlanType: loadownerConsigneeDict["exPlanType"] as? String, goCount: loadownerConsigneeDict["goCount"] as? Int, proCount: loadownerConsigneeDict["proCount"] as? Int, profileMailCount: loadownerConsigneeDict["profileMailCount"] as? Int, ewbFlagStr: loadownerConsigneeDict["ewbFlagStr"] as? String, planType: loadownerConsigneeDict["planType"] as? String, exSuwalletAmount: loadownerConsigneeDict["exSuwalletAmount"] as? Int, exSuhubMaxUser: loadownerConsigneeDict["exSuhubMaxUser"] as? Int, exSuallowTransaction: loadownerConsigneeDict["exSuallowTransaction"] as? Int, exSuremainsTr: loadownerConsigneeDict["exSuremainsTr"] as? Int, hubPlanID: loadownerConsigneeDict["hubPlanID"] as? Int, exSuhubTransactionCost: loadownerConsigneeDict["exSuhubTransactionCost"] as? Int, exPackType: loadownerConsigneeDict["exPackType"] as? String)
                            
                            loadOwnerDeliveryCustomer = LoadOwner1(id: loadownerDeliveryCustomerDict["id"] as? Int, name: loadownerDeliveryCustomerDict["name"] as? String, registrationNo: loadownerDeliveryCustomerDict["registrationNo"] as? String, companyCode: loadownerDeliveryCustomerDict["companyCode"] as? String, telephone: loadownerDeliveryCustomerDict["telephone"] as? String, fax: loadownerDeliveryCustomerDict["fax"] as? String, address: nil, typeOfCompany: nil, logo: nil, gstTax: loadownerDeliveryCustomerDict["gstTax"] as? Double, gstTaxRegNo: loadownerDeliveryCustomerDict["gstTaxRegNo"] as? String, jurisdiction: loadownerDeliveryCustomerDict["jurisdiction"] as? String, timeZone: loadownerDeliveryCustomerDict["timeZone"] as? String, panNo: loadownerDeliveryCustomerDict["panNo"] as? String, vatNo: loadownerDeliveryCustomerDict["vatNo"] as? String, iecNo: loadownerDeliveryCustomerDict["iecNo"] as? String, cinNo: loadownerDeliveryCustomerDict["cinNo"] as? String, cstNo: loadownerDeliveryCustomerDict["cstNo"] as? String, registrationDate: loadownerDeliveryCustomerDict["registrationDate"] as? Int, subscriptionStartDate: loadownerDeliveryCustomerDict["subscriptionStartDate"] as? Int, subscriptionEndDate: loadownerDeliveryCustomerDict["subscriptionEndDate"] as? Int, currentBalance: loadownerDeliveryCustomerDict["currentBalance"] as? Int, smsNotification: loadownerDeliveryCustomerDict["smsNotification"] as? String, companyAccess: nil, loadAdditionalInformation: nil, status:loadownerDeliveryCustomerDict["status"] as? String, expiredType: loadownerDeliveryCustomerDict["expiredType"] as? String, activeUserCount: loadownerDeliveryCustomerDict["activeUserCount"] as? Int, maxUser: loadownerDeliveryCustomerDict["maxUser"] as? Int, maxUserType: loadownerDeliveryCustomerDict["maxUserType"] as? String, exchangeBookingCount: loadownerDeliveryCustomerDict["exchangeBookingCount"] as? Int, exWalletValidFrom: loadownerDeliveryCustomerDict["exWalletValidFrom"] as? Int, exWalletValidTill: loadownerDeliveryCustomerDict["exWalletValidTill"] as? Int, exWalletValidDays: loadownerDeliveryCustomerDict["exWalletValidDays"] as? Int, exPlanType: loadownerDeliveryCustomerDict["exPlanType"] as? String, goCount: loadownerDeliveryCustomerDict["goCount"] as? Int, proCount: loadownerDeliveryCustomerDict["proCount"] as? Int, profileMailCount: loadownerDeliveryCustomerDict["profileMailCount"] as? Int, ewbFlagStr: loadownerDeliveryCustomerDict["ewbFlagStr"] as? String, planType: loadownerDeliveryCustomerDict["planType"] as? String, exSuwalletAmount: loadownerDeliveryCustomerDict["exSuwalletAmount"] as? Int, exSuhubMaxUser: loadownerDeliveryCustomerDict["exSuhubMaxUser"] as? Int, exSuallowTransaction: loadownerDeliveryCustomerDict["exSuallowTransaction"] as? Int, exSuremainsTr: loadownerDeliveryCustomerDict["exSuremainsTr"] as? Int, hubPlanID: loadownerDeliveryCustomerDict["hubPlanID"] as? Int, exSuhubTransactionCost: loadownerDeliveryCustomerDict["exSuhubTransactionCost"] as? Int, exPackType: loadownerDeliveryCustomerDict["exPackType"] as? String)
                            
                            
                            shipper = Shipper(id: shipperDict["id"] as? Int, name: shipperDict["name"] as? String, address: shipperDict["address"] as? String, location: shipperDict["location"] as? String, state: shipperDict["state"] as? String, contactPerson: shipperDict["contactPerson"] as? String, contactNo: shipperDict["contactNo"] as? String, loadOwner: loadOwnerShipper)
                            
                            consignee = Consignee(id: consigneeDict["id"] as? Int, name: consigneeDict["name"] as? String, address: consigneeDict["address"] as? String, location: consigneeDict["location"] as? String, state: consigneeDict["state"] as? String, contactPerson: consigneeDict["contactPerson"] as? String, contactNo: consigneeDict["contactNo"] as? String, loadOwner: loadOwnerConsignee, gstNo: consigneeDict["gstNo"] as? String, pinNo: consigneeDict["pinNo"] as? String, country: consigneeDict["country"] as? String, createDate: consigneeDict["createDate"] as? Int, updateDate: consigneeDict["updateDate"] as? Int, secAddress: consigneeDict["secAddress"] as? String, contactEmail: consigneeDict["contactEmail"] as? String, cinNo: consigneeDict["cinNo"] as? String, panNo: consigneeDict["panNo"] as? String)
                            
                            deliveryCustomer = Consignee(id: deliveryCustomerDict["id"] as? Int, name: deliveryCustomerDict["name"] as? String, address: deliveryCustomerDict["address"] as? String, location: deliveryCustomerDict["location"] as? String, state: deliveryCustomerDict["state"] as? String, contactPerson: deliveryCustomerDict["contactPerson"] as? String, contactNo: deliveryCustomerDict["contactNo"] as? String, loadOwner: loadOwnerDeliveryCustomer, gstNo: deliveryCustomerDict["gstNo"] as? String, pinNo: deliveryCustomerDict["pinNo"] as? String, country: deliveryCustomerDict["country"] as? String, createDate: deliveryCustomerDict["createDate"] as? Int, updateDate: deliveryCustomerDict["updateDate"] as? Int, secAddress: deliveryCustomerDict["secAddress"] as? String, contactEmail: deliveryCustomerDict["contactEmail"] as? String, cinNo: deliveryCustomerDict["cinNo"] as? String, panNo: deliveryCustomerDict["panNo"] as? String)
                            
                            freightTerm = FreightTermsClass(id: freightTermDict["id"] as? Int, freightTerms: freightTermDict["freightTerms"] as? String)
                            
                            exTaxInfo = ExTaxInfo(taxDetails: taxDetailArray, taxGroup: taxGroup, taxs: tax, taxType: exTaxInfojson["taxType"].string)
                            
                            let booking = BookingElement(id: data["id"].int, shipper: shipper, consignee: consignee, deliveryCustomer: deliveryCustomer, loadOwner: loadOwner, fleetOwner: fleetOwner, billingParty: data["billingParty"].stringValue, jobStatus: jobStatus, loadType: loadType, cargoType: cargoType, jobType: jobType, bookingType: bookingType, pickupDateTime: data["pickupDateTime"].int, deliveryDateTime: data["deliveryDateTime"].int, freightTerms: freightTerm, bookingNo: data["bookingNo"].stringValue, availableLoadBid: availableLoadBid, availableFleetBid: availbaleFleetBid, loadBidQuotaion: nil, pickUpLocation: data["pickUpLocation"].stringValue, destination: data["destination"].stringValue, createDate: data["createDate"].int, vehicleContainerDetails: vehicleDetails, user: nil, bookingCust: data["bookingCust"].bool, shiper: data["shiper"].bool, consigneer: data["consigneer"].bool, deliverCust: data["deliverCust"].bool, exInvoiceGrossAmount: data["exInvoiceGrossAmount"].int, exInvoiceGrossAmountWithoutTax: data["exInvoiceGrossAmountWithoutTax"].int, exTaxType: data["exTaxType"].stringValue, exTaxInfo: exTaxInfo , exInvoiceNo: data["exInvoiceNo"].stringValue, exTaxGroup: nil, invoiceDate: data["invoiceDate"].int, financeYear: data["financeYear"].stringValue, salesBillFinal: data["salesBillFinal"].bool, purchaseBillFinal: data["purchaseBillFinal"].bool, defaultBankAndBranch: data["defaultBankAndBranch"].stringValue, count: data["count"].intValue, lrNo: data["lrNo"].int, isinvite: data["isinvite"].bool, eLrTerms: data["eLrTerms"].stringValue, salesInvoiceTerms: data["salesInvoiceTerms"].stringValue, jobVehiclesDrivers: jobVehiclesDrivers, trips: nil, fleetBidInvite: nil)
                            
                            print("jobId: ========----- \(data["id"].int)")
                            if self.bookingList.contains(where: { boooking in boooking.id == booking.id  }) {
                                
                            } else {
                                
                                self.bookingList.append(booking)
                            }
                            
                        }
                        
                        //                        self.assignDetailArray.removeAll()
                        //                        for booking in self.bookingList{
                        //                            let assign = AssignDatum(id: booking.id, jobStatus: booking.jobStatus, loadType: booking.loadType, cargoType: booking.cargoType, pickupDateTime: booking.pickupDateTime, deliveryDateTime: booking.deliveryDateTime, bookingNo: booking.bookingNo, pickUpLocation: booking.pickUpLocation, destination: booking.destination, createDate: booking.createDate, vehicleContainerDetails: booking.vehicleContainerDetails, bookingCust: booking.bookingCust, shiper: booking.shiper, consigneer: booking.consigneer, deliverCust: booking.deliverCust, salesBillFinal: booking.salesBillFinal, purchaseBillFinal: booking.purchaseBillFinal, count: booking.count, lrNo: booking.lrNo, fleetBidCode: booking.availableFleetBid?.bidCode, loadBidCode: booking.availableLoadBid?.bidCode, inviteAmount: Double(booking.availableFleetBid?.expectedFreight ?? Int(0.0)), fleetBidID: booking.availableFleetBid?.id, isinvite: booking.isinvite, registrationNo: booking.availableFleetBid?.registrationNo, telephone: booking.loadOwner?.telephone, packgs: booking.availableFleetBid?.packgs, quoteAmount: booking.loadBidQuotaion?.quotationAmount)
                        //
                        //                            self.assignDetailArray.append(assign)
                        //                        }
                        //
                        //                        print("assignDetailArray ======= \(self.assignDetailArray.count)")
                        self.tableView.isHidden = false
                        self.noDataView.isHidden = false
                        self.tableView.reloadData()
                    }
                    break
                case .failure(let error):
                    
                    print("== error == \(error.localizedDescription)")
                    break
                }
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bookingList.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookingController") }, with: "tenth")
        
        sideMenuController?.setContentViewController(with: "tenth")
        NotificationCenter.default.post(name: .searchData ,object: nil,userInfo: ["booking" : self.bookingList[indexPath.row]])
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "search", for: indexPath) as! SearchTableViewCell
        if bookingList.count != 0 {
            
            
            let bookingList = self.bookingList[indexPath.item]
            cell.id.text = bookingList.availableLoadBid?.bidCode
            cell.bookingNo.text = bookingList.bookingNo
            let timeStamp = Double(bookingList.pickupDateTime! / 1000)
            let pickupdate = self.epochDate(epochDate: timeStamp)
            let pickupTime = self.epochTime(epochTimee: timeStamp)
            cell.pickupTime.text = pickupTime
            cell.pickupDate.text = pickupdate
            if isTrackCLicked == true{
                cell.button.backgroundColor = UIColor(hexString: ColorConstants.LIGHT_BLUE)
                cell.buttonLabel.text = "TRACK"
            }else{
                
                if bookingList.jobStatus?.jobStatus == "Assigned" || bookingList.jobStatus?.jobStatus == "In Progress"{
                    cell.button.backgroundColor = UIColor(hexString: ColorConstants.YELLOW)
                    cell.buttonLabel.text = "ASSIGNED"
                }else if bookingList.jobStatus?.jobStatus == "New Booking"{
                    cell.button.backgroundColor = UIColor(hexString: ColorConstants.GREEN)
                    cell.buttonLabel.text = "ASSIGN"
                    
                }else if bookingList.jobStatus?.jobStatus == "Completed"{
                    cell.button.backgroundColor = UIColor(hexString: ColorConstants.RED)
                    cell.buttonLabel.text = "COMPLETED"
                    
                }
            }
            
        }
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170
        
    }
    
    @IBAction func backClicked(_ sender: Any) {
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookingController") }, with: "tenth")
        sideMenuController?.setContentViewController(with: "tenth")
        self.searchTF.text = ""
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
