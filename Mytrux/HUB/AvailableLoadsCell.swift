//
//  AvailableLoadsCell.swift
//  Mytrux
//
//  Created by Mytrux on 24/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class AvailableLoadsCell: UICollectionViewCell {
    
    @IBOutlet weak var circularView: UIView!
    @IBOutlet weak var mainView: UIView!
    
    override func awakeFromNib() {
        circularView.layoutIfNeeded()
        circularView.layer.cornerRadius = circularView.frame.size.width / 2
        circularView.layer.borderWidth = 1
        circularView.layer.borderColor = UIColor(hexString: "#C0C6CC").cgColor
        circularView.layer.masksToBounds = true
        circularView.clipsToBounds = true
    }
    
    override func setNeedsLayout() {
        self.mainView.layoutIfNeeded()
    }
    
}
