//
//  CustomerViewController.swift
//  Mytrux
//
//  Created by Mytrux on 18/11/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import SideMenuSwift
import Alamofire
import SwiftyJSON
class CustomerViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var pendingImageView: UIImageView!
    @IBOutlet weak var pendingLabel: UILabel!
    @IBOutlet weak var pendingView: UIView!
    @IBOutlet weak var activeImageView: UIImageView!
    @IBOutlet weak var activeView: UIView!
    @IBOutlet weak var activeLabel: UILabel!
    @IBOutlet weak var othersView: UIView!
    @IBOutlet weak var othersImageView: UIImageView!
    @IBOutlet weak var othersLabel: UILabel!
    @IBOutlet weak var customerTableView: UITableView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchBar: UIView!
    @IBOutlet weak var searchBtn: UIView!
    @IBOutlet weak var addImageView: UIImageView!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var menuBtn: UIView!
    var isActiveClicked = false
    var isPendingClicked = false
    var isOthersClicked = false
    var isDragging = false
    
    var status = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.clearCache(with: "35")
        customerTableView.delegate = self
        customerTableView.dataSource = self
        topBarCard(topBar: topBar)
        cardViewArray(arrView: [searchBar])
        isActiveClicked = true
        isPendingClicked = false
        isOthersClicked = false
        isDragging = false
        self.status = "ACTIVE"
        toggleViews(mainView: activeView, view1: pendingView, view2: othersView, mainLabel: activeLabel, label1: pendingLabel, label2: othersLabel)
        toggleImage(mainImage: activeImageView, image1: pendingImageView, image2: othersImageView, mainImageName: "accepted", imageName1: "pending", imageName2: "others")
        self.customer(search: "")
        self.addImageView.isHidden = false
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func addBtnClicked(_ sender: Any) {
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "AddCustomerViewController") }, with: "37")
        sideMenuController?.setContentViewController(with: "37")
        
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        // print("offsetY = \(offsetY) ======= contentHeight ==== \(contentHeight - scrollView.frame.size.height - 300)")
        
        if offsetY > contentHeight - scrollView.frame.size.height - 300 {
            
            pageNo += 1
            
            isDragging = true
            
            if self.customerArray[0].count! != self.customerArray.count{
                self.customer(search: self.searchTF.text!)
            }
            
        }else{
            isDragging = false
            
        }
        
    }
    
    
    @IBAction func menuClicked(_ sender: Any) {
        sideMenuController?.revealMenu()
        
    }
    
    @IBAction func activeClicked(_ sender: Any) {
        if isActiveClicked == false{
            toggleViews(mainView: activeView, view1: pendingView, view2: othersView, mainLabel: activeLabel, label1: pendingLabel, label2: othersLabel)
            toggleImage(mainImage: activeImageView, image1: pendingImageView, image2: othersImageView, mainImageName: "accepted", imageName1: "pending", imageName2: "others")
            isActiveClicked = true
            isPendingClicked = false
            isOthersClicked = false
            
        }else{
            
        }
        self.customerArray.removeAll()
        self.status = "ACTIVE"
        pageNo = 0
        self.searchTF.text = ""
        self.customer(search: "\(searchTF.text!)")
        self.addImageView.isHidden = false
        self.customerTableView.reloadData()
    }
    
    
    var pageNo = 0
    var customerArray = [FavouriteCompany]()
    var customerDic : Customer?
    func customer(search:String){
        var parameters = [String:Any]()
        parameters = [
            "recordsPerPage" : 20,
            "pageNo" : "\(self.pageNo)",
            "search" : "\(search)",
            "financialYear": self.currentFinancialYear!
        ]
        
        //+918669696004
        //asdfgf1234$
        //8888870909
        //asdfgf1234$
        
        let headers = header()
        if isDragging == false{
            self.activityIndicatorBegin()
        }
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
        
        
        Alamofire.request(URLStatics.customer + "\(status)", method: .get, parameters: parameters,headers: headers)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                
                
                switch (response.result) {
                    
                case .success:
                    if self.isDragging == false{
                        self.activityIndicatorEnd()
                    }
                    if let reply = response.result.value {
                        print("======= JSON: Customer ========  \(reply)")
                        let mainResponse = JSON(reply)
                        print("======= JSON: mainResponse ========  \(mainResponse["data"]["favouriteCompany"][0]["name"])")
                        let data = mainResponse["data"]
                        var favDict = [[String:Any]]()
                        favDict = data["favouriteCompany"].arrayObject as! [[String:Any]]
                        var addr : [String:Any]?
                        var contactPersonn = [CustomerContactPerson]()
                        for favComp in favDict{
                            addr = favComp["address"] as? [String:Any]
                            let addres = CustomerAddress(id: addr?["id"] as? Int,
                                                         address: addr?["address"] as? String,
                                                         city: addr?["city"] as? String,
                                                         state: addr?["state"] as? String,
                                                         pincode: addr?["pincode"] as? String,
                                                         country: addr?["country"] as? String,
                                                         latitude: addr?["latitude"] as? Double,
                                                         longitude: addr?["longitude"] as? Double)
                            //                            let contPersonArr = favComp["contactPersons"] as? [[String:Any]]
                            var contactArray = [[String:Any]]()
                            
                            if let contPersonArr = favComp["contactPersons"] as? [[String:Any]] {
                                contactArray = contPersonArr
                            }
                            
                            
                            let typeOfComay = favComp["typeOfCompany"] as? [String:Any]
                            let typeOfCompany = TypeOfCompany1(id: typeOfComay?["id"] as? Int, companyType: typeOfComay?["companyType"] as? String, companyCategories: typeOfComay?["companyType"] as? String)
                            
                            for cont in contactArray{
                                print("full Name ==== \(String(describing: cont["fullName"] as? String))")
                                let contact = CustomerContactPerson(id: cont["id"] as? Int, fullName: cont["fullName"] as? String, contactNo: cont["contactNo"] as? String, email: cont["email"] as? String, loadOwnerID: cont["loadOwnerId"] as? Int)
                                print("contact ==== \(String(describing: contact))")
                                contactPersonn.append(contact)
                            }
                            
                            
                            let fav = FavouriteCompany(id: favComp["id"] as? Int, name: favComp["name"] as? String, address: addres, contactPersons: contactPersonn, fax: favComp["fax"] as? String, email: favComp["email"] as? String, gstTaxRegNo: favComp["gstTaxRegNo"] as? String, companyStatus: favComp["companyStatus"] as? String, code: favComp["code"] as? String, registrationDate: favComp["registrationDate"] as? Int, hubStatus: favComp["hubStatus"] as? String, count: favComp["count"] as? Int, telephone: favComp["telephone"] as? String, typeOfCompany: typeOfCompany, panNo: favComp["panNo"] as? String, cinNo: favComp["cinNo"] as? String, companyCode: favComp["companyCode"] as? String)
                            self.customerArray.append(fav)
                        }
                        
                        print("contactPersonn.count ===== \(contactPersonn.count)")
                        
                        let customerr = Customer(favouriteCompany: nil, activeCount: data["activeCount"].int, pendingCount: data["pendingCount"].int, inactiveCount: data["inactiveCount"].int, manualCount: data["manualCount"].int, blackListActiveCount: data["blackListActiveCount"].int, blackListInactiveCount: data["blackListInactiveCount"].int, totalcount: data["totalcount"].int)
                        self.customerDic = customerr
                        print("======= customerArray ======")
                        print(self.customerArray.count)
                        self.customerTableView.reloadData()
                    }
                    break
                case .failure(let error):
                    if self.isDragging == false{
                        self.activityIndicatorEnd()
                    }
                    print("error \(error.localizedDescription)")
                    break
                }
        }
        
    }
    
    @IBAction func pendingClicked(_ sender: Any) {
        
        if isPendingClicked == false{
            toggleViews(mainView: pendingView, view1: activeView, view2: othersView, mainLabel: pendingLabel, label1: activeLabel, label2: othersLabel)
            toggleImage(mainImage: pendingImageView, image1: activeImageView, image2: othersImageView, mainImageName: "pending", imageName1: "accepted", imageName2: "others")
            isActiveClicked = false
            isPendingClicked = true
            isOthersClicked = false
            
        }else{
            
        }
        self.customerArray.removeAll()
        self.addImageView.isHidden = true
        self.status = "PENDING"
        pageNo = 0
        self.searchTF.text = ""
        self.customer(search: "\(searchTF.text!)")
        self.customerTableView.reloadData()
    }
    
    
    @IBAction func othersClicked(_ sender: Any) {
        
        if isOthersClicked == false{
            toggleViews(mainView: othersView, view1: activeView, view2: pendingView, mainLabel:othersLabel , label1: activeLabel, label2: pendingLabel)
            toggleImage(mainImage: othersImageView, image1: activeImageView, image2: pendingImageView, mainImageName: "others", imageName1: "accepted", imageName2: "pending")
            isActiveClicked = false
            isPendingClicked = false
            isOthersClicked = true
            
        }else{
            
        }
        self.customerArray.removeAll()
        otherCustomer(search:"")
        self.addImageView.isHidden = true
        pageNo = 0
        self.searchTF.text = ""
        self.otherCustomer(search: "\(searchTF.text!)")
        
        self.customerTableView.reloadData()
    }
    
    
    func otherCustomer(search:String){
        var parameters = [String:Any]()
        parameters = [
            "pageSize" : 20,
            "pageNo" : "\(self.pageNo)",
            "search" : "\(search)",
            "financialYear": self.currentFinancialYear!
        ]
        
        //+918669696004
        //asdfgf1234$
        //8888870909
        //asdfgf1234$
        if isDragging == false{
            self.activityIndicatorBegin()
        }
        let headers = header()
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
        
        
        Alamofire.request(URLStatics.customerOther, method: .get, parameters: parameters,headers: headers)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                
                self.customerArray.removeAll()
                switch (response.result) {
                    
                case .success:
                    if self.isDragging == false{
                        self.activityIndicatorEnd()
                    }
                    if let reply = response.result.value {
                        print("======= JSON: CustomerOther ========  \(reply)")
                        let mainResponse = JSON(reply)
                        print("======= JSON: mainResponse CustomerOther ========  \(mainResponse["data"])")
                        let data = mainResponse["data"]
                        
                        var favDict = [[String:Any]]()
                        favDict = mainResponse.arrayObject as! [[String:Any]]
                        var addr : [String:Any]?
                        var contactPersonn : CustomerContactPerson?
                        for favComp in favDict{
                            addr = favComp["address"] as? [String:Any]
                            let addres = CustomerAddress(id: addr?["id"] as? Int,
                                                         address: addr?["address"] as? String,
                                                         city: addr?["city"] as? String,
                                                         state: addr?["state"] as? String,
                                                         pincode: addr?["pincode"] as? String,
                                                         country: addr?["country"] as? String,
                                                         latitude: addr?["latitude"] as? Double,
                                                         longitude: addr?["longitude"] as? Double)
                            var contactArray : [String:Any]?
                            
                            if let contPersonArr = favComp["contactPersons"] as? [String:Any] {
                                contactArray = contPersonArr
                            }
                            let typeOfComay = favComp["typeOfCompany"] as? [String:Any]
                            
                            let typeOfCompany =  TypeOfCompany1(id: typeOfComay?["id"] as? Int, companyType: typeOfComay?["companyType"] as? String, companyCategories: typeOfComay?["companyType"] as? String)
                            
                            
                            
                            print("full Name ==== \(String(describing: contactArray!["fullName"] as! String))")
                            let contact = CustomerContactPerson(id: contactArray!["id"] as! Int, fullName: contactArray!["fullName"] as! String, contactNo: contactArray!["contactNo"] as! String, email: contactArray!["email"] as! String, loadOwnerID: 0)
                            print("contact ==== \(String(describing: contact))")
                            contactPersonn = contact
                            
                            
                            
                            let fav = FavouriteCompany(id: favComp["id"] as? Int, name: favComp["name"] as? String, address: addres, contactPersons: [contactPersonn!], fax: favComp["fax"] as? String, email: favComp["email"] as? String, gstTaxRegNo: favComp["gstTaxRegNo"] as? String, companyStatus: favComp["companyStatus"] as? String, code: favComp["code"] as? String, registrationDate: favComp["registrationDate"] as? Int, hubStatus: favComp["hubStatus"] as? String, count: favComp["count"] as? Int, telephone: favComp["telephone"] as? String, typeOfCompany: typeOfCompany, panNo: favComp["panNo"] as? String, cinNo: favComp["cinNo"] as? String, companyCode: favComp["companyCode"] as? String)
                            self.customerArray.append(fav)
                        }
                        
                        let customerr = Customer(favouriteCompany: nil, activeCount: data["activeCount"].int, pendingCount: data["pendingCount"].int, inactiveCount: data["inactiveCount"].int, manualCount: data["manualCount"].int, blackListActiveCount: data["blackListActiveCount"].int, blackListInactiveCount: data["blackListInactiveCount"].int, totalcount: data["totalcount"].int)
                        self.customerDic = customerr
                        print("======= customerArray ======")
                        print(self.customerArray.count)
                        self.customerTableView.reloadData()
                    }
                    break
                case .failure(let error):
                    if self.isDragging == false{
                        self.activityIndicatorEnd()
                    }
                    print("error \(error.localizedDescription)")
                    break
                }
        }
        
    }
    
    
    
    
    @IBAction func searchClicked(_ sender: Any) {
        if isOthersClicked{
            pageNo = 0
            self.searchTF.text = ""
            self.otherCustomer(search: "\(searchTF.text!)")
            
            self.customerTableView.reloadData()
        }else{
            pageNo = 0
            self.searchTF.text = ""
            self.customer(search: "\(searchTF.text!)")
            self.customerTableView.reloadData()
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.customerArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "customer", for: indexPath) as! CustomerTableViewCell
        if customerArray.count != 0 {
            let customer = customerArray[indexPath.row]
            cell.name.text = customer.name
            cell.address.text = customer.address?.address
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let customer = customerArray[indexPath.row]
        sideMenuController?.setContentViewController(with: "36")
        
        NotificationCenter.default.post(name: .customerDetail, object: nil,userInfo: ["customer":customer])
    }
    
    func toggleViews(mainView:UIView,view1:UIView,view2:UIView,mainLabel:UILabel,label1:UILabel,label2:UILabel){
        mainView.backgroundColor = UIColor(hexString: ColorConstants.RED)
        label1.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
        label2.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
        mainLabel.textColor = UIColor.white
        
        view1.backgroundColor = UIColor.white
        view2.backgroundColor = UIColor.white
        
    }
    
    
    func toggleImage(mainImage:UIImageView,image1:UIImageView,image2:UIImageView,mainImageName:String,imageName1:String,imageName2:String){
        mainImage.image = UIImage(named: mainImageName)
        mainImage.image = mainImage.image?.withRenderingMode(.alwaysTemplate)
        mainImage.tintColor = UIColor.white
        image1.image = UIImage(named: imageName1)
        image1.image = image1.image?.withRenderingMode(.alwaysTemplate)
        image1.tintColor = UIColor(hexString: ColorConstants.RED)
        image2.image = UIImage(named: imageName2)
        image2.image = image2.image?.withRenderingMode(.alwaysTemplate)
        image2.tintColor = UIColor(hexString: ColorConstants.RED)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
