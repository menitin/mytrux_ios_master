//
//  AssignViewController.swift
//  Mytrux
//
//  Created by Mytrux on 22/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import SideMenuSwift
import Alamofire
import SwiftyJSON

class AssignViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource ,UITextFieldDelegate{
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var assignBtn: UIView!
    @IBOutlet weak var topBar: UIView!
    
    var jobId = Int()
    var isContainer = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        topBarCard(topBar: topBar)
        assignBtn.isHidden = true
        sideMenuController?.clearCache(with: "18")
        NotificationCenter.default.addObserver(self, selector: #selector(self.assignDetails(notification:)), name: .assign, object: nil)
    }
    
    var assignDetail : AssignDatum?
    
    var status = String()
    
    @objc func assignDetails(notification: Notification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        let assignDetails = userInfo!["assignDetail"] as! AssignDatum
        status = userInfo!["state"] as! String
        assignDetail = assignDetails
      
        registrationNo = assignDetails.registrationNo ?? ""
        self.registrationNo = self.registrationNo.replacingOccurrences(of: "-", with: "")
        self.jobId = assignDetails.id!
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    @IBAction func backBtnClicked(_ sender: Any) {
        if status == "Dashboard" {
            SideMenuController.preferences.basic.defaultCacheKey = "default"
            sideMenuController?.setContentViewController(with: "default")
        }else{
            
            sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookingController") }, with: "tenth")
            sideMenuController?.setContentViewController(with: "tenth")
        }
       
    }
    
    
    var isUpdateClicked = false
    var isConfirmClicked = false
    var driverName = String()
    var driverMobileNo = String()
    var startKms = Int()
    var endKms = Int()
    var containerNo1 = String()
    var containerNo2 = String()
    var registrationNo = String()
    var isEmpty = false
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "assignCell") as! AssignTableViewCell
        cell.updateBtn.isHidden = true
        cell.confirmBtn.isHidden = false
        
        let update = UITapGestureRecognizer(target: self, action: #selector(updateClicked(recognizer:)))
        cell.updateBtn.addGestureRecognizer(update)
        cell.updateBtn.tag = indexPath.row
        
        let confirm = UITapGestureRecognizer(target: self, action: #selector(confirmClicked(recognizer:)))
        cell.confirmBtn.addGestureRecognizer(confirm)
        
        let assign = self.assignDetail!
        cell.vehicleType.text = assign.vehicleContainerDetails!.vehicleType.name
   
        cell.vehicleNo.delegate = self
        cell.driverMobileNo.delegate = self
        cell.driverName.delegate = self
        cell.startKmsTF.delegate = self
        cell.endKmsTF.delegate = self
        cell.containerNoTF1.delegate = self
       cell.containerNoTF2.delegate = self
            cell.vehicleNo.text = self.registrationNo
        
        cell.driverMobileNo.tag = 1
        cell.driverName.tag = 2
        cell.startKmsTF.tag = 3
        cell.endKmsTF.tag = 4
        cell.containerNoTF1.tag = 5
        cell.containerNoTF2.tag = 6
        
       

        if assign.loadType!.code == "FTL" {
            cell.startEndKmsStack.isHidden = false
            cell.containerNoView.isHidden = true
            self.isContainer = false
        }else{
            cell.startEndKmsStack.isHidden = false
            cell.containerNoView.isHidden = false
        self.isContainer = true
        }
        
        if isConfirmClicked{
            cell.updateBtn.isHidden = false
            cell.confirmBtn.isHidden = true
        }
        
        if isUpdateClicked{
            cell.updateBtn.isHidden = false
            cell.confirmBtn.isHidden = true
        }
        
        self.driverName = cell.driverName.text!
        self.driverMobileNo = cell.driverMobileNo.text!
        self.registrationNo = cell.vehicleNo.text!
        self.startKms = Int(cell.startKmsTF.text!) ?? 0
        self.endKms = Int(cell.endKmsTF.text!) ?? 0
        return cell
    }
  
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isContainer{
             return self.tableView.frame.height - 5
        }else{
             return self.tableView.frame.height - 50
        }

    }
    
    @objc func updateClicked(recognizer: UITapGestureRecognizer){
        isConfirmClicked = false
        isUpdateClicked = true
        assignBtn.isHidden = false
            let tag = (recognizer.view?.tag)!
             let index = IndexPath(row: tag, section: 0)
            let cell: AssignTableViewCell = self.tableView.cellForRow(at: index) as! AssignTableViewCell
        
        if(registrationNo == "" || cell.driverMobileNo.text == "" || cell.driverName.text == "") {
            let alert = UIAlertController(title:"", message: "Fill all mandatory fields", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        self.registrationNo = cell.vehicleNo.text!
        self.startKms = Int(cell.startKmsTF.text!)!
        self.tableView.reloadData()
    }
    
    @IBAction func assignClicked(_ sender: Any) {
       
          assign()
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if !textField.text!.isEmpty // check textfield contains value or not
        {
            if textField.tag == 0{
                 registrationNo = textField.text!
            }
            
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField.tag {
        case 0:
            print("vehicleNo")
            self.registrationNo = textField.text!
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        case 1:
            print("driverMobileNo")
            self.driverMobileNo = textField.text!
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        case 2:
            print("driverName")
            self.driverName = textField.text!
        case 3:
            print("startTf")
          //  self.startKms = Int(textField.text!)!
            let maxLength = 6
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        case 4:
            print("endTf")
           // self.endKms = Int(textField.text!)!
            let maxLength = 6
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        case 5:
            print("containerNoTF1")
            self.containerNo1 = textField.text!
            let maxLength = 6
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        case 6:
            print("containerNoTF2")
             self.containerNo2 = textField.text!
            let maxLength = 6
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        default:
            print("error")
        }
        
        return true
    }
    
    
    
    @objc func confirmClicked(recognizer: UITapGestureRecognizer){
        let tag = (recognizer.view?.tag)!
        let index = IndexPath(row: tag, section: 0)
        let cell: AssignTableViewCell = self.tableView.cellForRow(at: index) as! AssignTableViewCell
        
        if(registrationNo == "" || cell.driverMobileNo.text == "" || cell.driverName.text == "") {
            let alert = UIAlertController(title:"", message: "Fill all mandatory fields", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
            return
        }else{
            isConfirmClicked = true
            isUpdateClicked = false
            assignBtn.isHidden = true
        }
       
         self.tableView.reloadData()
    }
    
    
    func assign(){
        
      
        
        let bidCode = assignDetail?.fleetBidCode
        let id = assignDetail?.fleetBidID
        
        let driverFullName = driverName
        self.registrationNo = self.registrationNo.replacingOccurrences(of: "-", with: "")
        
        let mimeType = assignDetail!.vehicleContainerDetails!.vehicleType.document?.mimeType
      
        
        let parameters : [String:Any] =  [
            "availableFleetBid":
                [
                    "bidCode":"\(String(describing: bidCode!))",
                    "id":id!
            ],
            "bookingTrips":[
                [
                    "allocationType":"MANUAL",
                    "driverFullName" : driverFullName,
                    "endKm":endKms,
                    "mobileNo":driverMobileNo,
                    "startKm" : startKms ,
                    "vehicleRegistrationNo":registrationNo,
                    "vehicleType":
                        [
                            "container":assignDetail!.vehicleContainerDetails!.vehicleType.container as Any,
                            "document":
                                [
                                    "id":assignDetail!.vehicleContainerDetails!.vehicleType.document?.id as Any,
                                    "mimeType":mimeType as Any
                            ],
                            "documentId":assignDetail!.vehicleContainerDetails!.vehicleType.documentID as Any,
                            "id":assignDetail!.vehicleContainerDetails!.vehicleType.id!,
                            "name":assignDetail!.vehicleContainerDetails!.vehicleType.name!,
                            "sequence":assignDetail!.vehicleContainerDetails!.vehicleType.sequence!,
                            "type":assignDetail!.vehicleContainerDetails!.vehicleType.type!
                    ]
                ]
            ]
        ]
        
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
    let header = self.header()
        
        Alamofire.request(URLStatics.assignDriver + "\(jobId)", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: header)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                let resultNSString = NSString(data: response.request!.httpBody!, encoding: String.Encoding.utf8.rawValue)!
                print("response.request?.httpBody = \(resultNSString)")
                switch (response.result) {
                case .success:
                      var returnDict = [String:Any]()
                    if let reply = response.result.value {
                        print("JSON: assign Driver ======== \(reply)")
                        let mainResponse = JSON(reply)
                        print("\(mainResponse)")
                        
                        
                        if mainResponse["warning"].stringValue != "" {
                            let alertController = UIAlertController(title: "\(mainResponse["warning"].stringValue)", message:"", preferredStyle: .alert)
                            
                            // Create the actions
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                              
                            }
                            alertController.addAction(okAction)
                            
                            // Present the controller
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                        
                        
                        let returnObj = mainResponse["returnObj"].stringValue
                        do {
                            returnDict = try self.convertToDictionary(from: returnObj)
                            print("returnDict = \(returnDict)")
                        } catch {
                            print(error)
                        }

                        
                       if mainResponse["success"].stringValue != ""{
                         let alertController = UIAlertController(title: "\(returnDict["bookingNo"]!)", message: "\(mainResponse["success"])", preferredStyle: .alert)
                        // Create the actions
                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                            UIAlertAction in
                            
                            SideMenuController.preferences.basic.defaultCacheKey = "default"
                            self.sideMenuController?.setContentViewController(with: "default")
                            
                        }
                        
                        alertController.addAction(okAction)
                        
                        // Present the controller
                        self.present(alertController, animated: true, completion: nil)
                        }
                        
                    }
                    
                    break
                case .failure(let error):
                    
                    print("error \(error.localizedDescription)")
                    break
                }
        }
        
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

