//
//  BookingController.swift
//  Mytrux
//
//  Created by Aboli on 09/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import SideMenuSwift
import Alamofire
import SwiftyJSON

class BookingController: BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet weak var ftlView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var completedStack: UIStackView!
    @IBOutlet weak var intransitStack: UIStackView!
    @IBOutlet weak var assignStackView: UIStackView!
    @IBOutlet weak var allStackView: UIStackView!
    @IBOutlet weak var invoiceLabel: UILabel!
    @IBOutlet weak var makeInvoiceView: UIView!
    @IBOutlet weak var callDriverView: UIView!
    @IBOutlet weak var eLRView: UIView!
    @IBOutlet weak var containerLabel: UILabel!
    @IBOutlet weak var containerCount: UILabel!
    @IBOutlet weak var ftlCount: UILabel!
    @IBOutlet weak var menuButton: UIView!
    @IBOutlet weak var bookingCollectionView: UICollectionView!
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var newBooking_assign: UILabel!
    @IBOutlet weak var containerCountWrraper: UIView!
    @IBOutlet weak var ftlCountWrraper: UIView!
    @IBOutlet weak var containerRedLine: UIImageView!
    @IBOutlet weak var ftlRedLine: UIImageView!
    @IBOutlet weak var ftlLabel: UILabel!
    var loadTypeName = String()
    var isContainerClicked = false
    var isFtlClicked = false
    var  isDragging = false
    var assignDetailArray = [AssignDatum]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //  sideMenuController?.clearCache(with: "tenth")
        
        bookingCollectionView.isScrollEnabled = true
        bookingCollectionView.delegate = self
        bookingCollectionView.dataSource = self
        cardViewArray(arrView: [menuView])
        self.menuView.isHidden = true
        isDragging = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.MyBooking(notification:)), name: Notification.Name("MyBooking"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.Assign(notification:)), name: Notification.Name("Assign"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.InTransit(notification:)), name: Notification.Name("InTransit"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.Completed(notification:)), name: Notification.Name("Completed"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.trackTransit(notification:)), name: Notification.Name("trackTransit"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.searchData(notification:)), name: .searchData, object: nil)
        
        if self.loginUserType == UserStates.SERVICE_USER{
            newBooking_assign.text = "New Booking"
        }else{
            newBooking_assign.text = "Assign"
        }
        isContainerClicked = true
        isFtlClicked = false
        if isContainerClicked == true{
            containerLabel.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
            self.containerRedLine.isHidden = false
            ftlLabel.textColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
            self.ftlRedLine.isHidden = true
        }
        self.circularView(viewArray: [containerCountWrraper,ftlCountWrraper])
        self.bookingCollectionView.isPagingEnabled = false
        self.callDriverView.isUserInteractionEnabled = false
    }
    
    var isMenuVisible = false
    var isBookingCLicked = true
    var isAssignCLicked = false
    var isInTransitCLicked = false
    var isCompletedCLicked = false
    var istrackTransitClicked = false
    
    @IBAction func callDriverClicked(_ sender: Any) {
        let indexPath = bookingCollectionView.indexPathsForVisibleItems
        print("=======IndexPath cell=======")
        print(indexPath[0].row)
        call(tag:indexPath[0].row)
    }
    
    @IBAction func allClicked(_ sender: Any) {
        print("allClicked")
        if isContainerClicked == true{
               containerLabel.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
               self.containerRedLine.isHidden = false
               ftlLabel.textColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
               self.ftlRedLine.isHidden = true
           }
        let state = UserStates.ALL
           
           isBookingCLicked = true
           isAssignCLicked = false
           isInTransitCLicked = false
           isCompletedCLicked = false
           istrackTransitClicked = false
           self.bookingCollectionView.reloadData()
           self.callDriverView.isUserInteractionEnabled = false
           reloadApiData(state: state, search: "")
        self.isMenuVisible = false
        UIView.animate(withDuration: 0.8) {
            self.menuView.isHidden = !self.isMenuVisible
        }
    }
    @IBAction func assignClicked(_ sender: Any) {
        if isContainerClicked == true{
                 containerLabel.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
                 self.containerRedLine.isHidden = false
                 ftlLabel.textColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
                 self.ftlRedLine.isHidden = true
             }
        let state = UserStates.NEW
             
             isBookingCLicked = false
             isAssignCLicked = true
             isInTransitCLicked = false
             isCompletedCLicked = false
             istrackTransitClicked = false
             self.callDriverView.isUserInteractionEnabled = true
             reloadApiData(state: state, search: "")
        self.isMenuVisible = false
        UIView.animate(withDuration: 0.8) {
            self.menuView.isHidden = !self.isMenuVisible
        }
    }
    
    @IBAction func inTransitClicked(_ sender: Any) {
        if isContainerClicked == true{
                containerLabel.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
                self.containerRedLine.isHidden = false
                ftlLabel.textColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
                self.ftlRedLine.isHidden = true
                
            }
        let state = UserStates.ASSIGN
            
            isBookingCLicked = false
            isAssignCLicked = false
            isInTransitCLicked = true
            isCompletedCLicked = false
            self.callDriverView.isUserInteractionEnabled = false
            istrackTransitClicked = false
            reloadApiData(state: state, search: "")
        self.isMenuVisible = false
        UIView.animate(withDuration: 0.8) {
            self.menuView.isHidden = !self.isMenuVisible
        }
    }
    @IBAction func completedClicked(_ sender: Any) {
        if isContainerClicked == true{
                containerLabel.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
                self.containerRedLine.isHidden = false
                ftlLabel.textColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
                self.ftlRedLine.isHidden = true
            }
        let state = UserStates.COMPLETE
            
            isBookingCLicked = false
            isAssignCLicked = false
            isInTransitCLicked = false
            self.callDriverView.isUserInteractionEnabled = false
            isCompletedCLicked = true
            istrackTransitClicked = false
            reloadApiData(state: state, search: "")
        self.isMenuVisible = false
              UIView.animate(withDuration: 0.8) {
                  self.menuView.isHidden = !self.isMenuVisible
              }
        
    }
    @objc func searchData(notification: Notification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        let booking = userInfo!["booking"] as! BookingElement
        self.bookingList.removeAll()
        self.bookingCollectionView.isScrollEnabled = false
        pageNo = 0
        if self.loadTypeName == "FTL" {
            self.containerCount.text = "0"
            self.ftlCount.text = "1"
            self.containerView.isUserInteractionEnabled = false
            self.ftlView.isUserInteractionEnabled = true
        }else{
            self.containerCount.text = "1"
            self.ftlCount.text = "0"
            self.containerView.isUserInteractionEnabled = true
           self.ftlView.isUserInteractionEnabled = false
        }
        self.bookingList.append(booking)
        self.bookingCollectionView.reloadData()
        
    }
    func call(tag:Int){
        let tag = tag
        let jobVehiclesDrivers = self.bookingList[tag].jobVehiclesDrivers
        let phoneNo = jobVehiclesDrivers![0].mobileNo
        self.callNumber(phoneNumber: phoneNo!)
    }
    
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        targetContentOffset.pointee = scrollView.contentOffset
        let pageWidth:Float = Float(self.view.bounds.width)
        let minSpace:Float = 10.0
        var mod = Double()
        if(velocity.x > 1)
        {
            mod = 0.5
            
        }
        else if(velocity.x < -1)
        {
            mod = -0.5
            
        }
        var cellToSwipe:Double = Double(Float((scrollView.contentOffset.x))/Float((pageWidth+minSpace))) + Double(0.5) + mod
        if cellToSwipe < 0 {
            cellToSwipe = 0
        } else if cellToSwipe >= Double(self.bookingList.count) {
            cellToSwipe = Double(self.bookingList.count) - Double(1)
        }
        let indexPath:IndexPath = IndexPath(row: Int(cellToSwipe), section:0)
        self.bookingCollectionView.scrollToItem(at:indexPath, at: UICollectionView.ScrollPosition.left, animated: true)
        
        let offsetY = scrollView.contentOffset.x
        let contentHeight = scrollView.contentSize.width
        if self.bookingList.count > 7 {
            if offsetY > contentHeight - scrollView.frame.size.width - 300 {
                if bookingList[0].count != self.bookingList.count {
                    pageNo += 1
                    
                    isDragging = true
                    self.myBooking(search: "")
                }
                
            }else{
                isDragging = false
            }
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        
        let bookingListt = self.bookingList[indexPath.row]
        if bookingListt.salesBillFinal! == true{
            self.invoiceLabel.text = "VIEW INVOICE"
        }else{
            self.invoiceLabel.text = "MAKE INVOICE"
        }
    }
    
    override func viewDidLayoutSubviews() {
        self.invoiceLabel.layoutIfNeeded()
    }
    
    
    @IBAction func eLRClicked(_ sender: Any) {
        let indexPath = bookingCollectionView.indexPathsForVisibleItems
        print("=======IndexPath cell=======")
        print(indexPath[0].row)
        let jobid = self.bookingList[indexPath[0].row].id!
        let fleetId = userDefaults.value(forKey: LoginDataConstants.fleetId)
        
        
        sideMenuController?.setContentViewController(with: "24")
        NotificationCenter.default.post(name:.eLR, object: nil,userInfo: ["url": URLStatics.report + "\(fleetId!)" + "/" + "\(jobid)"])
        //URLStatics.report + "\(fleetId!)" + "/" + "\(jobid)"
        
        
    }
    
    
    
    var status = String()
    
    
    func reloadApiData(state:String,search:String){
        self.status = state
        self.loadTypeName = "CONTAINER"
        count()
        self.bookingList.removeAll()
        self.bookingCollectionView.reloadData()
        myBooking(search:search)
        
        self.bookingCollectionView.reloadData()
        
    }
    
    @objc func MyBooking(notification: Notification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        isContainerClicked = true
        isFtlClicked = false
        if isContainerClicked == true{
            containerLabel.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
            self.containerRedLine.isHidden = false
            ftlLabel.textColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
            self.ftlRedLine.isHidden = true
        }
        let state = (userInfo!["status"] as! String)
        
        isBookingCLicked = true
        isAssignCLicked = false
        isInTransitCLicked = false
        isCompletedCLicked = false
        istrackTransitClicked = false
        self.bookingCollectionView.reloadData()
        self.callDriverView.isUserInteractionEnabled = false
        reloadApiData(state: state, search: "")
    }
    
    @objc func Assign(notification: Notification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        isContainerClicked = true
        isFtlClicked = false
        if isContainerClicked == true{
            containerLabel.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
            self.containerRedLine.isHidden = false
            ftlLabel.textColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
            self.ftlRedLine.isHidden = true
        }
        let state = (userInfo!["status"] as! String)
        
        isBookingCLicked = false
        isAssignCLicked = true
        isInTransitCLicked = false
        isCompletedCLicked = false
        istrackTransitClicked = false
        self.callDriverView.isUserInteractionEnabled = true
        reloadApiData(state: state, search: "")
    }
    
    @objc func InTransit(notification: Notification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        isContainerClicked = true
        isFtlClicked = false
        if isContainerClicked == true{
            containerLabel.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
            self.containerRedLine.isHidden = false
            ftlLabel.textColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
            self.ftlRedLine.isHidden = true
            
        }
        let state = (userInfo!["status"] as! String)
        
        isBookingCLicked = false
        isAssignCLicked = false
        isInTransitCLicked = true
        isCompletedCLicked = false
        self.callDriverView.isUserInteractionEnabled = false
        istrackTransitClicked = false
        reloadApiData(state: state, search: "")
    }
    
    @objc func Completed(notification: Notification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        isContainerClicked = true
        isFtlClicked = false
        if isContainerClicked == true{
            containerLabel.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
            self.containerRedLine.isHidden = false
            ftlLabel.textColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
            self.ftlRedLine.isHidden = true
        }
        let state = (userInfo!["status"] as! String)
        
        isBookingCLicked = false
        isAssignCLicked = false
        isInTransitCLicked = false
        self.callDriverView.isUserInteractionEnabled = false
        isCompletedCLicked = true
        istrackTransitClicked = false
        reloadApiData(state: state, search: "")
    }
    
    @objc func trackTransit(notification: Notification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        isContainerClicked = true
        isFtlClicked = false
        if isContainerClicked == true{
            containerLabel.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
            self.containerRedLine.isHidden = false
            ftlLabel.textColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
            self.ftlRedLine.isHidden = true
        }
        let state = (userInfo!["status"] as! String)
        
        isBookingCLicked = false
        isAssignCLicked = false
        isInTransitCLicked = false
        isCompletedCLicked = false
        istrackTransitClicked = true
        self.callDriverView.isUserInteractionEnabled = false
        
        self.bookingCollectionView.reloadData()
        reloadApiData(state: state, search: "")
    }
    
    @IBAction func containerClicked(_ sender: Any) {
        isContainerClicked = true
        isFtlClicked = false
        if isContainerClicked == true{
            containerLabel.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
            self.containerRedLine.isHidden = false
            ftlLabel.textColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
            self.ftlRedLine.isHidden = true
            
        }
        self.pageNo = 0
        self.loadTypeName = "CONTAINER"
        self.bookingList.removeAll()
        self.bookingCollectionView.reloadData()
        self.myBooking(search: "")
    }
    
    
    @IBAction func ftlClicked(_ sender: Any) {
        isFtlClicked = true
        isContainerClicked = false
        if isFtlClicked == true{
            containerLabel.textColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
            self.containerRedLine.isHidden = true
            ftlLabel.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
            self.ftlRedLine.isHidden = false
        }
        self.pageNo = 0
        self.loadTypeName = "FTL"
        self.bookingList.removeAll()
        self.bookingCollectionView.reloadData()
        self.myBooking(search: "")
    }
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.bookingList.count
    }
    
    
    
    func count(){
        
        let parameters : [String:Any]
        
        parameters = [
            
            "status" : self.status,
            "financialYear": self.currentFinancialYear!,
            
        ]
        let headers = header()
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
        
        
        Alamofire.request(URLStatics.bookingCounter, method:.get,parameters: parameters, headers: headers)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                
                switch (response.result) {
                case .success:
                    
                    if let reply = response.result.value {
                        
                        let mainResponse = JSON(reply)
                        print("======= JSON: bookingCounter ========  \(mainResponse)")
                        let data = mainResponse["data"]
                        self.containerCount.text = "\(data["contCount"])"
                        self.ftlCount.text = "\(data["ftlCount"])"
                    }
                    break
                case .failure(let error):
                    
                    print("== error == \(error.localizedDescription)")
                    break
                }
        }
        
    }
    
    
    
    
    
    
    var pageNo = 0
    var bookingList = [BookingElement]()
    
    func myBooking(search:String){
        
        if isDragging == false{
            self.activityIndicatorBegin()
        }
        let parameters : [String:Any]
        
        parameters = [
            "recordsPerPage" : 10,
            "pageNo" : "\(self.pageNo)",
            "search" : search,
            "status" : self.status,
            "financialYear": self.currentFinancialYear!,
            "loadTypeName": self.loadTypeName
        ]
        
        let headers = header()
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
        
        Alamofire.request(URLStatics.bookingList, method:.get,parameters: parameters, headers: headers)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                
                switch (response.result) {
                case .success:
                    if self.isDragging == false{
                        self.activityIndicatorEnd()
                    }
                    if let reply = response.result.value {
                        var vehicleDetails : VehicleContainerDetails1
                        var vehicleDetailsDict = [String:Any]()
                        let mainResponse = JSON(reply)
                        var vehicleType : AssignVehicleType1
                        var documentDict : [String:Any]?
                        var documentSmallDict : [String:Any]?
                        var document : FirstPicID
                        var doucmentSmall : FirstPicID
                        var vehicleTypeDict = [String:Any]()
                        var grossWeightUnit : WeightUnit
                        var chargeableWTUnit : WeightUnit
                        var netWeightUnit : WeightUnit
                        var grosWtUnit = [String:Any]()
                        var chargebleWtUnit = [String:Any]()
                        var netWtUnit = [String:Any]()
                        var jobStatusDict = [String:Any]()
                        var jobStatus : JobStatusClass
                        var cargoType : CargoTypeClass
                        var cargoo = [String:Any]()
                        var loadType : LoadTypeClass
                        var loadTypeDict = [String:Any]()
                        var jobType : JobTypeClass
                        var jobTypeDict = [String:Any]()
                        var bookingType : BookingTypeClass
                        var bookingTypeDict = [String:Any]()
                        var availbaleFleetBid : AvailableBid1?
                        var availFleetBidDict = [String:Any]()
                        var availableLoadBid : AvailableBid1?
                        var availLoadBidDict = [String:Any]()
                        var jobVehiclesDict = [[String:Any]]()
                        var podId : PodID?
                        var podDict : [String:Any]?
                        var jobCVehicleDct : [String:Any]?
                        var jobVehicleTYpe : VehicleType?
                        var loadOwner : LoadOwner1?
                        var fleetOwner : FleetOwner1?
                        var loadOwnerDict : [String:Any]?
                        var fleetOwnerDict : [String:Any]?
                        var shipper : Shipper?
                        var loadOwnerShipperDict = [String:Any]()
                        var loadOwnerShipper : LoadOwner1?
                        var consignee : Consignee?
                        var loadOwnerConsignee : LoadOwner1?
                        var loadownerConsigneeDict = [String:Any]()
                        var shipperDict = [String:Any]()
                        var consigneeDict = [String:Any]()
                        var deliveryCustomerDict = [String:Any]()
                        var deliveryCustomer : Consignee?
                        var loadOwnerDeliveryCustomer : LoadOwner1?
                        var loadownerDeliveryCustomerDict = [String:Any]()
                        var freightTerm : FreightTermsClass?
                        let freightTermDict = [String:Any]()
                        var exTaxInfo : ExTaxInfo?
                        var taxGroupDict = [String:Any]()
                        var taxGroup: TaxGroup?
                        _ = [[String:Any]]()
                        _ = [[String:Any]]()
                        print("======= JSON: bookingList ======== \(mainResponse)")
                        
                        for data in mainResponse.arrayValue{
                            
                            var jobVehiclesDrivers = [JobVehiclesDriver]()
                            var taxDetailArray = [TaxDetail]()
                            var tax = [Tax]()
                            
                            if let returnObject =
                                data["jobVehiclesDrivers"].arrayObject as? [[String:Any]] {
                                jobVehiclesDict = returnObject
                            }
                            
                            
                            
                            for job in jobVehiclesDict{
                                
                                podDict = job["podId"] as? [String:Any]
                                podId = PodID(id: podDict?["id"] as? Int, mimeType: podDict?["mimeType"] as? String)
                                let logo = Logo(id: nil, mimeType: nil, type: nil)
                                jobCVehicleDct = job["vehicleType"] as? [String:Any]
                                jobVehicleTYpe = VehicleType(id: jobCVehicleDct!["id"] as! Int,
                                                             name: jobCVehicleDct!["name"] as! String,
                                                             sequence: Int(jobCVehicleDct!["sequence"] as! NSNumber),
                                                             container: jobCVehicleDct!["container"] as! Bool,
                                                             type: jobCVehicleDct!["type"] as! String,
                                                             document: logo,
                                                             documentSmall: logo,
                                                             vehicleOrder: jobCVehicleDct!["vehicleOrder"] as! Int,
                                                             weightCapacity: jobCVehicleDct!["weightCapacity"] as? Int,
                                                             documentID: jobCVehicleDct!["documentID"] as? Int,
                                                             smallDocID: jobCVehicleDct!["smallDocID"] as? Int)
                                
                                let jobDriver = JobVehiclesDriver(id: job["id"] as? Int, availableFleetBid: nil, vehicleRegistrationNo: job["vehicleRegistrationNo"] as? String, driverFullName: job["driverFullName"] as? String, mobileNo: job["mobileNo"] as? String, startKM: job["startKM"] as? Int, endKM: job["endKM"] as? Int, user: nil, allocationType: job["allocationType"] as? String, createTime: job["createTime"] as? Int, vehicleType: jobVehicleTYpe, jobStatus: nil, lrNo: job["lrNo"] as? Int, uploadPodTime: job["uploadPodTime"] as? Int, podID: podId, exManualLrNo: job["exManualLrNo"] as? String, exManualLrDate: job["exManualLrDate"] as? Int)
                                
                                jobVehiclesDrivers.append(jobDriver)
                                print("podDict id ====== \(String(describing: podDict?["id"]))")
                                
                            }
                            
                            
                            let yourString = data["exTaxInfo"].stringValue
                            var exTaxInfojson: JSON
                            let dataToConvert = yourString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                            do {
                                exTaxInfojson = try JSON(data: dataToConvert!)
                            } catch _ {
                                exTaxInfojson = ""
                            }
                            
                            taxGroupDict = exTaxInfojson["taxGroup"].dictionaryObject as! [String:Any]
                            
                            taxGroup = TaxGroup(taxGroupDescription: taxGroupDict["taxGroupDescription"] as? String, tax: taxGroupDict["tax"] as? Int, id: taxGroupDict["id"] as? Int, status: taxGroupDict["status"] as? Bool, defaultType: taxGroupDict["defaultType"] as? String)
                            
                            
                            for taxx in exTaxInfojson["taxDetails"].arrayValue{
                                print("taxName \(taxx["taxName"].stringValue)")
                                let taxInfo = TaxDetail(taxAmount: taxx["taxAmount"].int, taxName: taxx["taxName"].string, taxRate: taxx["taxRate"].double, taxType: taxx["taxType"].string)
                                taxDetailArray.append(taxInfo)
                            }
                            for taxx in exTaxInfojson["taxs"].arrayValue{
                                var taxmasterdict = taxx["taxMaster"].dictionaryValue
                                let taxMaster = TaxMaster(id: taxmasterdict["id"]?.int, taxName: taxmasterdict["taxName"]?.string, taxType: taxmasterdict["taxType"]?.string)
                                let taxInfo = Tax(taxMaster: taxMaster, taxType: taxx["taxType"].string, id: taxx["id"].int, tax: taxx["tax"].double, status: taxx["status"].bool, taxName: taxx["taxName"].string)
                                tax.append(taxInfo)
                            }
                            
                            
                            print("exTaxInfojson string: " + String(describing: exTaxInfojson))
                            
                            print("id booking list ==== \(data["id"].stringValue)")
                            
                            shipperDict = data["shipper"].dictionaryObject!
                            consigneeDict = data["consignee"].dictionaryObject!
                            deliveryCustomerDict = data["deliveryCustomer"].dictionaryObject!
                            loadOwnerShipperDict = shipperDict["loadOwner"] as! [String:Any]
                            loadownerConsigneeDict = consigneeDict["loadOwner"] as! [String:Any]
                            loadownerDeliveryCustomerDict = deliveryCustomerDict["loadOwner"] as! [String:Any]
                            fleetOwnerDict = data["fleetOwner"].dictionaryObject as! [String:Any]
                            
                            vehicleDetailsDict = data["vehicleContainerDetails"].dictionaryObject as! [String:Any]
                            loadOwnerDict = data["loadOwner"].dictionaryObject as! [String:Any]
                            fleetOwnerDict = data["fleetOwner"].dictionaryObject as! [String:Any]
                            availLoadBidDict = data["availableLoadBid"].dictionaryObject as! [String:Any]
                            availFleetBidDict = data["availableFleetBid"].dictionaryObject as! [String:Any]
                            vehicleTypeDict = vehicleDetailsDict["vehicleType"] as! [String:Any]
                            cargoo = data["cargoType"].dictionaryObject as! [String:Any]
                            cargoType = CargoTypeClass(id: cargoo["id"] as! Int, cargoType: cargoo["cargoType"] as! String)
                            
                            loadTypeDict = data["loadType"].dictionaryObject as! [String:Any]
                            jobTypeDict = data["jobType"].dictionaryObject as! [String:Any]
                            jobType = JobTypeClass(id: jobTypeDict["id"] as? Int, jobType: jobTypeDict["jobType"] as? String)
                            loadType = LoadTypeClass(id: loadTypeDict["id"] as! Int, loadType: loadTypeDict["loadType"] as! String, code: loadTypeDict["code"] as! String)
                            
                            bookingTypeDict = data["bookingType"].dictionaryObject as! [String:Any]
                            bookingType = BookingTypeClass(id: bookingTypeDict["id"] as? Int, bookingType: bookingTypeDict["bookingType"] as? String)
                            
                            
                            
                            
                            documentDict = vehicleTypeDict["document"] as? [String:Any]
                            documentSmallDict = vehicleTypeDict["documentSmall"] as? [String:Any]
                            document = FirstPicID(id: documentDict?["id"] as? Int, mimeType: documentDict?["mimeType"] as? String, type: documentDict?["type"] as? String)
                            doucmentSmall = FirstPicID(id: documentSmallDict?["id"] as? Int, mimeType: documentSmallDict?["mimeType"] as? String, type: documentSmallDict?["type"] as? String)
                            grosWtUnit = vehicleDetailsDict["grossWeightUnit"] as! [String:Any]
                            chargebleWtUnit = vehicleDetailsDict["chargeableWeightUnit"] as! [String:Any]
                            netWtUnit = vehicleDetailsDict["netWeightUnit"] as! [String:Any]
                            
                            grossWeightUnit = WeightUnit(id: grosWtUnit["id"] as! Int, name: grosWtUnit["name"] as! String, type: grosWtUnit["type"] as! String)
                            chargeableWTUnit = WeightUnit(id: chargebleWtUnit["id"] as! Int, name: chargebleWtUnit["name"] as! String, type: chargebleWtUnit["type"] as! String)
                            jobStatusDict = data["jobStatus"].dictionaryObject!
                            jobStatus = JobStatusClass(id: jobStatusDict["id"] as! Int, jobStatus: jobStatusDict["jobStatus"] as! String)
                            
                            netWeightUnit = WeightUnit(id: netWtUnit["id"] as! Int, name: netWtUnit["name"] as! String, type: netWtUnit["type"] as! String)
                            vehicleType = AssignVehicleType1(id: vehicleTypeDict["id"] as? Int, name: vehicleTypeDict["name"] as? String, sequence: vehicleTypeDict["sequence"] as? Double, container: vehicleTypeDict["container"] as? Bool, type: vehicleTypeDict["type"] as? String, document: document, documentSmall: doucmentSmall, vehicleOrder: vehicleTypeDict["vehicleOrder"] as? Int, documentID: vehicleTypeDict["documentId"] as? Int, smallDocID: vehicleTypeDict["smallDocId"] as? Int, weightCapacity: vehicleTypeDict["weightCapacity"] as? Int)
                            
                            availableLoadBid = AvailableBid1(id: availLoadBidDict["id"] as? Int,
                                                             
                                                             bidCode: availLoadBidDict["bidCode"] as? String,
                                                             
                                                             creationDate: availLoadBidDict["creationDate"] as? Int,
                                                             
                                                             closedBidTime: availLoadBidDict["closedBidTime"] as? Int,
                                                             
                                                             availableLocation: availLoadBidDict["availableLocation"] as? String,
                                                             destinationLocation: availLoadBidDict["destinationLocation"] as? String,
                                                             comments: availLoadBidDict["comments"] as? String,
                                                             availableLatitude: availLoadBidDict["availableLatitude"] as? Double,
                                                             availableLongitude: availLoadBidDict["availableLongitude"] as? Double,
                                                             destinationLatitude: availLoadBidDict["destinationLatitude"] as? Double,
                                                             destinationLongitude: availLoadBidDict["destinationLongitude"] as? Double,
                                                             vehicleType:nil, noOfVehicles: availLoadBidDict["noOfVehicles"] as? Int,
                                                             availableDateTime: nil,
                                                             cargoType: nil,
                                                             capacityWeight:  Int(truncating: availLoadBidDict["capacityWeight"] as! NSNumber),
                                                             capacityWeightUnit:nil,
                                                             loadType: nil,
                                                             expectedFreight: availLoadBidDict["expectedFreight"] as? Int,
                                                             currencyMaster: nil,
                                                             status:availLoadBidDict["status"] as? String,
                                                             registrationNo: nil,
                                                             postType: nil,
                                                             bidInviteCount: nil, bidQuotationCount: nil,
                                                             containerNo: availLoadBidDict["containerNo"] as? String,
                                                             sealNo: availLoadBidDict["sealNo"] as? String, pinNo: nil, packgs: availLoadBidDict["packgs"] as? Int)
                            
                            
                            
                            
                            availbaleFleetBid = AvailableBid1(id: availFleetBidDict["id"] as? Int,
                                                              bidCode: availFleetBidDict["bidCode"] as? String,
                                                              creationDate: availFleetBidDict["creationDate"] as? Int,
                                                              closedBidTime: availFleetBidDict["closedBidTime"] as? Int,
                                                              availableLocation: availFleetBidDict["availableLocation"] as? String,
                                                              destinationLocation: availFleetBidDict["destinationLocation"] as? String,
                                                              comments: availFleetBidDict["comments"] as? String,
                                                              availableLatitude: availFleetBidDict["availableLatitude"] as? Double,
                                                              availableLongitude: availFleetBidDict["availableLongitude"] as? Double,
                                                              destinationLatitude: availFleetBidDict["destinationLatitude"] as? Double,
                                                              destinationLongitude: availFleetBidDict["destinationLongitude"] as? Double,
                                                              vehicleType: nil, noOfVehicles: availFleetBidDict["noOfVehicles"] as? Int,
                                                              availableDateTime: availFleetBidDict["availableDateTime"] as? Int,
                                                              cargoType: nil,
                                                              capacityWeight:  availFleetBidDict["capacityWeight"] as? Int,
                                                              capacityWeightUnit: nil ,
                                                              loadType: nil,
                                                              expectedFreight: nil ,
                                                              currencyMaster: nil,
                                                              status: availFleetBidDict["status"] as? String,
                                                              registrationNo: availFleetBidDict["registrationNo"] as? String,
                                                              postType: availFleetBidDict["postType"] as? String,
                                                              bidInviteCount: availFleetBidDict["bidInviteCount"] as? Int, bidQuotationCount: nil,
                                                              containerNo: availFleetBidDict["containerNo"] as? String,
                                                              sealNo: availFleetBidDict["sealNo"] as? String, pinNo: nil, packgs: availFleetBidDict["packgs"] as? Int)
                            
                            
                            
                            vehicleDetails = VehicleContainerDetails1(id: vehicleDetailsDict["id"] as! Int,
                                                                      vehicleType: vehicleType,
                                                                      noOfVehicles: vehicleDetailsDict["noOfVehicles"] as! Int,
                                                                      grossWeight: Int(truncating: vehicleDetailsDict["grossWeight"] as! NSNumber),
                                                                      grossWeightUnit: grossWeightUnit,
                                                                      chargeableWeight: Int(truncating: vehicleDetailsDict["chargeableWeight"] as! NSNumber),
                                                                      chargeableWeightUnit: chargeableWTUnit,
                                                                      netWeight: Int(truncating: vehicleDetailsDict["netWeight"] as! NSNumber),
                                                                      netWeightUnit: netWeightUnit)
                            
                            
                            loadOwner = LoadOwner1(id: loadOwnerDict!["id"] as? Int, name: loadOwnerDict!["name"] as? String, registrationNo: loadOwnerDict!["registrationNo"] as? String, companyCode: loadOwnerDict!["companyCode"] as? String, telephone: loadOwnerDict!["telephone"] as? String, fax: loadOwnerDict!["fax"] as? String, address: nil, typeOfCompany: nil, logo: nil, gstTax: loadOwnerDict!["gstTax"] as? Double, gstTaxRegNo: loadOwnerDict!["gstTaxRegNo"] as? String, jurisdiction: loadOwnerDict!["jurisdiction"] as? String, timeZone: loadOwnerDict!["timeZone"] as? String, panNo: loadOwnerDict!["panNo"] as? String, vatNo: loadOwnerDict!["vatNo"] as? String, iecNo: loadOwnerDict!["iecNo"] as? String, cinNo: loadOwnerDict!["cinNo"] as? String, cstNo: loadOwnerDict!["cstNo"] as? String, registrationDate: loadOwnerDict!["registrationDate"] as? Int, subscriptionStartDate: loadOwnerDict!["subscriptionStartDate"] as? Int, subscriptionEndDate: loadOwnerDict!["subscriptionEndDate"] as? Int, currentBalance: loadOwnerDict!["currentBalance"] as? Int, smsNotification: loadOwnerDict!["smsNotification"] as? String, companyAccess: nil, loadAdditionalInformation: nil, status: loadOwnerDict!["status"] as? String, expiredType: loadOwnerDict!["expiredType"] as? String, activeUserCount: loadOwnerDict!["activeUserCount"] as? Int, maxUser: loadOwnerDict!["maxUser"] as? Int, maxUserType: loadOwnerDict!["maxUserType"] as? String, exchangeBookingCount: loadOwnerDict!["exchangeBookingCount"] as? Int, exWalletValidFrom: loadOwnerDict!["exWalletValidFrom"] as? Int, exWalletValidTill: loadOwnerDict!["exWalletValidTill"] as? Int, exWalletValidDays: loadOwnerDict!["exWalletValidDays"] as? Int, exPlanType: loadOwnerDict!["exPlanType"] as? String, goCount: loadOwnerDict!["goCount"] as? Int, proCount: loadOwnerDict!["proCount"] as? Int, profileMailCount: loadOwnerDict!["profileMailCount"] as? Int, ewbFlagStr: loadOwnerDict!["ewbFlagStr"] as? String, planType: loadOwnerDict!["planType"] as? String, exSuwalletAmount: loadOwnerDict!["exSuwalletAmount"] as? Int, exSuhubMaxUser: loadOwnerDict!["exSuhubMaxUser"] as? Int, exSuallowTransaction: loadOwnerDict!["exSuallowTransaction"] as? Int, exSuremainsTr: loadOwnerDict!["exSuremainsTr"] as? Int, hubPlanID: loadOwnerDict!["hubPlanID"] as? Int, exSuhubTransactionCost: loadOwnerDict!["exSuhubTransactionCost"] as? Int, exPackType: loadOwnerDict!["exPackType"] as? String)
                            
                            
                            fleetOwner = FleetOwner1(id: fleetOwnerDict!["id"] as? Int, name: fleetOwnerDict!["name"] as? String, registrationNo: fleetOwnerDict!["registrationNo"] as? String, panNo: fleetOwnerDict!["panNo"] as? String, fax: fleetOwnerDict!["fax"] as? String, telephone: fleetOwnerDict!["telephone"] as? String, typeOfCompany: nil, fleetAdditionalInformation: nil, address: nil, userCount: fleetOwnerDict!["userCount"] as? Int, activeUserCount: fleetOwnerDict!["activeUserCount"] as? Int, companyCode: fleetOwnerDict!["companyCode"] as? String, logo: nil, gstTax: fleetOwnerDict!["gstTax"] as? Int, gstTaxRegNo: fleetOwnerDict!["gstTaxRegNo"] as? String, jurisdiction: fleetOwnerDict!["jurisdiction"] as? String, timeZone: fleetOwnerDict!["timeZone"] as? String, vatNo: fleetOwnerDict!["vatNo"] as? String, iecNo: fleetOwnerDict!["iecNo"] as? String, cinNo: fleetOwnerDict!["cinNo"] as? String, cstNo: fleetOwnerDict!["cstNo"] as? String, registrationDate: fleetOwnerDict!["registrationDate"] as? Int, expiredType: fleetOwnerDict!["expiredType"] as? String, subscriptionStartDate: fleetOwnerDict!["subscriptionStartDate"] as? Int, subscriptionEndDate: fleetOwnerDict!["subscriptionEndDate"] as? Int, perTransactionFee: fleetOwnerDict!["perTransactionFee"] as? Int, smsCharge: fleetOwnerDict!["smsCharge"] as? Int, currentBalance: fleetOwnerDict!["currentBalance"] as? Int, bookingCount: fleetOwnerDict!["bookingCount"] as? Int, purchasePaymentCount: fleetOwnerDict!["purchasePaymentCount"] as? Int, purchaseReceiptCount: fleetOwnerDict!["purchaseReceiptCount"] as? Int, routeCount: fleetOwnerDict!["routeCount"] as? Int, smsNotification: fleetOwnerDict!["smsNotification"] as? String, maxUser: fleetOwnerDict!["maxUser"] as? Int, maxUserType: fleetOwnerDict!["maxUserType"] as? String, status: fleetOwnerDict!["status"] as? String, contractMasterCount: fleetOwnerDict!["contractMasterCount"] as? Int, goCount: fleetOwnerDict!["goCount"] as? Int, proCount: fleetOwnerDict!["proCount"] as? Int, profileMailCount: fleetOwnerDict!["profileMailCount"] as? Int, ewbFlagStr: fleetOwnerDict!["ewbFlagStr"] as? String, walletAmount: fleetOwnerDict!["walletAmount"] as? Int, hubMaxUser: fleetOwnerDict!["hubMaxUser"] as? Int, allowTransaction: fleetOwnerDict!["allowTransaction"] as? Int, remainsTr: fleetOwnerDict!["remainsTr"] as? Int, hubPlanID: fleetOwnerDict!["hubPlanID"] as? Int, hubTransactionCost: fleetOwnerDict!["hubTransactionCost"] as? Int, thirdPartyAuthDateTime: fleetOwnerDict!["thirdPartyAuthDateTime"] as? Int, exPurchasePayCount: fleetOwnerDict!["exPurchasePayCount"] as? Int, exPurchaseReceiptCount: fleetOwnerDict!["exPurchaseReceiptCount"] as? Int, exWalletValidFrom: fleetOwnerDict!["exWalletValidFrom"] as? Int, exWalletValidTill: fleetOwnerDict!["exWalletValidTill"] as? Int, exWalletValidDays: fleetOwnerDict!["exWalletValidDays"] as? Int, exPlanType: fleetOwnerDict!["exPlanType"] as? String, exPackType: fleetOwnerDict!["exPackType"] as? String, userCounts: fleetOwnerDict!["userCounts"] as? Int)
                            
                            loadOwnerShipper = LoadOwner1(id: loadOwnerShipperDict["id"] as? Int, name: loadOwnerShipperDict["name"] as? String, registrationNo: loadOwnerShipperDict["registrationNo"] as? String, companyCode: loadOwnerShipperDict["companyCode"] as? String, telephone: loadOwnerShipperDict["telephone"] as? String, fax: loadOwnerShipperDict["fax"] as? String, address: nil, typeOfCompany: nil, logo: nil, gstTax: loadOwnerShipperDict["gstTax"] as? Double, gstTaxRegNo: loadOwnerShipperDict["gstTaxRegNo"] as? String, jurisdiction: loadOwnerShipperDict["jurisdiction"] as? String, timeZone: loadOwnerShipperDict["timeZone"] as? String, panNo: loadOwnerShipperDict["panNo"] as? String, vatNo: loadOwnerShipperDict["vatNo"] as? String, iecNo: loadOwnerShipperDict["iecNo"] as? String, cinNo: loadOwnerShipperDict["cinNo"] as? String, cstNo: loadOwnerShipperDict["cstNo"] as? String, registrationDate: loadOwnerShipperDict["registrationDate"] as? Int, subscriptionStartDate: loadOwnerShipperDict["subscriptionStartDate"] as? Int, subscriptionEndDate: loadOwnerShipperDict["subscriptionEndDate"] as? Int, currentBalance: loadOwnerShipperDict["currentBalance"] as? Int, smsNotification: loadOwnerShipperDict["smsNotification"] as? String, companyAccess: nil, loadAdditionalInformation: nil, status: loadOwnerShipperDict["status"] as? String, expiredType: loadOwnerShipperDict["expiredType"] as? String, activeUserCount: loadOwnerShipperDict["activeUserCount"] as? Int, maxUser: loadOwnerShipperDict["maxUser"] as? Int, maxUserType: loadOwnerShipperDict["maxUserType"] as? String, exchangeBookingCount: loadOwnerShipperDict["exchangeBookingCount"] as? Int, exWalletValidFrom: loadOwnerShipperDict["exWalletValidFrom"] as? Int, exWalletValidTill: loadOwnerShipperDict["exWalletValidTill"] as? Int, exWalletValidDays: loadOwnerShipperDict["exWalletValidDays"] as? Int, exPlanType: loadOwnerShipperDict["exPlanType"] as? String, goCount: loadOwnerShipperDict["goCount"] as? Int, proCount: loadOwnerShipperDict["proCount"] as? Int, profileMailCount: loadOwnerShipperDict["profileMailCount"] as? Int, ewbFlagStr: loadOwnerShipperDict["ewbFlagStr"] as? String, planType: loadOwnerShipperDict["planType"] as? String, exSuwalletAmount: loadOwnerShipperDict["exSuwalletAmount"] as? Int, exSuhubMaxUser: loadOwnerShipperDict["exSuhubMaxUser"] as? Int, exSuallowTransaction: loadOwnerShipperDict["exSuallowTransaction"] as? Int, exSuremainsTr: loadOwnerShipperDict["exSuremainsTr"] as? Int, hubPlanID: loadOwnerShipperDict["hubPlanID"] as? Int, exSuhubTransactionCost: loadOwnerShipperDict["exSuhubTransactionCost"] as? Int, exPackType: loadOwnerShipperDict["exPackType"] as? String)
                            
                            loadOwnerConsignee = LoadOwner1(id: loadownerConsigneeDict["id"] as? Int, name: loadownerConsigneeDict["name"] as? String, registrationNo: loadownerConsigneeDict["registrationNo"] as? String, companyCode: loadownerConsigneeDict["companyCode"] as? String, telephone: loadownerConsigneeDict["telephone"] as? String, fax: loadownerConsigneeDict["fax"] as? String, address: nil, typeOfCompany: nil, logo: nil, gstTax: loadownerConsigneeDict["gstTax"] as? Double, gstTaxRegNo: loadownerConsigneeDict["gstTaxRegNo"] as? String, jurisdiction: loadownerConsigneeDict["jurisdiction"] as? String, timeZone: loadownerConsigneeDict["timeZone"] as? String, panNo: loadownerConsigneeDict["panNo"] as? String, vatNo: loadownerConsigneeDict["vatNo"] as? String, iecNo: loadownerConsigneeDict["iecNo"] as? String, cinNo: loadownerConsigneeDict["cinNo"] as? String, cstNo: loadownerConsigneeDict["cstNo"] as? String, registrationDate: loadownerConsigneeDict["registrationDate"] as? Int, subscriptionStartDate: loadownerConsigneeDict["subscriptionStartDate"] as? Int, subscriptionEndDate: loadownerConsigneeDict["subscriptionEndDate"] as? Int, currentBalance: loadownerConsigneeDict["currentBalance"] as? Int, smsNotification: loadownerConsigneeDict["smsNotification"] as? String, companyAccess: nil, loadAdditionalInformation: nil, status:loadownerConsigneeDict["status"] as? String, expiredType: loadownerConsigneeDict["expiredType"] as? String, activeUserCount: loadownerConsigneeDict["activeUserCount"] as? Int, maxUser: loadownerConsigneeDict["maxUser"] as? Int, maxUserType: loadownerConsigneeDict["maxUserType"] as? String, exchangeBookingCount: loadownerConsigneeDict["exchangeBookingCount"] as? Int, exWalletValidFrom: loadownerConsigneeDict["exWalletValidFrom"] as? Int, exWalletValidTill: loadownerConsigneeDict["exWalletValidTill"] as? Int, exWalletValidDays: loadownerConsigneeDict["exWalletValidDays"] as? Int, exPlanType: loadownerConsigneeDict["exPlanType"] as? String, goCount: loadownerConsigneeDict["goCount"] as? Int, proCount: loadownerConsigneeDict["proCount"] as? Int, profileMailCount: loadownerConsigneeDict["profileMailCount"] as? Int, ewbFlagStr: loadownerConsigneeDict["ewbFlagStr"] as? String, planType: loadownerConsigneeDict["planType"] as? String, exSuwalletAmount: loadownerConsigneeDict["exSuwalletAmount"] as? Int, exSuhubMaxUser: loadownerConsigneeDict["exSuhubMaxUser"] as? Int, exSuallowTransaction: loadownerConsigneeDict["exSuallowTransaction"] as? Int, exSuremainsTr: loadownerConsigneeDict["exSuremainsTr"] as? Int, hubPlanID: loadownerConsigneeDict["hubPlanID"] as? Int, exSuhubTransactionCost: loadownerConsigneeDict["exSuhubTransactionCost"] as? Int, exPackType: loadownerConsigneeDict["exPackType"] as? String)
                            
                            loadOwnerDeliveryCustomer = LoadOwner1(id: loadownerDeliveryCustomerDict["id"] as? Int, name: loadownerDeliveryCustomerDict["name"] as? String, registrationNo: loadownerDeliveryCustomerDict["registrationNo"] as? String, companyCode: loadownerDeliveryCustomerDict["companyCode"] as? String, telephone: loadownerDeliveryCustomerDict["telephone"] as? String, fax: loadownerDeliveryCustomerDict["fax"] as? String, address: nil, typeOfCompany: nil, logo: nil, gstTax: loadownerDeliveryCustomerDict["gstTax"] as? Double, gstTaxRegNo: loadownerDeliveryCustomerDict["gstTaxRegNo"] as? String, jurisdiction: loadownerDeliveryCustomerDict["jurisdiction"] as? String, timeZone: loadownerDeliveryCustomerDict["timeZone"] as? String, panNo: loadownerDeliveryCustomerDict["panNo"] as? String, vatNo: loadownerDeliveryCustomerDict["vatNo"] as? String, iecNo: loadownerDeliveryCustomerDict["iecNo"] as? String, cinNo: loadownerDeliveryCustomerDict["cinNo"] as? String, cstNo: loadownerDeliveryCustomerDict["cstNo"] as? String, registrationDate: loadownerDeliveryCustomerDict["registrationDate"] as? Int, subscriptionStartDate: loadownerDeliveryCustomerDict["subscriptionStartDate"] as? Int, subscriptionEndDate: loadownerDeliveryCustomerDict["subscriptionEndDate"] as? Int, currentBalance: loadownerDeliveryCustomerDict["currentBalance"] as? Int, smsNotification: loadownerDeliveryCustomerDict["smsNotification"] as? String, companyAccess: nil, loadAdditionalInformation: nil, status:loadownerDeliveryCustomerDict["status"] as? String, expiredType: loadownerDeliveryCustomerDict["expiredType"] as? String, activeUserCount: loadownerDeliveryCustomerDict["activeUserCount"] as? Int, maxUser: loadownerDeliveryCustomerDict["maxUser"] as? Int, maxUserType: loadownerDeliveryCustomerDict["maxUserType"] as? String, exchangeBookingCount: loadownerDeliveryCustomerDict["exchangeBookingCount"] as? Int, exWalletValidFrom: loadownerDeliveryCustomerDict["exWalletValidFrom"] as? Int, exWalletValidTill: loadownerDeliveryCustomerDict["exWalletValidTill"] as? Int, exWalletValidDays: loadownerDeliveryCustomerDict["exWalletValidDays"] as? Int, exPlanType: loadownerDeliveryCustomerDict["exPlanType"] as? String, goCount: loadownerDeliveryCustomerDict["goCount"] as? Int, proCount: loadownerDeliveryCustomerDict["proCount"] as? Int, profileMailCount: loadownerDeliveryCustomerDict["profileMailCount"] as? Int, ewbFlagStr: loadownerDeliveryCustomerDict["ewbFlagStr"] as? String, planType: loadownerDeliveryCustomerDict["planType"] as? String, exSuwalletAmount: loadownerDeliveryCustomerDict["exSuwalletAmount"] as? Int, exSuhubMaxUser: loadownerDeliveryCustomerDict["exSuhubMaxUser"] as? Int, exSuallowTransaction: loadownerDeliveryCustomerDict["exSuallowTransaction"] as? Int, exSuremainsTr: loadownerDeliveryCustomerDict["exSuremainsTr"] as? Int, hubPlanID: loadownerDeliveryCustomerDict["hubPlanID"] as? Int, exSuhubTransactionCost: loadownerDeliveryCustomerDict["exSuhubTransactionCost"] as? Int, exPackType: loadownerDeliveryCustomerDict["exPackType"] as? String)
                            
                            
                            shipper = Shipper(id: shipperDict["id"] as? Int, name: shipperDict["name"] as? String, address: shipperDict["address"] as? String, location: shipperDict["location"] as? String, state: shipperDict["state"] as? String, contactPerson: shipperDict["contactPerson"] as? String, contactNo: shipperDict["contactNo"] as? String, loadOwner: loadOwnerShipper)
                            
                            consignee = Consignee(id: consigneeDict["id"] as? Int, name: consigneeDict["name"] as? String, address: consigneeDict["address"] as? String, location: consigneeDict["location"] as? String, state: consigneeDict["state"] as? String, contactPerson: consigneeDict["contactPerson"] as? String, contactNo: consigneeDict["contactNo"] as? String, loadOwner: loadOwnerConsignee, gstNo: consigneeDict["gstNo"] as? String, pinNo: consigneeDict["pinNo"] as? String, country: consigneeDict["country"] as? String, createDate: consigneeDict["createDate"] as? Int, updateDate: consigneeDict["updateDate"] as? Int, secAddress: consigneeDict["secAddress"] as? String, contactEmail: consigneeDict["contactEmail"] as? String, cinNo: consigneeDict["cinNo"] as? String, panNo: consigneeDict["panNo"] as? String)
                            
                            deliveryCustomer = Consignee(id: deliveryCustomerDict["id"] as? Int, name: deliveryCustomerDict["name"] as? String, address: deliveryCustomerDict["address"] as? String, location: deliveryCustomerDict["location"] as? String, state: deliveryCustomerDict["state"] as? String, contactPerson: deliveryCustomerDict["contactPerson"] as? String, contactNo: deliveryCustomerDict["contactNo"] as? String, loadOwner: loadOwnerDeliveryCustomer, gstNo: deliveryCustomerDict["gstNo"] as? String, pinNo: deliveryCustomerDict["pinNo"] as? String, country: deliveryCustomerDict["country"] as? String, createDate: deliveryCustomerDict["createDate"] as? Int, updateDate: deliveryCustomerDict["updateDate"] as? Int, secAddress: deliveryCustomerDict["secAddress"] as? String, contactEmail: deliveryCustomerDict["contactEmail"] as? String, cinNo: deliveryCustomerDict["cinNo"] as? String, panNo: deliveryCustomerDict["panNo"] as? String)
                            
                            freightTerm = FreightTermsClass(id: freightTermDict["id"] as? Int, freightTerms: freightTermDict["freightTerms"] as? String)
                            
                            exTaxInfo = ExTaxInfo(taxDetails: taxDetailArray, taxGroup: taxGroup, taxs: tax, taxType: exTaxInfojson["taxType"].string)
                            
                            let booking = BookingElement(id: data["id"].int, shipper: shipper, consignee: consignee, deliveryCustomer: deliveryCustomer, loadOwner: loadOwner, fleetOwner: fleetOwner, billingParty: data["billingParty"].stringValue, jobStatus: jobStatus, loadType: loadType, cargoType: cargoType, jobType: jobType, bookingType: bookingType, pickupDateTime: data["pickupDateTime"].int, deliveryDateTime: data["deliveryDateTime"].int, freightTerms: freightTerm, bookingNo: data["bookingNo"].stringValue, availableLoadBid: availableLoadBid, availableFleetBid: availbaleFleetBid, loadBidQuotaion: nil, pickUpLocation: data["pickUpLocation"].stringValue, destination: data["destination"].stringValue, createDate: data["createDate"].int, vehicleContainerDetails: vehicleDetails, user: nil, bookingCust: data["bookingCust"].bool, shiper: data["shiper"].bool, consigneer: data["consigneer"].bool, deliverCust: data["deliverCust"].bool, exInvoiceGrossAmount: data["exInvoiceGrossAmount"].int, exInvoiceGrossAmountWithoutTax: data["exInvoiceGrossAmountWithoutTax"].int, exTaxType: data["exTaxType"].stringValue, exTaxInfo: exTaxInfo , exInvoiceNo: data["exInvoiceNo"].stringValue, exTaxGroup: nil, invoiceDate: data["invoiceDate"].int, financeYear: data["financeYear"].stringValue, salesBillFinal: data["salesBillFinal"].bool, purchaseBillFinal: data["purchaseBillFinal"].bool, defaultBankAndBranch: data["defaultBankAndBranch"].stringValue, count: data["count"].intValue, lrNo: data["lrNo"].int, isinvite: data["isinvite"].bool, eLrTerms: data["eLrTerms"].stringValue, salesInvoiceTerms: data["salesInvoiceTerms"].stringValue, jobVehiclesDrivers: jobVehiclesDrivers, trips: nil, fleetBidInvite: nil)
                            
                            print("jobId: ========----- \(String(describing: data["id"].int))")
                            if self.bookingList.contains(where: { boooking in boooking.id == booking.id  }) {
                                
                            } else {
                                
                                self.bookingList.append(booking)
                            }
                            
                        }
                        
                        self.assignDetailArray.removeAll()
                        for booking in self.bookingList{
                            let assign = AssignDatum(id: booking.id, jobStatus: booking.jobStatus, loadType: booking.loadType, cargoType: booking.cargoType, pickupDateTime: booking.pickupDateTime, deliveryDateTime: booking.deliveryDateTime, bookingNo: booking.bookingNo, pickUpLocation: booking.pickUpLocation, destination: booking.destination, createDate: booking.createDate, vehicleContainerDetails: booking.vehicleContainerDetails, bookingCust: booking.bookingCust, shiper: booking.shiper, consigneer: booking.consigneer, deliverCust: booking.deliverCust, salesBillFinal: booking.salesBillFinal, purchaseBillFinal: booking.purchaseBillFinal, count: booking.count, lrNo: booking.lrNo, fleetBidCode: booking.availableFleetBid?.bidCode, loadBidCode: booking.availableLoadBid?.bidCode, inviteAmount: Double(booking.availableFleetBid?.expectedFreight ?? Int(0.0)), fleetBidID: booking.availableFleetBid?.id, isinvite: booking.isinvite, registrationNo: booking.availableFleetBid?.registrationNo, telephone: booking.loadOwner?.telephone, packgs: booking.availableFleetBid?.packgs, quoteAmount: booking.loadBidQuotaion?.quotationAmount)
                            
                            self.assignDetailArray.append(assign)
                        }
                        
                        print("assignDetailArray ======= \(self.assignDetailArray.count)")
                        self.bookingCollectionView.reloadData()
                    }
                    break
                case .failure(let error):
                    
                    print("== error == \(error.localizedDescription)")
                    break
                }
        }
        
    }
    
    
    @IBAction func showDropDownMenu(_ sender: Any) {
        isMenuVisible = !isMenuVisible
        UIView.animate(withDuration: 0.8) {
            self.menuView.isHidden = self.isMenuVisible
        }
    }
    
    @IBAction func menuClicked(_ sender: Any) {
        self.bookingCollectionView.isScrollEnabled = true
        self.containerView.isUserInteractionEnabled = true
                  self.ftlView.isUserInteractionEnabled = true
        sideMenuController?.setContentViewController(with: "sixth")
    }
    
    @IBAction func searchClicked(_ sender: Any) {
        sideMenuController?.setContentViewController(with: "16")
        NotificationCenter.default.post(name: .search ,object: nil,userInfo: ["loadTypeName" : self.loadTypeName,"isTractClicked": self.istrackTransitClicked])
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = bookingCollectionView.dequeueReusableCell(withReuseIdentifier: "BookingCell", for: indexPath) as! BookingCell
        
        cell.bookingButton.isHidden = false
        if bookingList.count != 0 {
            
            
            let bookingList = self.bookingList[indexPath.item]
            cell.id.text = bookingList.availableLoadBid?.bidCode
            cell.bookingNo.text = bookingList.bookingNo
            let timeStamp = Double(bookingList.pickupDateTime! / 1000)
            let pickupdate = self.epochDate(epochDate: timeStamp)
            let pickupTime = self.epochTime(epochTimee: timeStamp)
            cell.pickupTime.text = pickupTime
            cell.pickingDate.text = pickupdate
            cell.shipper.text = bookingList.pickUpLocation
            cell.delivery.text = bookingList.destination
            
            if self.istrackTransitClicked == true{
                cell.bookingButton.backgroundColor = UIColor(hexString: ColorConstants.LIGHT_BLUE)
                cell.bookingButtonLabel.text = "TRACK"
                cell.viewPodButton.isHidden = true
                self.callDriverView.isUserInteractionEnabled = true
            }else {
                if bookingList.jobStatus?.jobStatus == "Assigned" || bookingList.jobStatus?.jobStatus == "In Progress"{
                    cell.bookingButton.backgroundColor = UIColor(hexString: ColorConstants.YELLOW)
                    cell.bookingButtonLabel.text = "ASSIGNED"
                    cell.viewPodButton.isHidden = true
                    self.callDriverView.isUserInteractionEnabled = true
                }else if bookingList.jobStatus?.jobStatus == "New Booking"{
                    cell.bookingButton.backgroundColor = UIColor(hexString: ColorConstants.GREEN)
                    cell.bookingButtonLabel.text = "ASSIGN"
                    self.callDriverView.isUserInteractionEnabled = false
                    cell.viewPodButton.isHidden = true
                    
                }else if bookingList.jobStatus?.jobStatus == "Completed"{
                    cell.bookingButton.backgroundColor = UIColor(hexString: ColorConstants.RED)
                    cell.bookingButtonLabel.text = "COMPLETED"
                    cell.viewPodButton.isHidden = false
                    self.callDriverView.isUserInteractionEnabled = false
                }
            }
        }
        
        let trackRecognizer = UITapGestureRecognizer(target: self, action: #selector(track(recognizer:)))
        cell.bookingButton.addGestureRecognizer(trackRecognizer)
        cell.bookingButton.tag = indexPath.row
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(viewPodClicked(recognizer:)))
        cell.viewPodButton.addGestureRecognizer(recognizer)
        cell.viewPodButton.tag = indexPath.item
        return cell
        
    }
    
    @objc func assigned(recognizer: UITapGestureRecognizer){
        sideMenuController?.setContentViewController(with: "19")
    }
    
    @objc func track(recognizer: UITapGestureRecognizer){
        let tag = recognizer.view?.tag
        
        if istrackTransitClicked == true{
            let booking  = ["booking":self.bookingList[tag!],"state" : "Track"] as [String : Any]
            sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "TrackViewController") }, with: "twelve")
            sideMenuController?.setContentViewController(with: "twelve")
            NotificationCenter.default.post(name: .track ,object: nil,userInfo: booking)
        }else if bookingList[tag!].jobStatus?.jobStatus == "New Booking" && istrackTransitClicked == false {
            let assignDetail  = ["assignDetail":self.assignDetailArray[tag!],"state" : "Booking"] as [String : Any]
            sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "AssignViewController") }, with: "18")
            sideMenuController?.setContentViewController(with: "18")
            NotificationCenter.default.post(name: .assign ,object: nil,userInfo: assignDetail)
            
        }else if bookingList[tag!].jobStatus?.jobStatus == "Assigned" || bookingList[tag!].jobStatus?.jobStatus == "In Progress" && istrackTransitClicked == false{
            let assignDetail  = ["assignDetail":self.bookingList[tag!],"state" : "Booking"] as [String : Any]
            sideMenuController?.setContentViewController(with: "19")
            NotificationCenter.default.post(name: .uploadPod ,object: nil,userInfo: assignDetail)
        }
        
    }
    
    @objc func assign(recognizer: UITapGestureRecognizer){
        sideMenuController?.setContentViewController(with: "18")
    }
    
    @objc func viewPodClicked(recognizer: UITapGestureRecognizer){
        print("view pod")
        let tag = recognizer.view?.tag
        let booking = self.bookingList[tag!].jobVehiclesDrivers
        let podId = booking![0].podID?.id
        
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let presentedViewController = storyBoard.instantiateViewController(withIdentifier: "ViewPODViewController") as! ViewPODViewController
        presentedViewController.providesPresentationContextTransitionStyle = true
        presentedViewController.photoId = "\(podId!)"
        presentedViewController.definesPresentationContext = true
        presentedViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
        presentedViewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        
        self.present(presentedViewController, animated: true, completion: nil)
        //  sideMenuController?.setContentViewController(with: "17")
    }
    
    @IBAction func makeInvoiceClicked(_ sender: Any) {
        
        let indexPath = bookingCollectionView.indexPathsForVisibleItems
        print("=======IndexPath cell=======")
        print(indexPath[0].row)
        let booking = self.bookingList[indexPath[0].row]
        
        if booking.salesBillFinal! == false{
            sideMenuController?.setContentViewController(with: "eleven")
            NotificationCenter.default.post(name:.makeInvoice, object: nil,userInfo: ["booking" : booking])
        }else{
            sideMenuController?.setContentViewController(with: "31")
            NotificationCenter.default.post(name: .viewInvoice, object: nil,userInfo: ["booking" : booking])
            
            print("salesBillFinal == true")
        }
    }
    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    //        return CGSize(width: (self.view.frame.width), height: (self.bookingCollectionView.frame.height - 20))
    //    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let noOfCellsInRow = 1
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
        
        let size = Int((bookingCollectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        
        return CGSize(width: size, height: size)
    }
    
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
