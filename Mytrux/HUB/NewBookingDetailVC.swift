//
//  NewBookingDetailVC.swift
//  Mytrux
//
//  Created by Mytrux on 27/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import SideMenuSwift

class NewBookingDetailVC: BaseViewController {

    @IBOutlet weak var bookingDetailsView: UIView!
    @IBOutlet weak var transpoterView: UIView!
    @IBOutlet weak var vehicleDetailsView: UIView!
    @IBOutlet weak var createBookingBtn: UIView!
    @IBOutlet weak var shipperView: UIView!
    @IBOutlet weak var shipperLineView: UIView!
    @IBOutlet weak var deliveryView: UIView!
    @IBOutlet weak var nextBtnLabel: UILabel!
    @IBOutlet weak var nextBtn: UIView!
    @IBOutlet weak var createBookingWrraper: UIView!
    @IBOutlet weak var nextWraper: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cardViewArray(arrView: [shipperView,deliveryView])
        buttonCardArray(arrView: [nextBtn])
        createBookingBtn.layer.cornerRadius = 5
        createBookingBtn.layer.shadowColor = UIColor.gray.cgColor
        createBookingBtn.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
        createBookingBtn.layer.shadowRadius = 5
        createBookingBtn.layer.shadowOpacity = 0.7
          shipperLineView.roundCorners(corners: [.topRight, .bottomRight], radius: 5.0)
        bookingDetailsView.isHidden = true
        vehicleDetailsView.isHidden = true
        self.createBookingWrraper.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func createBookingClicked(_ sender: Any) {
        
    }
    
    @IBAction func backClicked(_ sender: Any) {
     backEvent()
    }
    func backEvent(){
        self.createBookingWrraper.isHidden = true
        self.nextWraper.isHidden = false
        if transpoterView.isHidden == false{
             SideMenuController.preferences.basic.defaultCacheKey = "default"
        sideMenuController?.setContentViewController(with: "default")
        }else if bookingDetailsView.isHidden == false{
             NotificationCenter.default.post(name: Notification.Name("bookingDetailsReverse"), object: nil)
            transpoterView.isHidden = false
            bookingDetailsView.isHidden = true
            vehicleDetailsView.isHidden = true
        }else if vehicleDetailsView.isHidden == false{
             NotificationCenter.default.post(name: Notification.Name("vehicleDetailsReverse"), object: nil)
            transpoterView.isHidden = true
            bookingDetailsView.isHidden = false
            vehicleDetailsView.isHidden = true
        }
    }
    @IBAction func backBtn(_ sender: Any) {
         backEvent()
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        self.createBookingWrraper.isHidden = true
        self.nextWraper.isHidden = false
        if transpoterView.isHidden == false{
            NotificationCenter.default.post(name: Notification.Name("bookingDetails"), object: nil)
            transpoterView.isHidden = true
            bookingDetailsView.isHidden = false
            vehicleDetailsView.isHidden = true
        }else if bookingDetailsView.isHidden == false{
              NotificationCenter.default.post(name: Notification.Name("vehicleDetails"), object: nil)
            transpoterView.isHidden = true
            bookingDetailsView.isHidden = true
            vehicleDetailsView.isHidden = false
            self.createBookingWrraper.isHidden = false
            self.nextWraper.isHidden = true
        }else if vehicleDetailsView.isHidden == false{
            
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
