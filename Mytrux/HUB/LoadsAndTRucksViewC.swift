//
//  LoadsAndTRucksViewC.swift
//  Mytrux
//
//  Created by Mukta Bhuyar Punjabi on 17/05/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class LoadsAndTRucksViewC: BaseViewController {

    @IBOutlet weak var loadsTruckStackView: UIStackView!
    @IBOutlet weak var backButton: UIView!
    @IBOutlet weak var back: UIButton!
    var viewArray = [UIView]()
    @IBOutlet weak var trucksView: UIView!
    @IBOutlet weak var loadsView: UIView!
    @IBOutlet weak var topBar: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
       topBarCard(topBar: topBar)
  UserDefaults.standard.removeObject(forKey: UserDefaultsContants.MTX_Load_STATE)
        UserDefaults.standard.synchronize()
        let backtap = UITapGestureRecognizer(target: self, action: #selector(self.handleBackButtonTap(_:)))
        backButton.addGestureRecognizer(backtap)
        backButton.isUserInteractionEnabled = true
        self.topBar.addSubview(backButton)
        viewArray = [loadsView,trucksView]
        cardViewArray(arrView: viewArray)
        if loadImageFromDiskWith(fileName: "profilePhoto") != nil && loadImageFromDiskWith(fileName: "panPhoto") != nil {
            self.clearAllFilesFromTempDirectory(imageName: "profilePhoto", image: loadImageFromDiskWith(fileName: "profilePhoto")!)
            self.clearAllFilesFromTempDirectory(imageName: "panPhoto", image: loadImageFromDiskWith(fileName: "panPhoto")!)
        }
      
        let state = UserDefaults.standard.value(forKey: UserDefaultsContants.MTX_STATE)! as! String
        print("state = \(state)")
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleFindTrucks(_:)))
         let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.handleFindLoad(_:)))
        loadsView.addGestureRecognizer(tap1)
          trucksView.addGestureRecognizer(tap)
        loadsView.isUserInteractionEnabled = true
            trucksView.isUserInteractionEnabled = true
        self.loadsTruckStackView.addSubview(loadsView)
       self.loadsTruckStackView.addSubview(trucksView)
        
            }
    func loadImageFromDiskWith(fileName: String) -> UIImage? {
        
        let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
        
        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
        
        if let dirPath = paths.first {
            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image
            
        }
        
        return nil
    }
    
    func clearAllFilesFromTempDirectory(imageName: String,image: UIImage){
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        
        let fileName = imageName
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        //Checks if file exists, removes it if so.
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(atPath: fileURL.path)
                print("Removed old image")
            } catch let removeError {
                print("couldn't remove file at path", removeError)
            }
            
        }
    }
    @objc func handleBackButtonTap(_ sender: UITapGestureRecognizer) {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier :"SelectServiceVC")
        viewController.modalPresentationStyle = .fullScreen
                transitionVc(vc: viewController, duration: 0.5, type: .fromLeft)
    }
    
    var state = CoreDataManager.getUserState()
    
    @objc func handleFindLoad(_ sender: UITapGestureRecognizer) {
       UserDefaults.standard.set(UserStates.HUB_LOAD, forKey: UserDefaultsContants.MTX_Load_STATE)
        UserDefaults.standard.set("Service Provider", forKey: UserDefaultsContants.SERVICE_STATE)
        UserDefaults.standard.set(UserStates.SERVICE_PROVIDER, forKey: UserDefaultsContants.LOGIN_USER_TYPE)
        UserDefaults.standard.synchronize()
        if state == UserStates.HUB{
            CoreDataManager.updateState(state:UserStates.HUB_LOAD)
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"selectProfileVC")
        viewController.modalPresentationStyle = .fullScreen
        transitionVc(vc: viewController, duration: 0.5, type: .fromRight)
    }
    
    @objc func handleFindTrucks(_ sender: UITapGestureRecognizer) {
        UserDefaults.standard.set(UserStates.HUB_TRUCK, forKey: UserDefaultsContants.MTX_Load_STATE)
          UserDefaults.standard.set("Service User", forKey: UserDefaultsContants.SERVICE_STATE)
        UserDefaults.standard.set(UserStates.SERVICE_USER, forKey: UserDefaultsContants.LOGIN_USER_TYPE)
        UserDefaults.standard.synchronize()
        if state == UserStates.HUB{
          //  CoreDataManager.setHubState(hubState: Hub(hub_load_owner: false, hub_truck_owner: true))
            CoreDataManager.updateState(state: UserStates.HUB_TRUCK)
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"selectProfileVC")
        viewController.modalPresentationStyle = .fullScreen
        transitionVc(vc: viewController, duration: 0.5, type: .fromRight)
    }

   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
