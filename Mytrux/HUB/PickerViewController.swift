//
//  PickerViewController.swift
//  Mytrux
//
//  Created by Mytrux on 20/08/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class PickerViewController: BaseViewController,UIPickerViewDelegate,UIPickerViewDataSource{
    
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var typePicker: UIPickerView!
    @IBOutlet weak var timePicker: UIPickerView!
    @IBOutlet weak var pickerWrraperView: UIView!
    var cargoType = [String]()
    var billingParty = [String]()
    var unitType = [String]()
    var billingHead = [String]()
    var selectedUnitType = String()
    var selectedBillingHead = String()
    var selectedCargoType = String()
    var selectedCity = String()
    var selectedState = String()
    var selectedBillingParty = String()
    var pickerState = String()
    var timeArray = [[String]]()
    var selectedTime = String()
    var selectedTimeType = String()
    var taxArray = [String]()
    var selectedTax = String()
    var amPmArray = [String]()
    var stateArray = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.vehichleCapacityUnit()
            self.getCargoType()
            self.exBillMasterList()
            self.tax()
        }
        timePicker.isHidden = true
        datePicker.isHidden = true
        typePicker.isHidden = true
        timePicker.delegate = self
        timePicker.dataSource = self
        typePicker.delegate = self
        typePicker.dataSource = self
        datePicker.datePickerMode = .date
        timeArray = [["1:00","1:30","2:00","2:30","3:00","3:30","4:00","4:30","5:00","5:30","6:00","6:30","7:00","7:30","8:00","8:30","9:00","9:30","10:00","10:30","11:00","11:30","12:00","12:30"],["AM","PM"]]
        amPmArray = ["AM","PM"]
        billingParty = ["BOOKING CUSTOMER","CONSIGNEE","CONSIGNOR/SHIPPER","DELIVERY CUSTOMER"]
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        cardViewArray(arrView: [pickerWrraperView])
        let states = appDelegate.getAllStateList()
              
              for state in states{
                  self.stateArray.append(state)
              }
        //            userDefaults.set("", forKey: "selectedTime")
        //            userDefaults.set("", forKey: "selectedTimeType")
        //            userDefaults.set("", forKey: "PostFleetFromTime")
        //            userDefaults.set("", forKey: "PostFleetFromTimeType")
        //            userDefaults.set("", forKey: "PostFleetTillTime")
        //            userDefaults.set("", forKey: "PostFleetFromTillType")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.paymentSelectedState(notification:)), name: .paymentSelectedState, object: nil)
        // Do any additional setup after loading the view.
    }
    
     var city = [String]()
    @objc func paymentSelectedState(notification: Notification) {
           
            let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
           let state = userInfo!["selectedState"] as! String
            print("paymentState========= \(state)")
        self.selectedState = state
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        let cities = appDelegate.getCity()
       
        for city in cities!{
            if state == city.state {
                print("\(city.state)")
                print("\(city.city)")
                self.city = city.city
            }
        }
         
       }
    
    override func viewWillAppear(_ animated: Bool) {
        if pickerState == "TimePicker" || pickerState == "TimePickerFrom" || pickerState == "TimePickerTill" {
            timePicker.isHidden = false
        }else if pickerState == "DatePicker" || pickerState == "DatePickerFrom" || pickerState == "DatePickerTill" || pickerState == "fleetPosted-FromDate" || pickerState == "fleetPosted-ToDate" || pickerState == "fleetInvited-FromDate" || pickerState == "fleetInvited-ToDate" || pickerState == "loadsQuoted-FromDate" || pickerState == "loadsQuoted-ToDate" || pickerState == "billingCustomerDate" {
            datePicker.isHidden = false
        }else if pickerState == "UnitType" || pickerState == "CargoType" || pickerState == "billingParty" || pickerState == "billingHead" || pickerState == "taxView" || pickerState == "Payment" || pickerState == "PaymentCity"{
            typePicker.isHidden = false
        }
        
        if pickerState == "DatePickerFrom" {
            datePicker.minimumDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        }
        if pickerState == "DatePickerTill" {
            //            datePicker.minimumDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())
            //
            //            datePicker.maximumDate = Calendar.current.date(byAdding: .day, value: 5, to: Date())
            var datee = userDefaults.value(forKey: "PostFleetFromDate")
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            if datee == nil{
                datee = Calendar.current.date(byAdding: .day, value: 1, to: Date())
            }else{
                guard let date = dateFormatter.date(from: datee as! String) else {
                    return
                         }
                    dateFormatter.dateFormat = "yyyy"
                    let year = dateFormatter.string(from: date)
                    dateFormatter.dateFormat = "MM"
                    let month = dateFormatter.string(from: date)
                    dateFormatter.dateFormat = "dd"
                    let day = dateFormatter.string(from: date)
                    print(year, month, day) // 2018 12 24
                    let calendar = Calendar.current
                    var minDateComponent = calendar.dateComponents([.day,.month,.year], from: Date())
                    minDateComponent.day = Int(day)
                    minDateComponent.month = Int(month)
                    minDateComponent.year = Int(year)
                    
                    let minDate = calendar.date(from: minDateComponent)
                    print(" min date : \(String(describing: minDate))")
                    
                    var maxDateComponent = calendar.dateComponents([.day,.month,.year], from: Date())
                    maxDateComponent.day = Int(day)! + 4
                    maxDateComponent.month = Int(month)
                    maxDateComponent.year = Int(year)
                    
                    let maxDate = calendar.date(from: maxDateComponent)
                    print("max date : \(String(describing: maxDate))")
                    
                    self.datePicker.minimumDate = minDate! as Date
                    self.datePicker.maximumDate =  maxDate! as Date
                    
               
            }
     
            
        }
        
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneClicked(_ sender: Any) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        print("Date =======>>>>> \(formatter.string(from: datePicker.date))")
        
        //        datePicker.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
        
        self.dismiss(animated: true, completion: nil)
        if pickerState == "TimePicker" {
            
            self.selectedTimeDict = ["time":userDefaults.value(forKey: "selectedTime")!,"timeType": userDefaults.value(forKey: "selectedTimeType")!]
            NotificationCenter.default.post(name: .selectedTime ,object: nil,userInfo: selectedTimeDict)
            
        }else if pickerState == "DatePicker"{
            
            
            self.selectedTimeDict = ["date": formatter.string(from: datePicker.date)]
            NotificationCenter.default.post(name: .selectedDate ,object: nil,userInfo: selectedTimeDict)
        }
        else if pickerState == "TimePickerFrom"{
            
            self.selectedTimeDict = ["time":userDefaults.value(forKey: "PostFleetFromTime")!,"timeType": userDefaults.value(forKey: "PostFleetFromTimeType")!]
            
            NotificationCenter.default.post(name: .postFleetFrom ,object: nil,userInfo: selectedTimeDict)
            
        } else if pickerState == "DatePickerFrom" {
            userDefaults.set(formatter.string(from: datePicker.date), forKey: "PostFleetFromDate")
            self.selectedTimeDict = ["date": userDefaults.value(forKey: "PostFleetFromDate")!]
            
            NotificationCenter.default.post(name: .postFleetFromDate ,object: nil,userInfo: selectedTimeDict)
            
        }
        else if pickerState == "TimePickerTill" {
            
            self.selectedTimeDict = ["time":userDefaults.value(forKey: "PostFleetTillTime")!,"timeType": userDefaults.value(forKey: "PostFleetFromTillType")!]
            
            NotificationCenter.default.post(name: .postFleetTill ,object: nil,userInfo: selectedTimeDict)
            
        }else if pickerState == "DatePickerTill" {
            userDefaults.set(formatter.string(from: datePicker.date), forKey: "PostFleetTillDate")
            
            self.selectedTimeDict = ["date": userDefaults.value(forKey: "PostFleetTillDate")!]
            
            NotificationCenter.default.post(name: .postFleetTillDate ,object: nil,userInfo: selectedTimeDict)
            
        }else if pickerState == "UnitType" {
            for unit in capacityWeightUnit{
                if unit.name! == self.selectedUnitType{
                    NotificationCenter.default.post(name: .postFleetUnitType ,object: nil,userInfo: ["unitType" : self.selectedUnitType,"id":unit.id!])
                    print("id cargo tpe === \(unit.id!)")
                }
                
            }
            
        }else if pickerState == "Payment" {
            for statee in stateArray{
                if statee == self.selectedState{
                    NotificationCenter.default.post(name: .paymentState ,object: nil,userInfo: ["state" : self.selectedState])
                    print("statee tpe === \(statee)")
                }
                
            }
            
        }else if pickerState == "PaymentCity" {
            for cityy in city{
                  print("selectedState tpe === \(self.selectedState)")
                if cityy == self.selectedCity{
                    NotificationCenter.default.post(name: .paymentCity ,object: nil,userInfo: ["city" : self.selectedCity])
                    print("cityy tpe === \(cityy)")
                }
                
            }
            
        }
        else if pickerState == "billingParty" {
            NotificationCenter.default.post(name: .billingParty ,object: nil,userInfo: ["billingParty" : self.selectedBillingParty])
            
        }else if pickerState == "taxView" {
            NotificationCenter.default.post(name: .taxView ,object: nil,userInfo: ["taxView" : self.selectedTax])
            
        }else if pickerState == "billingHead" {
            for taxInfo in extaxInfo{
                if taxInfo.billRecordType! == self.selectedBillingHead{
                    NotificationCenter.default.post(name: .billingHead ,object: nil,userInfo: ["taxInfo" : taxInfo])
                    print("billRecordType === \(taxInfo.billRecordType!)")
                }
                
            }
            
        }
        else if pickerState == "CargoType" {
            for cargo in cargoTypeArray{
                if cargo.cargoType! == self.selectedCargoType{
                    NotificationCenter.default.post(name: .postFleetCargoType ,object: nil,userInfo: ["cargoType" : self.selectedCargoType,"id":cargo.id!])
                    print("id cargo tpe === \(cargo.id!)")
                }
            }
        }
        else if pickerState == "fleetPosted-FromDate" {
            
            
            userDefaults.set(formatter.string(from: datePicker.date), forKey: "fleetPosted-FromDate")
            self.selectedTimeDict = ["date": userDefaults.value(forKey: "fleetPosted-FromDate")!]
            
            NotificationCenter.default.post(name: .fleetPostedFromDate ,object: nil,userInfo: selectedTimeDict)
            
        }else if pickerState == "fleetPosted-ToDate"{
            userDefaults.set(formatter.string(from: datePicker.date), forKey: "fleetPosted-ToDate")
            self.selectedTimeDict = ["date": userDefaults.value(forKey: "fleetPosted-ToDate")!]
            
            NotificationCenter.default.post(name: .fleetPostedToDate ,object: nil,userInfo: selectedTimeDict)
        }else if pickerState == "fleetInvited-FromDate" {
            
            
            userDefaults.set(formatter.string(from: datePicker.date), forKey: "fleetInvited-FromDate")
            self.selectedTimeDict = ["date": userDefaults.value(forKey: "fleetInvited-FromDate")!]
            
            NotificationCenter.default.post(name: .fleetInvitedFromDate ,object: nil,userInfo: selectedTimeDict)
            
        }else if pickerState == "fleetInvited-ToDate"{
            userDefaults.set(formatter.string(from: datePicker.date), forKey: "fleetInvited-ToDate")
            self.selectedTimeDict = ["date": userDefaults.value(forKey: "fleetInvited-ToDate")!]
            
            NotificationCenter.default.post(name: .fleetInvitedToDate ,object: nil,userInfo: selectedTimeDict)
        }else if pickerState == "loadsQuoted-FromDate" {
            
            
            userDefaults.set(formatter.string(from: datePicker.date), forKey: "loadsQuoted-FromDate")
            self.selectedTimeDict = ["date": userDefaults.value(forKey: "loadsQuoted-FromDate")!]
            
            NotificationCenter.default.post(name: .loadsQuotedFromDate ,object: nil,userInfo: selectedTimeDict)
            
        }else if pickerState == "loadsQuoted-ToDate"{
            userDefaults.set(formatter.string(from: datePicker.date), forKey: "loadsQuoted-ToDate")
            self.selectedTimeDict = ["date": userDefaults.value(forKey: "loadsQuoted-ToDate")!]
            
            NotificationCenter.default.post(name: .loadsQuotedToDate ,object: nil,userInfo: selectedTimeDict)
        }else if pickerState == "billingCustomerDate" {
            
            userDefaults.set(formatter.string(from: datePicker.date), forKey: "billingCustomerDate")
            self.selectedTimeDict = ["date": userDefaults.value(forKey: "billingCustomerDate")!]
            
            NotificationCenter.default.post(name: .billingCustomerDate ,object: nil,userInfo: selectedTimeDict)
        }
        
        
        
        
        //         NotificationCenter.default.post(name: .selectedTimeType ,object: nil,userInfo: selectedTimeTypeDict)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if pickerState == "TimePicker" || pickerState == "TimePickerFrom" || pickerState == "TimePickerTill" {
            return 2
        }else {
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerState == "TimePicker" || pickerState == "TimePickerFrom" || pickerState == "TimePickerTill" {
            return timeArray[component].count
        }else if pickerState == "UnitType" {
            return unitType.count
        }else if pickerState == "billingParty" {
            return billingParty.count
        }else if pickerState == "billingHead" {
            return billingHead.count
        }else if pickerState == "taxView" {
            return taxArray.count
        }else if pickerState == "Payment" {
            return stateArray.count
        }else if pickerState == "PaymentCity" {
            return city.count
        }else{
            return cargoType.count
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerState == "TimePicker" || pickerState == "TimePickerFrom" || pickerState == "TimePickerTill" {
            return timeArray[component][row]
        }else if pickerState == "UnitType" {
            return unitType[row]
        }else if pickerState == "Payment" {
            return stateArray[row]
        }else if pickerState == "PaymentCity" {
                return city[row]
        }else if pickerState == "billingParty" {
            return billingParty[row]
        }else if pickerState == "taxView" {
            let tax = taxArray[row]
            return "\(Double(tax)!)" + " %"
        }else if pickerState == "billingHead" {
            print("billRecordType ==== \(extaxInfo[row].billRecordType!)")
            return billingHead[row]
        }else{
            return cargoType[row]
        }
        
    }
    
    //
    //    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
    //        let attributedString = NSAttributedString(string: "12:00", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
    //        return attributedString
    //    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    var selectedTimeDict = [String:Any]()
    var selectedTimeTypeDict = [String:Any]()
    var selectedDateDict = [String:Any]()
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerState == "TimePicker" || pickerState == "TimePickerFrom" || pickerState == "TimePickerTill" {
            
            self.selectedTime = timeArray[component][row]
            
            let time =  timeArray[0][pickerView.selectedRow(inComponent: 0)]
            let timeType = timeArray[1][pickerView.selectedRow(inComponent: 1)]
            selectedTime = time
            selectedTimeType = timeType
            if pickerState == "TimePicker" {
                userDefaults.set(selectedTime, forKey: "selectedTime")
                userDefaults.set(selectedTimeType, forKey: "selectedTimeType")
            }else if pickerState == "TimePickerFrom" {
                
                userDefaults.set(selectedTime, forKey: "PostFleetFromTime")
                userDefaults.set(selectedTimeType, forKey: "PostFleetFromTimeType")
                
                
            }else if pickerState == "TimePickerTill" {
                userDefaults.set(selectedTime, forKey: "PostFleetTillTime")
                userDefaults.set(selectedTimeType, forKey: "PostFleetFromTillType")
            }
            
        }else if pickerState == "UnitType"{
            print(" UnitTypeNAme ===== \(unitType[row])")
            self.selectedUnitType = unitType[row]
        }else if pickerState == "Payment"{
            print(" Payment ===== \(stateArray[row])")
            self.selectedState = stateArray[row]
        }else if pickerState == "PaymentCity"{
            print(" PaymentCity ===== \(city[row])")
            self.selectedCity = city[row]
        }
        else if pickerState == "billingHead"{
            
            self.selectedBillingHead = self.billingHead[row]
            
        }
        else if pickerState == "billingParty"{
            
            self.selectedBillingParty = billingParty[row]
        } else if pickerState == "taxView"{
            self.selectedTax = "\(Double(taxArray[row])!)"
               }
        else{
            self.selectedCargoType = cargoType[row]
        }
        
    }
    
    
    var capacityWeightUnit = [CapacityWeightUnit]()
    
    func vehichleCapacityUnit(){
        
        Alamofire.request(URLStatics.unittype, method: .get).responseJSON { response in
            print("response.request=\(String(describing: response.request))")  // original URL request
            print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
            print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
            
            if let reply = response.result.value {
                
                let json = JSON(reply)
                print("unittype json  :  \(json)")
                
                var capUnit : CapacityWeightUnit?
                for i in 0 ..< json.count{
                    let unittype = json[i]
                    print("unittype :  \(unittype["name"])")
                    var str = unittype["name"].stringValue
                    self.unitType.append(str.uppercased())
                    capUnit = CapacityWeightUnit(id: unittype["id"].intValue, name: str.uppercased(), type: unittype["type"].stringValue)
                    self.capacityWeightUnit.append(capUnit!)
                }
                self.typePicker.reloadAllComponents()
                
            }
        }
        
    }
    
    var cargoTypeArray = [CargoType]()
    
    func getCargoType(){
        
        
        Alamofire.request(URLStatics.cargotype, method: .get).responseJSON { response in
            print("response.request=\(String(describing: response.request))")  // original URL request
            print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
            print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
            
            if let reply = response.result.value {
                
                let json = JSON(reply)
                print("cargotype  json : \(json)")
                var caargoTy : CargoType?
                for i in 0 ..< json.count{
                    let cargotype = json[i]
                    print("cargotype : \(cargotype["cargoType"])")
                    self.cargoType.append(cargotype["cargoType"].stringValue)
                    caargoTy = CargoType(id: cargotype["id"].intValue, cargoType: cargotype["cargoType"].stringValue)
                    self.cargoTypeArray.append(caargoTy!)
                }
                
                self.typePicker.reloadAllComponents()
                print("cargoType.count :  \(self.cargoType.count)")
                
            }
            
        }
        
    }
    
    var extaxInfo = [ExTaxInfo1]()
    
    func exBillMasterList(){
        let headers = header()
        
        Alamofire.request(URLStatics.exBillMasterList, method: .get, parameters: nil,encoding:
            URLEncoding.default, headers: headers).responseString{
                response in
                switch response.result {
                case .success:
                    print(response)
                    var returnDict = [[String:Any]]()
                    if let reply = response.result.value {
                        
                        let json = reply.toDictionary()
                        
                        if let returnObj = json["data"] as? [[String:Any]] {
                            returnDict = returnObj
                        }
                        print("returnDict json  :  \(returnDict.count)")
                        for data in returnDict{
                            self.billingHead.append(data["billRecordType"]! as! String)
                            
                            let taxInfo = ExTaxInfo1(id: data["id"]! as? Int, billRecordType: data["billRecordType"]! as? String, status: data["status"]! as? Bool, sacCode: data["sacCode"]! as? String)
                            
                            self.extaxInfo.append(taxInfo)
                        }
                        print("extaxInfo json  :  \(String(describing: self.extaxInfo.count))")
                        self.typePicker.reloadAllComponents()
                    }
                    
                    break
                    
                case .failure(let error):
                    
                    print(error)
                }
        }
        
    }
    
    
    
    func tax(){
        let headers = header()
        let parameters : [String:Any] = [
            "financialYear" : self.currentFinancialYear!
        ]
        
        Alamofire.request(URLStatics.taxgroup, method: .get, parameters: parameters,encoding:
            URLEncoding.default, headers: headers).responseJSON{
                response in
                switch response.result {
                case .success:
                    
                    print("returnDict json  :  \(response)")
                    var returnDict = [[String:Any]]()
                    
                    if let reply = response.result.value {
                        let json = JSON(reply)
                        print("taxgroup  json : \(json)")
                        
                        for i in 0 ..< json.count{
                            let taxgroup = json[i]
                            print("taxgroup : \(taxgroup["tax"])")
                            self.taxArray.append("\(taxgroup["tax"])")
                            
                        }
                        
                        self.typePicker.reloadAllComponents()
                    }
                    
                    break
                    
                case .failure(let error):
                    
                    print(error)
                }
        }
        
    }
    
    
    
    
    
    //    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
    //        var pickerLabel: UILabel? = (view as? UILabel)
    //        if pickerLabel == nil {
    //            pickerLabel = UILabel()
    //            pickerLabel?.font = UIFont(name: "", size: <Font Size>)
    //            pickerLabel?.textAlignment = .center
    //        }
    //        pickerLabel?.text = <Data Array>[row]
    //        pickerLabel?.textColor = UIColor.blue
    //
    //        return pickerLabel!
    //    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension String{
    func toDictionary() -> NSDictionary {
        let blankDict : NSDictionary = [:]
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            } catch {
                print(error.localizedDescription)
            }
        }
        return blankDict
    }
}
