//
//  ViewControllertest.swift
//  Mytrux
//
//  Created by Aboli on 11/06/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import GoogleMaps
import CTSlidingUpPanel
import SideMenuSwift
import Alamofire
import SwiftyJSON

enum MarkerType: Int32 {
    
    case CONTAINER, FTL
    
}


class POIItem: NSObject, GMUClusterItem {
    var position: CLLocationCoordinate2D
    var availableLocation: String!
    var destinationLocation: String!
    var type: MarkerType
    var vehicleTypeName : String!
    var icon: UIImage
    var bidCode : String!
    var loadType : String!
    var noOfVehicle : String!
    var cargoType : String!
    var cargoWt : String!
    var isQuoted : Bool
    var ownThreeBids : [AvailableLoadBidQuotaion]?
    init(position: CLLocationCoordinate2D,type: MarkerType,availableLocation: String, icon:UIImage,bitcode: String,destinationLocation: String, vehicleTypeName: String, loadType: String, noOfVehicle: String, cargoType: String, cargoWt: String,ownThreeBids : [AvailableLoadBidQuotaion]?,isQuoted : Bool) {
        self.position = position
        self.availableLocation = availableLocation
        self.type = type
        self.icon = icon
        self.bidCode = bitcode
        self.destinationLocation = destinationLocation
        self.vehicleTypeName = vehicleTypeName
        self.loadType = loadType
        self.noOfVehicle = noOfVehicle
        self.cargoWt = cargoWt
        self.cargoType = cargoType
        self.ownThreeBids = ownThreeBids
        self.isQuoted = isQuoted
    }
}


class MapViewController: BaseViewController,CLLocationManagerDelegate,GMSMapViewDelegate, GMUClusterManagerDelegate,GMUClusterRendererDelegate,UITableViewDelegate,UITableViewDataSource{
    var isSubmitQuoteSlideEnabled = false
    @IBOutlet weak var listView: UIView!
    @IBOutlet weak var searchListImageView: UIImageView!
    @IBOutlet weak var refreshView: UIView!
    @IBOutlet weak var fleetView: UIView!
    @IBOutlet weak var searchBar: UIView!
    @IBOutlet weak var loadSearch: UIView!
    @IBOutlet weak var currentLocationView: UIView!
    @IBOutlet weak var searchBarTextField: UITextField!
    @IBOutlet weak var notificationLabel: UIView!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var refreshImage: UIImageView!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var top: NSLayoutConstraint!
    @IBOutlet weak var heightt: NSLayoutConstraint!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var BIDView: UIView!
    @IBOutlet weak var quotedImage: UIImageView!
    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var vehicleType: UILabel!
    @IBOutlet weak var from: UILabel!
    @IBOutlet weak var to: UILabel!
    @IBOutlet weak var tableViewWrraper: UIView!
    @IBOutlet weak var dottedView: UIView!
    @IBOutlet weak var bidListTableView: UITableView!
    @IBOutlet weak var assignVechImageView: UIImageView!
    @IBOutlet weak var menuViewWrraper: UIView!
    @IBOutlet weak var menuCircularView: UIView!
    @IBOutlet weak var bookNow: UIView!
    @IBOutlet weak var hideMenuView: UIImageView!
    
    
    
    
    
    var tappedMarker : GMSMarker?
    var customInfoWindow : CustomInfoWindow?
    var activePoint : POIItem?
    let locationManager = CLLocationManager()
    let didFindMyLocation = false
    var bottomController:CTBottomSlideController?;
    var dashBoardClicked = false
    var searchLoadClicked = false
    var lat = Double()
    var lng = Double()
    var isBidListHidden = true
    var availableLatLong = [AvailableLatLong]()
    private var clusterManager: GMUClusterManager!
    
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadViewWithId()
        
        sideMenuController?.clearCache(with: "default")
         userDefaults.set(30, forKey: "index")
       
        NotificationCenter.default.addObserver(self, selector: #selector(self.ShowFleetInfoView(notification:)), name: .FleetInfoView, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.dashboardEvent(notification:)), name: .Dashboard, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.BackClicked(notification:)), name: .BackClicked, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.searchLoadEvent(notification:)), name: .SearchLoad, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.submitQuote(notification:)), name: .submitQuote, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.inviteFleet(notification:)), name: .inviteFleet, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.confirmSubmitQuote(notification:)), name: .confirmSubmitQuote, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.sliderValueChanged(notification:)), name: .sliderValueChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshView(notification:)), name: .refreshView, object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.dashboardActivityIndicator(notification:)), name: .dashboardActivityIndicator, object: nil)
       
        bottomController = CTBottomSlideController(topConstraint: top,
                                                   heightConstraint: heightt,
                                                   parent: view,
                                                   bottomView: bottomView,
                                                   tabController: nil,
                                                   navController: nil,
                                                   visibleHeight:self.searchBar.frame.height - 5 )
    
        
        
        bottomController?.onPanelCollapsed = {
//            NotificationCenter.default.post(name: Notification.Name("PannelCollapsed"), object: nil)
            self.bottomController?.setSlideEnabled(false)
            self.searchBar.isHidden = false
            self.loadSearch.isHidden = true
            self.searchLoadClicked = false
            self.dashBoardClicked = false
            if self.isSubmitQuoteSlideEnabled == true{
                self.bottomController?.setSlideEnabled(true)
                self.bottomController?.setAnchorPoint(anchor: 0.6)
            }else if self.isFleetInfoVIew == true{
                self.bottomController?.setSlideEnabled(true)
                self.bottomController?.setAnchorPoint(anchor: 0.85)
            }else{
                NotificationCenter.default.post(name: .pannelCollapsed, object: nil)
                self.bottomController?.setSlideEnabled(false)
            }
        }
        

    }
    
    
    func loadViewWithId() {
        SideMenuController.preferences.basic.defaultCacheKey = "default"
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "loadsQuotedVC") }, with: "second")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "fleetsPostedVC") }, with: "third")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "PostFleetVC") }, with: "fourth")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "WalletViewController") }, with: "fifth")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookingViewController") }, with: "sixth")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "LoadDetailViewController") }, with: "seventh")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "PostLoadViewController") }, with: "ninth")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookingController") }, with: "tenth")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "MakeInvoiceViewController") }, with: "eleven")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "TrackViewController") }, with: "twelve")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookingDetail") }, with: "thirteen")
        
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "ViewPODViewController") }, with: "fourteen")
        
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "SubmitQuoteDetailsVC") }, with: "15")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") }, with: "16")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "ViewPODViewController") }, with: "17")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "AssignViewController") }, with: "18")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "UploadPODViewController") }, with: "19")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "UploadPhotoViewController") }, with: "20")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "UploadSignatureViewController") }, with: "21")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "MatchingLoadsVC") }, with: "22")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "FleetMapViewController") }, with: "23")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookingNoteViewController") }, with: "24")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "NewBookingViewController") }, with: "25")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "FleetDetailsViewController") }, with: "26")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "InviteFleetDetailViewController") }, with: "27")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BidViewController") }, with: "28")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "FleetInvitedViewController") }, with: "29")
        
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") }, with: "30")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "LegalViewController") }, with: "eighth")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "ViewInvoiceViewController") }, with: "31")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "PaymentConfirmationVC") }, with: "33")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") }, with: "32")
            sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookNowViewController") }, with: "34")
         sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "CustomerViewController") }, with: "35")
        
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "CustomerDetailVC") }, with: "36")
        
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "AddCustomerViewController") }, with: "37")
        
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "HowItWorksVC") }, with: "38")
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "KnowHowViewController") }, with: "39")
  sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "ReverseLoadViewController") }, with: "40")
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        print(" ====== viewWillAppear ====== ")
        self.menuViewWrraper.isHidden = true
        self.menuCircularView.isHidden = false
        
        cardViewArray(arrView: [BIDView,tableViewWrraper,menuCircularView,menuViewWrraper])
        
        bidListTableView.delegate = self
        bidListTableView.dataSource = self
        loginUserType = userDefaults.value(forKey: UserDefaultsContants.LOGIN_USER_TYPE)! as! String
        // Generate and add random items to the cluster manager.
        self.tableViewWrraper.isHidden = true
       
        BIDView.isHidden = isBidListHidden
        topBarCard(topBar: topBar)
        self.searchBar.isHidden = false
        self.loadSearch.isHidden = true
        self.mapView.delegate = self
        searchBar.layer.cornerRadius = 8
        searchBar.layer.shadowColor = UIColor.gray.cgColor
        searchBar.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        searchBar.layer.shadowRadius = 5
        searchBar.layer.shadowOpacity = 0.4
        addDashedBorder(view: dottedView)
        loadSearch.layer.cornerRadius = 8
        loadSearch.layer.shadowColor = UIColor.gray.cgColor
        loadSearch.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        loadSearch.layer.shadowRadius = 5
        loadSearch.layer.shadowOpacity = 0.4
        
        
        self.isSubmitQuoteSlideEnabled = false
        self.isFleetInfoVIew = false
        bottomController?.closePanel()
   
        bottomController?.onPanelExpanded = {
            print("===== Panel Expanded in closure ======")
        }
        
        
        
        
        
        
        
        
        
        //        bottomController = CTBottomSlideController(topConstraint: top,
        //                                                   heightConstraint: heightt,
        //                                                   parent: view,
        //                                                   bottomView: bottomView,
        //                                                   tabController: nil,
        //                                                   navController: nil,
        //                                                   visibleHeight: 400)
        //0 is bottom and 1 is top. 0.5 would be center
        
        //     bottomController?.setExpandedTopMargin(pixels: self.view.frame.height - 400)
        bottomController?.setAnchorPoint(anchor: 0.6)
        bottomController?.setExpandedTopMargin(pixels: 250)
        SideMenuController.preferences.basic.menuWidth = self.view.frame.width
        
        self.searchListImageView.image = UIImage(named: "search-list")
        searchListImageView.image = searchListImageView.image?.withRenderingMode(.alwaysTemplate)
        searchListImageView.tintColor = UIColor(hexString: ColorConstants.RED)
        self.refreshImage.image = UIImage(named: "refresh")
        refreshImage.image = refreshImage.image?.withRenderingMode(.alwaysTemplate)
        refreshImage.tintColor = UIColor(hexString: ColorConstants.RED)
//        
//        self.assignVechImageView.image = UIImage(named: "assignVechUnselected")
//        assignVechImageView.image = assignVechImageView.image?.withRenderingMode(.alwaysTemplate)
//        assignVechImageView.tintColor = UIColor.white
        bottomController?.closePanel()
        self.mapView.isMyLocationEnabled = true
        self.bottomView.layer.borderWidth = 0.5
        self.bottomView.layer.cornerRadius = 20
        bottomView.layer.borderColor = UIColor(hexString: "#C0C6CC").cgColor
        //Location Manager code to fetch current location
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "map_style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                print("Unable to find map_style.json")
            }
        } catch {
            print("One or more of the map styles failed to load. \(error)")
        }
        // bottomController?.setSlideEnabled(false)
        
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView,
                                                 clusterIconGenerator: iconGenerator)
        renderer.delegate = self
        
        clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm,renderer: renderer)
        
        clusterManager.setDelegate(self, mapDelegate: self)
        // Do any additional setup after loading the view.
    }
    @objc func confirmSubmitQuote(notification: Notification) {
        bottomController?.closePanel()
    }
    @objc func sliderValueChanged(notification: Notification) {
        sideMenuController?.reloadInputViews()
        self.viewWillAppear(true)
    }
    @objc func refreshView(notification: Notification) {
        sideMenuController?.reloadInputViews()
        self.viewWillAppear(true)
    }
    
    
    
    @IBAction func expandMenuView(_ sender: Any) {
        UIView.transition(with: view, duration: 0.5, options: .curveEaseIn, animations: {
            self.menuViewWrraper.isHidden = false
            self.menuCircularView.isHidden = true
        })
    }
    
    
    @IBAction func bookNowClicked(_ sender: Any) {
         sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookNowViewController") }, with: "34")
        sideMenuController?.setContentViewController(with: "34")
    }
    
    @IBAction func hideMenuViewClicked(_ sender: Any) {
        
        UIView.transition(with: view, duration: 0.2, options: .curveEaseIn, animations: {
                  self.menuViewWrraper.isHidden = true
                  self.menuCircularView.isHidden = false
              })
    }
    
    
    @objc func dashboardActivityIndicator(notification: Notification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        let isActive = userInfo!["active"] as! Bool
        if isActive{
            self.activityIndicatorBegin()
        }else{
            self.activityIndicatorEnd()
        }
    }
    
    @objc func submitQuote(notification: Notification) {
        
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.6, animations: {
                
                self.bottomController?.setAnchorPoint(anchor: 0.6)
          
                self.bottomController?.expandPanel()
            })
        }
        self.searchBar.isHidden = true
        self.loadSearch.isHidden = true
        self.BIDView.isHidden = true
        isSubmitQuoteSlideEnabled = true
        isFleetInfoVIew = false
        self.bottomController?.setSlideEnabled(true)
        NotificationCenter.default.post(name: .sumbitQuoteView, object: nil)
    }
    
    @objc func inviteFleet(notification: Notification) {
        
        bottomController?.setAnchorPoint(anchor: 0.5)
        bottomController?.setExpandedTopMargin(pixels:250)
        self.searchBar.isHidden = true
        self.loadSearch.isHidden = true
        self.BIDView.isHidden = true
        NotificationCenter.default.post(name: .inviteFleetClicked, object: nil)
        self.bottomController?.setSlideEnabled(false)
        bottomController?.expandPanel()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "bidListCell") as! BidListTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    
    func checkIfMutlipleCoordinates(latitude : Float , longitude : Float) -> CLLocationCoordinate2D{
        var lat = latitude
        var lng = longitude   
        let arrTemp = self.availableLatLong.filter {
            let latt = Float($0.availableLatitude)
            let longg = Float($0.availableLongitude)
            
            return (((latitude == latt) && (longitude == longg)))
        }
        
        // arrTemp giving array of objects with similar lat long
        
        if arrTemp.count > 1{
            // Core Logic giving minor variation to similar lat long
            
            let variation = (randomFloat(min: 0.0, max: 2.0) - 0.5) / 1500
            lat = lat + variation
            lng = lng + variation
        }
        let finalPos = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lng))
        return  finalPos
    }
    
    
    
    func randomFloat(min: Float, max:Float) -> Float {
        return (Float(arc4random()) / 0xFFFFFFFF) * (max - min) + min
    }
    
    
    //    private func generateClusterItems() {
    //        let extent = 0.2
    //        for index in 1...availableLatLong.count {
    //            let lat = self.lat + extent
    //            let lng = self.lng + extent
    //            let name = "Item \(index)"
    //            let position = CLLocationCoordinate2DMake(lat, lng)
    //            let item = POIItem(position: position, type: MarkerType.FTL, availableLocation: name, icon: UIImage.init(named: "")!, bitcode: "", destinationLocation: "", vehicleTypeName: "", loadType: "", noOfVehicle: "", cargoType: "", cargoWt: "")
    //            clusterManager.add(item)
    //        }
    //    }
    
    //    private func generateClusterItems() {
    //        let extent = 0.2
    //        for index in 1...30 {
    //            let lat = self.lat + extent * randomScale()
    //            let lng = self.lng + extent * randomScale()
    //            let name = "Item \(index)"
    //            let item =
    //                POIItem(position: CLLocationCoordinate2DMake(lat, lng), name: name)
    //            clusterManager.add(item)
    //        }
    //    }
    //
    //    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
    //        if marker.userData is POIItem {
    //            marker.icon = UIImage(named: "Load-big03")
    //        }
    //    }
    
    
    /// Returns a random value between -1.0 and 1.0.
    private func randomScale() -> Double {
        return Double(arc4random()) / Double(UINT32_MAX) * 2.0 - 1.0
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let poiItem = marker.userData as? POIItem {
            // Remove previously opened window if any
            if activePoint != nil {
                customInfoWindow!.removeFromSuperview()
                activePoint = nil
            }
            // Load custom view from nib or create it manually
            // loadFromNib here is a custom extension of CustomInfoView
            customInfoWindow = CustomInfoWindow().loadView()
            customInfoWindow?.titleLabel.text = poiItem.availableLocation
            if poiItem.isQuoted == true{
                customInfoWindow?.quotedImage.isHidden = false
            }else{
                customInfoWindow?.quotedImage.isHidden = true
            }
            customInfoWindow?.layer.cornerRadius = 8
            customInfoWindow?.layer.shadowColor = UIColor.gray.cgColor
            customInfoWindow?.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            customInfoWindow?.layer.shadowRadius = 5
            customInfoWindow?.layer.shadowOpacity = 0.4
            customInfoWindow?.circularView.layoutIfNeeded()
            customInfoWindow?.circularView.layer.cornerRadius =  (customInfoWindow?.circularView.frame.size.width)!/2
            customInfoWindow?.circularView.layer.masksToBounds = true
            customInfoWindow?.circularView.clipsToBounds = true
            // Button is here
            customInfoWindow!.center = mapView.projection.point(for: poiItem.position)
            activePoint = poiItem
            self.view.addSubview(customInfoWindow!)
        }
        return false
    }
    
    @IBAction func menuViewClicked(_ sender: Any) {
        self.sideMenuController?.revealMenu()
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .right
        SideMenuController.preferences.basic.enablePanGesture = true
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        return customInfoWindow
    }
    
    override func viewDidLayoutSubviews() {
        self.circularView(viewArray: [notificationLabel])
    }
    
    @IBAction func bidViewClicked(_ sender: Any) {
        
       let fleetDetaill : FleetDatum?
        if loginUserType == UserStates.SERVICE_USER{
            self.sideMenuController?.hideMenu()
            sideMenuController?.setContentViewController(with: "26")
        }else{
            self.sideMenuController?.hideMenu()
            
            self.BIDView.isHidden = true
          
            for load in self.loadDetailsArray {
                let dataArr = load.data
                
                for data in dataArr{
                    print("data.availableLoadBidQuotaionElement?.id ===== ==== ==== \(String(describing: data.availableLoadBidQuotaionElement?.id))")
                    print("markerBidCode == \(markerBidCode)")
                    if (data.bidCode == markerBidCode){
                        print("data.bidCode === \(data.bidCode)")
                        let timestap: Double = Double(data.availableDateTime / 1000)
                        let availableFromtime = self.epochTime(epochTimee: timestap)
                        let availableFromdate = self.epochDate(epochDate: timestap)
                        let timestamp: Double = Double(data.closedBidTime / 1000)
                        let availableTilltime = self.epochTime(epochTimee: timestamp)
                        let availableTilldate = self.epochDate(epochDate: timestamp)
                        //  let lowestBid = data.availableLoadBidQuotaions[0].quotationAmount
                        let availableLoadQuotations = data.availableLoadBidQuotaions
                        let ownLastThreeBids = data.ownLastThreeBids
                     
                        for bid in ownLastThreeBids!{
                            print("====== bid.quotationAmount ======= \(bid.quotationAmount)")
                        }
                        print("====== vehicleTypeId ======= \(data.vehicleType.id)")
                        print("====== loadTypeId ======= \(data.loadType.id)")
                        
                        loadObject = ["id": data.id,"bidCode": data.bidCode,
                                      "vehicleType": data.vehicleType.name,
                                      "from": data.availableLocation,
                                      "to": data.destinationLocation,
                                      "loadType": data.loadType.loadType, "noOfVech":data.noOfVehicles,
                                      "cargoType": data.cargoType.cargoType!,
                                      "cargoWt": data.capacityWeight!,
                                      "packgs": data.packgs,
                                      "availableFromdate": availableFromdate,
                                      "availableFromtime": availableFromtime,
                                      "availableTilltime": availableTilltime,
                                      "availableTilldate": availableTilldate,
                                      "kgName": data.capacityWeightUnit.name!,
                                      "lowestBid": 0,"closedBidTime":data.closedBidTime,
                                      "bidRank": data.bidRank ?? 0,
                                      "availableLoadQuotations": availableLoadQuotations,
                                      "ownLastThreeBids": ownLastThreeBids!,
                                      "availableLoadBidQuotaionElement":data.availableLoadBidQuotaionElement!,"latitude":self.lat,"longitude":self.lng,"vehicleTypeId":data.vehicleType.id,"loadTypeId":data.loadType.id,"status":"MapView"]
                        
                    }
                }
                
            }
              sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "LoadDetailViewController") }, with: "seventh")
            sideMenuController?.setContentViewController(with: "seventh")
            NotificationCenter.default.post(name: .loadObj,
                                            object:nil,userInfo: loadObject)
            print("loadObject = \(loadObject)")
        }
        // LoadDetailViewController
        
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if let tempPoint = activePoint {
            customInfoWindow?.center = mapView.projection.point(for: tempPoint.position)
        }
    }
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        print("========= Cluster Clicked ======")
        
            let newCamera = GMSCameraPosition.camera(withTarget: cluster.position,                                      zoom: self.mapView.camera.zoom + 1)
            let update = GMSCameraUpdate.setCamera(newCamera)
            self.mapView.animate(with: update)
       
        var data = [Datum]()
        for dataa in loadDetailsArray{
            data = dataa.data
        }
        print("data.count === \(data.count)")
        

        return false
    }
    
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        customInfoWindow?.removeFromSuperview()
    }
    
    @IBAction func showCurrentLocation(_ sender: UITapGestureRecognizer) {
        guard let lat = self.mapView.myLocation?.coordinate.latitude,
            let lng = self.mapView.myLocation?.coordinate.longitude else { return }
        
        let camera = GMSCameraPosition.camera(withLatitude: lat ,longitude: lng , zoom: 10)
        self.mapView.animate(to: camera)
    }
    
    
    var isSearchBarClicked = false
    
    @IBAction func searchBarClicked(_ sender: Any) {
        
        isSearchBarClicked = !isSearchBarClicked
        if isSearchBarClicked == true{
            bottomController?.setSlideEnabled(true)
            bottomController?.setAnchorPoint(anchor: 0.5)
            bottomController?.setExpandedTopMargin(pixels: 290)
            self.searchBar.isHidden = true
            self.BIDView.isHidden = true
            self.loadSearch.isHidden = false
            NotificationCenter.default.post(name: .searchLoadCollapsed1, object: nil)
            bottomController?.expandPanel()
            
        }else{
            bottomController?.closePanel()
            NotificationCenter.default.post(name: .searchLoadCollapsed, object: nil)
            self.bottomController?.setSlideEnabled(false)
            self.searchBar.isHidden = false
            self.loadSearch.isHidden = true
        }
    }
    
    var loadObject = [String: Any]()
    var markerBidCode = String()
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        
        loadObject = [String: Any]()
        if let poiItem = marker.userData as? POIItem {
            self.id.text = poiItem.bidCode
            self.vehicleType.text = poiItem.vehicleTypeName
            self.from.text = poiItem.availableLocation
            self.to.text = poiItem.destinationLocation
            markerBidCode = poiItem.bidCode
            if poiItem.isQuoted == true{
                self.quotedImage.isHidden = false
            }else{
                self.quotedImage.isHidden = true
            }
        }
        
        self.BIDView.isHidden = false
    }
    
    
    
    @objc func BackClicked(notification: Notification) {
        self.viewWillAppear(true)
        self.BIDView.isHidden = true
        NotificationCenter.default.post(name: .pannelCollapsed, object: nil)
        self.bottomController?.setSlideEnabled(false)
        self.searchBar.isHidden = false
        self.loadSearch.isHidden = true
        self.searchLoadClicked = false
        self.dashBoardClicked = false
    }
    var isFleetInfoVIew = false
    
    @objc func ShowFleetInfoView(notification: Notification) {

                self.bottomController = CTBottomSlideController(topConstraint: self.top,
                                                                heightConstraint: self.heightt,
                                                           parent: self.view,
                                                           bottomView: self.bottomView,
                                                           tabController: nil,
                                                           navController: nil,
                                                           visibleHeight:self.searchBar.frame.height - 5 )
                 self.bottomController?.setAnchorPoint(anchor: 0.85)
                  self.bottomController?.setExpandedTopMargin(pixels: 100)
                 self.bottomController?.expandPanel()
      
        self.searchBar.isHidden = true
        self.loadSearch.isHidden = true
        self.BIDView.isHidden = true
        isSubmitQuoteSlideEnabled = false
        isFleetInfoVIew = true
        self.bottomController?.setSlideEnabled(true)
    }
    
    
    @objc func dashboardEvent(notification: Notification) {
        dashBoardClicked = !dashBoardClicked;
        //  print("dashboardClicked = \(dashBoardClicked)")
        if dashBoardClicked == true{
            bottomController?.setSlideEnabled(true)
            bottomController?.setAnchorPoint(anchor: 0.5)
            bottomController?.setExpandedTopMargin(pixels: 100)
            self.searchBar.isHidden = true
            bottomController?.expandPanel()
        }else {
            bottomController?.closePanel()
            NotificationCenter.default.post(name: .dashboardCollapsed, object: nil)
            self.bottomController?.setSlideEnabled(false)
            self.searchBar.isHidden = false
            self.loadSearch.isHidden = true
        }
    }
    
    @objc func searchLoadEvent(notification: Notification) {
        
        searchLoadClicked = !searchLoadClicked
        
        if searchLoadClicked == true{
            bottomController?.setSlideEnabled(true)
            bottomController?.setAnchorPoint(anchor: 0.5)
            bottomController?.setExpandedTopMargin(pixels: 290)
            self.searchBar.isHidden = true
            self.BIDView.isHidden = true
            self.loadSearch.isHidden = false
            bottomController?.expandPanel()
            
        }else{
            bottomController?.closePanel()
            NotificationCenter.default.post(name: .searchLoadCollapsed, object: nil)
            self.bottomController?.setSlideEnabled(false)
            self.searchBar.isHidden = false
            self.loadSearch.isHidden = true
        }
        
        
    }
    var address = ""
    func getAddressFromLatLon(pdblLatitude: CLLocationDegrees, withLongitude pdblLongitude: CLLocationDegrees) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks
                
                if pm!.count > 0 {
                    let pm = placemarks![0]
                    //                    print(pm.country)
                    //                    print(pm.locality)
                    //                    print(pm.subLocality)
                    //                    print(pm.thoroughfare)
                    //                    print(pm.postalCode)
                    //                    print(pm.subThoroughfare)
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality!
                        self.address = addressString
                        self.searchBarTextField.text = self.address
                        
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    print("address = " + addressString)
                }
        })
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 10)
        let marker = GMSMarker()
     //   getAddressFromLatLon(pdblLatitude: (location?.coordinate.latitude)!, withLongitude: (location?.coordinate.longitude)!)
        // I have taken a pin image which is a custom image
        //   var markerImage = UIImage(named: "Load-big03")!
        
        //creating a marker view
        //   let markerView = UIImageView(image: markerImage)
        lat = (location?.coordinate.latitude)!
        lng = (location?.coordinate.longitude)!
        getBidList()
        //changing the tint color of the image
        //        mapView.settings.myLocationButton = true
        //        mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
        //        marker.position = CLLocationCoordinate2D(latitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!)
        //        marker.icon = markerImage.scaleToSize(aSize: CGSize(width: self.mapView.frame.width/12, height: self.mapView.frame.width/12
        //        ))
        //        // marker.iconView = markerView
        //        marker.map = mapView
        self.tappedMarker = GMSMarker()
        // tappedMarker?.title = ""
        //  self.customInfoWindow = CustomInfoWindow().loadView()
        
        //comment this line if you don't wish to put a callout bubble
        mapView.selectedMarker = marker
        
        self.mapView.animate(to: camera)
        // generateClusterItems()
        
        //Finally stop updating location otherwise it will come again and again in this delegate
        self.locationManager.stopUpdatingLocation()
        
    }
   
    
    var isQuoted = false
    
    @IBAction func listViewClicked(_ sender: Any) {
     sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "MatchingLoadsVC") }, with: "22")
        sideMenuController?.setContentViewController(with: "22")
  
    }
    
    @IBAction func postFleetClicked(_ sender: Any) {
        if loginUserType == UserStates.SERVICE_USER{
            sideMenuController?.setContentViewController(with: "ninth")
        }else{
              sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "PostFleetVC") }, with: "fourth")
            sideMenuController?.setContentViewController(with: "fourth")
        }
    }
    
    var loadDetailsArray = [Load]()
    var getBidListPara = [String:Any]()
    var item : POIItem?
    
    
    
    func getBidList(){
        
        let indexx = userDefaults.value(forKey: "index") as! Int
        print("indexxx ===== \(indexx)")
        //self.activityIndicatorBegin()
     
        
        let bidlist = GetBidList(pageSize:"40",pageNo:"0",recordType:"",search:"",fromCity:"",fromState:"",toState:"",toCity:"",vehicleType:"",loadType:"",noOfVehicle:"",fromDate:"",toDate:"",currentLatitude:String(self.lat),currentLongitude:String(self.lng),km: String(indexx),financialYear: self.currentFinancialYear!,cargoType: "")
        
        let jsonDataa = try! JSONEncoder().encode(bidlist)
        let type = String(data: jsonDataa, encoding: .utf8)!
        do {
            self.getBidListPara = try self.convertToDictionary(from: type)
            print("listt = \(self.getBidListPara)")
        } catch {
            print(error)
        }
        
        let parameters : [String:Any] = getBidListPara
        
       let headers = header()
        
        //+918669696004
        //asdfgf1234$
        Alamofire.request(URLStatics.findavailableloadforbidlist,method: .get, parameters: parameters, headers: headers).responseJSON { response in
            print("response.request=\(String(describing: response.request))")  // original URL request
            print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
            print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
           //   self.activityIndicatorEnd()
            switch (response.result) {
              
            case .success:
                
                if let reply = response.result.value {
                    print("JSON:  bidlist  >>>>>>>>>>>>>>>>>>>>>>>>>> \(reply)")
                    let mainResponse = JSON(reply)
                    var returnDict = [[String:Any]]()
                    // let returnObj = mainResponse["data"].array
                    if let returnObj = mainResponse["data"].arrayObject as? [[String:Any]] {
                        returnDict = returnObj
                    }
                    var resultNew = [String:Any]()
                    var vehicleType = [String:Any]()
                    var availVehicleType = [String:Any]()
                    var documentType : [String:Any]?
                    var docuType = [String:Any]()
                    let availDocumentType = [String:Any]()
                    var availSmallDocType = [String:Any]()
                    var logoType : [String:Any]?
                    var addressType : [String:Any]?
                    var companyAccessArr = [String:Any]()
                   
                    
                    var cargoTypeArray = [String:Any]()
                     var availCargoTypeArray = [String:Any]()
                    var capacityWeightUnitArr = [String:Any]()
                    var availCapacityWeightUnitArr = [String:Any]()
                    
                 
                    var currencyMasterType = [String:Any]()
                    var availCurrencyType : [String:Any]?
                   
                    
                    var loadTypeArray = [String]()
                    
                    var loadType1 : LoadType1?
                    var vehicleTypee : VehicleType?
                    var availableVehicleTypee : AvailableVehicleType?
                   
                    var datumArray = [Datum]()
                    var loadDetails : Load?
                    var cargoTypee : CargoType?
                    var availCargoTypee : CargoType?
                    var capacityWeight : CapacityWeightUnit?
                    var availCapacityWeight : CapacityWeightUnit?
                    var loadOwnerArr = [String:Any]()
                    var companyAccess : CompanyAccess?
                    var loadOwner : LoadOwner?
                    var address : Address1?
                    var document : Logo?
                    var docs : Logo?
                    var availDocument : AvailableLogo?
                    var availSmallDocument: AvailableLogo?
                    var availableLoadBidQuotaions : AvailableLoadBidQuotaion?
                    var availableLoadBidQuotaionsType = [[String:Any]]()
                    var currencyMaster : CurrencyMaster?
                    var logo : Logo?
                    var availLoadType : AvailLoadType1?
                    
                    var bidArray = [Int]()
                    self.availableLatLong.removeAll()
                    self.clusterManager.clearItems()
                    for data in returnDict{
                       
                        var availableFleetBid : AvailableFleetBid?
                        var availableLoadBidQuotaionElement : AvailableLoadBidQuotaionElement?
                        var availableLoadCurrency: AvailableFleetBidCurrencyMaster?
                        var availableFleetBidCurrencyMaster: AvailableFleetBidCurrencyMaster?
                        var availFleetBidCurrencyType = [String:Any]()
                        var availableLoadBidQuotaionElementType = [String:Any]()
                        var availableLoadBidQuotaionsArray = [AvailableLoadBidQuotaion]()
                        var ownLastThreeBidsArray = [AvailableLoadBidQuotaion]()
                        var availableFleetBidType = [String:Any]()
                        var availableLoadType = [String:Any]()
                        resultNew = data["loadType"] as! [String : Any]
                        cargoTypeArray = data["cargoType"] as! [String : Any]
                        vehicleType = data["vehicleType"] as! [String:Any]
                        var ownLastThreeBidsType = [[String:Any]]()
                         var ownLastThreeBids : AvailableLoadBidQuotaion?
                       
                        
                        let cargoType = cargoTypeArray["cargoType"] as! String
                        let vehicleTypeName = vehicleType["name"] as! String
                        let loadType = resultNew["loadType"] as! String
                        let noOfVehicle = data["noOfVehicles"] as! Int
                        
                        print("noOfVehicle =\(noOfVehicle)")
                        print("bidCode =\(String(describing: data["bidCode"]))")
                        print("destinationLocation =\(String(describing: data["destinationLocation"]))")
                        loadTypeArray.append(loadType)
                        if self.loginUserType == UserStates.SERVICE_PROVIDER{
                            
                            if let returnObject =
                                data["availableLoadBidQuotaions"] as? [[String:Any]] {
                                availableLoadBidQuotaionsType = returnObject
                            }
                            if let returnObjectt =
                                data["ownLastThreeBids"] as? [[String:Any]] {
                                ownLastThreeBidsType = returnObjectt
                            }
                            
                            
                            
                            
                            if let availableLoad =
                                data["availableLoadBidQuotaion"] as? [String:Any] {
                                availableLoadBidQuotaionElementType = availableLoad
                            }
                            
                            
//                            availableLoadBidQuotaionElementType = data["availableLoadBidQuotaion"] as? [String:Any]
                            
                            availCurrencyType = availableLoadBidQuotaionElementType["currencyMaster"] as? [String:Any]
                            
                            if let fleetBidtype = availableLoadBidQuotaionElementType["availableFleetBid"] as? [String:Any]{
                                availableFleetBidType = fleetBidtype
                            }
                            
//                            availableFleetBidType = (availableLoadBidQuotaionElementType["availableFleetBid"] as? [String:Any])!
                            
                            if let loadTypee = availableFleetBidType["loadType"] as? [String:Any] {
                                availableLoadType = loadTypee
                            }
                            
                            if let cargoTy = availableFleetBidType["cargoType"] as? [String:Any] {
                                availCargoTypeArray = cargoTy
                            }

                            if let capacityWeight = availableFleetBidType["capacityWeightUnit"] as? [String:Any] {
                                availCapacityWeightUnitArr = capacityWeight
                            }
                            if let availVechTypee = availableFleetBidType["vehicleType"] as? [String:Any] {
                                availVehicleType = availVechTypee
                            }
                            
                          //  availableLoadType = (availableFleetBidType["loadType"] as? [String:Any])!
                         //   availVehicleType = (availableFleetBidType["vehicleType"] as? [String:Any])!
                            
                            print("availVehicleType == \(availVehicleType)")
                            
                          //  availDocumentType = (availVehicleType["document"] as? [String:Any])!
                            
                           
                            if let smallDocType = availVehicleType["documentSmall"] as? [String:Any] {
                                 availSmallDocType = smallDocType
                            }
                            if let fleetBid = availableFleetBidType["currencyMaster"] as? [String:Any] {
                                availFleetBidCurrencyType = fleetBid
                            }
                            

                       //     availSmallDocType = (availVehicleType["documentSmall"] as? [String:Any])!
                           // (availVehicleType["documentSmall"] as? [String:Any])!
                            
                           // availFleetBidCurrencyType = (availableFleetBidType["currencyMaster"] as? [String:Any])!
                            
                           
                            //                            availableLoadBidQuotaionsType =
                            //                                data["availableLoadBidQuotaions"] as! [[String : Any]]
                            
                            
                            
                            resultNew = data["loadType"] as! [String : Any]
                            loadOwnerArr = data["loadOwner"] as! [String:Any]
                            
                            documentType = loadOwnerArr["logo"] as? [String:Any]
                            docuType = vehicleType["document"] as! [String:Any]
                            
                            logoType = loadOwnerArr["logo"] as? [String:Any]
                            companyAccessArr = loadOwnerArr["companyAccess"] as! [String:Any]
                            capacityWeightUnitArr = data["capacityWeightUnit"] as! [String:Any]
                            addressType = loadOwnerArr["address"] as? [String:Any]
                            
                            for availableBid in availableLoadBidQuotaionsType{
                                currencyMasterType = availableBid["currencyMaster"] as! [String : Any]
                                currencyMaster = CurrencyMaster(id: currencyMasterType["id"] as? Int, currencyCode: currencyMasterType["currencyCode"] as? String)
                                availableLoadBidQuotaions = AvailableLoadBidQuotaion(id: availableBid["id"] as! Int,                  quotationAmount:Int(truncating: availableBid["quotationAmount"] as! NSNumber),                         currencyMaster: currencyMaster!,
                                                                                     quotationEntryDate: availableBid["quotationEntryDate"] as! Int)
                                availableLoadBidQuotaionsArray.append(availableLoadBidQuotaions!)
                                
                            }
                            
                            for ownbid in ownLastThreeBidsType{
                                currencyMasterType = ownbid["currencyMaster"] as! [String : Any]
                                currencyMaster = CurrencyMaster(id: currencyMasterType["id"] as? Int, currencyCode: currencyMasterType["currencyCode"] as? String)
                                ownLastThreeBids = AvailableLoadBidQuotaion(id: ownbid["id"] as! Int,                  quotationAmount:Int(truncating: ownbid["quotationAmount"] as! NSNumber),currencyMaster: currencyMaster!, quotationEntryDate: ownbid["quotationEntryDate"] as! Int)
                                ownLastThreeBidsArray.append(ownLastThreeBids!)
                                
                            }
                            for bid in ownLastThreeBidsArray{
                                print("bid.quotationAmount) databidCode  " + "\(bid.quotationAmount) " + "\(String(describing: data["bidCode"]))")
                            }
                            
                            
                            
                            
                            loadType1 = LoadType1(id: resultNew["id"] as! Int, loadType: resultNew["loadType"] as! String, code: resultNew["code"] as! String)
                            
                            capacityWeight = CapacityWeightUnit(id: capacityWeightUnitArr["id"] as? Int, name: capacityWeightUnitArr["name"] as? String, type: capacityWeightUnitArr["type"] as? String)
                            
                            address = Address1(id: addressType?["id"] as? Int, address: addressType?["address"] as? String, landmark: addressType?["landmark"] as? String, city: addressType?["city"] as? String, state: addressType?["state"] as? String, pincode: addressType?["pincode"] as? String, country: addressType?["country"] as? String)
                            
                            logo = Logo(id: logoType?["id"] as? Int, mimeType: logoType?["mimeType"] as? String, type: (logoType?["type"] as? String))
                            companyAccess = CompanyAccess(id:companyAccessArr["id"] as! Int)
                            
                            loadOwner = LoadOwner(id: loadOwnerArr["id"] as! Int,
                                                  name: loadOwnerArr["name"] as! String,
                                                  registrationNo: loadOwnerArr["registrationNo"] as! String,
                                                  companyCode: loadOwnerArr["companyCode"] as! String,
                                                  telephone: loadOwnerArr["telephone"] as! String,
                                                  fax:loadOwnerArr["fax"] as! String,
                                                  address: address!,
                                                  logo: logo!,
                                                  gstTax: loadOwnerArr["gstTax"] as! Double,
                                                  gstTaxRegNo: loadOwnerArr["gstTaxRegNo"] as! String,
                                                  jurisdiction: loadOwnerArr["jurisdiction"] as! String,
                                                  timeZone: loadOwnerArr["timeZone"] as! String,
                                                  panNo: loadOwnerArr["panNo"] as! String,
                                                  vatNo: loadOwnerArr["vatNo"] as! String,
                                                  iecNo: loadOwnerArr["iecNo"] as! String,
                                                  cinNo: loadOwnerArr["cinNo"] as! String,
                                                  cstNo: loadOwnerArr["cstNo"] as! String,
                                                  registrationDate: loadOwnerArr["registrationDate"] as! Int,
                                                  subscriptionStartDate: loadOwnerArr["subscriptionStartDate"] as! Int,
                                                  subscriptionEndDate: loadOwnerArr["subscriptionEndDate"] as! Int,
                                                  currentBalance: loadOwnerArr["currentBalance"] as! Int,
                                                  smsNotification: loadOwnerArr["smsNotification"] as! String,
                                                  companyAccess: companyAccess!,status: loadOwnerArr["status"] as! String,
                                                  expiredType: loadOwnerArr["expiredType"] as! String,
                                                  activeUserCount: loadOwnerArr["activeUserCount"] as! Int,
                                                  maxUser: loadOwnerArr["maxUser"] as! Int,
                                                  maxUserType: loadOwnerArr["maxUserType"] as! String,
                                                  exchangeBookingCount: loadOwnerArr["exchangeBookingCount"] as! Int,
                                                  goCount: loadOwnerArr["goCount"] as! Int,
                                                  proCount: loadOwnerArr["proCount"] as! Int,
                                                  profileMailCount: loadOwnerArr["profileMailCount"] as! Int,
                                                  ewbFlagStr: loadOwnerArr["ewbFlagStr"] as! String,
                                                  planType: loadOwnerArr["planType"] as! String,
                                                  exSuwalletAmount: loadOwnerArr["exSuwalletAmount"] as! Int,
                                                  exSuhubMaxUser: loadOwnerArr["exSuhubMaxUser"] as! Int,
                                                  exSuallowTransaction: loadOwnerArr["exSuallowTransaction"] as! Int,
                                                  exSuremainsTr: loadOwnerArr["exSuremainsTr"] as! Int,
                                                  hubPlanID: loadOwnerArr["hubPlanId"] as! Int,
                                                  exSuhubTransactionCost: loadOwnerArr["exSuhubTransactionCost"] as! Int,
                                                  thirdPartyAuthDateTime: loadOwnerArr["thirdPartyAuthDateTime"] as? Int)
                            
                            cargoTypee = CargoType(id: cargoTypeArray["id"] as? Int,
                                                   cargoType: cargoTypeArray["cargoType"] as? String)
                            
                            document = Logo(id: documentType?["id"] as? Int,mimeType: documentType?["mimeType"] as? String,type: ((documentType?["type"] as? String)))
                            
                            docs = Logo(id: docuType["id"] as? Int, mimeType: docuType["mimeType"] as? String, type: ((docuType["type"] as? String)))
                            
                            
                            vehicleTypee = VehicleType(id: vehicleType["id"] as! Int,
                                                       name: vehicleType["name"] as! String,
                                                       sequence: Int(vehicleType["sequence"] as! Double),
                                                       container: vehicleType["container"] as! Bool,
                                                       type: vehicleType["type"] as! String,
                                                       document: docs!, documentSmall: document!,
                                                       vehicleOrder: vehicleType["vehicleOrder"] as! Int,
                                                       weightCapacity: vehicleType["weightCapacity"] as? Int,
                                                       documentID: vehicleType["documentId"] as? Int,
                                                       smallDocID: vehicleType["smallDocId"] as? Int)
                            currencyMaster = CurrencyMaster(id: currencyMasterType["id"] as? Int, currencyCode: currencyMasterType["currencyCode"] as? String)
                            
                            availDocument = AvailableLogo(id: availDocumentType["id"] as? Int,
                                                          mimeType: availDocumentType["mimeType"] as? String,
                                                          type: availDocumentType["type"] as? String)

                            
                            availSmallDocument = AvailableLogo(id: availSmallDocType["id"] as? Int,mimeType: availSmallDocType["mimeType"] as? String,
                                                                    type: availSmallDocType["type"] as? String)
                            print("availSmallDocument === \(String(describing: availSmallDocument?.id))")
                            
                            availLoadType = AvailLoadType1(id: availableLoadType["id"] as? Int, loadType: availableLoadType["loadType"] as? String, code: availableLoadType["code"] as? String)
                            
                            availableVehicleTypee = AvailableVehicleType(id: availVehicleType["id"] as? Int,
                                                                         name: availVehicleType["name"] as? String,
                                                                         sequence: availVehicleType["sequence"] as? Double,
                                                                         container: availVehicleType["container"] as? Bool,
                                                                         type: availVehicleType["type"] as? String,
                                                       document: availDocument!, documentSmall: availSmallDocument!,
                                                       vehicleOrder: availVehicleType["vehicleOrder"] as? Int,
                                                       weightCapacity: availVehicleType["weightCapacity"] as? Int,
                                                       documentID: availVehicleType["documentId"] as? Int,
                                                       smallDocID: availVehicleType["smallDocId"] as? Int)
                            
                            
                            availableLoadCurrency = AvailableFleetBidCurrencyMaster(id: availCurrencyType?["id"] as? Int, currencyName: availCurrencyType?["currencyName"] as? String, currencyCountry: availCurrencyType?["currencyCountry"] as? String, currencyCode: availCurrencyType?["currencyCode"] as? String)

                            availableFleetBidCurrencyMaster = AvailableFleetBidCurrencyMaster(id: availFleetBidCurrencyType["id"] as? Int, currencyName: availFleetBidCurrencyType["currencyName"] as? String, currencyCountry: availFleetBidCurrencyType["currencyCountry"] as? String, currencyCode: availFleetBidCurrencyType["currencyCode"] as? String)
                            
                            availCargoTypee = CargoType(id: (availCargoTypeArray["id"] as? Int),
                                                        cargoType: availCargoTypeArray["cargoType"] as? String)
                            
                            availCapacityWeight = CapacityWeightUnit(id: availCapacityWeightUnitArr["id"] as? Int, name: availCapacityWeightUnitArr["name"] as? String, type: availCapacityWeightUnitArr["type"] as? String)
                            
                            
                            availableFleetBid = AvailableFleetBid(id: availableFleetBidType["id"] as? Int, bidCode: availableFleetBidType["bidCode"] as? String, creationDate: availableFleetBidType["creationDate"] as? Int, closedBidTime: availableFleetBidType["closedBidTime"] as? Int, availableLocation: availableFleetBidType["availableLocation"] as? String, destinationLocation: availableFleetBidType["destinationLocation"] as? String, availableLatitude: availableFleetBidType["availableLatitude"] as? Double, availableLongitude: availableFleetBidType["availableLongitude"] as? Double, destinationLatitude: availableFleetBidType["destinationLatitude"] as? Double, destinationLongitude: availableFleetBidType["destinationLongitude"] as? Double, vehicleType: availableVehicleTypee!, noOfVehicles: availableFleetBidType["noOfVehicles"] as? Int, availableDateTime: availableFleetBidType["availableDateTime"] as? Int,capacityWeight: availableFleetBidType["capacityWeight"] as? Int,loadType: availLoadType!, expectedFreight: availableFleetBidType["expectedFreight"] as? Int, currencyMaster: availableFleetBidCurrencyMaster!, status: availableFleetBidType["status"] as? String, cargoType: availCargoTypee!, capacityWeightUnit: availCapacityWeight!)

                            availableLoadBidQuotaionElement = AvailableLoadBidQuotaionElement(id: availableLoadBidQuotaionElementType["id"] as? Int, quotationAmount: availableLoadBidQuotaionElementType["quotationAmount"] as? Int, currencyMaster: availableLoadCurrency!, validDateTime: availableLoadBidQuotaionElementType["validDateTime"] as? Int, quotationEntryDate: availableLoadBidQuotaionElementType["quotationEntryDate"] as? Int, availableFleetBid: availableFleetBid!)

                            
                        
                            
                            let datum = Datum(id: data["id"] as! Int,
                                              bidCode: data["bidCode"] as! String,
                                              creationDate: data["creationDate"] as! Int,
                                              closedBidTime: data["closedBidTime"] as! Int,
                                              availableLocation: data["availableLocation"] as! String,
                                              destinationLocation: data["destinationLocation"] as! String,
                                              vehicleType: vehicleTypee!,
                                              noOfVehicles: data["noOfVehicles"] as! Int,
                                              availableDateTime: data["availableDateTime"] as! Int,
                                              cargoType: cargoTypee!,
                                              loadOwner: loadOwner!,
                                              loadType: loadType1!,
                                              capacityWeight: data["capacityWeight"] as? Int,
                                              capacityWeightUnit: capacityWeight!,
                                              availableLatitude: data["availableLatitude"] as? Double,
                                              availableLongitude: data["availableLongitude"] as? Double,
                                              destinationLatitude: data["destinationLatitude"] as? Double,
                                              destinationLongitude: data["destinationLongitude"] as? Double,
                                              bidQuotationCount: data["bidQuotationCount"] as! Int,
                                              bidRank: data["bidRank"] as? Int,
                                              status: data["status"] as! String,
                                              count:
                                data["count"] as! Int?,
                                              availableLoadBidQuotaions: availableLoadBidQuotaionsArray, ownLastThreeBids: ownLastThreeBidsArray,
                                              packgs: data["packgs"] as? Int,
                                              loadOwnerID: data["loadOwnerId"] as? Int,
                                              vehicleTypeMaster: data["vehicleTypeMaster"] as! String, availableLoadBidQuotaionElement: availableLoadBidQuotaionElement)
                            datumArray.append(datum)
                            
                            loadDetails = Load(status: true, data: datumArray)
                            self.loadDetailsArray.append(loadDetails!)
                            
                        }
                        
                        //                        self.userDefaults.set(self.loadDetailsArray, forKey: "load")
                        //
                        //                        self.userDefaults.synchronize()
                        for i in ownLastThreeBidsArray{
                            bidArray.append(i.quotationAmount)
                            
                        }
                        
                        if availableLoadBidQuotaionElementType["id"] as? Int != nil{
                            self.isQuoted = true
                        }else{
                            self.isQuoted = false
                        }
                        print(" availableLoadBidQuotaionElementType[] as? Int  === \( String(describing: availableLoadBidQuotaionElementType["id"] as? Int))")
                        
                        let arr = AvailableLatLong(availableLatitude: "\(String(describing: data["availableLatitude"]!))", availableLongitude: "\(String(describing: data["availableLongitude"]!))", loadType: loadType, availableLocation: "\(String(describing: data["availableLocation"]!))", bidCode: "\(String(describing: data["bidCode"]!))", destinationLocation: "\(String(describing: data["destinationLocation"]!))", vehicleTypeName: vehicleTypeName, noOfVehicle: String(noOfVehicle), cargoWt: "", cargoType: cargoType, bidArray: bidArray, isQuoted: self.isQuoted)
                        
                        self.availableLatLong.append(arr)
                    }
                    
                    
                    
                    for state in self.availableLatLong {
                        
                        let position = CLLocationCoordinate2DMake(Double(state.availableLatitude)!, Double(state.availableLongitude)!)
                        if self.loginUserType == UserStates.SERVICE_USER{
                            self.item = POIItem(position: position, type:MarkerType.CONTAINER, availableLocation: state.availableLocation, icon:UIImage(named:"Container-truck-big02")!, bitcode: state.bidCode, destinationLocation: state.destinationLocation!, vehicleTypeName: state.vehicleTypeName, loadType: state.loadType, noOfVehicle: "", cargoType: "", cargoWt: "", ownThreeBids: nil, isQuoted: false)
                        }else if state.loadType == "FTL" {
                            self.item = POIItem(position: position, type:MarkerType.FTL, availableLocation: state.availableLocation, icon: UIImage(named: "Load-big03")!, bitcode: state.bidCode, destinationLocation: state.destinationLocation!, vehicleTypeName: state.vehicleTypeName, loadType: state.loadType, noOfVehicle: state.noOfVehicle, cargoType: "", cargoWt: "", ownThreeBids: nil, isQuoted: state.isQuoted)
                        }else if state.loadType == "CONTAINER"{
                            self.item = POIItem(position: position, type:MarkerType.CONTAINER, availableLocation: state.availableLocation, icon: UIImage(named: "Container-big03")!, bitcode: state.bidCode, destinationLocation: state.destinationLocation!, vehicleTypeName: state.vehicleTypeName, loadType: "", noOfVehicle: "", cargoType: "", cargoWt: "", ownThreeBids: nil, isQuoted: state.isQuoted)
                        }
                        
                        
                        self.clusterManager.add(self.item!)
                        self.clusterManager.cluster()
                    }
                }
                break
            case .failure(let error):
                self.activityIndicatorEnd()
                print("POST REQUEST ERROR  - \(error.localizedDescription)")
                print("=== REQUEST INFORMATION ===")
                //                    print("Status Code: \(response.response!.statusCode)")
                print("Request Payload: \(parameters)")
                print("===")
                break
            }
        }
        
        
    }
    
    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
        if marker.userData is POIItem{
            let customClusterItem = (marker.userData! as! POIItem)
            let count = self.availableLatLong.count
            
            for i in 0...count {
                if self.loginUserType == UserStates.SERVICE_USER{
                    marker.icon = customClusterItem.icon.scaleToSize(aSize: CGSize(width: self.mapView.frame.width/9, height: self.mapView.frame.width/11 ))
                    break
                }else{
                    switch customClusterItem.type {
                    case MarkerType.FTL:
                        marker.icon = customClusterItem.icon.scaleToSize(aSize: CGSize(width: self.mapView.frame.width/12, height: self.mapView.frame.width/12))
                        break
                    case MarkerType.CONTAINER:
                        marker.icon = customClusterItem.icon.scaleToSize(aSize: CGSize(width: self.mapView.frame.width/10, height: self.mapView.frame.width/11 ))
                        break
                    }
                }
                
                
                marker.accessibilityLabel = "\(i)"
                
                //                customInfoWindow!.titleLabel.text = availableLatLong[index].availableLocation
            }
        }
    }
    
    @IBAction func menuClicked(_ sender: Any) {
        self.sideMenuController?.revealMenu()
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = true
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

//extension UIView {
//
//    func addDashedBorder(view : UIView) {
//        //Create a CAShapeLayer
//        let shapeLayer = CAShapeLayer()
//        shapeLayer.strokeColor = UIColor.white.cgColor
//        shapeLayer.lineWidth = 2
//        // passing an array with the values [2,3] sets a dash pattern that alternates between a 2-user-space-unit-long painted segment and a 3-user-space-unit-long unpainted segment
//        shapeLayer.lineDashPattern = [2,3]
//
//        let path = CGMutablePath()
//        path.move(to: CGPoint.zero)
//        path.addLine(to: CGPoint(x:view.frame.width, y: 0))
//        shapeLayer.path = path
//        layer.addSublayer(shapeLayer)
//    }
//}

extension String {
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self, options: Data.Base64DecodingOptions(rawValue: 0)) else {
            return nil
        }
        
        return String(data: data as Data, encoding: String.Encoding.utf8)
    }
    
    func toBase64() -> String? {
        guard let data = self.data(using: String.Encoding.utf8) else {
            return nil
        }
        
        return data.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
    }
}
