//
//  FleetPostedViewController.swift
//  Mytrux
//
//  Created by Aboli on 20/06/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import SideMenuSwift
import Alamofire
import SwiftyJSON

class FleetPostedViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    var cellTapped:Bool = false
    var currentRow = 0;
    var checked = Set<IndexPath>()
    var selectedIndex = IndexPath()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchBar: UIView!
    @IBOutlet weak var openLabel: UILabel!
    @IBOutlet weak var openRedLine: UIImageView!
    @IBOutlet weak var closeLabel: UILabel!
    @IBOutlet weak var searchWithDateView: UIView!
    @IBOutlet weak var fromDateTF: UITextField!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var closeRedline: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var mapViewButton: UIView!
    @IBOutlet weak var toDateTF: UITextField!
    @IBOutlet weak var fromDateView: UIView!
    @IBOutlet weak var toDateView: UIView!
     var isDragging = false
    var status = String()
    var isOpenClicked = false
     var closeClicked = false
    override func viewDidLoad() {
        super.viewDidLoad()
       sideMenuController?.clearCache(with: "third")
        NotificationCenter.default.addObserver(self, selector: #selector(self.fleetPostedFromDate(_:)), name: .fleetPostedFromDate, object: nil)
           isDragging = false
        NotificationCenter.default.addObserver(self, selector: #selector(self.fleetPostedToDate(_:)), name: .fleetPostedToDate, object: nil)
  
        // Do any additional setup after loading the view.
    }
    
  
    
      @objc func fleetPostedFromDate(_ notification: NSNotification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
       
        self.fromDateTF.text = (userInfo!["date"] as! String)
        
       }
    
     @objc func fleetPostedToDate(_ notification: NSNotification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        
        self.toDateTF.text = (userInfo!["date"] as! String)
        
        }
    
    override func viewWillAppear(_ animated: Bool) {
        if loginUserType == UserStates.SERVICE_PROVIDER{
            titleLabel.text = "Fleets Posted"
        }else{
            titleLabel.text = "Loads Posted"
        }
        
        openLabel.textColor = UIColor(hexString: "#0F2541")
        openRedLine.isHidden = false
        closeLabel.textColor = UIColor(hexString: "#6F7179")
        closeRedline.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self
        searchTF.delegate = self
        cardViewArray(arrView: [searchView])
        loginUserType = UserDefaults.standard.value(forKey: UserDefaultsContants.LOGIN_USER_TYPE)! as! String
        print("loginUserType = \(loginUserType)")
        searchWithDateView.isHidden = true
        searchSeperator.isHidden = true
        self.isOpenClicked = true
        UIView.animate(withDuration: 0.8) {
            //                self.searchView.transform = CGAffineTransform(scaleX: 1, y: 1)
            // self.tableViewTopSpacer.constant = 50
            self.searchViewHeightConstraint = self.searchViewHeightConstraint.setMultiplier(multiplier: 0.07)
        }
        self.status = "OPEN"
        pageNo = 0
        fleetsPosted(search: self.searchTF.text!)

 
        
    }
    
    @IBAction func menuClicked(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    @IBOutlet weak var searchViewHeightConstraint: NSLayoutConstraint!
    override func viewDidLayoutSubviews() {
        mapViewButton.layoutIfNeeded()
        mapViewButton.layer.cornerRadius = mapViewButton.frame.size.height / 2
        mapViewButton.layer.masksToBounds = true
        mapViewButton.clipsToBounds = true
    }
 
    @IBAction func menuView(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    
  
    
    @IBOutlet weak var searchSeperator: UIImageView!
    var isSearchOpen = false
    @IBAction func searchClicked(_ sender: UITapGestureRecognizer) {
        
        if isSearchOpen == false{
            searchSeperator.isHidden = false
            searchWithDateView.isHidden = false
            UIView.animate(withDuration: 0.8) {
                self.searchView.transform = CGAffineTransform.identity
                // self.tableViewTopSpacer.constant = 50
                self.searchViewHeightConstraint = self.searchViewHeightConstraint.setMultiplier(multiplier: 0.12)
            }
        }else{
            searchWithDateView.isHidden = true
            searchSeperator.isHidden = true
            UIView.animate(withDuration: 0.8) {
                //                self.searchView.transform = CGAffineTransform(scaleX: 1, y: 1)
                // self.tableViewTopSpacer.constant = 50
                self.searchViewHeightConstraint = self.searchViewHeightConstraint.setMultiplier(multiplier: 0.07)
            }
        }
        isSearchOpen = !isSearchOpen
       
        if searchTF.text != "" || fromDateTF.text != "" || toDateTF.text != "" {
            self.datumArray.removeAll()
            pageNo = 0
            fleetsPosted(search: self.searchTF.text!)
//            fromDateTF.text = ""
//            toDateTF.text = ""
        }
        
    }
    var fromDate = Int()
    var toDate = Int()
    @IBAction func mapViewBtnClicked(_ sender: Any) {
   
        self.fleetDetails  = ["fleetDetails":self.datumArray]
        
           sideMenuController?.setContentViewController(with: "23")
        
        NotificationCenter.default.post(name: .fleetDetailsMapView,object: nil,userInfo: self.fleetDetails)
           NotificationCenter.default.post(name: .refreshViewFleetMap, object: nil)
    }
        var datumArray = [FleetDatum]()
    var availableLatLong = [AvailableLatLong]()
    var loadDetailsArray = [Load]()
    var getBidListPara = [String:Any]()
    

    func fleetsPosted(search : String){
        if isOpenClicked == true{
            self.status = "OPEN"
        }else{
            self.status = "CLOSED"
        }
        var bidlist : GetBidList?
   
        if isDragging == false{
                 self.activityIndicatorBegin()
        }
      
        if fromDateTF.text! != "" && toDateTF.text! != ""{
            
            let dateTime = "\(String(describing: fromDateTF.text!))"
            
            let dateString = dateTime
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let s = dateFormatter.date(from: dateString)
            //print("validDateTime =>> \(String(describing: s!))")
            let dateTimestamp = Int(s!.timeIntervalSince1970)
            // print("dateTimestamp =>> \(dateTimestamp)")
            self.fromDate = Int((dateTimestamp * 1000))
            
          
                let dateTime1 = "\(String(describing: toDateTF.text!))"
                let dateString1 = dateTime1
                let dateFormatter1 = DateFormatter()
                dateFormatter1.dateFormat = "dd-MM-yyyy"
                let s1 = dateFormatter1.date(from: dateString1)
                //print("validDateTime =>> \(String(describing: s!))")
                let dateTimestamp1 = Int(s1!.timeIntervalSince1970)
                // print("dateTimestamp =>> \(dateTimestamp)")
                self.toDate = Int((dateTimestamp1 * 1000))
            
                bidlist =  GetBidList(pageSize:"10",pageNo:"\(self.pageNo)",recordType:"",search:"\(search)",fromCity:"",fromState:"",toState:"",toCity:"",vehicleType:"",loadType:"",noOfVehicle:"",fromDate:"\(fromDate)",toDate:"\(toDate)",currentLatitude:"",currentLongitude:"",km: "",financialYear: self.currentFinancialYear!,cargoType: "")
         
        }else if fromDateTF.text! != ""  {
            
            let dateTime = "\(String(describing: fromDateTF.text!))"
 
            let dateString = dateTime
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let s = dateFormatter.date(from: dateString)
            //print("validDateTime =>> \(String(describing: s!))")
            let dateTimestamp = Int(s!.timeIntervalSince1970)
            // print("dateTimestamp =>> \(dateTimestamp)")
            self.fromDate = Int((dateTimestamp * 1000))
            
            
             bidlist =  GetBidList(pageSize:"10",pageNo:"\(self.pageNo)",recordType:"",search:"\(search)",fromCity:"",fromState:"",toState:"",toCity:"",vehicleType:"",loadType:"",noOfVehicle:"",fromDate:"\(fromDate)",toDate:"",currentLatitude:"",currentLongitude:"",km: "",financialYear: self.currentFinancialYear!,cargoType: "")
         
           
        }else  if toDateTF.text! != "" {
            let dateTime1 = "\(String(describing: toDateTF.text!))"
            let dateString1 = dateTime1
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "dd-MM-yyyy"
            let s1 = dateFormatter1.date(from: dateString1)
            //print("validDateTime =>> \(String(describing: s!))")
            let dateTimestamp1 = Int(s1!.timeIntervalSince1970)
            // print("dateTimestamp =>> \(dateTimestamp)")
            self.toDate = Int((dateTimestamp1 * 1000))
            bidlist =  GetBidList(pageSize:"10",pageNo:"\(self.pageNo)",recordType:"",search:"\(search)",fromCity:"",fromState:"",toState:"",toCity:"",vehicleType:"",loadType:"",noOfVehicle:"",fromDate:"",toDate:"\(toDate)",currentLatitude:"",currentLongitude:"",km: "",financialYear: self.currentFinancialYear!,cargoType: "")
        }else{
            bidlist = GetBidList(pageSize:"10",pageNo:"\(self.pageNo)",recordType:"",search:"\(search)",fromCity:"",fromState:"",toState:"",toCity:"",vehicleType:"",loadType:"",noOfVehicle:"",fromDate:"",toDate:"",currentLatitude:"",currentLongitude:"",km: "",financialYear: self.currentFinancialYear!,cargoType: "")
        }
       
        
        
        let jsonDataa = try! JSONEncoder().encode(bidlist)
        let type = String(data: jsonDataa, encoding: .utf8)!
        do {
            self.getBidListPara = try self.convertToDictionary(from: type)
            print("listt = \(self.getBidListPara)")
        } catch {
            print(error)
        }
       
        let parameters : [String:Any] = getBidListPara
        
       let headers = header()
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
        
        Alamofire.request(URLStatics.fleetPosted + "\(self.status)" , method: .get, parameters: parameters,headers: headers)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                
                
                switch (response.result) {
            
                case .success:
                    
                    self.searchTF.text = ""
                    self.fromDateTF.text = ""
                    self.toDateTF.text = ""
                    
                    if self.isDragging == false{
                        self.activityIndicatorEnd()
                    }
                     
                    if let reply = response.result.value {
                        print("======= JSON: Fleet Posted ========  \(reply)")
                        let mainResponse = JSON(reply)
                        var returnDict = [[String:Any]]()
                        // var datumArray = [FleetDatum]()
                        var datum : FleetDatum?
                        var vehicleType = [String:Any]()
                        var smallDocumentType = [String:Any]()
                        var documentType = [String:Any]()
                        var document : FleetLogo?
                        var smallDocument : FleetLogo?
                        var cargoType : FleetCargoType?
                        var cargo = [String:Any]()
                        var capacityWt = [String:Any]()
                        var capacityWtUnit : FleetCapacityWeightUnit?
                        var loadType = [String:Any]()
                        var fleetLoadType : FleetLoadType?
                        var currencyMaster = [String:Any]()
                        var fleetCurrencyMaster : FleetCurrencyMaster
                        
                        var fleetVehicleType : FleetVehicleType?
                        if let returnObj = mainResponse["data"].arrayObject as? [[String:Any]] {
                            returnDict = returnObj
                        }
                        
                        for data in returnDict{
                            vehicleType = data["vehicleType"] as! [String:Any]
                            smallDocumentType = vehicleType["documentSmall"] as! [String:Any]
                            documentType = vehicleType["document"] as! [String:Any]
                            cargo = data["cargoType"] as! [String:Any]
                            capacityWt = data["capacityWeightUnit"] as! [String:Any]
                            loadType = data["loadType"] as! [String:Any]
                            currencyMaster = data["currencyMaster"] as! [String:Any]
                            
                            document = FleetLogo(id: documentType["id"] as! Int, mimeType: documentType["mimeType"] as! String, type: documentType["type"] as! String)
                            
                            smallDocument = FleetLogo(id: smallDocumentType["id"] as! Int, mimeType: smallDocumentType["mimeType"] as! String, type: smallDocumentType["type"] as! String)
                            
                            fleetVehicleType = FleetVehicleType(id: Int(truncating: vehicleType["id"] as! NSNumber), name: vehicleType["name"] as! String, sequence: Int(truncating: vehicleType["sequence"] as! NSNumber), container: vehicleType["container"] as! Bool, type: vehicleType["type"] as! String, document: document!, documentSmall: smallDocument!, vehicleOrder: Int(truncating: vehicleType["vehicleOrder"] as! NSNumber), documentID: Int(truncating: vehicleType["documentId"] as! NSNumber), smallDocID: Int(truncating: vehicleType["smallDocId"] as! NSNumber))
                            
                            fleetLoadType = FleetLoadType(id: Int(truncating: loadType["id"] as! NSNumber), loadType: loadType["loadType"] as! String, code: loadType["code"] as! String)
                            
                            cargoType = FleetCargoType(id: cargo["id"] as! Int, cargoType: cargo["cargoType"] as! String)
                            
                            capacityWtUnit = FleetCapacityWeightUnit(id: capacityWt["id"] as! Int, name: capacityWt["name"] as! String, type: capacityWt["type"] as! String)
                            
                            fleetCurrencyMaster = FleetCurrencyMaster(id: currencyMaster["id"] as! Int, currencyName: currencyMaster["currencyName"] as! String, currencyCountry: currencyMaster["currencyCountry"] as! String, currencyCode: currencyMaster["currencyCode"] as! String)
                            
                            datum = FleetDatum(id: data["id"] as! Int,
                                               bidCode: data["bidCode"] as! String,
                                               creationDate: data["creationDate"] as! Int,
                                               closedBidTime: data["closedBidTime"] as! Int,
                                               availableLocation: data["availableLocation"] as! String,
                                               destinationLocation: data["destinationLocation"] as? String,
                                               availableLatitude: data["availableLatitude"] as? Double,
                                               availableLongitude: data["availableLongitude"] as? Double,
                                               destinationLatitude: data["destinationLatitude"] as? Double,
                                               destinationLongitude: data["destinationLongitude"] as? Double,
                                               count: data["count"] as? Int,
                                               vehicleType: fleetVehicleType!,
                                               noOfVehicles: data["noOfVehicles"] as! Int,
                                               availableDateTime: data["availableDateTime"] as! Int,
                                               cargoType: cargoType!,
                                               capacityWeight: data["capacityWeight"] as! Int,
                                               capacityWeightUnit: capacityWtUnit!,
                                               loadType: fleetLoadType!,
                                               expectedFreight: data["expectedFreight"] as? NSNumber,
                                               currencyMaster: fleetCurrencyMaster,
                                               isInvited: data["isInvited"] as! Bool,
                                               bidInviteCount: data["bidInviteCount"] as! Int,
                                               vehicleTypeMaster: data["vehicleTypeMaster"] as? String,
                                               loadBidCode: data["loadBidCode"] as? String,
                                               registrationNo: data["registrationNo"] as? String,
                                               comments: data["comments"] as? String, sequence: data["sequence"] as? Int, postType: data["postType"] as? String)
                            
                            if self.datumArray.contains(where: { bid in bid.bidCode == datum?.bidCode }) {
                                print(" exists in the array")
                            } else {
                                print(" does not exists in the array")
                                 self.datumArray.append(datum!)
                            }
           
                            
                        }
                        print( "datumArray.count ====== \(self.datumArray.count)")
                       self.tableView.reloadData()
                    }
                    break
                case .failure(let error):
                    
                    print("error \(error.localizedDescription)")
                    break
                }
        }
        
    }
    
    
    

    var pageNo = 0
   
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        print("offsetY = \(offsetY) ======= contentHeight ==== \(contentHeight - scrollView.frame.size.height - 300)")
    
        if offsetY > contentHeight - scrollView.frame.size.height - 300 {

                 pageNo += 1
            
           isDragging = true
                 self.fleetsPosted(search: self.searchTF.text!)
           
           
        }else{
            isDragging = false
        }
        
    }
    
    
    
    
    var isLoading = false

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if !isLoading && indexPath.row == self.datumArray.count - 5 {
//            print("isLoading === \(isLoading)")
//            pageNo += 1
//            self.fleetsPosted()
//
//        }
//       if !isLoading && indexPath.row == 0 {
//           pageNo = 0
//        self.fleetsPosted(search: self.searchTF.text!)
//        }
//        isLoading = !isLoading
    }


    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.loginUserType == UserStates.SERVICE_PROVIDER{
            if cellTapped{
                
                if indexPath as IndexPath == selectedIndex {
                    if self.datumArray.count != 0{
                        let fleetDetails = datumArray[indexPath.row]
                        if fleetDetails.isInvited == true{
                         return 450
                        }
                        else{
                           return 360
                        }
                    }
                }
            }
            return 120
        }else{
            if cellTapped{
                if indexPath as IndexPath == selectedIndex {
                    if closeClicked == true{
                        return 380
                    }else{
                        return 380
                    }
                    //Size you want to increase to
                }
            }
            return 115
        }
    }

    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.datumArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "fleetCell") as! FleetPostCell
//                cell.fleetPostView.isHidden = true
//           cell.loadsPostedView.isHidden = true
        cell.loadView2.isHidden = true
        cell.loadView3.isHidden = true
        cell.fleetView2.isHidden = true
        cell.fleetView3.isHidden = true
        cell.messageView.isHidden = false
        cell.fleetRegNoLabel.text = ""
      
        if indexPath.row == currentRow {
            if loginUserType == UserStates.SERVICE_PROVIDER{
                if datumArray.count != 0 {
                    
               
                let fleetDetails = self.datumArray[indexPath.row]
                
                cell.fleetID.text = fleetDetails.bidCode
                cell.vehType.text = fleetDetails.vehicleType.name
                cell.fleetFrom.text = fleetDetails.availableLocation
                cell.fleetTo.text = fleetDetails.destinationLocation
                cell.fleetLoadType.text = fleetDetails.loadType.loadType
                cell.fleetCargoType.text = fleetDetails.cargoType.cargoType
                cell.fleetNoOfVech.text = String(fleetDetails.noOfVehicles)
                if fleetDetails.isInvited == true{
                    cell.fleetMessageView.isHidden = false
                    cell.msgLabel.text = String(fleetDetails.bidInviteCount)
                    let invite = UITapGestureRecognizer(target: self, action: #selector(invite(recognizer:)))
                    cell.invitesBtn.addGestureRecognizer(invite)
                    cell.invitesBtn.tag = indexPath.row
                }
                else{
                    cell.fleetMessageView.isHidden = true
                    cell.msgLabel.text = "00"
                }

                let timestap: Double = Double(fleetDetails.availableDateTime / 1000)
                let availableFromtime = self.epochTime(epochTimee: timestap)
                let availableFromdate = self.epochDate(epochDate: timestap)
                let timestamp: Double = Double(fleetDetails.closedBidTime / 1000)
                let availableTilltime = self.epochTime(epochTimee: timestamp)
                let availableTilldate = self.epochDate(epochDate: timestamp)
                cell.fleetAvailFromdate.text = availableFromdate
                cell.fleetAvailFromTime.text = availableFromtime
                cell.fleetAvailTillDate.text = availableTilldate
                cell.fleetAvailTillTime.text = availableTilltime
                
                
                    let edit = UITapGestureRecognizer(target: self, action: #selector(editFleetPosted(recognizer:)))
                    cell.editView.addGestureRecognizer(edit)
                    cell.editView.tag = indexPath.row
                    
                let cargoWt = "\(fleetDetails.capacityWeight)" + " \(fleetDetails.capacityWeightUnit.name)"
                cell.fleetCargoWt.text = cargoWt
                    if fleetDetails.expectedFreight != nil{
                        cell.freightAmountLabel.text = "\(fleetDetails.expectedFreight!)"
                    }else{
                         cell.freightAmountLabel.text = "00.00"
                    }
                
             
                cell.fleetRegNoLabel.text = fleetDetails.registrationNo
                    
                    
                    cell.loadsPostedView.isHidden = true
                    if cellTapped == false {
                        cell.fleetView2.isHidden = true
                        cell.fleetView3.isHidden = true
                        self.tableView.rowHeight = 120
                    }else{
                        cell.fleetView2.isHidden = false
                        
                        if fleetDetails.isInvited == true{
                            cell.fleetMessageView.isHidden = false
                            cell.msgLabel.text = String(fleetDetails.bidInviteCount)
                            cell.fleetView3.isHidden = false
                            self.tableView.rowHeight = 400
                        }
                        else{
                            cell.fleetMessageView.isHidden = true
                            cell.msgLabel.text = "00"
                            cell.fleetView3.isHidden = true
                            self.tableView.rowHeight = 360
                        }
                     
                    }
       
                 }
                cell.fleetPostView.isHidden = false
                
                if isOpenClicked == true {
                    cell.fleetVerticalImage.image = UIImage(named: "greenVerticalLine")
                }else{
                     cell.fleetVerticalImage.image = UIImage(named: "redVerticalLine")
                }
                
                
              
              //  cell.vechLabel.text = fleetDetails.

            }else if loginUserType == UserStates.SERVICE_USER{
                
                let fleetDetails = self.datumArray[indexPath.row]
                
                
                cell.fleetID.text = fleetDetails.bidCode
                cell.vehType.text = fleetDetails.vehicleType.name
                cell.fleetFrom.text = fleetDetails.availableLocation
                cell.fleetTo.text = fleetDetails.destinationLocation
                cell.fleetLoadType.text = fleetDetails.loadType.loadType
                cell.fleetCargoType.text = fleetDetails.cargoType.cargoType
                cell.fleetNoOfVech.text = String(fleetDetails.noOfVehicles)
                
                let timestap: Double = Double(fleetDetails.availableDateTime / 1000)
                let availableFromtime = self.epochTime(epochTimee: timestap)
                let availableFromdate = self.epochDate(epochDate: timestap)
                let timestamp: Double = Double(fleetDetails.closedBidTime / 1000)
                let availableTilltime = self.epochTime(epochTimee: timestamp)
                let availableTilldate = self.epochDate(epochDate: timestamp)
                cell.fleetAvailFromdate.text = availableFromdate
                cell.fleetAvailFromTime.text = availableFromtime
                cell.fleetAvailTillDate.text = availableTilldate
                cell.fleetAvailTillTime.text = availableTilltime
                let cargoWt = "\(fleetDetails.capacityWeight)" + " \(fleetDetails.capacityWeightUnit.name)"
                cell.fleetCargoWt.text = cargoWt
                cell.freightAmountLabel.text = "\(fleetDetails.expectedFreight!)"
                 cell.fleetRegNoLabel.text = fleetDetails.registrationNo
          
                let viewBid = UITapGestureRecognizer(target: self, action: #selector(ViewBid(recognizer:)))
                cell.viewBidBtn.addGestureRecognizer(viewBid)
              
                cell.fleetPostView.isHidden = true
                cell.loadsPostedView.isHidden = false
                if cellTapped == false {
                    cell.loadView2.isHidden = true
                    cell.loadView3.isHidden = true
                    self.tableView.rowHeight = 120
                }else{
                    cell.loadView2.isHidden = false
                    if closeClicked == true{
                        cell.loadVerticalImage.image = UIImage(named: "redVerticalLine")
                        cell.loadView3.isHidden = false
                        self.tableView.rowHeight = 380
                    }else{
                        cell.loadView3.isHidden = false
                        self.tableView.rowHeight = 400
                    }
                }
//                cell.fleetPostView.isHidden = true
//                 cell.loadsPostedView.isHidden = false
//                cell.messageView.isHidden = false
//                cell.messageImageView.image = UIImage(named: "messagepdf")
//                cell.messageImageView.image = cell.messageImageView.image?.withRenderingMode(.alwaysTemplate)
//                cell.messageImageView.tintColor = UIColor(hexString: ColorConstants.YELLOW)
//                //  cell.loadsPostedView.isHidden = false
//                if cellTapped == false {
//                    cell.loadView2.isHidden = true
//                     cell.messageView.isHidden = false
//                    cell.loadView3.isHidden = true
//                    self.tableView.rowHeight = 115
//
//                    if closeClicked == true {
//                         cell.loadVerticalImage.image = UIImage.init(named: "redVerticalLine")
//                    }
//                }else{
//                    cell.loadView2.isHidden = false
//                     cell.messageView.isHidden = false
//                    cell.loadView3.isHidden = false
//                    if closeClicked == true{
//
//                        self.tableView.rowHeight = 400
//
//
//                    }
//                }
       
            }
        }else{
            cell.loadsPostedView.isHidden = true
            if datumArray.count != 0 {
                
            
            let fleetDetails = self.datumArray[indexPath.row]
            
            
            cell.fleetID.text = fleetDetails.bidCode
            cell.vehType.text = fleetDetails.vehicleType.name
            cell.fleetFrom.text = fleetDetails.availableLocation
            cell.fleetTo.text = fleetDetails.destinationLocation
            cell.fleetLoadType.text = fleetDetails.loadType.loadType
            cell.fleetCargoType.text = fleetDetails.cargoType.cargoType
            cell.fleetNoOfVech.text = String(fleetDetails.noOfVehicles)
            
            if fleetDetails.isInvited == true{
                cell.fleetMessageView.isHidden = false
                cell.msgLabel.text = String(fleetDetails.bidInviteCount)
            }
            else{
                cell.fleetMessageView.isHidden = true
                cell.msgLabel.text = "00"
            }
                if isOpenClicked == true {
                    cell.fleetVerticalImage.image = UIImage(named: "greenVerticalLine")
                }else{
                    cell.fleetVerticalImage.image = UIImage(named: "redVerticalLine")
                }
            
            
            let timestap: Double = Double(fleetDetails.availableDateTime / 1000)
            let availableFromtime = self.epochTime(epochTimee: timestap)
            let availableFromdate = self.epochDate(epochDate: timestap)
            let timestamp: Double = Double(fleetDetails.closedBidTime / 1000)
            let availableTilltime = self.epochTime(epochTimee: timestamp)
            let availableTilldate = self.epochDate(epochDate: timestamp)
            cell.fleetAvailFromdate.text = availableFromdate
            cell.fleetAvailFromTime.text = availableFromtime
            cell.fleetAvailTillDate.text = availableTilldate
            cell.fleetAvailTillTime.text = availableTilltime
            let cargoWt = "\(fleetDetails.capacityWeight)" + " \(fleetDetails.capacityWeightUnit.name)"
            cell.fleetCargoWt.text = cargoWt
                
                if fleetDetails.expectedFreight != nil{
                     cell.freightAmountLabel.text = "\(fleetDetails.expectedFreight!)"
                }
           
             cell.fleetRegNoLabel.text = fleetDetails.registrationNo
            }
        }
        
        return cell
    }
    
      var fleetDetails = [String:Any]()
    @objc func editFleetPosted(recognizer: UITapGestureRecognizer){
        
         let tag = recognizer.view?.tag
        self.fleetDetails  = ["fleetDetails":self.datumArray[tag!],"state" : "FleetPosted"]
          sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "PostFleetVC") }, with: "fourth")
        sideMenuController?.setContentViewController(with: "fourth")
      
         NotificationCenter.default.post(name: .editFleetPosted,object: nil,userInfo: self.fleetDetails)
        
    }
    
    @objc func invite(recognizer: UITapGestureRecognizer){
        
        let tag = recognizer.view?.tag
        
     sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "FleetInvitedViewController") }, with: "29")
        sideMenuController?.setContentViewController(with: "29")
        
        NotificationCenter.default.post(name: .fleetInvited, object: nil,userInfo: ["bidId": self.datumArray[tag!].id])
        
    }
    
    @objc func ViewBid(recognizer: UITapGestureRecognizer){
        sideMenuController?.setContentViewController(with: "28")
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath
        let selectedRowIndex = indexPath
        currentRow = selectedRowIndex.row
        cellTapped = !cellTapped
        self.tableView.reloadDataWithAutoSizingCellWorkAround()
    }
    
    @IBAction func openClicked(_ sender: UITapGestureRecognizer) {
        print("openClicked")
        if openLabel.textColor == UIColor(hexString: "#6F7179"){
            openLabel.textColor = UIColor(hexString: "#0F2541")
            self.openRedLine.isHidden = false
            self.closeRedline.isHidden = true
            closeLabel.textColor = UIColor(hexString: "#6F7179")
            closeClicked = false
            isOpenClicked = true
          
        }else if openLabel.textColor == UIColor(hexString: "#0F2541"){
            
        }
        pageNo = 0
         self.status = "OPEN"
        self.searchTF.text = ""
        self.fromDateTF.text = ""
        self.toDateTF.text = ""
         isDragging = false
        self.datumArray.removeAll()
          self.fleetsPosted(search: self.searchTF.text!)
        self.tableView.reloadData()
        
    }
    
    
   
    @IBAction func closeClicked(_ sender: UITapGestureRecognizer) {
       
        if closeLabel.textColor == UIColor(hexString: "#6F7179"){
            closeLabel.textColor = UIColor(hexString: "#0F2541")
            self.openRedLine.isHidden = true
            self.closeRedline.isHidden = false
            openLabel.textColor = UIColor(hexString: "#6F7179")
            closeClicked = true
              isOpenClicked = false
           
           
        }else if closeLabel.textColor == UIColor(hexString: "#0F2541"){
            //                        self.closeLabel.textColor = UIColor(hexString: "#6F7179")
            //
            //                        self.openRedLine.isHidden = true
            //                        self.closeRedline.isHidden = true
        }
         self.status = "CLOSED"
        pageNo = 0
        isDragging = false
        self.searchTF.text = ""
        self.fromDateTF.text = ""
        self.toDateTF.text = ""
        self.datumArray.removeAll()
        self.fleetsPosted(search: self.searchTF.text!)
        self.tableView.reloadData()
       
    }
    
   
    @IBAction func fromDateClicked(_ sender: Any) {
        
        self.picker(state: "fleetPosted-FromDate")
    }
    
    
    @IBAction func toDateClicked(_ sender: Any) {
         self.picker(state: "fleetPosted-ToDate")
        
    }
    
   
    
    
    
    
}



extension NSLayoutConstraint {
    /**
     Change multiplier constraint
     
     - parameter multiplier: CGFloat
     - returns: NSLayoutConstraint
     */
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {
        
        NSLayoutConstraint.deactivate([self])
        
        let newConstraint = NSLayoutConstraint(
            item: firstItem,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)
        
        newConstraint.priority = priority
        newConstraint.shouldBeArchived = self.shouldBeArchived
        newConstraint.identifier = self.identifier
        
        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}
