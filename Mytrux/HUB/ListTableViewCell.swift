//
//  ListTableViewCell.swift
//  Mytrux
//
//  Created by Mukta Bhuyar Punjabi on 28/05/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class ListTableViewCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var mainView: UIView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        addDashedBorder(view: mainView)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension UIView {
    
    func addDashedBorder(view : UIView) {
        //Create a CAShapeLayer
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.white.cgColor
        shapeLayer.lineWidth = 2
        // passing an array with the values [2,3] sets a dash pattern that alternates between a 2-user-space-unit-long painted segment and a 3-user-space-unit-long unpainted segment
        shapeLayer.lineDashPattern = [2,3]
        
        let path = CGMutablePath()
        path.move(to: CGPoint.zero)
        path.addLine(to: CGPoint(x:view.frame.width, y: 0))
        shapeLayer.path = path
        layer.addSublayer(shapeLayer)
    }
}

