//
//  WalletViewController.swift
//  Mytrux
//
//  Created by Aboli on 26/06/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import SideMenuSwift
import Alamofire
import SwiftyJSON

class WalletViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
   
    var cellTapped:Bool = false
    var currentRow = 0;
    var checked = Set<IndexPath>()
    var selectedIndex = IndexPath()
    var isWalletClicked = false
    var isPaymentClicked = false
    var isTransactionClicked = false
    @IBOutlet weak var walletLabel: UILabel!
    @IBOutlet weak var paymentMainView: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var paymentTableView: UITableView!
    @IBOutlet weak var paymentImageView: UIImageView!
    @IBOutlet weak var transactionImageView: UIImageView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var transactionMainView: UIView!
    @IBOutlet weak var priceView1: UIView!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var topView1: UIView!
    @IBOutlet weak var topView2: UIView!
    @IBOutlet weak var transactionTableView: UITableView!
    @IBOutlet weak var paymentLabel: UILabel!
    @IBOutlet weak var transactionLabel: UILabel!
    @IBOutlet weak var walletImageView: UIImageView!
    @IBOutlet weak var walletBalanceView: UIView!
    @IBOutlet weak var walletView: UIView!
    @IBOutlet weak var topView3: UIView!
    @IBOutlet weak var paymentView: UIView!
    @IBOutlet weak var transactionView: UIView!
    @IBOutlet weak var makePaymentView: UIView!
    @IBOutlet weak var priceView2: UIView!
    @IBOutlet weak var priceView3: UIView!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var image3: UIImageView!
    @IBOutlet weak var amount1: UILabel!
    @IBOutlet weak var checkImage1: UIImageView!
    @IBOutlet weak var changesPerTrans_1: UILabel!
    @IBOutlet weak var transactions_1: UILabel!
    @IBOutlet weak var users_1: UILabel!
    @IBOutlet weak var chargesPerTrans_2: UILabel!
    @IBOutlet weak var transactions_2: UILabel!
    @IBOutlet weak var checkImage2: UIImageView!
    @IBOutlet weak var amount2: UILabel!
    @IBOutlet weak var chargesPerTrans_3: UILabel!
    @IBOutlet weak var transactions_3: UILabel!
    @IBOutlet weak var users_3: UILabel!
    @IBOutlet weak var checkImage3: UIImageView!
    @IBOutlet weak var amount3: UILabel!
    @IBOutlet weak var users_2: UILabel!
    @IBOutlet weak var amountBalance: UILabel!
    @IBOutlet weak var balance: UILabel!
    var selectedPlanIndex = Int()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topBarCard(topBar: topBar)
        cardViewArray(arrView: [walletBalanceView,view1,view2,view3])
       
        priceView1.layer.cornerRadius = priceView1.frame.height / 2
        priceView2.layer.cornerRadius = priceView2.frame.height / 2
        priceView3.layer.cornerRadius = priceView3.frame.height / 2
        buttonCardArray(arrView: [makePaymentView])
        paymentTableView.delegate = self
        paymentTableView.dataSource = self
        transactionTableView.delegate = self
        transactionTableView.dataSource = self
        paymentMainView.isHidden = true
        transactionMainView.isHidden = true
        self.walletLabel.textColor = UIColor.white
        walletView.backgroundColor = UIColor(hexString:ColorConstants.RED)
        walletImageView.image = UIImage(named: "wallet")
        walletImageView.image = walletImageView.image?.withRenderingMode(.alwaysTemplate)
        walletImageView.tintColor = UIColor.white
        paymentImageView.image = UIImage(named: "payment")
        paymentImageView.image = paymentImageView.image?.withRenderingMode(.alwaysTemplate)
        paymentImageView.tintColor = UIColor(hexString: ColorConstants.RED)
        transactionImageView.image = UIImage(named: "transactions")
        transactionImageView.image = transactionImageView.image?.withRenderingMode(.alwaysTemplate)
        transactionImageView.tintColor = UIColor(hexString: ColorConstants.RED)
        titleLabel.text = "Wallet"
        sideMenuController?.clearCache(with: "fifth")
        getAmount()
        paymentPlan()
        payment()
        transactions()
        selectedPlanIndex = -1
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        topView1.roundCorners(corners: [.topLeft, .topRight], radius: 5.0)
        topView2.roundCorners(corners: [.topLeft, .topRight], radius: 5.0)
        topView3.roundCorners(corners: [.topLeft, .topRight], radius: 5.0)
    }
   
    @IBAction func menuBtn(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    @IBAction func walletClicked(_ sender: Any) {
         titleLabel.text = "Wallet"
        if isWalletClicked == false{
            
         paymentMainView.isHidden = true
        toggleViews(mainView: walletView, view1: paymentView, view2: transactionView, mainLabel: walletLabel, label1: paymentLabel, label2: transactionLabel)
        toggleImage(mainImage: walletImageView, image1: paymentImageView, image2: transactionImageView, mainImageName: "wallet", imageName1: "payment", imageName2: "transactions")
            isWalletClicked = true
         isPaymentClicked = false
           isTransactionClicked = false
            self.paymentMainView.isHidden = true
            self.transactionMainView.isHidden = true
        }
        else{
            
        }
    }
    
    @IBAction func paymentClicked(_ sender: Any) {
         titleLabel.text = "Payment"
          if isPaymentClicked == false{
            paymentMainView.isHidden = false
            self.toggleViews(mainView: paymentView, view1: walletView, view2: transactionView, mainLabel: paymentLabel, label1: walletLabel, label2: transactionLabel)
            self.toggleImage(mainImage: paymentImageView, image1: walletImageView, image2: transactionImageView, mainImageName: "payment", imageName1: "wallet", imageName2: "transactions")
            isWalletClicked = false
            isPaymentClicked = true
            isTransactionClicked = false
            self.paymentMainView.isHidden = false
            self.transactionMainView.isHidden = true
            self.paymentTableView.reloadData()
          }else{
            
        }
  
    }
    
    @IBAction func transactionClicked(_ sender: Any) {
         titleLabel.text = "Transactions"
         if isTransactionClicked == false{
           paymentMainView.isHidden = false
        self.toggleViews(mainView: transactionView, view1: paymentView, view2: walletView, mainLabel: transactionLabel, label1: paymentLabel, label2: walletLabel)
          self.toggleImage(mainImage: transactionImageView, image1: walletImageView, image2: paymentImageView, mainImageName: "transactions", imageName1: "wallet", imageName2: "payment")
            isWalletClicked = false
            isPaymentClicked = false
            isTransactionClicked = true
            self.paymentMainView.isHidden = true
            self.transactionMainView.isHidden = false
              self.transactionTableView.reloadData()
         }else{
            
        }
    }
    var paymentPlannn : PaymentPlan?
    @IBAction func view1Clicked(_ sender: Any) {
         selectedPlanIndex = 0
        paymentPlannn = self.paymentPlann[0]
        toggleTopViews(mainView: topView1, view1: topView2, view2: topView3, mainImage: image1, image1: image2, image2: image3, mainImageName: "selectedCircle", imageName1: "unselectedCircle", imageName2: "unselectedCircle")
    }
    
    @IBAction func view2Clicked(_ sender: Any) {
         selectedPlanIndex = 1
        paymentPlannn = self.paymentPlann[1]
        toggleTopViews(mainView: topView2, view1: topView1, view2: topView3, mainImage: image2, image1: image1, image2: image3, mainImageName: "selectedCircle", imageName1: "unselectedCircle", imageName2: "unselectedCircle")
    }
    
    @IBAction func view3Clicked(_ sender: Any) {
        selectedPlanIndex = 2
        paymentPlannn = self.paymentPlann[2]
        toggleTopViews(mainView: topView3, view1: topView1, view2: topView2, mainImage: image3, image1: image1, image2: image2, mainImageName: "selectedCircle", imageName1: "unselectedCircle", imageName2: "unselectedCircle")
    }
    
    func toggleTopViews(mainView:UIView,view1:UIView,view2:UIView,mainImage:UIImageView,image1:UIImageView,image2:UIImageView,mainImageName:String,imageName1:String,imageName2:String){
        mainView.backgroundColor = UIColor(hexString: ColorConstants.DARK_BLUE)
        view1.backgroundColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
        view2.backgroundColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
        mainImage.image = UIImage(named: mainImageName)
        
        image1.image = UIImage(named: imageName1)
       
        image2.image = UIImage(named: imageName2)
     
        
    }
    
    func toggleViews(mainView:UIView,view1:UIView,view2:UIView,mainLabel:UILabel,label1:UILabel,label2:UILabel){
        mainView.backgroundColor = UIColor(hexString: ColorConstants.RED)
        label1.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
        label2.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
        mainLabel.textColor = UIColor.white
        view1.backgroundColor = UIColor.white
        view2.backgroundColor = UIColor.white
        
    }
    
    func toggleImage(mainImage:UIImageView,image1:UIImageView,image2:UIImageView,mainImageName:String,imageName1:String,imageName2:String){
        mainImage.image = UIImage(named: mainImageName)
        mainImage.image = mainImage.image?.withRenderingMode(.alwaysTemplate)
        mainImage.tintColor = UIColor.white
        image1.image = UIImage(named: imageName1)
        image1.image = image1.image?.withRenderingMode(.alwaysTemplate)
        image1.tintColor = UIColor(hexString: ColorConstants.RED)
        image2.image = UIImage(named: imageName2)
        image2.image = image2.image?.withRenderingMode(.alwaysTemplate)
        image2.tintColor = UIColor(hexString: ColorConstants.RED)
    }


    func getAmount(){
        
        let headers = self.header()
        
        Alamofire.request(URLStatics.getAmount, method: .get, headers: headers).responseJSON { response in
            print("response.request=\(String(describing: response.request))")  // original URL request
            print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
            print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
            
            if let reply = response.result.value {
                
                let json = JSON(reply)
                print(" getAmount  json =======  : \(json)")
                let data = json["data"].dictionaryObject
                print("amount === \(String(describing: data!["walletAmount"]!))")
                self.amountBalance.text = "\(data!["walletAmount"]!)" + ".00"
                self.balance.text = "\(data!["remainsTr"]!)"
                
            }
        }
        
    }
    
    
    var paymentArray = [Payment]()
     var transactionArray = [Transactions]()
    var paymentPlann = [PaymentPlan]()
    func paymentPlan(){
        var parameters = [String:Any]()
        let userType = userDefaults.value(forKey: LoginDataConstants.userType) as! String
        var type = String()
        if userType == "FLEET_ADMIN" || userType == "FLEET_USER"{
            type = "FLEET"
        }
        parameters = [
            "companyType" : type,
            "status" : "true",
            "recordsPerPage" : 3,
            "appType" : "HUB",
            "pageNo" : 0,
            "financialYear" : self.currentFinancialYear!
        ]
           let headers = self.header()
           
        Alamofire.request(URLStatics.paymentPlan, method: .get, parameters: parameters,  headers: headers ).responseJSON { response in
               print("response.request=\(String(describing: response.request))")  // original URL request
               print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
               print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
               
               if let reply = response.result.value {
                   
                   let json = JSON(reply)
                   print(" paymentPlanpaymentPlan  json =======  : \(json)")
                let data = json["data"].arrayObject as! [[String:Any]]
                for plan in data{
                     let paym = PaymentPlan(id: plan["id"] as? Int, amount: plan["amount"] as? Int, validityDays: plan["validityDays"] as? Int, status: plan["status"] as? Bool, perTransactionFee: plan["perTransactionFee"] as? Int, companyType: plan["companyType"] as? String, maxUser: plan["maxUser"] as? Int, totalTransaction: plan["totalTransaction"] as? Int, totalAmount: plan["totalAmount"] as? Int, taxPercentage: plan["taxPercentage"] as? Int, taxableAmount: plan["taxableAmount"] as? Int, count: plan["count"] as? Int)
                    self.paymentPlann.append(paym)
                }
                print("self.paymentPlann.count === \(self.paymentPlann.count)")
                self.amount1.text = "\(data[0]["amount"]! as! Double)"
                self.amount2.text = "\(data[1]["amount"]! as! Double)"
                self.amount3.text = "\(data[2]["amount"]! as! Double)"
                self.changesPerTrans_1.text = "\(data[0]["perTransactionFee"]! as! Double)"
                self.chargesPerTrans_2.text = "\(data[1]["perTransactionFee"]! as! Double)"
                self.chargesPerTrans_3.text = "\(data[2]["perTransactionFee"]! as! Double)"
                self.transactions_1.text = "\(data[0]["totalTransaction"]!)"
                self.transactions_2.text = "\(data[1]["totalTransaction"]!)"
                self.transactions_3.text = "\(data[2]["totalTransaction"]!)"
                self.users_1.text = "\(data[0]["maxUser"]!)"
                self.users_2.text = "\(data[1]["maxUser"]!)"
                self.users_3.text = "\(data[2]["maxUser"]!)"
                   
               }
           }
           
       }
       
    
    func payment(){
     var parameters = [String:Any]()
    
     parameters = [
         "receiptId" : "captured",
         "pageSize" : 10,
         "pageNo" : 0,
         "financialYear" : self.currentFinancialYear!
     ]
        let headers = self.header()
        
     Alamofire.request(URLStatics.addPayment+"captured", method: .get, parameters: parameters,  headers: headers ).responseJSON { response in
            print("response.request=\(String(describing: response.request))")  // original URL request
            print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
            print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
            
            if let reply = response.result.value {
                
                let json = JSON(reply)
                var planDict : [String:Any]?
                
                print(" addPaymentaddPayment  json =======  : \(json)")
            let data = json["data"].arrayObject as! [[String:Any]]
                for dataa in data{
                     
                    planDict = dataa["plan"] as? [String:Any]
                    let plan = Plan(id: planDict?["id"] as? Int, amount: planDict?["amount"] as? Double, validityDays: planDict?["validityDays"] as? Int, status: planDict?["status"] as? Bool, perTransactionFee: planDict?["perTransactionFee"] as? Double, companyType: planDict?["companyType"] as? String, maxUser: planDict?["maxUser"] as? Int, appType: planDict?["appType"] as? String, totalTransaction: planDict?["totalTransaction"] as? Int, totalAmount: planDict?["totalAmount"] as? Double, taxPercentage: planDict?["taxPercentage"] as? Double, taxableAmount: planDict?["taxableAmount"] as? Double)
                    let payment = Payment(appType: dataa["appType"] as? String, transDate: dataa["transDate"] as? Int, createdDate: dataa["createdDate"] as? Int, receiptNo: dataa["receiptNo"] as? String, billPartyCompanyName: dataa["billPartyCompanyName"] as? String, email: dataa["email"] as? String, transAmount: dataa["transAmount"] as? Int, id: dataa["id"] as? Int, count: dataa["count"] as? Int, status: dataa["status"] as? String, contact: dataa["contact"] as? String, paymentMode: dataa["paymentMode"] as? String, amount: dataa["amount"] as? Int, plan: plan)
                    
                    
                    self.paymentArray.append(payment)
                }
                print("paymentArray count === \(self.paymentArray.count)")
            }
        }
        
    }
    
    func transactions()
        {
         var parameters = [String:Any]()
        
         parameters = [
             "pageSize" : 10,
             "pageNo" : 0,
             "financialYear" : self.currentFinancialYear!
         ]
            let headers = self.header()
            
            Alamofire.request(URLStatics.hubtransactions, method: .get, parameters: parameters,  headers: headers ).responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                
                if let reply = response.result.value {
                    
                    let json = JSON(reply)
                    var exchangeBooking : BookingElement?
                    var exchangeBookingDict : [String:Any]?
                    var loadOwner : LoadOwner1?
                    var loadDict : [String:Any]?
                    var availLoadDict : [String:Any]?
                    var availLoadBid : AvailableBid1?
                    print(" hubtransactionshubtransactions  json =======  : \(json)")
                    
                    
                    
                let data = json["data"].arrayObject as! [[String:Any]]
                    for dataa in data{
                       
                        exchangeBookingDict = dataa["availableLoadBid"] as? [String:Any]
                        availLoadDict = exchangeBookingDict?["availableLoadBid"] as? [String:Any]
                        loadDict = exchangeBookingDict?["loadOwner"] as? [String:Any]
                        loadOwner = LoadOwner1(id: loadDict?["id"] as? Int, name: loadDict?["name"] as? String, registrationNo: loadDict?["registrationNo"] as? String, companyCode: loadDict?["companyCode"] as? String, telephone: loadDict?["telephone"] as? String, fax: loadDict?["fax"] as? String, address: nil, typeOfCompany: nil, logo: nil, gstTax: nil, gstTaxRegNo: loadDict?["gstTaxRegNo"] as? String, jurisdiction: nil, timeZone: loadDict?["timeZone"] as? String, panNo: loadDict?["panNo"] as? String, vatNo: loadDict?["vatNo"] as? String, iecNo: loadDict?["iecNo"] as? String, cinNo: nil, cstNo: nil, registrationDate: loadDict?["registrationDate"] as? Int, subscriptionStartDate: loadDict?["subscriptionStartDate"] as? Int, subscriptionEndDate: loadDict?["subscriptionEndDate"] as? Int, currentBalance: loadDict?["currentBalance"] as? Int, smsNotification: nil, companyAccess: nil, loadAdditionalInformation: nil, status: nil, expiredType: nil, activeUserCount: loadDict?["activeUserCount"] as? Int, maxUser: loadDict?["maxUser"] as? Int, maxUserType: nil, exchangeBookingCount: loadDict?["exchangeBookingCount"] as? Int, exWalletValidFrom: loadDict?["exWalletValidFrom"] as? Int, exWalletValidTill: loadDict?["exWalletValidTill"] as? Int, exWalletValidDays: loadDict?["exWalletValidDays"] as? Int, exPlanType: nil, goCount: loadDict?["goCount"] as? Int, proCount: nil, profileMailCount: loadDict?["profileMailCount"] as? Int, ewbFlagStr: nil, planType: nil, exSuwalletAmount: nil, exSuhubMaxUser: nil, exSuallowTransaction: nil, exSuremainsTr: nil, hubPlanID: nil, exSuhubTransactionCost: nil, exPackType: nil)
                        availLoadBid = AvailableBid1(id: availLoadDict?["id"] as? Int, bidCode: availLoadDict?["bidCode"] as? String, creationDate: availLoadDict?["creationDate"] as? Int, closedBidTime: availLoadDict?["closedBidTime"] as? Int, availableLocation: availLoadDict?["availableLocation"] as? String, destinationLocation: availLoadDict?["destinationLocation"] as? String, comments: availLoadDict?["comments"] as? String, availableLatitude: availLoadDict?["availableLatitude"] as? Double, availableLongitude: availLoadDict?["availableLongitude"] as? Double, destinationLatitude: availLoadDict?["destinationLatitude"] as? Double, destinationLongitude: availLoadDict?["destinationLongitude"] as? Double, vehicleType: nil, noOfVehicles: availLoadDict?["noOfVehicles"] as? Int, availableDateTime: availLoadDict?["availableDateTime"] as? Int, cargoType: nil, capacityWeight: availLoadDict?["capacityWeight"] as? Int, capacityWeightUnit: nil, loadType: nil, expectedFreight: availLoadDict?["expectedFreight"] as? Int, currencyMaster: nil, status: availLoadDict?["status"] as? String, registrationNo: availLoadDict?["registrationNo"] as? String, postType: availLoadDict?["postType"] as? String, bidInviteCount: availLoadDict?["bidInviteCount"] as? Int, bidQuotationCount: availLoadDict?["bidQuotationCount"] as? Int, containerNo: availLoadDict?["containerNo"] as? String, sealNo: availLoadDict?["sealNo"] as? String, pinNo: availLoadDict?["pinNo"] as? String, packgs: availLoadDict?["packgs"] as? Int)
                        print("exchangeBookingDict?[bookingNo] as? String === \(String(describing: exchangeBookingDict?["id"] as? Int))")
                        exchangeBooking = BookingElement(id: exchangeBookingDict?["id"] as? Int, shipper: nil, consignee: nil, deliveryCustomer: nil, loadOwner: loadOwner, fleetOwner: nil, billingParty: nil, jobStatus: nil, loadType: nil, cargoType: nil, jobType: nil, bookingType: nil, pickupDateTime: nil, deliveryDateTime: nil, freightTerms: nil, bookingNo: exchangeBookingDict?["bookingNo"] as? String, availableLoadBid: availLoadBid, availableFleetBid: nil, loadBidQuotaion: nil, pickUpLocation: exchangeBookingDict?["pickUpLocation"] as? String, destination: exchangeBookingDict?["destination"] as? String, createDate: exchangeBookingDict?["createDate"] as? Int, vehicleContainerDetails: nil, user: nil, bookingCust: nil, shiper: nil, consigneer: nil, deliverCust: nil, exInvoiceGrossAmount: exchangeBookingDict?["exInvoiceGrossAmount"] as? Int, exInvoiceGrossAmountWithoutTax: exchangeBookingDict?["exInvoiceGrossAmountWithoutTax"] as? Int, exTaxType: exchangeBookingDict?["exTaxType"] as? String, exTaxInfo: nil, exInvoiceNo: exchangeBookingDict?["exInvoiceNo"] as? String, exTaxGroup: nil, invoiceDate: exchangeBookingDict?["invoiceDate"] as? Int, financeYear: exchangeBookingDict?["financeYear"] as? String, salesBillFinal: nil, purchaseBillFinal: nil, defaultBankAndBranch: exchangeBookingDict?["defaultBankAndBranch"] as? String, count: exchangeBookingDict?["count"] as? Int, lrNo: exchangeBookingDict?["lrNo"] as? Int, isinvite: exchangeBookingDict?["isinvite"] as? Bool, eLrTerms: exchangeBookingDict?["eLrTerms"] as? String, salesInvoiceTerms: exchangeBookingDict?["salesInvoiceTerms"] as? String, jobVehiclesDrivers: nil, trips: nil, fleetBidInvite: nil)
                        
                      let transaction = Transactions(fleetBidCode: dataa["fleetBidCode"] as? String, transactionDate: dataa["transactionDate"] as? Int, transType: dataa["transType"] as? String, deductedAmount: dataa["deductedAmount"] as? Int, remainsAmount: dataa["remainsAmount"] as? Int, transReason: dataa["transReason"] as? String, fleet: nil, fleetBidID: dataa["fleetBidID"] as? Int, count: dataa["count"] as? Int, amountWithTax: dataa["amountWithTax"] as? Double, appliedTaxDetails: dataa["appliedTaxDetails"] as? String, id: dataa["id"] as? Int, currency: dataa["currency"] as? String, paymentMode: dataa["paymentMode"] as? String, exchangeBooking: exchangeBooking)
                          self.transactionArray.append(transaction)
                       
                    }
                    print("paymentArray count === \(self.paymentArray.count)")
                }
            }
            
        }
    
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath
        let selectedRowIndex = indexPath
        currentRow = selectedRowIndex.row
        cellTapped = !cellTapped
        if isTransactionClicked == true{
            self.transactionTableView.reloadDataWithAutoSizingCellWorkAround()
        }else{
              self.paymentTableView.reloadDataWithAutoSizingCellWorkAround()
        }
      
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == paymentTableView{
            return self.paymentArray.count
        }else{
            return self.transactionArray.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isTransactionClicked == true{
            let cell = transactionTableView.dequeueReusableCell(withIdentifier: "transactionCell") as! TransactionTableViewCell
            cell.view2.isHidden = true
            if indexPath.row == currentRow {
                if cellTapped == false {
                    cell.view2.isHidden = true
                    self.transactionTableView.rowHeight = 130
                }else{
                    cell.view2.isHidden = false
                    self.transactionTableView.rowHeight = 290
                    
                }
            }
            if transactionArray.count != 0 {
                let transactionsData = self.transactionArray[indexPath.row]
                
                cell.fleetId.text = transactionsData.fleetBidCode
                cell.vehicleNo.text = "NA"
                let timestamp = transactionsData.transactionDate! / 1000
                let date = epochDate(epochDate: Double(timestamp))
                let time = epochTime(epochTimee: Double(timestamp))
                cell.transactionDate.text = "\(date)" + " \(time)"
                if transactionsData.exchangeBooking?.availableLoadBid?.bidCode != nil || transactionsData.exchangeBooking?.bookingNo != nil {
                    cell.loadId.text = "\(String(describing: transactionsData.exchangeBooking?.availableLoadBid?.bidCode!))"
                    cell.bookingNo.text = "\(String(describing: transactionsData.exchangeBooking?.bookingNo!))"
                }else{
                    cell.loadId.text = "NA"
                                  cell.bookingNo.text = "NA"
                }
              
                  let trans = UITapGestureRecognizer(target: self, action: #selector(transReceipt(recognizer:)))
                              cell.circularView.addGestureRecognizer(trans)
                              cell.circularView.tag = indexPath.row

            }
            return cell
        }else{
            let cell = paymentTableView.dequeueReusableCell(withIdentifier: "paymentCell") as! PaymentTableViewCell
            cell.view2.isHidden = true
            if indexPath.row == currentRow {
                if cellTapped == false {
                    cell.view2.isHidden = true
                    self.paymentTableView.rowHeight = 130
                }else{
                    cell.view2.isHidden = false
                    self.paymentTableView.rowHeight = 250
                }
                
            }
            if paymentArray.count != 0 {
                let paymentData = self.paymentArray[indexPath.row]
                cell.billingParty.text = paymentData.billPartyCompanyName
                if paymentData.plan?.totalTransaction != nil{
                    cell.transactions.text = "\(String(describing: paymentData.plan?.totalTransaction!))"
                }else{
                    cell.transactions.text = "NA"
                }
                
                if paymentData.status == "CAPTURED"{
                    cell.status.text = "SUCCESS"
                }else{
                    cell.status.text = paymentData.status
                }
                
                cell.invoiceNo.text = paymentData.receiptNo
                cell.selectedPlan.text = "\(paymentData.amount!)"
                cell.amountIncTaxes.text = "\(paymentData.transAmount!)"
                cell.paymentMode.text = paymentData.status
                let timeStamp = paymentData.createdDate! / 1000
                let date = self.epochDate(epochDate: Double(timeStamp))
                let time = self.epochTime(epochTimee: Double(timeStamp))
                cell.invoiceDate.text = date + " " + time
                let invite = UITapGestureRecognizer(target: self, action: #selector(paymentReceipt(recognizer:)))
                cell.circularView.addGestureRecognizer(invite)
                cell.circularView.tag = indexPath.row

            }
            
            return cell
        }
    }
    
    @IBAction func makePaymentClicked(_ sender: Any) {
        if self.selectedPlanIndex == -1 || self.selectedPlanIndex > 2{
            let alert = UIAlertController(title: "", message: "", preferredStyle: UIAlertController.Style.alert)
                                   alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                   self.present(alert, animated: true, completion: nil)
            return
        }
        sideMenuController?.setContentViewController(with: "33")
         NotificationCenter.default.post(name: .paymentPlan, object: nil,userInfo: ["paymentPlan":  paymentPlann[self.selectedPlanIndex]])
    }
    
    @objc func paymentReceipt(recognizer: UITapGestureRecognizer){
        
        let tag = recognizer.view?.tag
        let paymentData = self.paymentArray[tag!]
        let fleetId = userDefaults.value(forKey: LoginDataConstants.fleetId)
        let url = URLStatics.paymentReceipt + "\(fleetId!)" + "/" + "\(paymentData.id!)"
        sideMenuController?.setContentViewController(with: "24")
        NotificationCenter.default.post(name: .paymentPlan, object: nil,userInfo: ["url": url,"state":"payment"])
        
    }
    
    @objc func transReceipt(recognizer: UITapGestureRecognizer){
          
          let tag = recognizer.view?.tag
          let transactionsData = self.transactionArray[tag!]
          let fleetId = userDefaults.value(forKey: LoginDataConstants.fleetId)
          let url = URLStatics.transactionReceipt + "\(transactionsData.id!)?" + "fleetId=\(fleetId!)" + "&" + "loadId="
          sideMenuController?.setContentViewController(with: "24")
          NotificationCenter.default.post(name: .eLR, object: nil,userInfo: ["url": url,"state":"payment"])
          
      }
      
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if cellTapped{
            if indexPath as IndexPath == selectedIndex {
                if isTransactionClicked == true{
                    return 290
                }else{
                     return 250
                }
            }
        }
        return 130
        
    }
 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

