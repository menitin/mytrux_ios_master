//
//  MakeInvoiceViewController.swift
//  Mytrux
//
//  Created by Aboli on 16/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import StepIndicator
import SideMenuSwift

class MakeInvoiceViewController: BaseViewController {
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var stepView: StepIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topBarCard(topBar: topBar)
        stepView.currentStep = 0
            NotificationCenter.default.addObserver(self, selector: #selector(self.billingCustomer(notification:)), name: .billingCustomer, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.billingDetails(notification:)), name: .billingDetails, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.billingPreview(notification:)), name: .billingPreview, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.backClicked(notification:)), name: .backClicked, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.billPreviewBackClicked(notification:)), name: .billPreviewBackClicked, object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    
    @objc func billingCustomer(notification: Notification) {
        
    }
    
    @objc func backClicked(notification: Notification) {
        stepView.currentStep = 0
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
           sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookingController") }, with: "tenth")
              sideMenuController?.setContentViewController(with: "tenth")
    }
    @objc func billPreviewBackClicked(notification: Notification) {
        stepView.currentStep = 1
    }
    
    @objc func billingDetails(notification: Notification) {
         stepView.currentStep = 1
    }
    
    @objc func billingPreview(notification: Notification) {
         stepView.currentStep = 2
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
