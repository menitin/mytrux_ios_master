//
//  SearchTableViewCell.swift
//  Mytrux
//
//  Created by Mytrux on 23/10/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var buttonLabel: UILabel!
    @IBOutlet weak var button: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var bookingNo: UILabel!
    @IBOutlet weak var pickupTime: UILabel!
    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var pickupDate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.layer.cornerRadius = 5
             mainView.layer.shadowColor = UIColor.gray.cgColor
             mainView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
             mainView.layer.shadowRadius = 4
             mainView.layer.shadowOpacity = 0.4
           
             buttonCardArray(arrView: [button])
         }
         
        func buttonCardArray(arrView:[UIView]){
             for button in arrView{
                 button.layer.cornerRadius = button.frame.height / 2
             }
         }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
