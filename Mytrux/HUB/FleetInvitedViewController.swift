//
//  FleetInvitedViewController.swift
//  Mytrux
//
//  Created by Mytrux on 22/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import SideMenuSwift
import Alamofire
import SwiftyJSON

class FleetInvitedViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate{
    
    var isReceivedClicked = false
    var isRejectedClicked = false
    var isLostClicked = false
    var cellTapped:Bool = false
    var currentRow = 0;
    var checked = Set<IndexPath>()
    var selectedIndex = IndexPath()
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchBar: UIView!
    @IBOutlet weak var rejectedView: UIView!
    @IBOutlet weak var fromDate: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var seperatorView: UIImageView!
    @IBOutlet weak var toDate: UIView!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var fromDateTF: UITextField!
    @IBOutlet weak var searchVIew: UIView!
    @IBOutlet weak var receivedView: UIView!
    @IBOutlet weak var lossView: UIView!
    @IBOutlet weak var rejectedImage: UIImageView!
    @IBOutlet weak var searchViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var toDateTF: UITextField!
    @IBOutlet weak var receivedImageView: UIImageView!
    @IBOutlet weak var receivedLabel: UILabel!
    @IBOutlet weak var rejectedLabel: UILabel!
    @IBOutlet weak var lossImage: UIImageView!
    @IBOutlet weak var lossLabel: UILabel!
    @IBOutlet weak var menuBtn: UIView!
    var status = String()
    var isSearchOpen = false
    override func viewDidLoad() {
        super.viewDidLoad()
        topBarCard(topBar: topBar)
        cardViewArray(arrView: [searchBar])
        searchBar.layer.cornerRadius = 8
        tableView.delegate = self
        tableView.dataSource = self
        dateView.isHidden = true
        seperatorView.isHidden = true
        
        sideMenuController?.clearCache(with: "29")
        toggleViews(mainView: receivedView, view1: rejectedView, view2: lossView, mainLabel: receivedLabel, label1: rejectedLabel, label2: lossLabel)
        toggleImage(mainImage: receivedImageView, image1: rejectedImage, image2: lossImage, mainImageName: "open", imageName1: "rejected", imageName2: "losePdf")
        isReceivedClicked = true
        isLostClicked = false
        isRejectedClicked = false
        self.status = "OPEN"
        self.searchViewHeightConstraint = self.searchViewHeightConstraint.setMultiplier(multiplier: 0.07)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.fleetInvited(_:)), name: .fleetInvited, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.fleetInvitedFromDate(_:)), name: .fleetInvitedFromDate, object: nil)
        isDragging = false
        NotificationCenter.default.addObserver(self, selector: #selector(self.fleetInvitedToDate(_:)), name: .fleetInvitedToDate, object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func menuClicked(_ sender: Any) {
        sideMenuController?.revealMenu()
        
    }
    
    var bidId = Int()
    @objc func fleetInvitedFromDate(_ notification: NSNotification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        
        self.fromDateTF.text = (userInfo!["date"] as! String)
        
    }
    
    @objc func fleetInvitedToDate(_ notification: NSNotification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        
        self.toDateTF.text = (userInfo!["date"] as! String)
        
    }
    @IBAction func fromDateClicked(_ sender: Any) {
        self.picker(state: "fleetInvited-FromDate")
    }
    
    @IBAction func toDateClicked(_ sender: Any) {
        self.picker(state: "fleetInvited-ToDate")
    }
    
    @objc func fleetInvited(_ notification: NSNotification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        let bidIdd = userInfo!["bidId"] as! Int
        print("bidId ========= \(bidId)")
        self.bidId = bidIdd
        self.status = "OPEN"
        self.fleetInvitedList(search: self.searchTF.text!)
        
    }

    
    
    @IBAction func receivedClicked(_ sender: UITapGestureRecognizer) {
        if isReceivedClicked == false{
            toggleViews(mainView: receivedView, view1: rejectedView, view2: lossView, mainLabel: receivedLabel, label1: rejectedLabel, label2: lossLabel)
            toggleImage(mainImage: receivedImageView, image1: rejectedImage, image2: lossImage, mainImageName: "open", imageName1: "rejected", imageName2: "losePdf")
            isReceivedClicked = true
            isLostClicked = false
            isRejectedClicked = false
            
        }else{
            
        }
        self.fleetInvitedArray.removeAll()
        self.status = "OPEN"
        pageNo = 0
        fleetInvitedList(search: self.searchTF.text!)
        self.tableView.reloadData()
    }
    @IBAction func rejectedClicked(_ sender: UITapGestureRecognizer) {
        if isRejectedClicked == false{
            toggleViews(mainView: rejectedView, view1: receivedView, view2: lossView, mainLabel: rejectedLabel, label1: receivedLabel , label2: lossLabel)
            toggleImage(mainImage: rejectedImage , image1: receivedImageView, image2: lossImage, mainImageName: "rejected", imageName1: "open", imageName2: "losePdf")
            isReceivedClicked = false
            isLostClicked = false
            isRejectedClicked = true
        }else{
            
        }
        self.fleetInvitedArray.removeAll()
        self.status = "REJECTED"
        pageNo = 0
        fleetInvitedList(search: self.searchTF.text!)
        self.tableView.reloadData()
    }
    var isDragging = false
    var pageNo = 0
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if self.fleetInvitedArray.count > 4 {
            if offsetY > contentHeight - scrollView.frame.size.height - 300 {
                
                pageNo += 1
                
                isDragging = true
                self.fleetInvitedList(search: self.searchTF.text!)
                
                
            }else{
                isDragging = false
            }
            
        }
        
    }
    
   
    var getBidListPara = [String:Any]()
    var fleetInvitedArray = [FleetInvitedDatum]()
    var fromDatee = Int()
    var toDatee = Int()
    
    func fleetInvitedList(search: String){
        var parameters = [String:Any]()
        if fromDateTF.text! != "" && toDateTF.text! != ""{
            
            let dateTime = "\(String(describing: fromDateTF.text!))"
            
            let dateString = dateTime
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let s = dateFormatter.date(from: dateString)
            //print("validDateTime =>> \(String(describing: s!))")
            let dateTimestamp = Int(s!.timeIntervalSince1970)
            // print("dateTimestamp =>> \(dateTimestamp)")
            self.fromDatee = Int((dateTimestamp * 1000))
            
            
            let dateTime1 = "\(String(describing: toDateTF.text!))"
            let dateString1 = dateTime1
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "dd-MM-yyyy"
            let s1 = dateFormatter1.date(from: dateString1)
            //print("validDateTime =>> \(String(describing: s!))")
            let dateTimestamp1 = Int(s1!.timeIntervalSince1970)
            // print("dateTimestamp =>> \(dateTimestamp)")
            self.toDatee = Int((dateTimestamp1 * 1000))
            
            parameters = [
                "pageSize" : 40,
                "pageNo" : "\(self.pageNo)",
                "bidId": self.bidId,
                "search" : "\(search)",
                "financialYear": self.currentFinancialYear!,
                "fromDate": "\(fromDatee)",
                "toDate" : "\(toDatee)"
            ]
            
            
        }else if fromDateTF.text! != ""  {
            
            let dateTime = "\(String(describing: fromDateTF.text!))"
            
            let dateString = dateTime
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let s = dateFormatter.date(from: dateString)
            //print("validDateTime =>> \(String(describing: s!))")
            let dateTimestamp = Int(s!.timeIntervalSince1970)
            // print("dateTimestamp =>> \(dateTimestamp)")
            self.fromDatee = Int((dateTimestamp * 1000))
            
            parameters = [
                "pageSize" : 40,
                "pageNo" : "\(self.pageNo)",
                "bidId": self.bidId,
                "search" : "\(search)",
                "financialYear": self.currentFinancialYear!,
                "fromDate": "\(fromDatee)",
                "toDate" : ""
            ]
            
            
        }else  if toDateTF.text! != "" {
            let dateTime1 = "\(String(describing: toDateTF.text!))"
            let dateString1 = dateTime1
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "dd-MM-yyyy"
            let s1 = dateFormatter1.date(from: dateString1)
            //print("validDateTime =>> \(String(describing: s!))")
            let dateTimestamp1 = Int(s1!.timeIntervalSince1970)
            // print("dateTimestamp =>> \(dateTimestamp)")
            self.toDatee = Int((dateTimestamp1 * 1000))
            parameters = [
                "pageSize" : 40,
                "pageNo" : "\(self.pageNo)",
                "bidId": self.bidId,
                "search" : "\(search)",
                "financialYear": self.currentFinancialYear!,
                "fromDate": "",
                "toDate" : "\(toDatee)"
            ]
            
        }else{
            parameters = [
                "pageSize" : 40,
                "pageNo" : "\(self.pageNo)",
                "bidId": self.bidId,
                "search" : "\(search)",
                "financialYear": self.currentFinancialYear!,
                "fromDate": "",
                "toDate" : ""
            ]
        }
        
        if isDragging == false{
            self.activityIndicatorBegin()
        }
    
        //+918669696004
        //asdfgf1234$
        //8888870909
        //asdfgf1234$
        let headers = header()
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
        
        
        Alamofire.request(URLStatics.inviteList + "\(self.status)" , method: .get, parameters: parameters,headers: headers)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                
                
                switch (response.result) {
                    
                case .success:
                    if self.isDragging == false{
                        self.activityIndicatorEnd()
                    }
                    if let reply = response.result.value {
                        print("======= JSON: Fleet Invited ========  \(reply)")
                        let mainResponse = JSON(reply)
                        
                        var returnDict = [[String:Any]]()
                        var availableFleetBid : AvailableBid?
                        var availableLoadBid : AvailableBid?
                        var availFleetBid = [String:Any]()
                        var availLoadBid = [String:Any]()
                        var vehicleType : VehicleType?
                        var cargoType : CargoType?
                        var document : Logo?
                        var docs : Logo?
                        var documentSmallType : [String:Any]?
                        var docuType = [String:Any]()
                        var cargoTypeArray = [String:Any]()
                        var vehicleTypeDict = [String:Any]()
                        var capacityWeightUnitArr = [String:Any]()
                        //  var capacityWeight : CapacityWeightUnit?
                        var availCapacityWeight : CapacityWeightUnit?
                        var loadType1 : LoadType1?
                        var loadTypeDict = [String:Any]()
                        var currencyMaster : CurrencyMaster?
                        var currencyMasterType = [String:Any]()
                        var loadOwner : LoadOwnerInvited?
                        var loadOwnerArr = [String:Any]()
                        var companyAccess : CompanyAccess?
                        var companyAccessArr = [String:Any]()
                        var contactPersonArray = [[String:Any]]()
                        
                        var cargoTypeLoadArray = [String:Any]()
                        var vehicleTypeLoadDict = [String:Any]()
                        var capacityWeightUnitLoadArr = [String:Any]()
                        var loadTypeLoadDict = [String:Any]()
                        var capacityWeightLoad : CapacityWeightUnit?
                        var currencyMasterLoad : CurrencyMaster?
                        var currencyMasterLoadType = [String:Any]()
                        var availableFleetBidInvite : AvailableFleetBidInvite?
                        var availableFleetBidInviteType : [String:Any]?
                        var loadType1Load : LoadType1?
                        var vehicleTypeLoad : VehicleType?
                        var cargoTypeLoad : CargoType?
                        var documentLoad : Logo?
                        var docsLoad : Logo?
                        var documentSmallTypeLoad : [String:Any]?
                        var docuTypeLoad = [String:Any]()
                        
                        
                        if let returnObj = mainResponse["data"].arrayObject as? [[String:Any]] {
                            returnDict = returnObj
                        }
                        
                        for data in returnDict{
                            print( "data ====== \(data["id"])")
                            var contactPerson = [ContactPerson]()
                            
                            if let returnObject =
                                data["availableFleetBid"] as? [String:Any] {
                                availFleetBid = returnObject
                            }
                            
                            if let returnObjectt =
                                data["availableLoadBid"] as? [String:Any] {
                                availLoadBid = returnObjectt
                            }
                            
                            
                            cargoTypeLoadArray = availLoadBid["cargoType"] as! [String : Any]
                             availableFleetBidInviteType = data["availableFleetBidInvite"] as? [String : Any]
                            vehicleTypeLoadDict = availLoadBid["vehicleType"] as! [String:Any]
                            cargoTypeArray = availFleetBid["cargoType"] as! [String : Any]
                            vehicleTypeDict = availFleetBid["vehicleType"] as! [String:Any]
                            
                            if let capacityWeight = availFleetBid["capacityWeightUnit"] as? [String:Any] {
                                capacityWeightUnitArr = capacityWeight
                            }
                            
                            if let capacityWeight = availLoadBid["capacityWeightUnit"] as? [String:Any] {
                                capacityWeightUnitLoadArr = capacityWeight
                            }
                            
                            availableFleetBidInvite = AvailableFleetBidInvite(id: availableFleetBidInviteType?["id"] as? Int, availableFleetBid: nil, loadOwner: nil, inviteDate: nil, availableLoadBid: nil, inviteStatus: nil)
                            print("availableFleetBidInviteType ===== \(String(describing: availableFleetBidInviteType?["id"] as? Int))")
                            loadOwnerArr = data["loadOwner"] as! [String:Any]
                            
                            
                            currencyMasterType = availFleetBid["currencyMaster"] as! [String : Any]
                            currencyMaster = CurrencyMaster(id: currencyMasterType["id"] as? Int, currencyCode: currencyMasterType["currencyCode"] as? String)
                            
                            //                            currencyMasterLoadType = availLoadBid["currencyMaster"] as! [String : Any]
                            //                            currencyMasterLoad = CurrencyMaster(id: currencyMasterLoadType["id"] as? Int, currencyCode: currencyMasterLoadType["currencyCode"] as? String)
                            
                            
                            if let returnObject =
                                loadOwnerArr["contactPersons"] as? [[String:Any]] {
                                contactPersonArray = returnObject
                            }
         
                            loadTypeDict = availFleetBid["loadType"] as! [String:Any]
                            loadTypeLoadDict = availLoadBid["loadType"] as! [String:Any]
                            companyAccessArr = loadOwnerArr["companyAccess"] as! [String:Any]
                            
                            loadType1 = LoadType1(id: loadTypeDict["id"] as! Int, loadType: loadTypeDict["loadType"] as! String, code: loadTypeDict["code"] as! String)
                            loadType1Load = LoadType1(id: loadTypeLoadDict["id"] as! Int, loadType: loadTypeLoadDict["loadType"] as! String, code: loadTypeLoadDict["code"] as! String)
                            
                            
                            availCapacityWeight = CapacityWeightUnit(id: capacityWeightUnitArr["id"] as? Int, name: capacityWeightUnitArr["name"] as? String, type: capacityWeightUnitArr["type"] as? String)
                            
                            
                            capacityWeightLoad = CapacityWeightUnit(id: capacityWeightUnitLoadArr["id"] as? Int, name: capacityWeightUnitLoadArr["name"] as? String, type: capacityWeightUnitLoadArr["type"] as? String)
                            
                            
                            
                            cargoType = CargoType(id: cargoTypeArray["id"] as? Int,
                                                  cargoType: cargoTypeArray["cargoType"] as? String)
                            
                            cargoTypeLoad = CargoType(id: cargoTypeLoadArray["id"] as? Int,
                                                      cargoType: cargoTypeLoadArray["cargoType"] as? String)
                            
                            documentSmallType = vehicleTypeDict["documentSmall"] as? [String:Any]
                            docuType = vehicleTypeDict["document"] as! [String:Any]
                            
                            documentSmallTypeLoad = vehicleTypeLoadDict["documentSmall"] as? [String:Any]
                            docuTypeLoad = vehicleTypeLoadDict["document"] as! [String:Any]
                            
                            document = Logo(id: documentSmallType?["id"] as? Int,mimeType: documentSmallType?["mimeType"] as? String,type: ((documentSmallType?["type"] as? String)))
                            
                            docs = Logo(id: docuType["id"] as? Int, mimeType: docuType["mimeType"] as? String, type: ((docuType["type"] as? String)))
                            
                            documentLoad = Logo(id: documentSmallTypeLoad?["id"] as? Int,mimeType: documentSmallTypeLoad?["mimeType"] as? String,type: ((documentSmallTypeLoad?["type"] as? String)))
                            
                            docsLoad = Logo(id: docuTypeLoad["id"] as? Int, mimeType: docuTypeLoad["mimeType"] as? String, type: ((docuTypeLoad["type"] as? String)))
                            
                            vehicleType = VehicleType(id: vehicleTypeDict["id"] as! Int,
                                                      name: vehicleTypeDict["name"] as! String,
                                                      sequence: Int(vehicleTypeDict["sequence"] as! Double),
                                                      container: vehicleTypeDict["container"] as! Bool,
                                                      type: vehicleTypeDict["type"] as! String,
                                                      document: docs!, documentSmall: document!,
                                                      vehicleOrder: vehicleTypeDict["vehicleOrder"] as! Int,
                                                      weightCapacity: vehicleTypeDict["weightCapacity"] as? Int,
                                                      documentID: vehicleTypeDict["documentId"] as? Int,
                                                      smallDocID: vehicleTypeDict["smallDocId"] as? Int)
                            
                            vehicleTypeLoad = VehicleType(id: vehicleTypeDict["id"] as! Int,
                                                          name: vehicleTypeDict["name"] as! String,
                                                          sequence: Int(vehicleTypeDict["sequence"] as! Double),
                                                          container: vehicleTypeDict["container"] as! Bool,
                                                          type: vehicleTypeDict["type"] as! String,
                                                          document: docsLoad!, documentSmall: documentLoad!,
                                                          vehicleOrder: vehicleTypeDict["vehicleOrder"] as! Int,
                                                          weightCapacity: vehicleTypeDict["weightCapacity"] as? Int,
                                                          documentID: vehicleTypeDict["documentId"] as? Int,
                                                          smallDocID: vehicleTypeDict["smallDocId"] as? Int)
                            
                            
                            availableFleetBid = AvailableBid(id: availFleetBid["id"] as! Int, bidCode: availFleetBid["bidCode"] as! String,
                                                             creationDate: availFleetBid["creationDate"] as! Int,
                                                             closedBidTime: availFleetBid["closedBidTime"] as! Int,
                                                             availableLocation: availFleetBid["availableLocation"] as! String,
                                                             destinationLocation: availFleetBid["destinationLocation"] as? String,
                                                             comments: availFleetBid["comments"] as? String,
                                                             availableLatitude: availFleetBid["availableLatitude"] as? Double,
                                                             availableLongitude: availFleetBid["availableLongitude"] as? Double,
                                                             destinationLatitude: availFleetBid["destinationLatitude"] as? Double,
                                                             destinationLongitude: availFleetBid["destinationLongitude"] as? Double,
                                                             bidInviteCount:                    availFleetBid["bidInviteCount"] as? Int,
                                                             vehicleType: vehicleType!,
                                                             noOfVehicles: availFleetBid["noOfVehicles"] as! Int,
                                                             availableDateTime: availFleetBid["availableDateTime"] as! Int,
                                                             cargoType: cargoType!,
                                                             capacityWeight: availFleetBid["capacityWeight"] as! Int, capacityWeightUnit: availCapacityWeight!, loadType: loadType1!,
                                                             expectedFreight: availFleetBid["expectedFreight"] as? Int,
                                                             currencyMaster: currencyMaster,
                                                             status: availFleetBid["status"] as! String,
                                                             registrationNo: availFleetBid["registrationNo"] as? String,
                                                             packgs: availFleetBid["packgs"] as? Int,
                                                             containerNo: availFleetBid["containerNo"] as? String)
                            
                            
                            availableLoadBid = AvailableBid(id: availLoadBid["id"] as! Int, bidCode: availLoadBid["bidCode"] as! String,
                                                            creationDate: availLoadBid["creationDate"] as! Int,
                                                            closedBidTime: availLoadBid["closedBidTime"] as! Int,
                                                            availableLocation: availLoadBid["availableLocation"] as! String,
                                                            destinationLocation: availLoadBid["destinationLocation"] as? String,
                                                            comments: availLoadBid["comments"] as? String,
                                                            availableLatitude: availLoadBid["availableLatitude"] as? Double,
                                                            availableLongitude: availLoadBid["availableLongitude"] as? Double,
                                                            destinationLatitude: availLoadBid["destinationLatitude"] as? Double,
                                                            destinationLongitude: availLoadBid["destinationLongitude"] as? Double,
                                                            bidInviteCount:                    availLoadBid["bidInviteCount"] as? Int,
                                                            vehicleType: vehicleTypeLoad!,
                                                            noOfVehicles: availLoadBid["noOfVehicles"] as! Int,
                                                            availableDateTime: availLoadBid["availableDateTime"] as! Int,
                                                            cargoType: cargoTypeLoad!,
                                                            capacityWeight: availLoadBid["capacityWeight"] as! Int,
                                                            
                                                            capacityWeightUnit: capacityWeightLoad!,
                                                            loadType: loadType1Load!,
                                                            expectedFreight: availLoadBid["expectedFreight"] as? Int,
                                                            currencyMaster: nil,
                                                            status: availLoadBid["status"] as! String,
                                                            registrationNo: availLoadBid["registrationNo"] as? String,
                                                            packgs: availLoadBid["packgs"] as? Int,
                                                            containerNo: availLoadBid["containerNo"] as? String)
                            
                            
                            
                            
                            
                            
                            companyAccess = CompanyAccess(id:companyAccessArr["id"] as! Int)
                            
                            
                            for contact in contactPersonArray{
                                let con = ContactPerson(id: contact["id"] as! Int, fullName: contact["fullName"] as! String, contactNo: contact["contactNo"] as! String, email: contact["email"] as! String, main: contact["main"] as! Bool)
                                contactPerson.append(con)
                                
                            }
                            
                            
                            loadOwner = LoadOwnerInvited(id: loadOwnerArr["id"] as! Int, name: loadOwnerArr["name"] as! String, registrationNo: loadOwnerArr["registrationNo"] as! String, companyCode: loadOwnerArr["companyCode"] as! String, telephone: loadOwnerArr["telephone"] as! String, fax: loadOwnerArr["fax"] as! String, gstTax: loadOwnerArr["gstTax"] as! Double,
                                                         gstTaxRegNo: loadOwnerArr["gstTaxRegNo"] as! String,
                                                         jurisdiction: loadOwnerArr["jurisdiction"] as! String,
                                                         timeZone: loadOwnerArr["timeZone"] as! String,
                                                         panNo: loadOwnerArr["panNo"] as! String,
                                                         vatNo: loadOwnerArr["vatNo"] as! String,
                                                         iecNo: loadOwnerArr["iecNo"] as! String,
                                                         cinNo: loadOwnerArr["cinNo"] as! String,
                                                         cstNo: loadOwnerArr["cstNo"] as! String,
                                                         registrationDate: loadOwnerArr["registrationDate"] as! Int,
                                                         subscriptionStartDate: loadOwnerArr["subscriptionStartDate"] as! Int,
                                                         subscriptionEndDate: loadOwnerArr["subscriptionEndDate"] as! Int,
                                                         currentBalance: loadOwnerArr["currentBalance"] as! Int,
                                                         smsNotification: loadOwnerArr["smsNotification"] as! String,
                                                         contactPersons: contactPerson,
                                                         companyAccess: companyAccess!,
                                                         status: loadOwnerArr["status"] as! String,
                                                         expiredType: loadOwnerArr["expiredType"] as! String,
                                                         activeUserCount: loadOwnerArr["activeUserCount"] as! Int,
                                                         maxUser: loadOwnerArr["maxUser"] as! Int,
                                                         maxUserType: loadOwnerArr["maxUserType"] as! String,
                                                         exchangeBookingCount: loadOwnerArr["exchangeBookingCount"] as! Int,
                                                         exPlanType: loadOwnerArr["exPlanType"] as! String,
                                                         goCount: loadOwnerArr["goCount"] as! Int,
                                                         proCount: loadOwnerArr["proCount"] as! Int,
                                                         profileMailCount: loadOwnerArr["profileMailCount"] as! Int,
                                                         ewbFlagStr: loadOwnerArr["ewbFlagStr"] as! String,
                                                         planType: loadOwnerArr["planType"] as! String,
                                                         exSuwalletAmount: loadOwnerArr["exSuwalletAmount"] as! Int,
                                                         exSuhubMaxUser: loadOwnerArr["exSuhubMaxUser"] as! Int,
                                                         exSuallowTransaction: loadOwnerArr["exSuallowTransaction"] as! Int,
                                                         exSuremainsTr: loadOwnerArr["exSuremainsTr"] as! Int,
                                                         hubPlanID: loadOwnerArr["hubPlanId"] as! Int,
                                                         exSuhubTransactionCost: loadOwnerArr["exSuhubTransactionCost"] as! Int,
                                                         exPackType: loadOwnerArr["exPackType"] as! String)
                            
                            
                            
                            
                            
                            
                            
                            let datum = FleetInvitedDatum(id: data["id"] as! Int,
                                                          availableFleetBid: availableFleetBid!,
                                                          loadOwner: loadOwner!,
                                                          inviteDate: data["inviteDate"] as? Int,
                                                          availableLoadBid: availableLoadBid!,
                                                          inviteStatus: data["inviteSatus"] as? String,
                                                          count: data["count"] as? Int, availableFleetBidInvite: availableFleetBidInvite!)
                            
                            
                            if self.fleetInvitedArray.contains(where: { bid in bid.id == datum.id }) {
                                print(" exists in the array")
                            } else {
                                print(" does not exists in the array")
                                self.fleetInvitedArray.append(datum)
                            }
                            
                            
                            
                        }
                        
                        print("fleetInvitedArray ==== \(self.fleetInvitedArray.count)")
                        
                        self.tableView.reloadData()
                    }
                    break
                case .failure(let error):
                    
                    print("error \(error.localizedDescription)")
                    break
                }
        }
        
    }
    
    
    @IBAction func lostClicked(_ sender: UITapGestureRecognizer) {
        if isLostClicked == false{
            toggleViews(mainView: lossView, view1: rejectedView, view2: receivedView, mainLabel: lossLabel, label1: rejectedLabel, label2: receivedLabel)
            toggleImage(mainImage: lossImage, image1: rejectedImage, image2: receivedImageView, mainImageName: "losePdf", imageName1: "rejected", imageName2: "open")
            isReceivedClicked = false
            isLostClicked = true
            isRejectedClicked = false
        }else{
            
        }
        self.fleetInvitedArray.removeAll()
        self.status = "LOSS"
        pageNo = 0
        fleetInvitedList(search: self.searchTF.text!)
        self.tableView.reloadData()
    }
    
    
    func toggleViews(mainView:UIView,view1:UIView,view2:UIView,mainLabel:UILabel,label1:UILabel,label2:UILabel){
        mainView.backgroundColor = UIColor(hexString: ColorConstants.RED)
        view1.backgroundColor = UIColor.white
        view2.backgroundColor = UIColor.white
        
        
        label1.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
        label2.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
        mainLabel.textColor = UIColor.white
    }
    
    func toggleImage(mainImage:UIImageView,image1:UIImageView,image2:UIImageView,mainImageName:String,imageName1:String,imageName2:String){
        mainImage.image = UIImage(named: mainImageName)
        mainImage.image = mainImage.image?.withRenderingMode(.alwaysTemplate)
        mainImage.tintColor = UIColor.white
        image1.image = UIImage(named: imageName1)
        image1.image = image1.image?.withRenderingMode(.alwaysTemplate)
        image1.tintColor = UIColor(hexString: ColorConstants.RED)
        image2.image = UIImage(named: imageName2)
        image2.image = image2.image?.withRenderingMode(.alwaysTemplate)
        image2.tintColor = UIColor(hexString: ColorConstants.RED)
    }
    
    
    @IBAction func searchClicked(_ sender: UITapGestureRecognizer) {
        if isSearchOpen == false{
            seperatorView.isHidden = false
            dateView.isHidden = false
            UIView.animate(withDuration: 0.8) {
                self.searchBar.transform = CGAffineTransform.identity
                // self.tableViewTopSpacer.constant = 50
                self.searchViewHeightConstraint = self.searchViewHeightConstraint.setMultiplier(multiplier: 0.12)
            }
        }else{
            dateView.isHidden = true
            seperatorView.isHidden = true
            UIView.animate(withDuration: 0.8) {
                //                self.searchView.transform = CGAffineTransform(scaleX: 1, y: 1)
                // self.tableViewTopSpacer.constant = 50
                self.searchViewHeightConstraint = self.searchViewHeightConstraint.setMultiplier(multiplier: 0.07)
            }
        }
        isSearchOpen = !isSearchOpen
        
        
        if searchTF.text != "" || fromDateTF.text != "" || toDateTF.text != "" {
            self.fleetInvitedArray.removeAll()
            pageNo = 0
            fleetInvitedList(search: self.searchTF.text!)
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fleetInvitedArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if cellTapped{
            if indexPath as IndexPath == selectedIndex {
                return 420
                //Size you want to increase to
            }
        }
        return 120
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath
        let selectedRowIndex = indexPath
        currentRow = selectedRowIndex.row
        cellTapped = !cellTapped
        self.tableView.reloadDataWithAutoSizingCellWorkAround()
    }
    
    
    var isAccept = Bool()
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "fleetInvited") as! FleetInvitedTableViewCell
        cell.view2.isHidden = true
        cell.view3.isHidden = true
        cell.view4.isHidden = true
        
        if fleetInvitedArray.count != 0 {
            
            let fleetInvited = self.fleetInvitedArray[indexPath.row]
            
            cell.id.text = fleetInvited.availableLoadBid.bidCode
            cell.vehicleType.text = fleetInvited.availableLoadBid.vehicleType.name
            cell.from.text = fleetInvited.availableLoadBid.availableLocation
            cell.to.text = fleetInvited.availableLoadBid.destinationLocation
            cell.loadType.text = fleetInvited.availableLoadBid.loadType.loadType
            cell.cargoType.text = fleetInvited.availableLoadBid.cargoType.cargoType
            cell.noOfVehicles.text = "\(fleetInvited.availableLoadBid.noOfVehicles)"
            let cargoWtString = "\(fleetInvited.availableLoadBid.capacityWeight)" + " \(String(describing: fleetInvited.availableLoadBid.capacityWeightUnit.name!.uppercased()))"
            cell.cargoWt.text = cargoWtString
            cell.contactPerson.text = fleetInvited.loadOwner.name
            cell.mobileNo.text = fleetInvited.loadOwner.telephone
            cell.emailId.text = "NA"
            
            print("inviteStatus ===== \(fleetInvited.availableLoadBid.status)")
            
          
            
            if isRejectedClicked{
                cell.rejectView.isHidden = true
                cell.acceptLabel.text = "Reopen"
                isAccept = false
            }else{
                 cell.rejectView.isHidden = false
                cell.acceptLabel.text = "Accept"
                  isAccept = true
            }
            let acceptView = UITapGestureRecognizer(target: self, action: #selector(acceptView(recognizer:)))
            cell.acceptView.addGestureRecognizer(acceptView)
            cell.acceptView.tag = indexPath.row
            
            
            let reject = UITapGestureRecognizer(target: self, action: #selector(rejectView(recognizer:)))
            cell.rejectView.addGestureRecognizer(reject)
            cell.rejectView.tag = indexPath.row
            
            
            let call = UITapGestureRecognizer(target: self, action: #selector(callView(recognizer:)))
            cell.callView.addGestureRecognizer(call)
            cell.callView.tag = indexPath.row
            
            let call1 = UITapGestureRecognizer(target: self, action: #selector(callVieww(recognizer:)))
            cell.callViewAccept.addGestureRecognizer(call)
            cell.callViewAccept.tag = indexPath.row
            
            
            
            let timestap: Double = Double(fleetInvited.availableLoadBid.availableDateTime / 1000)
            let availableFromtime = self.epochTime(epochTimee: timestap)
            cell.availableFromTime.text = availableFromtime
            
            //let fromTimeType = self.epochTimeType(epochtimeType: timestap)
            let availableFromdate = self.epochDate(epochDate: timestap)
            cell.availableFromDate.text = availableFromdate
            
            
            
            if indexPath.row == currentRow {
                if cellTapped == false {
                    if fleetInvited.availableLoadBid.status == "ACCEPTED" {
                        cell.acceptInstructionView.isHidden = false
                        cell.callViewAccept.isHidden = true
                        cell.acceptInstructionLabel.isHidden = false
                    }else{
                        cell.acceptInstructionView.isHidden = true
                        cell.callViewAccept.isHidden = true
                         cell.acceptInstructionLabel.isHidden = true
                    }
                  
                    
                    cell.view2.isHidden = true
                    cell.view3.isHidden = true
                    cell.view4.isHidden = true
                    self.tableView.rowHeight = 120
                }else{
                    if fleetInvited.availableLoadBid.status == "ACCEPTED" {
                        cell.acceptInstructionView.isHidden = false
                        cell.callViewAccept.isHidden = false
                        cell.acceptInstructionLabel.isHidden = false
                    }else{
                        cell.acceptInstructionView.isHidden = true
                          cell.acceptInstructionLabel.isHidden = true
                        cell.callViewAccept.isHidden = true
                    }
                    
                    cell.view2.isHidden = false
                    cell.view3.isHidden = false
                    cell.view4.isHidden = false
                    self.tableView.rowHeight = 420
                    
                }
            }
            
            
        }
        
        
       
        return cell
    }
    
    @objc func acceptView(recognizer: UITapGestureRecognizer){
        
        let tag = recognizer.view?.tag
        if self.fleetInvitedArray[tag!].inviteStatus == "REJECTED" {
            let id = (self.fleetInvitedArray[tag!].availableFleetBidInvite?.id!)!
            let rejectId = self.fleetInvitedArray[tag!].id
            self.fleetInvitedArray.remove(at: tag!)
             self.tableView.reloadData()
            reopen(id: id, rejectId: rejectId)
            
        }else{
            let id = self.fleetInvitedArray[tag!].id
            self.fleetInvitedArray.remove(at: tag!)
             self.tableView.reloadData()
            inviteStatus(id: id, type: "ACCEPTED")
        }
      
        
    }
    
    @objc func rejectView(recognizer: UITapGestureRecognizer){
        
        let tag = recognizer.view?.tag
        let id = self.fleetInvitedArray[tag!].id
        self.fleetInvitedArray.remove(at: tag!)
        self.tableView.reloadData()
        inviteStatus(id: id, type: "REJECTED")
        
    }
    
    func call(tag:Int){
        let tag = tag
        let phoneNo = self.fleetInvitedArray[tag].loadOwner.telephone
      self.callNumber(phoneNumber: phoneNo)
    }
    
    @objc func callView(recognizer: UITapGestureRecognizer){
          let tag = recognizer.view?.tag
       call(tag: tag!)
        
    }
    
    @objc func callVieww(recognizer: UITapGestureRecognizer){
       let tag = recognizer.view?.tag
        call(tag: tag!)
    }
    
    
    func inviteStatus(id:Int,type:String){

     let headers = header()
        Alamofire.request(URLStatics.inviteStatus + "\(id)" + "/" + "\(type)", method: .post, encoding: JSONEncoding.default, headers: headers)
                .responseJSON { response in
                    print("response.request=\(String(describing: response.request))")  // original URL request
                    print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                    print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                   
                    switch (response.result) {
                    case .success:
                      
                        if let reply = response.result.value {
                          
                            
                            let mainResponse = JSON(reply)
                            print("mainResponse ===== \(mainResponse)")
                            if mainResponse["success"].stringValue != "" {
                                
                                
                                let alertController = UIAlertController(title: "", message: "\(mainResponse["success"])", preferredStyle: .alert)

                                // Create the actions
                                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                    UIAlertAction in
                                    self.sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "fleetsPostedVC") }, with: "third")
                                    self.sideMenuController?.setContentViewController(with: "third")
                                }


                                // Add the actions
                                alertController.addAction(okAction)

                                // Present the controller
                                self.present(alertController, animated: true, completion: nil)
                                
                                
                                
                            }
                        }
                        break
                    case .failure(let error):
                        self.activityIndicatorEnd()
                        print("== error == \(error.localizedDescription)")
                        break
                    }
            }
            
            
        }
        
        

    func reopen(id:Int,rejectId:Int) {
        
  let headers = header()
        let str = "rejectID="+"\(rejectId)"+"&inviteId="+"\(id)"+"&financialYear="+"\(self.currentFinancialYear!)"
        
        Alamofire.request(URLStatics.rejectToAvailableInvite + str, method: .post, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                
                switch (response.result) {
                case .success:
                    var returnDict = [String:Any]()
                    if let reply = response.result.value {

                        let mainResponse = JSON(reply)
                        print("mainResponse ===== \(mainResponse)")
                        if mainResponse["success"].stringValue != "" {
              
                            let alertController = UIAlertController(title: "", message: "\(mainResponse["success"])", preferredStyle: .alert)

                            // Create the actions
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                                
                                self.sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "fleetsPostedVC") }, with: "third")
                                self.sideMenuController?.setContentViewController(with: "third")
                            }


                            // Add the actions
                            alertController.addAction(okAction)

                            // Present the controller
                            self.present(alertController, animated: true, completion: nil)

                            
                            
                        }
                    }
                    break
                case .failure(let error):
                    self.activityIndicatorEnd()
                    print("== error == \(error.localizedDescription)")
                    break
                }
        }
        
        
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
