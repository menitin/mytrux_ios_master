//
//  BidListViewController.swift
//  Mytrux
//
//  Created by Aboli on 05/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class BidListViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "bidListCell") as! BidListTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    @IBOutlet weak var bidListTableView: UITableView!
    override func viewDidLoad() {
         super.viewDidLoad()
      
        
       
        bidListTableView.delegate = self
        bidListTableView.dataSource = self
        
        // Do any additional setup after loading the view.
    }
   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
