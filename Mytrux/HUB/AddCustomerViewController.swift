//
//  AddCustomerViewController.swift
//  Mytrux
//
//  Created by Mytrux on 18/11/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SideMenuSwift

class AddCustomerViewController: BaseViewController,UITextFieldDelegate {
    
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchResultView: UIView!
    @IBOutlet weak var searchBar: UIView!
    @IBOutlet weak var addToFavBtn: UIView!
    @IBOutlet weak var searchBtn: UIView!
    @IBOutlet weak var searchResult: UILabel!
    @IBOutlet weak var noteLAbel: UILabel!
    @IBOutlet weak var inviteBtn: UIView!
    @IBOutlet weak var searchResultBar: UIView!
    var isSearchClicked = false
    var transporterId = String()
    
    @IBOutlet weak var searchViewTopConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.clearCache(with: "37")
        topBarCard(topBar: topBar)
        cardViewArray(arrView: [searchBar,searchResultBar])
        buttonCardArray(arrView: [addToFavBtn,inviteBtn])
        self.searchResultView.isHidden = true
        self.searchViewTopConstraint.constant = 150
        searchTF.delegate = self
        searchTF.keyboardType = .emailAddress
        self.inviteBtn.isUserInteractionEnabled = true
        // Do any additional setup after loading the view.
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == searchTF{
            if searchTF.text!.isValidEmail() == false{
                let alert = UIAlertController(title: "", message: "Invalid Email", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    
    
    @IBAction func searchClicked(_ sender: Any) {
   
        if searchTF.text != "" {
            searchCustomer()
        }
        
    }
    
    
    @IBAction func addToFavClicked(_ sender: Any) {
        addToFavCustomer()
    }
    
    
    
    
    @IBAction func inviteClicked(_ sender: Any) {
        inviteCustomer()
    }
    
    
    func searchCustomer(){
        
        
        let headers = header()
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
        
        Alamofire.request(URLStatics.searchCustomer + "\(String(describing: self.searchTF.text!))" , method: .get, headers: headers)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                
                switch (response.result) {
                    
                case .success:
                    
                    if let reply = response.result.value {
                        print("======= JSON: searchCustomer ========  \(reply)")
                        
                        
                        let mainResponse = JSON(reply)
                        
                        var mainResponse1 = [[String:Any]]()
                        if let maiin = mainResponse.arrayObject as? [[String : Any]] {
                            mainResponse1 = maiin
                        }
                        if mainResponse1.count == 0 {
                            self.noteLAbel.text = "Above email ID/Customer is not Registered with Mytrux platform. Send link for Registration by sending invite."
                                                       self.searchResult.text = "\(self.searchTF.text!)"
                                                       self.inviteBtn.isHidden = false
                                                      UIView.animate(withDuration: 0.8) {
                                                          self.searchViewTopConstraint.constant = 60
                                                          self.searchResultView.isHidden = false
                                                      }
                            self.addToFavBtn.isHidden = true
                        }else{
                            
                            if mainResponse1[0]["name"] != nil{
                                                     self.searchResult.text = "\(String(describing: mainResponse1[0]["name"]!))"
                                                     self.noteLAbel.text = "Avove email ID/Customer is Registered with Mytrux platform. This user email ID belongs to above company"
                                                     self.inviteBtn.isHidden = true
                                                     UIView.animate(withDuration: 0.8) {
                                                                   self.searchViewTopConstraint.constant = 60
                                                                   self.searchResultView.isHidden = false
                                                               }
                                self.addToFavBtn.isHidden = false
                                                     self.transporterId = "\(mainResponse1[0]["id"]!)"
                                                     
                                                 }
                                                 
                        }
                     
                        
                        // var datumArray = [FleetDatum]()
                        
                    }
                    break
                case .failure(let error):
                    
                    print("error \(error.localizedDescription)")
                    break
                }
        }
    }
    
    
    func inviteCustomer(){
        let parameters : [String:Any] = [
            "financialYear":self.currentFinancialYear!
        ]
        let headers = header()
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
        
        Alamofire.request(URLStatics.inviteCustomer + "\(String(describing: self.searchTF.text!))" , method: .post, parameters:parameters,
                          headers: headers)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                
                switch (response.result) {
                    
                case .success:
                    
                    if let reply = response.result.value {
                        print("======= JSON: inviteCustomer ========  \(reply)")
                        
                        let mainResponse = JSON(reply)
                       let alert = UIAlertController(title: "", message: "\(mainResponse["success"].stringValue)", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        self.searchTF.text = ""
                        UIView.animate(withDuration: 0.8) {
                                       self.searchViewTopConstraint.constant = 150
                                       self.searchResultView.isHidden = true
                                   }
                        
                    }
                    break
                case .failure(let error):
                    
                    print("error \(error.localizedDescription)")
                    break
                }
        }
    }
    
    
    
    
    func addToFavCustomer(){
        let parameters : [String:Any] = [
            "transporterID": self.transporterId,
            "financialYear":self.currentFinancialYear!
        ]
        let headers = header()
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
        
        Alamofire.request(URLStatics.addToFavCustomer + "\(String(describing: self.transporterId))" + "/status" , method: .post, parameters:parameters,
                          headers: headers)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                
                switch (response.result) {
                    
                case .success:
                   // https://www.mytrux.com/mytrux/company/hubFavourite/2825/status?financialYear=2019-2020
                    if let reply = response.result.value {
                        print("======= JSON: searchCustomer ========  \(reply)")
                        let mainResponse = JSON(reply)
                       
                        // var datumArray = [FleetDatum]()
                        print("mainResponse warning==== \(mainResponse["warning"].stringValue)")
                        let alert = UIAlertController(title: "", message: "\(mainResponse["warning"].stringValue)", preferredStyle: UIAlertController.Style.alert)
                                       alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                       self.present(alert, animated: true, completion: nil)
                        
                    }
                    break
                case .failure(let error):
                    
                    print("error \(error.localizedDescription)")
                    break
                }
        }
    }
    
    
    
    
    @IBAction func backClicked(_ sender: Any) {
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "CustomerViewController") }, with: "35")
        sideMenuController?.setContentViewController(with: "35")
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
