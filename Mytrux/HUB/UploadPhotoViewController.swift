
//
//  UploadPhotoViewController.swift
//  Mytrux
//
//  Created by Mytrux on 19/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SideMenuSwift

class UploadPhotoViewController: BaseViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    var jobID = String()
    var tripID = String()
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var photoView: UIView!
    @IBOutlet weak var uploadPhotoButton: UIView!
    @IBOutlet weak var cameraWrapperView: UIView!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var popUpView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        cardViewArray(arrView: [photoView,commentView])
        topBarCard(topBar: topBar)
        buttonCardView(button: uploadPhotoButton)
        cameraWrapperView.isHidden = true
        popUpView.isHidden = true
            NotificationCenter.default.addObserver(self, selector: #selector(self.uploadSignature(notification:)), name: .uploadSignature, object: nil)
        // Do any additional setup after loading the view.
    }
    var imageStr = String()
    @IBAction func photoViewClicked(_ sender: Any) {
        UIView.animate(withDuration: 0.8) {
            self.popUpView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            self.cameraWrapperView.isHidden = false
            self.popUpView.isHidden = false
        }
    }
    @IBAction func uploadPhotoClicked(_ sender: Any) {
        uploadSign(jobId: self.jobID, tripId: self.tripID, imageStr: imageStr)
    }
    
    @IBAction func cameraViewClicked(_ sender: Any) {
        openCamera()
    }
    
    @IBAction func galleryView(_ sender: Any) {
        openGallery()
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
  
    
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        self.popUpView.isHidden = true
        self.cameraWrapperView.isHidden = true
    }
    @objc func uploadSignature(notification: Notification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        let jobId = userInfo!["jobId"] as! String
        let tripId = userInfo!["tripId"] as! String
        self.jobID = jobId
        self.tripID = tripId
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        photoImageView.image = chosenImage
        self.imageStr = chosenImage.toBase64()!
        print("imageStr +++++++++ \(imageStr)")
        self.popUpView.isHidden = true
        picker.dismiss(animated:  true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.popUpView.isHidden = true
        picker.dismiss(animated:  true, completion: nil)
    }
    
    
    
    @IBAction func backClicked(_ sender: Any) {
     sideMenuController?.setContentViewController(with: "19")
    }
    
    
    func uploadSign(jobId:String,tripId:String,imageStr:String){
        
        self.activityIndicatorBegin()
        
        var parameters = [String:Any]()
        parameters = [
            "jobId":jobId,
            "podString" : "gfggghghg",
            "podUploadMode" : "ONLINE"
        ]
        
        print("para ===== \(parameters)")
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
        let header = self.header()
        
        Alamofire.request(URLStatics.uploadPod + "\(jobId)" + "/" + "\(tripId)?" + "&"+"jobId="+jobId+"&"+"podString="+"sdsfd"+"&"+"podUploadMode="+"ONLINE", method: .put, encoding: JSONEncoding.default, headers: header)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                switch (response.result) {
                case .success:
                    var returnDict = [String:Any]()
                    if let reply = response.result.value {
                        print("JSON: uploadPod ======== \(reply)")
                        let mainResponse = JSON(reply)
                        
                        if mainResponse["warning"].stringValue != "" {
                            let alertController = UIAlertController(title: "\(mainResponse["warning"].stringValue)", message:"", preferredStyle: .alert)
                            
                            // Create the actions
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                                
                            }
                            alertController.addAction(okAction)
                            
                            // Present the controller
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                        self.activityIndicatorEnd()
                        
                        
                    }
                    
                    break
                case .failure(let error):
                    
                    print("error \(error.localizedDescription)")
                    break
                }
        }
    }

    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
