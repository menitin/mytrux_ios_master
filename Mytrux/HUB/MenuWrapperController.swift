//
//  MenuWrapperController.swift
//  Mytrux
//
//  Created by Aboli on 05/06/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import SideMenuSwift

class MenuWrapperController: UIViewController {
  var loginUserType = String()
   
    @IBOutlet weak var containerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
      
        NotificationCenter.default.addObserver(self, selector: #selector(self.LoadsQuotedClicked(notification:)), name: .LoadsQuotedClicked, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.fleetsPostedClicked(notification:)), name: .fleetsPostedClicked, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.exchangeClicked(notification:)), name: .exchangeClicked, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.postFleetClicked(notification:)), name: .postFleetClicked, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.walletClicked(notification:)), name: .walletClicked, object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.bookingClicked(notification:)), name: .bookingClicked, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.legalClicked(notification:)), name: .legalClicked, object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.financialYear(notification:)), name: .financialYear, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.spNotificationClicked(notification:)), name: .spNotificationClicked, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.suNotificationClicked(notification:)), name: .suNotificationClicked, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.spCustomerViewClicked(notification:)), name: .spCustomerViewClicked, object: nil)
 loginUserType = UserDefaults.standard.value(forKey: UserDefaultsContants.LOGIN_USER_TYPE)! as! String
     
        // Do any additional setup after loading the view.
    }
    
    @objc func spCustomerViewClicked(notification: Notification) {
          self.sideMenuController?.hideMenu()
          sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "CustomerViewController") }, with: "35")
          sideMenuController?.setContentViewController(with: "35")
      }
    @objc func suNotificationClicked(notification: Notification) {
        self.sideMenuController?.hideMenu()
        sideMenuController?.setContentViewController(with: "30")
    }
    @objc func spNotificationClicked(notification: Notification) {
        self.sideMenuController?.hideMenu()
        sideMenuController?.setContentViewController(with: "30")
    }
    
    @objc func LoadsQuotedClicked(notification: Notification) {
         self.sideMenuController?.hideMenu()
        
         sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "loadsQuotedVC") }, with: "second")
        sideMenuController?.setContentViewController(with: "second")
    }
    
    @objc func fleetsPostedClicked(notification: Notification) {
        self.sideMenuController?.hideMenu()
        
           sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "fleetsPostedVC") }, with: "third")
        sideMenuController?.setContentViewController(with: "third")
    }
    
    @objc func postFleetClicked(notification: Notification) {
        self.sideMenuController?.hideMenu()
        if loginUserType == UserStates.SERVICE_PROVIDER{
             sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "PostFleetVC") }, with: "fourth")
             sideMenuController?.setContentViewController(with: "fourth")
//            NotificationCenter.default.post(name: Notification.Name("refreshViewPostFleet"), object: nil)
        }else{
             sideMenuController?.setContentViewController(with: "ninth")
//            NotificationCenter.default.post(name: Notification.Name("refreshViewPostFleet"), object: nil)
        }
    }
    @objc func exchangeClicked(notification: Notification) {
        self.sideMenuController?.hideMenu()
         SideMenuController.preferences.basic.defaultCacheKey = "default"
        sideMenuController?.setContentViewController(with: "default")
        
         NotificationCenter.default.post(name: Notification.Name("refreshView"), object: nil)
    }
    @objc func walletClicked(notification: Notification) {
        self.sideMenuController?.hideMenu()
         sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "WalletViewController") }, with: "fifth")
        sideMenuController?.setContentViewController(with: "fifth")
    }
    @objc func bookingClicked(notification: Notification) {
        self.sideMenuController?.hideMenu()
        sideMenuController?.setContentViewController(with: "sixth")
    }
    @objc func legalClicked(notification: Notification) {
        self.sideMenuController?.hideMenu()
        sideMenuController?.setContentViewController(with: "eighth")
    }
    @objc func financialYear(notification: Notification) {
        self.sideMenuController?.hideMenu()
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let presentedViewController = storyBoard.instantiateViewController(withIdentifier: "FinancialYear") as! FinancialYearViewController
                presentedViewController.providesPresentationContextTransitionStyle = true
                presentedViewController.definesPresentationContext = true
                presentedViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
                presentedViewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
                self.present(presentedViewController, animated: true, completion: nil)
        
    }
    
   @IBAction func backClikced(_ sender: Any) {
         self.sideMenuController?.hideMenu()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
