//
//  BidViewController.swift
//  Mytrux
//
//  Created by Aboli on 12/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import SideMenuSwift

class BidViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    var cellTapped:Bool = false
    var currentRow = 0;
    var checked = Set<IndexPath>()
    var selectedIndex = IndexPath()
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchBar: UIView!
    @IBOutlet weak var seperatorLine: UIImageView!
    @IBOutlet weak var searchFromToView: UIView!
    @IBOutlet weak var searchViewHeightConstraint: NSLayoutConstraint!
    var isSearchOpen = false
    override func viewDidLoad() {
        super.viewDidLoad()
      topBarCard(topBar: topBar)
      cardViewArray(arrView: [searchView])
        searchView.layer.cornerRadius = 8
        tableView.delegate = self
        tableView.dataSource = self
        searchFromToView.isHidden = true
        seperatorLine.isHidden = true
        self.searchViewHeightConstraint = self.searchViewHeightConstraint.setMultiplier(multiplier: 0.07)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backBtn(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    
    @IBAction func searchClicked(_ sender: UITapGestureRecognizer) {
        if isSearchOpen == false{
            seperatorLine.isHidden = false
            searchFromToView.isHidden = false
            UIView.animate(withDuration: 0.8) {
                self.searchView.transform = CGAffineTransform.identity
                // self.tableViewTopSpacer.constant = 50
                self.searchViewHeightConstraint = self.searchViewHeightConstraint.setMultiplier(multiplier: 0.12)
            }
        }else{
            searchFromToView.isHidden = true
            seperatorLine.isHidden = true
            UIView.animate(withDuration: 0.8) {
                //                self.searchView.transform = CGAffineTransform(scaleX: 1, y: 1)
                // self.tableViewTopSpacer.constant = 50
                self.searchViewHeightConstraint = self.searchViewHeightConstraint.setMultiplier(multiplier: 0.07)
            }
        }
        isSearchOpen = !isSearchOpen
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "bid") as! BidTableViewCell
        cell.view2.isHidden = true
        cell.view3.isHidden = true
        cell.view4.isHidden = true
        if indexPath.row == currentRow {
            if cellTapped == false {
                cell.view2.isHidden = true
                cell.view3.isHidden = true
                cell.view4.isHidden = true
                self.tableView.rowHeight = 120
            }else{
                cell.view2.isHidden = false
                cell.view3.isHidden = false
                cell.view4.isHidden = false
                self.tableView.rowHeight = 410
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if cellTapped{
            if indexPath as IndexPath == selectedIndex {
                return 410
                //Size you want to increase to
            }
        }
        return 120
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath
        let selectedRowIndex = indexPath
        currentRow = selectedRowIndex.row
        cellTapped = !cellTapped
        self.tableView.reloadDataWithAutoSizingCellWorkAround()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
