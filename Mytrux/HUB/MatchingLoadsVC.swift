//
//  MatchingLoadsVC.swift
//  Mytrux
//
//  Created by Aboli on 10/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import SideMenuSwift
import Alamofire
import SwiftyJSON


class MatchingLoadsVC: BaseViewController,UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    
    
    @IBOutlet weak var fleetPOstedCountView: UIView!
    @IBOutlet weak var fleetPOstedCountLabel: UILabel!
    @IBOutlet weak var matchingLoadCountLabel: UILabel!
    @IBOutlet weak var matchingLoadCountView: UIView!
    @IBOutlet weak var matchingLoadCV: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var refreshImageView: UIImageView!
    @IBOutlet weak var bookNowBtn: UIView!
    @IBOutlet weak var bookNowImage: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.clearCache(with: "22")
        tableView.delegate = self
        tableView.dataSource = self
        matchingLoadCV.delegate = self
        matchingLoadCV.dataSource = self
        circularView(viewArray: [fleetPOstedCountView,matchingLoadCountView])
        self.fleetsPosted()
        self.currentRow = -1
        loadDetails()
         self.bookNowImage.image = UIImage(named: "bookNow")
               bookNowImage.image = bookNowImage.image?.withRenderingMode(.alwaysTemplate)
               bookNowImage.tintColor = UIColor(hexString: ColorConstants.DARK_BLUE)
        // Do any additional setup after loading the view.
    }
    
    var loadArray = [Load]()
    var loadDatumArray = [Datum]()
    
    @IBAction func bookNowClicked(_ sender: Any) {
         sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookNowViewController") }, with: "34")
        sideMenuController?.setContentViewController(with: "34")
    }
    @IBAction func refreshView(_ sender: Any) {
        cargoTypeId = ""
        loadTypeId = ""
         vehicleTypeId = ""
         latitude = ""
        longitude = ""
        fleetId = ""
        self.fleetIdd = ""
        self.datumArray.removeAll()
        self.loadDatumArray.removeAll()
        self.loadArray.removeAll()
        
       self.tableView.reloadData()
        self.matchingLoadCV.reloadData()
        self.currentRow = -1
        loadDetails()
         self.fleetsPosted()
    }
    
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        //        print("offsetY = \(offsetY) ======= contentHeight ==== \(contentHeight - scrollView.frame.size.height - 300)")
        
        if scrollView == tableView{
            if offsetY > contentHeight - scrollView.frame.size.height - 300 {
                
                pageNo += 1
                
                isDragging = true
                self.fleetsPosted()
                
            }else{
                isDragging = false
            }
        }
        
        if scrollView == matchingLoadCV{
            
            if self.loadDatumArray[0].count != self.loadDatumArray.count {
                pageNo1 += 1
                
                isDragging = true
                self.loadDetails()
            }
            
        }
        
    }
    
    
    @IBAction func backClicked(_ sender: Any) {
        SideMenuController.preferences.basic.defaultCacheKey = "default"
        sideMenuController?.setContentViewController(with: "default")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datumArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "matchingLoadCell") as! MatchingLoadCell
        if datumArray.count != 0{
            let fleet = datumArray[indexPath.row]
            cell.fleetId.text = fleet.bidCode
            cell.fleetVehicleType.text = fleet.vehicleType.name
            cell.fleetFrom.text = fleet.availableLocation
            cell.fleetTo.text = fleet.destinationLocation
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(fleetDetailView(recognizer:)))
            cell.fleetDetailView.addGestureRecognizer(tap)
            cell.fleetDetailView.tag = indexPath.row
            
            if self.currentRow == indexPath.row{
                cell.selectedFleetImage.image = UIImage(named: "selectedCircleRed")
            }else{
                cell.selectedFleetImage.image = UIImage(named: "unselectedCircleRed")
            }
        }

        return cell
        //matchingLoadCell
    }
    var isSelected = false
    var selectedIndex = IndexPath()
    var currentRow = Int()
    var cellTapped:Bool = false
  var fleetId = String()
    
        @objc func fleetDetailView(recognizer: UITapGestureRecognizer){
              let tag = recognizer.view?.tag
            let fleetDetail = self.datumArray[tag!]
            
          
            sideMenuController?.setContentViewController(with: "26")
            NotificationCenter.default.post(name: Notification.Name("fleetDetail"),
                                            object:nil,userInfo: ["fleetDetails":fleetDetail])
        }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.loadDatumArray.count
    }
    
    var cargoTypeId = String()
    var loadTypeId = String()
    var vehicleTypeId = String()
    var latitude = String()
    var longitude = String()
    var fleetIdd = String()
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("indexPath.row ==== \(indexPath.row)")
        self.selectedIndex = indexPath
        currentRow = selectedIndex.row
        cellTapped = !cellTapped
        let load = self.datumArray[indexPath.row]
        cargoTypeId = String(load.cargoType.id)
        loadTypeId = String(load.loadType.id)
        vehicleTypeId = String(load.vehicleType.id)
        fleetIdd = load.bidCode
        latitude = String(load.availableLatitude!)
        longitude = String(load.availableLongitude!)
        self.loadDatumArray.removeAll()
        self.loadArray.removeAll()
        self.matchingLoadCV.reloadData()
        self.loadDetails()
        self.tableView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //matchingCollection
        let cell = matchingLoadCV.dequeueReusableCell(withReuseIdentifier: "matchingCollection", for: indexPath) as! MatchingLoadCollectionViewCell
        if self.loadDatumArray.count != 0 {
            
            let loadDetail = loadDatumArray[indexPath.row]
            cell.loadId.text = loadDetail.bidCode
            cell.loadVehicleType.text = loadDetail.vehicleType.name
            cell.loadTo.text = loadDetail.destinationLocation
            cell.loadFrom.text = loadDetail.availableLocation
            cell.quotedView.roundCorners(corners: [.bottomRight,.topRight], radius: 5)
            if loadDetail.availableLoadBidQuotaionElement?.id != nil{
                cell.quotedView.isHidden = false
                cell.fleetId.text = loadDetail.availableLoadBidQuotaionElement?.availableFleetBid.bidCode
                
            }else{
                cell.quotedView.isHidden = true
            }
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(loadViewClicked(recognizer:)))
            cell.mainView.addGestureRecognizer(tap)
            cell.mainView.tag = indexPath.row
        }
        return cell
    }
    
    @objc func loadViewClicked(recognizer: UITapGestureRecognizer){
        
        if fleetIdd == ""{
            let alert = UIAlertController(title:"", message: "Please Select Fleet First", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

            self.present(alert, animated: true, completion: nil)
            return
        }
            let tag = recognizer.view?.tag
            let loadDetail = self.loadDatumArray[tag!]
            var fleetDetail : FleetDatum?
        for data in datumArray{
            if self.fleetIdd == data.bidCode{
                fleetDetail = data
            }
        }
        
            
            let timestap: Double = Double(loadDetail.availableDateTime / 1000)
            let availableFromtime = self.epochTime(epochTimee: timestap)
            let availableFromdate = self.epochDate(epochDate: timestap)
            let timestamp: Double = Double(loadDetail.closedBidTime / 1000)
            let availableTilltime = self.epochTime(epochTimee: timestamp)
            let availableTilldate = self.epochDate(epochDate: timestamp)
            let availableLoadQuotations = loadDetail.availableLoadBidQuotaions
            let ownLastThreeBids = loadDetail.ownLastThreeBids
            let availableLoadBidQuotaionElement = loadDetail.availableLoadBidQuotaionElement
            let loadObject = ["id": loadDetail.id,"bidCode": loadDetail.bidCode,
                              "vehicleType": loadDetail.vehicleType.name,
                              "from": loadDetail.availableLocation,
                              "to": loadDetail.destinationLocation,
                              "loadType": loadDetail.loadType.loadType, "noOfVech":loadDetail.noOfVehicles,
                              "cargoType": loadDetail.cargoType.cargoType!,
                              "cargoWt": loadDetail.capacityWeight!,
                              "packgs": loadDetail.packgs,
                              "availableFromdate": availableFromdate,
                              "availableFromtime": availableFromtime,
                              "availableTilltime": availableTilltime,
                              "availableTilldate": availableTilldate,
                              "kgName": loadDetail.capacityWeightUnit.name!,
                              "lowestBid": 0,"closedBidTime":loadDetail.closedBidTime,
                              "bidRank": loadDetail.bidRank ?? 0,
                              "availableLoadQuotations": availableLoadQuotations,
                              "ownLastThreeBids": ownLastThreeBids!,
                              "availableLoadBidQuotaionElement": availableLoadBidQuotaionElement!,"latitude": 0.0,"longitude":0.0,"vehicleTypeId":loadDetail.vehicleType.id,"loadTypeId":loadDetail.loadType.id,"status":"MatchLoad","fleet":fleetDetail] as [String : Any]
            //
            //        print("fleetIdd === \(self.fleetIdd)")
            //
            //        print("loadId ==== \(String(describing: loadDetail.availableLoadBidQuotaionElement?.availableFleetBid.bidCode!))")
            if loadDetail.availableLoadBidQuotaionElement?.id != nil {
                
            
            if self.fleetIdd != loadDetail.availableLoadBidQuotaionElement?.availableFleetBid.bidCode{
                let alert = UIAlertController(title:"Note!", message: "Sorry you have already Quoted for the Same Load with Fleet id \(String(describing: loadDetail.availableLoadBidQuotaionElement!.availableFleetBid.bidCode!))", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }else{
                sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "LoadDetailViewController") }, with: "seventh")
                sideMenuController?.setContentViewController(with: "seventh")
                NotificationCenter.default.post(name: Notification.Name("loadObj"),
                                                object:nil,userInfo: loadObject)
                }
            }else{
                sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "LoadDetailViewController") }, with: "seventh")
                sideMenuController?.setContentViewController(with: "seventh")
                NotificationCenter.default.post(name: Notification.Name("loadObj"),
                                                object:nil,userInfo: loadObject)
            }
            
        
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        _ = 80 * collectionView.numberOfItems(inSection: 0)
        _ = 10 * (collectionView.numberOfItems(inSection: 0) - 1)
        
        let leftInset = (collectionView.layer.frame.size.width / 10)
        let rightInset = leftInset
        
        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
        
        
        //        let cellWidth : CGFloat = collectionView.layer.frame.size.width
        //
        //        let numberOfCells = floor(self.view.frame.size.width / cellWidth)
        //        let edgeInsets = (self.view.frame.size.width - (numberOfCells * cellWidth)) / (numberOfCells + 1)
        //
        //        return UIEdgeInsets(top: 0, left: edgeInsets, bottom: 0, right: edgeInsets)
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.matchingLoadCV.scrollToNearestVisibleCollectionViewCell()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            self.matchingLoadCV.scrollToNearestVisibleCollectionViewCell()
        }
    }
    
    
    var isDragging = false
    var status = String()
    var pageNo = 0
    var pageNo1 = 0
    var getBidListPara = [String:Any]()
    var datumArray = [FleetDatum]()
    
    
    func loadDetails() {
        if longitude == "0.0" || latitude == "0.0" {
            longitude = ""
            latitude = ""
        }
        let indexx = userDefaults.value(forKey: "index") as! Int
        print("indexxx ===== \(indexx)")
        //self.activityIndicatorBegin()
        
        
        let bidlist = GetBidList(pageSize:"20",pageNo:"\(self.pageNo1)",recordType:"",search:"",fromCity:"",fromState:"",toState:"",toCity:"",vehicleType:"\(vehicleTypeId)",loadType:"\(loadTypeId)",noOfVehicle:"",fromDate:"",toDate:"",currentLatitude:"\(latitude)",currentLongitude:"\(longitude)",km: String(indexx),financialYear: self.currentFinancialYear!,cargoType:"\(cargoTypeId)")
        
        
        let jsonDataa = try! JSONEncoder().encode(bidlist)
        let type = String(data: jsonDataa, encoding: .utf8)!
        do {
            self.getBidListPara = try self.convertToDictionary(from: type)
            print("loadDetails listt = \(self.getBidListPara)")
        } catch {
            print(error)
        }
        
        let parameters : [String:Any] = getBidListPara
        
        let headers = header()
        
        //+918669696004
        //asdfgf1234$
        Alamofire.request(URLStatics.findavailableloadforbidlist,method: .get, parameters: parameters, headers: headers).responseJSON { response in
            print("response.request=\(String(describing: response.request))")  // original URL request
            print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
            print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
            //   self.activityIndicatorEnd()
            switch (response.result) {
                
            case .success:
                self.loadDatumArray.removeAll()
                if let reply = response.result.value {
                    print("JSON: >>>>>>>>>>>>>>>>>>>>>>>>>> \(reply)")
                    let mainResponse = JSON(reply)
                    var returnDict = [[String:Any]]()
                    // let returnObj = mainResponse["data"].array
                    if let returnObj = mainResponse["data"].arrayObject as? [[String:Any]] {
                        returnDict = returnObj
                    }
                    var resultNew = [String:Any]()
                    var vehicleType = [String:Any]()
                    var availVehicleType = [String:Any]()
                    var documentType : [String:Any]?
                    var docuType = [String:Any]()
                    let availDocumentType = [String:Any]()
                    var availSmallDocType = [String:Any]()
                    var logoType : [String:Any]?
                    var addressType : [String:Any]?
                    var companyAccessArr = [String:Any]()
                    
                    
                    var cargoTypeArray = [String:Any]()
                    var availCargoTypeArray = [String:Any]()
                    var capacityWeightUnitArr = [String:Any]()
                    var availCapacityWeightUnitArr = [String:Any]()
                    
                    
                    var currencyMasterType = [String:Any]()
                    var availCurrencyType : [String:Any]?
                    
                    
                    var loadTypeArray = [String]()
                    
                    var loadType1 : LoadType1?
                    var vehicleTypee : VehicleType?
                    var availableVehicleTypee : AvailableVehicleType?
                    
                    var datumArray = [Datum]()
                    var loadDetails : Load?
                    var cargoTypee : CargoType?
                    var availCargoTypee : CargoType?
                    var capacityWeight : CapacityWeightUnit?
                    var availCapacityWeight : CapacityWeightUnit?
                    var loadOwnerArr = [String:Any]()
                    var companyAccess : CompanyAccess?
                    var loadOwner : LoadOwner?
                    var address : Address1?
                    var document : Logo?
                    var docs : Logo?
                    var availDocument : AvailableLogo?
                    var availSmallDocument: AvailableLogo?
                    var availableLoadBidQuotaions : AvailableLoadBidQuotaion?
                    var availableLoadBidQuotaionsType = [[String:Any]]()
                    var currencyMaster : CurrencyMaster?
                    var logo : Logo?
                    var availLoadType : AvailLoadType1?
                    
                    var bidArray = [Int]()
                    
                    for data in returnDict{
                        
                        var availableFleetBid : AvailableFleetBid?
                        var availableLoadBidQuotaionElement : AvailableLoadBidQuotaionElement?
                        var availableLoadCurrency: AvailableFleetBidCurrencyMaster?
                        var availableFleetBidCurrencyMaster: AvailableFleetBidCurrencyMaster?
                        var availFleetBidCurrencyType = [String:Any]()
                        var availableLoadBidQuotaionElementType = [String:Any]()
                        var availableLoadBidQuotaionsArray = [AvailableLoadBidQuotaion]()
                        var ownLastThreeBidsArray = [AvailableLoadBidQuotaion]()
                        var availableFleetBidType = [String:Any]()
                        var availableLoadType = [String:Any]()
                        resultNew = data["loadType"] as! [String : Any]
                        cargoTypeArray = data["cargoType"] as! [String : Any]
                        vehicleType = data["vehicleType"] as! [String:Any]
                        var ownLastThreeBidsType = [[String:Any]]()
                        var ownLastThreeBids : AvailableLoadBidQuotaion?
                        
                     
                        let loadType = resultNew["loadType"] as! String
                        let noOfVehicle = data["noOfVehicles"] as! Int
                        
                        print("noOfVehicle =\(noOfVehicle)")
                        print("bidCode =\(String(describing: data["bidCode"]))")
                        print("destinationLocation =\(String(describing: data["destinationLocation"]))")
                        loadTypeArray.append(loadType)
                        if self.loginUserType == UserStates.SERVICE_PROVIDER{
                            
                            if let returnObject =
                                data["availableLoadBidQuotaions"] as? [[String:Any]] {
                                availableLoadBidQuotaionsType = returnObject
                            }
                            if let returnObjectt =
                                data["ownLastThreeBids"] as? [[String:Any]] {
                                ownLastThreeBidsType = returnObjectt
                            }
                            
                            
                            
                            
                            if let availableLoad =
                                data["availableLoadBidQuotaion"] as? [String:Any] {
                                availableLoadBidQuotaionElementType = availableLoad
                            }
                            
                            
                            //                            availableLoadBidQuotaionElementType = data["availableLoadBidQuotaion"] as? [String:Any]
                            
                            availCurrencyType = availableLoadBidQuotaionElementType["currencyMaster"] as? [String:Any]
                            
                            if let fleetBidtype = availableLoadBidQuotaionElementType["availableFleetBid"] as? [String:Any]{
                                availableFleetBidType = fleetBidtype
                            }
                            
                            //                            availableFleetBidType = (availableLoadBidQuotaionElementType["availableFleetBid"] as? [String:Any])!
                            
                            if let loadTypee = availableFleetBidType["loadType"] as? [String:Any] {
                                availableLoadType = loadTypee
                            }
                            
                            if let cargoTy = availableFleetBidType["cargoType"] as? [String:Any] {
                                availCargoTypeArray = cargoTy
                            }
                            
                            if let capacityWeight = availableFleetBidType["capacityWeightUnit"] as? [String:Any] {
                                availCapacityWeightUnitArr = capacityWeight
                            }
                            if let availVechTypee = availableFleetBidType["vehicleType"] as? [String:Any] {
                                availVehicleType = availVechTypee
                            }
                            
                            //  availableLoadType = (availableFleetBidType["loadType"] as? [String:Any])!
                            //   availVehicleType = (availableFleetBidType["vehicleType"] as? [String:Any])!
                            
                            print("availVehicleType == \(availVehicleType)")
                            
                            //  availDocumentType = (availVehicleType["document"] as? [String:Any])!
                            
                            
                            if let smallDocType = availVehicleType["documentSmall"] as? [String:Any] {
                                availSmallDocType = smallDocType
                            }
                            if let fleetBid = availableFleetBidType["currencyMaster"] as? [String:Any] {
                                availFleetBidCurrencyType = fleetBid
                            }
                            
                            
                            //     availSmallDocType = (availVehicleType["documentSmall"] as? [String:Any])!
                            // (availVehicleType["documentSmall"] as? [String:Any])!
                            
                            // availFleetBidCurrencyType = (availableFleetBidType["currencyMaster"] as? [String:Any])!
                            
                            
                            //                            availableLoadBidQuotaionsType =
                            //                                data["availableLoadBidQuotaions"] as! [[String : Any]]
                            
                            
                            
                            resultNew = data["loadType"] as! [String : Any]
                            loadOwnerArr = data["loadOwner"] as! [String:Any]
                            
                            documentType = loadOwnerArr["logo"] as? [String:Any]
                            docuType = vehicleType["document"] as! [String:Any]
                            
                            logoType = loadOwnerArr["logo"] as? [String:Any]
                            companyAccessArr = loadOwnerArr["companyAccess"] as! [String:Any]
                            capacityWeightUnitArr = data["capacityWeightUnit"] as! [String:Any]
                            addressType = loadOwnerArr["address"] as? [String:Any]
                            
                            for availableBid in availableLoadBidQuotaionsType{
                                currencyMasterType = availableBid["currencyMaster"] as! [String : Any]
                                currencyMaster = CurrencyMaster(id: currencyMasterType["id"] as? Int, currencyCode: currencyMasterType["currencyCode"] as? String)
                                availableLoadBidQuotaions = AvailableLoadBidQuotaion(id: availableBid["id"] as! Int,                  quotationAmount:Int(truncating: availableBid["quotationAmount"] as! NSNumber),                         currencyMaster: currencyMaster!,
                                                                                     quotationEntryDate: availableBid["quotationEntryDate"] as! Int)
                                availableLoadBidQuotaionsArray.append(availableLoadBidQuotaions!)
                                
                            }
                            
                            for ownbid in ownLastThreeBidsType{
                                currencyMasterType = ownbid["currencyMaster"] as! [String : Any]
                                currencyMaster = CurrencyMaster(id: currencyMasterType["id"] as? Int, currencyCode: currencyMasterType["currencyCode"] as? String)
                                ownLastThreeBids = AvailableLoadBidQuotaion(id: ownbid["id"] as! Int,                  quotationAmount:Int(truncating: ownbid["quotationAmount"] as! NSNumber),currencyMaster: currencyMaster!, quotationEntryDate: ownbid["quotationEntryDate"] as! Int)
                                ownLastThreeBidsArray.append(ownLastThreeBids!)
                                
                            }
                            for bid in ownLastThreeBidsArray{
                                print("bid.quotationAmount) databidCode  " + "\(bid.quotationAmount) " + "\(String(describing: data["bidCode"]))")
                            }
                            
                            
                            
                            
                            loadType1 = LoadType1(id: resultNew["id"] as! Int, loadType: resultNew["loadType"] as! String, code: resultNew["code"] as! String)
                            
                            capacityWeight = CapacityWeightUnit(id: capacityWeightUnitArr["id"] as? Int, name: capacityWeightUnitArr["name"] as? String, type: capacityWeightUnitArr["type"] as? String)
                            
                            address = Address1(id: addressType?["id"] as? Int, address: addressType?["address"] as? String, landmark: addressType?["landmark"] as? String, city: addressType?["city"] as? String, state: addressType?["state"] as? String, pincode: addressType?["pincode"] as? String, country: addressType?["country"] as? String)
                            
                            logo = Logo(id: logoType?["id"] as? Int, mimeType: logoType?["mimeType"] as? String, type: (logoType?["type"] as? String))
                            companyAccess = CompanyAccess(id:companyAccessArr["id"] as! Int)
                            
                            loadOwner = LoadOwner(id: loadOwnerArr["id"] as! Int,
                                                  name: loadOwnerArr["name"] as! String,
                                                  registrationNo: loadOwnerArr["registrationNo"] as! String,
                                                  companyCode: loadOwnerArr["companyCode"] as! String,
                                                  telephone: loadOwnerArr["telephone"] as! String,
                                                  fax:loadOwnerArr["fax"] as! String,
                                                  address: address!,
                                                  logo: logo!,
                                                  gstTax: loadOwnerArr["gstTax"] as! Double,
                                                  gstTaxRegNo: loadOwnerArr["gstTaxRegNo"] as! String,
                                                  jurisdiction: loadOwnerArr["jurisdiction"] as! String,
                                                  timeZone: loadOwnerArr["timeZone"] as! String,
                                                  panNo: loadOwnerArr["panNo"] as! String,
                                                  vatNo: loadOwnerArr["vatNo"] as! String,
                                                  iecNo: loadOwnerArr["iecNo"] as! String,
                                                  cinNo: loadOwnerArr["cinNo"] as! String,
                                                  cstNo: loadOwnerArr["cstNo"] as! String,
                                                  registrationDate: loadOwnerArr["registrationDate"] as! Int,
                                                  subscriptionStartDate: loadOwnerArr["subscriptionStartDate"] as! Int,
                                                  subscriptionEndDate: loadOwnerArr["subscriptionEndDate"] as! Int,
                                                  currentBalance: loadOwnerArr["currentBalance"] as! Int,
                                                  smsNotification: loadOwnerArr["smsNotification"] as! String,
                                                  companyAccess: companyAccess!,status: loadOwnerArr["status"] as! String,
                                                  expiredType: loadOwnerArr["expiredType"] as! String,
                                                  activeUserCount: loadOwnerArr["activeUserCount"] as! Int,
                                                  maxUser: loadOwnerArr["maxUser"] as! Int,
                                                  maxUserType: loadOwnerArr["maxUserType"] as! String,
                                                  exchangeBookingCount: loadOwnerArr["exchangeBookingCount"] as! Int,
                                                  goCount: loadOwnerArr["goCount"] as! Int,
                                                  proCount: loadOwnerArr["proCount"] as! Int,
                                                  profileMailCount: loadOwnerArr["profileMailCount"] as! Int,
                                                  ewbFlagStr: loadOwnerArr["ewbFlagStr"] as! String,
                                                  planType: loadOwnerArr["planType"] as! String,
                                                  exSuwalletAmount: loadOwnerArr["exSuwalletAmount"] as! Int,
                                                  exSuhubMaxUser: loadOwnerArr["exSuhubMaxUser"] as! Int,
                                                  exSuallowTransaction: loadOwnerArr["exSuallowTransaction"] as! Int,
                                                  exSuremainsTr: loadOwnerArr["exSuremainsTr"] as! Int,
                                                  hubPlanID: loadOwnerArr["hubPlanId"] as! Int,
                                                  exSuhubTransactionCost: loadOwnerArr["exSuhubTransactionCost"] as! Int,
                                                  thirdPartyAuthDateTime: loadOwnerArr["thirdPartyAuthDateTime"] as? Int)
                            
                            cargoTypee = CargoType(id: cargoTypeArray["id"] as? Int,
                                                   cargoType: cargoTypeArray["cargoType"] as? String)
                            
                            document = Logo(id: documentType?["id"] as? Int,mimeType: documentType?["mimeType"] as? String,type: ((documentType?["type"] as? String)))
                            
                            docs = Logo(id: docuType["id"] as? Int, mimeType: docuType["mimeType"] as? String, type: ((docuType["type"] as? String)))
                            
                            
                            vehicleTypee = VehicleType(id: vehicleType["id"] as! Int,
                                                       name: vehicleType["name"] as! String,
                                                       sequence: Int(vehicleType["sequence"] as! Double),
                                                       container: vehicleType["container"] as! Bool,
                                                       type: vehicleType["type"] as! String,
                                                       document: docs!, documentSmall: document!,
                                                       vehicleOrder: vehicleType["vehicleOrder"] as! Int,
                                                       weightCapacity: vehicleType["weightCapacity"] as? Int,
                                                       documentID: vehicleType["documentId"] as? Int,
                                                       smallDocID: vehicleType["smallDocId"] as? Int)
                            currencyMaster = CurrencyMaster(id: currencyMasterType["id"] as? Int, currencyCode: currencyMasterType["currencyCode"] as? String)
                            
                            availDocument = AvailableLogo(id: availDocumentType["id"] as? Int,
                                                          mimeType: availDocumentType["mimeType"] as? String,
                                                          type: availDocumentType["type"] as? String)
                            
                            
                            availSmallDocument = AvailableLogo(id: availSmallDocType["id"] as? Int,mimeType: availSmallDocType["mimeType"] as? String,
                                                               type: availSmallDocType["type"] as? String)
                            print("availSmallDocument === \(String(describing: availSmallDocument?.id))")
                            
                            availLoadType = AvailLoadType1(id: availableLoadType["id"] as? Int, loadType: availableLoadType["loadType"] as? String, code: availableLoadType["code"] as? String)
                            
                            availableVehicleTypee = AvailableVehicleType(id: availVehicleType["id"] as? Int,
                                                                         name: availVehicleType["name"] as? String,
                                                                         sequence: availVehicleType["sequence"] as? Double,
                                                                         container: availVehicleType["container"] as? Bool,
                                                                         type: availVehicleType["type"] as? String,
                                                                         document: availDocument!, documentSmall: availSmallDocument!,
                                                                         vehicleOrder: availVehicleType["vehicleOrder"] as? Int,
                                                                         weightCapacity: availVehicleType["weightCapacity"] as? Int,
                                                                         documentID: availVehicleType["documentId"] as? Int,
                                                                         smallDocID: availVehicleType["smallDocId"] as? Int)
                            
                            
                            availableLoadCurrency = AvailableFleetBidCurrencyMaster(id: availCurrencyType?["id"] as? Int, currencyName: availCurrencyType?["currencyName"] as? String, currencyCountry: availCurrencyType?["currencyCountry"] as? String, currencyCode: availCurrencyType?["currencyCode"] as? String)
                            
                            availableFleetBidCurrencyMaster = AvailableFleetBidCurrencyMaster(id: availFleetBidCurrencyType["id"] as? Int, currencyName: availFleetBidCurrencyType["currencyName"] as? String, currencyCountry: availFleetBidCurrencyType["currencyCountry"] as? String, currencyCode: availFleetBidCurrencyType["currencyCode"] as? String)
                            
                            availCargoTypee = CargoType(id: (availCargoTypeArray["id"] as? Int),
                                                        cargoType: availCargoTypeArray["cargoType"] as? String)
                            
                            availCapacityWeight = CapacityWeightUnit(id: availCapacityWeightUnitArr["id"] as? Int, name: availCapacityWeightUnitArr["name"] as? String, type: availCapacityWeightUnitArr["type"] as? String)
                            
                            
                            availableFleetBid = AvailableFleetBid(id: availableFleetBidType["id"] as? Int, bidCode: availableFleetBidType["bidCode"] as? String, creationDate: availableFleetBidType["creationDate"] as? Int, closedBidTime: availableFleetBidType["closedBidTime"] as? Int, availableLocation: availableFleetBidType["availableLocation"] as? String, destinationLocation: availableFleetBidType["destinationLocation"] as? String, availableLatitude: availableFleetBidType["availableLatitude"] as? Double, availableLongitude: availableFleetBidType["availableLongitude"] as? Double, destinationLatitude: availableFleetBidType["destinationLatitude"] as? Double, destinationLongitude: availableFleetBidType["destinationLongitude"] as? Double, vehicleType: availableVehicleTypee!, noOfVehicles: availableFleetBidType["noOfVehicles"] as? Int, availableDateTime: availableFleetBidType["availableDateTime"] as? Int,capacityWeight: availableFleetBidType["capacityWeight"] as? Int,loadType: availLoadType!, expectedFreight: availableFleetBidType["expectedFreight"] as? Int, currencyMaster: availableFleetBidCurrencyMaster!, status: availableFleetBidType["status"] as? String, cargoType: availCargoTypee!, capacityWeightUnit: availCapacityWeight!)
                            
                            availableLoadBidQuotaionElement = AvailableLoadBidQuotaionElement(id: availableLoadBidQuotaionElementType["id"] as? Int, quotationAmount: availableLoadBidQuotaionElementType["quotationAmount"] as? Int, currencyMaster: availableLoadCurrency!, validDateTime: availableLoadBidQuotaionElementType["validDateTime"] as? Int, quotationEntryDate: availableLoadBidQuotaionElementType["quotationEntryDate"] as? Int, availableFleetBid: availableFleetBid!)
                            
                            
                            
                            
                            let datum = Datum(id: data["id"] as! Int,
                                              bidCode: data["bidCode"] as! String,
                                              creationDate: data["creationDate"] as! Int,
                                              closedBidTime: data["closedBidTime"] as! Int,
                                              availableLocation: data["availableLocation"] as! String,
                                              destinationLocation: data["destinationLocation"] as! String,
                                              vehicleType: vehicleTypee!,
                                              noOfVehicles: data["noOfVehicles"] as! Int,
                                              availableDateTime: data["availableDateTime"] as! Int,
                                              cargoType: cargoTypee!,
                                              loadOwner: loadOwner!,
                                              loadType: loadType1!,
                                              capacityWeight: data["capacityWeight"] as? Int,
                                              capacityWeightUnit: capacityWeight!,
                                              availableLatitude: data["availableLatitude"] as? Double,
                                              availableLongitude: data["availableLongitude"] as? Double,
                                              destinationLatitude: data["destinationLatitude"] as? Double,
                                              destinationLongitude: data["destinationLongitude"] as? Double,
                                              bidQuotationCount: data["bidQuotationCount"] as! Int,
                                              bidRank: data["bidRank"] as? Int,
                                              status: data["status"] as! String,
                                              count:
                                data["count"] as! Int?,
                                              availableLoadBidQuotaions: availableLoadBidQuotaionsArray, ownLastThreeBids: ownLastThreeBidsArray,
                                              packgs: data["packgs"] as? Int,
                                              loadOwnerID: data["loadOwnerId"] as? Int,
                                              vehicleTypeMaster: data["vehicleTypeMaster"] as! String, availableLoadBidQuotaionElement: availableLoadBidQuotaionElement)
                            datumArray.append(datum)
                            
                            if self.loadDatumArray.contains(where: { bid in bid.bidCode == datum.bidCode }) {
                                print(" exists in the array")
                            } else {
                                print(" does not exists in the array")
                                self.loadDatumArray.append(datum)
                                
                            }
                            
                            
                            loadDetails = Load(status: true, data: datumArray)
                            
                            self.loadArray.append(loadDetails!)
                         
                            
                        }
                        
                        for i in ownLastThreeBidsArray{
                            bidArray.append(i.quotationAmount)
                            
                        }
                        
                        if availableLoadBidQuotaionElementType["id"] as? Int != nil{
                            //    self.isQuoted = true
                        }else{
                            //  self.isQuoted = false
                        }
                        print(" availableLoadBidQuotaionElementType[] as? Int  === \( String(describing: availableLoadBidQuotaionElementType["id"] as? Int))")
                        
                        
                        
                    }
                    print("self.loadArray.count ==== \(self.loadArray.count)")
                    self.matchingLoadCV.reloadData()
                    
                }
                   self.matchingLoadCountLabel.text = "\(self.loadDatumArray.count)"
                break
            case .failure(let error):
                self.activityIndicatorEnd()
                print("POST REQUEST ERROR  - \(error.localizedDescription)")
                print("=== REQUEST INFORMATION ===")
                //                    print("Status Code: \(response.response!.statusCode)")
                print("Request Payload: \(parameters)")
                print("===")
                break
            }
        }
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    func fleetsPosted(){
        
        var bidlist : GetBidList?
        status = "OPEN"
        if isDragging == false{
            self.activityIndicatorBegin()
        }
        
        
        
        
        bidlist = GetBidList(pageSize:"10",pageNo:"\(self.pageNo)",recordType:"",search:"",fromCity:"",fromState:"",toState:"",toCity:"",vehicleType:"",loadType:"",noOfVehicle:"",fromDate:"",toDate:"",currentLatitude:"",currentLongitude:"",km: "",financialYear: self.currentFinancialYear!,cargoType: "")
        
        
        
        
        let jsonDataa = try! JSONEncoder().encode(bidlist)
        let type = String(data: jsonDataa, encoding: .utf8)!
        do {
            self.getBidListPara = try self.convertToDictionary(from: type)
            print("listt = \(self.getBidListPara)")
        } catch {
            print(error)
        }
        
        let parameters : [String:Any] = getBidListPara
        
        let headers = header()
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
        
        Alamofire.request(URLStatics.fleetPosted + "\(self.status)" , method: .get, parameters: parameters,headers: headers)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                
                
                switch (response.result) {
                    
                case .success:
                    
                    if self.isDragging == false{
                        self.activityIndicatorEnd()
                    }
                    
                    if let reply = response.result.value {
                        print("======= JSON: Fleet Posted ========  \(reply)")
                        let mainResponse = JSON(reply)
                        
                        
                        var returnDict = [[String:Any]]()
                        // var datumArray = [FleetDatum]()
                        var datum : FleetDatum?
                        var vehicleType = [String:Any]()
                        var smallDocumentType = [String:Any]()
                        var documentType = [String:Any]()
                        var document : FleetLogo?
                        var smallDocument : FleetLogo?
                        var cargoType : FleetCargoType?
                        var cargo = [String:Any]()
                        var capacityWt = [String:Any]()
                        var capacityWtUnit : FleetCapacityWeightUnit?
                        var loadType = [String:Any]()
                        var fleetLoadType : FleetLoadType?
                        var currencyMaster = [String:Any]()
                        var fleetCurrencyMaster : FleetCurrencyMaster
                        
                        var fleetVehicleType : FleetVehicleType?
                        if let returnObj = mainResponse["data"].arrayObject as? [[String:Any]] {
                            returnDict = returnObj
                        }
                        
                        for data in returnDict{
                            vehicleType = data["vehicleType"] as! [String:Any]
                            smallDocumentType = vehicleType["documentSmall"] as! [String:Any]
                            documentType = vehicleType["document"] as! [String:Any]
                            cargo = data["cargoType"] as! [String:Any]
                            capacityWt = data["capacityWeightUnit"] as! [String:Any]
                            loadType = data["loadType"] as! [String:Any]
                            currencyMaster = data["currencyMaster"] as! [String:Any]
                            
                            document = FleetLogo(id: documentType["id"] as! Int, mimeType: documentType["mimeType"] as! String, type: documentType["type"] as! String)
                            
                            smallDocument = FleetLogo(id: smallDocumentType["id"] as! Int, mimeType: smallDocumentType["mimeType"] as! String, type: smallDocumentType["type"] as! String)
                            
                            fleetVehicleType = FleetVehicleType(id: Int(truncating: vehicleType["id"] as! NSNumber), name: vehicleType["name"] as! String, sequence: Int(truncating: vehicleType["sequence"] as! NSNumber), container: vehicleType["container"] as! Bool, type: vehicleType["type"] as! String, document: document!, documentSmall: smallDocument!, vehicleOrder: Int(truncating: vehicleType["vehicleOrder"] as! NSNumber), documentID: Int(truncating: vehicleType["documentId"] as! NSNumber), smallDocID: Int(truncating: vehicleType["smallDocId"] as! NSNumber))
                            
                            fleetLoadType = FleetLoadType(id: Int(truncating: loadType["id"] as! NSNumber), loadType: loadType["loadType"] as! String, code: loadType["code"] as! String)
                            
                            cargoType = FleetCargoType(id: cargo["id"] as! Int, cargoType: cargo["cargoType"] as! String)
                            
                            capacityWtUnit = FleetCapacityWeightUnit(id: capacityWt["id"] as! Int, name: capacityWt["name"] as! String, type: capacityWt["type"] as! String)
                            
                            fleetCurrencyMaster = FleetCurrencyMaster(id: currencyMaster["id"] as! Int, currencyName: currencyMaster["currencyName"] as! String, currencyCountry: currencyMaster["currencyCountry"] as! String, currencyCode: currencyMaster["currencyCode"] as! String)
                            
                            datum = FleetDatum(id: data["id"] as! Int,
                                               bidCode: data["bidCode"] as! String,
                                               creationDate: data["creationDate"] as! Int,
                                               closedBidTime: data["closedBidTime"] as! Int,
                                               availableLocation: data["availableLocation"] as! String,
                                               destinationLocation: data["destinationLocation"] as? String,
                                               availableLatitude: data["availableLatitude"] as? Double,
                                               availableLongitude: data["availableLongitude"] as? Double,
                                               destinationLatitude: data["destinationLatitude"] as? Double,
                                               destinationLongitude: data["destinationLongitude"] as? Double,
                                               count: data["count"] as? Int,
                                               vehicleType: fleetVehicleType!,
                                               noOfVehicles: data["noOfVehicles"] as! Int,
                                               availableDateTime: data["availableDateTime"] as! Int,
                                               cargoType: cargoType!,
                                               capacityWeight: data["capacityWeight"] as! Int,
                                               capacityWeightUnit: capacityWtUnit!,
                                               loadType: fleetLoadType!,
                                               expectedFreight: data["expectedFreight"] as? NSNumber,
                                               currencyMaster: fleetCurrencyMaster,
                                               isInvited: data["isInvited"] as! Bool,
                                               bidInviteCount: data["bidInviteCount"] as! Int,
                                               vehicleTypeMaster: data["vehicleTypeMaster"] as? String,
                                               loadBidCode: data["loadBidCode"] as? String,
                                               registrationNo: data["registrationNo"] as? String,
                                               comments: data["comments"] as? String, sequence: data["sequence"] as? Int, postType: data["postType"] as? String)
                            
                            if self.datumArray.contains(where: { bid in bid.bidCode == datum?.bidCode }) {
                                print(" exists in the array")
                            } else {
                                print(" does not exists in the array")
                                self.datumArray.append(datum!)
                            }
                            
                            
                        }
                        print( "datumArray.count ====== \(self.datumArray.count)")
                       
                        self.tableView.reloadData()
                    }
                     self.fleetPOstedCountLabel.text = "\(self.datumArray.count)"
                    break
                case .failure(let error):
                    
                    print("error \(error.localizedDescription)")
                    break
                }
        }
        
    }
    
    
}

extension UICollectionView {
    func scrollToNearestVisibleCollectionViewCell() {
        self.decelerationRate = UIScrollView.DecelerationRate.fast
        let visibleCenterPositionOfScrollView = Float(self.contentOffset.x + (self.bounds.size.width / 2))
        var closestCellIndex = -1
        var closestDistance: Float = .greatestFiniteMagnitude
        for i in 0..<self.visibleCells.count {
            let cell = self.visibleCells[i]
            let cellWidth = cell.bounds.size.width
            let cellCenter = Float(cell.frame.origin.x + cellWidth)
            
            // Now calculate closest cell
            let distance: Float = fabsf(visibleCenterPositionOfScrollView - cellCenter)
            if distance < closestDistance {
                closestDistance = distance
                closestCellIndex = self.indexPath(for: cell)!.row
            }
        }
        if closestCellIndex != -1 {
            self.scrollToItem(at: IndexPath(row: closestCellIndex, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
}

