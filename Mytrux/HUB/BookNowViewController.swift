//
//  BookNowViewController.swift
//  Mytrux
//
//  Created by Mytrux on 11/11/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON
import Pulsator

class BookNowViewController: BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,GMSMapViewDelegate{
    
    
    @IBOutlet weak var matchLoadView: UIView!
    @IBOutlet weak var viewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var matchLoadSuperview: UIView!
    @IBOutlet weak var fleetPOstedSuperView: UIView!
    @IBOutlet weak var matchLoadCollectionView: UICollectionView!
    @IBOutlet weak var fleetPOstedCount: UILabel!
    @IBOutlet weak var matchingLoadCountView: UIView!
    @IBOutlet weak var fleetPostedCountView: UIView!
    @IBOutlet weak var topbar: UIView!
    @IBOutlet weak var matchingLoadCount: UILabel!
    @IBOutlet weak var fleetPostedCollectionView: UICollectionView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var showHideButton: UIView!
    @IBOutlet weak var buttonImage: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var fleetDetailView: UIView!
    
    @IBOutlet weak var goLabel: UILabel!
    @IBOutlet weak var goBtn: UIView!
    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var vehicleType: UILabel!
    @IBOutlet weak var to: UILabel!
    @IBOutlet weak var from: UILabel!
    
    
    var loadArray = [Load]()
    var loadDatumArray = [Datum]()
    var isSelected = false
    var selectedIndex = IndexPath()
    var selectedIndex1 = IndexPath()
    var currentRow = Int()
    var currentRow1 = Int()
    var cellTapped:Bool = false
    var cellTapped1:Bool = false
    var fleetId = String()
    var cargoTypeId = String()
    var loadTypeId = String()
    var vehicleTypeId = String()
    var latitude = String()
    var longitude = String()
    var fleetIdd = String()
    
    var isDragging = false
    var status = String()
    var pageNo = 0
    var pageNo1 = 0
    var getBidListPara = [String:Any]()
    var datumArray = [FleetDatum]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.clearCache(with: "34")
        self.fleetPostedCollectionView.delegate =  self
        self.fleetPostedCollectionView.dataSource =  self
        self.matchLoadCollectionView.delegate =  self
        self.matchLoadCollectionView.dataSource =  self
        self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = true
        circularView(viewArray: [showHideButton,matchingLoadCountView,fleetPostedCountView,goBtn])
        topBarCard(topBar: topbar)
        cardViewArray(arrView: [matchLoadSuperview,fleetPOstedSuperView,fleetDetailView])
        goBtn.layer.shadowColor = UIColor.gray.cgColor
        goBtn.layer.shadowOffset = CGSize(width: 0.2, height: 0.2)
        goBtn.layer.shadowRadius = 5
        goBtn.layer.shadowOpacity = 0.5
        
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "map_style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                print("Unable to find map_style.json")
            }
        } catch {
            print("One or more of the map styles failed to load. \(error)")
        }
        self.fleetPosted()
        self.currentRow = -1
         self.currentRow1 = -1
        loadDetails()
        self.buttonImage.image = UIImage(named: "bottomArrow")
        buttonImage.image = buttonImage.image?.withRenderingMode(.alwaysTemplate)
        buttonImage.tintColor = UIColor.white
          self.buttonImage.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        
        //   drawPath(origin:"VimanNagar",destination:"Wagholi")
        // Do any additional setup after loading the view.
    }
    var isViewOpen = true
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
       
        if self.startLat == marker.position.latitude && self.startLng == marker.position.longitude{
            print(" =====startLat  marker.position ===== ")
                   print(marker.position)
            self.fleetDetailView.isHidden = false
              fleetDetailView.gestureRecognizers?.removeAll()
            let tap = UITapGestureRecognizer(target: self, action: #selector(fleetDetailViewClicked(recognizer:)))
            self.fleetDetailView.addGestureRecognizer(tap)
            self.id.text = self.fleetData?.bidCode
            self.vehicleType.text = self.fleetData?.vehicleType.name
            self.from.text = self.fleetData?.availableLocation
            self.to.text = self.fleetData?.destinationLocation
     
        }else if self.endLat == marker.position.latitude && self.endLng == marker.position.longitude{
            print(" =====endLat startLat  marker.position ===== ")
                              print(marker.position)
            self.fleetDetailView.isHidden = false
            fleetDetailView.gestureRecognizers?.removeAll()
            let tap = UITapGestureRecognizer(target: self, action: #selector(loadDetailViewClicked(recognizer:)))
                       self.fleetDetailView.addGestureRecognizer(tap)
        }else{
            self.fleetDetailView.isHidden = true
        }
        
        return true
    }
    
    @IBAction func goBtnClicked(_ sender: Any) {
      
            if fleetIdd == ""{
                          let alert = UIAlertController(title:"", message: "Please Select Fleet First", preferredStyle: UIAlertController.Style.alert)
                          alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

                          self.present(alert, animated: true, completion: nil)
                          return
                      }
        if loadDetail == nil{
            let alert = UIAlertController(title:"Note!", message: "Please select vehicle", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        let loadDetail = self.loadDetail!
               let timestap: Double = Double(loadDetail.availableDateTime / 1000)
               let availableFromtime = self.epochTime(epochTimee: timestap)
               let availableFromdate = self.epochDate(epochDate: timestap)
               let timestamp: Double = Double(loadDetail.closedBidTime / 1000)
               let availableTilltime = self.epochTime(epochTimee: timestamp)
               let availableTilldate = self.epochDate(epochDate: timestamp)
               let availableLoadQuotations = loadDetail.availableLoadBidQuotaions
               let ownLastThreeBids = loadDetail.ownLastThreeBids
               let availableLoadBidQuotaionElement = loadDetail.availableLoadBidQuotaionElement
               let loadObject = ["id": loadDetail.id,"bidCode": loadDetail.bidCode,
                                 "vehicleType": loadDetail.vehicleType.name,
                                 "from": loadDetail.availableLocation,
                                 "to": loadDetail.destinationLocation,
                                 "loadType": loadDetail.loadType.loadType, "noOfVech":loadDetail.noOfVehicles,
                                 "cargoType": loadDetail.cargoType.cargoType!,
                                 "cargoWt": loadDetail.capacityWeight!,
                                 "packgs": loadDetail.packgs,
                                 "availableFromdate": availableFromdate,
                                 "availableFromtime": availableFromtime,
                                 "availableTilltime": availableTilltime,
                                 "availableTilldate": availableTilldate,
                                 "kgName": loadDetail.capacityWeightUnit.name!,
                                 "lowestBid": 0,"closedBidTime":loadDetail.closedBidTime,
                                 "bidRank": loadDetail.bidRank ?? 0,
                                 "availableLoadQuotations": availableLoadQuotations,
                                 "ownLastThreeBids": ownLastThreeBids!,
                                 "availableLoadBidQuotaionElement": availableLoadBidQuotaionElement!,"latitude": 0.0,"longitude":0.0,"vehicleTypeId":loadDetail.vehicleType.id,"loadTypeId":loadDetail.loadType.id,"status":"MatchLoad","fleet":fleetData!] as [String : Any]
               //
               //        print("fleetIdd === \(self.fleetIdd)")
               //
               //        print("loadId ==== \(String(describing: loadDetail.availableLoadBidQuotaionElement?.availableFleetBid.bidCode!))")
               if loadDetail.availableLoadBidQuotaionElement?.id != nil {
                   
               
               if self.fleetIdd != loadDetail.availableLoadBidQuotaionElement?.availableFleetBid.bidCode{
                   let alert = UIAlertController(title:"Note!", message: "Sorry you have already Quoted for the Same Load with Fleet id \(String(describing: loadDetail.availableLoadBidQuotaionElement!.availableFleetBid.bidCode!))", preferredStyle: UIAlertController.Style.alert)
                   alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                   self.present(alert, animated: true, completion: nil)
               }else{
                   sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "LoadDetailViewController") }, with: "seventh")
                   sideMenuController?.setContentViewController(with: "seventh")
                   NotificationCenter.default.post(name: Notification.Name("loadObj"),
                                                   object:nil,userInfo: loadObject)
                   }
               }else{
                   sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "LoadDetailViewController") }, with: "seventh")
                   sideMenuController?.setContentViewController(with: "seventh")
                   NotificationCenter.default.post(name: Notification.Name("loadObj"),
                                                   object:nil,userInfo: loadObject)
               }
    }
    
     @objc func fleetDetailViewClicked(recognizer: UITapGestureRecognizer){
        sideMenuController?.setContentViewController(with: "26")
                              NotificationCenter.default.post(name: Notification.Name("fleetDetail"),
                                                              object:nil,userInfo: ["fleetDetails":self.fleetData!,"state": "bookNow"])
    }
    
    @objc func loadDetailViewClicked(recognizer: UITapGestureRecognizer){
        let loadDetail = self.loadDetail!
        let timestap: Double = Double(loadDetail.availableDateTime / 1000)
        let availableFromtime = self.epochTime(epochTimee: timestap)
        let availableFromdate = self.epochDate(epochDate: timestap)
        let timestamp: Double = Double(loadDetail.closedBidTime / 1000)
        let availableTilltime = self.epochTime(epochTimee: timestamp)
        let availableTilldate = self.epochDate(epochDate: timestamp)
        let availableLoadQuotations = loadDetail.availableLoadBidQuotaions
        let ownLastThreeBids = loadDetail.ownLastThreeBids
        let availableLoadBidQuotaionElement = loadDetail.availableLoadBidQuotaionElement
        let loadObject = ["id": loadDetail.id,"bidCode": loadDetail.bidCode,
                          "vehicleType": loadDetail.vehicleType.name,
                          "from": loadDetail.availableLocation,
                          "to": loadDetail.destinationLocation,
                          "loadType": loadDetail.loadType.loadType, "noOfVech":loadDetail.noOfVehicles,
                          "cargoType": loadDetail.cargoType.cargoType!,
                          "cargoWt": loadDetail.capacityWeight!,
                          "packgs": loadDetail.packgs,
                          "availableFromdate": availableFromdate,
                          "availableFromtime": availableFromtime,
                          "availableTilltime": availableTilltime,
                          "availableTilldate": availableTilldate,
                          "kgName": loadDetail.capacityWeightUnit.name!,
                          "lowestBid": 0,"closedBidTime":loadDetail.closedBidTime,
                          "bidRank": loadDetail.bidRank ?? 0,
                          "availableLoadQuotations": availableLoadQuotations,
                          "ownLastThreeBids": ownLastThreeBids!,
                          "availableLoadBidQuotaionElement": availableLoadBidQuotaionElement!,"latitude": 0.0,"longitude":0.0,"vehicleTypeId":loadDetail.vehicleType.id,"loadTypeId":loadDetail.loadType.id,"status":"bookNow","fleet":fleetData!] as [String : Any]
        //
        //        print("fleetIdd === \(self.fleetIdd)")
        //
        //        print("loadId ==== \(String(describing: loadDetail.availableLoadBidQuotaionElement?.availableFleetBid.bidCode!))")
        if loadDetail.availableLoadBidQuotaionElement?.id != nil {
            
        
        if self.fleetIdd != loadDetail.availableLoadBidQuotaionElement?.availableFleetBid.bidCode{
            let alert = UIAlertController(title:"Note!", message: "Sorry you have already Quoted for the Same Load with Fleet id \(String(describing: loadDetail.availableLoadBidQuotaionElement!.availableFleetBid.bidCode!))", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "LoadDetailViewController") }, with: "seventh")
            sideMenuController?.setContentViewController(with: "seventh")
            NotificationCenter.default.post(name: Notification.Name("loadObj"),
                                            object:nil,userInfo: loadObject)
            }
        }else{
            sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "LoadDetailViewController") }, with: "seventh")
            sideMenuController?.setContentViewController(with: "seventh")
            NotificationCenter.default.post(name: Notification.Name("loadObj"),
                                            object:nil,userInfo: loadObject)
        }
        
    }
    
    @IBAction func showHIdeClicked(_ sender: Any) {
        if isViewOpen{
            
            UIView.animate(withDuration: 0.5) {
                let newConstraint = self.viewHeightConstraint.constraintWithMultiplier(0.001)
                self.view.removeConstraint(self.viewHeightConstraint)
                self.view.addConstraint(newConstraint)
                self.viewHeightConstraint = newConstraint
                self.matchLoadView.isHidden = true
                self.view.layoutIfNeeded()
                UIView.animate(withDuration:5.0, animations: {
                    self.buttonImage.transform = .identity
                })
                
            }
            
        }else{
            
            UIView.animate(withDuration: 0.5) {
                self.matchLoadView.isHidden = false
                let newConstraint = self.viewHeightConstraint.constraintWithMultiplier(0.6)
                self.view.removeConstraint(self.viewHeightConstraint)
                self.view.addConstraint(newConstraint)
                
                self.viewHeightConstraint = newConstraint
                self.view.layoutIfNeeded()
                UIView.animate(withDuration:5.0, animations: {
                    self.buttonImage.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                })
            }
        }
        //    drawPath(origin:"VimanNagar",destination:"Wagholi")
        
        if currentRow == -1 || currentRow1 == -1{
            
        }else{
            
       
        if self.startLat == self.endLat && self.startLng == self.endLng{

           let startPosition = CLLocationCoordinate2D(latitude: self.startLat,longitude: self.startLng)
           let startMarker = GMSMarker(position: startPosition)
           
           startMarker.icon = UIImage(named: "sourceMarker")
           startMarker.setIconSize(scaledToSize: .init(width: self.mapView.frame.width/20, height: self.mapView.frame.width/14))
          pulseAnimation(position: startPosition)
           startMarker.appearAnimation = .pop
           startMarker.map = self.mapView
             

              let endPosition = CLLocationCoordinate2D(latitude: self.endLat - 0.0001,longitude: self.endLng - 0.0001)
                              let endMarker = GMSMarker(position: endPosition)
                      
                              endMarker.appearAnimation = .pop
                              endMarker.icon = UIImage(named: "destinationMarker")
                              endMarker.setIconSize(scaledToSize: .init(width: self.mapView.frame.width/20, height: self.mapView.frame.width/14))
               pulseAnimation(position: endPosition)
                              endMarker.map = self.mapView
             
             let bounds = GMSCoordinateBounds(coordinate: startPosition, coordinate: endPosition)
                               self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
             
         }else{
             if timer != nil{
                             self.timer.invalidate()
                       }
             drawPath(origin: "\(self.startLat),\(self.startLng)", destination: "\(self.endLat),\(self.endLng)")
         }
             }
        isViewOpen = !isViewOpen
    }
  
    @IBAction func refreshViewClicked(_ sender: Any) {
                cargoTypeId = ""
              loadTypeId = ""
               vehicleTypeId = ""
               latitude = ""
              longitude = ""
              fleetId = ""
              self.fleetIdd = ""
              self.datumArray.removeAll()
              self.loadDatumArray.removeAll()
              self.loadArray.removeAll()
        mapView.clear()
        self.fleetDetailView.isHidden = true
        if timer != nil{
            timer.invalidate()
        }
             self.fleetPostedCollectionView.reloadData()
              self.matchLoadCollectionView.reloadData()
              self.currentRow = -1
                self.currentRow1 = -1
              loadDetails()
               self.fleetPosted()
    }
    
    @IBAction func listViewClicked(_ sender: Any) {
          sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "MatchingLoadsVC") }, with: "22")
        sideMenuController?.setContentViewController(with: "22")
    }
    
    @IBAction func backClicked(_ sender: Any) {
        sideMenuController?.setContentViewController(with: "default")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let numberofItem: CGFloat = 4
        
        let collectionViewWidth = collectionView.bounds.width
        
        let extraSpace = (numberofItem - 1) * flowLayout.minimumInteritemSpacing
        
        let inset = flowLayout.sectionInset.right + flowLayout.sectionInset.left
        
        let width = Int((collectionViewWidth - extraSpace - inset) / numberofItem)
        
        print(width)
        
        return CGSize(width: width , height: width - 10)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == fleetPostedCollectionView {
            return datumArray.count
        }else{
            return self.loadDatumArray.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //  bookNowFleetPosed
        if collectionView == fleetPostedCollectionView{
            let cell = fleetPostedCollectionView.dequeueReusableCell(withReuseIdentifier: "bookNowFleetPosed", for: indexPath) as! BookNowCollectionViewCell
            if datumArray.count != 0{
                let fleet = datumArray[indexPath.row]
                cell.fleetNo.text = fleet.bidCode
                cell.fleetVehicleName.text = fleet.vehicleType.name
                cell.amount.text = "\(Double(fleet.expectedFreight!))" + "0"
                
                let tap = UITapGestureRecognizer(target: self, action: #selector(fleetDetailView(recognizer:)))
                cell.fleetPOstedView.addGestureRecognizer(tap)
                cell.fleetPOstedView.tag = indexPath.row
                
                if self.currentRow == indexPath.row{
                    cell.fleetPOstedView.backgroundColor = UIColor(hexString: ColorConstants.LIGHT_BLUE)
                }else{
                    cell.fleetPOstedView.backgroundColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
                }
                
            }
            return cell
            
        }
        else{
            let cell = matchLoadCollectionView.dequeueReusableCell(withReuseIdentifier: "bookNowMatchLoad", for: indexPath) as! BookNowMatchLoadCell
            if self.loadDatumArray.count != 0 {
                
                let loadDetail = loadDatumArray[indexPath.row]
                cell.loadNoLabel.text = loadDetail.bidCode
                cell.nameLabel.text = loadDetail.vehicleType.name
                cell.amount.text = "NA"
                //                       cell.quotedView.roundCorners(corners:        [.bottomRight,.topRight], radius: 5)
                
                       if loadDetail.availableLoadBidQuotaionElement?.id != nil{
                           cell.quotedImage.isHidden = false
                           cell.matchLoadView.backgroundColor = UIColor(hexString: ColorConstants.DARK_BLUE)
                           cell.amount.text = "\(Double(loadDetail.availableLoadBidQuotaions[0].quotationAmount))" + "0"
                           cell.loadNoLabel.textColor = UIColor.white
                           //                           cell.fleetId.text = loadDetail.availableLoadBidQuotaionElement?.availableFleetBid.bidCode
                           
                       }else{
                           cell.quotedImage.isHidden = true
                           cell.matchLoadView.backgroundColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
                           cell.loadNoLabel.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
                       }
                
                if self.currentRow1 == indexPath.row{
                                 cell.matchLoadView.backgroundColor = UIColor(hexString: ColorConstants.LIGHT_BLUE)
                     cell.loadNoLabel.textColor = UIColor.white
                    
                             }else{
                                 cell.matchLoadView.backgroundColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
                     cell.loadNoLabel.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
                    if loadDetail.availableLoadBidQuotaionElement?.id != nil{
                                             cell.quotedImage.isHidden = false
                                             cell.matchLoadView.backgroundColor = UIColor(hexString: ColorConstants.DARK_BLUE)
                                             cell.amount.text = "\(Double(loadDetail.availableLoadBidQuotaions[0].quotationAmount))" + "0"
                                             cell.loadNoLabel.textColor = UIColor.white
                                             //                           cell.fleetId.text = loadDetail.availableLoadBidQuotaionElement?.availableFleetBid.bidCode
                                             
                                         }else{
                                             cell.quotedImage.isHidden = true
                                             cell.matchLoadView.backgroundColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
                                             cell.loadNoLabel.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
                                         }
                             }
       
                
                                       let tap = UITapGestureRecognizer(target: self, action: #selector(loadViewClicked(recognizer:)))
                                       cell.matchLoadView.addGestureRecognizer(tap)
                                       cell.matchLoadView.tag = indexPath.row
            }
            return cell
        }
        
        
    }
    
    var endLat = Double()
    var endLng = Double()
    var loadDetail : Datum?
    @objc func loadViewClicked(recognizer: UITapGestureRecognizer){
        mapView.clear()
         self.fleetDetailView.isHidden = true
           if fleetIdd == ""{
               let alert = UIAlertController(title:"", message: "Please Select Fleet First", preferredStyle: UIAlertController.Style.alert)
               alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

               self.present(alert, animated: true, completion: nil)
               return
           }
               let tag = recognizer.view?.tag
               let loadDetaill = self.loadDatumArray[tag!]
        self.loadDetail = loadDetaill
                  currentRow1 = tag!
                  cellTapped1 = !cellTapped1
        self.id.text = loadDetaill.bidCode
        self.vehicleType.text = loadDetaill.vehicleType.name
        self.to.text = loadDetaill.destinationLocation
        self.from.text = loadDetaill.availableLocation
        
                  self.matchLoadCollectionView.reloadData()
        let lat = loadDetail?.availableLatitude!
        let lng = loadDetail?.availableLongitude!
        self.endLat = lat!
        self.endLng = lng!
        if self.startLat == self.endLat && self.startLng == self.endLng{

          let startPosition = CLLocationCoordinate2D(latitude: self.startLat,longitude: self.startLng)
          let startMarker = GMSMarker(position: startPosition)
          
//          startMarker.icon = UIImage(named: "sourceMarker")
//          startMarker.setIconSize(scaledToSize: .init(width: self.mapView.frame.width/20, height: self.mapView.frame.width/14))
         pulseAnimation(position: startPosition)
          startMarker.appearAnimation = .pop
            startMarker.iconView = UIView(frame: CGRect(x: 0, y: 0, width:  self.mapView.frame.width/20, height:  self.mapView.frame.width/14))
                                                   let imageView =  UIImageView(frame: CGRect(x: 0, y: 0, width: self.mapView.frame.width/20, height: self.mapView.frame.width/14))
                                                   imageView.contentMode = .scaleAspectFit
                                                   imageView.image = UIImage(named: "sourceMarker")
                                                   startMarker.iconView?.addSubview(imageView)
                                            //     imageView.layer.anchorPoint = CGPoint(x: 0.5, y: 1.0) //we need adjust anchor point to achieve the desired behavior
                                                   imageView.layer.frame = CGRect(x: 0, y: 0, width: self.mapView.frame.width/20, height: self.mapView.frame.width/14) //we need adjust the layer frame

                               let origin:CGPoint = imageView.center
                                                 let target:CGPoint = CGPoint(x: imageView.center.x, y: imageView.center.y+5)
                                                 let bounce = CABasicAnimation(keyPath: "position.y")
                                                 bounce.duration = 0.5
                                                 bounce.fromValue = target.y
                                                 bounce.toValue = origin.y
                                                 bounce.repeatCount = 2
                                                 bounce.autoreverses = true
                                                 imageView.layer.add(bounce, forKey: "position")
                                                   
                                         
                                        startMarker.map = self.mapView
                                          
            

             let endPosition = CLLocationCoordinate2D(latitude: self.endLat - 0.0001,longitude: self.endLng - 0.0001)
                             let endMarker = GMSMarker(position: endPosition)
                     
//                             endMarker.appearAnimation = .pop
//                             endMarker.icon = UIImage(named: "destinationMarker")
//                             endMarker.setIconSize(scaledToSize: .init(width: self.mapView.frame.width/20, height: self.mapView.frame.width/14))
              pulseAnimation(position: endPosition)
                            // endMarker.map = self.mapView
            
            
            endMarker.iconView = UIView(frame: CGRect(x: 0, y: 0, width:  self.mapView.frame.width/20, height:  self.mapView.frame.width/14))
                                                  let imageView1 = UIImageView(frame: CGRect(x: 0, y: 0, width: self.mapView.frame.width/20, height: self.mapView.frame.width/14))
                                                  imageView1.contentMode = .scaleAspectFit
                                                  imageView1.image = UIImage(named: "destinationMarker")
                                                  endMarker.iconView?.addSubview(imageView1)
                                           //     imageView.layer.anchorPoint = CGPoint(x: 0.5, y: 1.0) //we need adjust anchor point to achieve the desired behavior
                                                  imageView1.layer.frame = CGRect(x: 0, y: 0, width: self.mapView.frame.width/20, height: self.mapView.frame.width/14) //we need adjust the layer frame

                               let origin1:CGPoint = imageView1.center
                                                let target1:CGPoint = CGPoint(x: imageView1.center.x, y: imageView1.center.y+5)
                                                let bounce1 = CABasicAnimation(keyPath: "position.y")
                                                bounce1.duration = 0.5
                                                bounce1.fromValue = target1.y
                                                bounce1.toValue = origin1.y
                                                bounce1.repeatCount = 2
                                                bounce1.autoreverses = true
                                                imageView1.layer.add(bounce1, forKey: "position")
                                            endMarker.map = self.mapView
            
            let bounds = GMSCoordinateBounds(coordinate: startPosition, coordinate: endPosition)
                              self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
            
            
            
            
        }else{
            if timer != nil{
                  self.timer.invalidate()
            }
          
            drawPath(origin: "\(self.startLat),\(self.startLng)", destination: "\(self.endLat),\(self.endLng)")
        }
       
        
        
        
        
        
    }
    
    var startLat = Double()
    var startLng = Double()
    var fleetData : FleetDatum?
    
    @objc func fleetDetailView(recognizer: UITapGestureRecognizer){
        let tag = recognizer.view?.tag
        let fleetDetail = self.datumArray[tag!]
        self.fleetData = fleetDetail
        self.fleetDetailView.isHidden = true
        self.currentRow1 = -1
        self.currentRow = tag!
        cellTapped = !cellTapped
               let load = self.datumArray[tag!]
               cargoTypeId = String(load.cargoType.id)
               loadTypeId = String(load.loadType.id)
               vehicleTypeId = String(load.vehicleType.id)
               fleetIdd = load.bidCode
        
               latitude = String(load.availableLatitude!)
               longitude = String(load.availableLongitude!)
               self.loadDatumArray.removeAll()
               self.loadArray.removeAll()
               self.matchLoadCollectionView.reloadData()
               self.loadDetails()
               self.fleetPostedCollectionView.reloadData()
        self.fleetPostedCollectionView.reloadData()
        self.mapView.clear()
        let lat = fleetDetail.availableLatitude!
        let lng = fleetDetail.availableLongitude!
        self.startLat = lat
        self.startLng = lng
        let startPosition = CLLocationCoordinate2D(latitude: lat,longitude: lng)
        let startMarker = GMSMarker(position: startPosition)
        if timer != nil {
            self.timer.invalidate()
        }
        
     //   startMarker.icon = UIImage(named: "sourceMarker")
    //    startMarker.icon = UIImage(named: "sourceMarker")
     //   startMarker.setIconSize(scaledToSize: .init(width: self.mapView.frame.width/22, height: self.mapView.frame.width/14))
        //  startMarker.snippet = origin
       // startMarker.appearAnimation = .pop

        self.mapView.camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: 9)
              CATransaction.begin()
              CATransaction.setValue(2.0, forKey: kCATransactionAnimationDuration)
              let city = GMSCameraPosition.camera(withLatitude: lat,longitude: lng, zoom: 14)
              self.mapView.animate(to: city)
              CATransaction.commit()

        //we need a bigger UIView to avoid the clip problem with the UIImageView
       startMarker.iconView = UIView(frame: CGRect(x: 0, y: 0, width:  self.mapView.frame.width/20, height:  self.mapView.frame.width/14))
                                                         let imageView =  UIImageView(frame: CGRect(x: 0, y: 0, width: self.mapView.frame.width/20, height: self.mapView.frame.width/14))
                                                         imageView.contentMode = .scaleAspectFit
                                                         imageView.image = UIImage(named: "sourceMarker")
                                                         startMarker.iconView?.addSubview(imageView)
                                                  //     imageView.layer.anchorPoint = CGPoint(x: 0.5, y: 1.0) //we need adjust anchor point to achieve the desired behavior
                                                         imageView.layer.frame = CGRect(x: 0, y: 0, width: self.mapView.frame.width/20, height: self.mapView.frame.width/14) //we need adjust the layer frame

                                     let origin:CGPoint = imageView.center
                                                       let target:CGPoint = CGPoint(x: imageView.center.x, y: imageView.center.y+5)
                                                       let bounce = CABasicAnimation(keyPath: "position.y")
        bounce.duration = 0.8
                                                       bounce.fromValue = target.y
                                                       bounce.toValue = origin.y
                                                       bounce.repeatCount = 2
                                                       bounce.autoreverses = true
                                                       imageView.layer.add(bounce, forKey: "position")
                                              startMarker.map = self.mapView
   
    }
    
    
    func pulseAnimation(position: CLLocationCoordinate2D){
        let m = GMSMarker(position: position)

                     //custom marker image
                     let pulseRingImg = UIImageView(frame: CGRect(x: -10, y: -10, width: 20, height:15))
                     pulseRingImg.image = UIImage(named: "circle2")
           
               
           pulseRingImg.image = pulseRingImg.image?.withRenderingMode(.alwaysTemplate)
                pulseRingImg.tintColor = UIColor(hexString: ColorConstants.DARK_BLUE )
                     pulseRingImg.isUserInteractionEnabled = false
                     CATransaction.begin()
           CATransaction.setAnimationDuration(2.5)

                     //transform scale animation
                     var theAnimation: CABasicAnimation?
                     theAnimation = CABasicAnimation(keyPath: "transform.scale.xy")
                     theAnimation?.repeatCount = Float.infinity
                     theAnimation?.autoreverses = false
                     theAnimation?.fromValue = Float(0.0)
                     theAnimation?.toValue = Float(2.0)
                     theAnimation?.isRemovedOnCompletion = false

                     pulseRingImg.layer.add(theAnimation!, forKey: "pulse")
                     pulseRingImg.isUserInteractionEnabled = false
                     CATransaction.setCompletionBlock({() -> Void in

                         //alpha Animation for the image
                         let animation = CAKeyframeAnimation(keyPath: "opacity")
                       animation.duration = 2.5
                         animation.repeatCount = Float.infinity
                         animation.values = [Float(2.0), Float(0.0)]
                         m.iconView?.layer.add(animation, forKey: "opacity")
                     })

                     CATransaction.commit()
                     m.iconView = pulseRingImg
                     m.layer.addSublayer(pulseRingImg.layer)
           m.map = self.mapView
                     m.groundAnchor = CGPoint(x: 0.5, y: 0.5)
           
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == fleetPostedCollectionView{

        }else{
          

        }
        
    }
    
    
    
    func loadDetails(){
        if longitude == "0.0" || latitude == "0.0" {
            longitude = ""
            latitude = ""
        }
        let indexx = userDefaults.value(forKey: "index") as! Int
        print("indexxx ===== \(indexx)")
        //self.activityIndicatorBegin()
        
        
        let bidlist = GetBidList(pageSize:"20",pageNo:"\(self.pageNo1)",recordType:"",search:"",fromCity:"",fromState:"",toState:"",toCity:"",vehicleType:"\(vehicleTypeId)",loadType:"\(loadTypeId)",noOfVehicle:"",fromDate:"",toDate:"",currentLatitude:"\(latitude)",currentLongitude:"\(longitude)",km: String(indexx),financialYear: self.currentFinancialYear!,cargoType:"\(cargoTypeId)")
        
        
        let jsonDataa = try! JSONEncoder().encode(bidlist)
        let type = String(data: jsonDataa, encoding: .utf8)!
        do {
            self.getBidListPara = try self.convertToDictionary(from: type)
            print("loadDetails listt = \(self.getBidListPara)")
        } catch {
            print(error)
        }
        
        let parameters : [String:Any] = getBidListPara
        
        let headers = header()
        
        //+918669696004
        //asdfgf1234$
        Alamofire.request(URLStatics.findavailableloadforbidlist,method: .get, parameters: parameters, headers: headers).responseJSON { response in
            print("response.request=\(String(describing: response.request))")  // original URL request
            print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
            print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
            //   self.activityIndicatorEnd()
            switch (response.result) {
                
            case .success:
                self.loadDatumArray.removeAll()
                if let reply = response.result.value {
                    print("JSON: >>>>>>>>>>>>>>>>>>>>>>>>>> \(reply)")
                    let mainResponse = JSON(reply)
                    var returnDict = [[String:Any]]()
                    // let returnObj = mainResponse["data"].array
                    if let returnObj = mainResponse["data"].arrayObject as? [[String:Any]] {
                        returnDict = returnObj
                    }
                    var resultNew = [String:Any]()
                    var vehicleType = [String:Any]()
                    var availVehicleType = [String:Any]()
                    var documentType : [String:Any]?
                    var docuType = [String:Any]()
                    let availDocumentType = [String:Any]()
                    var availSmallDocType = [String:Any]()
                    var logoType : [String:Any]?
                    var addressType : [String:Any]?
                    var companyAccessArr = [String:Any]()
                    
                    
                    var cargoTypeArray = [String:Any]()
                    var availCargoTypeArray = [String:Any]()
                    var capacityWeightUnitArr = [String:Any]()
                    var availCapacityWeightUnitArr = [String:Any]()
                    
                    
                    var currencyMasterType = [String:Any]()
                    var availCurrencyType : [String:Any]?
                    
                    
                    var loadTypeArray = [String]()
                    
                    var loadType1 : LoadType1?
                    var vehicleTypee : VehicleType?
                    var availableVehicleTypee : AvailableVehicleType?
                    
                    var datumArray = [Datum]()
                    var loadDetails : Load?
                    var cargoTypee : CargoType?
                    var availCargoTypee : CargoType?
                    var capacityWeight : CapacityWeightUnit?
                    var availCapacityWeight : CapacityWeightUnit?
                    var loadOwnerArr = [String:Any]()
                    var companyAccess : CompanyAccess?
                    var loadOwner : LoadOwner?
                    var address : Address1?
                    var document : Logo?
                    var docs : Logo?
                    var availDocument : AvailableLogo?
                    var availSmallDocument: AvailableLogo?
                    var availableLoadBidQuotaions : AvailableLoadBidQuotaion?
                    var availableLoadBidQuotaionsType = [[String:Any]]()
                    var currencyMaster : CurrencyMaster?
                    var logo : Logo?
                    var availLoadType : AvailLoadType1?
                    
                    var bidArray = [Int]()
                    
                    for data in returnDict{
                        
                        var availableFleetBid : AvailableFleetBid?
                        var availableLoadBidQuotaionElement : AvailableLoadBidQuotaionElement?
                        var availableLoadCurrency: AvailableFleetBidCurrencyMaster?
                        var availableFleetBidCurrencyMaster: AvailableFleetBidCurrencyMaster?
                        var availFleetBidCurrencyType = [String:Any]()
                        var availableLoadBidQuotaionElementType = [String:Any]()
                        var availableLoadBidQuotaionsArray = [AvailableLoadBidQuotaion]()
                        var ownLastThreeBidsArray = [AvailableLoadBidQuotaion]()
                        var availableFleetBidType = [String:Any]()
                        var availableLoadType = [String:Any]()
                        resultNew = data["loadType"] as! [String : Any]
                        cargoTypeArray = data["cargoType"] as! [String : Any]
                        vehicleType = data["vehicleType"] as! [String:Any]
                        var ownLastThreeBidsType = [[String:Any]]()
                        var ownLastThreeBids : AvailableLoadBidQuotaion?
                        
                        
                        let loadType = resultNew["loadType"] as! String
                        let noOfVehicle = data["noOfVehicles"] as! Int
                        
                        print("noOfVehicle =\(noOfVehicle)")
                        print("bidCode =\(String(describing: data["bidCode"]))")
                        print("destinationLocation =\(String(describing: data["destinationLocation"]))")
                        loadTypeArray.append(loadType)
                        if self.loginUserType == UserStates.SERVICE_PROVIDER{
                            
                            if let returnObject =
                                data["availableLoadBidQuotaions"] as? [[String:Any]] {
                                availableLoadBidQuotaionsType = returnObject
                            }
                            if let returnObjectt =
                                data["ownLastThreeBids"] as? [[String:Any]] {
                                ownLastThreeBidsType = returnObjectt
                            }
                            
                            
                            
                            
                            if let availableLoad =
                                data["availableLoadBidQuotaion"] as? [String:Any] {
                                availableLoadBidQuotaionElementType = availableLoad
                            }
                            
                            
                            //                            availableLoadBidQuotaionElementType = data["availableLoadBidQuotaion"] as? [String:Any]
                            
                            availCurrencyType = availableLoadBidQuotaionElementType["currencyMaster"] as? [String:Any]
                            
                            if let fleetBidtype = availableLoadBidQuotaionElementType["availableFleetBid"] as? [String:Any]{
                                availableFleetBidType = fleetBidtype
                            }
                            
                            //                            availableFleetBidType = (availableLoadBidQuotaionElementType["availableFleetBid"] as? [String:Any])!
                            
                            if let loadTypee = availableFleetBidType["loadType"] as? [String:Any] {
                                availableLoadType = loadTypee
                            }
                            
                            if let cargoTy = availableFleetBidType["cargoType"] as? [String:Any] {
                                availCargoTypeArray = cargoTy
                            }
                            
                            if let capacityWeight = availableFleetBidType["capacityWeightUnit"] as? [String:Any] {
                                availCapacityWeightUnitArr = capacityWeight
                            }
                            if let availVechTypee = availableFleetBidType["vehicleType"] as? [String:Any] {
                                availVehicleType = availVechTypee
                            }
                            
                            //  availableLoadType = (availableFleetBidType["loadType"] as? [String:Any])!
                            //   availVehicleType = (availableFleetBidType["vehicleType"] as? [String:Any])!
                            
                            print("availVehicleType == \(availVehicleType)")
                            
                            //  availDocumentType = (availVehicleType["document"] as? [String:Any])!
                            
                            
                            if let smallDocType = availVehicleType["documentSmall"] as? [String:Any] {
                                availSmallDocType = smallDocType
                            }
                            if let fleetBid = availableFleetBidType["currencyMaster"] as? [String:Any] {
                                availFleetBidCurrencyType = fleetBid
                            }
                            
                            
                            //     availSmallDocType = (availVehicleType["documentSmall"] as? [String:Any])!
                            // (availVehicleType["documentSmall"] as? [String:Any])!
                            
                            // availFleetBidCurrencyType = (availableFleetBidType["currencyMaster"] as? [String:Any])!
                            
                            
                            //                            availableLoadBidQuotaionsType =
                            //                                data["availableLoadBidQuotaions"] as! [[String : Any]]
                            
                            
                            
                            resultNew = data["loadType"] as! [String : Any]
                            loadOwnerArr = data["loadOwner"] as! [String:Any]
                            
                            documentType = loadOwnerArr["logo"] as? [String:Any]
                            docuType = vehicleType["document"] as! [String:Any]
                            
                            logoType = loadOwnerArr["logo"] as? [String:Any]
                            companyAccessArr = loadOwnerArr["companyAccess"] as! [String:Any]
                            capacityWeightUnitArr = data["capacityWeightUnit"] as! [String:Any]
                            addressType = loadOwnerArr["address"] as? [String:Any]
                            
                            for availableBid in availableLoadBidQuotaionsType{
                                currencyMasterType = availableBid["currencyMaster"] as! [String : Any]
                                currencyMaster = CurrencyMaster(id: currencyMasterType["id"] as? Int, currencyCode: currencyMasterType["currencyCode"] as? String)
                                availableLoadBidQuotaions = AvailableLoadBidQuotaion(id: availableBid["id"] as! Int,                  quotationAmount:Int(truncating: availableBid["quotationAmount"] as! NSNumber),                         currencyMaster: currencyMaster!,
                                                                                     quotationEntryDate: availableBid["quotationEntryDate"] as! Int)
                                availableLoadBidQuotaionsArray.append(availableLoadBidQuotaions!)
                                
                            }
                            
                            for ownbid in ownLastThreeBidsType{
                                currencyMasterType = ownbid["currencyMaster"] as! [String : Any]
                                currencyMaster = CurrencyMaster(id: currencyMasterType["id"] as? Int, currencyCode: currencyMasterType["currencyCode"] as? String)
                                ownLastThreeBids = AvailableLoadBidQuotaion(id: ownbid["id"] as! Int,                  quotationAmount:Int(truncating: ownbid["quotationAmount"] as! NSNumber),currencyMaster: currencyMaster!, quotationEntryDate: ownbid["quotationEntryDate"] as! Int)
                                ownLastThreeBidsArray.append(ownLastThreeBids!)
                                
                            }
                            for bid in ownLastThreeBidsArray{
                                print("bid.quotationAmount) databidCode  " + "\(bid.quotationAmount) " + "\(String(describing: data["bidCode"]))")
                            }
                            
                            
                            
                            
                            loadType1 = LoadType1(id: resultNew["id"] as! Int, loadType: resultNew["loadType"] as! String, code: resultNew["code"] as! String)
                            
                            capacityWeight = CapacityWeightUnit(id: capacityWeightUnitArr["id"] as? Int, name: capacityWeightUnitArr["name"] as? String, type: capacityWeightUnitArr["type"] as? String)
                            
                            address = Address1(id: addressType?["id"] as? Int, address: addressType?["address"] as? String, landmark: addressType?["landmark"] as? String, city: addressType?["city"] as? String, state: addressType?["state"] as? String, pincode: addressType?["pincode"] as? String, country: addressType?["country"] as? String)
                            
                            logo = Logo(id: logoType?["id"] as? Int, mimeType: logoType?["mimeType"] as? String, type: (logoType?["type"] as? String))
                            companyAccess = CompanyAccess(id:companyAccessArr["id"] as! Int)
                            
                            loadOwner = LoadOwner(id: loadOwnerArr["id"] as! Int,
                                                  name: loadOwnerArr["name"] as! String,
                                                  registrationNo: loadOwnerArr["registrationNo"] as! String,
                                                  companyCode: loadOwnerArr["companyCode"] as! String,
                                                  telephone: loadOwnerArr["telephone"] as! String,
                                                  fax:loadOwnerArr["fax"] as! String,
                                                  address: address!,
                                                  logo: logo!,
                                                  gstTax: loadOwnerArr["gstTax"] as! Double,
                                                  gstTaxRegNo: loadOwnerArr["gstTaxRegNo"] as! String,
                                                  jurisdiction: loadOwnerArr["jurisdiction"] as! String,
                                                  timeZone: loadOwnerArr["timeZone"] as! String,
                                                  panNo: loadOwnerArr["panNo"] as! String,
                                                  vatNo: loadOwnerArr["vatNo"] as! String,
                                                  iecNo: loadOwnerArr["iecNo"] as! String,
                                                  cinNo: loadOwnerArr["cinNo"] as! String,
                                                  cstNo: loadOwnerArr["cstNo"] as! String,
                                                  registrationDate: loadOwnerArr["registrationDate"] as! Int,
                                                  subscriptionStartDate: loadOwnerArr["subscriptionStartDate"] as! Int,
                                                  subscriptionEndDate: loadOwnerArr["subscriptionEndDate"] as! Int,
                                                  currentBalance: loadOwnerArr["currentBalance"] as! Int,
                                                  smsNotification: loadOwnerArr["smsNotification"] as! String,
                                                  companyAccess: companyAccess!,status: loadOwnerArr["status"] as! String,
                                                  expiredType: loadOwnerArr["expiredType"] as! String,
                                                  activeUserCount: loadOwnerArr["activeUserCount"] as! Int,
                                                  maxUser: loadOwnerArr["maxUser"] as! Int,
                                                  maxUserType: loadOwnerArr["maxUserType"] as! String,
                                                  exchangeBookingCount: loadOwnerArr["exchangeBookingCount"] as! Int,
                                                  goCount: loadOwnerArr["goCount"] as! Int,
                                                  proCount: loadOwnerArr["proCount"] as! Int,
                                                  profileMailCount: loadOwnerArr["profileMailCount"] as! Int,
                                                  ewbFlagStr: loadOwnerArr["ewbFlagStr"] as! String,
                                                  planType: loadOwnerArr["planType"] as! String,
                                                  exSuwalletAmount: loadOwnerArr["exSuwalletAmount"] as! Int,
                                                  exSuhubMaxUser: loadOwnerArr["exSuhubMaxUser"] as! Int,
                                                  exSuallowTransaction: loadOwnerArr["exSuallowTransaction"] as! Int,
                                                  exSuremainsTr: loadOwnerArr["exSuremainsTr"] as! Int,
                                                  hubPlanID: loadOwnerArr["hubPlanId"] as! Int,
                                                  exSuhubTransactionCost: loadOwnerArr["exSuhubTransactionCost"] as! Int,
                                                  thirdPartyAuthDateTime: loadOwnerArr["thirdPartyAuthDateTime"] as? Int)
                            
                            cargoTypee = CargoType(id: cargoTypeArray["id"] as? Int,
                                                   cargoType: cargoTypeArray["cargoType"] as? String)
                            
                            document = Logo(id: documentType?["id"] as? Int,mimeType: documentType?["mimeType"] as? String,type: ((documentType?["type"] as? String)))
                            
                            docs = Logo(id: docuType["id"] as? Int, mimeType: docuType["mimeType"] as? String, type: ((docuType["type"] as? String)))
                            
                            
                            vehicleTypee = VehicleType(id: vehicleType["id"] as! Int,
                                                       name: vehicleType["name"] as! String,
                                                       sequence: Int(vehicleType["sequence"] as! Double),
                                                       container: vehicleType["container"] as! Bool,
                                                       type: vehicleType["type"] as! String,
                                                       document: docs!, documentSmall: document!,
                                                       vehicleOrder: vehicleType["vehicleOrder"] as! Int,
                                                       weightCapacity: vehicleType["weightCapacity"] as? Int,
                                                       documentID: vehicleType["documentId"] as? Int,
                                                       smallDocID: vehicleType["smallDocId"] as? Int)
                            currencyMaster = CurrencyMaster(id: currencyMasterType["id"] as? Int, currencyCode: currencyMasterType["currencyCode"] as? String)
                            
                            availDocument = AvailableLogo(id: availDocumentType["id"] as? Int,
                                                          mimeType: availDocumentType["mimeType"] as? String,
                                                          type: availDocumentType["type"] as? String)
                            
                            
                            availSmallDocument = AvailableLogo(id: availSmallDocType["id"] as? Int,mimeType: availSmallDocType["mimeType"] as? String,
                                                               type: availSmallDocType["type"] as? String)
                            print("availSmallDocument === \(String(describing: availSmallDocument?.id))")
                            
                            availLoadType = AvailLoadType1(id: availableLoadType["id"] as? Int, loadType: availableLoadType["loadType"] as? String, code: availableLoadType["code"] as? String)
                            
                            availableVehicleTypee = AvailableVehicleType(id: availVehicleType["id"] as? Int,
                                                                         name: availVehicleType["name"] as? String,
                                                                         sequence: availVehicleType["sequence"] as? Double,
                                                                         container: availVehicleType["container"] as? Bool,
                                                                         type: availVehicleType["type"] as? String,
                                                                         document: availDocument!, documentSmall: availSmallDocument!,
                                                                         vehicleOrder: availVehicleType["vehicleOrder"] as? Int,
                                                                         weightCapacity: availVehicleType["weightCapacity"] as? Int,
                                                                         documentID: availVehicleType["documentId"] as? Int,
                                                                         smallDocID: availVehicleType["smallDocId"] as? Int)
                            
                            
                            availableLoadCurrency = AvailableFleetBidCurrencyMaster(id: availCurrencyType?["id"] as? Int, currencyName: availCurrencyType?["currencyName"] as? String, currencyCountry: availCurrencyType?["currencyCountry"] as? String, currencyCode: availCurrencyType?["currencyCode"] as? String)
                            
                            availableFleetBidCurrencyMaster = AvailableFleetBidCurrencyMaster(id: availFleetBidCurrencyType["id"] as? Int, currencyName: availFleetBidCurrencyType["currencyName"] as? String, currencyCountry: availFleetBidCurrencyType["currencyCountry"] as? String, currencyCode: availFleetBidCurrencyType["currencyCode"] as? String)
                            
                            availCargoTypee = CargoType(id: (availCargoTypeArray["id"] as? Int),
                                                        cargoType: availCargoTypeArray["cargoType"] as? String)
                            
                            availCapacityWeight = CapacityWeightUnit(id: availCapacityWeightUnitArr["id"] as? Int, name: availCapacityWeightUnitArr["name"] as? String, type: availCapacityWeightUnitArr["type"] as? String)
                            
                            
                            availableFleetBid = AvailableFleetBid(id: availableFleetBidType["id"] as? Int, bidCode: availableFleetBidType["bidCode"] as? String, creationDate: availableFleetBidType["creationDate"] as? Int, closedBidTime: availableFleetBidType["closedBidTime"] as? Int, availableLocation: availableFleetBidType["availableLocation"] as? String, destinationLocation: availableFleetBidType["destinationLocation"] as? String, availableLatitude: availableFleetBidType["availableLatitude"] as? Double, availableLongitude: availableFleetBidType["availableLongitude"] as? Double, destinationLatitude: availableFleetBidType["destinationLatitude"] as? Double, destinationLongitude: availableFleetBidType["destinationLongitude"] as? Double, vehicleType: availableVehicleTypee!, noOfVehicles: availableFleetBidType["noOfVehicles"] as? Int, availableDateTime: availableFleetBidType["availableDateTime"] as? Int,capacityWeight: availableFleetBidType["capacityWeight"] as? Int,loadType: availLoadType!, expectedFreight: availableFleetBidType["expectedFreight"] as? Int, currencyMaster: availableFleetBidCurrencyMaster!, status: availableFleetBidType["status"] as? String, cargoType: availCargoTypee!, capacityWeightUnit: availCapacityWeight!)
                            
                            availableLoadBidQuotaionElement = AvailableLoadBidQuotaionElement(id: availableLoadBidQuotaionElementType["id"] as? Int, quotationAmount: availableLoadBidQuotaionElementType["quotationAmount"] as? Int, currencyMaster: availableLoadCurrency!, validDateTime: availableLoadBidQuotaionElementType["validDateTime"] as? Int, quotationEntryDate: availableLoadBidQuotaionElementType["quotationEntryDate"] as? Int, availableFleetBid: availableFleetBid!)
                            
                            
                            
                            
                            let datum = Datum(id: data["id"] as! Int,
                                              bidCode: data["bidCode"] as! String,
                                              creationDate: data["creationDate"] as! Int,
                                              closedBidTime: data["closedBidTime"] as! Int,
                                              availableLocation: data["availableLocation"] as! String,
                                              destinationLocation: data["destinationLocation"] as! String,
                                              vehicleType: vehicleTypee!,
                                              noOfVehicles: data["noOfVehicles"] as! Int,
                                              availableDateTime: data["availableDateTime"] as! Int,
                                              cargoType: cargoTypee!,
                                              loadOwner: loadOwner!,
                                              loadType: loadType1!,
                                              capacityWeight: data["capacityWeight"] as? Int,
                                              capacityWeightUnit: capacityWeight!,
                                              availableLatitude: data["availableLatitude"] as? Double,
                                              availableLongitude: data["availableLongitude"] as? Double,
                                              destinationLatitude: data["destinationLatitude"] as? Double,
                                              destinationLongitude: data["destinationLongitude"] as? Double,
                                              bidQuotationCount: data["bidQuotationCount"] as! Int,
                                              bidRank: data["bidRank"] as? Int,
                                              status: data["status"] as! String,
                                              count:
                                data["count"] as! Int?,
                                              availableLoadBidQuotaions: availableLoadBidQuotaionsArray, ownLastThreeBids: ownLastThreeBidsArray,
                                              packgs: data["packgs"] as? Int,
                                              loadOwnerID: data["loadOwnerId"] as? Int,
                                              vehicleTypeMaster: data["vehicleTypeMaster"] as! String, availableLoadBidQuotaionElement: availableLoadBidQuotaionElement)
                            datumArray.append(datum)
                            
                            if self.loadDatumArray.contains(where: { bid in bid.bidCode == datum.bidCode }) {
                                print(" exists in the array")
                            } else {
                                print(" does not exists in the array")
                                self.loadDatumArray.append(datum)
                                
                            }
                            
                            
                            loadDetails = Load(status: true, data: datumArray)
                            
                            self.loadArray.append(loadDetails!)
                            
                            
                        }
                        
                        for i in ownLastThreeBidsArray{
                            bidArray.append(i.quotationAmount)
                            
                        }
                        
                        if availableLoadBidQuotaionElementType["id"] as? Int != nil{
                            //    self.isQuoted = true
                        }else{
                            //  self.isQuoted = false
                        }
                        print(" availableLoadBidQuotaionElementType[] as? Int  === \( String(describing: availableLoadBidQuotaionElementType["id"] as? Int))")
                        
                        
                        
                    }
                    print("self.loadArray.count ==== \(self.loadArray.count)")
                    self.matchLoadCollectionView.reloadData()
                    
                }
                self.matchingLoadCount.text = "\(self.loadDatumArray.count)"
                break
            case .failure(let error):
                self.activityIndicatorEnd()
                print("POST REQUEST ERROR  - \(error.localizedDescription)")
                print("=== REQUEST INFORMATION ===")
                //                    print("Status Code: \(response.response!.statusCode)")
                print("Request Payload: \(parameters)")
                print("===")
                break
            }
        }
        
        
    }
    
    
    func fleetPosted(){
        
        var bidlist : GetBidList?
        status = "OPEN"
        if isDragging == false{
            self.activityIndicatorBegin()
        }
        
        
        
        
        bidlist = GetBidList(pageSize:"20",pageNo:"\(self.pageNo)",recordType:"",search:"",fromCity:"",fromState:"",toState:"",toCity:"",vehicleType:"",loadType:"",noOfVehicle:"",fromDate:"",toDate:"",currentLatitude:"",currentLongitude:"",km: "",financialYear: self.currentFinancialYear!,cargoType: "")
        
        
        
        
        let jsonDataa = try! JSONEncoder().encode(bidlist)
        let type = String(data: jsonDataa, encoding: .utf8)!
        do {
            self.getBidListPara = try self.convertToDictionary(from: type)
            print("listt = \(self.getBidListPara)")
        } catch {
            print(error)
        }
        
        let parameters : [String:Any] = getBidListPara
        
        let headers = header()
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
        
        Alamofire.request(URLStatics.fleetPosted + "\(self.status)" , method: .get, parameters: parameters,headers: headers)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                
                
                switch (response.result) {
                    
                case .success:
                    
                    if self.isDragging == false{
                        self.activityIndicatorEnd()
                    }
                    
                    if let reply = response.result.value {
                        print("======= JSON: Fleet Posted ========  \(reply)")
                        let mainResponse = JSON(reply)
                        
                        
                        var returnDict = [[String:Any]]()
                        // var datumArray = [FleetDatum]()
                        var datum : FleetDatum?
                        var vehicleType = [String:Any]()
                        var smallDocumentType = [String:Any]()
                        var documentType = [String:Any]()
                        var document : FleetLogo?
                        var smallDocument : FleetLogo?
                        var cargoType : FleetCargoType?
                        var cargo = [String:Any]()
                        var capacityWt = [String:Any]()
                        var capacityWtUnit : FleetCapacityWeightUnit?
                        var loadType = [String:Any]()
                        var fleetLoadType : FleetLoadType?
                        var currencyMaster = [String:Any]()
                        var fleetCurrencyMaster : FleetCurrencyMaster
                        
                        var fleetVehicleType : FleetVehicleType?
                        if let returnObj = mainResponse["data"].arrayObject as? [[String:Any]] {
                            returnDict = returnObj
                        }
                        
                        for data in returnDict{
                            vehicleType = data["vehicleType"] as! [String:Any]
                            smallDocumentType = vehicleType["documentSmall"] as! [String:Any]
                            documentType = vehicleType["document"] as! [String:Any]
                            cargo = data["cargoType"] as! [String:Any]
                            capacityWt = data["capacityWeightUnit"] as! [String:Any]
                            loadType = data["loadType"] as! [String:Any]
                            currencyMaster = data["currencyMaster"] as! [String:Any]
                            
                            document = FleetLogo(id: documentType["id"] as! Int, mimeType: documentType["mimeType"] as! String, type: documentType["type"] as! String)
                            
                            smallDocument = FleetLogo(id: smallDocumentType["id"] as! Int, mimeType: smallDocumentType["mimeType"] as! String, type: smallDocumentType["type"] as! String)
                            
                            fleetVehicleType = FleetVehicleType(id: Int(truncating: vehicleType["id"] as! NSNumber), name: vehicleType["name"] as! String, sequence: Int(truncating: vehicleType["sequence"] as! NSNumber), container: vehicleType["container"] as! Bool, type: vehicleType["type"] as! String, document: document!, documentSmall: smallDocument!, vehicleOrder: Int(truncating: vehicleType["vehicleOrder"] as! NSNumber), documentID: Int(truncating: vehicleType["documentId"] as! NSNumber), smallDocID: Int(truncating: vehicleType["smallDocId"] as! NSNumber))
                            
                            fleetLoadType = FleetLoadType(id: Int(truncating: loadType["id"] as! NSNumber), loadType: loadType["loadType"] as! String, code: loadType["code"] as! String)
                            
                            cargoType = FleetCargoType(id: cargo["id"] as! Int, cargoType: cargo["cargoType"] as! String)
                            
                            capacityWtUnit = FleetCapacityWeightUnit(id: capacityWt["id"] as! Int, name: capacityWt["name"] as! String, type: capacityWt["type"] as! String)
                            
                            fleetCurrencyMaster = FleetCurrencyMaster(id: currencyMaster["id"] as! Int, currencyName: currencyMaster["currencyName"] as! String, currencyCountry: currencyMaster["currencyCountry"] as! String, currencyCode: currencyMaster["currencyCode"] as! String)
                            
                            datum = FleetDatum(id: data["id"] as! Int,
                                               bidCode: data["bidCode"] as! String,
                                               creationDate: data["creationDate"] as! Int,
                                               closedBidTime: data["closedBidTime"] as! Int,
                                               availableLocation: data["availableLocation"] as! String,
                                               destinationLocation: data["destinationLocation"] as? String,
                                               availableLatitude: data["availableLatitude"] as? Double,
                                               availableLongitude: data["availableLongitude"] as? Double,
                                               destinationLatitude: data["destinationLatitude"] as? Double,
                                               destinationLongitude: data["destinationLongitude"] as? Double,
                                               count: data["count"] as? Int,
                                               vehicleType: fleetVehicleType!,
                                               noOfVehicles: data["noOfVehicles"] as! Int,
                                               availableDateTime: data["availableDateTime"] as! Int,
                                               cargoType: cargoType!,
                                               capacityWeight: data["capacityWeight"] as! Int,
                                               capacityWeightUnit: capacityWtUnit!,
                                               loadType: fleetLoadType!,
                                               expectedFreight: data["expectedFreight"] as? NSNumber,
                                               currencyMaster: fleetCurrencyMaster,
                                               isInvited: data["isInvited"] as! Bool,
                                               bidInviteCount: data["bidInviteCount"] as! Int,
                                               vehicleTypeMaster: data["vehicleTypeMaster"] as? String,
                                               loadBidCode: data["loadBidCode"] as? String,
                                               registrationNo: data["registrationNo"] as? String,
                                               comments: data["comments"] as? String, sequence: data["sequence"] as? Int, postType: data["postType"] as? String)
                            
                            if self.datumArray.contains(where: { bid in bid.bidCode == datum?.bidCode }) {
                                print(" exists in the array")
                            } else {
                                print(" does not exists in the array")
                                self.datumArray.append(datum!)
                            }
                            
                            
                        }
                        print( "datumArray.count ====== \(self.datumArray.count)")
                        
                        self.fleetPostedCollectionView.reloadData()
                    }
                    self.fleetPOstedCount.text = "\(self.datumArray.count)"
                    break
                case .failure(let error):
                    
                    print("error \(error.localizedDescription)")
                    break
                }
        }
        
    }
    
    
    var polyline = GMSPolyline()
    var animationPolyline = GMSPolyline()
    var path = GMSPath()
    var animationPath = GMSMutablePath()
    var i: UInt = 0
    var timer: Timer!
    
    
    func drawPath(origin:String,destination:String){
                let originn = origin
                let destinationn = destination
        self.mapView.clear()
        
//        let originn = "VimanNagar"
//        let destinationn = "LohagaonPune"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(originn)&destination=\(destinationn)&mode=driving&key=\(URLStatics.googleApiKey)"
        print("url == \(url)")
        Alamofire.request(url).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            print(response.data)
            print(" server data") // server data
            print(response.result.value)   // result of response serialization
            
            let json = JSON(response.result.value!)
            let routes = json["routes"].arrayValue
            if routes.count != 0{
                let legs = routes[0]["legs"].arrayValue
                for leg in legs{
                    
                    print("location  === \(leg["end_location"].dictionaryValue)")
                    let endLocationDict = leg["end_location"].dictionaryValue
                    let distanceDict = leg["distance"].dictionaryValue
                    let durationDict = leg["duration"].dictionaryValue
                    
                    let endLat = endLocationDict["lat"]?.doubleValue
                    let endLng = endLocationDict["lng"]?.doubleValue
                    let startLocationDict = leg["start_location"].dictionaryValue
                    let startLat = startLocationDict["lat"]?.doubleValue
                    let startLng = startLocationDict["lng"]?.doubleValue
                    self.startLat = startLat!
                    self.startLng = startLng!
                    self.endLat = endLat!
                    self.endLng = endLng!
                    
                    let startPosition = CLLocationCoordinate2D(latitude: startLat!,longitude: startLng!)
                    let startMarker = GMSMarker(position: startPosition)
                    
//                    startMarker.icon = UIImage(named: "sourceMarker")
//                    startMarker.setIconSize(scaledToSize: .init(width: self.mapView.frame.width/20, height: self.mapView.frame.width/14))
                    startMarker.snippet = origin
                    startMarker.appearAnimation = .pop
                    self.pulseAnimation(position: startPosition)
                   startMarker.iconView = UIView(frame: CGRect(x: 0, y: 0, width:  self.mapView.frame.width/20, height:  self.mapView.frame.width/14))
                                        let imageView =  UIImageView(frame: CGRect(x: 0, y: 0, width: self.mapView.frame.width/20, height: self.mapView.frame.width/14))
                                        imageView.contentMode = .scaleAspectFit
                                        imageView.image = UIImage(named: "sourceMarker")
                                        startMarker.iconView?.addSubview(imageView)
                                 //     imageView.layer.anchorPoint = CGPoint(x: 0.5, y: 1.0) //we need adjust anchor point to achieve the desired behavior
                                        imageView.layer.frame = CGRect(x: 0, y: 0, width: self.mapView.frame.width/20, height: self.mapView.frame.width/14) //we need adjust the layer frame

                    let origin:CGPoint = imageView.center
                                      let target:CGPoint = CGPoint(x: imageView.center.x, y: imageView.center.y+5)
                                      let bounce = CABasicAnimation(keyPath: "position.y")
                                      bounce.duration = 0.5
                                      bounce.fromValue = target.y
                                      bounce.toValue = origin.y
                                      bounce.repeatCount = 2
                                      bounce.autoreverses = true
                                      imageView.layer.add(bounce, forKey: "position")
                                        
                              
                             startMarker.map = self.mapView
                               
                    
//                    var circle = GMSCircle(position: startPosition, radius: 50)
//                    circle.strokeColor = UIColor.blue
//                    circle.fillColor = UIColor.blue
//
//                    circle.map = self.mapView
                    
                    self.mapView.camera = GMSCameraPosition.camera(withLatitude: startLat!, longitude: startLng!, zoom: 9)
                    CATransaction.begin()
                    CATransaction.setValue(2.0, forKey: kCATransactionAnimationDuration)
                    let city = GMSCameraPosition.camera(withLatitude: startLat!,longitude: startLng!, zoom: 14)
                    self.mapView.animate(to: city)
                    CATransaction.commit()
 
                    let endPosition = CLLocationCoordinate2D(latitude: endLat!,longitude: endLng!)
                    let endMarker = GMSMarker(position: endPosition)
                    
                    
//                    endMarker.appearAnimation = .pop
//
//                    endMarker.icon = UIImage(named: "destinationMarker")
//                    endMarker.setIconSize(scaledToSize: .init(width: self.mapView.frame.width/20, height: self.mapView.frame.width/14))
                    endMarker.snippet = destination
                    self.pulseAnimation(position: endPosition)
                    endMarker.iconView = UIView(frame: CGRect(x: 0, y: 0, width:  self.mapView.frame.width/20, height:  self.mapView.frame.width/14))
                                       let imageView1 = UIImageView(frame: CGRect(x: 0, y: 0, width: self.mapView.frame.width/20, height: self.mapView.frame.width/14))
                                       imageView1.contentMode = .scaleAspectFit
                                       imageView1.image = UIImage(named: "destinationMarker")
                                       endMarker.iconView?.addSubview(imageView1)
                                //     imageView.layer.anchorPoint = CGPoint(x: 0.5, y: 1.0) //we need adjust anchor point to achieve the desired behavior
                                       imageView1.layer.frame = CGRect(x: 0, y: 0, width: self.mapView.frame.width/20, height: self.mapView.frame.width/14) //we need adjust the layer frame

                    let origin1:CGPoint = imageView1.center
                                     let target1:CGPoint = CGPoint(x: imageView1.center.x, y: imageView1.center.y+5)
                                     let bounce1 = CABasicAnimation(keyPath: "position.y")
                                     bounce1.duration = 0.5
                                     bounce1.fromValue = target1.y
                                     bounce1.toValue = origin1.y
                                     bounce1.repeatCount = 2
                                     bounce1.autoreverses = true
                                     imageView1.layer.add(bounce1, forKey: "position")
                                 endMarker.map = self.mapView
                              
                  

                    
                }
                for route in routes
                {
                    let routeOverviewPolyline = route["overview_polyline"].dictionary
                    
                    let points = routeOverviewPolyline?["points"]?.stringValue
                    
                    self.path = GMSPath.init(fromEncodedPath: points!)!
                    self.polyline.path = self.path
                    self.polyline.strokeColor = UIColor(hexString: ColorConstants.DARK_BLUE)
                    self.polyline.strokeWidth = 2.0
                               self.polyline.map = self.mapView
                    
                    self.timer = Timer.scheduledTimer(timeInterval: 0.005, target: self, selector: #selector(self.animatePolylinePath), userInfo: nil, repeats: true)
                  //  self.mapView.addPath(path!, strokeColor: .black, strokeWidth: 2, geodesic: nil, spans: nil)
                    let bounds = GMSCoordinateBounds(path: self.path)
                    self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
                    
                    //      self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.animatePolylinePath), userInfo: nil, repeats: true)
                }
            }
            
        }
    }
    
    
    @objc func animatePolylinePath() {
        if (self.i < self.path.count()) {
            
                 self.animationPath.add(self.path.coordinate(at: self.i))
                                                         self.animationPolyline.path = self.animationPath
                                                         self.animationPolyline.strokeColor = UIColor(hexString: ColorConstants.LIGHT_GRAY)
                                                         self.animationPolyline.strokeWidth = 2
                                                         self.animationPolyline.map = self.mapView
                                                        self.i += 1
  
        }
        else {
            self.i = 0
            self.animationPath = GMSMutablePath()
            self.animationPolyline.map = nil
            
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem!, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    }
}


class LeftAlignedFlowLayout: UICollectionViewFlowLayout {
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let attributes = super.layoutAttributesForElements(in: rect)
        
        var leftMargin = sectionInset.left
        
        attributes?.forEach { layoutAttribute in
            if layoutAttribute.frame.origin.x == sectionInset.left {
                leftMargin = sectionInset.left
            }
            else {
                layoutAttribute.frame.origin.x = leftMargin
            }
            
            leftMargin += layoutAttribute.frame.width + minimumInteritemSpacing
        }
        
        return attributes
    }
}

