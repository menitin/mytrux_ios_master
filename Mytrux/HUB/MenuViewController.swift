//
//  MenuViewController.swift
//  Mytrux
//
//  Created by Aboli on 05/06/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class MenuViewController: BaseViewController {
    @IBOutlet weak var hubImage: UIImageView!
    @IBOutlet weak var erpImage: UIImageView!
    @IBOutlet weak var profilePicView: UIView!
    @IBOutlet weak var profilePhoto: UIImageView!
    @IBOutlet weak var hubView: UIView!
    @IBOutlet weak var erpView: UIView!
    @IBOutlet weak var walletView: UIView!
    @IBOutlet weak var mainView: UIView!
  //  @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var exchangeView: UIView!
    @IBOutlet weak var postLoadView: UIView!
    @IBOutlet weak var bookingView: UIView!
    @IBOutlet weak var loadsPostedView: UIView!
    @IBOutlet weak var spNotificationView: UIView!
    @IBOutlet weak var fleetsInvitedView: UIView!
    @IBOutlet weak var suNotificationView: UIView!
    @IBOutlet weak var financialYearView: UIView!
    
    @IBOutlet weak var customerView: UIView!
    @IBOutlet weak var legalView: UIView!
    @IBOutlet weak var logoutView: UIView!
    @IBOutlet weak var spMenu: UIView!
    @IBOutlet weak var suMenu: UIView!
    @IBOutlet weak var suExchangeView: UIView!
    @IBOutlet weak var suBookingView: UIView!
    @IBOutlet weak var suPostLoadView: UIView!
    @IBOutlet weak var suLoadsPOsted: UIView!
    @IBOutlet weak var suFleetInvited: UIView!
    @IBOutlet weak var suFinancialYr: UIView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var suLegalView: UIView!
    @IBOutlet weak var suLogoutView: UIView!
    @IBOutlet weak var exchangeImageView: UIImageView!
    @IBOutlet weak var bookingImageView: UIImageView!
    @IBOutlet weak var fleetPostedImageView: UIImageView!
    @IBOutlet weak var financialYearImageView: UIImageView!
    @IBOutlet weak var notificationImageView: UIImageView!
    
    @IBOutlet weak var walletImageView: UIImageView!
    @IBOutlet weak var customerImageView: UIImageView!
    @IBOutlet weak var loadsQuotedImageView: UIImageView!
    @IBOutlet weak var postFleetImageView: UIImageView!
    @IBOutlet weak var logoutImageView: UIImageView!
    
    @IBOutlet weak var legalImageView: UIImageView!
   
    var viewArr = [UIView]()
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.tableView.delegate = self
//        self.tableView.dataSource = self
     viewArr = [exchangeView,bookingView,postLoadView,loadsPostedView,fleetsInvitedView,financialYearView,legalView,logoutView,walletView,suExchangeView,suBookingView,suPostLoadView,suLoadsPOsted,suFleetInvited,suFinancialYr,suLegalView,suLogoutView,suNotificationView,spNotificationView,customerView]
        self.cardViewArray(arrView: viewArr)
        erpImage.image = UIImage(named: "erpMenuImage")
        erpImage.image = erpImage.image?.withRenderingMode(.alwaysTemplate)
        erpImage.tintColor = UIColor.white
        hubImage.image = UIImage(named: "hubMenuImage")
        hubImage.image = hubImage.image?.withRenderingMode(.alwaysTemplate)
        hubImage.tintColor = UIColor.white
        loginUserType = UserDefaults.standard.value(forKey: UserDefaultsContants.LOGIN_USER_TYPE)! as! String
        print("loginUserType == \(String(describing: loginUserType))")
        
        
        // Do any additional setup after loading the view.
    }
   
    override func viewWillAppear(_ animated: Bool) {
        if loginUserType == UserStates.SERVICE_USER{
            spMenu.isHidden = true
            suMenu.isHidden = false
            self.profileName.text = "SU"
        }else if loginUserType == UserStates.SERVICE_PROVIDER{
            spMenu.isHidden = false
            suMenu.isHidden = true
            self.profileName.text = "SP"
        }
    }
    
    func circularrView(viewArray: [UIView]){
        for view in viewArray{
            view.layoutIfNeeded()
            view.layer.cornerRadius = view.frame.size.height / 2
            view.layer.masksToBounds = true
            view.clipsToBounds = true
        }
    }

  // SP MENU //
    
    @IBAction func loadsQuotedClicked(_ sender: Any) {
         NotificationCenter.default.post(name: .LoadsQuotedClicked, object: nil)
    }
    
    @IBAction func spNotificationClicked(_ sender: Any) {
         NotificationCenter.default.post(name: .spNotificationClicked, object: nil)
    }
    
    @IBAction func suNotificationClicked(_ sender: Any) {
         NotificationCenter.default.post(name: .suNotificationClicked, object: nil)
    }
    
    @IBAction func spCustomerViewClicked(_ sender: Any) {
         NotificationCenter.default.post(name: .spCustomerViewClicked, object: nil)
    }
    @IBAction func fleetsPostedClicked(_ sender: Any) {
       NotificationCenter.default.post(name: .fleetsPostedClicked, object: nil)
    }
    
    @IBAction func spLogoutClicked(_ sender: Any) {
        let alertController = UIAlertController(title: "Logout", message: "Do You Want to Logout?", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
    @IBAction func bookingClicked(_ sender: Any) {
        NotificationCenter.default.post(name: .bookingClicked, object: nil)
    }
    @IBAction func exchangeClicked(_ sender: Any) {
         NotificationCenter.default.post(name: .exchangeClicked, object: nil)
    }
    @IBAction func postFleetClicked(_ sender: Any) {
        NotificationCenter.default.post(name: .postFleetClicked, object: nil)
        
        
    }
    @IBAction func walletClicked(_ sender: Any) {
          NotificationCenter.default.post(name: .walletClicked, object: nil)
    }
    
    @IBAction func financialYear(_ sender: Any) {
                NotificationCenter.default.post(name: .financialYear, object: nil)
        
    }
    
    @IBAction func legalClicked(_ sender: Any) {
        NotificationCenter.default.post(name: .legalClicked, object: nil)
    }
    
    @IBAction func suExchangeClicked(_ sender: Any) {
        NotificationCenter.default.post(name: .exchangeClicked, object: nil)
    }
    @IBAction func suBookingClicked(_ sender: Any) {
          NotificationCenter.default.post(name: .bookingClicked, object: nil)
    }
    
    @IBAction func suPostLoad(_ sender: Any) {
        NotificationCenter.default.post(name: .postFleetClicked, object: nil)
    }
    
    @IBAction func suLoadsPosted(_ sender: Any) {
            NotificationCenter.default.post(name: .fleetsPostedClicked, object: nil)
    }
    @IBAction func suFleetsInvited(_ sender: Any) {
         NotificationCenter.default.post(name: .LoadsQuotedClicked, object: nil)
    }
    @IBAction func suFinancialYr(_ sender: Any) {
            NotificationCenter.default.post(name: .financialYear, object: nil)
    }
    @IBAction func suLegal(_ sender: Any) {
          NotificationCenter.default.post(name: .legalClicked, object: nil)
    }
    @IBAction func suLogout(_ sender: Any) {
        let alertController = UIAlertController(title: "Logout", message: "Do You Want to Logout?", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    // SU MENU //
    override func viewDidLayoutSubviews() {
        circularrView(viewArray: [hubView,erpView,profilePicView])
    }
    
    func circleFilledWithOutline(circleView: UIView, fillColor: UIColor, outlineColor:UIColor) {
        let circleLayer = CAShapeLayer()
        let width = Double(circleView.bounds.size.width);
        let height = Double(circleView.bounds.size.height);
        circleLayer.bounds = CGRect(x: 2.0, y: 2.0, width: width-2.0, height: height-2.0)
        circleLayer.position = CGPoint(x: width/2.3, y: height/2.3);
        let rect = CGRect(x: 2.0, y: 2.0, width: width-2.0, height: height-2.0)
        let path = UIBezierPath.init(ovalIn: rect)
        circleLayer.path = path.cgPath
        circleLayer.fillColor = fillColor.cgColor
        circleLayer.strokeColor = outlineColor.cgColor
        circleLayer.lineWidth = 2.0
        circleView.layer.addSublayer(circleLayer)
    }


//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 8
//    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 50
//
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell") as! MenuCell
//        switch indexPath.row {
//        case 0:
//            cell.menuLabel.text = "Bookings"
//            cell.menuImage.image = UIImage(named: "loadsQuoted")
//            break
//        case 1:
//            cell.menuLabel.text = "Post Fleet"
//            cell.menuImage.image = UIImage(named: "postFleet")
//            break
//        case 2:
//            cell.menuLabel.text = "Fleets Posted"
//            cell.menuImage.image = UIImage(named: "fleetPost")
//            break
//        case 3:
//            cell.menuLabel.text = "Loads Quoted"
//            cell.menuImage.image = UIImage(named: "loadsQuoted")
//            break
//        case 4:
//            cell.menuLabel.text = "Wallet"
//            cell.menuImage.image = UIImage(named: "loadsQuoted")
//            break
//        case 5:
//            cell.menuLabel.text = "Financial Year"
//            cell.menuImage.image = UIImage(named: "loadsQuoted")
//            break
//        case 6:
//            cell.menuLabel.text = "Legal"
//            cell.menuImage.image = UIImage(named: "legal")
//            break
//        case 7:
//            cell.menuLabel.text = "Logout"
//            cell.menuImage.image = UIImage(named: "logout")
//            break
//        default:
//            cell.menuLabel.text = ""
//        }
//        return cell
//    }
//
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UIView {
    func makeCircular() {
      
        self.layer.cornerRadius = min(self.bounds.size.height, self.bounds.size.width) / 2.0
        self.clipsToBounds = true
    }
}
