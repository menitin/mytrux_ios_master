//
//  FleetInvitedTableViewCell.swift
//  Mytrux
//
//  Created by Mytrux on 22/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class FleetInvitedTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var contentVIew: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var acceptedImage: UIImageView!
    @IBOutlet weak var rejectedImage: UIImageView!
    @IBOutlet weak var callImage: UIImageView!
    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var vehicleType: UILabel!
    @IBOutlet weak var from: UILabel!
    @IBOutlet weak var to: UILabel!
    @IBOutlet weak var loadType: UILabel!
    @IBOutlet weak var cargoType: UILabel!
    @IBOutlet weak var noOfVehicles: UILabel!
    @IBOutlet weak var cargoWt: UILabel!
    @IBOutlet weak var availableFromDate: UILabel!
    @IBOutlet weak var availableFromTime: UILabel!
    @IBOutlet weak var contactPerson: UILabel!
    @IBOutlet weak var mobileNo: UILabel!
    @IBOutlet weak var emailId: UILabel!
    @IBOutlet weak var callView: UIView!
    @IBOutlet weak var acceptView: UIView!
    @IBOutlet weak var rejectView: UIView!
    @IBOutlet weak var acceptLabel: UILabel!
    @IBOutlet weak var acceptInstructionView: UIView!
    @IBOutlet weak var callViewAccept: UIView!
    @IBOutlet weak var callAcceptImageView: UIImageView!
    @IBOutlet weak var acceptInstructionLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.layer.cornerRadius = 5
        mainView.layer.shadowColor = UIColor.gray.cgColor
        mainView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        mainView.layer.shadowRadius = 5
        mainView.layer.shadowOpacity = 0.4
        view1.layer.cornerRadius = 5
        view4.layer.cornerRadius = 5
        acceptInstructionView.layer.cornerRadius = 5
        imageTint(imageView: acceptedImage, imageName: "accepted")
        imageTint(imageView: callImage, imageName: "call")
      imageTint(imageView: rejectedImage, imageName: "rejected")
         imageTint(imageView: callAcceptImageView, imageName: "call")
        callViewAccept.roundCorners(corners: .bottomLeft, radius: 5)
      
        
        // Initialization code
    }

    func imageTint (imageView : UIImageView, imageName: String) {
        imageView.image = UIImage(named: imageName)
        imageView.image = imageView.image?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor(hexString: ColorConstants.GREEN)
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

