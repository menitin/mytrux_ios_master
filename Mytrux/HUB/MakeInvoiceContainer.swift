//
//  MakeInvoiceContainer.swift
//  Mytrux
//
//  Created by Aboli on 18/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SideMenuSwift
import CoreImage

class MakeInvoiceContainer: BaseViewController,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource{
    
    var array = [Int]()
    
    //1 Step
    @IBOutlet weak var billingPartyLabel: UILabel!
    @IBOutlet weak var invoiceNo: UILabel!
    @IBOutlet weak var invoiceDate: UILabel!
    @IBOutlet weak var billNoView: UIView!
    @IBOutlet weak var nameAddressLabel: UILabel!
    @IBOutlet weak var gstinLabel: UILabel!
    @IBOutlet weak var invoiceMobileNo: UILabel!
    @IBOutlet weak var bookingCustomerNameAdd: UILabel!
    @IBOutlet weak var bookingCustomerGSTIn: UILabel!
    @IBOutlet weak var bookingCustomerMobileNo: UILabel!
    @IBOutlet weak var rateTF: UITextField!
    
    //2 Step
    
    @IBOutlet weak var billingHeadCheckView: UIImageView!
    @IBOutlet weak var selectBillingHeadLabel: UIView!
    @IBOutlet weak var unitTextField: UITextField!
    @IBOutlet weak var selectBillingHead: UILabel!
    @IBOutlet weak var sacCodeTextField: UITextField!
    @IBOutlet weak var transpoterNameAddress: UILabel!
    @IBOutlet weak var transpoterGSTNo: UILabel!
    @IBOutlet weak var transpoterCurrency: UILabel!
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet weak var taxView: UIView!
    @IBOutlet weak var amountInWordsLabel: UILabel!
    @IBOutlet weak var fleetId: UILabel!
    @IBOutlet weak var loadId: UILabel!
    @IBOutlet weak var vehicleNo: UILabel!
    @IBOutlet weak var bookingNo: UILabel!
    @IBOutlet weak var driverName: UILabel!
    @IBOutlet weak var mobileNo: UILabel!
    var selectedTags = [Int]()
    var isTaxable = Bool()
    @IBOutlet weak var subtotalLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var igstLabel: UILabel!
    @IBOutlet weak var sgstLabel: UILabel!
    @IBOutlet weak var cgstLabel: UILabel!
    
    
    // step 3
    
    @IBOutlet weak var billingPreviewTableView: UITableView!
    @IBOutlet weak var billingPreviewInvoiceNo: UILabel!
    @IBOutlet weak var billingPreviewInvoiceDate: UILabel!
    @IBOutlet weak var billingpreviewInvoiceType: UILabel!
    @IBOutlet weak var billingPreNameAddress: UILabel!
    @IBOutlet weak var billingPreInvoiceGSTIN: UILabel!
    @IBOutlet weak var billPreInvoiceMobile: UILabel!
    @IBOutlet weak var billPreBookingNameAdd: UILabel!
    @IBOutlet weak var billPreBoookingGSTIN: UILabel!
    @IBOutlet weak var billPreBookingMobile: UILabel!
    @IBOutlet weak var billPreSACode: UILabel!
    @IBOutlet weak var billPreTax: UILabel!
    @IBOutlet weak var billPreSubTotal: UILabel!
    @IBOutlet weak var billPreCGST: UILabel!
    @IBOutlet weak var billPreSGST: UILabel!
    @IBOutlet weak var billPreTotalAmount: UILabel!
    @IBOutlet weak var billPreTableHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var qrCodeImage: UIImageView!
    @IBOutlet weak var qrcodeView: UIView!
    
    
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cancelBtn: UIView!
    @IBOutlet weak var billingPreviewScrollView: UIScrollView!
    @IBOutlet weak var AddBillingHeadBtn: UIView!
    @IBOutlet weak var billingDetailsView: UIView!
    @IBOutlet weak var billingPreview: UIView!
    @IBOutlet weak var billingCustomerView: UIView!
    @IBOutlet weak var nextBtn: UIView!
    @IBOutlet weak var backBtn: UIView!
    @IBOutlet weak var createBtn: UIView!
    @IBOutlet weak var billPreviewBackBtn: UIView!
    @IBOutlet weak var finalizeBtn: UIView!
    @IBOutlet weak var addBillingHeadPreviewBtn: UIView!
    var bookingList = [BookingElement]()
    var bookingId = Int()
    var currencyName = String()
    var currencyCode = String()
    var currencyCountry = String()
    var currId = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonCardArray(arrView: [nextBtn,createBtn,finalizeBtn])
        billingPreview.isHidden = true
        billingDetailsView.isHidden = true
        billingCustomerView.isHidden = false
        billingPreviewScrollView.delegate = self
        addBillingHeadPreviewBtn.layer.cornerRadius = addBillingHeadPreviewBtn.frame.height / 2
        AddBillingHeadBtn.layer.cornerRadius = AddBillingHeadBtn.frame.height / 2
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.billingPreviewTableView.delegate = self
        self.billingPreviewTableView.delegate = self
        array = [1]
        NotificationCenter.default.addObserver(self, selector: #selector(self.makeInvoice(_:)), name: .makeInvoice, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.billingParty(_:)), name: .billingParty, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.billingCustomerDate(_:)), name: .billingCustomerDate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.billingHead(_:)), name: .billingHead, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.taxView(_:)), name: .taxView, object: nil)
        self.tableView.allowsMultipleSelection = true
        
        // Do any additional setup after loading the view.
    }
    var tax = Double()
    
    @objc func taxView(_ notification: NSNotification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        let taxx = userInfo!["taxView"] as! String
        print("tax == \(taxx)")
        totaal = 0
        self.SGST_CGST = 0
        
        if Double(taxx) == nil{
            self.tax = 0.0
        }else{
            self.tax = Double(taxx) as! Double
        }
        
        for billRecord in self.billRecords{
            if billRecord.taxable == true{
                let total = Double(billRecord.units!) * Double(billRecord.amount!)
                totaal += total
                print("totaal taxable === \(totaal) ")
                print("taxxx ==== \(self.tax)")
                self.SGST_CGST = totaal*tax/100
                let gst = self.SGST_CGST / 2
                //                let roundedValue = gst.rounded(.toNearestOrAwayFromZero)
                print("SGST_CGST ==== \(self.SGST_CGST / 2)")
                self.cgstLabel.text = "\(gst)"
                self.billPreCGST.text = "\(gst)"
                self.billPreSGST.text = "\(gst)"
                self.sgstLabel.text = "\(gst)"
            }else{
                let total = Double(billRecord.units!) * Double(billRecord.amount!)
                totaal -= total
                print("totaal taxable === \(totaal) ")
                print("taxxx ==== \(self.tax)")
                self.SGST_CGST = totaal*tax/100
                let gst = self.SGST_CGST / 2
                //                let roundedValue = gst.rounded(.toNearestOrAwayFromZero)
                print("SGST_CGST ==== \(gst)")
                self.cgstLabel.text = "\(gst)"
                self.sgstLabel.text = "\(gst)"
                self.billPreCGST.text = "\(gst)"
                self.billPreSGST.text = "\(gst)"
                
            }
        }
        
        
        self.taxLabel.text = "\(taxx)" + " %"
        self.billPreTax.text = "\(taxx)" + " %"
        
    }
    
    @IBAction func selectBillingHeadClicked(_ sender: Any) {
        picker(state: "billingHead")
    }
    
    var taxInfoData : ExTaxInfo1?
    @objc func billingHead(_ notification: NSNotification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        let taxInfo = userInfo!["taxInfo"] as! ExTaxInfo1
        print("taxInfo == \(taxInfo.billRecordType!)")
        taxInfoData = taxInfo
        
        self.selectBillingHead.text = taxInfo.billRecordType!
        
    }
    
    @objc func billingCustomerDate(_ notification: NSNotification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        self.invoiceDate.text = (userInfo!["date"] as! String)
        
    }
    
    @objc func billingParty(_ notification: NSNotification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        var billingParty = userInfo!["billingParty"] as! String
        self.billingPartyLabel.text = billingParty
        self.billingpreviewInvoiceType.text = billingParty
        if billingParty == "CONSIGNOR/SHIPPER" {
            billingParty = "SHIPPER"
        }
        self.updateBilling(billingParty: billingParty)
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        billingPreviewScrollView.contentOffset.x = 0
    }
    
    
    @IBAction func billingHeadCheckBox(_ sender: Any) {
        if self.billingHeadCheckView.image == UIImage(named: "blueUnchecked"){
            self.billingHeadCheckView.image = UIImage(named: "blueChecked")
            self.isTaxable = true
        }else{
            self.billingHeadCheckView.image = UIImage(named: "blueUnchecked")
            self.isTaxable = false
        }
    }
    
    @objc func makeInvoice(_ notification: NSNotification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        let bookingList = userInfo!["booking"] as! BookingElement
        self.bookingId = bookingList.id!
        print("bookingId ====== \(bookingId)")
        print("Booking billingParty ==== \(String(describing: bookingList.billingParty!))")
        self.transpoterNameAddress.text = bookingList.shipper!.name! + " " + bookingList.shipper!.address!
        self.transpoterGSTNo.text = bookingList.shipper!.loadOwner!.gstTaxRegNo!
        self.fleetId.text = bookingList.availableFleetBid?.bidCode!
        self.loadId.text = bookingList.availableLoadBid?.bidCode!
        self.billRecords.removeAll()
        self.exchangeBillRecordsArr.removeAll()
        self.updateBillingCustomerText(bookingList: bookingList)
        
        
    }
    
    func updateBillingCustomerText(bookingList:BookingElement){
        invoiceNo.text = bookingList.bookingNo!
        billingPreviewInvoiceNo.text = bookingList.bookingNo!
        billingPartyLabel.text = bookingList.billingParty!
        billingpreviewInvoiceType.text = bookingList.billingParty!
        let timestamp = bookingList.createDate! / 1000
        let invoiceDatee = self.epochDate(epochDate: Double(timestamp))
        invoiceDate.text = invoiceDatee
        billingPreviewInvoiceDate.text = invoiceDatee
        
        
        switch bookingList.billingParty! {
        case "BOOKING CUSTOMER":
            self.nameAddressLabel.text = bookingList.loadOwner!.name!
            self.gstinLabel.text = bookingList.loadOwner!.gstTaxRegNo!
            self.invoiceMobileNo.text = bookingList.loadOwner!.telephone!
        
            self.billingPreNameAddress.text = bookingList.loadOwner!.name!
                      self.billingPreInvoiceGSTIN.text = bookingList.loadOwner!.gstTaxRegNo!
                      self.billPreInvoiceMobile.text = bookingList.loadOwner!.telephone!
            
        case "CONSIGNEE":
            self.nameAddressLabel.text = bookingList.consignee!.name! + " " + bookingList.consignee!.address!
            self.gstinLabel.text = bookingList.consignee!.loadOwner!.gstTaxRegNo!
            self.invoiceMobileNo.text = bookingList.consignee?.contactNo
            
            self.billingPreNameAddress.text =  bookingList.consignee!.name! + " " + bookingList.consignee!.address!
                      self.billingPreInvoiceGSTIN.text = bookingList.consignee!.loadOwner!.gstTaxRegNo!
                      self.billPreInvoiceMobile.text = bookingList.consignee?.contactNo
            
        case "DELIVERY CUSTOMER":
            self.nameAddressLabel.text = bookingList.deliveryCustomer!.name! + " " + bookingList.deliveryCustomer!.address!
            self.gstinLabel.text = bookingList.deliveryCustomer!.loadOwner!.gstTaxRegNo!
            self.invoiceMobileNo.text = bookingList.deliveryCustomer!.contactNo
            
            self.billingPreNameAddress.text =  bookingList.deliveryCustomer!.name! + " " + bookingList.deliveryCustomer!.address!
                                self.billingPreInvoiceGSTIN.text = bookingList.deliveryCustomer!.loadOwner!.gstTaxRegNo!
                                self.billPreInvoiceMobile.text = bookingList.deliveryCustomer!.contactNo
                      
        case "SHIPPER":
            self.nameAddressLabel.text = bookingList.shipper!.name! + " " + bookingList.shipper!.address!
            self.gstinLabel.text = bookingList.shipper!.loadOwner!.gstTaxRegNo!
            self.invoiceMobileNo.text = bookingList.shipper!.contactNo
            
            self.billingPreNameAddress.text =  bookingList.shipper!.name! + " " + bookingList.shipper!.address!
            self.billingPreInvoiceGSTIN.text = bookingList.shipper!.loadOwner!.gstTaxRegNo!
            self.billPreInvoiceMobile.text = bookingList.shipper!.contactNo
            
        default:
            self.nameAddressLabel.text = bookingList.loadOwner!.name!
            self.gstinLabel.text = bookingList.loadOwner!.gstTaxRegNo!
            self.invoiceMobileNo.text = bookingList.loadOwner!.telephone!
            self.billingPreNameAddress.text = bookingList.loadOwner!.name!
            self.billingPreInvoiceGSTIN.text = bookingList.loadOwner!.gstTaxRegNo!
            self.billPreInvoiceMobile.text = bookingList.loadOwner!.telephone!
            
        }
        
        
        bookingCustomerNameAdd.text = bookingList.loadOwner!.name!
        bookingCustomerGSTIn.text = bookingList.loadOwner!.gstTaxRegNo!
        bookingCustomerMobileNo.text = bookingList.loadOwner!.telephone!
        self.billPreBookingNameAdd.text = bookingList.loadOwner!.name!
                self.billPreBoookingGSTIN.text = bookingList.loadOwner!.gstTaxRegNo!
                self.billPreBookingMobile.text = bookingList.loadOwner!.telephone!
    }
    
    
    
    @IBAction func invoiceBillClicked(_ sender: Any) {
        picker(state: "billingParty")
    }
    
    @IBAction func taxViewClicked(_ sender: Any) {
        picker(state: "taxView")
    }
    
    func updateBilling(billingParty:String) {
        
        //  let billingPartyy = billingParty.replacingOccurrences(of: " ", with: "")
        self.activityIndicatorBegin()
        let headers = header()
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
        
        Alamofire.request(URLStatics.updatebilling+"\(self.bookingId)"+"/"+"\(billingParty)", method: .put, headers: headers)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                
                switch (response.result) {
                case .success:
                    self.activityIndicatorEnd()
                    if let reply = response.result.value {
                        var vehicleDetails : VehicleContainerDetails1
                        var vehicleDetailsDict = [String:Any]()
                        let mainResponse = JSON(reply)
                        var vehicleType : AssignVehicleType1
                        var documentDict = [String:Any]()
                        var documentSmallDict = [String:Any]()
                        var document : FirstPicID
                        var doucmentSmall : FirstPicID
                        var vehicleTypeDict = [String:Any]()
                        var grossWeightUnit : WeightUnit
                        var chargeableWTUnit : WeightUnit
                        var netWeightUnit : WeightUnit
                        var grosWtUnit = [String:Any]()
                        var chargebleWtUnit = [String:Any]()
                        var netWtUnit = [String:Any]()
                        var jobStatusDict = [String:Any]()
                        var jobStatus : JobStatusClass
                        var cargoType : CargoTypeClass
                        var cargoo = [String:Any]()
                        var loadType : LoadTypeClass
                        var loadTypeDict = [String:Any]()
                        var jobType : JobTypeClass
                        var jobTypeDict = [String:Any]()
                        var bookingType : BookingTypeClass
                        var bookingTypeDict = [String:Any]()
                        var availbaleFleetBid : AvailableBid1?
                        var availFleetBidDict = [String:Any]()
                        var availableLoadBid : AvailableBid1?
                        var availLoadBidDict = [String:Any]()
                        var jobVehiclesDict = [[String:Any]]()
                        var podId : PodID?
                        var podDict : [String:Any]?
                        var loadOwner : LoadOwner1?
                        var fleetOwner : FleetOwner1?
                        var loadOwnerDict : [String:Any]?
                        var fleetOwnerDict : [String:Any]?
                        var shipper : Shipper?
                        var loadOwnerShipperDict = [String:Any]()
                        var loadOwnerShipper : LoadOwner1?
                        var consignee : Consignee?
                        var loadOwnerConsignee : LoadOwner1?
                        var loadownerConsigneeDict = [String:Any]()
                        var shipperDict = [String:Any]()
                        var consigneeDict = [String:Any]()
                        var deliveryCustomerDict = [String:Any]()
                        var deliveryCustomer : Consignee?
                        var loadOwnerDeliveryCustomer : LoadOwner1?
                        var loadownerDeliveryCustomerDict = [String:Any]()
                        var freightTerm : FreightTermsClass?
                        var freightTermDict = [String:Any]()
                        var exTaxInfo : ExTaxInfo?
                        var taxGroupDict = [String:Any]()
                        var taxGroup: TaxGroup?
                        var taxDetailArrDict = [[String:Any]]()
                        var taxDict = [[String:Any]]()
                        var selectedTags = [Int]()
                        print("======= JSON: updatebilling ======== \(mainResponse)")
                        
                        let data = mainResponse["data"].dictionaryObject!
                        var jobVehiclesDrivers = [JobVehiclesDriver]()
                        var taxDetailArray = [TaxDetail]()
                        var tax = [Tax]()
                        if let returnObject =
                            data["jobVehiclesDrivers"] as? [[String:Any]] {
                            jobVehiclesDict = returnObject
                        }
                        
                        
                        
                        for job in jobVehiclesDict{
                            
                            podDict = job["podId"] as? [String:Any]
                            podId = PodID(id: podDict?["id"] as? Int, mimeType: podDict?["mimeType"] as? String)
                            
                            let jobDriver = JobVehiclesDriver(id: job["id"] as? Int, availableFleetBid: nil, vehicleRegistrationNo: job["vehicleRegistrationNo"] as? String, driverFullName: job["driverFullName"] as? String, mobileNo: job["mobileNo"] as? String, startKM:  job["startKM"] as? Int, endKM: job["endKM"] as? Int, user: nil, allocationType: job["allocationType"] as? String, createTime: job["createTime"] as? Int, vehicleType: nil, jobStatus: nil, lrNo: job["lrNo"] as? Int, uploadPodTime: job["uploadPodTime"] as? Int, podID: podId, exManualLrNo: job["exManualLrNo"] as? String, exManualLrDate: job["exManualLrDate"] as? Int)
                            
                            jobVehiclesDrivers.append(jobDriver)
                            print("podDict id ====== \(String(describing: podDict?["id"]))")
                            
                        }
                        
                        
                        let yourString = data["exTaxInfo"] as? String
                        var exTaxInfojson: JSON
                        let dataToConvert = yourString!.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                        do {
                            exTaxInfojson = try JSON(data: dataToConvert!)
                        } catch _ {
                            exTaxInfojson = ""
                        }
                        
                        taxGroupDict = exTaxInfojson["taxGroup"].dictionaryObject!
                        
                        taxGroup = TaxGroup(taxGroupDescription: taxGroupDict["taxGroupDescription"] as? String, tax: taxGroupDict["tax"] as? Int, id: taxGroupDict["id"] as? Int, status: taxGroupDict["status"] as? Bool, defaultType: taxGroupDict["defaultType"] as? String)
                        
                        
                        for taxx in exTaxInfojson["taxDetails"].arrayValue{
                            print("taxName \(taxx["taxName"].stringValue)")
                            let taxInfo = TaxDetail(taxAmount: taxx["taxAmount"].int, taxName: taxx["taxName"].string, taxRate: taxx["taxRate"].double, taxType: taxx["taxType"].string)
                            taxDetailArray.append(taxInfo)
                        }
                        for taxx in exTaxInfojson["taxs"].arrayValue{
                            let taxmasterdict = taxx["taxMaster"].dictionaryValue
                            let taxMaster = TaxMaster(id: taxmasterdict["id"]?.int, taxName: taxmasterdict["taxName"]?.string, taxType: taxmasterdict["taxType"]?.string)
                            let taxInfo = Tax(taxMaster: taxMaster, taxType: taxx["taxType"].string, id: taxx["id"].int, tax: taxx["tax"].double, status: taxx["status"].bool, taxName: taxx["taxName"].string)
                            tax.append(taxInfo)
                        }
                        
                        
                        print("exTaxInfojson string: " + String(describing: exTaxInfojson))
                        
                        print("id booking list ==== \(String(describing: data["id"]))")
                        
                        shipperDict = data["shipper"] as! [String:Any]
                        consigneeDict = data["consignee"] as! [String:Any]
                        deliveryCustomerDict = data["deliveryCustomer"] as! [String:Any]
                        loadOwnerShipperDict = shipperDict["loadOwner"] as! [String:Any]
                        loadownerConsigneeDict = consigneeDict["loadOwner"] as! [String:Any]
                        loadownerDeliveryCustomerDict = deliveryCustomerDict["loadOwner"] as! [String:Any]
                        fleetOwnerDict = (data["fleetOwner"] as! [String:Any])
                        
                        vehicleDetailsDict = data["vehicleContainerDetails"] as! [String:Any]
                        loadOwnerDict = data["loadOwner"] as? [String:Any]
                        fleetOwnerDict = data["fleetOwner"] as? [String:Any]
                        availLoadBidDict = data["availableLoadBid"] as! [String:Any]
                        availFleetBidDict = data["availableFleetBid"] as! [String:Any]
                        vehicleTypeDict = vehicleDetailsDict["vehicleType"] as! [String:Any]
                        cargoo = data["cargoType"] as! [String:Any]
                        cargoType = CargoTypeClass(id: cargoo["id"] as! Int, cargoType: cargoo["cargoType"] as! String)
                        
                        loadTypeDict = data["loadType"] as! [String:Any]
                        jobTypeDict = data["jobType"] as! [String:Any]
                        jobType = JobTypeClass(id: jobTypeDict["id"] as? Int, jobType: jobTypeDict["jobType"] as? String)
                        loadType = LoadTypeClass(id: loadTypeDict["id"] as! Int, loadType: loadTypeDict["loadType"] as! String, code: loadTypeDict["code"] as! String)
                        
                        bookingTypeDict = data["bookingType"] as! [String:Any]
                        bookingType = BookingTypeClass(id: bookingTypeDict["id"] as? Int, bookingType: bookingTypeDict["bookingType"] as? String)
                        
                        
                        
                        
                        documentDict = vehicleTypeDict["document"] as! [String:Any]
                        documentSmallDict = vehicleTypeDict["documentSmall"] as! [String:Any]
                        document = FirstPicID(id: documentDict["id"] as! Int, mimeType: documentDict["mimeType"] as! String, type: documentDict["type"] as! String)
                        doucmentSmall = FirstPicID(id: documentSmallDict["id"] as! Int, mimeType: documentSmallDict["mimeType"] as! String, type: documentSmallDict["type"] as! String)
                        grosWtUnit = vehicleDetailsDict["grossWeightUnit"] as! [String:Any]
                        chargebleWtUnit = vehicleDetailsDict["chargeableWeightUnit"] as! [String:Any]
                        netWtUnit = vehicleDetailsDict["netWeightUnit"] as! [String:Any]
                        
                        grossWeightUnit = WeightUnit(id: grosWtUnit["id"] as! Int, name: grosWtUnit["name"] as! String, type: grosWtUnit["type"] as! String)
                        chargeableWTUnit = WeightUnit(id: chargebleWtUnit["id"] as! Int, name: chargebleWtUnit["name"] as! String, type: chargebleWtUnit["type"] as! String)
                        jobStatusDict = data["jobStatus"] as! [String:Any]
                        jobStatus = JobStatusClass(id: jobStatusDict["id"] as! Int, jobStatus: jobStatusDict["jobStatus"] as! String)
                        
                        netWeightUnit = WeightUnit(id: netWtUnit["id"] as! Int, name: netWtUnit["name"] as! String, type: netWtUnit["type"] as! String)
                        vehicleType = AssignVehicleType1(id: vehicleTypeDict["id"] as! Int, name: vehicleTypeDict["name"] as! String, sequence: vehicleTypeDict["sequence"] as! Double, container: vehicleTypeDict["container"] as! Bool, type: vehicleTypeDict["type"] as! String, document: document, documentSmall: doucmentSmall, vehicleOrder: vehicleTypeDict["vehicleOrder"] as! Int, documentID: vehicleTypeDict["documentId"] as! Int, smallDocID: vehicleTypeDict["smallDocId"] as! Int, weightCapacity: vehicleTypeDict["weightCapacity"] as? Int)
                        
                        availableLoadBid = AvailableBid1(id: availLoadBidDict["id"] as? Int,
                                                         
                                                         bidCode: availLoadBidDict["bidCode"] as? String,
                                                         
                                                         creationDate: availLoadBidDict["creationDate"] as? Int,
                                                         
                                                         closedBidTime: availLoadBidDict["closedBidTime"] as? Int,
                                                         
                                                         availableLocation: availLoadBidDict["availableLocation"] as? String,
                                                         destinationLocation: availLoadBidDict["destinationLocation"] as? String,
                                                         comments: availLoadBidDict["comments"] as? String,
                                                         availableLatitude: availLoadBidDict["availableLatitude"] as? Double,
                                                         availableLongitude: availLoadBidDict["availableLongitude"] as? Double,
                                                         destinationLatitude: availLoadBidDict["destinationLatitude"] as? Double,
                                                         destinationLongitude: availLoadBidDict["destinationLongitude"] as? Double,
                                                         vehicleType:nil, noOfVehicles: availLoadBidDict["noOfVehicles"] as? Int,
                                                         availableDateTime: nil,
                                                         cargoType: nil,
                                                         capacityWeight:  Int(truncating: availLoadBidDict["capacityWeight"] as! NSNumber),
                                                         capacityWeightUnit:nil,
                                                         loadType: nil,
                                                         expectedFreight: availLoadBidDict["expectedFreight"] as? Int,
                                                         currencyMaster: nil,
                                                         status:availLoadBidDict["status"] as? String,
                                                         registrationNo: nil,
                                                         postType: nil,
                                                         bidInviteCount: nil, bidQuotationCount: nil,
                                                         containerNo: availLoadBidDict["containerNo"] as? String,
                                                         sealNo: availLoadBidDict["sealNo"] as? String, pinNo: nil, packgs: availLoadBidDict["packgs"] as? Int)
                        
                        
                        
                        
                        availbaleFleetBid = AvailableBid1(id: availFleetBidDict["id"] as? Int,
                                                          bidCode: availFleetBidDict["bidCode"] as? String,
                                                          creationDate: availFleetBidDict["creationDate"] as? Int,
                                                          closedBidTime: availFleetBidDict["closedBidTime"] as? Int,
                                                          availableLocation: availFleetBidDict["availableLocation"] as? String,
                                                          destinationLocation: availFleetBidDict["destinationLocation"] as? String,
                                                          comments: availFleetBidDict["comments"] as? String,
                                                          availableLatitude: availFleetBidDict["availableLatitude"] as? Double,
                                                          availableLongitude: availFleetBidDict["availableLongitude"] as? Double,
                                                          destinationLatitude: availFleetBidDict["destinationLatitude"] as? Double,
                                                          destinationLongitude: availFleetBidDict["destinationLongitude"] as? Double,
                                                          vehicleType: nil, noOfVehicles: availFleetBidDict["noOfVehicles"] as? Int,
                                                          availableDateTime: availFleetBidDict["availableDateTime"] as? Int,
                                                          cargoType: nil,
                                                          capacityWeight:  availFleetBidDict["capacityWeight"] as? Int,
                                                          capacityWeightUnit: nil ,
                                                          loadType: nil,
                                                          expectedFreight: nil ,
                                                          currencyMaster: nil,
                                                          status: availFleetBidDict["status"] as? String,
                                                          registrationNo: availFleetBidDict["registrationNo"] as? String,
                                                          postType: availFleetBidDict["postType"] as? String,
                                                          bidInviteCount: availFleetBidDict["bidInviteCount"] as? Int, bidQuotationCount: nil,
                                                          containerNo: availFleetBidDict["containerNo"] as? String,
                                                          sealNo: availFleetBidDict["sealNo"] as? String, pinNo: nil, packgs: availFleetBidDict["packgs"] as? Int)
                        
                        
                        
                        vehicleDetails = VehicleContainerDetails1(id: vehicleDetailsDict["id"] as! Int,
                                                                 vehicleType: vehicleType,
                                                                 noOfVehicles: vehicleDetailsDict["noOfVehicles"] as! Int,
                                                                 grossWeight: Int(truncating:  vehicleDetailsDict["grossWeight"] as! NSNumber),
                                                                 grossWeightUnit: grossWeightUnit,
                                                                 chargeableWeight: Int(truncating: vehicleDetailsDict["chargeableWeight"] as! NSNumber),
                                                                 chargeableWeightUnit: chargeableWTUnit,
                                                                 netWeight: Int(truncating: vehicleDetailsDict["netWeight"] as! NSNumber),
                                                                 netWeightUnit: netWeightUnit)
                        
                        
                        loadOwner = LoadOwner1(id: loadOwnerDict!["id"] as? Int, name: loadOwnerDict!["name"] as? String, registrationNo: loadOwnerDict!["registrationNo"] as? String, companyCode: loadOwnerDict!["companyCode"] as? String, telephone: loadOwnerDict!["telephone"] as? String, fax: loadOwnerDict!["fax"] as? String, address: nil, typeOfCompany: nil, logo: nil, gstTax: loadOwnerDict!["gstTax"] as? Double, gstTaxRegNo: loadOwnerDict!["gstTaxRegNo"] as? String, jurisdiction: loadOwnerDict!["jurisdiction"] as? String, timeZone: loadOwnerDict!["timeZone"] as? String, panNo: loadOwnerDict!["panNo"] as? String, vatNo: loadOwnerDict!["vatNo"] as? String, iecNo: loadOwnerDict!["iecNo"] as? String, cinNo: loadOwnerDict!["cinNo"] as? String, cstNo: loadOwnerDict!["cstNo"] as? String, registrationDate: loadOwnerDict!["registrationDate"] as? Int, subscriptionStartDate: loadOwnerDict!["subscriptionStartDate"] as? Int, subscriptionEndDate: loadOwnerDict!["subscriptionEndDate"] as? Int, currentBalance: loadOwnerDict!["currentBalance"] as? Int, smsNotification: loadOwnerDict!["smsNotification"] as? String, companyAccess: nil, loadAdditionalInformation: nil, status: loadOwnerDict!["status"] as? String, expiredType: loadOwnerDict!["expiredType"] as? String, activeUserCount: loadOwnerDict!["activeUserCount"] as? Int, maxUser: loadOwnerDict!["maxUser"] as? Int, maxUserType: loadOwnerDict!["maxUserType"] as? String, exchangeBookingCount: loadOwnerDict!["exchangeBookingCount"] as? Int, exWalletValidFrom: loadOwnerDict!["exWalletValidFrom"] as? Int, exWalletValidTill: loadOwnerDict!["exWalletValidTill"] as? Int, exWalletValidDays: loadOwnerDict!["exWalletValidDays"] as? Int, exPlanType: loadOwnerDict!["exPlanType"] as? String, goCount: loadOwnerDict!["goCount"] as? Int, proCount: loadOwnerDict!["proCount"] as? Int, profileMailCount: loadOwnerDict!["profileMailCount"] as? Int, ewbFlagStr: loadOwnerDict!["ewbFlagStr"] as? String, planType: loadOwnerDict!["planType"] as? String, exSuwalletAmount: loadOwnerDict!["exSuwalletAmount"] as? Int, exSuhubMaxUser: loadOwnerDict!["exSuhubMaxUser"] as? Int, exSuallowTransaction: loadOwnerDict!["exSuallowTransaction"] as? Int, exSuremainsTr: loadOwnerDict!["exSuremainsTr"] as? Int, hubPlanID: loadOwnerDict!["hubPlanID"] as? Int, exSuhubTransactionCost: loadOwnerDict!["exSuhubTransactionCost"] as? Int, exPackType: loadOwnerDict!["exPackType"] as? String)
                        
                        
                        fleetOwner = FleetOwner1(id: fleetOwnerDict!["id"] as? Int, name: fleetOwnerDict!["name"] as? String, registrationNo: fleetOwnerDict!["registrationNo"] as? String, panNo: fleetOwnerDict!["panNo"] as? String, fax: fleetOwnerDict!["fax"] as? String, telephone: fleetOwnerDict!["telephone"] as? String, typeOfCompany: nil, fleetAdditionalInformation: nil, address: nil, userCount: fleetOwnerDict!["userCount"] as? Int, activeUserCount: fleetOwnerDict!["activeUserCount"] as? Int, companyCode: fleetOwnerDict!["companyCode"] as? String, logo: nil, gstTax: fleetOwnerDict!["gstTax"] as? Int, gstTaxRegNo: fleetOwnerDict!["gstTaxRegNo"] as? String, jurisdiction: fleetOwnerDict!["jurisdiction"] as? String, timeZone: fleetOwnerDict!["timeZone"] as? String, vatNo: fleetOwnerDict!["vatNo"] as? String, iecNo: fleetOwnerDict!["iecNo"] as? String, cinNo: fleetOwnerDict!["cinNo"] as? String, cstNo: fleetOwnerDict!["cstNo"] as? String, registrationDate: fleetOwnerDict!["registrationDate"] as? Int, expiredType: fleetOwnerDict!["expiredType"] as? String, subscriptionStartDate: fleetOwnerDict!["subscriptionStartDate"] as? Int, subscriptionEndDate: fleetOwnerDict!["subscriptionEndDate"] as? Int, perTransactionFee: fleetOwnerDict!["perTransactionFee"] as? Int, smsCharge: fleetOwnerDict!["smsCharge"] as? Int, currentBalance: fleetOwnerDict!["currentBalance"] as? Int, bookingCount: fleetOwnerDict!["bookingCount"] as? Int, purchasePaymentCount: fleetOwnerDict!["purchasePaymentCount"] as? Int, purchaseReceiptCount: fleetOwnerDict!["purchaseReceiptCount"] as? Int, routeCount: fleetOwnerDict!["routeCount"] as? Int, smsNotification: fleetOwnerDict!["smsNotification"] as? String, maxUser: fleetOwnerDict!["maxUser"] as? Int, maxUserType: fleetOwnerDict!["maxUserType"] as? String, status: fleetOwnerDict!["status"] as? String, contractMasterCount: fleetOwnerDict!["contractMasterCount"] as? Int, goCount: fleetOwnerDict!["goCount"] as? Int, proCount: fleetOwnerDict!["proCount"] as? Int, profileMailCount: fleetOwnerDict!["profileMailCount"] as? Int, ewbFlagStr: fleetOwnerDict!["ewbFlagStr"] as? String, walletAmount: fleetOwnerDict!["walletAmount"] as? Int, hubMaxUser: fleetOwnerDict!["hubMaxUser"] as? Int, allowTransaction: fleetOwnerDict!["allowTransaction"] as? Int, remainsTr: fleetOwnerDict!["remainsTr"] as? Int, hubPlanID: fleetOwnerDict!["hubPlanID"] as? Int, hubTransactionCost: fleetOwnerDict!["hubTransactionCost"] as? Int, thirdPartyAuthDateTime: fleetOwnerDict!["thirdPartyAuthDateTime"] as? Int, exPurchasePayCount: fleetOwnerDict!["exPurchasePayCount"] as? Int, exPurchaseReceiptCount: fleetOwnerDict!["exPurchaseReceiptCount"] as? Int, exWalletValidFrom: fleetOwnerDict!["exWalletValidFrom"] as? Int, exWalletValidTill: fleetOwnerDict!["exWalletValidTill"] as? Int, exWalletValidDays: fleetOwnerDict!["exWalletValidDays"] as? Int, exPlanType: fleetOwnerDict!["exPlanType"] as? String, exPackType: fleetOwnerDict!["exPackType"] as? String, userCounts: fleetOwnerDict!["userCounts"] as? Int)
                        
                        loadOwnerShipper = LoadOwner1(id: loadOwnerShipperDict["id"] as? Int, name: loadOwnerShipperDict["name"] as? String, registrationNo: loadOwnerShipperDict["registrationNo"] as? String, companyCode: loadOwnerShipperDict["companyCode"] as? String, telephone: loadOwnerShipperDict["telephone"] as? String, fax: loadOwnerShipperDict["fax"] as? String, address: nil, typeOfCompany: nil, logo: nil, gstTax: loadOwnerShipperDict["gstTax"] as? Double, gstTaxRegNo: loadOwnerShipperDict["gstTaxRegNo"] as? String, jurisdiction: loadOwnerShipperDict["jurisdiction"] as? String, timeZone: loadOwnerShipperDict["timeZone"] as? String, panNo: loadOwnerShipperDict["panNo"] as? String, vatNo: loadOwnerShipperDict["vatNo"] as? String, iecNo: loadOwnerShipperDict["iecNo"] as? String, cinNo: loadOwnerShipperDict["cinNo"] as? String, cstNo: loadOwnerShipperDict["cstNo"] as? String, registrationDate: loadOwnerShipperDict["registrationDate"] as? Int, subscriptionStartDate: loadOwnerShipperDict["subscriptionStartDate"] as? Int, subscriptionEndDate: loadOwnerShipperDict["subscriptionEndDate"] as? Int, currentBalance: loadOwnerShipperDict["currentBalance"] as? Int, smsNotification: loadOwnerShipperDict["smsNotification"] as? String, companyAccess: nil, loadAdditionalInformation: nil, status: loadOwnerShipperDict["status"] as? String, expiredType: loadOwnerShipperDict["expiredType"] as? String, activeUserCount: loadOwnerShipperDict["activeUserCount"] as? Int, maxUser: loadOwnerShipperDict["maxUser"] as? Int, maxUserType: loadOwnerShipperDict["maxUserType"] as? String, exchangeBookingCount: loadOwnerShipperDict["exchangeBookingCount"] as? Int, exWalletValidFrom: loadOwnerShipperDict["exWalletValidFrom"] as? Int, exWalletValidTill: loadOwnerShipperDict["exWalletValidTill"] as? Int, exWalletValidDays: loadOwnerShipperDict["exWalletValidDays"] as? Int, exPlanType: loadOwnerShipperDict["exPlanType"] as? String, goCount: loadOwnerShipperDict["goCount"] as? Int, proCount: loadOwnerShipperDict["proCount"] as? Int, profileMailCount: loadOwnerShipperDict["profileMailCount"] as? Int, ewbFlagStr: loadOwnerShipperDict["ewbFlagStr"] as? String, planType: loadOwnerShipperDict["planType"] as? String, exSuwalletAmount: loadOwnerShipperDict["exSuwalletAmount"] as? Int, exSuhubMaxUser: loadOwnerShipperDict["exSuhubMaxUser"] as? Int, exSuallowTransaction: loadOwnerShipperDict["exSuallowTransaction"] as? Int, exSuremainsTr: loadOwnerShipperDict["exSuremainsTr"] as? Int, hubPlanID: loadOwnerShipperDict["hubPlanID"] as? Int, exSuhubTransactionCost: loadOwnerShipperDict["exSuhubTransactionCost"] as? Int, exPackType: loadOwnerShipperDict["exPackType"] as? String)
                        
                        loadOwnerConsignee = LoadOwner1(id: loadownerConsigneeDict["id"] as? Int, name: loadownerConsigneeDict["name"] as? String, registrationNo: loadownerConsigneeDict["registrationNo"] as? String, companyCode: loadownerConsigneeDict["companyCode"] as? String, telephone: loadownerConsigneeDict["telephone"] as? String, fax: loadownerConsigneeDict["fax"] as? String, address: nil, typeOfCompany: nil, logo: nil, gstTax: loadownerConsigneeDict["gstTax"] as? Double, gstTaxRegNo: loadownerConsigneeDict["gstTaxRegNo"] as? String, jurisdiction: loadownerConsigneeDict["jurisdiction"] as? String, timeZone: loadownerConsigneeDict["timeZone"] as? String, panNo: loadownerConsigneeDict["panNo"] as? String, vatNo: loadownerConsigneeDict["vatNo"] as? String, iecNo: loadownerConsigneeDict["iecNo"] as? String, cinNo: loadownerConsigneeDict["cinNo"] as? String, cstNo: loadownerConsigneeDict["cstNo"] as? String, registrationDate: loadownerConsigneeDict["registrationDate"] as? Int, subscriptionStartDate: loadownerConsigneeDict["subscriptionStartDate"] as? Int, subscriptionEndDate: loadownerConsigneeDict["subscriptionEndDate"] as? Int, currentBalance: loadownerConsigneeDict["currentBalance"] as? Int, smsNotification: loadownerConsigneeDict["smsNotification"] as? String, companyAccess: nil, loadAdditionalInformation: nil, status:loadownerConsigneeDict["status"] as? String, expiredType: loadownerConsigneeDict["expiredType"] as? String, activeUserCount: loadownerConsigneeDict["activeUserCount"] as? Int, maxUser: loadownerConsigneeDict["maxUser"] as? Int, maxUserType: loadownerConsigneeDict["maxUserType"] as? String, exchangeBookingCount: loadownerConsigneeDict["exchangeBookingCount"] as? Int, exWalletValidFrom: loadownerConsigneeDict["exWalletValidFrom"] as? Int, exWalletValidTill: loadownerConsigneeDict["exWalletValidTill"] as? Int, exWalletValidDays: loadownerConsigneeDict["exWalletValidDays"] as? Int, exPlanType: loadownerConsigneeDict["exPlanType"] as? String, goCount: loadownerConsigneeDict["goCount"] as? Int, proCount: loadownerConsigneeDict["proCount"] as? Int, profileMailCount: loadownerConsigneeDict["profileMailCount"] as? Int, ewbFlagStr: loadownerConsigneeDict["ewbFlagStr"] as? String, planType: loadownerConsigneeDict["planType"] as? String, exSuwalletAmount: loadownerConsigneeDict["exSuwalletAmount"] as? Int, exSuhubMaxUser: loadownerConsigneeDict["exSuhubMaxUser"] as? Int, exSuallowTransaction: loadownerConsigneeDict["exSuallowTransaction"] as? Int, exSuremainsTr: loadownerConsigneeDict["exSuremainsTr"] as? Int, hubPlanID: loadownerConsigneeDict["hubPlanID"] as? Int, exSuhubTransactionCost: loadownerConsigneeDict["exSuhubTransactionCost"] as? Int, exPackType: loadownerConsigneeDict["exPackType"] as? String)
                        
                        loadOwnerDeliveryCustomer = LoadOwner1(id: loadownerDeliveryCustomerDict["id"] as? Int, name: loadownerDeliveryCustomerDict["name"] as? String, registrationNo: loadownerDeliveryCustomerDict["registrationNo"] as? String, companyCode: loadownerDeliveryCustomerDict["companyCode"] as? String, telephone: loadownerDeliveryCustomerDict["telephone"] as? String, fax: loadownerDeliveryCustomerDict["fax"] as? String, address: nil, typeOfCompany: nil, logo: nil, gstTax: loadownerDeliveryCustomerDict["gstTax"] as? Double, gstTaxRegNo: loadownerDeliveryCustomerDict["gstTaxRegNo"] as? String, jurisdiction: loadownerDeliveryCustomerDict["jurisdiction"] as? String, timeZone: loadownerDeliveryCustomerDict["timeZone"] as? String, panNo: loadownerDeliveryCustomerDict["panNo"] as? String, vatNo: loadownerDeliveryCustomerDict["vatNo"] as? String, iecNo: loadownerDeliveryCustomerDict["iecNo"] as? String, cinNo: loadownerDeliveryCustomerDict["cinNo"] as? String, cstNo: loadownerDeliveryCustomerDict["cstNo"] as? String, registrationDate: loadownerDeliveryCustomerDict["registrationDate"] as? Int, subscriptionStartDate: loadownerDeliveryCustomerDict["subscriptionStartDate"] as? Int, subscriptionEndDate: loadownerDeliveryCustomerDict["subscriptionEndDate"] as? Int, currentBalance: loadownerDeliveryCustomerDict["currentBalance"] as? Int, smsNotification: loadownerDeliveryCustomerDict["smsNotification"] as? String, companyAccess: nil, loadAdditionalInformation: nil, status:loadownerDeliveryCustomerDict["status"] as? String, expiredType: loadownerDeliveryCustomerDict["expiredType"] as? String, activeUserCount: loadownerDeliveryCustomerDict["activeUserCount"] as? Int, maxUser: loadownerDeliveryCustomerDict["maxUser"] as? Int, maxUserType: loadownerDeliveryCustomerDict["maxUserType"] as? String, exchangeBookingCount: loadownerDeliveryCustomerDict["exchangeBookingCount"] as? Int, exWalletValidFrom: loadownerDeliveryCustomerDict["exWalletValidFrom"] as? Int, exWalletValidTill: loadownerDeliveryCustomerDict["exWalletValidTill"] as? Int, exWalletValidDays: loadownerDeliveryCustomerDict["exWalletValidDays"] as? Int, exPlanType: loadownerDeliveryCustomerDict["exPlanType"] as? String, goCount: loadownerDeliveryCustomerDict["goCount"] as? Int, proCount: loadownerDeliveryCustomerDict["proCount"] as? Int, profileMailCount: loadownerDeliveryCustomerDict["profileMailCount"] as? Int, ewbFlagStr: loadownerDeliveryCustomerDict["ewbFlagStr"] as? String, planType: loadownerDeliveryCustomerDict["planType"] as? String, exSuwalletAmount: loadownerDeliveryCustomerDict["exSuwalletAmount"] as? Int, exSuhubMaxUser: loadownerDeliveryCustomerDict["exSuhubMaxUser"] as? Int, exSuallowTransaction: loadownerDeliveryCustomerDict["exSuallowTransaction"] as? Int, exSuremainsTr: loadownerDeliveryCustomerDict["exSuremainsTr"] as? Int, hubPlanID: loadownerDeliveryCustomerDict["hubPlanID"] as? Int, exSuhubTransactionCost: loadownerDeliveryCustomerDict["exSuhubTransactionCost"] as? Int, exPackType: loadownerDeliveryCustomerDict["exPackType"] as? String)
                        
                        
                        shipper = Shipper(id: shipperDict["id"] as? Int, name: shipperDict["name"] as? String, address: shipperDict["address"] as? String, location: shipperDict["location"] as? String, state: shipperDict["state"] as? String, contactPerson: shipperDict["contactPerson"] as? String, contactNo: shipperDict["contactNo"] as? String, loadOwner: loadOwnerShipper)
                        
                        consignee = Consignee(id: consigneeDict["id"] as? Int, name: consigneeDict["name"] as? String, address: consigneeDict["address"] as? String, location: consigneeDict["location"] as? String, state: consigneeDict["state"] as? String, contactPerson: consigneeDict["contactPerson"] as? String, contactNo: consigneeDict["contactNo"] as? String, loadOwner: loadOwnerConsignee, gstNo: consigneeDict["gstNo"] as? String, pinNo: consigneeDict["pinNo"] as? String, country: consigneeDict["country"] as? String, createDate: consigneeDict["createDate"] as? Int, updateDate: consigneeDict["updateDate"] as? Int, secAddress: consigneeDict["secAddress"] as? String, contactEmail: consigneeDict["contactEmail"] as? String, cinNo: consigneeDict["cinNo"] as? String, panNo: consigneeDict["panNo"] as? String)
                        
                        deliveryCustomer = Consignee(id: deliveryCustomerDict["id"] as? Int, name: deliveryCustomerDict["name"] as? String, address: deliveryCustomerDict["address"] as? String, location: deliveryCustomerDict["location"] as? String, state: deliveryCustomerDict["state"] as? String, contactPerson: deliveryCustomerDict["contactPerson"] as? String, contactNo: deliveryCustomerDict["contactNo"] as? String, loadOwner: loadOwnerDeliveryCustomer, gstNo: deliveryCustomerDict["gstNo"] as? String, pinNo: deliveryCustomerDict["pinNo"] as? String, country: deliveryCustomerDict["country"] as? String, createDate: deliveryCustomerDict["createDate"] as? Int, updateDate: deliveryCustomerDict["updateDate"] as? Int, secAddress: deliveryCustomerDict["secAddress"] as? String, contactEmail: deliveryCustomerDict["contactEmail"] as? String, cinNo: deliveryCustomerDict["cinNo"] as? String, panNo: deliveryCustomerDict["panNo"] as? String)
                        
                        freightTerm = FreightTermsClass(id: freightTermDict["id"] as? Int, freightTerms: freightTermDict["freightTerms"] as? String)
                        
                        exTaxInfo = ExTaxInfo(taxDetails: taxDetailArray, taxGroup: taxGroup, taxs: tax, taxType: exTaxInfojson["taxType"].string)
                        
                        let booking = BookingElement(id: data["id"] as? Int, shipper: shipper, consignee: consignee, deliveryCustomer: deliveryCustomer, loadOwner: loadOwner, fleetOwner: fleetOwner, billingParty: data["billingParty"] as? String, jobStatus: jobStatus, loadType: loadType, cargoType: cargoType, jobType: jobType, bookingType: bookingType, pickupDateTime: data["pickupDateTime"] as? Int, deliveryDateTime: data["deliveryDateTime"] as? Int, freightTerms: freightTerm, bookingNo: data["bookingNo"] as? String, availableLoadBid: availableLoadBid, availableFleetBid: availbaleFleetBid, loadBidQuotaion: nil, pickUpLocation: data["pickUpLocation"] as? String, destination: data["destination"] as? String, createDate: data["createDate"] as? Int, vehicleContainerDetails: vehicleDetails, user: nil, bookingCust: data["bookingCust"] as? Bool, shiper: data["shiper"] as? Bool, consigneer: data["consigneer"] as? Bool, deliverCust: data["deliverCust"] as? Bool, exInvoiceGrossAmount: data["exInvoiceGrossAmount"] as? Int, exInvoiceGrossAmountWithoutTax: data["exInvoiceGrossAmountWithoutTax"] as? Int, exTaxType: data["exTaxType"] as? String, exTaxInfo: exTaxInfo , exInvoiceNo: data["exInvoiceNo"] as? String, exTaxGroup: nil, invoiceDate: data["invoiceDate"] as? Int, financeYear: data["financeYear"] as? String, salesBillFinal: data["salesBillFinal"] as? Bool, purchaseBillFinal: data["purchaseBillFinal"] as? Bool, defaultBankAndBranch: data["defaultBankAndBranch"] as? String, count: data["count"] as? Int, lrNo: data["lrNo"] as? Int, isinvite: data["isinvite"] as? Bool, eLrTerms: data["eLrTerms"] as? String, salesInvoiceTerms: data["salesInvoiceTerms"] as? String, jobVehiclesDrivers: jobVehiclesDrivers, trips: nil, fleetBidInvite: nil)
                        
                        print("jobId: ========----- \(data["id"] as? Int)")
                        //    if self.bookingList.contains(where: { boooking in boooking.id == booking.id  }) {
                        //
                        //    }else {
                        //
                        //    self.bookingList.append(booking)
                        //    }
                        
                        
                        let bookking = booking
                        self.updateBillingCustomerText(bookingList: bookking)
                        
                    }
                    break
                case .failure(let error):
                    self.activityIndicatorEnd()
                    print("== error == \(error.localizedDescription)")
                    break
                }
        }
        
    }
    
    
    @IBAction func dateViewClicked(_ sender: Any) {
        picker(state: "billingCustomerDate")
    }
    
    @IBAction func finalize(_ sender: Any) {
      
              finalizeInvoice()
    }
    
    func convert(cmage:CIImage) -> UIImage
    {
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent)!
        let image:UIImage = UIImage.init(cgImage: cgImage)
        return image
    }
    
    func finalizeInvoice(){

              self.activityIndicatorBegin()
                    let headers = header()
                    
                    //+918669696004
                    //asdfgf1234$
                    Alamofire.request(URLStatics.finalize+"bookingId=\(bookingId)",method: .put, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                        print("response.request=\(String(describing: response.request))")  // original URL request
                        print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                        print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                        //   self.activityIndicatorEnd()
                        switch (response.result) {
                            
                        case .success:
                            
                            if let reply = response.result.value {
                                print("finalize JSON: >>>>>>>>>>>>>>>>>>>>>>>>>> \(reply)")
                                let mainResponse = JSON(reply)
                                var returnDict = [String:Any]()
                                // let returnObj = mainResponse["data"].array
                                
                                if let returnObj = mainResponse["data"].dictionaryObject {
                                    returnDict = returnObj
                                }
                           
                            }
                            self.activityIndicatorEnd()
                            break
                        case .failure(let error):
                            self.activityIndicatorEnd()
                            print("POST REQUEST ERROR  - \(error.localizedDescription)")
                            print("=== REQUEST INFORMATION ===")
                           
                           
                            print("===")
                            break
                        }
    
                    }
                    
             
    }
    
    @IBAction func createClicked(_ sender: Any) {
        NotificationCenter.default.post(name: .billingPreview, object: nil)
        billingPreview.isHidden = false
        billingDetailsView.isHidden = true
        billingCustomerView.isHidden = true
        updateRecords()
                     let myStr = URLStatics.qrCode+"bookingId=\(bookingId)"
                     let qrImage = createQRFromString(str: myStr)
                     self.qrCodeImage.image = convert(cmage:qrImage!)
    }
    
    @IBAction func nextClicked(_ sender: Any) {
        NotificationCenter.default.post(name: .billingDetails, object: nil)
        billingPreview.isHidden = true
        billingDetailsView.isHidden = false
        billingCustomerView.isHidden = true
        self.exchangeBillrecords(bookingId: bookingId)
        self.selectedTags.removeAll()
    }
    
    @IBAction func cancel(_ sender: Any) {
          NotificationCenter.default.post(name: .backClicked, object: nil)
    }
    
    @IBAction func backClicked(_ sender: Any) {
        NotificationCenter.default.post(name: .backClicked, object: nil)
        billingPreview.isHidden = true
        billingDetailsView.isHidden = true
        billingCustomerView.isHidden = false
    }
    
    @IBAction func billPreviewBackClicked(_ sender: Any) {
        NotificationCenter.default.post(name: .billPreviewBackClicked, object: nil)
        billingPreview.isHidden = true
        billingDetailsView.isHidden = false
        billingCustomerView.isHidden = true
    }
    var i = 11
    
    @IBAction func addBillingClicked(_ sender: Any) {
        
        i += 1
        if taxInfoData?.billRecordType! == self.selectBillingHead.text{
            let billRec = BillRecordType(id: nil, billRecordType: self.selectBillingHead.text!, status: taxInfoData?.status, sacCode: taxInfoData?.sacCode)
            let currency = CurrencyMaster1(id: self.currId, currencyName: self.currencyName, currencyCountry: self.currencyCountry, currencyCode: self.currencyCode)
            let billing = BillRecord(id: i, billRecordType: billRec, currencyMaster: currency, exchangeRate: nil, units: Int(self.unitTextField.text!), amount: Int(self.rateTF.text!), taxable: self.isTaxable, recordType: "DEFAULT", sacCode: self.sacCodeTextField.text!, billRecordDescription: "", taxDetail: "", fleet: nil)
            self.billRecords.append(billing)
        }
        
        self.subTotal = 0
        
        for bill in self.billRecords{
            
            let total = Double(bill.units!) * Double(bill.amount!)
            self.subTotal += total
            
            
        }
        self.subtotalLabel.text = "\(self.subTotal)"
        self.billPreSubTotal.text = "\(self.subTotal)"
        self.totalAmount = self.subTotal + self.SGST_CGST + self.SGST_CGST
        self.totalAmountLabel.text = "\(self.totalAmount)"
        self.billPreTotalAmount.text = "\(self.totalAmount)"
        print("self.subTotal ===== \(self.subTotal)")
        tableView.reloadData()
        billingPreviewTableView.reloadData()
        self.tableHeightConstraint.constant = self.tableView.contentSize.height
        self.billPreTableHeightConstraint.constant = self.billingPreviewTableView.contentSize.height
        
        tableView.reloadData()
          billingPreviewTableView.reloadData()
        self.selectBillingHead.text = "Select Billing Head"
        self.unitTextField.text = ""
        self.rateTF.text = ""
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.billRecords.count != 0{
            return billRecords.count
        }else{
            return 0
        }
        
    }
    
    
    var totalAmount = Double()
    var SGST_CGST = Double()
    var subTotal = Double()
    var totaal = Double()
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "makeInvoice") as! MakeInvoiceTableViewCell
                    
                    self.tableHeightConstraint.constant = self.tableView.contentSize.height
                    let billRecord = self.billRecords[indexPath.row]
            //        if billRecord.taxable == true {
            //            cell.taxCheckBoxImage.image = UIImage(named: "blueChecked")
            //        }
                    if indexPath.row == 0{
                        cell.deleteView.isUserInteractionEnabled = false
                        cell.editImage.isHidden = true
                    }else{
                        cell.deleteView.isUserInteractionEnabled = true
                        cell.editImage.isHidden = false
                    }
                    cell.billDescription.text = billRecord.billRecordType?.billRecordType!
            if billRecord.units != nil && billRecord.amount != nil{
                cell.unit.text = "\(Double(billRecord.units!))"
                cell.rate.text =  "\(Double(billRecord.amount!))" + "0"
                let total = Double(billRecord.units!) * Double(billRecord.amount!)
                                 print("total === \(total)")
                                 cell.total.text = "\(total)" + "0"
            }else{
                cell.unit.text = ""
                cell.rate.text =  ""
                 cell.total.text = ""
            }
                    
                 
                    let taxClicked = UITapGestureRecognizer(target: self, action: #selector(taxClicked(recognizer:)))
                    
                    cell.deleteView.addGestureRecognizer(taxClicked)
                    cell.deleteView.tag = indexPath.row
                    
                    // let tagg = self.selectedTags[indexPath.row]
                    self.subTotal = 0
                    
                            if self.selectedTags.contains(indexPath.row){
                                cell.taxCheckBoxImage.image = UIImage(named: "blueChecked")
                            }else{
                                cell.taxCheckBoxImage.image = UIImage(named: "blueUnchecked")
                            }
                    return cell
        }else{
             let cell = billingPreviewTableView.dequeueReusableCell(withIdentifier: "finalize") as! FinalizeInvoiceCell
             self.billPreTableHeightConstraint.constant = self.billingPreviewTableView.contentSize.height
             let billRecord = self.billRecords[indexPath.row]
                       //        if billRecord.taxable == true {
                       //            cell.taxCheckBoxImage.image = UIImage(named: "blueChecked")
                       //        }
                               
                                   cell.deleteView.isUserInteractionEnabled = false
                                   cell.editImage.isHidden = true
                             
                               cell.billDescription.text = billRecord.billRecordType?.billRecordType!
                               cell.unit.text = "\(Double(billRecord.units!))"
                               cell.rate.text =  "\(Double(billRecord.amount!))" + "0"
                               let total = Double(billRecord.units!) * Double(billRecord.amount!)
                               print("total === \(total)")
                               cell.total.text = "\(total)" + "0"
                                           cell.taxCheckBox.image = UIImage(named: "blueChecked")
                                      
                               return cell
        }
        
        
    }
    
    private func createQRFromString(str: String) -> CIImage? {
        let stringData = str.data(using: .utf8)

        let filter = CIFilter(name: "CIQRCodeGenerator")
        filter?.setValue(stringData, forKey: "inputMessage")
        filter?.setValue("H", forKey: "inputCorrectionLevel")

        return filter?.outputImage
    }

    var qrCode: UIImage? {
        if let img = createQRFromString(str: "Hello world program created by someone") {
            let someImage = UIImage(
                ciImage: img,
                scale: 1.0,
                orientation: UIImage.Orientation.down
            )
            return someImage
        }

        return nil
    }
    
    @objc func taxClicked(recognizer: UITapGestureRecognizer){
        
        let tag = recognizer.view?.tag
        let billrecord = self.billRecords[tag!]
        self.selectBillingHead.text = billrecord.billRecordType?.billRecordType!
        self.unitTextField.text = "\(billrecord.units!)"
        self.rateTF.text = "\(billrecord.amount!)"
        self.billRecords.remove(at: tag!)
        self.tableView.reloadData()
        self.billingPreviewTableView.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tableView{
            let billRecord = self.billRecords[indexPath.row]
            if self.selectedTags.contains(indexPath.row) {
                let myIndex = self.selectedTags.index(of: indexPath.row)
                self.selectedTags.remove(at: myIndex!)
                billRecord.taxable = true
                let total = Double(billRecord.units!) * Double(billRecord.amount!)
                totaal -= total
                print("totaal taxable === \(totaal) ")
                self.SGST_CGST = totaal*tax/100
                let gst = self.SGST_CGST / 2
                let roundedValue = gst.rounded(.toNearestOrAwayFromZero)
                print("SGST_CGST ==== \(self.SGST_CGST / 2)")
                self.cgstLabel.text = "\(gst)"
                self.sgstLabel.text = "\(gst)"
            }else {
                self.selectedTags.append(indexPath.row)
                
                billRecord.taxable = true
                let total = Double(billRecord.units!) * Double(billRecord.amount!)
                totaal += total
                print("totaal taxable === \(totaal) ")
                print("taxxx ==== \(self.tax)")
                self.SGST_CGST = totaal*tax/100
                let gst = self.SGST_CGST / 2
                let roundedValue = gst.rounded(.up)
                print("SGST_CGST ==== \(roundedValue)")
                self.cgstLabel.text = "\(gst)"
                self.sgstLabel.text = "\(gst)"
            }
            tableView.reloadData()
        }else{
            
        }
        
    }
    
    var exchangeBillRecordsArr = [ExchangeBillrecords]()
    var billRecords = [BillRecord]()
    
    func exchangeBillrecords(bookingId:Int){
        let parameters : [String:Any] = [
            "bookingId" : "\(bookingId)",
            "financialYear" : self.currentFinancialYear!
        ]
        self.activityIndicatorBegin()
        let headers = header()
        
        //+918669696004
        //asdfgf1234$
        
        Alamofire.request(URLStatics.exchangeBillrecords,method: .get, parameters: parameters, headers: headers).responseJSON { response in
            print("response.request=\(String(describing: response.request))")  // original URL request
            print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
            print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
            //   self.activityIndicatorEnd()
            switch (response.result) {
                
            case .success:
                
                if let reply = response.result.value {
                    print("exchangeBillrecords JSON: >>>>>>>>>>>>>>>>>>>>>>>>>> \(reply)")
                    let mainResponse = JSON(reply)
                    var returnDict = [String:Any]()
                    // let returnObj = mainResponse["data"].array
                    
                    if let returnObj = mainResponse["data"].dictionaryObject {
                        returnDict = returnObj
                    }
                    self.billRecords.removeAll()
                    let billRec = returnDict["billRecords"] as! [[String:Any]]
                    for bill in billRec{
                        print("bill billRecords === \(String(describing: bill["amount"]))")
                        let billDict = bill["billRecordType"] as! [String:Any]
                        let currencyMasDict = bill["currencyMaster"] as! [String:Any]
                        let currencyMaster = CurrencyMaster1(id: currencyMasDict["id"] as? Int, currencyName: currencyMasDict["currencyName"] as? String, currencyCountry: currencyMasDict["currencyCountry"] as? String, currencyCode: currencyMasDict["currencyCode"] as? String)
                        
                        let billRecordType = BillRecordType(id: billDict["id"] as? Int, billRecordType: billDict["billRecordType"] as? String, status: billDict["status"] as? Bool, sacCode: billDict["sacCode"] as? String)
                        
                        
                        
                        let billl = BillRecord(id: bill["id"]! as? Int,
                                               billRecordType: billRecordType,
                                               currencyMaster: currencyMaster,
                                               exchangeRate: bill["exchangeRate"]! as? Int,
                                               units: bill["units"]! as? Int,
                                               amount: bill["amount"]! as? Int,
                                               taxable: bill["taxable"]! as? Bool,
                                               recordType: bill["recordType"]! as? String,
                                               sacCode: bill["sacCode"]! as? String,
                                               billRecordDescription: bill["description"] as? String,
                                               taxDetail: bill["taxDetail"] as? String,
                                               fleet: nil)
                        self.billRecords.append(billl)
                    }
                    print("billRecords count  \(String(describing: self.billRecords.count))")
                    let exchangeBill = ExchangeBillrecords(billRecords: nil, invoiceDate: returnDict["invoiceDate"] as? Int, invoiceNo: returnDict["invoiceNo"] as? String, invoiceGrossAmount: returnDict["invoiceGrossAmount"] as? Int)
                    self.exchangeBillRecordsArr.append(exchangeBill)
                }
                print("exchangeBillRecordsArr count  \(String(describing: self.exchangeBillRecordsArr.count))")
                self.tableView.reloadData()
                self.billingPreviewTableView.reloadData()
                self.sacCodeTextField.text =  self.billRecords[0].sacCode!
                self.bookingNo.text = self.exchangeBillRecordsArr[0].invoiceNo!
                self.subTotal = 0
                for bill in self.billRecords{
                    if bill.amount != nil && bill.units != nil{
                        let total = Double(bill.units!) * Double(bill.amount!)
                        
                        self.subTotal += total
                        self.currencyName = (bill.currencyMaster?.currencyName!)!
                        self.currencyCode = (bill.currencyMaster?.currencyCode!)!
                        self.currencyCountry = (bill.currencyMaster?.currencyCountry!)!
                        self.currId = (bill.currencyMaster?.id!)!
                    }
                    
                }
                self.subtotalLabel.text = "\(self.subTotal)"
                self.billPreSubTotal.text = "\(self.subTotal)"
                self.totalAmount = self.subTotal + self.SGST_CGST + self.SGST_CGST
                self.totalAmountLabel.text = "\(self.totalAmount)"
                self.billPreTotalAmount.text = "\(self.totalAmount)"
                self.activityIndicatorEnd()
                break
            case .failure(let error):
                self.activityIndicatorEnd()
                print("POST REQUEST ERROR  - \(error.localizedDescription)")
                print("=== REQUEST INFORMATION ===")
                //                    print("Status Code: \(response.response!.statusCode)")
                print("Request Payload: \(parameters)")
                print("===")
                break
            }
            
            
            
        }
        
    }
    
    
    var result = [[String : Any]]()
    func updateRecords(){
        
        for bill in self.billRecords{
        
            
            let billl : [String:Any] = [
                "description" : bill.billRecordDescription ?? "",
                "sacCode" : bill.sacCode!,
                "units": bill.units,
                "amount": bill.amount,
                "taxable": bill.taxable!,
                "exchangeRate" : bill.exchangeRate ?? 0,
                "recordType" : bill.recordType ?? "",
                "taxDetail" : "",
                "billRecordType" : [
                    "billRecordType" : bill.billRecordType!.billRecordType!,
                    "id" : bill.billRecordType!.id ?? 0,
                    "sacCode" : bill.billRecordType!.sacCode!,
                    "status" : bill.billRecordType!.status!
                ],"currencyMaster" : [
                    "currencyCode" : bill.currencyMaster!.currencyCode!,
                    "currencyCountry" : bill.currencyMaster!.currencyCountry!,
                    "currencyName" : bill.currencyMaster!.currencyName!,
                    "id" : bill.currencyMaster!.id!
                ]
            ]
            
            self.result.append(billl)
        }
        
        let dateTime = "\(String(describing: invoiceDate.text!))"
        
        let dateString = dateTime
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let s = dateFormatter.date(from: dateString)
        //print("validDateTime =>> \(String(describing: s!))")
        let dateTimestamp = Int(s!.timeIntervalSince1970)
        // print("dateTimestamp =>> \(dateTimestamp)")
        let invoiceDatee = Int((dateTimestamp * 1000))
        let parameters : [String:Any] = [
            "invoiceDate": "\(invoiceDatee)",
            "invoiceNo": "\(self.invoiceNo.text!)",
            "invoiceBranchYear": "",
            "invoiceGrossAmount": "\(self.totalAmountLabel.text!)",
            "taxGroupId": "",
            "billRecords" : self.result
        ]
  
        print("updateRecords parameters ========= \(parameters)")
        
        self.activityIndicatorBegin()
        let headers = header()
        
        //+918669696004
        //asdfgf1234$
        Alamofire.request(URLStatics.exchangeBillrecords+"bookingId=\(bookingId)&"+"financialYear=\(self.currentFinancialYear!)&"+"salesRecordType=\("SALES_INVOICE")",method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            print("response.request=\(String(describing: response.request))")  // original URL request
            print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
            print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
            //   self.activityIndicatorEnd()
            switch (response.result) {
                
            case .success:
                
                if let reply = response.result.value {
                    print("updateRecords JSON: >>>>>>>>>>>>>>>>>>>>>>>>>> \(reply)")
                    let mainResponse = JSON(reply)
                    var returnDict = [String:Any]()
                    // let returnObj = mainResponse["data"].array
                    
                    if let returnObj = mainResponse["data"].dictionaryObject {
                        returnDict = returnObj
                    }
               
                }
                self.activityIndicatorEnd()
                break
            case .failure(let error):
                self.activityIndicatorEnd()
                print("POST REQUEST ERROR  - \(error.localizedDescription)")
                print("=== REQUEST INFORMATION ===")
                //                    print("Status Code: \(response.response!.statusCode)")
                print("Request Payload: \(parameters)")
                print("===")
                break
            }
            
            
            
        }
        
    }
    
    
    /*
     
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
