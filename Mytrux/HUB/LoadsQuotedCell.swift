//
//  LoadsQuotedCell.swift
//  Mytrux
//
//  Created by Aboli on 21/06/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class LoadsQuotedCell: UITableViewCell {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var rankView: UIView!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var freightView: UIView!
    @IBOutlet weak var suView3: UIView!
    @IBOutlet weak var loadId: UILabel!
    @IBOutlet weak var from: UILabel!
    @IBOutlet weak var to: UILabel!
    @IBOutlet weak var rankCount: UILabel!
    @IBOutlet weak var registrationNo: UILabel!
    @IBOutlet weak var cagoType: UILabel!
    @IBOutlet weak var cargoWt: UILabel!
    @IBOutlet weak var availableTime: UILabel!
    @IBOutlet weak var availableDate: UILabel!
    @IBOutlet weak var noOfVehicle: UILabel!
    @IBOutlet weak var loadType: UILabel!
    @IBOutlet weak var freightAmount: UILabel!
    @IBOutlet weak var vehicleType: UILabel!
    @IBOutlet weak var bidNow: UIView!
    @IBOutlet weak var fleetId: UILabel!
    @IBOutlet weak var lowestBid: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.layer.cornerRadius = 4
        mainView.layer.shadowColor = UIColor.gray.cgColor
        mainView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        mainView.layer.shadowRadius = 5
        mainView.layer.shadowOpacity = 0.4
      
        // Initialization code
    }
    
   
    
    func circularView(circleViewArr:[UIView]){
        for circleView in circleViewArr{
            circleView.layoutIfNeeded()
            circleView.layer.cornerRadius = circleView.frame.size.height / 2
            circleView.layer.masksToBounds = true
            circleView.clipsToBounds = true
        }
    }
    
    override func setNeedsLayout() {
        circularView(circleViewArr: [cameraView,rankView])
        freightView.layer.cornerRadius = freightView.frame.height / 2

    }
    
  

    
//    override func prepareForReuse() {
//        view2.isHidden = true
//        view3.isHidden = true
//        rankView.isHidden = true
//        cameraView.isHidden = true
//        rankLabel.isHidden = true
//    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
