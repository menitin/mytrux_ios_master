//
//  BookingCell.swift
//  Mytrux
//
//  Created by Aboli on 09/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class BookingCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var bookingButton: UIView!
    @IBOutlet weak var viewPodButton: UIView!
    
    @IBOutlet weak var bookingButtonLabel: UILabel!
    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var bookingNo: UILabel!
    @IBOutlet weak var pickingDate: UILabel!
    @IBOutlet weak var pickupTime: UILabel!
    @IBOutlet weak var shipper: UILabel!
    @IBOutlet weak var delivery: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainView.layer.cornerRadius = 5
        mainView.layer.shadowColor = UIColor.gray.cgColor
        mainView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        mainView.layer.shadowRadius = 4
        mainView.layer.shadowOpacity = 0.4
      
        buttonCardArray(arrView: [bookingButton,viewPodButton])
    }
    
   func buttonCardArray(arrView:[UIView]){
        for button in arrView{
            button.layer.cornerRadius = button.frame.height / 2
        }
    }
}
