//
//  LoadsQuotedViewController.swift
//  Mytrux
//
//  Created by Aboli on 21/06/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import SideMenuSwift
import Alamofire
import SwiftyJSON

class LoadsQuotedViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {
    var cellTapped:Bool = false
    var currentRow = 0;
    var checked = Set<IndexPath>()
    var selectedIndex = IndexPath()
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var spBottomView: UIView!
    
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var suBottomView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var winImage: UIImageView!
    @IBOutlet weak var winLabel: UILabel!
    @IBOutlet weak var openLabel: UILabel!
    @IBOutlet weak var lostImage: UIImageView!
    @IBOutlet weak var lostLabel: UILabel!
    @IBOutlet weak var openImage: UIImageView!
    @IBOutlet weak var lostView: UIView!
    @IBOutlet weak var onHoldView: UIView!
    @IBOutlet weak var suOpenView: UIView!
    @IBOutlet weak var winView: UIView!
    @IBOutlet weak var openView: UIView!
    @IBOutlet weak var acceptedView: UIView!
    @IBOutlet weak var suOpenLabel: UILabel!
    @IBOutlet weak var acceptedLabel: UILabel!
    @IBOutlet weak var searchSeperator: UIImageView!
    @IBOutlet weak var searchWithDateView: UIView!
    @IBOutlet weak var rejectedView: UIView!
    @IBOutlet weak var rejectedLabel: UILabel!
    @IBOutlet weak var searchBar: UIView!
    @IBOutlet weak var onHoldLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var suOpenImage: UIImageView!
    @IBOutlet weak var onHoldImage: UIImageView!
    @IBOutlet weak var acceptedImage: UIImageView!
    @IBOutlet weak var rejectedImage: UIImageView!
    @IBOutlet weak var fromDate: UITextField!
    @IBOutlet weak var toDate: UITextField!
    
    var isOpenClicked = false
    var isWinClicked = false
    var isLostClicked = false
    var status = String()
    var loadsQuotedArray = [LoadQuotationDatum]()
    var pageNo = Int()
    var isDragging = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.clearCache(with: "second")
        tableView.delegate = self
        tableView.dataSource = self
        cardViewArray(arrView: [searchView])
        topBarCard(topBar: topBar)
        loginUserType = UserDefaults.standard.value(forKey: UserDefaultsContants.LOGIN_USER_TYPE)! as! String
        self.openLabel.textColor = UIColor.white
        openView.backgroundColor = UIColor(hexString:ColorConstants.RED)
        isOpenClicked = true
        openImage.image = UIImage(named: "open")
        openImage.image = openImage.image?.withRenderingMode(.alwaysTemplate)
        openImage.tintColor = UIColor.white
        isDragging = false
        suOpenLabel.textColor = UIColor.white
        suOpenView.backgroundColor = UIColor(hexString: ColorConstants.RED)
        suOpenImage.image = UIImage(named: "open")
        suOpenImage.image = suOpenImage.image?.withRenderingMode(.alwaysTemplate)
        suOpenImage.tintColor = UIColor.white
        status = "OPEN"
        searchWithDateView.isHidden = true
        searchSeperator.isHidden = true
        
        UIView.animate(withDuration: 0.8) {
            //                self.searchView.transform = CGAffineTransform(scaleX: 1, y: 1)
            // self.tableViewTopSpacer.constant = 50
            self.searchViewHeightConstraint = self.searchViewHeightConstraint.setMultiplier(multiplier: 0.06)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.loadsQuotedFromDate(_:)), name: .loadsQuotedFromDate, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.loadsQuotedToDate(_:)), name: .loadsQuotedToDate, object: nil)

        loadsQouted(search: self.searchTF.text!)
        
    }
    
    @objc func loadsQuotedFromDate(_ notification: NSNotification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        
        self.fromDate.text = (userInfo!["date"] as! String)
        
    }
    
    @objc func loadsQuotedToDate(_ notification: NSNotification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        
        self.toDate.text = (userInfo!["date"] as! String)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if loginUserType == UserStates.SERVICE_PROVIDER{
            self.spBottomView.isHidden = false
            self.suBottomView.isHidden = true
            self.titleLabel.text = "Loads Quoted"
            
        }else{
            self.spBottomView.isHidden = true
            self.suBottomView.isHidden = false
            self.titleLabel.text = "Fleets Invited"
        }
    }
    
    
    @IBAction func fromDateClicked(_ sender: Any) {
           self.picker(state: "loadsQuoted-FromDate")
    }
    
    @IBAction func toDateClicked(_ sender: Any) {
          self.picker(state: "loadsQuoted-ToDate")
    }
    
    @IBAction func menuClicked(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return loadsQuotedArray.count
    }
    
    var isSuOpenClicked = false
    var isOnHoldClicked = false
    var isAcceptedClicked = false
    var isRejectedClicked = false
    
    @IBAction func suOpenClicked(_ sender: Any) {
        if isSuOpenClicked == false{
            toggleSUViews(mainView: suOpenView, view1: onHoldView, view2: acceptedView, view3:rejectedView, mainLabel: suOpenLabel, label1: onHoldLabel, label2: acceptedLabel, label3: rejectedLabel)
            toggleSUImage(mainImage: suOpenImage, image1: onHoldImage, image2: acceptedImage, image3: rejectedImage, mainImageName: "open", imageName1: "open", imageName2: "winPdf", imageName3: "rejected")
        }else{
            
        }
    }
    
    
    
    @IBAction func onHoldClicked(_ sender: Any) {
        if isOnHoldClicked == false{
            toggleSUViews(mainView: onHoldView, view1: suOpenView, view2: acceptedView, view3:rejectedView, mainLabel: onHoldLabel, label1: suOpenLabel, label2: acceptedLabel, label3: rejectedLabel)
            toggleSUImage(mainImage: onHoldImage, image1: suOpenImage, image2: acceptedImage, image3: rejectedImage, mainImageName: "open", imageName1: "open", imageName2: "winPdf", imageName3: "rejected")
        }else{
            
        }
        
    }
    @IBAction func acceptedClicked(_ sender: Any) {
        if isAcceptedClicked == false{
            toggleSUViews(mainView: acceptedView, view1: suOpenView, view2: onHoldView, view3:rejectedView, mainLabel: acceptedLabel, label1: suOpenLabel, label2: onHoldLabel, label3: rejectedLabel)
            toggleSUImage(mainImage: acceptedImage, image1: suOpenImage, image2: onHoldImage, image3: rejectedImage, mainImageName: "winPdf", imageName1: "open", imageName2: "open", imageName3: "rejected")
        }else{
            
        }
    }
    
    @IBAction func rejectedClicked(_ sender: Any) {
        if isAcceptedClicked == false{
            toggleSUViews(mainView: rejectedView, view1: suOpenView, view2: onHoldView, view3: acceptedView, mainLabel: rejectedLabel, label1: suOpenLabel, label2: onHoldLabel, label3: acceptedLabel)
            toggleSUImage(mainImage: rejectedImage, image1: suOpenImage, image2: onHoldImage, image3: acceptedImage, mainImageName: "rejected", imageName1: "open", imageName2: "open", imageName3: "winPdf")
        }else{
            
        }
    }
    
    
    
    var loadObject = [String: Any]()
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "loadsQuotedCell") as! LoadsQuotedCell
        cell.view2.isHidden = true
        cell.view3.isHidden = true
        cell.suView3.isHidden = true
        cell.rankView.isHidden = true
        cell.cameraView.isHidden = true
        cell.rankLabel.isHidden = true
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(bidNowClicked(sender:)))
//        cell.bidNow.tag = indexPath.row
//        cell.bidNow.addGestureRecognizer(tapGesture)
        
        
        if loginUserType == UserStates.SERVICE_PROVIDER{
            cell.rankView.isHidden = false
            cell.cameraView.isHidden = false
            cell.rankLabel.isHidden = false
            if loadsQuotedArray.count != 0{
                
                let load = loadsQuotedArray[indexPath.row]
                
                cell.loadType.text = load.availableLoadBid?.loadType.loadType
                cell.vehicleType.text = load.availableLoadBid?.vehicleType.name
                cell.fleetId.text = load.availableFleetBid?.bidCode
                cell.loadId.text = load.availableLoadBid?.bidCode
                cell.from.text = load.availableLoadBid?.availableLocation
                cell.to.text = load.availableLoadBid?.destinationLocation
                cell.registrationNo.text = load.availableFleetBid?.registrationNo
                cell.cagoType.text = load.availableFleetBid?.cargoType.cargoType
                
                if load.quotationAmount != nil {
                    cell.freightAmount.text = "\((load.quotationAmount!))" + "0"
                }else{
                    cell.freightAmount.text = "00.00"
                }
                
                cell.noOfVehicle.text = "\(String(describing: load.availableFleetBid!.noOfVehicles))"
                
                if load.availableLoadBid?.packgs != nil{
                    cell.cargoWt.text = "\(String(describing: load.availableLoadBid!.capacityWeight))" + ".0" + " \(String(describing: load.availableLoadBid!.capacityWeightUnit.name!))" + " / " + "\(String(describing: load.availableLoadBid!.packgs!))"
                }else{
                    cell.cargoWt.text = "\(String(describing: load.availableLoadBid!.capacityWeight))" + ".0" + " \(String(describing: load.availableLoadBid?.capacityWeightUnit.name!))"
                }
                
                if load.lowestBidAmount != nil {
                    cell.lowestBid.text = "\(String(describing: load.lowestBidAmount!))" + "0"
                }else{
                    cell.lowestBid.text =  "00.00"
                }
                
                let bidNow = UITapGestureRecognizer(target: self, action: #selector(bidNow(recognizer:)))
                cell.bidNow.addGestureRecognizer(bidNow)
                cell.bidNow.tag = indexPath.row
                
                if load.bidRank! == 1 {
                    cell.rankView.backgroundColor = UIColor(hexString: ColorConstants.GREEN)
                }else{
                    cell.rankView.backgroundColor = UIColor(hexString: ColorConstants.YELLOW)
                }
                cell.rankCount.text = "\(load.bidRank!)"
                
                let availDate = Double((load.availableFleetBid?.availableDateTime)!) / 1000
                
                cell.availableDate.text = self.epochDate(epochDate: availDate)
                cell.availableTime.text = self.epochTimeSec(epochTimee: availDate)
                
                if isWinClicked || isLostClicked {
                cell.bidNow.isHidden = true
                }else{
                    cell.bidNow.isHidden = false
                }
                
                
            }
            
        }else{
            cell.rankView.isHidden = true
            cell.cameraView.isHidden = true
            cell.rankLabel.isHidden = true
        }

        
        if indexPath.row == currentRow {
            
            if loginUserType == UserStates.SERVICE_PROVIDER{
                if cellTapped == false {
                    cell.view2.isHidden = true
                    cell.view3.isHidden = true
                    self.tableView.rowHeight = 140
                }else{
                    cell.view2.isHidden = false
                    //                if closeClicked == true{
                    //                    cell.fleetView3.isHidden = true
                    //                    self.tableView.rowHeight = 360
                    //                }else{
                    cell.view3.isHidden = false
                    self.tableView.rowHeight = 420
                    //                }
                }
            }else{
                if cellTapped == false {
                    cell.view2.isHidden = true
                    cell.view3.isHidden = true
                    cell.suView3.isHidden = true
                    self.tableView.rowHeight = 140
                }else{
                    cell.view2.isHidden = false
      
                    cell.view3.isHidden = true
                    cell.suView3.isHidden = false
                    self.tableView.rowHeight = 420
                    //                }
                }
            }
            
        }
        return cell
    }
   
    @objc func bidNow(recognizer: UITapGestureRecognizer){
        
        let tag = recognizer.view?.tag
        
      
        for data in loadsQuotedArray{
            
            print("markerBidCode == \(String(describing: self.loadsQuotedArray[tag!].availableLoadBid?.bidCode))")
            if (data.availableLoadBid?.bidCode == self.loadsQuotedArray[tag!].availableLoadBid?.bidCode){
                print("data.bidCode === \(String(describing: data.availableLoadBid?.bidCode))")
                let timestap: Double = Double(data.availableFleetBid!.availableDateTime / 1000)
                let availableFromtime = self.epochTime(epochTimee: timestap)
                let availableFromdate = self.epochDate(epochDate: timestap)
                let timestamp: Double = Double(data.availableFleetBid!.closedBidTime / 1000)
                let availableTilltime = self.epochTime(epochTimee: timestamp)
                let availableTilldate = self.epochDate(epochDate: timestamp)
                //  let lowestBid = data.availableLoadBidQuotaions[0].quotationAmount
               let ownLastThreeBids = data.loadBidQuotaionDetails
                
                for bid in ownLastThreeBids!{
                    print("====== bid.quotationAmount ======= \(String(describing: bid.quotationAmount))")
                }
               
             
                
                loadObject = ["id": data.id!,"bidCode": data.availableLoadBid!.bidCode,
                              "vehicleType": data.availableLoadBid!.vehicleType.name,
                              "from": data.availableLoadBid!.availableLocation,
                              "to": data.availableLoadBid!.destinationLocation as Any,
                              "loadType": data.availableLoadBid!.loadType.loadType, "noOfVech":data.availableFleetBid!.noOfVehicles,
                              "cargoType": data.availableLoadBid!.cargoType.cargoType!,
                              "cargoWt": data.availableLoadBid!.capacityWeight ,
                              "packgs": data.availableLoadBid!.packgs!,
                              "availableFromdate": availableFromdate,
                              "availableFromtime": availableFromtime,
                              "availableTilltime": availableTilltime,
                              "availableTilldate": availableTilldate,
                              "kgName":data.availableLoadBid!.capacityWeightUnit.name!,
                              "lowestBid": 0,"closedBidTime":data.availableFleetBid!.closedBidTime ,
                              "bidRank": data.bidRank! ,
                              "availableLoadQuotations": "",
                              "ownLastThreeBids": ownLastThreeBids!,
                              "availableLoadBidQuotaionElement":data.loadBidQuotaionDetails!,"latitude":0.0,"longitude":0.0,"vehicleTypeId":0,"loadTypeId":0,"status":"LoadsQuoted"]
                
            }
        }
          sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "LoadDetailViewController") }, with: "seventh")
        sideMenuController?.setContentViewController(with: "seventh")
        NotificationCenter.default.post(name: Notification.Name("loadObj"),
                                        object:nil,userInfo: loadObject)
    }
    
    
    
//    @objc func bidNowClicked(sender: UITapGestureRecognizer) {
//        sideMenuController?.setContentViewController(with: "seventh")
//        print("bidNow clicked")
//
//
//        sideMenuController?.setContentViewController(with: "seventh")
//        NotificationCenter.default.post(name: Notification.Name("loadObj"),
//                                        object:nil,userInfo: loadObject)
//
//
//    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        // print("offsetY = \(offsetY) ======= contentHeight ==== \(contentHeight - scrollView.frame.size.height - 300)")
        
        if offsetY > contentHeight - scrollView.frame.size.height - 300 {
            
            pageNo += 1
            
            isDragging = true
            
            if self.loadsQuotedArray[0].count! != self.loadsQuotedArray.count{
                self.loadsQouted(search: self.searchTF.text!)
            }
            
        }else{
            isDragging = false
            
        }
        
    }
    
    
    
    var fromDatee = Int()
    var toDatee = Int()
    
    
    func loadsQouted(search:String){
        var parameters = [String:Any]()
        
        if fromDate.text! != "" && toDate.text! != ""{
            
            let dateTime = "\(String(describing: fromDate.text!))"
            
            let dateString = dateTime
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let s = dateFormatter.date(from: dateString)
            //print("validDateTime =>> \(String(describing: s!))")
            let dateTimestamp = Int(s!.timeIntervalSince1970)
            // print("dateTimestamp =>> \(dateTimestamp)")
            self.fromDatee = Int((dateTimestamp * 1000))
            
            
            let dateTime1 = "\(String(describing: toDate.text!))"
            let dateString1 = dateTime1
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "dd-MM-yyyy"
            let s1 = dateFormatter1.date(from: dateString1)
            //print("validDateTime =>> \(String(describing: s!))")
            let dateTimestamp1 = Int(s1!.timeIntervalSince1970)
            // print("dateTimestamp =>> \(dateTimestamp)")
            self.toDatee = Int((dateTimestamp1 * 1000))
            
            parameters = [
                "recordsPerPage" : 10,
                "pageNo" : "\(self.pageNo)",
                "search" : search,
                "status" : self.status,
                "financialYear": self.currentFinancialYear!,
                "toDate" : "\(self.toDatee)",
                "fromDate" : "\(self.fromDatee)"
            ]
            
        }else if fromDate.text! != ""  {
            
            let dateTime = "\(String(describing: fromDate.text!))"
            
            let dateString = dateTime
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let s = dateFormatter.date(from: dateString)
            //print("validDateTime =>> \(String(describing: s!))")
            let dateTimestamp = Int(s!.timeIntervalSince1970)
            // print("dateTimestamp =>> \(dateTimestamp)")
            self.fromDatee = Int((dateTimestamp * 1000))
            
            parameters = [
                "recordsPerPage" : 10,
                "pageNo" : "\(self.pageNo)",
                "search" : search,
                "status" : self.status,
                "financialYear": self.currentFinancialYear!,
                "toDate" : "",
                "fromDate" : "\(self.fromDatee)"
            ]
            
            
        }else  if toDate.text! != "" {
            let dateTime1 = "\(String(describing: toDate.text!))"
            let dateString1 = dateTime1
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "dd-MM-yyyy"
            let s1 = dateFormatter1.date(from: dateString1)
            //print("validDateTime =>> \(String(describing: s!))")
            let dateTimestamp1 = Int(s1!.timeIntervalSince1970)
            // print("dateTimestamp =>> \(dateTimestamp)")
            self.toDatee = Int((dateTimestamp1 * 1000))
            parameters = [
                "recordsPerPage" : 10,
                "pageNo" : "\(self.pageNo)",
                "search" : search,
                "status" : self.status,
                "financialYear": self.currentFinancialYear!,
                "toDate" : "\(self.toDatee)",
                "fromDate" : ""
            ]
        }else{
            parameters = [
                "recordsPerPage" : 10,
                "pageNo" : "\(self.pageNo)",
                "search" : search,
                "status" : self.status,
                "financialYear": self.currentFinancialYear!,
                "toDate" : "",
                "fromDate" : ""
            ]
        }
        
        
        
       
        if isDragging == false{
            self.activityIndicatorBegin()
        }
          let headers = header()
        
        Alamofire.request(URLStatics.quotations , method: .get, parameters: parameters,headers: headers)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                
                
                switch (response.result) {
                    
                case .success:
              
                    self.searchTF.text = ""
                    self.fromDate.text = ""
                    self.toDate.text = ""
                    
                    if self.isDragging == false{
                        self.activityIndicatorEnd()
                    }
                    
                    
                    if let reply = response.result.value {
                        var returnDict = [[String:Any]]()
                        print("======= JSON: Load Quoted ========  \(reply)")
                        let mainResponse = JSON(reply)
                        if let returnObj = mainResponse["data"].arrayObject as? [[String:Any]] {
                            returnDict = returnObj
                        }
                        
                        var availbaleFleetBid : AvailableBid?
                        var availFleetBidDict = [String:Any]()
                        var capacityFleetWeightUnitArr = [String:Any]()
                        var vehicleTypeFleet = [String:Any]()
                        var vehicleTypeeFleet : VehicleType?
                        var documentFleet : Logo?
                        var docsFleet : Logo?
                        var cargoTypeFleet : CargoType?
                        var cargoTypeArrayFleet = [String:Any]()
                        var documentSmallTypeFleet : [String:Any]?
                        var docuTypeFleet = [String:Any]()
                        var capacityWeightLoadFleet : CapacityWeightUnit?
                        var loadType1Fleet : LoadType1?
                        var loadTypeDictFleet = [String:Any]()
                        var currencyMasterFleet : CurrencyMaster?
                        var currencyMasterTypeFleet = [String:Any]()
                        
                        
                        
                        var capacityWeightUnitArr = [String:Any]()
                        var availLoadBidDict = [String:Any]()
                        var vehicleType = [String:Any]()
                        var availableLoadBid : AvailableBid?
                        var vehicleTypee : VehicleType?
                        var document : Logo?
                        var docs : Logo?
                        var cargoType : CargoType?
                        var cargoTypeArray = [String:Any]()
                        var documentSmallType : [String:Any]?
                        var docuType = [String:Any]()
                        var capacityWeightLoad : CapacityWeightUnit?
                        var loadType1 : LoadType1?
                        var loadTypeDict = [String:Any]()
                        var currencyMaster : CurrencyMaster?
                        var currencyMasterType = [String:Any]()
                        var loadBid = [[String:Any]]()
                        
                        var loadBidDetails : AvailableLoadBidQuotaion?
                        for data in returnDict{
                            
                           var loadBidQuotation = [AvailableLoadBidQuotaion]()
                     
                            //   availLoadBid
                            availLoadBidDict = data["availableLoadBid"] as! [String:Any]
                            vehicleType = availLoadBidDict["vehicleType"] as! [String:Any]
                            documentSmallType = vehicleType["documentSmall"] as? [String:Any]
                            docuType = vehicleType["document"] as! [String:Any]
                            
                            document = Logo(id: documentSmallType?["id"] as? Int,mimeType: documentSmallType?["mimeType"] as? String,type: ((documentSmallType?["type"] as? String)))
                            
                            docs = Logo(id: docuType["id"] as? Int, mimeType: docuType["mimeType"] as? String, type: ((docuType["type"] as? String)))
                            
                            
                            if let returnObject =
                                data["loadBidQuotaionDetails"] as? [[String:Any]] {
                                loadBid = returnObject
                            }
                            
                            for bid in loadBid{
                                print("quotationAmount ==== \( String(describing: bid["quotationAmount"] as? Double))")
                                loadBidDetails = AvailableLoadBidQuotaion(id: (bid["id"] as? Int)!, quotationAmount:Int(truncating: bid["quotationAmount"] as! NSNumber) , currencyMaster: nil, quotationEntryDate: (bid["quotationEntryDate"] as? Int)!)
                                loadBidQuotation.append(loadBidDetails!)
                            }
                            
                            print("loadBidQuotation?.count ====== \(String(describing: loadBidQuotation.count))")
                            
                            
                            vehicleTypee = VehicleType(id: vehicleType["id"] as! Int,
                                                       name: vehicleType["name"] as! String,
                                                       sequence: Int(vehicleType["sequence"] as! Double),
                                                       container: vehicleType["container"] as! Bool,
                                                       type: vehicleType["type"] as! String,
                                                       document: docs!, documentSmall: document!,
                                                       vehicleOrder: vehicleType["vehicleOrder"] as! Int,
                                                       weightCapacity: vehicleType["weightCapacity"] as? Int,
                                                       documentID: vehicleType["documentId"] as? Int,
                                                       smallDocID: vehicleType["smallDocId"] as? Int)
                            
                            cargoTypeArray = availLoadBidDict["cargoType"] as! [String : Any]
                            
                            cargoType = CargoType(id: cargoTypeArray["id"] as? Int,
                                                  cargoType: cargoTypeArray["cargoType"] as? String)
                            
                            if let capacityWeight = availLoadBidDict["capacityWeightUnit"] as? [String:Any] {
                                capacityWeightUnitArr = capacityWeight
                            }
                            
                            capacityWeightLoad = CapacityWeightUnit(id: capacityWeightUnitArr["id"] as? Int, name: capacityWeightUnitArr["name"] as? String, type: capacityWeightUnitArr["type"] as? String)
                            loadTypeDict = availLoadBidDict["loadType"] as! [String:Any]
                            loadType1 = LoadType1(id: loadTypeDict["id"] as! Int, loadType: loadTypeDict["loadType"] as! String, code: loadTypeDict["code"] as! String)
                            
                            currencyMasterType = data["currencyMaster"] as! [String : Any]
                            currencyMaster = CurrencyMaster(id: currencyMasterType["id"] as? Int, currencyCode: currencyMasterType["currencyCode"] as? String)
                            
                            availableLoadBid = AvailableBid(id: availLoadBidDict["id"] as! Int,
                                                            bidCode: availLoadBidDict["bidCode"] as! String,
                                                            creationDate: availLoadBidDict["creationDate"] as! Int,
                                                            closedBidTime: availLoadBidDict["closedBidTime"] as! Int,
                                                            availableLocation: availLoadBidDict["availableLocation"] as! String,
                                                            destinationLocation: availLoadBidDict["destinationLocation"] as? String,
                                                            comments: availLoadBidDict["comments"] as? String,
                                                            availableLatitude: availLoadBidDict["availableLatitude"] as? Double,
                                                            availableLongitude: availLoadBidDict["availableLongitude"] as? Double,
                                                            destinationLatitude: availLoadBidDict["destinationLatitude"] as? Double,
                                                            destinationLongitude: availLoadBidDict["destinationLongitude"] as? Double,
                                                            bidInviteCount: availLoadBidDict["bidInviteCount"] as? Int,
                                                            vehicleType: vehicleTypee!,
                                                            noOfVehicles: availLoadBidDict["noOfVehicles"] as! Int,
                                                            availableDateTime:  availLoadBidDict["availableDateTime"] as! Int,
                                                            cargoType: cargoType!,
                                                            capacityWeight: Int(truncating: availLoadBidDict["capacityWeight"] as! NSNumber),
                                                            capacityWeightUnit: capacityWeightLoad!,
                                                            loadType: loadType1!,
                                                            expectedFreight: availLoadBidDict["expectedFreight"] as? Int,
                                                            currencyMaster: nil,
                                                            status: availLoadBidDict["status"] as! String,
                                                            registrationNo: availLoadBidDict["registrationNo"] as? String,
                                                            packgs: availLoadBidDict["packgs"] as? Int,
                                                            containerNo: availLoadBidDict["containerNo"] as? String)
                            
                            
                            //  availbaleFleetBid
                            
                            availFleetBidDict = data["availableFleetBid"] as! [String:Any]
                            
                            vehicleTypeFleet = availFleetBidDict["vehicleType"] as! [String:Any]
                            documentSmallTypeFleet = vehicleTypeFleet["documentSmall"] as? [String:Any]
                            docuTypeFleet = vehicleTypeFleet["document"] as! [String:Any]
                            
                            documentFleet = Logo(id: documentSmallTypeFleet?["id"] as? Int,mimeType: documentSmallTypeFleet?["mimeType"] as? String,type: ((documentSmallTypeFleet?["type"] as? String)))
                            
                            docsFleet = Logo(id: docuTypeFleet["id"] as? Int, mimeType: docuTypeFleet["mimeType"] as? String, type: ((docuTypeFleet["type"] as? String)))
                            
                            vehicleTypeeFleet = VehicleType(id: vehicleTypeFleet["id"] as! Int,
                                                            name: vehicleTypeFleet["name"] as! String,
                                                            sequence: Int(vehicleTypeFleet["sequence"] as! Double),
                                                            container: vehicleTypeFleet["container"] as! Bool,
                                                            type: vehicleTypeFleet["type"] as! String,
                                                            document: docsFleet!, documentSmall: documentFleet!,
                                                            vehicleOrder: vehicleTypeFleet["vehicleOrder"] as! Int,
                                                            weightCapacity: vehicleTypeFleet["weightCapacity"] as? Int,
                                                            documentID: vehicleTypeFleet["documentId"] as? Int,
                                                            smallDocID: vehicleTypeFleet["smallDocId"] as? Int)
                            
                            cargoTypeArrayFleet = availFleetBidDict["cargoType"] as! [String : Any]
                            
                            cargoTypeFleet = CargoType(id: cargoTypeArrayFleet["id"] as? Int,
                                                       cargoType: cargoTypeArrayFleet["cargoType"] as? String)
                            
                            if let capacityWeight = availFleetBidDict["capacityWeightUnit"] as? [String:Any] {
                                capacityFleetWeightUnitArr = capacityWeight
                            }
                            
                            capacityWeightLoadFleet = CapacityWeightUnit(id: capacityFleetWeightUnitArr["id"] as? Int, name: capacityFleetWeightUnitArr["name"] as? String, type: capacityFleetWeightUnitArr["type"] as? String)
                            
                            
                            loadTypeDictFleet = availFleetBidDict["loadType"] as! [String:Any]
                            
                            loadType1Fleet = LoadType1(id: loadTypeDictFleet["id"] as! Int, loadType: loadTypeDictFleet["loadType"] as! String, code: loadTypeDictFleet["code"] as! String)
                            
                            //    currencyMasterTypeFleet = availFleetBidDict["currencyMaster"] as! [String : Any]
                            //
                            //    currencyMasterFleet = CurrencyMaster(id: currencyMasterTypeFleet["id"] as? Int, currencyCode: currencyMasterTypeFleet["currencyCode"] as? String)
                            
                            availbaleFleetBid = AvailableBid(id: availFleetBidDict["id"] as! Int,
                                                             bidCode: availFleetBidDict["bidCode"] as! String,
                                                             creationDate: availFleetBidDict["creationDate"] as! Int,
                                                             closedBidTime: availFleetBidDict["closedBidTime"] as! Int,
                                                             availableLocation: availFleetBidDict["availableLocation"] as! String,
                                                             destinationLocation: availFleetBidDict["destinationLocation"] as? String,
                                                             comments: availFleetBidDict["comments"] as? String,
                                                             availableLatitude: availFleetBidDict["availableLatitude"] as? Double,
                                                             availableLongitude: availFleetBidDict["availableLongitude"] as? Double,
                                                             destinationLatitude: availFleetBidDict["destinationLatitude"] as? Double,
                                                             destinationLongitude: availFleetBidDict["destinationLongitude"] as? Double,
                                                             bidInviteCount: availFleetBidDict["bidInviteCount"] as? Int,
                                                             vehicleType: vehicleTypeeFleet!,
                                                             noOfVehicles: availFleetBidDict["noOfVehicles"] as! Int,
                                                             availableDateTime:  availFleetBidDict["availableDateTime"] as! Int,
                                                             cargoType: cargoTypeFleet! ,
                                                             capacityWeight: availFleetBidDict["capacityWeight"] as! Int,
                                                             capacityWeightUnit: capacityWeightLoadFleet! ,
                                                             loadType: loadType1Fleet!,
                                                             expectedFreight: availFleetBidDict["expectedFreight"] as? Int,
                                                             currencyMaster: nil,
                                                             status: availFleetBidDict["status"] as! String,
                                                             registrationNo: availFleetBidDict["registrationNo"] as? String,
                                                             packgs: availFleetBidDict["packgs"] as? Int,
                                                             containerNo: availFleetBidDict["containerNo"] as? String)
                            
                            
                            
                            
                            let loadQUote = LoadQuotationDatum(id: data["id"] as? Int, availableLoadBid: availableLoadBid, fleetOwner: nil,
                                                               quotationAmount: data["quotationAmount"] as? Double, currencyMaster: currencyMaster, validDateTime: data["validDateTime"] as? Int, quotationEntryDate: data["quotationEntryDate"] as? Int,
                                                               quoteID: data["quoteID"] as? String,
                                                               status: data["status"] as? String, availableFleetBid: availbaleFleetBid, updateCount: data["updateCount"] as? Int, loadBidQuotaionDetails: loadBidQuotation, count: data["count"] as? Int, bidRank: data["bidRank"] as? Int, loadOwner: nil,
                                                               lowestBidAmount: data["lowestBidAmount"] as? Double,
                                                               quotationUpdatedDate: data["quotationUpdatedDate"] as? Double)
                            
                            
                            
                            if self.loadsQuotedArray.contains(where: { bid in bid.availableLoadBid?.bidCode == loadQUote.availableLoadBid?.bidCode }) {
                                
                            } else {
                                
                                self.loadsQuotedArray.append(loadQUote)
                            }
                            
                            
                        }
                        
                        print("self.loadsQuotedArray.count ====== \(self.loadsQuotedArray.count)")
                        
                        
                        self.tableView.reloadData()
                    }
                    break
                case .failure(let error):
                    
                    print("error \(error.localizedDescription)")
                    break
                }
        }
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if cellTapped{
            if indexPath as IndexPath == selectedIndex {
                return 420
                
            }
        }
        return 140
    }
    
    
    
    @IBAction func menuViewClicked(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath
        let selectedRowIndex = indexPath
        currentRow = selectedRowIndex.row
        cellTapped = !cellTapped
        self.tableView.reloadDataWithAutoSizingCellWorkAround()
    }
    
    var isSearchOpen = false
    
    @IBOutlet weak var searchViewHeightConstraint: NSLayoutConstraint!
    @IBAction func searchClicked(_ sender: UITapGestureRecognizer) {
        if isSearchOpen == false{
            searchWithDateView.isHidden = false
            searchSeperator.isHidden = false
            UIView.animate(withDuration: 0.8) {
                self.searchView.transform = CGAffineTransform.identity
                // self.tableViewTopSpacer.constant = 50
                self.searchViewHeightConstraint = self.searchViewHeightConstraint.setMultiplier(multiplier: 0.12)
            }
        }else{
            searchWithDateView.isHidden = true
            searchSeperator.isHidden = true
            UIView.animate(withDuration: 0.8) {
                //                self.searchView.transform = CGAffineTransform(scaleX: 1, y: 1)
                // self.tableViewTopSpacer.constant = 50
                self.searchViewHeightConstraint = self.searchViewHeightConstraint.setMultiplier(multiplier: 0.06)
            }
        }
        isSearchOpen = !isSearchOpen
        
        if searchTF.text != "" || fromDate.text != "" || toDate.text != "" {
            self.loadsQuotedArray.removeAll()
            pageNo = 0
            loadsQouted(search: self.searchTF.text!)
            //            fromDateTF.text = ""
            //            toDateTF.text = ""
        }
        
        
    }
    
    @IBAction func openClicked(_ sender: UITapGestureRecognizer) {
        if isOpenClicked == false{
            toggleViews(mainView: openView, view1: winView, view2: lostView, mainLabel: openLabel, label1: winLabel, label2: lostLabel)
            toggleImage(mainImage: openImage, image1: winImage, image2: lostImage, mainImageName: "open", imageName1: "winPdf", imageName2: "losePdf")
            isOpenClicked = true
            isLostClicked = false
            isWinClicked = false
            
        }else{
            
        }
        loadsQuotedArray.removeAll()
        pageNo = 0
         self.searchTF.text = ""
        self.fromDate.text = ""
        self.toDate.text = ""
        self.status = "OPEN"
        self.loadsQouted(search: self.searchTF.text!)
        self.tableView.reloadData()
        
    }
    @IBAction func winClicked(_ sender: UITapGestureRecognizer) {
        if isWinClicked == false{
            toggleViews(mainView: winView, view1: openView, view2: lostView, mainLabel: winLabel, label1: openLabel, label2: lostLabel)
            toggleImage(mainImage: winImage, image1: openImage, image2: lostImage, mainImageName: "winPdf", imageName1: "open", imageName2: "losePdf")
            isOpenClicked = false
            isLostClicked = false
            isWinClicked = true
        }else{
            
        }
         loadsQuotedArray.removeAll()
        pageNo = 0
        self.searchTF.text = ""
        self.fromDate.text = ""
        self.toDate.text = ""
        self.status = "WIN"
        self.loadsQouted(search: self.searchTF.text!)
        self.tableView.reloadData()
    }
    @IBAction func lostClicked(_ sender: UITapGestureRecognizer) {
        if isLostClicked == false{
            toggleViews(mainView: lostView, view1: winView, view2: openView, mainLabel: lostLabel, label1: winLabel, label2: openLabel)
            toggleImage(mainImage: lostImage, image1: winImage, image2: openImage, mainImageName: "losePdf", imageName1: "winPdf", imageName2: "open")
            isOpenClicked = false
            isLostClicked = true
            isWinClicked = false
        }else{
            
        }
         loadsQuotedArray.removeAll()
        pageNo = 0
        self.searchTF.text = ""
        self.fromDate.text = ""
        self.toDate.text = ""
        self.status = "LOSS"
        self.loadsQouted(search: self.searchTF.text!)
        self.tableView.reloadData()
    }
    
    
    func toggleViews(mainView:UIView,view1:UIView,view2:UIView,mainLabel:UILabel,label1:UILabel,label2:UILabel){
        mainView.backgroundColor = UIColor(hexString: ColorConstants.RED)
        label1.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
        label2.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
        mainLabel.textColor = UIColor.white
        
        view1.backgroundColor = UIColor.white
        view2.backgroundColor = UIColor.white
        
    }
    func toggleSUViews(mainView:UIView,view1:UIView,view2:UIView,view3:UIView,mainLabel:UILabel,label1:UILabel,label2:UILabel,label3:UILabel){
        mainView.backgroundColor = UIColor(hexString: ColorConstants.RED)
        label1.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
        label2.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
        label3.textColor = UIColor(hexString: ColorConstants.DARK_BLUE)
        mainLabel.textColor = UIColor.white
        
        view1.backgroundColor = UIColor.white
        view2.backgroundColor = UIColor.white
        view3.backgroundColor = UIColor.white
        
    }
    func toggleImage(mainImage:UIImageView,image1:UIImageView,image2:UIImageView,mainImageName:String,imageName1:String,imageName2:String){
        mainImage.image = UIImage(named: mainImageName)
        mainImage.image = mainImage.image?.withRenderingMode(.alwaysTemplate)
        mainImage.tintColor = UIColor.white
        image1.image = UIImage(named: imageName1)
        image1.image = image1.image?.withRenderingMode(.alwaysTemplate)
        image1.tintColor = UIColor(hexString: ColorConstants.RED)
        image2.image = UIImage(named: imageName2)
        image2.image = image2.image?.withRenderingMode(.alwaysTemplate)
        image2.tintColor = UIColor(hexString: ColorConstants.RED)
    }
    func toggleSUImage(mainImage:UIImageView,image1:UIImageView,image2:UIImageView,image3:UIImageView,mainImageName:String,imageName1:String,imageName2:String,imageName3:String){
        mainImage.image = UIImage(named: mainImageName)
        mainImage.image = mainImage.image?.withRenderingMode(.alwaysTemplate)
        mainImage.tintColor = UIColor.white
        image1.image = UIImage(named: imageName1)
        image1.image = image1.image?.withRenderingMode(.alwaysTemplate)
        image1.tintColor = UIColor(hexString: ColorConstants.RED)
        image2.image = UIImage(named: imageName2)
        image2.image = image2.image?.withRenderingMode(.alwaysTemplate)
        image2.tintColor = UIColor(hexString: ColorConstants.RED)
        image3.image = UIImage(named: imageName3)
        image3.image = image3.image?.withRenderingMode(.alwaysTemplate)
        image3.tintColor = UIColor(hexString: ColorConstants.RED)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
