//
//  PaymentConfirmationVC.swift
//  Mytrux
//
//  Created by Mytrux on 05/11/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import SideMenuSwift
import Alamofire
import SwiftyJSON
import Razorpay

//,RazorpayPaymentCompletionProtocol
class PaymentConfirmationVC: BaseViewController,RazorpayPaymentCompletionProtocol {
    

    func onPaymentError(_ code: Int32, description str: String) {
        let alertController = UIAlertController(title: "FAILURE", message: str, preferredStyle: UIAlertController.Style.alert)
        let cancelAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
           alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        print("code==== \(code)")
        print("str === \(str)")
    }

    @IBAction func cityClicked(_ sender: Any) {
        if stateTF.text == "" {
            let alertController = UIAlertController(title: "", message: "Select state to continue", preferredStyle: UIAlertController.Style.alert)
                   let cancelAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
                      alertController.addAction(cancelAction)
                   self.present(alertController, animated: true, completion: nil)
            return
        }else{
             
            self.picker(state: "PaymentCity")
           NotificationCenter.default.post(name: .paymentSelectedState, object: nil,userInfo: ["selectedState": self.stateTF.text!])
        }
    }
    @IBAction func stateClicked(_ sender: Any) {
        self.picker(state: "Payment")
    }
    func onPaymentSuccess(_ payment_id: String) {
//        let alertController = UIAlertController(title: "SUCCESS", message: "Payment Id \(payment_id)", preferredStyle: UIAlertController.Style.alert)
//        let cancelAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
//        alertController.addAction(cancelAction)
//        self.present(alertController, animated: true, completion: nil)     //pay_DdPglh2JbdGGNf
        print("payment_id === \(payment_id)")
        self.paymentId = payment_id
       addWallet()

    }
    
  
    
    
   var razorpay: Razorpay!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var companyView: UIView!
    @IBOutlet weak var billingAddView: UIView!
    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var pincodeView: UIView!
    @IBOutlet weak var panNoView: UIView!
    @IBOutlet weak var dottedView1: UIView!
    @IBOutlet weak var dottedView2: UIView!
    
    @IBOutlet weak var scrollVieww: UIScrollView!
    @IBOutlet weak var invoiceView: UIView!
    @IBOutlet weak var billingCustomerView: UIView!
    @IBOutlet weak var billingCustomer: UIView!
    @IBOutlet weak var nameAddress: UILabel!
    @IBOutlet weak var billingCustomerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var amountInWords: UILabel!
    @IBOutlet weak var sgstPercent: UILabel!
    @IBOutlet weak var paynowBtn: UIView!
    @IBOutlet weak var checkmark: UIImageView!
    @IBOutlet weak var igstPercent: UILabel!
    @IBOutlet weak var totalPaybleAmount: UILabel!
    @IBOutlet weak var panNo: UILabel!
    @IBOutlet weak var gstinLabel: UILabel!
    @IBOutlet weak var cgstPercent: UILabel!
    @IBOutlet weak var transactionView: UIView!
    @IBOutlet weak var companyTF: UITextField!
    @IBOutlet weak var billingAddress: UITextField!
    @IBOutlet weak var stateTF: UITextField!
    @IBOutlet weak var cityTF: UITextField!
    @IBOutlet weak var pinCodeTF: UITextField!
    @IBOutlet weak var sgstAmount: UILabel!
    @IBOutlet weak var panNoTF: UITextField!
    @IBOutlet weak var chargesPerTrans: UILabel!
    @IBOutlet weak var transactions: UILabel!
    @IBOutlet weak var users: UILabel!
    @IBOutlet weak var selectedPlanAmount: UILabel!
    @IBOutlet weak var igstAmount: UILabel!
    @IBOutlet weak var cgstAmount: UILabel!
    var viewArray = [UIView]()
    var tax = Double()
    var paymentId = String()
    var isSameOfTheAbove = true
    override func viewDidLoad() {
        super.viewDidLoad()
        topBarCard(topBar: topBar)
        cardViewArray(arrView: [invoiceView,billingCustomerView,transactionView
        ])
        buttonCardView(button: paynowBtn)
       razorpay = Razorpay.initWithKey("rzp_test_7wPPLk8t94W8i2", andDelegate: self)
        billingCustomerHeightConstraint = self.billingCustomerHeightConstraint.setMultiplier(multiplier: 0.1)
      viewArray = [companyView,billingAddView,stateView,cityView,pincodeView,panNoView]
        for vieew in viewArray
        {
            vieew.isHidden = true
            
        }
         addDashedBorder(view: dottedView2)
         addDashedBorder(view: dottedView1)
        self.billingCustomerView.layoutIfNeeded()
        getCompanyDetails()
        NotificationCenter.default.addObserver(self, selector: #selector(self.paymentPlan(notification:)), name: .paymentPlan, object: nil)
        tax = 9
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.paymentState(notification:)), name: .paymentState, object: nil)
           NotificationCenter.default.addObserver(self, selector: #selector(self.paymentCity(notification:)), name: .paymentCity, object: nil)
        scrollVieww.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        // Do any additional setup after loading the view.
    }
    
    
    @objc func paymentCity(notification: Notification) {
        
         let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        let city = userInfo!["city"] as! String
         print("PaymentCity========= \(city)")
        self.cityTF.text = city
    }
    
    @objc func paymentState(notification: Notification) {
        
         let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        let state = userInfo!["state"] as! String
        if state != self.companyDetails!.address!.state!{
            self.tax = 18
            self.selectedPlanAmount.text = "\(Double(self.paymentPlan!.amount!))" + "0"
                  let total = Double(self.paymentPlan!.amount!)
                  self.amount = total
                  self.cgstPercent.text = "0" + "%"
                  self.sgstPercent.text = "0" + "%"
                  let SGST_CGST = total*self.tax/100
                               let gst = SGST_CGST
                  self.cgstAmount.text = "00.00"
                  self.sgstAmount.text = "00.00"
                  self.sgst_cgst = gst
                  self.igstAmount.text = "\(gst)"
            self.igstPercent.text = "\(self.tax) %"
                  let totalAmount = total + SGST_CGST
                  self.totalAmounnt = totalAmount
                  self.totalPaybleAmount.text = "\(totalAmount)"
                  let formatter = NumberFormatter()
                  formatter.numberStyle = NumberFormatter.Style.spellOut
                  let spellOutText = formatter.string(for: totalAmount)!
                  print("============")
                  print(spellOutText) // prints: two thousand eighteen
                  self.amountInWords.text = spellOutText.capitalized
        }else{
            self.tax = 9
            self.selectedPlanAmount.text = "\(Double(self.paymentPlan!.amount!))" + "0"
                   let total = Double(self.paymentPlan!.amount!)
                   self.amount = total
                   self.cgstPercent.text = "\(self.tax)" + "%"
                   self.sgstPercent.text = "\(self.tax)" + "%"
                   let SGST_CGST = total*self.tax/100
                                let gst = SGST_CGST
                   self.cgstAmount.text = "\(gst)"
                   self.sgstAmount.text = "\(gst)"
                   self.sgst_cgst = gst
                   self.igstAmount.text = "00.00"
                   self.igstPercent.text = "0 %"
                   let totalAmount = total + SGST_CGST + SGST_CGST
                   self.totalAmounnt = totalAmount
                   self.totalPaybleAmount.text = "\(totalAmount)"
                   let formatter = NumberFormatter()
                   formatter.numberStyle = NumberFormatter.Style.spellOut
                   let spellOutText = formatter.string(for: totalAmount)!
                   print("============")
                   print(spellOutText) // prints: two thousand eighteen
                   self.amountInWords.text = spellOutText.capitalized
        }
         print("paymentState========= \(state)")
        self.stateTF.text = state
    }
    
    func addWallet(){

        var parameters = [String:Any]()

        parameters = [
            "companyName" : "\(String(describing: self.companyDetails!.name!))",
            "billPartyCompanyName" : "\(String(describing: self.companyDetails!.name!))",
            "billPartyCompanyAddress" : [
                "address":"\(String(describing: self.companyDetails!.address!.address!))",
                "city":"\(String(describing: self.companyDetails!.address!.city!))",
                "state":"\(String(describing: self.companyDetails!.address!.state!))",
                "pincode":"\(String(describing: self.companyDetails!.address!.pincode!))",
                "country":"\(String(describing: self.companyDetails!.address!.country!))"
            ],
            "billPartyCompanyContactPerson" : [
                "fullName":"\(String(describing: self.companyDetails!.contactPersons![0].fullName))",
                "contactNo":"\(String(describing: self.companyDetails!.contactPersons![0].contactNo))",
                "email":"\(String(describing: self.companyDetails!.contactPersons![0].email))"
            ],
            "appliedTaxDetails":"[{\"taxName\":\"CGST\",\"taxRate\":\(self.tax),\"taxAmount\":\"\(self.sgst_cgst)\"},{\"taxName\":\"SGST\",\"taxRate\":\(self.tax),\"taxAmount\":\"\(self.sgst_cgst)\"},{\"taxName\":\"IGST\",\"taxRate\":0,\"taxAmount\":\"0.00\"}]",
            "paymentId":"\(self.paymentId)",
            "appType":"HUB",
            "planId":40,
            "currency":"INR",
            "amountWithTax":Int(self.totalAmounnt),
        ]

        let header = self.header()
               
               Alamofire.request(URLStatics.addToWallet, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header)
                   .responseJSON { response in
                       print("response.request=\(String(describing: response.request))")  // original URL request
                       print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                       print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                       let resultNSString = NSString(data: response.request!.httpBody!, encoding: String.Encoding.utf8.rawValue)!
                       print("response.request?.httpBody = \(resultNSString)")
                       switch (response.result) {
                       case .success:
                             var returnDict = [String:Any]()
                           if let reply = response.result.value {
                               print("JSON: addToWallet addToWallet ======== \(reply)")
                               let mainResponse = JSON(reply)
                            print("\(mainResponse["message"].stringValue)")
                            let alertController = UIAlertController(title: "\(mainResponse["message"].stringValue)", message: "", preferredStyle: UIAlertController.Style.alert)
                            let cancelAction = UIAlertAction(title: "OK", style: .cancel) { (alert) in
                                       self.sideMenuController?.hideMenu()
                                 SideMenuController.preferences.basic.defaultCacheKey = "default"
                                self.sideMenuController?.setContentViewController(with: "default")

                            }
                            
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion: nil)
                           }
                           
                           break
                       case .failure(let error):
                           
                           print("error \(error.localizedDescription)")
                           break
                       }
               }
               
               
    }
    
    var sgst_cgst = Double()
    var amount = Double()
    var paymentPlan : PaymentPlan?
    var totalAmounnt = Double()
    
    @objc func paymentPlan(notification: Notification) {
         let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        paymentPlan = userInfo!["paymentPlan"] as? PaymentPlan
        
        self.chargesPerTrans.text = "\(String(describing: self.paymentPlan!.perTransactionFee!))"
        self.transactions.text = "\(String(describing: self.paymentPlan!.totalTransaction!))"
        self.users.text = "\(String(describing: self.paymentPlan!.maxUser!))"
        self.selectedPlanAmount.text = "\(Double(self.paymentPlan!.amount!))" + "0"
        let total = Double(self.paymentPlan!.amount!)
        self.amount = total
        self.cgstPercent.text = "\(self.tax)" + "%"
        self.sgstPercent.text = "\(self.tax)" + "%"
        let SGST_CGST = total*self.tax/100
                     let gst = SGST_CGST
        self.cgstAmount.text = "\(gst)"
        self.sgstAmount.text = "\(gst)"
        self.sgst_cgst = gst
        self.igstAmount.text = "00.00"
        self.igstPercent.text = "0 %"
        let totalAmount = total + SGST_CGST + SGST_CGST
        self.totalAmounnt = totalAmount
        self.totalPaybleAmount.text = "\(totalAmount)"
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.spellOut
        let spellOutText = formatter.string(for: totalAmount)!
        print("============")
        print(spellOutText) // prints: two thousand eighteen
        self.amountInWords.text = spellOutText.capitalized

        
    }
    
    
    @IBAction func backClicked(_ sender: Any) {
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "WalletViewController") }, with: "fifth")
        sideMenuController?.setContentViewController(with: "fifth")
    }
    
    @IBAction func checkmarkClicked(_ sender: Any) {
        print("checkmarkClicked")
        if checkmark.image == UIImage(named: "checkedBox"){
            checkmark.image = UIImage(named: "checkbox_empty")
            for vieew in viewArray
            {
                vieew.isHidden = false
                
            }
            billingCustomerHeightConstraint = self.billingCustomerHeightConstraint.setMultiplier(multiplier: 0.52)

            self.billingCustomerView.layoutIfNeeded()
        }else{
            for vieew in viewArray
                            {
                                vieew.isHidden = true
                                
                            }
           billingCustomerHeightConstraint = self.billingCustomerHeightConstraint.setMultiplier(multiplier: 0.1)
            checkmark.image = UIImage(named: "checkedBox")
               self.billingCustomerView.layoutIfNeeded()
        }
    }
    
    var companyDetails : CompanyDetail? = nil
    
    func getCompanyDetails() {
      
       let headers = self.header()
        let userType = userDefaults.value(forKey: LoginDataConstants.userType) as! String
        var type = String()
        if userType == "FLEET_ADMIN" || userType == "FLEET_USER"{
            type = "fleet"
        }else if userType == "LOAD_ADMIN" || userType == "LOAD_USER"{
            type = "load"
        }
        
        var parameters = [String:Any]()
        
        parameters = [
            "financialYear":self.currentFinancialYear!
        ]
        Alamofire.request(URLStatics.companyDetails + "\(type)", method: .get, parameters: parameters, headers: headers).responseJSON { response in
                   print("response.request=\(String(describing: response.request))")  // original URL request
                   print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                   print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                   
                   if let reply = response.result.value {
                       
                       let json = JSON(reply)
                       print(" companyDetails  json =======  : \(json)")
                    let typeOfCompDict = json["typeOfCompany"].dictionary
                    
                    let contactPersonArray = json["contactPersons"].arrayObject as? [[String:Any]]
                    var contactArray = [ContactPerson]()
                    for contact in contactPersonArray!{
                        print("fullName=========\(String(describing: contact["fullName"] as! String))")
                        let con = ContactPerson(id: contact["id"] as! Int, fullName: contact["fullName"] as! String, contactNo: contact["contactNo"] as! String, email: contact["email"] as! String, main: contact["main"] as! Bool)
                        contactArray.append(con)
                    }
                    print("contactArray=========\(String(describing: contactArray.count))")
                    
                    let typeOfComp = TypeOfCompany1(id: typeOfCompDict?["id"]?.int, companyType: typeOfCompDict?["companyType"]?.string, companyCategories: typeOfCompDict?["typeOfCompDict"]?.string)
                    let addressdict = json["address"].dictionary
                    let adress = Address1(id: addressdict!["id"]?.int, address: addressdict!["address"]?.string, landmark: addressdict!["landmark"]?.string, city: addressdict!["city"]?.string, state: addressdict!["state"]?.string, pincode: addressdict!["pincode"]?.string, country: addressdict!["country"]?.string)
                    let comp = CompanyDetail(id: json["id"].int, name: json["name"].string, registrationNo: json["registrationNo"].string, panNo: json["panNo"].string, fax: json["fax"].string, telephone: json["telephone"].string, typeOfCompany: typeOfComp, fleetAdditionalInformation: nil, address: adress, userCount: json["userCount"].int, activeUserCount: json["activeUserCount"].int, companyCode: json["companyCode"].string, logo: nil, gstTax: json["gstTax"].int, gstTaxRegNo: json["gstTaxRegNo"].string, jurisdiction: json["jurisdiction"].string, timeZone: json["timeZone"].string, vatNo: json["vatNo"].string, iecNo: json["iecNo"].string, cinNo: json["cinNo"].string, cstNo: json["cstNo"].string, registrationDate: json["registrationDate"].int, expiredType: json["expiredType"].string, subscriptionStartDate: json["subscriptionStartDate"].int, subscriptionEndDate: json["subscriptionEndDate"].int, perTransactionFee: json["perTransactionFee"].int, smsCharge: json["smsCharge"].int, currentBalance: json["currentBalance"].int, bookingCount: json["bookingCount"].int, purchasePaymentCount: json["purchasePaymentCount"].int, purchaseReceiptCount: json["purchaseReceiptCount"].int, routeCount: json["routeCount"].int, smsNotification: json["smsNotification"].string, maxUser: json["maxUser"].int, maxUserType: json["maxUserType"].string, users: nil, taxs: nil, contactPersons: contactArray, status: json["status"].string, fleetDevicePassword: json["fleetDevicePassword"].string, contractMasterCount: json["contractMasterCount"].int, goCount: json["goCount"].int, proCount: json["proCount"].int, profileMailCount: json["profileMailCount"].int, ewbFlagStr: json["ewbFlagStr"].string, walletAmount: json["walletAmount"].int, hubMaxUser: nil, allowTransaction: nil, remainsTr: nil, hubPlanID: nil, hubTransactionCost: nil, thirdPartyAuthDateTime: nil, exPurchasePayCount: nil, exPurchaseReceiptCount: nil, exWalletValidFrom: nil, exWalletValidTill: nil, exWalletValidDays: nil, exPlanType: json["exPlanType"].string, exPackType: json["exPackType"].string, eLrTerms: json["eLrTerms"].string, salesInvoiceTerms: json["salesInvoiceTerms"].string, userCounts: nil)
                    self.companyDetails = comp
                    let address = json["address"].dictionary
                    let name = json["name"].stringValue + " " + address!["address"]!.stringValue + " "
                    let state = address!["city"]!.stringValue + " " + address!["state"]!.stringValue
                    self.nameAddress.text = name + state
                    self.gstinLabel.text = json["gstTaxRegNo"].stringValue
                    self.panNo.text = json["panNo"].stringValue
                    
                    
                    
                    
                    
                   }
               }
    }
 
    
    @IBAction func paynow(_ sender: Any) {
        
   
        let options: [String:Any] = [
         "amount": "\(self.totalAmounnt * 100)", //This is in currency subunits. 100 = 100 paise= INR 1.
          "currency": "INR",//We support more that 92 international currencies.
            "description": "Order no",
            "name": "Mytrux Transporting Service",
        ]
    
        razorpay.open(options, display: self)
   }
//
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension NSLayoutConstraint {

    static func setMultiplier(_ multiplier: CGFloat, of constraint: inout NSLayoutConstraint) {
        NSLayoutConstraint.deactivate([constraint])

        let newConstraint = NSLayoutConstraint(item: constraint.firstItem, attribute: constraint.firstAttribute, relatedBy: constraint.relation, toItem: constraint.secondItem, attribute: constraint.secondAttribute, multiplier: multiplier, constant: constraint.constant)

        newConstraint.priority = constraint.priority
        newConstraint.shouldBeArchived = constraint.shouldBeArchived
        newConstraint.identifier = constraint.identifier

        NSLayoutConstraint.activate([newConstraint])
        constraint = newConstraint
    }

}
