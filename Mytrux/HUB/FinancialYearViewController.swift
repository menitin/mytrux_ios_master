//
//  FinancialYearViewController.swift
//  Mytrux
//
//  Created by Aboli on 11/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class FinancialYearViewController: BaseViewController {
    
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var okBtn: UIView!
    @IBOutlet weak var cancelBtn: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var financialYearCollectionView: UICollectionView!
    var viewArr = [UIView]()
    override func viewDidLoad() {
        super.viewDidLoad()
    
        buttonCardArray(arrView: [cancelBtn,okBtn])
        mainView.layer.cornerRadius = 10
        mainView.layer.shadowColor = UIColor.gray.cgColor
        mainView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        mainView.layer.shadowRadius = 5
        mainView.layer.shadowOpacity = 0.4
        viewArr = [view1,view2,view3]
        for view in viewArr {
            view.layer.borderWidth = 1
            view.layer.cornerRadius = 20
            view.layer.borderColor = UIColor(hexString: ColorConstants.DARK_BLUE).cgColor
        }

   }
    
    var isView1selected = true
    var isView2Selected = false
    var isView3Selected = false
    
    @IBAction func view1819Clicked(_ sender: Any) {
        isView1selected = true
        if isView1selected == true {
         self.toggleSelection(mainView: view1, view1: view2, view2: view3)
        }
    }
    @IBAction func view1920Clicked(_ sender: Any) {
        isView2Selected = true
         isView1selected = false
         isView3Selected = false
        if isView2Selected == true {
         toggleSelection(mainView: view2, view1: view1, view2: view3)
        }
    }
    
    @IBAction func view2021Clicked(_ sender: Any) {
        isView2Selected = false
        isView1selected = false
        isView3Selected = true
        if isView3Selected == true {
         toggleSelection(mainView: view3, view1: view1, view2: view2)
        }
    }
    
    func toggleSelection(mainView:UIView,view1:UIView,view2:UIView) {
       
     mainView.backgroundColor = UIColor(hexString: ColorConstants.LIGHT_BLUE)
        mainView.layer.borderWidth = 0
      view1.backgroundColor = UIColor.white
     view1.layer.borderWidth = 1
       view2.backgroundColor = UIColor.white
        view2.layer.borderWidth = 1

    }
    
    @IBAction func cancelClicked(_ sender: Any) {
      self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
