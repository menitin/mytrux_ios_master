//
//  TransactionTableViewCell.swift
//  Mytrux
//
//  Created by Aboli on 27/06/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {

    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var circularView: UIView!
    @IBOutlet weak var fleetId: UILabel!
    @IBOutlet weak var transactionDate: UILabel!
    @IBOutlet weak var vehicleNo: UILabel!
    @IBOutlet weak var transactionAmount: UILabel!
    @IBOutlet weak var loadId: UILabel!
    @IBOutlet weak var companyFullName: UILabel!
    @IBOutlet weak var driverName: UILabel!
    @IBOutlet weak var bookingNo: UILabel!
    @IBOutlet weak var bookingDate: UILabel!
    @IBOutlet weak var driverMobileNumber: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.layer.cornerRadius = 4
        mainView.layer.shadowColor = UIColor.gray.cgColor
        mainView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        mainView.layer.shadowRadius = 5
        mainView.layer.shadowOpacity = 0.4
        // Initialization code
    }
    override func setNeedsLayout() {
        circularView.layoutIfNeeded()
        circularView.layer.cornerRadius = circularView.frame.size.height / 2
        circularView.layer.masksToBounds = true
        circularView.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
