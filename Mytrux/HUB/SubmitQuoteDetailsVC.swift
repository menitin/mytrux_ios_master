//
//  SubmitQuoteDetailsVC.swift
//  Mytrux
//
//  Created by Mytrux on 23/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import SideMenuSwift
import Alamofire
import SwiftyJSON

class SubmitQuoteDetailsVC: BaseViewController {

    
    @IBOutlet weak var cargoTypeLabel: UILabel!
    @IBOutlet weak var fleetIdLabel: UILabel!
    @IBOutlet weak var validLabel: UILabel!
    @IBOutlet weak var rangeLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var vehicleTypeLabel: UILabel!
    
     var timer = Timer()
    var seconds : Int64 = 0
    
    @IBOutlet weak var loadIdLabel: UILabel!
    @IBOutlet weak var loadTypeLabel: UILabel!
    @IBOutlet weak var loadDetailsFromLabel: UILabel!
    @IBOutlet weak var loadDetailsToLabel: UILabel!
    @IBOutlet weak var loadDetailsCargoWt: UILabel!
    @IBOutlet weak var loadDetailsNoOfVech: UILabel!
    @IBOutlet weak var loadDetailsAvailDate: UILabel!
    @IBOutlet weak var loadDetailsAvailTime: UILabel!
    @IBOutlet weak var remainingDaysLabel: UILabel!
    @IBOutlet weak var remainingHrsLabel: UILabel!
    
    
    
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var remainingTimeView: UIView!
    @IBOutlet weak var confirmBtn: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        topBarCard(topBar: topBar)
        cardViewArray(arrView: [mainView])
        self.remainingDaysLabel.isHidden = true
        remainingTimeView.layer.cornerRadius = remainingTimeView.frame.height / 2
        confirmBtn.layer.cornerRadius = 5
           NotificationCenter.default.addObserver(self, selector: #selector(self.submitQuoteClickedFleetInfo(notification:)), name: Notification.Name("SubmitQuoteClickedFleetInfo"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    var postLoadQuotationPara = [String:Any]()
    var validDateTime = Int()
    var loadBidId = Int()
    var fleetBidId = Int()
    var quotationAmount = String()
    var id = Int()
    var isAvailableLoadBidQuotation = Bool()
    
    var closeBidTime = Int()
    @objc func submitQuoteClickedFleetInfo(notification: Notification) {
        
       let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
    self.cargoTypeLabel.text = (userInfo!["vehicleDetailsCargoType"]! as! String)
        self.fleetIdLabel.text = (userInfo!["fleetId"]! as! String)
        self.vehicleTypeLabel.text = (userInfo!["vehicleType"]! as! String)
        self.amountLabel.text = (userInfo!["amount"]! as! String)
        self.rangeLabel.text = "\((userInfo!["range"]! as! String)) " + " \((userInfo!["time"]! as! String))"
        self.validLabel.text = (userInfo!["valid"]! as! String)
        self.quotationAmount = (userInfo!["amount"]! as! String)
        
     
    
        
        self.loadIdLabel.text = (userInfo!["loadDetailsLoadId"]! as! String)
        self.loadTypeLabel.text = (userInfo!["loadDetailsLoadType"]! as! String)
        self.loadDetailsCargoWt.text = (userInfo!["loadDetailsCargoWt"]! as! String)
        self.loadDetailsFromLabel.text = (userInfo!["loadDetailsFrom"]! as! String)
        self.loadDetailsToLabel.text = (userInfo!["loadDetailsTo"]! as! String)
        self.loadDetailsNoOfVech.text = (userInfo!["loadDetailsNoOfVech"]! as! String)
        self.loadDetailsAvailDate.text = (userInfo!["loadDetailsAvailDate"]! as! String)
        self.loadDetailsAvailTime.text = (userInfo!["loadDetailsAvailTime"]! as! String)
        loadBidId = (userInfo!["loadBidId"]! as! Int)
        fleetBidId = (userInfo!["fleetBidId"]! as! Int)
        id = (userInfo!["id"]! as! Int)
        isAvailableLoadBidQuotation = (userInfo!["isAvailableLoadBidQuotation"]! as! Bool)
      
        let closeBidTime = (userInfo!["closedBidTime"] as! Int)
        self.closeBidTime = closeBidTime
        let closeBidFinalTime = Int64(closeBidTime) / 1000
        let Timestamp = Date().toMillis()
        print("closeBidTime =>> \(closeBidTime)")
        print("timestamp =>> \(String(describing: Timestamp))")
        let time = Timestamp! - closeBidFinalTime
        
        let dateTime = "\(self.validLabel.text!)" + " \(self.rangeLabel.text!)"
         print("dateTime =>> \(dateTime)")
        
        let dateString = dateTime
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm  a"
        let s = dateFormatter.date(from: dateString)
       
       // self.validDateTime = Double(convertDateFormatter(date: dateTime))!
        
        print("validDateTime =>> \(String(describing: s!))")
        let dateTimestamp = Int(s!.timeIntervalSince1970)
        print("dateTimestamp =>> \(dateTimestamp)")
        self.validDateTime = (dateTimestamp * 1000)
        let sec = time/1000
        print("sec = \(sec)")
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
        seconds = time
        
 
        
        
       // self.fleetIdLabel.text = (userInfo!["fleetId"]! as! String)
        
        print("======== > loadDetailsLoadId ======== >  \(userInfo!["loadDetailsLoadId"]!)")
        print("======== > loadDetailsLoadType ======== >  \(userInfo!["loadDetailsLoadType"]!)")
        print("======== > loadDetailsFrom ======== >  \(userInfo!["loadDetailsFrom"]!)")
    }
    

//    func getDateFromString(dateStr: String) -> Date?
//    {
//
//    }
    
    
    
    var noOfDays = 0
    @objc func updateTimer() {
        let time = Int64(closeBidTime) - Date().toMillis()
      
        let sec = time/1000
        let min = sec/60
        let hrs = min/60
        let days = hrs/24
        let finalSec = sec % 60
        let finalMin = min % 60
        let finalHrs = hrs % 24
        
        DispatchQueue.main.async {
            //            self.remainingHrsLabel.text = "\(days)" + " days " + "\(finalHrs):" + "\(finalMin): + "\(finalSec)" + "hrs"
            
            if (time > 0) {
                if (days >= 1) {
                    self.remainingHrsLabel.text = "\(days)" + " " + "days"
                        + " " + "\(finalHrs)" + ":" + "\(finalMin)"
                        + ":" + "\(finalSec)" + " " + "hrs"
                    
                }else if(days == 0 && finalHrs != 0) {
                    self.remainingHrsLabel.text = "\(finalHrs)" + ":" + "\(finalMin)"
                        + ":" + "\(finalSec)" + " " + "hrs"
                    self.remainingHrsLabel.textColor = UIColor(hexString: ColorConstants.RED)
                }else if(finalHrs == 0 && finalMin <= 59) {
                    self.remainingHrsLabel.text = "\(finalHrs)" + ":" + "\(finalMin)"
                        + ":" + "\(finalSec)" + " " + "min"
                    self.remainingHrsLabel.textColor = UIColor(hexString: ColorConstants.RED)
                }else if(finalMin == 0 && finalSec <= 59) {
                    self.remainingHrsLabel.text = "\(finalHrs)" + ":" + "\(finalMin)"
                        + ":" + "\(finalSec)" + " " + "sec"
                    self.remainingHrsLabel.textColor = UIColor(hexString: ColorConstants.RED)
                }
            } else {
                self.remainingHrsLabel.text = "Closed"
            }
            
        }
    }
    
    func epochTime1(epochTimee:Double)->String{
        
        let timeResult:Double = epochTimee
        let date = NSDate(timeIntervalSince1970: timeResult)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm:s" //Set time style
        
        let timeZone = TimeZone.current.identifier as String
        dateFormatter.timeZone = TimeZone(identifier: timeZone)
        let localDate = dateFormatter.string(from: date as Date)
        return "\(localDate)"
        
    }
    func epochHrs(epochhr:Double)->String{
        
        let timeResult:Double = epochhr
        let date = NSDate(timeIntervalSince1970: timeResult)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h" //Set time style
        
        let timeZone = TimeZone.current.identifier as String
        dateFormatter.timeZone = TimeZone(identifier: timeZone)
        let localDate = dateFormatter.string(from: date as Date)
        return "\(localDate)"
        
    }
    
    
    @IBAction func backClicked(_ sender: Any) {
         SideMenuController.preferences.basic.defaultCacheKey = "default"
        sideMenuController?.setContentViewController(with: "default")
    }
    
    @IBAction func confirmAndSubmit(_ sender: Any) {
        
         self.postLoadQuotation(isAvailableLoadBidQuotation : isAvailableLoadBidQuotation)
        
       
    }
    
 
    func postLoadQuotation(isAvailableLoadBidQuotation:Bool) {
        self.activityIndicatorBegin()
        print("isAvailableLoadBidQuotation ######### \(isAvailableLoadBidQuotation)")
//        print("self.fleetBidId ######### \(self.fleetBidId)")
//        print("self.loadBidId ######### \(self.loadBidId)")
        let method : HTTPMethod?
       
       
//        print("validDateTime ######### \(self.validDateTime)")
         let parameters : [String:Any]
        if isAvailableLoadBidQuotation {
          
            parameters = [
                "id" : self.id,
                "quotationAmount" : self.quotationAmount,
                "currencyMaster" : [
                    "id" : 2
                ],
                "validDateTime" : self.validDateTime
                
            ]
            method = .put
        }else{
            parameters = [
                "quotationAmount" : self.quotationAmount,
                "currencyMaster" : [
                    "id" : 2
                ],
                "validDateTime" : self.validDateTime
            ]
           method = .post
        }
        
       
//            let jsonDataa = try! JSONEncoder().encode(postLoadQuoation)
//            let type = String(data: jsonDataa, encoding: .utf8)!
//            do {
//                self.postLoadQuotationPara = try self.convertToDictionary(from: type)
//                print("listt = \(self.postLoadQuotationPara)")
//            } catch {
//                print(error)
//            }
        let headers = header()
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 35
        

        Alamofire.request(URLStatics.postloadquotation + "\(self.loadBidId)" + "/" + "\(self.fleetBidId)", method: method!, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
                .responseJSON { response in
                    print("response.request=\(String(describing: response.request))")  // original URL request
                    print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                    print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
            
                    switch (response.result) {
                    case .success:
                        self.activityIndicatorEnd()
                        if let reply = response.result.value {
                           
                            let mainResponse = JSON(reply)
                             print("======= JSON: postloadquotation ========  \(mainResponse)")
                            if mainResponse["warning"].stringValue != "" {
                                let alert = UIAlertController(title: "", message: "\(mainResponse["warning"])", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                               
                            }
                            if mainResponse["success"].stringValue != "" {
                                let alert = UIAlertController(title: "", message: "\(mainResponse["success"])", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (alert) in
                                    SideMenuController.preferences.basic.defaultCacheKey = "default"
                                    self.sideMenuController?.setContentViewController(with: "default")
                                    NotificationCenter.default.post(name: .confirmSubmitQuote, object: nil)
                                }))
                             
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                                
                            }
                            
                        }
                        break
                    case .failure(let error):
                        
                        print("== error == \(error.localizedDescription)")
                        break
                    }
            }
            
     
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
