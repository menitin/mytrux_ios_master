//
//  FleetMapViewController.swift
//  Mytrux
//
//  Created by Mytrux on 26/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import GoogleMaps
import CTSlidingUpPanel
import SideMenuSwift
import Alamofire
import SwiftyJSON





class FleetMapViewController: BaseViewController,CLLocationManagerDelegate,GMSMapViewDelegate,GMUClusterManagerDelegate,GMUClusterRendererDelegate{
let locationManager = CLLocationManager()
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var currentLocationView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var searchList: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var heightt: NSLayoutConstraint!
    @IBOutlet weak var topConst: NSLayoutConstraint!
    private var clusterManager: GMUClusterManager!
     var fleetDetails = [FleetDatum]()
    var bottomController:CTBottomSlideController?;
    var tappedMarker : GMSMarker?
    var item : POIItem?
    var customInfoWindow : CustomInfoWindow?
    var activePoint : POIItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.fleetDetailsMapView(_:)), name: .fleetDetailsMapView, object: nil)
        
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView,
                                                 clusterIconGenerator: iconGenerator)
        renderer.delegate = self
        
        clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm,renderer: renderer)
        clusterManager.setDelegate(self, mapDelegate: self)
        
           NotificationCenter.default.addObserver(self, selector: #selector(self.refreshViewFleetMap), name: .refreshViewFleetMap, object: nil)
        // Do any additional setup after loading the view.
    }
    
    @objc func refreshViewFleetMap() {
      
        viewWillAppear(true)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = true
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
        topBarCard(topBar: topBar)
        bottomController = CTBottomSlideController(topConstraint: topConst,
                                                   heightConstraint: heightt,
                                                   parent: view,
                                                   bottomView: bottomView,
                                                   tabController: nil,
                                                   navController: nil,
                                                   visibleHeight: 120)
        bottomController?.setAnchorPoint(anchor: 0.6)
        bottomController?.setExpandedTopMargin(pixels: 230)
        self.bottomView.layer.borderWidth = 0.5
        self.bottomView.layer.cornerRadius = 20
        bottomView.layer.borderColor = UIColor(hexString: "#C0C6CC").cgColor
        
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "map_style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                print("Unable to find map_style.json")
            }
        } catch {
            print("One or more of the map styles failed to load. \(error)")
        }
        
        
        bottomView.isHidden = true
    }
     @objc func fleetDetailsMapView(_ notification: NSNotification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        
     
        
        let fleet = userInfo!["fleetDetails"] as! [FleetDatum]
        self.fleetDetails = fleet
        self.clusterManager.clearItems()

        
              for state in self.fleetDetails {
                print("state.loadType.loadType ==  ====== \(state.loadType.loadType)")
                let position = CLLocationCoordinate2DMake(Double(state.availableLatitude!), Double(state.availableLongitude!))
                if self.loginUserType == UserStates.SERVICE_USER{
                    self.item = POIItem(position: position, type:MarkerType.CONTAINER, availableLocation: state.availableLocation, icon:UIImage(named:"Container-truck-big02")!, bitcode: state.bidCode, destinationLocation: state.destinationLocation!, vehicleTypeName: state.vehicleType.name, loadType: state.loadType.loadType, noOfVehicle: "", cargoType: "", cargoWt: "", ownThreeBids: nil, isQuoted: false)
                }else if state.loadType.loadType == "FTL" {
                    self.item = POIItem(position: position, type:MarkerType.FTL, availableLocation: state.availableLocation, icon: UIImage(named: "Container-truck-small03")!, bitcode: state.bidCode, destinationLocation: state.destinationLocation!, vehicleTypeName: state.vehicleType.name, loadType: state.loadType.loadType, noOfVehicle: String(state.noOfVehicles), cargoType: "", cargoWt: "", ownThreeBids: nil, isQuoted: false)
                }else if state.loadType.loadType == "CONTAINER"{
                    self.item = POIItem(position: position, type:MarkerType.CONTAINER, availableLocation: state.availableLocation, icon: UIImage(named: "Container-truck-big02")!, bitcode: state.bidCode, destinationLocation: state.destinationLocation!, vehicleTypeName: state.vehicleType.name, loadType: "", noOfVehicle: "", cargoType: "", cargoWt: "", ownThreeBids: nil, isQuoted: false)
                }
                
              
                self.clusterManager.add(self.item!)
                 self.clusterManager.cluster()
        }
        
       
        
    }
    
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        print("========= Cluster Clicked ======")
        
        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position,                                      zoom: self.mapView.camera.zoom + 1)
        let update = GMSCameraUpdate.setCamera(newCamera)
        self.mapView.animate(with: update)
        
//        var data = [Datum]()
//        for dataa in loadDetailsArray{
//            data = dataa.data
//        }
//        print("data.count === \(data.count)")
        
        
        return false
    }
    
    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
        if marker.userData is POIItem{
            let customClusterItem = (marker.userData! as! POIItem)
            let count = self.fleetDetails.count
            
            for i in 0...count {
                if self.loginUserType == UserStates.SERVICE_USER{
                    marker.icon = customClusterItem.icon.scaleToSize(aSize: CGSize(width: self.mapView.frame.width/9, height: self.mapView.frame.width/11 ))
                    break
                }else{
                    switch customClusterItem.type {
                    case MarkerType.FTL:
                        marker.icon = customClusterItem.icon.scaleToSize(aSize: CGSize(width: self.mapView.frame.width/10, height: self.mapView.frame.width/12))
                        break
                    case MarkerType.CONTAINER:
                        marker.icon = customClusterItem.icon.scaleToSize(aSize: CGSize(width: self.mapView.frame.width/8, height: self.mapView.frame.width/11 ))
                        break
                    }
                }
                
                
                marker.accessibilityLabel = "\(i)"
                
                //                customInfoWindow!.titleLabel.text = availableLatLong[index].availableLocation
            }
        }
    }
    
    @IBAction func searchListClicked(_ sender: Any) {
          sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "fleetsPostedVC") }, with: "third")
        sideMenuController?.setContentViewController(with: "third")
    }
    
    @IBAction func currentLocationViewClicked(_ sender: Any) {
        
    }
    
    @IBAction func backClicked(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    override func viewDidLayoutSubviews() {
        circularView(viewArray: [searchList,currentLocationView])
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 10)
        let markerImage = UIImage()
//        if loginUserType == UserStates.SERVICE_USER{
//            markerImage = UIImage(named: "Load-big03")!
//        }else{
//            markerImage = UIImage(named: "Container-truck-big02")!
//        }
        
        
   let marker = GMSMarker()
           let markerView = UIImageView(image: markerImage)
               // marker.position = CLLocationCoordinate2D(latitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!)
         //       marker.icon = markerImage.scaleToSize(aSize: CGSize(width: self.mapView.frame.width/8, height: self.mapView.frame.width/10
              // ))
        self.tappedMarker = GMSMarker()
        mapView.selectedMarker = marker
                marker.iconView = markerView
               marker.map = mapView
       
   mapView.selectedMarker = marker
       self.mapView.animate(to: camera)
        self.locationManager.stopUpdatingLocation()
        
        
    }
    
    var id = String()
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        self.bottomView.isHidden = false
     
        if let poiItem = marker.userData as? POIItem {
            self.id = poiItem.bidCode

            let fleetDetails  = ["fleetDetails":self.fleetDetails,"id":self.id] as [String : Any]
            
            NotificationCenter.default.post(name: .fleetDetailsBottom,object: nil,userInfo: fleetDetails)
        }
        
      
    }
    
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        customInfoWindow?.removeFromSuperview()
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        return customInfoWindow
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if let tempPoint = activePoint {
            customInfoWindow?.center = mapView.projection.point(for: tempPoint.position)
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let poiItem = marker.userData as? POIItem {
            // Remove previously opened window if any
            if activePoint != nil {
                customInfoWindow!.removeFromSuperview()
                activePoint = nil
            }
            // Load custom view from nib or create it manually
            // loadFromNib here is a custom extension of CustomInfoView
            customInfoWindow = CustomInfoWindow().loadView()
            customInfoWindow?.titleLabel.text = poiItem.availableLocation
            customInfoWindow?.layer.cornerRadius = 8
            customInfoWindow?.layer.shadowColor = UIColor.gray.cgColor
            customInfoWindow?.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            customInfoWindow?.layer.shadowRadius = 5
            customInfoWindow?.layer.shadowOpacity = 0.4
            customInfoWindow?.circularView.layoutIfNeeded()
            customInfoWindow?.circularView.layer.cornerRadius =  (customInfoWindow?.circularView.frame.size.width)!/2
            customInfoWindow?.circularView.layer.masksToBounds = true
            customInfoWindow?.circularView.clipsToBounds = true
            // Button is here
            customInfoWindow!.center = mapView.projection.point(for: poiItem.position)
            activePoint = poiItem
            self.view.addSubview(customInfoWindow!)
        }
        return false
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
