//
//  MatchingLoadCell.swift
//  Mytrux
//
//  Created by Aboli on 10/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class MatchingLoadCell: UITableViewCell {

    @IBOutlet weak var fleetView: UIView!
    @IBOutlet weak var selectFleetView: UIView!
    @IBOutlet weak var fleetDetailView: UIView!
    @IBOutlet weak var fleetArrowView: UIView!
    @IBOutlet weak var fleetId: UILabel!
    @IBOutlet weak var fleetVehicleType: UILabel!
    @IBOutlet weak var selectedFleetImage: UIImageView!
    @IBOutlet weak var fleetFrom: UILabel!
    @IBOutlet weak var fleetTo: UILabel!
    @IBOutlet weak var mainView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.layer.cornerRadius = 5
        mainView.layer.shadowColor = UIColor.gray.cgColor
        mainView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        mainView.layer.shadowRadius = 4
        mainView.layer.shadowOpacity = 0.4
        selectedFleetImage.image = UIImage(named: "unselectedCircleRed")
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
