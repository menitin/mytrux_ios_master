//
//  BookingCollectionViewCell.swift
//  Mytrux
//
//  Created by Aboli on 09/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class BookingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var mainView: UIView!
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainView.layer.cornerRadius = 5
        mainView.layer.shadowColor = UIColor.gray.cgColor
        mainView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        mainView.layer.shadowRadius = 5
        mainView.layer.shadowOpacity = 0.4
        imageView.layer.cornerRadius = 5
        imageView.layer.shadowColor = UIColor.gray.cgColor
        imageView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        imageView.layer.shadowRadius = 5
        imageView.layer.shadowOpacity = 0.4
       
        
        
        
    }

}

