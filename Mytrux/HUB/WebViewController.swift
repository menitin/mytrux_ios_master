//
//  WebViewController.swift
//  Mytrux
//
//  Created by Mytrux on 09/11/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import WebKit
import SideMenuSwift

class WebViewController:BaseViewController,UIWebViewDelegate {
    @IBOutlet weak var webviewWrraper: UIView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var topBar: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.legal(_:)), name: .legal, object: nil)
        topBarCard(topBar: topBar)
        // Do any additional setup after loading the view.

    }
    
    @objc func legal(_ notification: NSNotification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        self.activityIndicatorBegin()
        let urlStr = userInfo!["url"] as! String
        let url = URL(string: urlStr)
        let requestObj = URLRequest(url: url!)
            webView.loadRequest(requestObj)
        self.activityIndicatorEnd()
    }
    @IBAction func backClicked(_ sender: Any) {
        sideMenuController?.setContentViewController(with: "eighth")
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
