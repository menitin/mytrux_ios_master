//
//  ListModelViewController.swift
//  Mytrux
//
//  Created by Mukta Bhuyar Punjabi on 25/05/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol passSelectedItem {
    func passState(state : String)
    func passCity(city : String)
    func passCompanyType(companyType : String, id:Int, companyCategories:String)
}


class ListModelViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,selectedMode {

let appDelegate = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet weak var tableView: UITableView!
    var stateArray = [String]()
    var tableArray = [String]()
    var companyType = [String]()
    var selectedState = String()
    var selectedMode = 0
    var city = [String]()
    var delegate : passSelectedItem?
    @IBOutlet weak var mainView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        let step2 = Step2RegisterVC()
        step2.delegateMode = self
        companyTypee = []
        companyCategory=[]
        DispatchQueue.background(delay: 0.0, background: {
            print("This is run on the background queue")
             self.getTypeOfCompany()
        }) {
            print("completed")
           self.tableView.reloadData()
        }
       // self.selectedMode = UserDefaults.standard.value(forKey: UserDefaultsContants.SELECTED_MODE) as! Int
 
        self.tableView.contentInset = UIEdgeInsets(top: -10, left: 0, bottom: 0, right: 0)
        
//        let dismiss = UITapGestureRecognizer(target: self, action: #selector(self.handleDissmiss(_:)))
//        self.view.addGestureRecognizer(dismiss)
//        self.view.isUserInteractionEnabled = true
       
        // Do any additional setup after loading the view.
    }
    func getTypeOfCompany() -> [TypeOfCompany]?{
        print("user state = \(UserDefaults.standard.value(forKey: UserDefaultsContants.MTX_Load_STATE)! as! String)")
        Alamofire.request(URLStatics.typeOfCompany, method: .get).responseJSON { response in
            print("response.request=\(String(describing: response.request))")  // original URL request
            print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
            print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
            
            if let reply = response.result.value {
                
                let json = JSON(reply)
                
                print("JSON: \(json)")
                 self.tableView.reloadData()
                for i in 0 ..< json.count{
                    let type = json[i]
                    
                    let typeOfComp = TypeOfCompany(id: type["id"].int! , companyType: type["companyType"].string!, companyCategories: type["companyCategories"].string!)
                    self.typeOfCompany.append(typeOfComp)
                    
                    print("type = \(json[i])")
                }
                
                self.companyCategory.removeAll()
                self.companyTypee.removeAll()
                self.id.removeAll()
                for type in self.typeOfCompany{
                    if  UserDefaults.standard.value(forKey: UserDefaultsContants.MTX_Load_STATE)! as! String == UserStates.HUB_TRUCK {
                        if type.companyCategories == "FLEET"{
                            print("Fleet = \(type.companyCategories)")
                            DispatchQueue.main.async {
                          self.companyCategory.append(type.companyCategories)
                                self.companyTypee.append(type.companyType)
                                self.id.append(type.id)
                                self.tableView.reloadData()
                            }
                        }
                    }else if UserDefaults.standard.value(forKey: UserDefaultsContants.MTX_Load_STATE)! as! String == UserStates.HUB_LOAD{
                        if type.companyCategories == "LOAD"{
                            
                            print("load = \(type.companyCategories)")
                            DispatchQueue.main.async {
                            self.companyCategory.append(type.companyCategories)
                                self.companyTypee.append(type.companyType)
                                self.id.append(type.id)
                                self.tableView.reloadData()
                            }
                            
                        }
                    }
                    
                }
            }
        }
        return self.typeOfCompany
    }
    var typeOfCompany = [TypeOfCompany]()
    var companyCategory = [String]()
    var companyTypee = [String]()
    var id = [Int]()
    
    override func viewDidAppear(_ animated: Bool) {
        
       if UserDefaults.standard.value(forKey: UserDefaultsContants.SELECTED_MODE) as! Int == 1 {
        let states = appDelegate.getAllStateList()
        print("mode = \(UserDefaults.standard.value(forKey: UserDefaultsContants.SELECTED_MODE) as! Int)")
        print("city.count  \(self.city.count)")
        for state in states{
            self.stateArray.append(state)
        }
        self.tableArray = self.stateArray
        self.tableView.reloadData()
        }
        if UserDefaults.standard.value(forKey: UserDefaultsContants.SELECTED_MODE) as! Int == 2 {
            let cities = appDelegate.getCity()
            let selectState = String(describing: UserDefaults.standard.value(forKey: UserDefaultsContants.SELECTED_STATE)!)
            for city in cities!{
                if selectState == city.state {
                    print("\(city.state)")
                    print("\(city.city)")
                    self.city = city.city
                }
            }
            self.tableArray = self.city
            self.tableView.reloadData()
            print("city.count  \(self.city.count)")
        }
        if UserDefaults.standard.value(forKey: UserDefaultsContants.SELECTED_MODE) as! Int == 3 {
          
          //  let selectState = String(describing: UserDefaults.standard.value(forKey: UserDefaultsContants.SELECTED_STATE)!)
            self.tableArray = self.companyTypee
            self.tableView.reloadData()
            print("city.count  \(self.city.count)")
        }
    }
    func mode(mode: Int) {
        selectedMode = mode
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return tableArray.count
    }
    
//    @objc func handleDissmiss(_ sender: UITapGestureRecognizer){
//        self.dismiss(animated: true, completion: nil)
//    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ListTableViewCell
        let state = tableArray[indexPath.row]
        cell.label.text = state
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("\(indexPath.row)")
        let cell = tableView.cellForRow(at: indexPath) as! ListTableViewCell
        print(cell.label.text!)
        
 
        if UserDefaults.standard.value(forKey: UserDefaultsContants.SELECTED_MODE) as! Int == 1{
            if delegate != nil {
                delegate?.passState(state: String(cell.label.text!))
                 UserDefaults.standard.set(cell.label.text!, forKey: UserDefaultsContants.SELECTED_STATE)
                UserDefaults.standard.synchronize()
            }
        }
        else if UserDefaults.standard.value(forKey: UserDefaultsContants.SELECTED_MODE) as! Int == 2{
            if delegate != nil {
                delegate?.passCity(city: String(cell.label.text!))
            }
        }
       else if UserDefaults.standard.value(forKey: UserDefaultsContants.SELECTED_MODE) as! Int == 3{
                _ = [TypeOfCompany]()
                    var id = 0
                    var companyCategories = ""
                let type = getTypeOfCompany()
                for t in type!{
                    if UserDefaults.standard.value(forKey: UserDefaultsContants.MTX_Load_STATE)! as! String == UserStates.HUB_TRUCK{
                        if t.companyCategories == "FLEET"{
                            if t.companyType == cell.label.text!{
                                id = t.id
                                companyCategories = t.companyCategories
                            }
                        }
                       
                    }else if UserDefaults.standard.value(forKey: UserDefaultsContants.MTX_Load_STATE)! as! String == UserStates.HUB_LOAD{
                        if t.companyCategories == "LOAD"{
                            if t.companyType == cell.label.text!{
                                id = t.id
                                companyCategories = t.companyCategories
                            }
                        }
                    }
                   
                }
            print("id = \(id)")
            print("companyCategories = \(companyCategories)")
                    if delegate != nil {
                        delegate?.passCompanyType(companyType: cell.label.text!, id: id, companyCategories: companyCategories)
                    }
            
                UserDefaults.standard.set(cell.label.text!, forKey: UserDefaultsContants.CompanyType)
                }
      //  print("\(String(describing: UserDefaults.standard.value(forKey: UserDefaultsContants.SELECTED_STATE)!))")
        
        self.dismiss(animated: true, completion: nil)
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
}
