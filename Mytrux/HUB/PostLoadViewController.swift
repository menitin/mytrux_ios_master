//
//  PostLoadViewController.swift
//  Mytrux
//
//  Created by Aboli on 12/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class PostLoadViewController: BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var scrollVieww: UIScrollView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var seperatorLine: UIView!
    @IBOutlet weak var cancelBtn: UIView!
    @IBOutlet weak var cameraView: UIView!
    var selectedIndex = IndexPath()
    @IBOutlet weak var postLoadBtn: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        cardViewArray(arrView: [view1,view2])
        // topBarCard(topBar: topBar)
        scrollVieww.delegate = self
        buttonCardArray(arrView: [cancelBtn,postLoadBtn])
        circularView(viewArray: [cameraView])
        collectionView.delegate = self
        collectionView.dataSource = self
        addDashedBorder(view: seperatorLine)
        // Do any additional setup after loading the view.
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        scrollVieww.contentOffset.x = 0
    }
    @IBAction func menuBtn(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "postLoadCV", for: indexPath) as! PostLoadCollectionViewCell
        if selectedIndex == indexPath
        {
            cell.innerView.backgroundColor = UIColor.init(hexString: "#DD0B51")
        }
        else
        {
            cell.innerView.backgroundColor = UIColor.clear
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth: CGFloat = ((collectionView.bounds.size.width)  / CGFloat(4.0))
        return CGSize(width: itemWidth, height: collectionView.bounds.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath
        self.collectionView.reloadData()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
