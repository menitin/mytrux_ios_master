//
//  OTPScreenVC.swift
//  Mytrux
//
//  Created by Mukta Bhuyar Punjabi on 23/05/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class OTPScreenVC: BaseViewController,UITextFieldDelegate {
    @IBOutlet weak var otpTFWrapperView: UIView!
    
    @IBOutlet weak var otp6: UITextField!
    @IBOutlet weak var otp5: UITextField!
    @IBOutlet weak var otp4: UITextField!
    @IBOutlet weak var otp3: UITextField!
    @IBOutlet weak var otp2: UITextField!
    @IBOutlet weak var otp1: UITextField!
    @IBOutlet weak var securedLoginBtn: UIView!
    @IBOutlet weak var topBar: UIView!
    var tFArray = [UITextField]()
    @IBOutlet weak var instructionLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        topBarCard(topBar: topBar)
      tFArray = [otp1,otp2,otp3,otp4,otp5,otp6]
        for tf in tFArray{
            tf.delegate = self
        }
        instructionLabel.text = "Please type the verification code sent on your mobile number"
       instructionLabel.adjustsFontSizeToFitWidth = true
        instructionLabel.minimumScaleFactor = 0.5
        cardViewArray(arrView: [otpTFWrapperView] as! [UIView])
        // Do any additional setup after loading the view.
    }
    
    @IBAction func securedLoginClicked(_ sender: UITapGestureRecognizer) {
        
        self.verifyOTP()
    }
   
    func resendOTP(){
         let userID = UserDefaults.standard.value(forKey: UserDefaultsContants.USER_ID)!
        let parameters : [String:Any] = [
            "userId": userID
        ]
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
        print("url = \(URLStatics.hubRegister)")
        
        Alamofire.request(URLStatics.resendOTP, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: [:])
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                let resultNSString = NSString(data: response.request!.httpBody!, encoding: String.Encoding.utf8.rawValue)!
                print("response.request?.httpBody = \(resultNSString)")
                switch (response.result) {
                case .success:
                    
                    if let reply = response.result.value {
                        print("JSON: \(reply)")
                        let mainResponse = JSON(reply)
                        print("\(mainResponse)")
                        let alert = UIAlertController(title: "", message: mainResponse["success"].stringValue, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    break
                case .failure(let error):
                    
                    print("error \(error.localizedDescription)")
                    break
                }
        }
    }
    
    func verifyOTP(){
        let userID = UserDefaults.standard.value(forKey: UserDefaultsContants.USER_ID)!
        let otp11 = otp1.text! + otp2.text! + otp3.text!
        let otp22 = otp4.text! + otp5.text! + otp6.text!
        let otp = otp11 + otp22
        let parameters : [String:Any] = [
            "otp":otp,
            "userId":userID
        ]
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
        print("url = \(URLStatics.hubRegister)")

        Alamofire.request(URLStatics.verifyOTP, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: [:])
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                let resultNSString = NSString(data: response.request!.httpBody!, encoding: String.Encoding.utf8.rawValue)!
                print("response.request?.httpBody = \(resultNSString)")
                switch (response.result) {
                case .success:
                   
                    if let reply = response.result.value {
                        print("JSON: \(reply)")
                        let mainResponse = JSON(reply)
                        print("\(mainResponse)")
                        if mainResponse["status"].intValue == 1 {
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let viewController = storyboard.instantiateViewController(withIdentifier :"LoginVC")
                            self.transitionVc(vc: viewController, duration: 0.5, type: .fromRight)
                       }else{
                            let alert = UIAlertController(title: "", message: mainResponse["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
            }

                    }

                    break
                case .failure(let error):
                   
                    print("error \(error.localizedDescription)")
                    break
                }
        }
    }
    @IBAction func ResentOTP(_ sender: UITapGestureRecognizer) {
        resendOTP()
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        for tf in tFArray{
            if(textField == tf) {
                let maxLength = 1
                let currentString: NSString = tf.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }
        }
        return true
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
