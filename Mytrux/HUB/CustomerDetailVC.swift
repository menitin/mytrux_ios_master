//
//  CustomerDetailVC.swift
//  Mytrux
//
//  Created by Mytrux on 18/11/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class CustomerDetailVC: BaseViewController {

    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var emailId: UILabel!
    @IBOutlet weak var mobileNo: UILabel!
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var cinNo: UILabel!
    @IBOutlet weak var panNo: UILabel!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var companyType: UILabel!
    @IBOutlet weak var bookingsLabel: UILabel!
    @IBOutlet weak var gstNo: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        circularView(viewArray: [profileImageView,profileView])
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.customer(notification:)), name: .customerDetail, object: nil)
        // Do any additional setup after loading the view.
    }
    
    @objc func customer(notification: Notification) {
        
    let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        let cust = userInfo!["customer"] as? FavouriteCompany
        print(cust?.name! as Any)
        let contactPerson = cust!.contactPersons![0]
        
        self.customerName.text = "\(String(describing: contactPerson.fullName))"
        self.mobileNo.text = "\(String(describing: contactPerson.contactNo))"
        self.emailId.text = "\(String(describing: contactPerson.email))"
        self.companyName.text = "\(String(describing: cust!.name!))"
        self.address.text = "\(String(describing: cust!.address!.address!))"
        if cust!.cinNo != nil{
             self.cinNo.text = "\(String(describing: cust!.cinNo!))"
        }else{
            self.cinNo.text = "NA"
        }
        if cust!.gstTaxRegNo != nil{
            self.gstNo.text = "\(String(describing: cust!.gstTaxRegNo!))"
        }else{
             self.gstNo.text = "NA"
        }
        if cust!.panNo != nil{
           self.panNo.text = "\(String(describing: cust!.panNo!))"
        }else{
            self.panNo.text = "NA"
        }
        if cust!.typeOfCompany!.companyType != nil{
              self.companyType.text = "\(String(describing: cust!.typeOfCompany!.companyType!))"
        }else{
            self.companyType.text = "NA"
        }
  
    }
    
    
    @IBAction func backBtnClicked(_ sender: Any) {
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "CustomerViewController") }, with: "35")
        sideMenuController?.setContentViewController(with: "35")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
