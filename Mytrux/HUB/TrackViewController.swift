//
//  TrackViewController.swift
//  Mytrux
//
//  Created by Aboli on 15/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import GoogleMaps
import SideMenuSwift
import Alamofire
import SwiftyJSON
import AlamofireImage

class TrackViewController: BaseViewController,CLLocationManagerDelegate,GMSMapViewDelegate, GMUClusterManagerDelegate,GMUClusterRendererDelegate{
    
    @IBOutlet weak var tripDuration: UILabel!
    @IBOutlet weak var tripKm: UILabel!
    @IBOutlet weak var hoursLabelETA: UILabel!
    @IBOutlet weak var kmETALabel: UILabel!
    
    @IBOutlet weak var driverProfile: UIView!
    let locationManager = CLLocationManager()
    @IBOutlet weak var bookingNo: UILabel!
    @IBOutlet weak var manualLRNo: UILabel!
    @IBOutlet weak var manualLRDate: UILabel!
    @IBOutlet weak var pickupDate: UILabel!
    @IBOutlet weak var pickupTime: UILabel!
    @IBOutlet weak var eventView: UIView!
    
    @IBOutlet weak var driverDetailView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var topBarBookingLabel: UILabel!
    @IBOutlet weak var deliveryAddress: UILabel!
    @IBOutlet weak var shipperAddress: UILabel!
    
    @IBOutlet weak var jobStatusBackgroundView: UIView!
    @IBOutlet weak var jobStatus: UILabel!
    @IBOutlet weak var bookingDetailsView: UIView!
    @IBOutlet weak var viewPodView: UIView!
    @IBOutlet weak var bookingNoteView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tripView: UIView!
    @IBOutlet weak var flagImage: UIImageView!
    @IBOutlet weak var eventLabel: UILabel!
    @IBOutlet weak var eventDate: UILabel!
    @IBOutlet weak var driverNameLabel: UILabel!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var eventTime: UILabel!
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star5: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var driverLrNo: UILabel!
    @IBOutlet weak var LRNO: UILabel!
    @IBOutlet weak var driverBookingId: UILabel!
    @IBOutlet weak var driverVehicleNo: UILabel!
    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var callBtnView: UIView!
    @IBOutlet weak var driverDate: UILabel!
    @IBOutlet weak var driverTime: UILabel!
    
    var isTopViewOpen = false
    var customInfoWindow : CustomInfoWindow?
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    var jobId = String()
    var tripId = String()
    var bookingId = String()
    
    
    var animationPolyline = GMSPolyline()
    var path = GMSPath()
    var animationPath = GMSMutablePath()
    var i: UInt = 0
    var timer: Timer!
    var tripTimer: Timer!
    
    @IBOutlet weak var driverImage: UIImageView!
    @IBOutlet weak var callBtn: UIView!
    var tripArr = [JobVehiclesDriver]()
    override func viewDidLoad() {
        super.viewDidLoad()
        driverDetailView.isHidden = true
        eventView.isHidden = true
        sideMenuController?.clearCache(with: "twelve")
        circularView(viewArray: [bookingNoteView,bookingDetailsView,viewPodView,driverProfile])
        cardViewArray(arrView: [driverDetailView,eventView])
        buttonCardView(button: callBtn)
        NotificationCenter.default.addObserver(self, selector: #selector(self.track(notification:)), name: .track, object: nil)
        self.mapView.delegate = self
        self.locationManager.delegate = self
        
        self.locationManager.startUpdatingLocation()
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "map_style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                print("Unable to find map_style.json")
            }
        } catch {
            print("One or more of the map styles failed to load. \(error)")
        }
        // Do any additional setup after loading the view.
    }
    var currentLat : CLLocationDegrees?
    var currentLong : CLLocationDegrees?
    
    var lat : CLLocationDegrees?
    var lng : CLLocationDegrees?
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        //        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 10)
        //        self.mapView.animate(to: camera)
        self.lat = (location?.coordinate.latitude)!
        self.lng = (location?.coordinate.longitude)!
        if self.currentLat != nil && self.currentLong != nil {
            
            let position = CLLocationCoordinate2D(latitude: self.currentLat!,longitude: self.currentLong!)
            let marker = GMSMarker(position: position)
            marker.icon = UIImage(named: "Container-truck-small03")
            marker.setIconSize(scaledToSize: .init(width: self.mapView.frame.width/10, height: self.mapView.frame.width/12))
            marker.map = self.mapView
        }
        
        self.locationManager.stopUpdatingLocation()
    }
    var bookingDetail : BookingElement?
    
    @objc func track(notification: Notification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        let booking = userInfo!["booking"] as! BookingElement
        jobId = "\(booking.id!)"
        print("jobId ==== \(jobId)")
        triplist(jobId:jobId)
        bookingId = "\(booking.id!)"
        self.bookingNo.text = booking.bookingNo!
        self.bookingDetail = booking
        self.topBarBookingLabel.text = booking.bookingNo!
        let timestamp = Double(booking.pickupDateTime!/1000)
        let date = self.epochDate(epochDate: timestamp)
        let time = self.epochTime(epochTimee: timestamp)
        self.pickupDate.text = date
        self.pickupTime.text = time
        self.shipperAddress.text = booking.shipper!.name! + " " + booking.shipper!.address!
        self.deliveryAddress.text = booking.deliveryCustomer!.name! + " " + booking.deliveryCustomer!.address!
        
    }
    
    
    var driverPhotoId = Int()
    func triplist(jobId:String){
        
        self.activityIndicatorBegin()
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
        let header = self.header()
        var parameters = [String:Any]()
        parameters = [
            "financialYear" : self.currentFinancialYear!
        ]
        Alamofire.request(URLStatics.tripId + "\(jobId)", method: .get, encoding: JSONEncoding.default, headers: header)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                
                switch (response.result) {
                case .success:
                    var returnDict = [String:Any]()
                    if let reply = response.result.value {
                        print("JSON: ====== tripId ======== \(reply)")
                        let mainResponse = JSON(reply)
                        print("\(mainResponse)")
                        if mainResponse["warning"].stringValue != "" {
                            let alertController = UIAlertController(title: "\(mainResponse["warning"].stringValue)", message:"", preferredStyle: .alert)
                            
                            // Create the actions
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                                
                            }
                            alertController.addAction(okAction)
                            
                            // Present the controller
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                        let trip = mainResponse["trips"].arrayObject as! [[String:Any]]
                        let shipper = mainResponse["shipper"].dictionaryValue
                        let driverPhoto = mainResponse["driverphoto"].dictionaryValue
                        let jobStatus = mainResponse["jobStatus"].dictionaryValue
                        let jobStatusId = jobStatus["id"]!.intValue
                        if driverPhoto["id"]?.int != nil{
                            self.driverPhotoId = (driverPhoto["id"]?.int)!
                        }
                        

                        let shipperName = shipper["name"]?.stringValue
                        let shipperAddress = shipper["address"]?.stringValue
                        let shipperlocation = shipper["location"]?.stringValue
                        let shipperState = shipper["state"]?.stringValue
                        let origin = shipperName! + " " + shipperAddress! + " " + shipperlocation! + " " + shipperState!
                        
                        let deliveryCustomer = mainResponse["deliveryCustomer"].dictionaryValue
                        let deliveryCustomerName = deliveryCustomer["name"]?.stringValue
                        let deliveryCustomerAddress = deliveryCustomer["address"]?.stringValue
                        let deliveryCustomerlocation = deliveryCustomer["location"]?.stringValue
                        let deliveryCustomerState = deliveryCustomer["state"]?.stringValue
                        
                        let destination = deliveryCustomerName! + " " + deliveryCustomerAddress! + " " + deliveryCustomerlocation! + " " + deliveryCustomerState!
                        
                        
                        self.drawPath(origin: origin, destination: destination)
                        for i in 0 ..< trip.count{
                            let type = trip[i]
                            print("tripId  id ----------- \(type["id"]!)")
                            let trip = JobVehiclesDriver(id: type["id"] as? Int, availableFleetBid: nil, vehicleRegistrationNo: type["vehicleRegistrationNo"] as? String, driverFullName: type["driverFullName"] as? String, mobileNo:type["mobileNo"] as? String, startKM: type["startKM"] as? Int, endKM: type["endKM"] as? Int, user: nil, allocationType: type["allocationType"] as? String, createTime: type["createTime"] as? Int, vehicleType: nil, jobStatus: nil, lrNo: type["lrNo"] as? Int, uploadPodTime: type["uploadPodTime"] as? Int, podID: nil, exManualLrNo: type["exManualLrNo"] as? String, exManualLrDate: type["exManualLrDate"] as? Int)
                            self.tripArr.append(trip)
                            self.tripId = "\(type["id"]!)"
                        }
                        self.activityIndicatorEnd()
                        if jobStatusId != JobStatusId.ARRIVED_AT_DESTINATION && jobStatusId != JobStatusId.UPLOADED_POD{
                            self.trackingList()
                            self.tripTimer = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(self.trackingList), userInfo: nil, repeats: true)
                        }
                    }
                    
                    break
                case .failure(let error):
                    
                    print("error \(error.localizedDescription)")
                    break
                }
        }
    }
    var jobStatusArray = [MileStoneElement]()
    var isScreenVisibleFirstTime = true
    
    @objc func trackingList(){
        
        self.activityIndicatorBegin()
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
        let header = self.header()
        
        Alamofire.request(URLStatics.trackinglist + "\(tripId)" + "?bookingId=" + bookingId, method: .get,encoding: JSONEncoding.default, headers: header)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                
                switch (response.result) {
                case .success:
                    var returnDict = [String:Any]()
                    if let reply = response.result.value {
                        print("JSON: ====== trackingList ======== \(reply)")
                        let mainResponse = JSON(reply)
                        print("\(mainResponse["status"])")
                        if mainResponse["warning"].stringValue != "" {
                            let alertController = UIAlertController(title: "\(mainResponse["warning"].stringValue)", message:"", preferredStyle: .alert)
                            
                            // Create the actions
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                                
                            }
                            alertController.addAction(okAction)
                            
                            // Present the controller
                            self.present(alertController, animated: true, completion: nil)
                        }
                        
                        if mainResponse["status"] == true{
                            if let returnObj = mainResponse["data"].dictionaryObject as? [String:Any] {
                                returnDict = returnObj
                            }
                            let currentLocation = returnDict["currentLocation"] as! [String:Any]
                            let currentLat = currentLocation["latitude"]!
                            let currentLong = currentLocation["longitude"]!
                            self.currentLat = currentLat as? CLLocationDegrees
                            self.currentLong = currentLong as? CLLocationDegrees
                            print("latitude ----- \(currentLocation["latitude"]!)")
                            print("longitude ----- \(currentLocation["longitude"]!)")
                            let position = CLLocationCoordinate2D(latitude: self.currentLat!,longitude: self.currentLong!)
                            let marker = GMSMarker(position: position)
                            
                            marker.icon = UIImage(named: "Container-truck-small03")
                            marker.setIconSize(scaledToSize: .init(width: self.mapView.frame.width/10, height: self.mapView.frame.width/12))
                            marker.map = self.mapView
                            if self.isScreenVisibleFirstTime{
                                self.mapView.camera = GMSCameraPosition.camera(withLatitude: self.currentLat!, longitude: self.currentLong!, zoom: 1)
                                self.isScreenVisibleFirstTime = false
                            }else{
                                self.mapView.camera = GMSCameraPosition.camera(withLatitude: self.currentLat!, longitude: self.currentLong!, zoom: 13)
                            }
                            
                            CATransaction.begin()
                            CATransaction.setValue(2.0, forKey: kCATransactionAnimationDuration)
                            let city = GMSCameraPosition.camera(withLatitude: self.currentLat!,longitude: self.currentLong!, zoom: 14)
                            self.mapView.animate(to: city)
                            CATransaction.commit()
                        }
                        
                        let milestoneArr = returnDict["milestone"] as? [[String:Any]]
                        
                        
                        self.jobStatusArray.removeAll()
                        if milestoneArr != nil{
                            for milestone in milestoneArr!{
                                
                                print("jobStatusId  id ----------- \(milestone["jobStatusId"]!)")
                                let milestone = MileStoneElement(id: milestone["id"]! as? Int, createdDate:  milestone["createdDate"]! as? Int, latitude:  milestone["latitude"]! as? Double, longitude:  milestone["longitude"]! as? Double, speed:  milestone["speed"]! as? Double, distance:  milestone["distance"]! as? Int, jobStatusID: milestone["jobStatusId"]! as? Int, bearing: milestone["bearing"]! as? Double)
                                self.jobStatusArray.append(milestone)
                                
                            }
                            
                        }
                        
                        self.activityIndicatorEnd()
                        
                        for data in self.jobStatusArray{
                            let location = CLLocationCoordinate2D(latitude: data.latitude!, longitude: data.longitude!)
                            print("location: \(location)")
                            let markerr = GMSMarker()
                            
                            markerr.position = location
                            if data.jobStatusID == JobStatusId.LEFT_FOR_PICKUP{
                                markerr.icon = UIImage(named: "blueFlag")
                            }else if data.jobStatusID == JobStatusId.REACHED_PICKUP{
                                markerr.icon = UIImage(named: "greenFlag")
                            }else if data.jobStatusID == JobStatusId.LEFT_FOR_DESTINATION{
                                markerr.icon = UIImage(named: "orangeFlag")
                            }else if data.jobStatusID == JobStatusId.ARRIVED_AT_DESTINATION{
                                markerr.icon = UIImage(named: "redFlag")
                            }
                            
                            markerr.setIconSize(scaledToSize: .init(width: self.mapView.frame.width/15, height: self.mapView.frame.width/13))
                            markerr.map = self.mapView
                        }
                        
                        
                    }
                    
                    
                    break
                case .failure(let error):
                    
                    print("error \(error.localizedDescription)")
                    break
                }
        }
    }
    
    var startLatitude = Double()
    var startLongitude = Double()
    var endLatitude = Double()
    var endLongitude = Double()
    
    func drawPath(origin:String,destination:String)
    {
        let originn = origin.replacingOccurrences(of: " ", with: "")
        let destinationn = destination.replacingOccurrences(of: " ", with: "")
//                 let originn = "VimanNagar"
//                let destinationn = "LohagaonPune"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(originn)&destination=\(destinationn)&mode=driving&key=\(URLStatics.googleApiKey)"
        print("url == \(url)")
        Alamofire.request(url).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            print(response.data)
            print(" server data") // server data
            print(response.result.value)   // result of response serialization
            
            let json = JSON(response.result.value!)
            let routes = json["routes"].arrayValue
            if routes.count != 0{
                let legs = routes[0]["legs"].arrayValue
                for leg in legs{
                    print("location  === \(leg["end_location"].dictionaryValue)")
                    let endLocationDict = leg["end_location"].dictionaryValue
                    let distanceDict = leg["distance"].dictionaryValue
                    let durationDict = leg["duration"].dictionaryValue
                    self.tripDuration.text = durationDict["text"]!.stringValue.replacingOccurrences(of: "hours", with: "Hrs")
                    self.tripKm.text = distanceDict["text"]!.stringValue
                    self.hoursLabelETA.text = durationDict["text"]!.stringValue.replacingOccurrences(of: "hours", with: "Hrs")
                    self.kmETALabel.text = distanceDict["text"]!.stringValue
                    let endLat = endLocationDict["lat"]?.doubleValue
                    let endLng = endLocationDict["lng"]?.doubleValue
                    let startLocationDict = leg["start_location"].dictionaryValue
                    let startLat = startLocationDict["lat"]?.doubleValue
                    let startLng = startLocationDict["lng"]?.doubleValue
                    
                    
                    let startPosition = CLLocationCoordinate2D(latitude: startLat!,longitude: startLng!)
                    let startMarker = GMSMarker(position: startPosition)
                    
                    startMarker.icon = UIImage(named: "from")
                    startMarker.setIconSize(scaledToSize: .init(width: self.mapView.frame.width/18, height: self.mapView.frame.width/14))
                    startMarker.snippet = origin
                    self.startLatitude = startLat!
                    self.startLongitude = startLng!
                    startMarker.map = self.mapView
                    
                   
//                    if self.currentLat == nil && self.currentLong == nil{
                         self.mapView.camera = GMSCameraPosition.camera(withLatitude: startLat!, longitude: startLng!, zoom: 1)
                        CATransaction.begin()
                        CATransaction.setValue(2.0, forKey: kCATransactionAnimationDuration)
                        let city = GMSCameraPosition.camera(withLatitude: startLat!,longitude: startLng!, zoom: 14)
                        self.mapView.animate(to: city)
                        CATransaction.commit()
           //         }
                    
                    
                    let endPosition = CLLocationCoordinate2D(latitude: endLat!,longitude: endLng!)
                    let endMarker = GMSMarker(position: endPosition)
                    self.endLatitude = endLat!
                    self.endLongitude = endLng!
                    endMarker.icon = UIImage(named: "mytruxRedIcon")
                    endMarker.setIconSize(scaledToSize: .init(width: self.mapView.frame.width/18, height: self.mapView.frame.width/14))
                     endMarker.snippet = destination
                    endMarker.map = self.mapView
                    
                }
                for route in routes
                {
                    let routeOverviewPolyline = route["overview_polyline"].dictionary
                    
                    let points = routeOverviewPolyline?["points"]?.stringValue
                    
                    let path = GMSPath.init(fromEncodedPath: points!)
                    //            let polyline = GMSPolyline.init(path: path)
                    //            polyline.map = self.mapView
                    self.path = path!
                    self.mapView.addPath(path!, strokeColor: .black, strokeWidth: 2, geodesic: nil, spans: nil)
                    //      self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.animatePolylinePath), userInfo: nil, repeats: true)
                }
            }
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //   self.timer.invalidate()
        if tripTimer != nil{
            self.tripTimer.invalidate()
        }
        
    }
    
    func jobStatus(statusId:Int) -> String{
        switch (statusId) {
        case 1:
            return "New Booking"
        case 5:
            return "Assigned"
        case 6:
            return "Acknowledged"
        case 7:
            return "Left for Pickup"
        case 8:
            return "Reached Pickup"
        case 9:
            return "Left for Destination"
        case 10:
            return "Arrived at Destination"
        case 11:
            return "Uploaded POD"
        default:
            return ""
        }
    }
    
    @IBAction func backClicked(_ sender: Any) {
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookingController") }, with: "tenth")
        sideMenuController?.setContentViewController(with: "tenth")
    }
    
    @IBAction func bookingDetailsClicked(_ sender: Any) {
        
        sideMenuController?.setContentViewController(with: "thirteen")
        NotificationCenter.default.post(name: .bookingDetailController, object: nil,userInfo: ["bookinng" : self.bookingDetail!])
    }
    
    @IBAction func viewPodClicked(_ sender: Any) {
        let podIdd = bookingDetail?.jobVehiclesDrivers![0]
        
        let podId = podIdd!.podID!.id
        if podId != nil{
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let presentedViewController = storyBoard.instantiateViewController(withIdentifier: "ViewPODViewController") as! ViewPODViewController
            presentedViewController.providesPresentationContextTransitionStyle = true
               presentedViewController.photoId = "\(podId!)"
            presentedViewController.definesPresentationContext = true
            presentedViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
            presentedViewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
            
            self.present(presentedViewController, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func bookingNoteClicked(_ sender: Any) {
        let jobid = self.bookingDetail!.id!
        let fleetId = userDefaults.value(forKey: LoginDataConstants.fleetId)
        sideMenuController?.setContentViewController(with: "24")
        NotificationCenter.default.post(name:.eLR, object: nil,userInfo: ["url": URLStatics.report + "\(fleetId!)" + "/" + "\(jobid)"])
    }
    
    @IBAction func currentLocation(_ sender: Any) {
        if self.currentLat != nil && self.currentLong != nil{
            
            
            self.mapView.camera = GMSCameraPosition.camera(withLatitude: self.currentLat!, longitude: self.currentLong!, zoom: 12)
            
            CATransaction.begin()
            CATransaction.setValue(2.0, forKey: kCATransactionAnimationDuration)
            let city = GMSCameraPosition.camera(withLatitude: self.currentLat!,longitude: self.currentLong!, zoom: 14)
            self.mapView.animate(to: city)
            CATransaction.commit()
        }
    }
    
    @IBAction func navigateToBookingNote(_ sender: Any) {
        let jobid = self.bookingDetail!.id!
        let fleetId = userDefaults.value(forKey: LoginDataConstants.fleetId)
        sideMenuController?.setContentViewController(with: "24")
        NotificationCenter.default.post(name:.eLR, object: nil,userInfo: ["url": URLStatics.report + "\(fleetId!)" + "/" + "\(jobid)"])
    }
    
    
    
    @IBAction func dropDownClicked(_ sender: Any) {
        
        if isTopViewOpen == false{
            UIView.animate(withDuration: 0.5) {
                self.topConstraint.constant = self.topView.frame.height
            }
        }else{
            UIView.animate(withDuration: 0.5) {
                self.topConstraint.constant = 0
            }
        }
        isTopViewOpen = !isTopViewOpen
    }
    
    @objc func animatePolylinePath() {
        if (self.i < self.path.count()) {
            self.animationPath.add(self.path.coordinate(at: self.i))
            self.animationPolyline.path = self.animationPath
            self.animationPolyline.strokeColor = UIColor.black
            self.animationPolyline.strokeWidth = 2
            self.animationPolyline.map = self.mapView
            self.i += 1
        }
        else {
            self.i = 0
            self.animationPath = GMSMutablePath()
            self.animationPolyline.map = nil
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print("marker.marker.position ========= \(marker.position.latitude)")
        for mile in self.jobStatusArray{
            
            print("latitude \(String(describing: mile.latitude)) longitude ==== \(String(describing: mile.longitude))")
           
            if mile.latitude == marker.position.latitude && mile.longitude == marker.position.longitude{
                
                self.eventView.isHidden = false
                self.driverDetailView.isHidden = true
                self.eventLabel.text = jobStatus(statusId: mile.jobStatusID!)
                let timestamp = Double(mile.createdDate! / 1000)
                self.eventDate.text = self.epochDate(epochDate: timestamp)
                self.eventTime.text = self.epochTime(epochTimee: timestamp)
                if mile.jobStatusID == JobStatusId.LEFT_FOR_PICKUP{
                    self.flagImage.image = UIImage(named: "blueFlag")
                }else if mile.jobStatusID == JobStatusId.REACHED_PICKUP{
                    self.flagImage.image = UIImage(named: "greenFlag")
                }else if mile.jobStatusID == JobStatusId.LEFT_FOR_DESTINATION{
                    self.flagImage.image = UIImage(named: "orangeFlag")
                }else if mile.jobStatusID == JobStatusId.ARRIVED_AT_DESTINATION{
                    self.flagImage.image = UIImage(named: "redFlag")
                }
                
            }
            
            
            if self.currentLat == marker.position.latitude && self.currentLong == marker.position.longitude{
                self.eventView.isHidden = true
                self.driverDetailView.isHidden = false
                let trips = self.tripArr
                for trip in trips{
                    
                    
                    self.driverNameLabel.text = trip.driverFullName
                    self.driverLrNo.text = "\(trip.lrNo!)"
                    self.driverBookingId.text = self.bookingDetail?.bookingNo!
                    self.driverVehicleNo.text = trip.vehicleRegistrationNo!
                    self.speedLabel.text = "0.0"
                    let timeStamp = Double(trip.createTime! / 1000)
                    self.driverDate.text = self.epochDate(epochDate: timeStamp)
                    self.driverTime.text = self.epochTime(epochTimee: timeStamp)
                    Alamofire.request(URLStatics.vehicleImage+"\(driverPhotoId)").responseImage { response in
                        //  print("response === \(String(describing: response.data))")
                        if let imagee = response.result.value {
                            self.driverImage.image = imagee
                            //print("fleetImg.count ======== \(self.fleetImg.count)")
                        }
                    }
                }
                
            }
            
            
            
        }
        
        
        if self.startLatitude == marker.position.latitude && self.startLongitude == marker.position.longitude{
self.mapView.selectedMarker = marker
            print("marker.snippet! == \(marker.snippet!)")
        }else if self.endLatitude == marker.position.latitude && self.endLongitude == marker.position.longitude{
             print("marker.snippet! == \(marker.snippet!)")
self.mapView.selectedMarker = marker
        }
        return true
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension GMSMarker {
    func setIconSize(scaledToSize newSize: CGSize) {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        icon?.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        icon = newImage
    }
}

extension GMSMapView {
    func addPath(_ path: GMSPath, strokeColor: UIColor? = nil, strokeWidth: CGFloat? = nil, geodesic: Bool? = nil, spans: [GMSStyleSpan]? = nil) {
        let line = GMSPolyline(path: path)
        line.strokeColor = strokeColor ?? line.strokeColor
        line.strokeWidth = strokeWidth ?? line.strokeWidth
        line.geodesic = geodesic ?? line.geodesic
        line.spans = spans ?? line.spans
        line.map = self
    }
}
