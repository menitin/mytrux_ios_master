//
//  UploadPODViewController.swift
//  Mytrux
//
//  Created by Aboli on 18/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SideMenuSwift

class UploadPODViewController: BaseViewController {
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var enterOtpView: UIView!
    @IBOutlet weak var uploadPhotoView: UIView!
    @IBOutlet weak var uploadSignatureView: UIView!
    var jobId = String()
    
    var tripId = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    topBarCard(topBar: topBar)
        cardViewArray(arrView: [enterOtpView,uploadSignatureView,uploadPhotoView])
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.uploadPod(notification:)), name: .uploadPod, object: nil)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func uploadSignature(_ sender: Any) {

        sideMenuController?.setContentViewController(with: "21")
        NotificationCenter.default.post(name: .uploadSignature ,object: nil,userInfo: ["jobId":self.jobId,"tripId":tripId])
    }
    
    
    
    @objc func uploadPod(notification: Notification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        let assignDetails = userInfo!["assignDetail"] as! BookingElement
       
       jobId = "\(assignDetails.id!)"
        print("jobId ==== \(jobId)")
        triplist(jobId: "\(jobId)")
    }
    
    @IBAction func uploadPhoto(_ sender: Any) {
       sideMenuController?.setContentViewController(with: "20")
             NotificationCenter.default.post(name: .uploadSignature ,object: nil,userInfo: ["jobId":self.jobId,"tripId":tripId])
    }
    
    @IBAction func backbtn(_ sender: Any) {
           sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookingController") }, with: "tenth")
        sideMenuController?.setContentViewController(with: "tenth")
    }
    
    
    func triplist(jobId:String){
        
        self.activityIndicatorBegin()
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 35
        let header = self.header()
        
        Alamofire.request(URLStatics.triplist + "\(jobId)", method: .get,encoding: JSONEncoding.default, headers: header)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                
                switch (response.result) {
                case .success:
                    var returnDict = [String:Any]()
                    if let reply = response.result.value {
                        print("JSON: triplist ======== \(reply)")
                        let mainResponse = JSON(reply)
                        print("\(mainResponse)")
                        if mainResponse["warning"].stringValue != "" {
                            let alertController = UIAlertController(title: "\(mainResponse["warning"].stringValue)", message:"", preferredStyle: .alert)
                            
                            // Create the actions
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                                
                            }
                            alertController.addAction(okAction)
                            
                            // Present the controller
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                        for i in 0 ..< mainResponse.count{
                            let type = mainResponse[i]
                            print("id ----------- \(type["id"])")
                         self.tripId = "\(type["id"])"
                        }
                         self.activityIndicatorEnd()
                    }
                    
                    break
                case .failure(let error):
                    
                    print("error \(error.localizedDescription)")
                    break
                }
        }
    }
    
    
    
    @IBAction func enterOtp(_ sender: Any) {
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
