//
//  NotificationViewController.swift
//  Mytrux
//
//  Created by Mytrux on 06/08/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import SwipeCellKit
import SideMenuSwift

class NotificationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, SwipeTableViewCellDelegate{
var arr = [Int]()
    @IBOutlet weak var undoView: UIView!
    @IBOutlet weak var undoBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var notificationTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        notificationTableView.delegate = self
        notificationTableView.dataSource = self
        arr = [1,2,3,4,5,6,7,8,9,0]
        self.notificationTableView.estimatedRowHeight = 100;
        self.notificationTableView.rowHeight = UITableView.automaticDimension;
        self.notificationTableView.setNeedsLayout()
        self.notificationTableView.layoutIfNeeded()
        undoBottomConstraint.constant = -self.undoView.frame.size.height
       
        // Do any additional setup after loading the view.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    //    @objc func handleDissmiss(_ sender: UITapGestureRecognizer){
    //        self.dismiss(animated: true, completion: nil)
    //    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell") as! NotificationTableViewCell
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        let deleteAction = SwipeAction(style: .destructive, title: "Delete") { action, indexPath in
            self.removeRow(indexPath: indexPath)
            UIView.animate(withDuration:  0.8, animations: {
                self.undoBottomConstraint.constant = 0
            })
            // handle action by updating model with deletion
        }
        deleteAction.backgroundColor = UIColor(hexString: ColorConstants.DARK_BLUE)
        deleteAction.style = .destructive
    
        // customize the action appearance
        deleteAction.image = UIGraphicsImageRenderer(size: CGSize(width: 20, height: 20)).image { _ in
                        UIImage(named: "delete")?.draw(in: CGRect(x: 0, y: 0, width: 20, height: 20))
                    }
      
        return [deleteAction]
        
    }
//    let dataSource: [CustomObject] = [Object1, Object2, Object3]
  var undoSource = [Int]()
    
    @IBAction func undoChanges(_ sender: Any) {
    self.undo()
    }
    func removeRow(indexPath:IndexPath) {
        let object = arr[indexPath.row]
        undoSource.append(object)
        arr.remove(at: object)
    }
    
    func undo() {
        for object in undoSource {
            arr.append(object)
        }
        UIView.animate(withDuration:  0.8, animations: {
            self.undoBottomConstraint.constant = -self.undoView.frame.size.height
        })

        notificationTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        
        options.expansionStyle = .destructive
        options.buttonVerticalAlignment = .center
        options.transitionStyle = .drag
        return options
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    @IBAction func backBtn(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    


}
