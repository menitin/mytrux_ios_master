//
//  MapViewController.swift
//  Mytrux
//
//  Created by Aboli on 05/06/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import SideMenuSwift
import GoogleMaps
//import Bottomsheet
import CTSlidingUpPanel




class MapViewControllerTest: BaseViewController,CLLocationManagerDelegate,GMSMapViewDelegate, GMUClusterManagerDelegate {

    @IBOutlet weak var searchView: UIView!
    var bottomController:CTBottomSlideController?
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var bottomViewTop: NSLayoutConstraint!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var bottomViewHeight: NSLayoutConstraint!
    let locationManager = CLLocationManager()
    let didFindMyLocation = false
    var jsonObj = [String:Any]()
    private var clusterManager: GMUClusterManager!
   // let controller = Bottomsheet.Controller()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView,
                                                 clusterIconGenerator: iconGenerator)
        clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm,
                                           renderer: renderer)
        
        // Generate and add random items to the cluster manager.
      
        
        // Call cluster() after items have been added to perform the clustering
        // and rendering on map.
        clusterManager.cluster()
        // Adds View
       
//        controller.addContentsView(bottomView)
//        controller.initializeHeight = 200
//        controller.viewActionType = .swipe
        searchView.layer.cornerRadius = 8
        searchView.layer.shadowColor = UIColor.gray.cgColor
        searchView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        searchView.layer.shadowRadius = 5
        searchView.layer.shadowOpacity = 0.4
        topBarCard(topBar: topBar)
        //You can provide nil to tabController and navigationController
//        bottomController = CTBottomSlideController(topConstraint: bottomViewTop, heightConstraint: bottomViewHeight, parent: view, bottomView: bottomView, tabController: nil, navController: nil, visibleHeight: 50)
        //0 is bottom and 1 is top. 0.5 would be center
        bottomView.center.x = mapView.center.x
      bottomController = CTBottomSlideController(parent: view, bottomView: bottomView, tabController: nil, navController: nil, visibleHeight: 50)
        bottomController?.setAnchorPoint(anchor: 0.5)
        bottomController?.setExpandedTopMargin(pixels: 100)
      
        self.mapView.isMyLocationEnabled = true
        self.bottomView.layer.borderWidth = 0.5
        //Location Manager code to fetch current location
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "map_style", withExtension: ".json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
        // Do any additional setup after loading the view.
    }
//    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//        super.viewWillTransition(to: size, with: coordinator)
//        bottomController?.viewWillTransition(to: size, with: coordinator)
//    }
    
//    private func generateClusterItems() {
//        let extent = 0.2
//        for index in 1...30 {
//            let lat = self.lat + extent * randomScale()
//            let lng = self.lng + extent * randomScale()
//            let name = "Item \(index)"
//            let item =
//                POIItem(position: CLLocationCoordinate2DMake(lat, lng), type: MarkerType.FTL, availableLocation: name, icon: UIImage.init(named: "")!, bitcode: "", destinationLocation: "", vehicleTypeName: "")
//                clusterManager.add(item)
//
//        }
//    }
    
//    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
//        if marker.userData is POIItem {
//            marker.icon = UIImage(named: "Load-big03")
//        }
//    }

    
    /// Returns a random value between -1.0 and 1.0.
    private func randomScale() -> Double {
        return Double(arc4random()) / Double(UINT32_MAX) * 2.0 - 1.0
    }
   
    var lat = Double()
    var lng = Double()
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
        let marker = GMSMarker()
        
        // I have taken a pin image which is a custom image
        let markerImage = UIImage(named: "Load-big03")!
        
        //creating a marker view
     //   let markerView = UIImageView(image: markerImage)
       
        //changing the tint color of the image
       lat = (location?.coordinate.latitude)!
        lng = (location?.coordinate.longitude)!
      
        marker.position = CLLocationCoordinate2D(latitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!)
        marker.icon = markerImage.scaleToSize(aSize: CGSize(width: self.mapView.frame.width/12, height: self.mapView.frame.width/12
        ))
       // marker.iconView = markerView
        marker.map = mapView
          //  generateClusterItems()
        //comment this line if you don't wish to put a callout bubble
        mapView.selectedMarker = marker
        self.mapView.animate(to: camera)
     
        
        //Finally stop updating location otherwise it will come again and again in this delegate
        self.locationManager.stopUpdatingLocation()
        
    }

    @IBAction func menuClicked(_ sender: UIButton) {
        self.sideMenuController?.revealMenu()
        SideMenuController.preferences.basic.menuWidth = self.view.frame.width
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .right
        SideMenuController.preferences.basic.enablePanGesture = true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension Dictionary {
    var json: String {
        let invalidJson = "Not a valid JSON"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
        } catch {
            return invalidJson
        }
    }
    
    func dict2json() -> String {
        return json
    }
}
extension UIImage {
    func scaleToSize(aSize :CGSize) -> UIImage {
        if (self.size.equalTo(aSize)) {
            return self
        }
        
        UIGraphicsBeginImageContextWithOptions(aSize, false, 0.0)
        self.draw(in: CGRect(x: 0, y: 0, width: aSize.width, height: aSize.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
