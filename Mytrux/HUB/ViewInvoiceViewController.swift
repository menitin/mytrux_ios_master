//
//  ViewInvoiceViewController.swift
//  Mytrux
//
//  Created by Aboli on 18/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class ViewInvoiceViewController: BaseViewController,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource {
    
    
    
    @IBOutlet weak var invoiceNo: UILabel!
    @IBOutlet weak var invoiceDate: UILabel!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var invoiceBillToLabel: UILabel!
    @IBOutlet weak var invoiceGSTIN: UILabel!
    @IBOutlet weak var invoiceMobileNo: UILabel!
    @IBOutlet weak var bookingCustomerNameAdd: UILabel!
    @IBOutlet weak var invoiceNameAddress: UILabel!
    @IBOutlet weak var bookingGSTIN: UILabel!
    @IBOutlet weak var bookingMobileNo: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sacCode: UILabel!
    @IBOutlet weak var transporterAddress: UILabel!
    @IBOutlet weak var transporterGSTNO: UILabel!
    @IBOutlet weak var currency: UILabel!
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet weak var subTotalLabel: UILabel!
    @IBOutlet weak var CGSTLabel: UILabel!
    @IBOutlet weak var SGSTLabel: UILabel!
    @IBOutlet weak var IGSTLabel: UILabel!
    @IBOutlet weak var TotalAmount: UILabel!
    @IBOutlet weak var amountInWords: UILabel!
    @IBOutlet weak var fleetId: UILabel!
    @IBOutlet weak var loadId: UILabel!
    @IBOutlet weak var vehicleNo: UILabel!
    @IBOutlet weak var bookingNo: UILabel!
    @IBOutlet weak var mobileNo: UILabel!
    @IBOutlet weak var driverName: UILabel!
    //  @IBOutlet weak var addBillingBtnView: UIView!
    @IBOutlet weak var qrCodeView: UIView!
    @IBOutlet weak var qrCodeImageView: UIImageView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.topBarCard(topBar: topBar)
        //   self.buttonCardView(button: addBillingBtnView)
        NotificationCenter.default.addObserver(self, selector: #selector(self.viewInvoice(_:)), name: .viewInvoice, object: nil)
        scrollView.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    
    var bookingId = Int()
    
    @objc func viewInvoice(_ notification: NSNotification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        let bookingList = userInfo!["booking"] as! BookingElement
        self.bookingId = bookingList.id!
        print("bookingId ====== \(bookingId)")
        print("Booking billingParty ==== \(String(describing: bookingList.billingParty!))")
        self.transporterAddress.text = bookingList.shipper!.name! + " " + bookingList.shipper!.address!
        self.transporterGSTNO.text = bookingList.shipper!.loadOwner!.gstTaxRegNo!
        self.fleetId.text = bookingList.availableFleetBid?.bidCode!
        self.loadId.text = bookingList.availableLoadBid?.bidCode!
        self.billRecords.removeAll()
        self.exchangeBillRecordsArr.removeAll()
        billRecords(bookingId: self.bookingId)
        updateBillingCustomerText(bookingList: bookingList)
        let myStr = URLStatics.qrCode+"bookingId=\(bookingId)"
        let qrImage = createQRFromString(str: myStr)
        self.qrCodeImageView.image = convert(cmage:qrImage!)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        scrollView.contentOffset.x = 0
    }
    
   
    func updateBillingCustomerText(bookingList:BookingElement){
        invoiceNo.text = bookingList.bookingNo!
        invoiceNo.text = bookingList.bookingNo!
        invoiceBillToLabel.text = bookingList.billingParty!
        
        let timestamp = bookingList.createDate! / 1000
        let invoiceDatee = self.epochDate(epochDate: Double(timestamp))
        invoiceDate.text = invoiceDatee
        
        switch bookingList.billingParty! {
        case "BOOKING CUSTOMER":
            self.invoiceNameAddress.text = bookingList.loadOwner!.name!
            self.invoiceGSTIN.text = bookingList.loadOwner!.gstTaxRegNo!
            self.invoiceMobileNo.text = bookingList.loadOwner!.telephone!
            
            
        case "CONSIGNEE":
            self.invoiceNameAddress.text = bookingList.consignee!.name! + " " + bookingList.consignee!.address!
            self.invoiceGSTIN.text = bookingList.consignee!.loadOwner!.gstTaxRegNo!
            self.invoiceMobileNo.text = bookingList.consignee?.contactNo
            
        case "DELIVERY CUSTOMER":
            self.invoiceNameAddress.text = bookingList.deliveryCustomer!.name! + " " + bookingList.deliveryCustomer!.address!
            self.invoiceGSTIN.text = bookingList.deliveryCustomer!.loadOwner!.gstTaxRegNo!
            self.invoiceMobileNo.text = bookingList.deliveryCustomer!.contactNo
            
        case "SHIPPER":
            self.invoiceNameAddress.text = bookingList.shipper!.name! + " " + bookingList.shipper!.address!
            self.invoiceGSTIN.text = bookingList.shipper!.loadOwner!.gstTaxRegNo!
            self.invoiceMobileNo.text = bookingList.shipper!.contactNo
            
        default:
            self.invoiceNameAddress.text = bookingList.loadOwner!.name!
            self.invoiceGSTIN.text = bookingList.loadOwner!.gstTaxRegNo!
            self.invoiceMobileNo.text = bookingList.loadOwner!.telephone!
            
        }
        
        bookingCustomerNameAdd.text = bookingList.loadOwner!.name!
        bookingGSTIN.text = bookingList.loadOwner!.gstTaxRegNo!
        bookingMobileNo.text = bookingList.loadOwner!.telephone!
        
    }
    
    @IBAction func backClicked(_ sender: Any) {
        sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "BookingController") }, with: "tenth")
        sideMenuController?.setContentViewController(with: "tenth")
    }
    
    var exchangeBillRecordsArr = [ExchangeBillrecords]()
    var billRecords = [BillRecord]()
    var subTotal = Double()
    var SGST_CGST = Double()
    var totalAmount = Double()
    
    func billRecords(bookingId:Int){
        let parameters : [String:Any] = [
            "bookingId" : "\(bookingId)",
            "financialYear" : self.currentFinancialYear!
        ]
        self.activityIndicatorBegin()
        let headers = header()
        
        //+918669696004
        //asdfgf1234$
        
        Alamofire.request(URLStatics.exchangeBillrecords,method: .get, parameters: parameters, headers: headers).responseJSON { response in
            print("response.request=\(String(describing: response.request))")  // original URL request
            print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
            print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
            //   self.activityIndicatorEnd()
            switch (response.result) {
                
            case .success:
                
                if let reply = response.result.value {
                    print("exchangeBillrecords JSON: >>>>>>>>>>>>>>>>>>>>>>>>>> \(reply)")
                    let mainResponse = JSON(reply)
                    var returnDict = [String:Any]()
                    // let returnObj = mainResponse["data"].array
                    
                    if let returnObj = mainResponse["data"].dictionaryObject {
                        returnDict = returnObj
                    }
                    self.billRecords.removeAll()
                    let billRec = returnDict["billRecords"] as! [[String:Any]]
                    for bill in billRec{
                        print("bill billRecords === \(String(describing: bill["amount"]))")
                        let billDict = bill["billRecordType"] as! [String:Any]
                        let currencyMasDict = bill["currencyMaster"] as! [String:Any]
                        let currencyMaster = CurrencyMaster1(id: currencyMasDict["id"] as? Int, currencyName: currencyMasDict["currencyName"] as? String, currencyCountry: currencyMasDict["currencyCountry"] as? String, currencyCode: currencyMasDict["currencyCode"] as? String)
                        
                        let billRecordType = BillRecordType(id: billDict["id"] as? Int, billRecordType: billDict["billRecordType"] as? String, status: billDict["status"] as? Bool, sacCode: billDict["sacCode"] as? String)
                        
                        let billl = BillRecord(id: bill["id"]! as? Int,
                                               billRecordType: billRecordType,
                                               currencyMaster: currencyMaster,
                                               exchangeRate: bill["exchangeRate"]! as? Int,
                                               units: bill["units"]! as? Int,
                                               amount: bill["amount"]! as? Int,
                                               taxable: bill["taxable"]! as? Bool,
                                               recordType: bill["recordType"]! as? String,
                                               sacCode: bill["sacCode"]! as? String,
                                               billRecordDescription: bill["description"] as? String,
                                               taxDetail: bill["taxDetail"] as? String,
                                               fleet: nil)
                        self.billRecords.append(billl)
                    }
                    print("billRecords count  \(String(describing: self.billRecords.count))")
                    let exchangeBill = ExchangeBillrecords(billRecords: nil, invoiceDate: returnDict["invoiceDate"] as? Int, invoiceNo: returnDict["invoiceNo"] as? String, invoiceGrossAmount: returnDict["invoiceGrossAmount"] as? Int)
                    self.exchangeBillRecordsArr.append(exchangeBill)
                }
                
                print("exchangeBillRecordsArr count  \(String(describing: self.exchangeBillRecordsArr.count))")
                self.tableView.reloadData()
                self.tableViewHeightConstraint.constant = self.tableView.contentSize.height
                self.tableView.reloadData()
                //                self.billingPreviewTableView.reloadData()
                self.sacCode.text =  self.billRecords[0].sacCode!
                self.bookingNo.text = self.exchangeBillRecordsArr[0].invoiceNo!
                self.subTotal = 0
                for bill in self.billRecords{
                    if bill.amount != nil && bill.units != nil{
                        let total = Double(bill.units!) * Double(bill.amount!)
                        
                        self.subTotal += total
                    }
                }
                self.subTotalLabel.text = "\(self.subTotal)"
                self.totalAmount = self.subTotal + self.SGST_CGST + self.SGST_CGST
                self.TotalAmount.text = "\(self.totalAmount)"
                self.amountInWords.text = self.totalAmount.asWord.capitalized
                self.activityIndicatorEnd()
                break
            case .failure(let error):
                self.activityIndicatorEnd()
                print("POST REQUEST ERROR  - \(error.localizedDescription)")
                print("=== REQUEST INFORMATION ===")
                print("Request Payload: \(parameters)")
                print("===")
                break
            }
            
        }
        
    }
    
    private func createQRFromString(str: String) -> CIImage? {
        let stringData = str.data(using: .utf8)
        
        let filter = CIFilter(name: "CIQRCodeGenerator")
        filter?.setValue(stringData, forKey: "inputMessage")
        filter?.setValue("H", forKey: "inputCorrectionLevel")
        
        return filter?.outputImage
    }
    
    func convert(cmage:CIImage) -> UIImage
    {
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent)!
        let image:UIImage = UIImage.init(cgImage: cgImage)
        return image
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.billRecords.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "viewInvoice") as! VIewINvoiceTableViewCell
        let billRecord = self.billRecords[indexPath.row]
        self.tableViewHeightConstraint.constant = self.tableView.contentSize.height
        cell.billDescription.text = billRecord.billRecordType?.billRecordType!
        if billRecord.taxable == true {
            cell.checkMarkImage.image = UIImage(named: "blueChecked")
        }
        if billRecord.units != nil && billRecord.amount != nil{
            cell.unit.text = "\(Double(billRecord.units!))"
            cell.rate.text =  "\(Double(billRecord.amount!))" + "0"
            let total = Double(billRecord.units!) * Double(billRecord.amount!)
            print("total === \(total)")
            cell.total.text = "\(total)" + "0"
            
        }else{
            cell.unit.text = ""
            cell.rate.text =  ""
            cell.total.text = ""
        }
        return cell
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

public extension Double {
    public var asWord: String {
        let numberValue = NSNumber(value: self)
        var formatter = NumberFormatter()
        formatter.numberStyle = .spellOut
        return formatter.string(from: numberValue)!
    }
}
