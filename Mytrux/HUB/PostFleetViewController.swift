//
//  PostFleetViewController.swift
//  Mytrux
//
//  Created by Aboli on 25/06/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import SideMenuSwift
import GooglePlaces
import Alamofire
import SwiftyJSON
import iOSDropDown
import SearchTextField

class PostFleetViewController: BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate, GMSAutocompleteViewControllerDelegate,UITextFieldDelegate{
    
    @IBOutlet weak var commentTF: UITextField!
    @IBOutlet weak var freightAmountTF: UITextField!
    @IBOutlet weak var noOfVechTF: UITextField!
    @IBOutlet weak var toAMPMTF: UITextField!
    @IBOutlet weak var toTimeTF: UITextField!
    @IBOutlet weak var fromAMPMTF: UITextField!
    @IBOutlet weak var fromTimeTF: UITextField!
    @IBOutlet weak var cargoWtTF: UITextField!
    @IBOutlet weak var cargoTypeTF: UITextField!
    @IBOutlet weak var tillDateTF: UITextField!
    @IBOutlet weak var fromDateTF: UITextField!
    @IBOutlet weak var vehicleWtCapTF: UITextField!
    @IBOutlet weak var vechNoTF: UITextField!
    @IBOutlet weak var publicPrivateView: UIView!
    @IBOutlet weak var privateView: UIView!
    @IBOutlet weak var publicImageView: UIImageView!
    @IBOutlet weak var privateImageView: UIImageView!
    
    @IBOutlet weak var publicView: UIView!
    @IBOutlet weak var ftlRedLine: UIImageView!
    @IBOutlet weak var ftlLabel: UILabel!
    @IBOutlet weak var ftlWrapperView: UIView!
    @IBOutlet weak var containerRedLine: UIImageView!
    @IBOutlet weak var containerLabel: UILabel!
    @IBOutlet weak var containerWrapper: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var scrollVieww: UIScrollView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var topBar: UIView!
   
   
    @IBOutlet weak var fromTF: SearchTextField!
    @IBOutlet weak var toTF: SearchTextField!
    
    @IBOutlet weak var seperatorLine: UIView!
    @IBOutlet weak var cancelBtn: UIView!
    var selectedIndex = IndexPath()
    @IBOutlet weak var postFleetBtn: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    enum Roboto: String {
        case regular = ""
        case bold = "-Bold"
        func font(size: CGFloat) -> UIFont {
            return UIFont(name: "Roboto\(rawValue)", size: size)!
        }
    }
     var unitId = Int()
    var vehicleList = [VehicleList]()
    var FleetVehicleNameArray = [String]()
    var ContainerVehicleNameArray = [String]()
    var LTLVehicleNameArray = [String]()
    var vehicleTypeArray = [String]()
    var containerImageArray = [String]()
    var fleetImageArray = [String]()
    var ltlImageArray = [String]()
    var collectionViewNameArray = [String]()
    var collectionViewImageArray = [String]()
    var loadTypee = String()
    var id = Int()
    var mainId = Int()
    var isPrivate = false
    var isPublic = false
    var postType = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.clearCache(with: "fourth")
        NotificationCenter.default.addObserver(self, selector: #selector(self.postFleetFrom(notification:)), name: .postFleetFrom, object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.postFleetTill(notification:)), name: .postFleetTill, object: nil)
          NotificationCenter.default.addObserver(self, selector: #selector(self.postFleetUnitType(notification:)), name: .postFleetUnitType, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.postFleetCargoType(notification:)), name: .postFleetCargoType, object: nil)
             NotificationCenter.default.addObserver(self, selector: #selector(self.editFleetPosted(notification:)), name: .editFleetPosted, object: nil)
//  NotificationCenter.default.addObserver(self, selector: #selector(self.refreshViewPostFleet(notification:)), name: .refreshViewPostFleet, object: nil)
       
        NotificationCenter.default.addObserver(self, selector: #selector(self.postFleetFromDate(notification:)), name: .postFleetFromDate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.postFleetTillDate(notification:)), name: .postFleetTillDate, object: nil)
       
         
         
    }
    
    @IBAction func privateClicked(_ sender: Any) {
        isPrivate = true
        
        if isPrivate {
            publicImageView.image = UIImage.init(named: "unselectedCircleRed")
            privateImageView.image = UIImage.init(named: "sselectedBlue")
              postType = "PRIVATE"
            
        }else{
            
        }
    }
    
    @IBAction func publicViewClicked(_ sender: Any) {
        isPublic = true
        if isPublic {
            publicImageView.image = UIImage.init(named: "selectedCircleRed")
            privateImageView.image = UIImage.init(named: "unselectedBlue")
     
            postType = "PUBLIC"
        }else{
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        cardViewArray(arrView: [view1,view2,publicPrivateView])
        // topBarCard(topBar: topBar)
        scrollVieww.delegate = self
        buttonCardArray(arrView: [cancelBtn,postFleetBtn])
        collectionView.delegate = self
        collectionView.dataSource = self
        addDashedBorder(view: seperatorLine)
        self.isContainerClicked = true
        self.isFTLClicked = false
        self.ftlRedLine.isHidden = true
        self.containerRedLine.isHidden = false
        self.fromTF.delegate = self
        self.toTF.delegate = self
        self.cargoWtTF.text = "KGS"
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let result = formatter.string(from: date)
        self.fromDateTF.text = result
        postType = "PUBLIC"
         self.id = 3
        getLoadType()
        
        publicImageView.image = UIImage.init(named: "selectedCircleRed")
        privateImageView.image = UIImage.init(named: "unselectedBlue")
      
        self.isPublic = true
        
        for load in self.loadTypeArr{
            if load.code == "CON"{
                containerLabel.text = load.loadType
                self.id = load.id
                
            }
            if load.code == "FTL"{
                ftlLabel.text = load.loadType
            }
        }
        
        fromTF.theme.bgColor = UIColor(hexString: "F9F9F9")
        toTF.theme.bgColor = UIColor(hexString: "F9F9F9")
        fromTF.forceNoFiltering = true
        toTF.forceNoFiltering = true
        
        fromTF.theme.font = Roboto.bold.font(size: 12.0)
        toTF.theme.font = Roboto.bold.font(size: 12.0)
       fromTimeTF.font = Roboto.bold.font(size: 12.0)
        fromAMPMTF.font = Roboto.bold.font(size: 12.0)
        toAMPMTF.font = Roboto.bold.font(size: 12.0)
        toTimeTF.font = Roboto.bold.font(size: 12.0)
        
        tillDateTF.font = Roboto.bold.font(size: 12.0)
        cargoWtTF.font = Roboto.bold.font(size: 12.0)
        noOfVechTF.font = Roboto.bold.font(size: 12.0)
        cargoTypeTF.font = Roboto.bold.font(size: 12.0)
        vehicleWtCapTF.font = Roboto.bold.font(size: 12.0)
        freightAmountTF.font = Roboto.bold.font(size: 12.0)
        vechNoTF.font = Roboto.bold.font(size: 12.0)
        commentTF.font = Roboto.bold.font(size: 12.0)
      
        getVehicleList()
        
    }
    
    
    var cargoId = Int()
    
    @IBOutlet weak var postFleetLabel: UILabel!
    var container = Bool()
    var documentId = Int()
    var vechTyId = Int()
    var sequence : Int?
    var vechName = String()
    var fleetDatum : FleetDatum?
    var state = String()
    @objc func editFleetPosted(notification: Notification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        let fleetDetails = userInfo!["fleetDetails"] as! FleetDatum
        self.state =  userInfo!["state"] as! String
        self.fromTF.text = fleetDetails.availableLocation
        self.toTF.text = fleetDetails.destinationLocation
          let timestap: Double = Double(fleetDetails.availableDateTime / 1000)
        let availableFromtime = self.epochTime1(epochTimee: timestap)
         let fromTimeType = self.epochTimeType(epochtimeType: timestap)
        let availableFromdate = self.epochDate(epochDate: timestap)
        let timestamp: Double = Double(fleetDetails.closedBidTime / 1000)
        let availableTilltime = self.epochTime1(epochTimee: timestamp)
        let tillTimeType = self.epochTimeType(epochtimeType: timestamp)
        let availableTilldate = self.epochDate(epochDate: timestamp)
        self.fromDateTF.text = availableFromdate
        self.tillDateTF.text = availableTilldate
        self.cargoTypeTF.text = fleetDetails.cargoType.cargoType
        self.noOfVechTF.text = "1"
        self.cargoWtTF.text = fleetDetails.capacityWeightUnit.name
        self.unitId = fleetDetails.capacityWeightUnit.id
        self.vechNoTF.text = fleetDetails.registrationNo
        self.commentTF.text = fleetDetails.comments
        self.fromTimeTF.text = availableFromtime
        self.toTimeTF.text = availableTilltime
        self.fromAMPMTF.text = fromTimeType
        self.toAMPMTF.text = tillTimeType
        self.cargoId = fleetDetails.cargoType.id
        
        if fleetDetails.expectedFreight != nil {
             self.freightAmountTF.text = String(Double(Int(truncating: fleetDetails.expectedFreight!))) + "0"
        }else{
             self.freightAmountTF.text = "00.00"
        }
        
       
        self.postFleetLabel.text = "UPDATE"
        self.vehicleWtCapTF.text = String(fleetDetails.capacityWeight)
        self.mainId = fleetDetails.id
        self.container = fleetDetails.vehicleType.container
        self.documentId = fleetDetails.vehicleType.documentID
         self.vechTyId = fleetDetails.vehicleType.id
        self.postType = fleetDetails.postType!
        if postType == "PUBLIC"{
            publicImageView.image = UIImage.init(named: "selectedCircleRed")
            privateImageView.image = UIImage.init(named: "unselectedBlue")
            
        }else{
            publicImageView.image = UIImage.init(named: "unselectedCircleRed")
            privateImageView.image = UIImage.init(named: "sselectedBlue")
        }
        vechName = fleetDetails.vehicleType.name
        self.sequence = fleetDetails.sequence
        self.fleetDatum = fleetDetails
        self.collectionView.reloadData()
      
            if fleetDetails.loadType.code == "CON"{
                self.id = fleetDetails.loadType.id
            }else if fleetDetails.loadType.code == "FTL"{
                self.id = fleetDetails.loadType.id
            }
        
    }
    
//    @objc func refreshViewPostFleet(notification: Notification) {
//      viewDidLoad()
//
//    }
    
    @objc func postFleetCargoType(notification: Notification) {
        
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        let cargoType = userInfo!["cargoType"] as! String
        let cargoId = userInfo!["id"] as! Int
        self.cargoTypeTF.text = cargoType
        self.cargoId = cargoId
 
    }
    
    func epochTime1(epochTimee:Double)->String{
        
        let timeResult:Double = epochTimee
        let date = NSDate(timeIntervalSince1970: timeResult)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm" //Set time style
        
        let timeZone = TimeZone.current.identifier as String
        dateFormatter.timeZone = TimeZone(identifier: timeZone)
        let localDate = dateFormatter.string(from: date as Date)
        return "\(localDate)"
        
    }
    func epochTimeType(epochtimeType:Double)->String{
        
        let timeResult:Double = epochtimeType
        let date = NSDate(timeIntervalSince1970: timeResult)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "a" //Set time style
        
        let timeZone = TimeZone.current.identifier as String
        dateFormatter.timeZone = TimeZone(identifier: timeZone)
        let localDate = dateFormatter.string(from: date as Date)
        return "\(localDate)"
        
    }
    @objc func postFleetUnitType(notification: Notification) {
        
         let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        let unitType = userInfo!["unitType"] as! String
         unitId = userInfo!["id"] as! Int
        self.cargoWtTF.text = unitType
    }

    var availableDateTime = Double()
    var closeBidTime = Double()
    
    @objc func postFleetFrom(notification: Notification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        self.fromTimeTF.text = (userInfo!["time"] as! String)
        self.fromAMPMTF.text = (userInfo!["timeType"] as! String)
       
       
    }
    @objc func postFleetFromDate(notification: Notification) {
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        
        self.fromDateTF.text = (userInfo!["date"] as! String)
        
    }
    @objc func postFleetTill(notification: Notification) {
        
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
        self.toTimeTF.text = (userInfo!["time"] as! String)
        self.toAMPMTF.text = (userInfo!["timeType"] as! String)
      
        
    }
    @objc func postFleetTillDate(notification: Notification) {
        
        let userInfo : [String:Any]? = notification.userInfo as? [String:Any]
       
        self.tillDateTF.text = (userInfo!["date"] as! String)
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollVieww.contentOffset.x = 0
    }
    
   var loadTypeArr = [LoadType]()
    func getLoadType(){
        
        
        Alamofire.request(URLStatics.loadType, method: .get).responseJSON { response in
            print("response.request=\(String(describing: response.request))")  // original URL request
            print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
            print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
            
            if let reply = response.result.value {
                
                let json = JSON(reply)
                print(" loadType  json ===  : \(json)")
                var loadType : LoadType?
                for i in 0 ..< json.count{
                    let load = json[i]
                 loadType = LoadType(id: load["id"].intValue, code: load["code"].stringValue, loadType: load["loadType"].stringValue)
                    self.loadTypeArr.append(loadType!)
                }
                
            }
        }
        
    }
    
    
    
    func getVehicleList(){
        //+918669696004
        //asdfgf1234$
        Alamofire.request(URLStatics.vehicleList,method: .get, parameters: nil).responseJSON { response in
          
            switch (response.result) {
            case .success:
                
                if let reply = response.result.value {
                    let mainResponse = JSON(reply)
                     print(" vehicle list JSON: ==== \(mainResponse)")
                    
                    var returnDict = [[String:Any]]()
                    
                    if let returnObj = mainResponse.arrayObject as? [[String:Any]] {
                        returnDict = returnObj
                    }
                    self.ContainerVehicleNameArray.removeAll()
                    self.FleetVehicleNameArray.removeAll()
                    self.LTLVehicleNameArray.removeAll()
                    for data in returnDict{
                        var vehicleOrder = ""
                        var documentId = ""
                        var id = ""
                        var sequence = ""
                        var smallDocId = ""
                        if let vehicleOrderr = data["vehicleOrder"] as? Int {
                            vehicleOrder = "\(vehicleOrderr)"
                        }else{
                            vehicleOrder = ""
                        }
                        if let documentIdd = data["documentId"] as? Int {
                            documentId = "\(documentIdd)"
                        }else{
                            documentId = ""
                        }
                        if let idd = data["id"] as? Int {
                            id = "\(idd)"
                        }else{
                            id = ""
                        }
                        if let sequencee = data["sequence"] as? Int {
                            sequence = "\(sequencee)"
                        }else{
                            sequence = ""
                        }
                        if let smallDocIdd = data["smallDocId"] as? Int {
                            smallDocId = "\(smallDocIdd)"
                        }else{
                            smallDocId = ""
                        }
                        
                        
                        let vechicleListt = VehicleList(name: data["name"] as! String, vehicleOrder: vehicleOrder, smallDocId: smallDocId, documentId:documentId, id: id, vehicleType: data["vehicleType"] as! String, container: String(data["container"] as! Bool), type: data["type"] as! String, sequence: sequence)
                        self.vehicleList.append(vechicleListt)
                    }

                    for v in self.vehicleList{
                        if v.vehicleType == "FLEET" || v.vehicleType == "CAR/BIKE CARRIER" {
                            self.FleetVehicleNameArray.append(v.name)
                            self.fleetImageArray.append(v.smallDocId)
                        }
                        if v.vehicleType == "CONTAINER"{
                            self.ContainerVehicleNameArray.append(v.name)
                            self.containerImageArray.append(v.smallDocId)
                        }
                        if v.name == "PICKUP" || v.name == "407" || v.name == "709" || v.name == "1109" {
                            self.LTLVehicleNameArray.append(v.name)
                            self.ltlImageArray.append(v.smallDocId)
                        }
                    }
                 self.collectionViewNameArray = self.ContainerVehicleNameArray
                    self.collectionViewImageArray = self.containerImageArray
                    self.collectionView.reloadData()
                }
                break
            case .failure(let error):
                
                print("POST REQUEST ERROR  - \(error.localizedDescription)")
                print("=== REQUEST INFORMATION ===")
                break
            }
        }
    }
    
    
    
    
    var isfromTF = false
    var istoTF = false
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let inputFromStr = fromTF.text!.replacingOccurrences(of: " ", with: "")
        let inputToStr = toTF.text!.replacingOccurrences(of: " ", with: "")
        
        if textField == fromTF {
            self.isfromTF = true
            self.istoTF = false
            if inputFromStr != "" {
                placesApi(input: inputFromStr)
            }
        }
        
        if textField == toTF {
            self.isfromTF = false
            self.istoTF = true
            if inputToStr != "" {
                placesApi(input: inputToStr)
            }
        }
       
        return true
    }
    
    @IBAction func menuBtn(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionViewNameArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "postFleetCell", for: indexPath) as! PostFleetCollectionViewCell
        
        cell.imageView.image = UIImage(named: "")
        
        if selectedIndex == indexPath
        {
            cell.innerView.backgroundColor = UIColor.init(hexString: "#DD0B51")
        }
        else
        {
            cell.innerView.backgroundColor = UIColor.clear
        }
        
      
        

        let name = collectionViewNameArray[indexPath.item]
        let image = collectionViewImageArray[indexPath.item]
        cell.nameLabel.text = name
        if name == self.vechName {
            cell.innerView.backgroundColor = UIColor.init(hexString: "#DD0B51")
        }
        Alamofire.request(URLStatics.vehicleImage + "\(image)" ).responseImage { response in
            debugPrint(response)
            
            print(response.request!)
            print(response.response!)
            debugPrint(response.result)
            if let imagee = response.result.value {
                print("image downloaded: \(imagee)")
                cell.imageView.image = imagee
            }
        }
  
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemWidth: CGFloat = ((collectionView.bounds.size.width)  / CGFloat(4.0))
        return CGSize(width: itemWidth, height: collectionView.bounds.size.height)
        
    }
    var selectedVech = String()
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath
       print("self.collectionViewNameArray[indexPath] ===  \(self.collectionViewNameArray[indexPath.row])")
        selectedVech = self.collectionViewNameArray[indexPath.row]
    self.collectionView.reloadData()
    }
    
    var isContainerClicked = false
    var isFTLClicked = false
    @IBAction func containerClicked(_ sender: Any) {
        isContainerClicked = true
        isFTLClicked = false
        if isContainerClicked == true{
            containerLabel.textColor = UIColor(hexString: "#0F2541")
            self.containerRedLine.isHidden = false
            self.ftlRedLine.isHidden = true
            self.ftlLabel.textColor = UIColor(hexString: "#6F7179")
            self.collectionViewNameArray = self.ContainerVehicleNameArray
            self.collectionViewImageArray = self.containerImageArray
          
            
            for load in self.loadTypeArr{
                if load.code == "CON" {
                    self.loadTypee = load.loadType
                    self.id = load.id
                }
            }
              self.collectionView.reloadData()
            //   self.saveVehicleImage()
        }else if isContainerClicked == false {
            
        }
        self.collectionView.reloadData()
    }
    
    @IBAction func ftlClicked(_ sender: Any) {
        
        isContainerClicked = false
        isFTLClicked = true
        
        if isFTLClicked == true{
            ftlLabel.textColor = UIColor(hexString: "#0F2541")
            self.containerLabel.textColor = UIColor(hexString: "#6F7179")
            self.containerRedLine.isHidden = true
            self.ftlRedLine.isHidden = false
            self.collectionViewNameArray = self.FleetVehicleNameArray
            self.collectionViewImageArray = self.fleetImageArray
            for load in self.loadTypeArr{
                if load.code == "FTL" {
                    self.loadTypee = load.loadType
                    self.id = load.id
                }
            }
            self.collectionView.reloadData()
        }else if isFTLClicked == false {
        }
        
        self.collectionView.reloadData()
        
    }
    
    var placesArray = [String]()
    
    func placesApi(input:String){
   Alamofire.request("https://maps.googleapis.com/maps/api/place/autocomplete/json?key=AIzaSyDw6zMdDicRS0fXZB5Ga5Q1NWzhkzpFcfo&components=country:in&region=in&input=\(input)", method: .get, parameters: nil)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                
                switch (response.result) {
                case .success:
                    var returnDict = [[String:Any]]()
                    if let reply = response.result.value {
                        
                        let mainResponse = JSON(reply)
                        print("======= JSON: places api ========  \(mainResponse)")
                        
                        if let returnObj = mainResponse["predictions"].arrayObject as? [[String:Any]] {
                            returnDict = returnObj
                        }
                        self.placesArray.removeAll()
                        for data in returnDict{
                            print("== description ==  \(String(describing: data["description"]))")
                            self.placesArray.append(data["description"] as! String)
                        }
                        
                        if self.placesArray.count != 0 && self.isfromTF{
                            self.fromTF.filterStrings(self.placesArray)

                        }else if self.placesArray.count != 0 && self.istoTF{
                            self.toTF.filterStrings(self.placesArray)
                        }else{
                            return
                        }
                        
                        
                        let geocoder = CLGeocoder()
                        let availableLocation = self.fromTF.text!
                        geocoder.geocodeAddressString(availableLocation, completionHandler: {(placemarks, error) -> Void in
                            if((error) != nil){
                                print("Error", error ?? "")
                            }
                            if let placemark = placemarks?.first {
                                let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
                                print("Lat: \(coordinates.latitude) -- Long: \(coordinates.longitude)")
                                self.availLat = Double(coordinates.latitude)
                                self.availLong = Double(coordinates.longitude)
                            }
                        })
                        
                         let geocoder1 = CLGeocoder()
                        
                        let destinationLocation = self.toTF.text!
                        geocoder1.geocodeAddressString(destinationLocation, completionHandler: {(placemarks, error) -> Void in
                            if((error) != nil){
                                print("Error", error ?? "")
                            }
                            if let placemark = placemarks?.first {
                                let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
                                print("Lat: \(coordinates.latitude) -- Long: \(coordinates.longitude)")
                                self.destLat = Double(coordinates.latitude)
                                self.destLong = Double(coordinates.longitude)
                            }
                        })
                        
                    }
                    break
                case .failure(let error):
                    
                    print("== error == \(error.localizedDescription)")
                    break
                }
        }
    }
    
    var availLat = Double()
    var availLong = Double()
    var destLat = Double()
    var destLong = Double()
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // Get the place name from 'GMSAutocompleteViewController'
        // Then display the name in textField
        fromTF.text = place.name
       
        // Dismiss the GMSAutocompleteViewController when something is selected
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        // Dismiss when the user canceled the action
        dismiss(animated: true, completion: nil)
    }
    
  
    
    @IBAction func fromDateClicked(_ sender: Any) {
          self.picker(state:"DatePickerFrom")
    }
    
    @IBAction func tillDateClicked(_ sender: Any) {
         self.picker(state:"DatePickerTill")
    }
    
    @IBAction func cargoTypeClicked(_ sender: Any) {
        self.picker(state: "CargoType")
    }
    
    @IBAction func fromTimeClicked(_ sender: Any) {
        self.picker(state:"TimePickerFrom")
    }
    
    @IBAction func tillTimeCLicked(_ sender: Any) {
        self.picker(state:"TimePickerTill")
    }
    
    @IBAction func cargoWtCapClicked(_ sender: Any) {
        self.picker(state: "UnitType")
    }
    
    @IBAction func postFleetClicked(_ sender: Any) {
        self.postFleet()
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
    
      
        if postFleetLabel.text == "UPDATE" && self.state == "FleetPosted"{
              sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "fleetsPostedVC") }, with: "third")
            sideMenuController?.setContentViewController(with: "third")
        }else if postFleetLabel.text == "UPDATE" && self.state == "Dashboard"{
           
            SideMenuController.preferences.basic.defaultCacheKey = "default"
            sideMenuController?.setContentViewController(with: "default")
            
        }
        else{
              sideMenuController?.revealMenu()
        }
        
    }
    
 
    
    func postFleet(){
        
        if fromTF.text == "" && toTF.text == "" && fromDateTF.text == "" && toTimeTF.text == "" && self.fromTimeTF.text == "" && self.fromAMPMTF.text == "" && self.tillDateTF.text == "" && self.toAMPMTF.text == "" && cargoTypeTF.text == "" && cargoWtTF.text == "" && vehicleWtCapTF.text == "" && freightAmountTF.text == "" && self.id == 0 && self.cargoId == 0 && self.mainId == 0 && self.unitId == 0{
            
            let alert = UIAlertController(title: "", message: "Fill all mandatory fields", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        
        self.activityIndicatorBegin()
        
        let dateTime = "\(String(describing: fromDateTF.text!))" + " \(fromTimeTF.text!)" + " \(fromAMPMTF.text!)"
      // print("dateTime =>>> \(dateTime)")
        
        let dateString = dateTime
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm  a"
        let s = dateFormatter.date(from: dateString)
        //print("validDateTime =>> \(String(describing: s!))")
        let dateTimestamp = Int(s!.timeIntervalSince1970)
       // print("dateTimestamp =>> \(dateTimestamp)")
        self.availableDateTime = Double((dateTimestamp * 1000))
       // print("availableDateTime =>> \(availableDateTime)")
        
        
        let datetillTime = "\(String(describing: tillDateTF.text!))" + " \(toTimeTF.text!)" + " \(toAMPMTF.text!)"
        print("dateTime =>>> \(datetillTime)")
        
        let dateStringg = datetillTime
        let dateFormatterr = DateFormatter()
        dateFormatterr.dateFormat = "dd-MM-yyyy HH:mm  a"
        let s1 = dateFormatterr.date(from: dateStringg)
        print("validDateTime =>> \(String(describing: s1!))")
        let dateTimestampp = Int(s1!.timeIntervalSince1970)
        print("dateTimestamp =>> \(dateTimestampp)")
        self.closeBidTime = Double((dateTimestampp * 1000))
        print("closeBidTime =>> \(closeBidTime)")
    

    
        var vechList : VehicleList?
        for vech in self.vehicleList{
            if vech.name == self.selectedVech{
           vechList = VehicleList(name: vech.name, vehicleOrder: vech.vehicleOrder, smallDocId: vech.smallDocId, documentId: vech.documentId, id: vech.id, vehicleType: vech.vehicleType, container: vech.container, type: vech.type, sequence: vech.sequence)
            }
        }
         var parameters = [String:Any]()
       let availableLocation = self.fromTF.text!
        let destinationLocation = self.toTF.text!

        if postFleetLabel.text == "UPDATE"{
            
            parameters = ["availableDateTime":fleetDatum!.availableDateTime,
                          "availableLatitude":fleetDatum!.availableLatitude!,
                          "availableLocation":"\(fleetDatum!.availableLocation)",
                "availableLongitude":fleetDatum!.availableLongitude!,
                "capacityWeight":self.vehicleWtCapTF.text!,
                "capacityWeightUnit":["id":self.unitId],
                "cargoType":["id":self.cargoId],
                "closedBidTime":fleetDatum!.closedBidTime,
                "comments":"\(self.commentTF.text!)",
                "currencyMaster":["id":2],
                "destinationLatitude":fleetDatum?.destinationLatitude as Any,
                "destinationLocation":"\(destinationLocation)",
                "destinationLongitude":fleetDatum!.destinationLongitude!,
                "expectedFreight": self.freightAmountTF.text!,
                "loadType":["id":self.id],
                "id": self.mainId,
                "noOfVehicles":1,
                "registrationNo":"\(self.vechNoTF.text!)",
                "postType":"\(self.postType)",
                "vehicleType":["container": self.container,"documentId":self.documentId,"id":self.vechTyId,"sequence":self.sequence ?? 0]]
        
            
        }else{
            parameters = ["availableDateTime":availableDateTime,
                          "availableLatitude":availLat,
                          "availableLocation":"\(availableLocation)",
                "availableLongitude":availLong,
                "capacityWeight":self.vehicleWtCapTF.text!,
                "capacityWeightUnit":["id":self.unitId],
                "cargoType":["id":self.cargoId],
                "closedBidTime":closeBidTime,
                "comments":"\(self.commentTF.text!)",
                "currencyMaster":["id":2],
                "destinationLatitude":destLat,
                "destinationLocation":"\(destinationLocation)",
                "destinationLongitude":destLong,
                "expectedFreight": self.freightAmountTF.text!,
                "loadType":["id":self.id],
                
                "noOfVehicles":1,
                "registrationNo":"\(self.vechNoTF.text!)",
                 "postType":"\(self.postType)", "vehicleType":["container":vechList!.container,"documentId":vechList!.documentId,"id":vechList!.id,"sequence":vechList!.sequence]]
        }
        
       
   let headers = header()
        
        Alamofire.request(URLStatics.postFleet, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                print("response.request=\(String(describing: response.request))")  // original URL request
                print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                
                switch (response.result) {
                case .success:
                    var returnDict = [String:Any]()
                    if let reply = response.result.value {
            self.activityIndicatorEnd()
            
                       let mainResponse = JSON(reply)
         print("mainResponse ===== \(mainResponse)")
                        
                        if mainResponse["warning"].stringValue != "" {
                            let alertController = UIAlertController(title: "\(mainResponse["warning"].stringValue)", message:"", preferredStyle: .alert)
                            
                            // Create the actions
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                                if self.postFleetLabel.text == "UPDATE" && self.state == "FleetPosted"{
                                    self.sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "fleetsPostedVC") }, with: "third")
                                    self.sideMenuController?.setContentViewController(with: "third")
                                }else if self.postFleetLabel.text == "UPDATE" && self.state == "Dashboard"{
                                    
                                    SideMenuController.preferences.basic.defaultCacheKey = "default"
                                    self.sideMenuController?.setContentViewController(with: "default")
                                    
                                }
                                else{
                                    self.sideMenuController?.revealMenu()
                                }
                            }
                            
                            
                            // Add the actions
                            alertController.addAction(okAction)
                            
                            // Present the controller
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                        
                        if mainResponse["returnObj"].stringValue != "" {
                            
                       
                        let returnObj = mainResponse["returnObj"].stringValue
                        do {
                            returnDict = try self.convertToDictionary(from: returnObj)
                            print("returnDict = \(returnDict)")
                        } catch {
                            print(error)
                        }

                    if mainResponse["success"].stringValue != ""{
                            
                            let alertController = UIAlertController(title: "\(returnDict["bidCode"]!)", message: "\(mainResponse["success"])", preferredStyle: .alert)
                            
                            // Create the actions
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                                if self.postFleetLabel.text == "UPDATE" && self.state == "FleetPosted"{
                                    self.sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: "fleetsPostedVC") }, with: "third")
                                    self.sideMenuController?.setContentViewController(with: "third")
                                }else if self.postFleetLabel.text == "UPDATE" && self.state == "Dashboard"{
                                    
                                    SideMenuController.preferences.basic.defaultCacheKey = "default"
                                    self.sideMenuController?.setContentViewController(with: "default")
                                    
                                }
                                else{
                                    self.sideMenuController?.revealMenu()
                                }
                            }
                            
                            
                            // Add the actions
                            alertController.addAction(okAction)
                            
                            // Present the controller
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                            }
                     
                            
                        }
                }
                    break
                case .failure(let error):
                    self.activityIndicatorEnd()
                    print("== error == \(error.localizedDescription)")
                    break
                }
        }

        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
}
