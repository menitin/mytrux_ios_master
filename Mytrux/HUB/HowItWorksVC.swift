//
//  HowItWorksVC.swift
//  Mytrux
//
//  Created by Mytrux on 19/11/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class HowItWorksVC: BaseViewController {
    @IBOutlet weak var topBar: UIView!
    
    @IBOutlet weak var driverTrackingView: UIView!
    @IBOutlet weak var serviceUserView: UIView!
    @IBOutlet weak var serviceProviderView: UIView!
    @IBOutlet weak var awardView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        topBarCard(topBar: topBar)
        cardViewArray(arrView: [driverTrackingView,serviceUserView,serviceProviderView,awardView])
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
