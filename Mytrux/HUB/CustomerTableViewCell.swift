//
//  CustomerTableViewCell.swift
//  Mytrux
//
//  Created by Mytrux on 18/11/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class CustomerTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var subVieww: UIView!
    @IBOutlet weak var address: UILabel!
   
  override func awakeFromNib() {
        super.awakeFromNib()
        mainView.layer.cornerRadius = 5
              mainView.layer.shadowColor = UIColor.gray.cgColor
              mainView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
              mainView.layer.shadowRadius = 5
              mainView.layer.shadowOpacity = 0.4
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
