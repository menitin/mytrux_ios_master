//
//  BookNowMatchLoadCell.swift
//  Mytrux
//
//  Created by Mytrux on 11/11/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class BookNowMatchLoadCell: UICollectionViewCell {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var matchLoadView: UIView!
    @IBOutlet weak var loadNoLabel: UILabel!
    @IBOutlet weak var quotedImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var amount: UILabel!
    
    override func awakeFromNib() {
            super.awakeFromNib()
            matchLoadView.layer.cornerRadius = 4
//            mainView.layer.shadowColor = UIColor.gray.cgColor
//            mainView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
//            mainView.layer.shadowRadius = 5
//            mainView.layer.shadowOpacity = 0.4            // Initialization code
        }
    override func setNeedsLayout() {
           self.mainView.layoutIfNeeded()
       }
       
}
