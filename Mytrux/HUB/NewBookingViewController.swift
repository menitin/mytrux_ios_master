//
//  NewBookingViewController.swift
//  Mytrux
//
//  Created by Mytrux on 27/07/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import SideMenuSwift
import StepIndicator

class NewBookingViewController: UIViewController {
    @IBOutlet weak var stepView: StepIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    stepView.currentStep = 0
          NotificationCenter.default.addObserver(self, selector: #selector(self.bookingDetails(notification:)), name: Notification.Name("bookingDetails"), object: nil)
          NotificationCenter.default.addObserver(self, selector: #selector(self.vehicleDetails(notification:)), name: Notification.Name("vehicleDetails"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.bookingDetailsReverse(notification:)), name: Notification.Name("bookingDetailsReverse"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.vehicleDetailsReverse(notification:)), name: Notification.Name("vehicleDetailsReverse"), object: nil)
        
        // Do any additional setup after loading the view.
    }
     @objc func bookingDetails(notification: Notification) {
        stepView.currentStep = 1
    }
     @objc func vehicleDetails(notification: Notification) {
        stepView.currentStep = 2
    }
    @objc func bookingDetailsReverse(notification: Notification) {
        stepView.currentStep = 0
    }
    @objc func vehicleDetailsReverse(notification: Notification) {
        stepView.currentStep = 1
    }

    @IBAction func backClicked(_ sender: Any) {
         SideMenuController.preferences.basic.defaultCacheKey = "default"
        sideMenuController?.setContentViewController(with: "default")
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
