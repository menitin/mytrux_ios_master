//
//  SelectProfileVC.swift
//  Mytrux
//
//  Created by Mukta Bhuyar Punjabi on 17/05/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit



class SelectProfileVC: BaseViewController {

    @IBOutlet weak var backButton: UIView!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var proGoStackView: UIStackView!
    @IBOutlet weak var mtx_goView: UIView!
    @IBOutlet weak var mtx_proView: UIView!
    @IBOutlet weak var serviceState: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let arr = [mtx_goView,mtx_proView]
        cardViewArray(arrView: arr as! [UIView])
       topBarCard(topBar: topBar)
     //   CoreDataManager.getUserState()
        self.serviceState.text! = userDefaults.value(forKey: UserDefaultsContants.SERVICE_STATE)! as! String
        let backtap = UITapGestureRecognizer(target: self, action: #selector(self.handleBackButtonTap(_:)))
        backButton.addGestureRecognizer(backtap)
        backButton.isUserInteractionEnabled = true
        self.topBar.addSubview(backButton)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleMtxGo(_:)))
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.handleMtxPro(_:)))
        mtx_goView.addGestureRecognizer(tap)
        mtx_proView.addGestureRecognizer(tap1)
        mtx_goView.isUserInteractionEnabled = true
        mtx_proView.isUserInteractionEnabled = true
        self.proGoStackView.addSubview(mtx_goView)
        self.proGoStackView.addSubview(mtx_proView)
        
        // Do any additional setup after loading the view.
    }
    
    @objc func handleBackButtonTap(_ sender: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"loadsAndTrucksVC")
        viewController.modalPresentationStyle = .fullScreen
        transitionVc(vc: viewController, duration: 0.5, type: .fromLeft)
    }
    
    @objc func handleMtxGo(_ sender: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"LoginVC") as! LoginViewController
        viewController.image_logo = "Mtx_Go"
        viewController.label_title = "MTX_GO"
        viewController.modalPresentationStyle = .fullScreen
        userDefaults.set(UserStates.GO, forKey: UserDefaultsContants.MTX_STATE);
          userDefaults.synchronize()
        transitionVc(vc: viewController, duration: 0.5, type: .fromRight)
    }
    @objc func handleMtxPro(_ sender: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"LoginVC") as! LoginViewController
        viewController.modalPresentationStyle = .fullScreen
        viewController.image_logo = "Mtx_Pro"
        viewController.label_title = "MTX_PRO"
        userDefaults.set(UserStates.PRO, forKey: UserDefaultsContants.MTX_STATE);
        userDefaults.synchronize()
        transitionVc(vc: viewController, duration: 0.5, type: .fromRight)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
