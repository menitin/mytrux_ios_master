//
//  Step2RegisterVC.swift
//  Mytrux
//
//  Created by Mukta Bhuyar Punjabi on 22/05/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import StepIndicator
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

protocol selectedMode {
    func mode(mode : Int)
    
}
class Step2RegisterVC: BaseViewController,UITextFieldDelegate,passSelectedItem {
    func passCompanyType(companyType: String, id: Int, companyCategories: String) {
        }
    
    func passCompanyType(userSubType: String) {
        
    }
    
    func passState(state: String) {
        self.stateTf.text = state
    }
    
    func passCity(city: String) {
        self.city.text = city
    }
     var delegateMode : selectedMode?
  
  
    @IBOutlet weak var pan10: UITextField!
    @IBOutlet weak var pan9: UITextField!
    @IBOutlet weak var pan8: UITextField!
    @IBOutlet weak var pan7: UITextField!
    @IBOutlet weak var pan6: UITextField!
    @IBOutlet weak var pan5: UITextField!
    @IBOutlet weak var pan4: UITextField!
    @IBOutlet weak var pan3: UITextField!
    @IBOutlet weak var pan2: UITextField!
    @IBOutlet weak var pan1: UITextField!
    
    @IBOutlet weak var seperator_1: UIImageView!
    @IBOutlet weak var backBtn: UIView!
    @IBOutlet weak var panNumberWrapper: UIView!
    @IBOutlet weak var nextBtn: UIView!
    @IBOutlet weak var companyFullName: UITextField!
    @IBOutlet weak var companyCode: UITextField!
    @IBOutlet weak var billingAddress: UITextField!
    @IBOutlet weak var textFieldWrraper: UIView!
    @IBOutlet weak var backButton: UIView!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var seperator_2: UIImageView!
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var buttonStack: UIStackView!
    @IBOutlet weak var stateTf: UITextField!
    @IBOutlet weak var pincode: UITextField!
    @IBOutlet weak var country: UITextField!
    @IBOutlet weak var stepIndicatorView: StepIndicatorView!
    @IBOutlet weak var companyCodeView: UIView!
    @IBOutlet weak var stepIndicatorWrapper: UIView!
    @IBOutlet weak var bottomView: UIView!
    var userData = [User]()
    var tfArray = [UITextField]()
    override func viewDidLoad() {
        super.viewDidLoad()
        topBarCard(topBar: topBar)
        tfArray = [pan1,pan2,pan3,pan4,pan5,pan6,pan7,pan8,pan9,pan10]
        for tf in tfArray{
            tf.delegate = self
        }
     
        UserDefaults.standard.removeObject(forKey: UserDefaultsContants.SELECTED_STATE)
        let state = UserDefaults.standard.value(forKey: UserDefaultsContants.MTX_STATE)! as! String
        print("state = \(state)")
    

        self.titleLabel.text = state
        if (state == "MTX GO"){
            self.companyCodeView.isHidden = true
            self.seperator_2.isHidden = true
        }else{
            self.companyCodeView.isHidden = false
            self.seperator_2.isHidden = false
        }
  
        let textFieldArray = [companyFullName,billingAddress,stateTf,city,pincode,country] as! [UITextField]
        for tf in textFieldArray{
            tf.delegate = self
        }
        companyCode.delegate = self
        let buttonArr = [backBtn,nextBtn] as! [UIView]
        buttonCardArray(arrView: buttonArr)
        let backBtnn = UITapGestureRecognizer(target: self, action: #selector(self.backButtonTap(_:)))
        backBtn.addGestureRecognizer(backBtnn)
        backBtn.isUserInteractionEnabled = true
       
        let cardArr = [textFieldWrraper,panNumberWrapper] as! [UIView]
        cardViewArray(arrView: cardArr)
        let backtap = UITapGestureRecognizer(target: self, action: #selector(self.handleBackButtonTap(_:)))
        backButton.addGestureRecognizer(backtap)
        backButton.isUserInteractionEnabled = true
        self.topBar.addSubview(backButton)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil);
    }
   
    var email = ""
    var firstName = ""
    var surname = ""
    var mobile = ""
    var password = ""
    var confirmPassword = ""
    var companytype = ""
    var companyCate = ""
    var id = ""
    var cit = ""
    var add = ""
    var coun = ""
    var sta = ""
    var pin = ""
    var type = [String:Any]()
    var user = [String:Any]()
    
    override func viewWillAppear(_ animated: Bool) {
        email = userDefaults.value(forKey: RegisterConstants.Email)! as! String
        firstName = userDefaults.value(forKey: RegisterConstants.FirstName)! as! String
        surname = userDefaults.value(forKey: RegisterConstants.Surname)! as! String
        mobile = userDefaults.value(forKey: RegisterConstants.MobileNo)! as! String
        password = userDefaults.value(forKey: RegisterConstants.passward)! as! String
        confirmPassword = userDefaults.value(forKey: RegisterConstants.confirmPassword)! as! String
        
        if isKeyPresentInUserDefaults(key: RegisterConstants.companyType) == true{
            companytype = userDefaults.value(forKey: RegisterConstants.companyType)! as! String
        }
        
        if isKeyPresentInUserDefaults(key: RegisterConstants.companyCategories) == true{
             companyCate = userDefaults.value(forKey: RegisterConstants.companyCategories)! as! String
        }
       
        id = userDefaults.value(forKey: RegisterConstants.id)! as! String
        let typee = TypeofCompany(id: Int(id)!, companyType: companytype, companyCategories: companyCate)
        let jsonDataa = try! JSONEncoder().encode(typee)
        let type = String(data: jsonDataa, encoding: .utf8)!
        
        do {
            self.type = try self.convertToDictionary(from: type)
            print(self.user)
        } catch {
            print(error)
        }
        let userr = User(firstName: firstName, surname: surname, designation: "", mobileNo: "+91" + mobile, email: email, serviceType: UserDefaults.standard.value(forKey: UserDefaultsContants.MTX_STATE)! as! String, appActivated: UserDefaults.standard.value(forKey: UserDefaultsContants.SELECTED_SERVICE) as! String)
        let jsonData = try! JSONEncoder().encode(userr)
        let user = String(data: jsonData, encoding: .utf8)!
        do {
            self.user = try self.convertToDictionary(from: user)
            print(self.user)
        } catch {
            print(error)
        }
        print("email = \(email)")
    }
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var stateView: UIView!
    @IBAction func stateTapped(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let presentedViewController = storyBoard.instantiateViewController(withIdentifier: "ListMV") as! ListModelViewController
        presentedViewController.providesPresentationContextTransitionStyle = true
        presentedViewController.definesPresentationContext = true
        UserDefaults.standard.set(1, forKey: UserDefaultsContants.SELECTED_MODE)
        presentedViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
        presentedViewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        presentedViewController.delegate = self
        presentedViewController.selectedState = self.stateTf.text!
        if delegateMode != nil {
            delegateMode?.mode(mode: 1)
           
        }
        UserDefaults.standard.set(1, forKey: UserDefaultsContants.SELECTED_MODE)
        UserDefaults.standard.synchronize()
       
        self.present(presentedViewController, animated: true, completion: nil)
        
        
    }
    override func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    func nextTag(currentTag:Int)->Int{
        
        if(currentTag < tfArray.count - 1){
            return currentTag + 1
        }
        return -1
    }
    
    func nextTextFieldToFirstResponder(textField: UITextField) {
        
        let nextTag = self.nextTag(currentTag: 1)
        
        if(nextTag >= 0){
            self.tfArray[nextTag].becomeFirstResponder()
        }else{
          textField.resignFirstResponder()
        }
    }
    @IBAction func cityTapped(_ sender: Any) {
        if isKeyPresentInUserDefaults(key:UserDefaultsContants.SELECTED_STATE) == false{
            let alert = UIAlertController(title: "", message: "Please select state", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let presentedViewController = storyBoard.instantiateViewController(withIdentifier: "ListMV") as! ListModelViewController
        presentedViewController.providesPresentationContextTransitionStyle = true
        presentedViewController.definesPresentationContext = true
        presentedViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
        presentedViewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        presentedViewController.delegate = self
        UserDefaults.standard.set(2, forKey: UserDefaultsContants.SELECTED_MODE)
        UserDefaults.standard.synchronize()
        if delegateMode != nil {
            delegateMode?.mode(mode: 2)
        }
        self.present(presentedViewController, animated: true, completion: nil)
    }
    
    @IBOutlet weak var img1: UIImageView!
    @objc func keyboardWillShow(notification: NSNotification) {
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        UIView.animate(withDuration: 0.2) {
            self.bottomView.transform = CGAffineTransform.identity.translatedBy(x: 0, y: -keyboardHeight)
        }
    }
    
    @IBAction func nextBtnClicked(_ sender: UITapGestureRecognizer) {
        userDefaults.set("\(companyFullName.text!)", forKey: RegisterConstants.CompanyName)
        userDefaults.set("\(companyCode.text!)", forKey: RegisterConstants.compCode)
        userDefaults.set("\(billingAddress.text!)", forKey: RegisterConstants.address)
        userDefaults.set("\(stateTf.text!)", forKey: RegisterConstants.state)
        userDefaults.set("\(city.text!)", forKey: RegisterConstants.city)
        userDefaults.set("\(pincode.text!)", forKey: RegisterConstants.pincode)
        userDefaults.set("\(country.text!)", forKey: RegisterConstants.country)
        userDefaults.synchronize()
        self.register()
        print("comp = \(userDefaults.value(forKey: RegisterConstants.CompanyName)! as! String)")
        print("state = \(userDefaults.value(forKey: RegisterConstants.state)! as! String)")
    }
    
    @IBAction func selectCountry(_ sender: UITapGestureRecognizer) {
        self.presentModal(title: "Select Country", item: "India")
     
    }
    
    func presentModal(title:String,item:String){
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let presentedViewController = storyBoard.instantiateViewController(withIdentifier: "ModelVC") as! ModelViewController
        presentedViewController.providesPresentationContextTransitionStyle = true
        presentedViewController.definesPresentationContext = true
        presentedViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
        presentedViewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        presentedViewController.isSelectModalPresent = true
        presentedViewController.isCameraModalPresent = false
        presentedViewController.titleLabel.text = title
        presentedViewController.itemLabel.text = item
        
        self.present(presentedViewController, animated: true, completion: nil)
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let tfTag = [1,2,3,4,5,10]
        for tf in tfArray{
            if(textField == tf) {
                let maxLength = 1
                let currentString: NSString = tf.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }
         if textField == tf {
                for tag in tfTag{
                    if tf.tag == tag{
                        let allowedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
                        let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
                        let typedCharacterSet = CharacterSet(charactersIn: string)
                        let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
                        return alphabet
                    }else{
                        let allowedCharacters = CharacterSet(charactersIn:"0123456789")//Here change this characters based on your requirement
                        let characterSet = CharacterSet(charactersIn: string)
                        return allowedCharacters.isSuperset(of: characterSet)
                    }
                }

            }
            
//            if textField == pan1{
//                let allowedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
//                                        let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
//                                        let typedCharacterSet = CharacterSet(charactersIn: string)
//                                        let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
//                                        return alphabet
//            }
            
    }
        return true
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        _ = keyboardRectangle.height
        UIView.animate(withDuration: 0.2) {
            self.bottomView.transform = CGAffineTransform.identity
        }
    }
    var panNumber = ""
    func validation(){
        let panNo1 = (pan1.text! + pan2.text!)
        let panNo2 = (pan3.text! + pan4.text!)
        let panNo3 = (pan5.text! + pan6.text!)
        let panNo4 = (pan7.text! + pan8.text!)
        let panNo5 = (pan9.text! + pan10.text!)
        panNumber = panNo1 + panNo2 + panNo3 + panNo4 + panNo5
        if panNumber.isPanNumberValid() == false {
            let alert = UIAlertController(title: "", message: "Enter valid pan number", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        if password != confirmPassword{
            let alert = UIAlertController(title: "", message: "Password does not match", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    var addresss : [Address] = []
     var selectedService = ""
    var addressDict = [String:Any]()
    func register(){
 
        let selectedState = userDefaults.value(forKey: UserDefaultsContants.MTX_Load_STATE)! as! String
        if selectedState == UserStates.HUB_LOAD{
            selectedService = "load"
        }else if selectedState == UserStates.HUB_TRUCK{
            selectedService = "fleet"
        }else{
            return
        }
        var photo = ""
        var panPhoto = ""
        DispatchQueue.background(delay: 0.0, background: {
          
            print("This is run on the background queue")
            photo = (self.loadImageFromDiskWith(fileName: "profilePhoto")?.toBase64())!
            panPhoto = (self.loadImageFromDiskWith(fileName: "panPhoto")?.toBase64())!
        }) {
//            print("photo completed = \(photo)")
//             print("panPhoto completed = \(panPhoto)")
             print("Download completed")
            self.cit = self.userDefaults.value(forKey: RegisterConstants.city)! as! String
            self.add = self.userDefaults.value(forKey: RegisterConstants.address)! as! String
            self.coun = self.userDefaults.value(forKey: RegisterConstants.country)! as! String
            self.sta = self.userDefaults.value(forKey: RegisterConstants.state)! as! String
            self.pin = self.userDefaults.value(forKey: RegisterConstants.pincode)! as! String
            let address = Address.init(city: self.cit, address: self.add, country: self.coun, state: self.sta, pincode: self.pin)
            let jsonData = try! JSONEncoder().encode(address)
            let addressJsonString = String(data: jsonData, encoding: .utf8)!
            do {
                self.addressDict = try self.convertToDictionary(from: addressJsonString)
                print(self.addressDict)
            } catch {
                print(error)
            }

            let parameters : [String:Any] = [
                "masters" : [
                    "address": self.addressDict
                ],
                "user" : self.user,
                "password": self.userDefaults.value(forKey: RegisterConstants.passward)! as! String,
                "fleet" : [
                    "typeOfCompany": self.type,
                    "name": self.userDefaults.value(forKey: RegisterConstants.CompanyName)! as! String,
                    "companyCode": self.userDefaults.value(forKey: RegisterConstants.compCode)! as! String,
                    "jurisdiction": "pune",
                    "panNo": self.panNumber
                ],
                "profileImage": photo,
                "documentImage": panPhoto
            ]
            
            
          // print("parameters = \(parameters)")
            
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 35
            print("url = \(URLStatics.hubRegister)")
            
            Alamofire.request(URLStatics.hubRegister, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: [:])
                .responseJSON { response in
                    print("response.request=\(String(describing: response.request))")  // original URL request
                    print("response.request?.httpMethod = \(String(describing: response.request?.httpMethod))")
                    print("response.request?.allHTTPHeaderFields = \(String(describing: response.request?.allHTTPHeaderFields))")
                    let resultNSString = NSString(data: response.request!.httpBody!, encoding: String.Encoding.utf8.rawValue)!
                    print("response.request?.httpBody = \(resultNSString)")
                    switch (response.result) {
                    case .success:
                      
                        if let reply = response.result.value {
                            print("JSON: \(reply)")
                            let mainResponse = JSON(reply)
                            if mainResponse["success"].stringValue != "" {
                                print("mainResponse = \(mainResponse["success"])")
                                print("mainResponse = \(mainResponse["returnObj"])")
                                var returnDict = [String:Any]()
                    let returnObj = mainResponse["returnObj"].stringValue
                                do {
                                   returnDict = try self.convertToDictionary(from: returnObj)
                                    print("returnDict = \(returnDict)")
                                } catch {
                                    print(error)
                                }
                                print(returnDict["id"]!)
                                print(returnDict["otp"]!)
                                self.userDefaults.set(returnDict["id"]!, forKey: UserDefaultsContants.USER_ID)
                                self.userDefaults.set(returnDict["otp"]!, forKey: UserDefaultsContants.OTP)
                                self.userDefaults.synchronize()
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let viewController = storyboard.instantiateViewController(withIdentifier :"otpScreen") as! OTPScreenVC
                                self.transitionVc(vc: viewController, duration: 0.5, type: .fromRight)
                            }else{
                                
                                if mainResponse["warning"].stringValue != ""{
                                    let alert = UIAlertController(title: "", message: mainResponse["warning"].stringValue, preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }
                                
                            }
                            
                        }
                        
                        break
                    case .failure(let error):
                        print("error \(error.localizedDescription)")
                        break
                    }
            }
            
        }
       
   
    
    }
 
    func backFunction(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"step1VC")
        viewController.modalPresentationStyle = .fullScreen
        transitionVc(vc: viewController, duration: 0.5, type: .fromLeft)
        
    }
    
    @objc func handleBackButtonTap(_ sender: UITapGestureRecognizer) {
       self.backFunction()
    }
    @objc func backButtonTap(_ sender: UITapGestureRecognizer){
        self.backFunction()
}
    func loadImageFromDiskWith(fileName: String) -> UIImage? {
        
        let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
        
        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
        
        if let dirPath = paths.first {
            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image
            
        }
        
        return nil
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.nextTextFieldToFirstResponder(textField: textField)
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DispatchQueue {
    
    static func background(delay: Double = 0.0, background: (()->Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            background?()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    completion()
                })
            }
        }
    }
    
}
