//
//  ActivityIndicatorViewController.swift
//  Mytrux
//
//  Created by Mytrux on 04/09/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ActivityIndicatorViewController: UIViewController {

    @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
    var stopAnimating = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    activityIndicator.startAnimating()
      NotificationCenter.default.addObserver(self, selector: #selector(self.activityIndicator(notification:)), name: .activityIndicator, object: nil)
        // Do any additional setup after loading the view.
    }
    
     @objc func activityIndicator(notification: Notification) {
       self.dismiss(animated: true, completion: nil)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
