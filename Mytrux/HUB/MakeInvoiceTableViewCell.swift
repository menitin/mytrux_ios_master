//
//  MakeInvoiceTableViewCell.swift
//  Mytrux
//
//  Created by Mytrux on 25/09/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class MakeInvoiceTableViewCell: UITableViewCell {

    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var unit: UILabel!
    @IBOutlet weak var rate: UILabel!
    @IBOutlet weak var billingHeadView: UIView!
    @IBOutlet weak var checkmarkView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var billDescription: UILabel!
    @IBOutlet weak var taxCheckBoxImage: UIImageView!
    @IBOutlet weak var editImage: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
