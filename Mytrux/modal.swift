////
////  modal.swift
////  Mytrux
////
////  Created by Mukta Bhuyar Punjabi on 21/05/19.
////  Copyright © 2019 Mytrux. All rights reserved.
////
//
//import Foundation
////
////  Models.swift
////  Flooent
////
////  Created by Aeon's Mac on 27/06/18.
////  Copyright © 2018 Raj Singh. All rights reserved.
////
//
//import Foundation
//import SwiftyJSON
//import Alamofire
//
//class TranslatedMessage{
//
//    init(id : Int64, message : String) {
//        self.id = id
//        self.message = message
//    }
//
//    var id : Int64
//    var message : String
//}
//
//class UserProfileData{
//    var id : String
//    var about:String
//    var email:String
//    var phone:String
//    var websit:String
//
//    init(
//        id : String,
//        about:String,
//        email:String,
//        phone:String,
//        websit:String
//        ){
//        self.id = id
//        self.about = about
//        self.email = email
//        self.phone = phone
//        self.websit = websit
//    }
//}
//
//
//
//class availibilityStatusGuide{
//    var code : String
//    var status : String
//    init(code : String,
//         status : String) {
//        self.code = code
//        self.status = status
//    }
//}
//
//class UnreadMessages{
//    var id : String
//    var count : Int16
//
//    init(id : String, count : Int16) {
//        self.id = id
//        self.count = count
//    }
//
//}
//
//class UserLoginData{
//    var sessionString : String
//    var id : String
//    var level : String
//    var status : String
//    var firstName:String
//    var lastName:String
//    var language:String
//
//    init(
//        sessionString : String,
//        id : String,
//        level : String,
//        status : String,
//        firstName:String,
//        lastName:String,
//        language:String
//        ){
//        self.sessionString = sessionString
//        self.id = id
//        self.level = level
//        self.status = status
//        self.firstName = firstName
//        self.lastName = lastName
//        self.language = language
//    }
//}
//
//class myContacts {
//    static let ONLINE_STATUS_AVAILABLE = "online"
//    static let ONLINE_STATUS_OFFLINE = "offline"
//    static let ONLINE_STATUS_UNKOWN = "unknown"
//    var id:Int64
//    var friend_status:String
//    var display_name:String
//    var first_name:String
//    var last_name:String
//    var email:String
//    var profile_email:String
//    var lang:String
//    var website:String
//    var about:String
//    var phone:String
//    var status_text:String
//    var room_id:String
//    var profile_pic:String
//    var fullName:String
//    var online_status : String
//    var unread : Int
//    var user_status : String
//
//
//    init(id:Int64,
//         friend_status:String,
//         display_name:String,
//         first_name:String,
//         last_name:String,
//         email:String,
//         lang:String,
//         website:String,
//         about:String,
//         phone:String,
//         status_text:String,
//         room_id:String,
//         profile_pic:String,
//         user_status : String
//        ){
//        self.id = id
//        self.friend_status = friend_status
//        self.display_name = display_name
//        self.first_name = first_name
//        self.last_name = last_name
//        self.email = email
//        self.profile_email = ""
//        self.lang = lang
//        self.website = website
//        self.about = about
//        self.phone = phone
//        self.status_text = status_text
//        self.room_id = room_id
//        self.profile_pic = profile_pic
//        self.fullName = first_name+" "+last_name
//        self.online_status = ""
//        self.user_status = user_status
//        self.unread = 0
//    }
//
//    class func parseContact(contactJSON : JSON) -> myContacts?{
//        let contact = myContacts(id: contactJSON["ID"].int64Value, friend_status: contactJSON["friend_status"].stringValue, display_name: contactJSON["display_name"].stringValue, first_name: contactJSON["first_name"].stringValue, last_name: contactJSON["last_name"].stringValue, email: contactJSON["profile_email"].stringValue, lang: contactJSON["language"].stringValue, website: contactJSON["website"].stringValue, about: contactJSON["about_me"].stringValue, phone: contactJSON["mobile"].stringValue, status_text: contactJSON["status_text"].stringValue, room_id: "", profile_pic: URLStatics.baseURL + contactJSON["profile_pic"].stringValue, user_status: contactJSON["user_status"].stringValue)
//        return contact
//    }
//
//}
//
//class Message {
//    /*
//     from = 7;
//     id = 159;
//     message = ijyhuh;
//     "message_type" = 10;
//     old = 1;
//     self = 0;
//     sent = 1497508625438;
//     */
//    var contactId : Int64
//    var contactName : String
//    var chatMessageId : String
//    var timestamp : Int64
//    var messages : String
//    var date: String
//    var friend_status : String
//    //var transMsg : String
//
//    init(
//        contactId : Int64,
//        contactName : String,
//        chatMessageId : String,
//        timestamp : Int64,
//        messages : String,
//        friend_status : String
//        //  transMsg : String
//
//
//        ){
//        self.contactId = contactId
//        self.timestamp = timestamp
//        self.contactName = contactName
//        self.chatMessageId = chatMessageId
//        self.messages = messages
//        self.friend_status = friend_status
//        // self.transMsg = transMsg
//
//        let dateFormatter1 = DateFormatter()
//        dateFormatter1.dateFormat = "MMM dd YYYY"
//        let dateString1 = dateFormatter1.string(from: NSDate(timeIntervalSince1970: TimeInterval(timestamp)) as Date)
//        self.date = dateString1
//
//
//    }
//}
//
//
