//
//  FirstIntroVC.swift
//  Mytrux
//
//  Created by Mukta Bhuyar Punjabi on 16/05/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import AssistantKit


class FirstIntroVC: BaseViewController {

    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var nextButtonView: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
       self.adjustConstraints()
        buttonCardView(button: nextButtonView)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.nextBtnClicked(_:)))
        nextButtonView.addGestureRecognizer(tap)
        nextButtonView.isUserInteractionEnabled = true
        self.view.addSubview(nextButtonView)
        // Do any additional setup after loading the view.
    }
    
    @objc func nextBtnClicked(_ sender: UITapGestureRecognizer) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue:"first"), object: nil)
    }
    
    @IBAction func skipBtnClicked(_ sender: Any) {
         UserDefaults.standard.set(false, forKey: "isLaunched")
       
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"loadsAndTrucksVC")
        viewController.modalPresentationStyle = .fullScreen
        transitionVc(vc: viewController, duration: 0.5, type: .fromRight)
    }
    
   
    func adjustConstraints(){
        let version = Device.version
        switch version {
        case .phone5S:
            print("iPhone5s")
            topConstraint!.constant = 60
            
        case .phone8Plus:
            print("iPhone8Plus")
            topConstraint!.constant = 90
            
        case .phoneX:
            topConstraint!.constant = 120
            
        default:
            topConstraint!.constant = 80
            print("default")
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


    

