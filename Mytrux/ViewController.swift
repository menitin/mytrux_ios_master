//
//  ViewController.swift
//  Mytrux
//
//  Created by Mukta Bhuyar Punjabi on 14/05/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ViewController: UIViewController {
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var splashImage: UIImageView!
    @IBOutlet weak var activityIndicator: NVActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.alpha = 0.0
        UserDefaults.standard.set("a", forKey: UserDefaultsContants.MTX_STATE)
        activityIndicator?.startAnimating()
    
      logoImage.image = UIImage.init(named: "logo")
        self.view.backgroundColor = UIColor.white
        self.titleLabel.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
    
    }
    override func viewDidAppear(_ animated: Bool) {
        
        UIView.animate(withDuration: 3.0, animations: {
            self.view.backgroundColor = UIColor(hexString:"#0F2541")
            self.view.alpha = 1.0
            self.titleLabel.transform = CGAffineTransform.identity
        })
        
    
        DispatchQueue.main.asyncAfter(deadline:.now() + 7.0, execute: {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier :"SelectServiceVC")
            viewController.modalPresentationStyle = .fullScreen
            self.transitionVc(vc: viewController, duration: 0.5, type: .fromRight)
        })
    }
}

