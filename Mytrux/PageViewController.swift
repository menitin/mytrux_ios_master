//
//  PageViewController.swift
//  Mytrux
//
//  Created by Mukta Bhuyar Punjabi on 16/05/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController, UIPageViewControllerDataSource,UIPageViewControllerDelegate{
  
    
   var pageControl = UIPageControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
      NotificationCenter.default.addObserver(self, selector: #selector(self.nextScreen(_:)), name: NSNotification.Name(rawValue: "first"), object: nil)
          NotificationCenter.default.addObserver(self, selector: #selector(self.nextScreen(_:)), name: NSNotification.Name(rawValue: "second"), object: nil)
          NotificationCenter.default.addObserver(self, selector: #selector(self.nextScreen(_:)), name: NSNotification.Name(rawValue: "third"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.backBtn(_:)), name: NSNotification.Name(rawValue: "back"), object: nil)
        
        configurePageControl()
        if let firstViewController = introViewControllers.first {
            firstViewController.modalPresentationStyle = .fullScreen
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
        
    }
    
    @objc func nextScreen(_ notification: NSNotification) {
        var index = pageControl.currentPage
        if (index >= 0 || pageControl.currentPage >= 0) {
            index = index + 1
            setViewControllers([introViewControllers[index]], direction: .forward, animated: true, completion: nil)
            pageControl.currentPage = index
            
        }
      
    }
    @objc func backBtn(_ notification: NSNotification) {
        setViewControllers([introViewControllers[2]], direction: .forward, animated: true, completion: nil)
        pageControl.currentPage = 2
    }

    
    func configurePageControl() {
        pageControl = UIPageControl(frame: CGRect(x: 0,y: UIScreen.main.bounds.maxY - 50,width: UIScreen.main.bounds.width,height: 50))
        self.pageControl.numberOfPages = introViewControllers.count
        
        self.pageControl.currentPage = 0
        self.pageControl.tintColor = UIColor(hexString: "#C0C6CC")
        self.pageControl.pageIndicatorTintColor = UIColor(hexString: "#C0C6CC")
        self.pageControl.currentPageIndicatorTintColor = UIColor(hexString: "#559AFF")
        pageControl.layer.position.y = self.view.frame.height - 100;
        self.view.addSubview(pageControl)
    }
    func newVc(viewController: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: viewController)
    }
    
    lazy var introViewControllers: [UIViewController] = {
        return [self.newVc(viewController: "FirstVC"),
                self.newVc(viewController: "SecondVC"),
                self.newVc(viewController: "ThirdVC"),
                self.newVc(viewController: "FourthVC")]
    }()
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl.currentPage = introViewControllers.firstIndex(of: pageContentViewController)!
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = introViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
       
        guard previousIndex >= 0 else {
            return nil
           
        }
        
        guard introViewControllers.count > previousIndex else {
            return nil
        }
        
        return introViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = introViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = introViewControllers.count
        
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return introViewControllers[nextIndex]
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
