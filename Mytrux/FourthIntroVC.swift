//
//  FourthIntroVC.swift
//  Mytrux
//
//  Created by Mukta Bhuyar Punjabi on 16/05/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import AssistantKit

class FourthIntroVC: BaseViewController {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var letsGoButton: UIView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.adjustConstraints()
        buttonCardView(button: letsGoButton)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.letsGoButtonClick(_:)))
        letsGoButton.addGestureRecognizer(tap)
        letsGoButton.isUserInteractionEnabled = true
        self.view.addSubview(letsGoButton)
        // Do any additional setup after loading the view.
    }
    
    @objc func letsGoButtonClick(_ sender: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"loadsAndTrucksVC")
          viewController.modalPresentationStyle = .fullScreen
       transitionVc(vc: viewController, duration: 0.5, type: .fromRight)
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
         NotificationCenter.default.post(name: NSNotification.Name(rawValue:"back"), object: nil)
    }
    func adjustConstraints(){
        let version = Device.version
        switch version {
        case .phone5S:
            print("iPhone5s")
            topConstraint!.constant = 60
            
        case .phone8Plus:
            print("iPhone8Plus")
            topConstraint!.constant = 90
            
        case .phoneX:
            topConstraint!.constant = 120
            
        default:
            topConstraint!.constant = 80
            print("default")
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
