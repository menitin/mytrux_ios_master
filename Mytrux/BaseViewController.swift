//
//  BaseViewController.swift
//  Mytrux
//
//  Created by Mukta Bhuyar Punjabi on 20/05/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import AssistantKit
import NVActivityIndicatorView
extension Date {
    func getNextYear() -> Date? {
     return Calendar.current.date(byAdding: .year, value: 1, to: self)
     
    }
    
    func getPreviousYear() -> Date? {
        return Calendar.current.date(byAdding: .year, value: -1, to: self)
    }
    func getCurrentYear() -> Date? {
        return Calendar.current.date(byAdding: .year, value: 0, to: self)
    }
    func getCurrentMonth() -> Date? {
        return Calendar.current.date(byAdding: .month, value: 0, to: self)
    }
}
class BaseViewController: UIViewController {
    public let userDefaults = UserDefaults.standard
     open var activityIndicator: NVActivityIndicatorView?
    open var loginUserType = String()
    open var nextYear : Int?
    open var currentYear : Int?
    open var previousYear : Int?
    open var currentFinancialYear : String?
    open var previousFinancialYear : String?
    var month : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isKeyPresentInUserDefaults(key: UserDefaultsContants.LOGIN_USER_TYPE){
            loginUserType = UserDefaults.standard.value(forKey: UserDefaultsContants.LOGIN_USER_TYPE)! as! String
        }
       
        currentYear = Calendar.current.component(.year, from: Date())
        nextYear = currentYear! + 1
        previousYear = currentYear! - 1
        month = Calendar.current.component(.month, from: Date())
        previousFinancialYear = String(previousYear!) + "-" + String(currentYear!)
        if month! > 3{
            currentFinancialYear = String(currentYear!) + "-" + String(nextYear!)
        }else{
            currentFinancialYear = String(previousYear!) + "-" + String(currentYear!)
        }
        
    }
    
    func header() -> [String:String] {
        var headerss = [String:String]()
         if isKeyPresentInUserDefaults(key: LoginDataConstants.token){
            
            let token : String = userDefaults.value(forKey: LoginDataConstants.token) as! String
            print("token == \(String(describing: token))")
            let formattedToken = token + ":"
            print("formattedToken == \(String(describing: formattedToken))")
            let base64Token = formattedToken.toBase64()
            print(" base64Token == \(String(describing: base64Token))")
            let headers = [
                "Authorization": "Basic " + "\(base64Token!)",
                "Content-Type": "application/json"
            ]
            
            headerss = headers
        }
        return headerss
    }
    
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }

    open func topBarCard(topBar:UIView){
        topBar.layer.masksToBounds = false
        topBar.layer.shadowRadius = 4
        topBar.layer.shadowOpacity = 0.5
        topBar.layer.shadowColor = UIColor.gray.cgColor
        topBar.layer.shadowOffset = CGSize(width: 0 , height:2)
        
    }
    
    
    open func roundedCornerView(viewArray : [UIView]){
        for view in viewArray{
            view.layoutIfNeeded()
            view.layer.cornerRadius = view.frame.size.height / 2
            
        }
    }
    
    
   open func callNumber(phoneNumber:String) {
        if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
            
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                    application.openURL(phoneCallURL as URL)
                    
                }
            }
        }
    }
    
    
   open func convertToDictionary(from text: String) throws -> [String: Any] {
        guard let data = text.data(using: .utf8) else { return [:] }
        let anyResult: Any = try JSONSerialization.jsonObject(with: data, options: [])
        return anyResult as? [String: Any] ?? [:]
    }
    
    var greyView = UIView()
    var waitLabel = UILabel()
    
    open func activityIndicatorBegin() {
        greyView = UIView()
        greyView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        greyView.backgroundColor = UIColor.black
        greyView.alpha = 0.7
        self.view.addSubview(greyView)
        
        waitLabel = UILabel(frame: CGRect(x: self.view.center.x, y: self.view.center.y + 60 , width: 200, height: 20))
        waitLabel.textAlignment = .center
        waitLabel.center.x = self.view.center.x
        waitLabel.font = waitLabel.font.withSize(15)
        waitLabel.text = "Please Wait..."
        waitLabel.textColor = UIColor.white
        view.addSubview(waitLabel)
        activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
        activityIndicator!.center = self.view.center

        activityIndicator!.type = .ballTrianglePath
        view.addSubview(activityIndicator!)
        activityIndicator!.startAnimating()
       // disableUserInteraction()
   
    }
    
   open func activityIndicatorEnd() {
    self.activityIndicator?.stopAnimating()
       // enableUserInteraction()
    self.waitLabel.removeFromSuperview()
        self.greyView.removeFromSuperview()
    }
    
   open func epochTime(epochTimee:Double)->String{
        let timeResult:Double = epochTimee
        let date = NSDate(timeIntervalSince1970: timeResult)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a" //Set time style
        
        let timeZone = TimeZone.current.identifier as String
        dateFormatter.timeZone = TimeZone(identifier: timeZone)
        let localDate = dateFormatter.string(from: date as Date)
        return "\(localDate)"
    }
    
    open func epochTimeSec(epochTimee:Double)->String{
        
        let timeResult:Double = epochTimee
        let date = NSDate(timeIntervalSince1970: timeResult)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm:ss" //Set time style
        
        let timeZone = TimeZone.current.identifier as String
        dateFormatter.timeZone = TimeZone(identifier: timeZone)
        let localDate = dateFormatter.string(from: date as Date)
        return "\(localDate)"
    }
    
   open func epochDate(epochDate:Double)->String{
        
        let timeResult:Double = epochDate
        let date = NSDate(timeIntervalSince1970: timeResult)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"  //Set date style
        let timeZone = TimeZone.current.identifier as String
        dateFormatter.timeZone = TimeZone(identifier: timeZone)
        let localDate = dateFormatter.string(from: date as Date)
        return "\(localDate)"
        
    }
    open func epochDayDate(epochDate:Double)->String{
           
           let timeResult:Double = epochDate
           let date = NSDate(timeIntervalSince1970: timeResult)
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "EEEE dd/MM/yyyy"  //Set date style
           let timeZone = TimeZone.current.identifier as String
           dateFormatter.timeZone = TimeZone(identifier: timeZone)
           let localDate = dateFormatter.string(from: date as Date)
           return "\(localDate)"
           
       }
    
    
    open func circularView(viewArray : [UIView]){
        
        for view in viewArray{
            view.layoutIfNeeded()
            view.layer.cornerRadius = view.frame.size.height / 2
            view.layer.masksToBounds = true
            view.clipsToBounds = true
            view.layer.shadowColor = UIColor.gray.cgColor
            view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            view.layer.shadowRadius = 10
            view.layer.shadowOpacity = 0.4
        }
        
    }
    
    open func addDashedBorder(view : UIView) {
        //Create a CAShapeLayer
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.white.cgColor
        shapeLayer.lineWidth = 2
        // passing an array with the values [2,3] sets a dash pattern that alternates between a 2-user-space-unit-long painted segment and a 3-user-space-unit-long unpainted segment
        shapeLayer.lineDashPattern = [2,3]
        
        let path = CGMutablePath()
        path.move(to: CGPoint.zero)
        path.addLine(to: CGPoint(x:view.frame.width, y: 0))
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    
    open func cardViewArray(arrView:[UIView]){
        for view in arrView{
            view.layer.cornerRadius = 5
            view.layer.shadowColor = UIColor.gray.cgColor
            view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            view.layer.shadowRadius = 5
            view.layer.shadowOpacity = 0.4
            
        }
        
    }
    
    open func buttonCardView(button:UIView){
        button.layer.cornerRadius = button.frame.height / 2
        button.layer.shadowColor = UIColor.gray.cgColor
        button.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
        button.layer.shadowRadius = 5
        button.layer.shadowOpacity = 0.7
    }
    
    open func buttonCardArray(arrView:[UIView]){
        for button in arrView{
            button.layer.cornerRadius = button.frame.height / 2
            button.layer.shadowColor = UIColor.gray.cgColor
            button.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
            button.layer.shadowRadius = 5
            button.layer.shadowOpacity = 0.7
        }
    }
    
    open func picker(state:String){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let presentedViewController = storyBoard.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        presentedViewController.providesPresentationContextTransitionStyle = true
        presentedViewController.definesPresentationContext = true
        presentedViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
        presentedViewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        presentedViewController.pickerState = state
        self.present(presentedViewController, animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
