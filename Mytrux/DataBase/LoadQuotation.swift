
import Foundation

// MARK: - LoadQuotation
class LoadQuotation {
    let status: Bool?
    let data: [LoadQuotationDatum]?
    
    init(status: Bool?, data: [LoadQuotationDatum]?) {
        self.status = status
        self.data = data
    }
}

// MARK: - Datum
class LoadQuotationDatum {
    let id: Int?
    let availableLoadBid: AvailableBid?
    let fleetOwner: FleetOwner?
    let quotationAmount: Double?
    let currencyMaster: CurrencyMaster?
    let validDateTime, quotationEntryDate: Int?
    let quoteID: String?
    let status: String?
    let availableFleetBid: AvailableBid?
    let updateCount: Int?
    let loadBidQuotaionDetails: [AvailableLoadBidQuotaion]?
    let count, bidRank: Int?
    let loadOwner: LoadOwner?
    let lowestBidAmount, quotationUpdatedDate: Double?
    
    init(id: Int?, availableLoadBid: AvailableBid?, fleetOwner: FleetOwner?, quotationAmount: Double?, currencyMaster: CurrencyMaster?, validDateTime: Int?, quotationEntryDate: Int?, quoteID: String?, status: String?, availableFleetBid: AvailableBid?, updateCount: Int?, loadBidQuotaionDetails: [AvailableLoadBidQuotaion]?, count: Int?, bidRank: Int?, loadOwner: LoadOwner?, lowestBidAmount: Double?, quotationUpdatedDate: Double?) {
        self.id = id
        self.availableLoadBid = availableLoadBid
        self.fleetOwner = fleetOwner
        self.quotationAmount = quotationAmount
        self.currencyMaster = currencyMaster
        self.validDateTime = validDateTime
        self.quotationEntryDate = quotationEntryDate
        self.quoteID = quoteID
        self.status = status
        self.availableFleetBid = availableFleetBid
        self.updateCount = updateCount
        self.loadBidQuotaionDetails = loadBidQuotaionDetails
        self.count = count
        self.bidRank = bidRank
        self.loadOwner = loadOwner
        self.lowestBidAmount = lowestBidAmount
        self.quotationUpdatedDate = quotationUpdatedDate
    }
}



// MARK: - CapacityWeightUnit


class LoadBidQuotaionDetail {
    let id, quotationEntryDate: Int?
    let quotationAmount: Double?
    let currencyMaster: CurrencyMaster?
    
    init(id: Int?, quotationEntryDate: Int?, quotationAmount: Double?, currencyMaster: CurrencyMaster?) {
        self.id = id
        self.quotationEntryDate = quotationEntryDate
        self.quotationAmount = quotationAmount
        self.currencyMaster = currencyMaster
    }
}

