//
//  HubLogin.swift
//  Mytrux
//
//  Created by Aboli on 04/06/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import Foundation

struct Login : Codable {
    var loginUserType : String
    var fcmToken : String
    var mobileNo : String
    var password : String
    var appActivated : String
    var serviceType : String
 
    enum CodingKeys: String, CodingKey {
        case loginUserType = "loginUserType"
        case fcmToken = "fcmToken"
        case mobileNo = "mobileNo"
        case password = "password"
        case appActivated = "appActivated"
        case serviceType = "serviceType"
    }
}
