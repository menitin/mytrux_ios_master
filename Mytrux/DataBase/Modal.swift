
import Foundation

class Hub{
  
    var hub_load_owner:Bool
    var hub_truck_owner:Bool
    init(
        hub_load_owner:Bool,
        hub_truck_owner:Bool
        ){
         self.hub_load_owner = hub_load_owner
        self.hub_truck_owner = hub_truck_owner
    }
}


class State{
    var state:String
    
       init(
     state:String
        )
       {
        self.state = state
    }
}
class City{
    var state:String
    var city : [String]
    
    init(
        state:String,
        city:[String]
        )
    {
        self.state = state
        self.city = city
    }
}

class UserProfilee {
    var id : String
    var fullName : String
    var email : String
    var userSubType : String
    var mobileNumber : String
    var photo : Data
    var panAdharPhoto : Data
    
    init(
        id:String,
        fullName:String,
        email : String,
        userSubType : String,
        mobileNumber : String,
        photo : Data,
        panAdharPhoto : Data
        )
    {
    self.fullName = fullName
        self.id = id
        self.email = email
        self.userSubType = userSubType
        self.mobileNumber = mobileNumber
        self.photo = photo
        self.panAdharPhoto = panAdharPhoto
    }
}



class TypeOfCompany {
    var id : Int
    var companyType : String
    var companyCategories : String
    
    init(
        id:Int,
        companyType:String,
        companyCategories : String        
        )
    {
       
        self.id = id
        self.companyType = companyType
        self.companyCategories = companyCategories
    }
}
class TypeOfCompany1 {
    var id : Int?
    var companyType : String?
    var companyCategories : String?
    
    init(
        id:Int?,
        companyType:String?,
        companyCategories : String?
        )
    {
       
        self.id = id
        self.companyType = companyType
        self.companyCategories = companyCategories
    }
}
class AvailableLatLong {
    var availableLatitude : String
    var availableLongitude : String
    var loadType:String
    var availableLocation:String
    var destinationLocation:String?
    var bidCode:String
    var vehicleTypeName : String
    var noOfVehicle : String
    var cargoType : String
    var cargoWt : String
    var bidArray : [Int]?
    var isQuoted : Bool
    init(
        availableLatitude:String,
        availableLongitude : String,
        loadType : String,
        availableLocation:String,
        bidCode:String,
        destinationLocation:String?,
        vehicleTypeName:String,
        noOfVehicle : String,
        cargoWt : String,
        cargoType : String,
        bidArray : [Int]?,
         isQuoted : Bool
        )
    {
        self.availableLatitude = availableLatitude
        self.availableLongitude = availableLongitude
        self.loadType = loadType
        self.availableLocation = availableLocation
        self.bidCode = bidCode
        self.destinationLocation = destinationLocation
        self.vehicleTypeName = vehicleTypeName
        self.noOfVehicle = noOfVehicle
        self.cargoWt = cargoWt
        self.cargoType = cargoType
        self.bidArray = bidArray
        self.isQuoted = isQuoted
    }
}

class LoadType {
    var id : Int
    var code : String
    var loadType:String
    
    init(
        id:Int,
        code : String,
        loadType : String
        )
    {
        self.id = id
        self.code = code
        self.loadType = loadType
    }
}


class VehicleList {
    var name : String
    var vehicleOrder : String
    var smallDocId : String
    var documentId : String
    var id : String
    var vehicleType : String
    var container : String
    var type : String
    var sequence : String
  
    
    init(
        name:String,
        vehicleOrder : String,
        smallDocId:String,
        documentId : String,
        id:String,
        vehicleType : String,
        container: String,
        type : String,
        sequence:String
        )
    {
        self.name = name
        self.vehicleOrder = vehicleOrder
        self.smallDocId = smallDocId
        self.documentId = documentId
        self.id = id
        self.vehicleType = vehicleType
        self.container = container
        self.vehicleOrder = vehicleOrder
        self.type = type
        self.sequence = sequence
        
        
    }
}

class Payment {
    let appType: String?
    let transDate, createdDate: Int?
    let receiptNo, billPartyCompanyName, email: String?
    let transAmount, id, count: Int?
    let status, contact, paymentMode: String?
    let amount: Int?
    let plan : Plan?

    init(appType: String?, transDate: Int?, createdDate: Int?, receiptNo: String?, billPartyCompanyName: String?, email: String?, transAmount: Int?, id: Int?, count: Int?, status: String?, contact: String?, paymentMode: String?, amount: Int?,plan:Plan?) {
        self.appType = appType
        self.transDate = transDate
        self.createdDate = createdDate
        self.receiptNo = receiptNo
        self.billPartyCompanyName = billPartyCompanyName
        self.email = email
        self.transAmount = transAmount
        self.id = id
        self.count = count
        self.status = status
        self.contact = contact
        self.paymentMode = paymentMode
        self.amount = amount
        self.plan = plan
    }
}


class Plan {
    let id ,validityDays,maxUser,totalTransaction: Int?
    let amount, perTransactionFee,totalAmount,taxPercentage,taxableAmount: Double?
    let status :  Bool?
    let companyType, appType: String?
   

    init(id: Int?, amount: Double?, validityDays: Int?, status: Bool?, perTransactionFee: Double?, companyType: String?, maxUser: Int?, appType: String?, totalTransaction: Int?, totalAmount: Double?, taxPercentage: Double?, taxableAmount: Double?) {
        self.appType = appType
        self.id = id
        self.validityDays = validityDays
        self.maxUser = maxUser
        self.totalTransaction = totalTransaction
        self.amount = amount
        self.perTransactionFee = perTransactionFee
        self.totalAmount = totalAmount
        self.taxPercentage = taxPercentage
        self.taxableAmount = taxableAmount
        self.status = status
        self.companyType = companyType
        
    }
}

