//
//  AssignBookingList.swift
//  Mytrux
//
//  Created by Mytrux on 06/09/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//


import Foundation

// MARK: - Assign
class Assign {
    let status: Bool
    let data: [AssignDatum]
    
    init(status: Bool, data: [AssignDatum]) {
        self.status = status
        self.data = data
    }
}


// MARK: - Datum

class AssignDatum {
    let id: Int?
    let jobStatus: JobStatusClass?
    let loadType: LoadTypeClass?
    let cargoType: CargoTypeClass?
    let pickupDateTime, deliveryDateTime: Int?
    let bookingNo, pickUpLocation, destination: String?
    let createDate: Int?
    let vehicleContainerDetails: VehicleContainerDetails1?
    let bookingCust, shiper, consigneer, deliverCust: Bool?
    let salesBillFinal, purchaseBillFinal: Bool?
    let count: Int?
    let lrNo: Int?
    let fleetBidCode, loadBidCode: String?
    let inviteAmount: Double?
    let fleetBidID: Int?
    let isinvite: Bool?
    let registrationNo: String?
    let telephone: String?
    let packgs, quoteAmount: Int?
   
    
    init(id: Int?, jobStatus: JobStatusClass?, loadType: LoadTypeClass?, cargoType: CargoTypeClass?, pickupDateTime: Int?, deliveryDateTime: Int?, bookingNo: String?, pickUpLocation: String?, destination: String?, createDate: Int?, vehicleContainerDetails: VehicleContainerDetails1?, bookingCust: Bool?, shiper: Bool?, consigneer: Bool?, deliverCust: Bool?, salesBillFinal: Bool?, purchaseBillFinal: Bool?, count: Int?, lrNo: Int?, fleetBidCode: String?, loadBidCode: String?, inviteAmount: Double?, fleetBidID: Int?, isinvite: Bool?, registrationNo: String?, telephone: String?, packgs: Int?, quoteAmount: Int?) {
        self.id = id
        self.jobStatus = jobStatus
        self.loadType = loadType
        self.cargoType = cargoType
        self.pickupDateTime = pickupDateTime
        self.deliveryDateTime = deliveryDateTime
        self.bookingNo = bookingNo
        self.pickUpLocation = pickUpLocation
        self.destination = destination
        self.createDate = createDate
        self.vehicleContainerDetails = vehicleContainerDetails
        self.bookingCust = bookingCust
        self.shiper = shiper
        self.consigneer = consigneer
        self.deliverCust = deliverCust
        self.salesBillFinal = salesBillFinal
        self.purchaseBillFinal = purchaseBillFinal
        self.count = count
        self.lrNo = lrNo
        self.fleetBidCode = fleetBidCode
        self.loadBidCode = loadBidCode
        self.inviteAmount = inviteAmount
        self.fleetBidID = fleetBidID
        self.isinvite = isinvite
        self.registrationNo = registrationNo
        self.telephone = telephone
        self.packgs = packgs
        self.quoteAmount = quoteAmount
      
    }
}

// MARK: - CargoTypeClass
class CargoTypeClass {
    let id: Int
    let cargoType: String
    
    init(id: Int, cargoType: String) {
        self.id = id
        self.cargoType = cargoType
    }
}




// MARK: - FirstPicID
class FirstPicID {
    let id: Int?
    let mimeType: String?
    let type: String?
    
    init(id: Int?, mimeType: String?, type: String?) {
        self.id = id
        self.mimeType = mimeType
        self.type = type
    }
}





// MARK: - JobStatusClass
class JobStatusClass {
    let id: Int
    let jobStatus: String
    
    init(id: Int, jobStatus: String) {
        self.id = id
        self.jobStatus = jobStatus
    }
}


// MARK: - LoadTypeClass
class LoadTypeClass {
    let id: Int
    let loadType: String
    let code: String
    
    init(id: Int, loadType: String, code: String) {
        self.id = id
        self.loadType = loadType
        self.code = code
    }
}




// MARK: - VehicleContainerDetails
class VehicleContainerDetails {
    let id: Int
    let vehicleType: AssignVehicleType
    let noOfVehicles, grossWeight: Int
    let grossWeightUnit: WeightUnit
    let chargeableWeight: Int
    let chargeableWeightUnit: WeightUnit
    let netWeight: Int
    let netWeightUnit: WeightUnit
    
    init(id: Int, vehicleType: AssignVehicleType, noOfVehicles: Int, grossWeight: Int, grossWeightUnit: WeightUnit, chargeableWeight: Int, chargeableWeightUnit: WeightUnit, netWeight: Int, netWeightUnit: WeightUnit) {
        self.id = id
        self.vehicleType = vehicleType
        self.noOfVehicles = noOfVehicles
        self.grossWeight = grossWeight
        self.grossWeightUnit = grossWeightUnit
        self.chargeableWeight = chargeableWeight
        self.chargeableWeightUnit = chargeableWeightUnit
        self.netWeight = netWeight
        self.netWeightUnit = netWeightUnit
    }
}
class VehicleContainerDetails1 {
    let id: Int
    let vehicleType: AssignVehicleType1
    let noOfVehicles, grossWeight: Int
    let grossWeightUnit: WeightUnit
    let chargeableWeight: Int
    let chargeableWeightUnit: WeightUnit
    let netWeight: Int
    let netWeightUnit: WeightUnit
    
    init(id: Int, vehicleType: AssignVehicleType1, noOfVehicles: Int, grossWeight: Int, grossWeightUnit: WeightUnit, chargeableWeight: Int, chargeableWeightUnit: WeightUnit, netWeight: Int, netWeightUnit: WeightUnit) {
        self.id = id
        self.vehicleType = vehicleType
        self.noOfVehicles = noOfVehicles
        self.grossWeight = grossWeight
        self.grossWeightUnit = grossWeightUnit
        self.chargeableWeight = chargeableWeight
        self.chargeableWeightUnit = chargeableWeightUnit
        self.netWeight = netWeight
        self.netWeightUnit = netWeightUnit
    }
}

// MARK: - WeightUnit
class WeightUnit {
    let id: Int
    let name: String
    let type: String
    
    init(id: Int, name: String, type: String) {
        self.id = id
        self.name = name
        self.type = type
    }
}


// MARK: - VehicleType
class AssignVehicleType {
    let id: Int
    let name: String
    let sequence: Double
    let container: Bool
    let type: String
    let document, documentSmall: FirstPicID
    let vehicleOrder, documentID, smallDocID: Int
    let weightCapacity: Int?
    
    init(id: Int, name: String, sequence: Double, container: Bool, type: String, document: FirstPicID, documentSmall: FirstPicID, vehicleOrder: Int, documentID: Int, smallDocID: Int, weightCapacity: Int?) {
        self.id = id
        self.name = name
        self.sequence = sequence
        self.container = container
        self.type = type
        self.document = document
        self.documentSmall = documentSmall
        self.vehicleOrder = vehicleOrder
        self.documentID = documentID
        self.smallDocID = smallDocID
        self.weightCapacity = weightCapacity
    }
}

class AssignVehicleType1 {
    let id: Int?
    let name: String?
    let sequence: Double?
    let container: Bool?
    let type: String?
    let document, documentSmall: FirstPicID?
    let vehicleOrder, documentID, smallDocID: Int?
    let weightCapacity: Int?
    
    init(id: Int?, name: String?, sequence: Double?, container: Bool?, type: String?, document: FirstPicID?, documentSmall: FirstPicID?, vehicleOrder: Int?, documentID: Int?, smallDocID: Int?, weightCapacity: Int?) {
        self.id = id
        self.name = name
        self.sequence = sequence
        self.container = container
        self.type = type
        self.document = document
        self.documentSmall = documentSmall
        self.vehicleOrder = vehicleOrder
        self.documentID = documentID
        self.smallDocID = smallDocID
        self.weightCapacity = weightCapacity
    }
}
