//
//  Transactions.swift
//  Mytrux
//
//  Created by Mytrux on 24/10/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import Foundation

// MARK: - Transactions

// MARK: - Datum
class Transactions {
    let fleetBidCode: String?
    let transactionDate: Int?
    let transType: String?
    let deductedAmount, remainsAmount: Int?
    let transReason: String?
    let fleet: Fleet?
    let fleetBidID, count: Int?
    let amountWithTax: Double?
    let appliedTaxDetails: String?
    let id: Int?
    let currency: String?
    let paymentMode: String?
    let exchangeBooking: BookingElement?

    init(fleetBidCode: String?, transactionDate: Int?, transType: String?, deductedAmount: Int?, remainsAmount: Int?, transReason: String?, fleet: Fleet?, fleetBidID: Int?, count: Int?, amountWithTax: Double?, appliedTaxDetails: String?, id: Int?, currency: String?, paymentMode: String?, exchangeBooking: BookingElement?) {
        self.fleetBidCode = fleetBidCode
        self.transactionDate = transactionDate
        self.transType = transType
        self.deductedAmount = deductedAmount
        self.remainsAmount = remainsAmount
        self.transReason = transReason
        self.fleet = fleet
        self.fleetBidID = fleetBidID
        self.count = count
        self.amountWithTax = amountWithTax
        self.appliedTaxDetails = appliedTaxDetails
        self.id = id
        self.currency = currency
        self.paymentMode = paymentMode
        self.exchangeBooking = exchangeBooking
    }
}



class PaymentPlan {
    let id, amount, validityDays: Int?
    let status: Bool?
    let perTransactionFee: Int?
    let companyType: String?
    let maxUser, totalTransaction, totalAmount, taxPercentage: Int?
    let taxableAmount, count: Int?

    init(id: Int?, amount: Int?, validityDays: Int?, status: Bool?, perTransactionFee: Int?, companyType: String?, maxUser: Int?, totalTransaction: Int?, totalAmount: Int?, taxPercentage: Int?, taxableAmount: Int?, count: Int?) {
        self.id = id
        self.amount = amount
        self.validityDays = validityDays
        self.status = status
        self.perTransactionFee = perTransactionFee
        self.companyType = companyType
        self.maxUser = maxUser
        self.totalTransaction = totalTransaction
        self.totalAmount = totalAmount
        self.taxPercentage = taxPercentage
        self.taxableAmount = taxableAmount
        self.count = count
    }
}

// MARK: - CompanyDetail
class CompanyDetail {
    let id: Int?
    let name, registrationNo, panNo, fax: String?
    let telephone: String?
    let typeOfCompany: TypeOfCompany1?
    let fleetAdditionalInformation: FleetAdditionalInformation?
    let address: Address1?
    let userCount, activeUserCount: Int?
    let companyCode: String?
    let logo: Logo?
    let gstTax: Int?
    let gstTaxRegNo, jurisdiction, timeZone, vatNo: String?
    let iecNo, cinNo, cstNo: String?
    let registrationDate: Int?
    let expiredType: String?
    let subscriptionStartDate, subscriptionEndDate, perTransactionFee, smsCharge: Int?
    let currentBalance, bookingCount, purchasePaymentCount, purchaseReceiptCount: Int?
    let routeCount: Int?
    let smsNotification: String?
    let maxUser: Int?
    let maxUserType: String?
    let users: [User]?
    let taxs: [Tax]?
    let contactPersons: [ContactPerson]?
    let status, fleetDevicePassword: String?
    let contractMasterCount, goCount, proCount, profileMailCount: Int?
    let ewbFlagStr: String?
    let walletAmount, hubMaxUser, allowTransaction, remainsTr: Int?
    let hubPlanID, hubTransactionCost, thirdPartyAuthDateTime, exPurchasePayCount: Int?
    let exPurchaseReceiptCount, exWalletValidFrom, exWalletValidTill, exWalletValidDays: Int?
    let exPlanType, exPackType, eLrTerms, salesInvoiceTerms: String?
    let userCounts: Int?

    init(id: Int?, name: String?, registrationNo: String?, panNo: String?, fax: String?, telephone: String?, typeOfCompany: TypeOfCompany1?, fleetAdditionalInformation: FleetAdditionalInformation?, address: Address1?, userCount: Int?, activeUserCount: Int?, companyCode: String?, logo: Logo?, gstTax: Int?, gstTaxRegNo: String?, jurisdiction: String?, timeZone: String?, vatNo: String?, iecNo: String?, cinNo: String?, cstNo: String?, registrationDate: Int?, expiredType: String?, subscriptionStartDate: Int?, subscriptionEndDate: Int?, perTransactionFee: Int?, smsCharge: Int?, currentBalance: Int?, bookingCount: Int?, purchasePaymentCount: Int?, purchaseReceiptCount: Int?, routeCount: Int?, smsNotification: String?, maxUser: Int?, maxUserType: String?, users: [User]?, taxs: [Tax]?, contactPersons: [ContactPerson]?, status: String?, fleetDevicePassword: String?, contractMasterCount: Int?, goCount: Int?, proCount: Int?, profileMailCount: Int?, ewbFlagStr: String?, walletAmount: Int?, hubMaxUser: Int?, allowTransaction: Int?, remainsTr: Int?, hubPlanID: Int?, hubTransactionCost: Int?, thirdPartyAuthDateTime: Int?, exPurchasePayCount: Int?, exPurchaseReceiptCount: Int?, exWalletValidFrom: Int?, exWalletValidTill: Int?, exWalletValidDays: Int?, exPlanType: String?, exPackType: String?, eLrTerms: String?, salesInvoiceTerms: String?, userCounts: Int?) {
        self.id = id
        self.name = name
        self.registrationNo = registrationNo
        self.panNo = panNo
        self.fax = fax
        self.telephone = telephone
        self.typeOfCompany = typeOfCompany
        self.fleetAdditionalInformation = fleetAdditionalInformation
        self.address = address
        self.userCount = userCount
        self.activeUserCount = activeUserCount
        self.companyCode = companyCode
        self.logo = logo
        self.gstTax = gstTax
        self.gstTaxRegNo = gstTaxRegNo
        self.jurisdiction = jurisdiction
        self.timeZone = timeZone
        self.vatNo = vatNo
        self.iecNo = iecNo
        self.cinNo = cinNo
        self.cstNo = cstNo
        self.registrationDate = registrationDate
        self.expiredType = expiredType
        self.subscriptionStartDate = subscriptionStartDate
        self.subscriptionEndDate = subscriptionEndDate
        self.perTransactionFee = perTransactionFee
        self.smsCharge = smsCharge
        self.currentBalance = currentBalance
        self.bookingCount = bookingCount
        self.purchasePaymentCount = purchasePaymentCount
        self.purchaseReceiptCount = purchaseReceiptCount
        self.routeCount = routeCount
        self.smsNotification = smsNotification
        self.maxUser = maxUser
        self.maxUserType = maxUserType
        self.users = users
        self.taxs = taxs
        self.contactPersons = contactPersons
        self.status = status
        self.fleetDevicePassword = fleetDevicePassword
        self.contractMasterCount = contractMasterCount
        self.goCount = goCount
        self.proCount = proCount
        self.profileMailCount = profileMailCount
        self.ewbFlagStr = ewbFlagStr
        self.walletAmount = walletAmount
        self.hubMaxUser = hubMaxUser
        self.allowTransaction = allowTransaction
        self.remainsTr = remainsTr
        self.hubPlanID = hubPlanID
        self.hubTransactionCost = hubTransactionCost
        self.thirdPartyAuthDateTime = thirdPartyAuthDateTime
        self.exPurchasePayCount = exPurchasePayCount
        self.exPurchaseReceiptCount = exPurchaseReceiptCount
        self.exWalletValidFrom = exWalletValidFrom
        self.exWalletValidTill = exWalletValidTill
        self.exWalletValidDays = exWalletValidDays
        self.exPlanType = exPlanType
        self.exPackType = exPackType
        self.eLrTerms = eLrTerms
        self.salesInvoiceTerms = salesInvoiceTerms
        self.userCounts = userCounts
    }
}



// MARK: - FleetAdditionalInformation
class FleetAdditionalInformation {
    let id, noOfOwnVehicles, noOfAttachVehicles, noOfDrivers: Int?
    let noOfEmployees, noOfOfficesLocations, noOfAnnualTurnover, noOfTransactionsPM: Int?
    let noOfCustomers: Int?
    let topFiveCustomerName, futureTargetLocations, companyBrief, majorVehicleBusiness: String?
    let majorFocusedRoutes, focusedCommodity: String?

    init(id: Int?, noOfOwnVehicles: Int?, noOfAttachVehicles: Int?, noOfDrivers: Int?, noOfEmployees: Int?, noOfOfficesLocations: Int?, noOfAnnualTurnover: Int?, noOfTransactionsPM: Int?, noOfCustomers: Int?, topFiveCustomerName: String?, futureTargetLocations: String?, companyBrief: String?, majorVehicleBusiness: String?, majorFocusedRoutes: String?, focusedCommodity: String?) {
        self.id = id
        self.noOfOwnVehicles = noOfOwnVehicles
        self.noOfAttachVehicles = noOfAttachVehicles
        self.noOfDrivers = noOfDrivers
        self.noOfEmployees = noOfEmployees
        self.noOfOfficesLocations = noOfOfficesLocations
        self.noOfAnnualTurnover = noOfAnnualTurnover
        self.noOfTransactionsPM = noOfTransactionsPM
        self.noOfCustomers = noOfCustomers
        self.topFiveCustomerName = topFiveCustomerName
        self.futureTargetLocations = futureTargetLocations
        self.companyBrief = companyBrief
        self.majorVehicleBusiness = majorVehicleBusiness
        self.majorFocusedRoutes = majorFocusedRoutes
        self.focusedCommodity = focusedCommodity
    }
}


