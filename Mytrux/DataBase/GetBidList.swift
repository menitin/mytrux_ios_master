//
//  GetBidList.swift
//  Mytrux
//
//  Created by Aboli on 28/06/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import Foundation


struct GetBidList : Codable {
    var pageSize : String
    var pageNo : String
    var recordType : String
    var search : String
    var fromCity : String
    var fromState : String
    var toState : String
    var toCity : String
    var vehicleType : String
    var loadType : String
    var noOfVehicle : String
    var fromDate : String
    var toDate : String
    var currentLatitude : String
    var currentLongitude : String
    var km : String
    var financialYear : String
    var cargoType : String
    
    
    enum CodingKeys: String, CodingKey {
        case pageSize = "pageSize"
        case pageNo = "pageNo"
        case recordType = "recordType"
        case search = "search"
        case fromCity = "fromCity"
        case fromState = "fromState"
        case toState = "toState"
        case toCity = "toCity"
        case vehicleType = "vehicleType"
        case loadType = "loadType"
        case noOfVehicle = "noOfVehicle"
        case fromDate = "fromDate"
        case toDate = "toDate"
        case currentLatitude = "currentLatitude"
        case currentLongitude = "currentLongitude"
        case km = "km"
        case financialYear = "financialYear"
        case cargoType = "cargoType"
    }
}

struct PostLoadQuoation : Codable {
    var id : String
    var quotationAmount: String
    var currencyMaster: CurrencyMasterr
    var validDateTime: String
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case quotationAmount = "quotationAmount"
        case currencyMaster = "currencyMaster"
        case validDateTime = "validDateTime"
    }
}

struct CurrencyMasterr : Codable {
    var id : String
    enum CodingKeys: String, CodingKey {
        case id = "id"
    }
}
