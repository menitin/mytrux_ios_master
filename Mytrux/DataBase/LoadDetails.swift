//
//  LoadDetails.swift
//  Mytrux
//
//  Created by Mytrux on 01/08/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import Foundation

class LoadDetails{
    
    static let id = "id"
    static let to = "to"
    static let from = "from"
    static let vehicleType = "vehicleType"
    static let cargoType = "cargoType"
    static let cargoWeight = "cargoWeight"
    static let loadType = "loadType"
    static let noOfVehicles = "noOfVehicles"
    static let availablefromdate = "availablefromdate"
    static let availablefromtime = "availablefromtime"
    static let availabletilldate = "availabletilldate"
    static let availabletilltime = "availabletilltime"
    static let kgName = "kgName"
    static let packgs = "packgs"
    static let lowestBid = "lowestBid"
    static let closedBidTime = "closedBidTime"
    
 
    
}



