//
//  CoreDataManager.swift
//  Mytrux
//
//  Created by Mukta Bhuyar Punjabi on 20/05/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit
import CoreData

@available(iOS 10.0, *)
class CoreDataManager:NSObject{
    
    private class func getMainContext() -> NSManagedObjectContext {
       let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    private class func getBackgroundContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.newBackgroundContext()
    }
    
    class func setUserLoggedIn(isLoggedIn : Bool){
        
        let context = getMainContext()
        context.perform {
            
            let entity = NSEntityDescription.entity(forEntityName: "UserStatePrefs", in: context)
            let managedObj = NSManagedObject(entity: entity!, insertInto: context)
            
            managedObj.setValue(isLoggedIn, forKey: "isLoggedIn")
            
            do {
                try context.save()
                print("setUserLoggedIn saved!")
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    class func setUserState(state: State){
        
                let context = getMainContext()
        
                let entity = NSEntityDescription.entity(forEntityName: "UserState", in: context)
        
                let managedObj = NSManagedObject(entity: entity!, insertInto: context)
                 managedObj.setValue(state.state, forKey: "state")
          //  CoreDataManager.deleteUserProfileDataByID(userID : userProfileData.id)
                do {
                        try context.save()
                        print("userProfileData saved!")
                } catch {
                    print(error.localizedDescription)
                }
        
        
    }
    
    class func updateState(state:String) {
     
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "UserState")
     
        do
        {
            let test = try managedContext.fetch(fetchRequest)
            
            let objectUpdate = test[0] as! NSManagedObject
            objectUpdate.setValue(state, forKey: "state")
         
            do{
                try managedContext.save()
            }
            catch
            {
                print(error)
            }
        }
        catch
        {
            print(error)
        }
        
    }
    
    class func getUserState() -> String{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "UserState")
        //request.predicate = NSPredicate(format: "age = %@", "12")
        request.returnsObjectsAsFaults = false
        do {
            let result = try getMainContext().fetch(request)
            for data in result as! [NSManagedObject] {
                print(data.value(forKey: "state") as! String)
                var value = data.value(forKey: "state") as! String
                return value
            }
            
        } catch {
           print("Failed")
        
        }
        return ""
    }
    
    class func deleteData(){
        
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UserState")
 
        
        do
        {
            let test = try managedContext.fetch(fetchRequest)
            
            let objectToDelete = test[0] as! NSManagedObject
            managedContext.delete(objectToDelete)
            
            do{
                try managedContext.save()
            }
            catch
            {
                print(error)
            }
            
        }
        catch
        {
            print(error)
        }
    }
    
    class func setUserProfileData(userProfileData : UserProfilee){
        
                let context = getMainContext()
        
                let entity = NSEntityDescription.entity(forEntityName: "UserProfile", in: context)
        
                let managedObj = NSManagedObject(entity: entity!, insertInto: context)
        
        
                managedObj.setValue(userProfileData.id, forKey: "id")
                managedObj.setValue(userProfileData.email, forKey: "email")
                managedObj.setValue(userProfileData.fullName, forKey: "fullName")
                managedObj.setValue(userProfileData.mobileNumber, forKey: "mobileNumber")
                managedObj.setValue(userProfileData.userSubType, forKey: "userSubType")
                managedObj.setValue(userProfileData.panAdharPhoto, forKey: "panAdharPhoto")
                managedObj.setValue(userProfileData.photo, forKey: "photo")
        
        
                print("OBJECT setUserProfileData")
                print("setUserProfileData id = \(userProfileData.id)")
                print("setUserProfileData fullName = \(userProfileData.fullName)")
                print("setUserProfileData email = \(userProfileData.email)")
                print("setUserProfileData mobile = \(userProfileData.mobileNumber)")
                print("setUserProfileData userSubType = \(userProfileData.userSubType)")
        
                //  CoreDataManager.deleteUserProfileDataByID(userID : userProfileData.id)
                do {
                    try context.save()
                    print("userProfileData saved!")
        
                } catch {
                    print(error.localizedDescription)
                }
            }
    
    
    class func getUserProfileData(userID : String) -> UserProfilee?{
        
                let fetchRequest:NSFetchRequest<UserProfile> = UserProfile.fetchRequest()
        
                let predicate = NSPredicate(format: "id == %@", userID)
                fetchRequest.predicate = predicate
        
                do {
                    let fetchResult = try getMainContext().fetch(fetchRequest)
        
                    print("fetchResult.count = \(fetchResult.count)")
                    if fetchResult.count == 0{
                        return nil
                    }
                    let result = fetchResult.first
        
                    var fullName = ""
                    var email = ""
                    var mobile = ""
                    var userSubType = ""
                    var id = ""
                    var photo = UIImage()
                    var panAdharPhoto = UIImage()
                    
        
                    if result?.id != nil{
                        id = (result?.id)!
                    }
                    if result?.fullName != nil{
                        fullName = (result?.fullName)!
                    }
                    if result?.email != nil{
                        email = (result?.email)!
                    }
                    if result?.mobileNumber != nil{
                        mobile = (result?.mobileNumber)!
                    }
                    if result?.userSubType != nil{
                        userSubType = (result?.userSubType)!
                    }
                    if let photoinData = result?.value(forKey: "photo") as? UIImage{
                        photo = photoinData
                    }
                    if let panAdharPhotoinData = result?.value(forKey: "panAdharPhoto") as? UIImage{
                        panAdharPhoto = panAdharPhotoinData
                    }
                    let photoData = photo.pngData() as NSData?
                    let panAdharData = panAdharPhoto.pngData() as NSData?
                    let userProfileData = UserProfilee(id: id, fullName: fullName, email: email, userSubType: userSubType, mobileNumber: mobile, photo: photoData as! Data, panAdharPhoto: panAdharData as! Data)
        
                    print("OBJECT getUserProfileData")
                    print("getUserProfileData id = \(userProfileData.id)")
                    print("getUserProfileData aboutme = \(userProfileData.fullName)")
                    print("getUserProfileData contactemail = \(userProfileData.email)")
                    print("getUserProfileData mobile = \(userProfileData.mobileNumber)")
                    print("getUserProfileData website = \(userProfileData.userSubType)")
        
                    return userProfileData
                }catch {
                    print(error.localizedDescription)
                }
        
                return nil
            }
    
    
    
    
    
    
    
//    class func setHubState(hubState: Hub){
//
//        let context = getMainContext()
//        let entity = NSEntityDescription.entity(forEntityName: "HUB", in: context)
//
//        let managedObj = NSManagedObject(entity: entity!, insertInto: context)
//
//        managedObj.setValue(hubState.hub_load_owner, forKey: "hub_load_owner")
//        managedObj.setValue(hubState.hub_truck_owner, forKey: "hub_truck_owner")
//
//        do {
//            try context.save()
//            print("userHubState saved!")
//
//        } catch {
//            print(error.localizedDescription)
//        }
//      }
//
//    class func getHubState() -> String{
//        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "UserState")
//        //request.predicate = NSPredicate(format: "age = %@", "12")
//        request.returnsObjectsAsFaults = false
//        do {
//            let result = try getMainContext().fetch(request)
//            for data in result as! [NSManagedObject] {
//                print(data.value(forKey: "state") as! String)
//                var value = data.value(forKey: "state") as! String
//                return value
//            }
//
//        } catch {
//            print("Failed")
//
//        }
//        return ""
//    }
//
    
//    class func isUserLoggedIn() -> Bool{
//
//        let fetchRequest:NSFetchRequest<UserStatePrefs> = UserStatePrefs.fetchRequest()
//
//        do {
//            let fetchResult = try getMainContext().fetch(fetchRequest)
//            if fetchResult.count == 0{
//                return false
//            }else{
//                let item = fetchResult.first
//                return item!.isLoggedIn
//            }
//        }catch {
//            print(error.localizedDescription)
//        }
//        return false
//    }
    
    
}

