
import Foundation

// MARK: - Load
class FleetLoad {
    let status: Bool
    let data: [FleetDatum]
    
    init(status: Bool, data: [FleetDatum]) {
        self.status = status
        self.data = data
    }
}

// MARK: - Datum
class FleetDatum {
    let id: Int
    let bidCode: String
    let creationDate, closedBidTime: Int
    let availableLocation: String
    let destinationLocation: String?
    let availableLatitude, availableLongitude: Double?
    let destinationLatitude, destinationLongitude: Double?
    let count: Int?
    let vehicleType: FleetVehicleType
    let noOfVehicles, availableDateTime: Int
    let cargoType: FleetCargoType
    let capacityWeight: Int
    let capacityWeightUnit: FleetCapacityWeightUnit
    let comments:String?
    //let fleetOwner: FleetOwner
    let sequence: Int?
    let loadType: FleetLoadType
    let expectedFreight: NSNumber?
    let currencyMaster: FleetCurrencyMaster
    let isInvited: Bool
    let bidInviteCount: Int
    let vehicleTypeMaster: String?
    let loadBidCode: String?
    let registrationNo : String?
    let postType : String?
    init(id: Int, bidCode: String, creationDate: Int, closedBidTime: Int, availableLocation: String, destinationLocation: String?, availableLatitude: Double?, availableLongitude: Double?, destinationLatitude: Double?, destinationLongitude: Double?, count: Int?, vehicleType: FleetVehicleType, noOfVehicles: Int, availableDateTime: Int, cargoType: FleetCargoType, capacityWeight: Int, capacityWeightUnit: FleetCapacityWeightUnit, loadType: FleetLoadType, expectedFreight: NSNumber?, currencyMaster: FleetCurrencyMaster, isInvited: Bool, bidInviteCount: Int, vehicleTypeMaster: String?, loadBidCode: String?,registrationNo:String?,comments:String?,sequence:Int?,postType:String?) {
        self.id = id
        self.bidCode = bidCode
        self.creationDate = creationDate
        self.closedBidTime = closedBidTime
        self.availableLocation = availableLocation
        self.destinationLocation = destinationLocation
        self.availableLatitude = availableLatitude
        self.availableLongitude = availableLongitude
        self.destinationLatitude = destinationLatitude
        self.destinationLongitude = destinationLongitude
        self.count = count
        self.vehicleType = vehicleType
        self.noOfVehicles = noOfVehicles
        self.availableDateTime = availableDateTime
        self.cargoType = cargoType
        self.capacityWeight = capacityWeight
        self.capacityWeightUnit = capacityWeightUnit
    //    self.fleetOwner = fleetOwner
        self.loadType = loadType
        self.expectedFreight = expectedFreight
        self.currencyMaster = currencyMaster
        self.isInvited = isInvited
        self.bidInviteCount = bidInviteCount
        self.vehicleTypeMaster = vehicleTypeMaster
        self.loadBidCode = loadBidCode
        self.registrationNo = registrationNo
        self.comments = comments
        self.sequence = sequence
        self.postType = postType
    }
}

// MARK: - CapacityWeightUnit
class FleetCapacityWeightUnit {
    let id: Int
    let name, type: String
    
    init(id: Int, name: String, type: String) {
        self.id = id
        self.name = name
        self.type = type
    }
}

// MARK: - CargoType
class FleetCargoType {
    let id: Int
    let cargoType: String
    
    init(id: Int, cargoType: String) {
        self.id = id
        self.cargoType = cargoType
    }
}

// MARK: - CurrencyMaster
class FleetCurrencyMaster {
    let id: Int
    let currencyName, currencyCountry, currencyCode: String
    
    init(id: Int, currencyName: String, currencyCountry: String, currencyCode: String) {
        self.id = id
        self.currencyName = currencyName
        self.currencyCountry = currencyCountry
        self.currencyCode = currencyCode
    }
}

// MARK: - FleetOwner
class FleetOwner {
    let id: Int
    let name, registrationNo, panNo, fax: String
    let telephone: String
    let address: FleetAddress
    let userCount, activeUserCount: Int
    let companyCode: String
    let logo: FleetLogo
    let gstTax: Int
    let gstTaxRegNo, jurisdiction, timeZone, vatNo: String
    let iecNo, cinNo, cstNo: String
    let registrationDate: Int
    let expiredType: String
    let subscriptionStartDate, subscriptionEndDate, perTransactionFee, smsCharge: Int
    let currentBalance, bookingCount, purchasePaymentCount, purchaseReceiptCount: Int
    let routeCount: Int
    let smsNotification: String
    let maxUser: Int
    let maxUserType, status: String
    let contractMasterCount, goCount, proCount, profileMailCount: Int
    let ewbFlagStr: String
    let walletAmount, hubMaxUser, allowTransaction, remainsTr: Int
    let hubPlanID, hubTransactionCost, exPurchasePayCount, exPurchaseReceiptCount: Int
    let userCounts: Int
    
    init(id: Int, name: String, registrationNo: String, panNo: String, fax: String, telephone: String, address: FleetAddress, userCount: Int, activeUserCount: Int, companyCode: String, logo: FleetLogo, gstTax: Int, gstTaxRegNo: String, jurisdiction: String, timeZone: String, vatNo: String, iecNo: String, cinNo: String, cstNo: String, registrationDate: Int, expiredType: String, subscriptionStartDate: Int, subscriptionEndDate: Int, perTransactionFee: Int, smsCharge: Int, currentBalance: Int, bookingCount: Int, purchasePaymentCount: Int, purchaseReceiptCount: Int, routeCount: Int, smsNotification: String, maxUser: Int, maxUserType: String, status: String, contractMasterCount: Int, goCount: Int, proCount: Int, profileMailCount: Int, ewbFlagStr: String, walletAmount: Int, hubMaxUser: Int, allowTransaction: Int, remainsTr: Int, hubPlanID: Int, hubTransactionCost: Int, exPurchasePayCount: Int, exPurchaseReceiptCount: Int, userCounts: Int) {
        self.id = id
        self.name = name
        self.registrationNo = registrationNo
        self.panNo = panNo
        self.fax = fax
        self.telephone = telephone
        self.address = address
        self.userCount = userCount
        self.activeUserCount = activeUserCount
        self.companyCode = companyCode
        self.logo = logo
        self.gstTax = gstTax
        self.gstTaxRegNo = gstTaxRegNo
        self.jurisdiction = jurisdiction
        self.timeZone = timeZone
        self.vatNo = vatNo
        self.iecNo = iecNo
        self.cinNo = cinNo
        self.cstNo = cstNo
        self.registrationDate = registrationDate
        self.expiredType = expiredType
        self.subscriptionStartDate = subscriptionStartDate
        self.subscriptionEndDate = subscriptionEndDate
        self.perTransactionFee = perTransactionFee
        self.smsCharge = smsCharge
        self.currentBalance = currentBalance
        self.bookingCount = bookingCount
        self.purchasePaymentCount = purchasePaymentCount
        self.purchaseReceiptCount = purchaseReceiptCount
        self.routeCount = routeCount
        self.smsNotification = smsNotification
        self.maxUser = maxUser
        self.maxUserType = maxUserType
        self.status = status
        self.contractMasterCount = contractMasterCount
        self.goCount = goCount
        self.proCount = proCount
        self.profileMailCount = profileMailCount
        self.ewbFlagStr = ewbFlagStr
        self.walletAmount = walletAmount
        self.hubMaxUser = hubMaxUser
        self.allowTransaction = allowTransaction
        self.remainsTr = remainsTr
        self.hubPlanID = hubPlanID
        self.hubTransactionCost = hubTransactionCost
        self.exPurchasePayCount = exPurchasePayCount
        self.exPurchaseReceiptCount = exPurchaseReceiptCount
        self.userCounts = userCounts
    }
}

// MARK: - Address
class FleetAddress {
    let id: Int
    let address, landmark, city, state: String
    let pincode, country: String
    let latitude, longitude: Double
    
    init(id: Int, address: String, landmark: String, city: String, state: String, pincode: String, country: String, latitude: Double, longitude: Double) {
        self.id = id
        self.address = address
        self.landmark = landmark
        self.city = city
        self.state = state
        self.pincode = pincode
        self.country = country
        self.latitude = latitude
        self.longitude = longitude
    }
}

// MARK: - Logo
class FleetLogo {
    let id: Int
    let mimeType, type: String
    
    init(id: Int, mimeType: String, type: String) {
        self.id = id
        self.mimeType = mimeType
        self.type = type
    }
}

// MARK: - LoadType
class FleetLoadType {
    let id: Int
    let loadType, code: String
    
    init(id: Int, loadType: String, code: String) {
        self.id = id
        self.loadType = loadType
        self.code = code
    }
}

// MARK: - VehicleType
class FleetVehicleType {
    let id: Int
    let name: String
    let sequence: Int
    let container: Bool
    let type: String
    let document, documentSmall: FleetLogo
    let vehicleOrder, documentID, smallDocID: Int
    
    init(id: Int, name: String, sequence: Int, container: Bool, type: String, document: FleetLogo, documentSmall: FleetLogo, vehicleOrder: Int, documentID: Int, smallDocID: Int) {
        self.id = id
        self.name = name
        self.sequence = sequence
        self.container = container
        self.type = type
        self.document = document
        self.documentSmall = documentSmall
        self.vehicleOrder = vehicleOrder
        self.documentID = documentID
        self.smallDocID = smallDocID
    }
}
