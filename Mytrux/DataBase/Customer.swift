//
//  Customer.swift
//  Mytrux
//
//  Created by Mytrux on 21/11/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import Foundation

// MARK: - DataClass
class Customer {
    let favouriteCompany: [FavouriteCompany]?
    let activeCount, pendingCount, inactiveCount, manualCount: Int?
    let blackListActiveCount, blackListInactiveCount, totalcount: Int?

    init(favouriteCompany: [FavouriteCompany]?, activeCount: Int?, pendingCount: Int?, inactiveCount: Int?, manualCount: Int?, blackListActiveCount: Int?, blackListInactiveCount: Int?, totalcount: Int?) {
        self.favouriteCompany = favouriteCompany
        self.activeCount = activeCount
        self.pendingCount = pendingCount
        self.inactiveCount = inactiveCount
        self.manualCount = manualCount
        self.blackListActiveCount = blackListActiveCount
        self.blackListInactiveCount = blackListInactiveCount
        self.totalcount = totalcount
    }
}

// MARK: - FavouriteCompany
class FavouriteCompany {
    let id: Int?
    let name: String?
    let address: CustomerAddress?
    let contactPersons: [CustomerContactPerson]?
    let fax, email, gstTaxRegNo: String?
    let companyStatus: String?
    let code: String?
    let registrationDate: Int?
    let hubStatus: String?
    let count: Int?
    let telephone: String?
    let typeOfCompany: TypeOfCompany1?
    let panNo, cinNo, companyCode: String?

    init(id: Int?, name: String?, address: CustomerAddress?, contactPersons: [CustomerContactPerson]?, fax: String?, email: String?, gstTaxRegNo: String?, companyStatus: String?, code: String?, registrationDate: Int?, hubStatus: String?, count: Int?, telephone: String?, typeOfCompany: TypeOfCompany1?, panNo: String?, cinNo: String?, companyCode: String?) {
        self.id = id
        self.name = name
        self.address = address
        self.contactPersons = contactPersons
        self.fax = fax
        self.email = email
        self.gstTaxRegNo = gstTaxRegNo
        self.companyStatus = companyStatus
        self.code = code
        self.registrationDate = registrationDate
        self.hubStatus = hubStatus
        self.count = count
        self.telephone = telephone
        self.typeOfCompany = typeOfCompany
        self.panNo = panNo
        self.cinNo = cinNo
        self.companyCode = companyCode
    }
}

// MARK: - Address
class CustomerAddress {
    let id: Int?
    let address, city, state, pincode: String?
    let country: String?
    let latitude, longitude: Double?

    init(id: Int?, address: String?, city: String?, state: String?, pincode: String?, country: String?, latitude: Double?, longitude: Double?) {
        self.id = id
        self.address = address
        self.city = city
        self.state = state
        self.pincode = pincode
        self.country = country
        self.latitude = latitude
        self.longitude = longitude
    }
}
// MARK: - ContactPerson
class CustomerContactPerson {
    let id: Int?
    let fullName, contactNo, email: String?
    let loadOwnerID: Int?

    init(id: Int?, fullName: String?, contactNo: String?, email: String?, loadOwnerID: Int?) {
        self.id = id
        self.fullName = fullName
        self.contactNo = contactNo
        self.email = email
        self.loadOwnerID = loadOwnerID
    }
}

// MARK: - TypeOfCompany
//class TypeOfCompany {
//    let id: Int?
//    let companyType, companyCategories: String?
//
//    init(id: Int?, companyType: String?, companyCategories: String?) {
//        self.id = id
//        self.companyType = companyType
//        self.companyCategories = companyCategories
//    }
//}
