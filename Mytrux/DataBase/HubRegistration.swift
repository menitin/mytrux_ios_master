//
//  HubRegistration.swift
//  Mytrux
//
//  Created by Mukta Bhuyar Punjabi on 29/05/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import Foundation
struct masters : Codable {
    var address : [Address]
}
struct Address : Codable {
    var city : String
    var address : String
    var country : String
    var state : String
    var pincode : String
    
    enum CodingKeys: String, CodingKey {
        case city = "city"
        case address = "address"
        case country = "country"
        case state = "state"
        case pincode = "pincode"
       
    }

}

struct User : Codable {
    var firstName : String
    var surname : String
    var designation : String
    var mobileNo : String
    var email : String
    var serviceType : String
    var appActivated : String
    enum CodingKeys: String, CodingKey {
        case firstName = "firstName"
        case surname = "surname"
        case designation = "designation"
        case mobileNo = "mobileNo"
        case email = "email"
        case serviceType = "serviceType"
        case appActivated = "appActivated"
    }
}
struct TypeofCompany : Codable {
    var id : Int
    var companyType : String
    var companyCategories : String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case companyType = "companyType"
        case companyCategories = "companyCategories"
       
    }
}
