// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let load = try? newJSONDecoder().decode(Load.self, from: jsonData)

import Foundation

// MARK: - Load
class Load {
    let status: Bool
    let data: [Datum]
    
    init(status: Bool, data: [Datum]) {
        self.status = status
        self.data = data
    }
}

// MARK: - Datum
class Datum {
    let id: Int
    let bidCode: String
    let creationDate, closedBidTime: Int
    let availableLocation, destinationLocation: String
    let vehicleType: VehicleType
    let noOfVehicles, availableDateTime: Int
    let cargoType: CargoType
    let loadOwner: LoadOwner
    let loadType: LoadType1
    let capacityWeight: Int?
    let capacityWeightUnit: CapacityWeightUnit
    let availableLatitude, availableLongitude, destinationLatitude, destinationLongitude: Double?
    let bidQuotationCount: Int
    let bidRank: Int?
    let status: String
    let count: Int?
    let availableLoadBidQuotaions: [AvailableLoadBidQuotaion]
    let packgs, loadOwnerID: Int?
    let vehicleTypeMaster: String
    let ownLastThreeBids: [AvailableLoadBidQuotaion]?
   let availableLoadBidQuotaionElement: AvailableLoadBidQuotaionElement?
   
    init(id: Int, bidCode: String, creationDate: Int, closedBidTime: Int, availableLocation: String, destinationLocation: String, vehicleType: VehicleType, noOfVehicles: Int, availableDateTime: Int, cargoType: CargoType, loadOwner: LoadOwner, loadType: LoadType1, capacityWeight: Int?, capacityWeightUnit: CapacityWeightUnit, availableLatitude: Double?, availableLongitude: Double?, destinationLatitude: Double?, destinationLongitude: Double?, bidQuotationCount: Int, bidRank: Int?, status: String, count: Int?, availableLoadBidQuotaions: [AvailableLoadBidQuotaion],ownLastThreeBids: [AvailableLoadBidQuotaion]?,
        packgs: Int?, loadOwnerID: Int?, vehicleTypeMaster: String, availableLoadBidQuotaionElement: AvailableLoadBidQuotaionElement?) {
        self.id = id
        self.bidCode = bidCode
        self.creationDate = creationDate
        self.closedBidTime = closedBidTime
        self.availableLocation = availableLocation
        self.destinationLocation = destinationLocation
        self.vehicleType = vehicleType
        self.noOfVehicles = noOfVehicles
        self.availableDateTime = availableDateTime
        self.cargoType = cargoType
        self.loadOwner = loadOwner
        self.loadType = loadType
        self.capacityWeight = capacityWeight
        self.capacityWeightUnit = capacityWeightUnit
        self.availableLatitude = availableLatitude
        self.availableLongitude = availableLongitude
        self.destinationLatitude = destinationLatitude
        self.destinationLongitude = destinationLongitude
        self.bidQuotationCount = bidQuotationCount
        self.bidRank = bidRank
        self.status = status
        self.count = count
        self.availableLoadBidQuotaions = availableLoadBidQuotaions
        self.packgs = packgs
        self.loadOwnerID = loadOwnerID
        self.vehicleTypeMaster = vehicleTypeMaster
        self.ownLastThreeBids = ownLastThreeBids
        self.availableLoadBidQuotaionElement = availableLoadBidQuotaionElement
    }
}

// MARK: - AvailableLoadBidQuotaion
class AvailableLoadBidQuotaion {
    let id: Int
    let quotationAmount: Int
    let currencyMaster: CurrencyMaster?
    let quotationEntryDate: Int
    
    init(id: Int, quotationAmount: Int, currencyMaster: CurrencyMaster?, quotationEntryDate: Int) {
        self.id = id
        self.quotationAmount = quotationAmount
        self.currencyMaster = currencyMaster
        self.quotationEntryDate = quotationEntryDate
    }
}



class AvailableLoadBidQuotaionElement {
    let id, quotationAmount: Int?
    let currencyMaster: AvailableFleetBidCurrencyMaster
    let validDateTime, quotationEntryDate: Int?
    let availableFleetBid: AvailableFleetBid
    
    init(id: Int?, quotationAmount: Int?, currencyMaster: AvailableFleetBidCurrencyMaster, validDateTime: Int?, quotationEntryDate: Int?, availableFleetBid: AvailableFleetBid) {
        self.id = id
        self.quotationAmount = quotationAmount
        self.currencyMaster = currencyMaster
        self.validDateTime = validDateTime
        self.quotationEntryDate = quotationEntryDate
        self.availableFleetBid = availableFleetBid
    }
}

class AvailableFleetBid {
    let id: Int?
    let bidCode: String?
    let creationDate, closedBidTime: Int?
    let availableLocation, destinationLocation: String?
    let availableLatitude, availableLongitude, destinationLatitude, destinationLongitude: Double?
    let vehicleType: AvailableVehicleType
    let noOfVehicles, availableDateTime: Int?
   let cargoType: CargoType
    let capacityWeight: Int?
   let capacityWeightUnit: CapacityWeightUnit
    let loadType: AvailLoadType1
    let expectedFreight: Int?
    let currencyMaster: AvailableFleetBidCurrencyMaster
    let status: String?
    
    init(id: Int?, bidCode: String?, creationDate: Int?, closedBidTime: Int?, availableLocation: String?, destinationLocation: String?, availableLatitude: Double?, availableLongitude: Double?, destinationLatitude: Double?, destinationLongitude: Double?, vehicleType: AvailableVehicleType, noOfVehicles: Int?, availableDateTime: Int?,capacityWeight: Int?, loadType: AvailLoadType1, expectedFreight: Int?, currencyMaster: AvailableFleetBidCurrencyMaster, status: String?,cargoType:CargoType,capacityWeightUnit:CapacityWeightUnit) {
        self.id = id
        self.bidCode = bidCode
        self.creationDate = creationDate
        self.closedBidTime = closedBidTime
        self.availableLocation = availableLocation
        self.destinationLocation = destinationLocation
        self.availableLatitude = availableLatitude
        self.availableLongitude = availableLongitude
        self.destinationLatitude = destinationLatitude
        self.destinationLongitude = destinationLongitude
        self.vehicleType = vehicleType
        self.noOfVehicles = noOfVehicles
        self.availableDateTime = availableDateTime
        self.capacityWeight = capacityWeight
        self.loadType = loadType
        self.expectedFreight = expectedFreight
        self.currencyMaster = currencyMaster
        self.status = status
        self.cargoType = cargoType
         self.capacityWeightUnit = capacityWeightUnit
    }
}


// MARK: - AvailableFleetBidCurrencyMaster
class AvailableFleetBidCurrencyMaster {
    let id: Int?
    let currencyName, currencyCountry, currencyCode: String?
    
    init(id: Int?, currencyName: String?, currencyCountry: String?, currencyCode: String?) {
        self.id = id
        self.currencyName = currencyName
        self.currencyCountry = currencyCountry
        self.currencyCode = currencyCode
    }
}

// MARK: - CurrencyMaster
class CurrencyMaster {
    let id: Int?
    let currencyCode: String?
    
    init(id: Int?, currencyCode: String?) {
        self.id = id
        self.currencyCode = currencyCode
    }
}

// MARK: - CapacityWeightUnit
class CapacityWeightUnit {
    let id: Int?
    let name, type: String?
    
    init(id: Int?, name: String?, type: String?) {
        self.id = id
        self.name = name
        self.type = type
    }
}

// MARK: - CargoType
class CargoType {
    let id: Int?
    let cargoType: String?
    
    init(id: Int?, cargoType: String?) {
        self.id = id
        self.cargoType = cargoType
    }
}

// MARK: - LoadOwner
class LoadOwner {
    let id: Int
    let name, registrationNo, companyCode, telephone: String
    let fax: String
    let address: Address1
    let logo: Logo
    let gstTax: Double
    let gstTaxRegNo, jurisdiction, timeZone, panNo: String
    let vatNo, iecNo, cinNo, cstNo: String
    let registrationDate, subscriptionStartDate, subscriptionEndDate, currentBalance: Int
    let smsNotification: String
    let companyAccess: CompanyAccess
    let status, expiredType: String
    let activeUserCount, maxUser: Int
    let maxUserType: String
    let exchangeBookingCount, goCount, proCount, profileMailCount: Int
    let ewbFlagStr, planType: String
    let exSuwalletAmount, exSuhubMaxUser, exSuallowTransaction, exSuremainsTr: Int
    let hubPlanID, exSuhubTransactionCost: Int
    let thirdPartyAuthDateTime: Int?
   
    
    init(id: Int, name: String, registrationNo: String, companyCode: String, telephone: String, fax: String, address: Address1, logo: Logo, gstTax: Double, gstTaxRegNo: String, jurisdiction: String, timeZone: String, panNo: String, vatNo: String, iecNo: String, cinNo: String, cstNo: String, registrationDate: Int, subscriptionStartDate: Int, subscriptionEndDate: Int, currentBalance: Int, smsNotification: String, companyAccess: CompanyAccess, status: String, expiredType: String, activeUserCount: Int, maxUser: Int, maxUserType: String, exchangeBookingCount: Int, goCount: Int, proCount: Int, profileMailCount: Int, ewbFlagStr: String, planType: String, exSuwalletAmount: Int, exSuhubMaxUser: Int, exSuallowTransaction: Int, exSuremainsTr: Int, hubPlanID: Int, exSuhubTransactionCost: Int, thirdPartyAuthDateTime: Int?) {
        self.id = id
        self.name = name
        self.registrationNo = registrationNo
        self.companyCode = companyCode
        self.telephone = telephone
        self.fax = fax
        self.address = address
        self.logo = logo
        self.gstTax = gstTax
        self.gstTaxRegNo = gstTaxRegNo
        self.jurisdiction = jurisdiction
        self.timeZone = timeZone
        self.panNo = panNo
        self.vatNo = vatNo
        self.iecNo = iecNo
        self.cinNo = cinNo
        self.cstNo = cstNo
        self.registrationDate = registrationDate
        self.subscriptionStartDate = subscriptionStartDate
        self.subscriptionEndDate = subscriptionEndDate
        self.currentBalance = currentBalance
        self.smsNotification = smsNotification
        self.companyAccess = companyAccess
        self.status = status
        self.expiredType = expiredType
        self.activeUserCount = activeUserCount
        self.maxUser = maxUser
        self.maxUserType = maxUserType
        self.exchangeBookingCount = exchangeBookingCount
        self.goCount = goCount
        self.proCount = proCount
        self.profileMailCount = profileMailCount
        self.ewbFlagStr = ewbFlagStr
        self.planType = planType
        self.exSuwalletAmount = exSuwalletAmount
        self.exSuhubMaxUser = exSuhubMaxUser
        self.exSuallowTransaction = exSuallowTransaction
        self.exSuremainsTr = exSuremainsTr
        self.hubPlanID = hubPlanID
        self.exSuhubTransactionCost = exSuhubTransactionCost
        self.thirdPartyAuthDateTime = thirdPartyAuthDateTime
    }
}

// MARK: - Address
class Address1 {
    let id: Int?
    let address, landmark, city, state: String?
    let pincode, country: String?
    
    init(id: Int?, address: String?, landmark: String?, city: String?, state: String?, pincode: String?, country: String?) {
        self.id = id
        self.address = address
        self.landmark = landmark
        self.city = city
        self.state = state
        self.pincode = pincode
        self.country = country
    }
}

// MARK: - CompanyAccess
class CompanyAccess {
    let id: Int
    
    init(id: Int) {
        self.id = id
    }
}

// MARK: - Logo
class Logo {
    let id: Int?
    let mimeType: String?
    let type: String?
    
    init(id: Int?, mimeType: String?, type: String?) {
        self.id = id
        self.mimeType = mimeType
        self.type = type
    }
}


// MARK: - Logo
class AvailableLogo {
    let id: Int?
    let mimeType: String?
    let type: String?
    
    init(id: Int?, mimeType: String?, type: String?) {
        self.id = id
        self.mimeType = mimeType
        self.type = type
    }
}

// MARK: - LoadType
class LoadType1 {
    let id: Int
    let loadType, code: String
    
    init(id: Int, loadType: String, code: String) {
        self.id = id
        self.loadType = loadType
        self.code = code
    }
}


class AvailLoadType1 {
    let id: Int?
    let loadType, code: String?
    
    init(id: Int?, loadType: String?, code: String?) {
        self.id = id
        self.loadType = loadType
        self.code = code
    }
}
// MARK: - VehicleType
class VehicleType {
    let id: Int
    let name: String
    let sequence: Int
    let container: Bool
    let type: String
    let document: Logo
    let documentSmall: Logo
    let vehicleOrder: Int
    let weightCapacity, documentID, smallDocID : Int?
    init(id: Int, name: String, sequence: Int, container: Bool, type: String, document: Logo, documentSmall: Logo, vehicleOrder: Int, weightCapacity: Int?, documentID: Int?, smallDocID: Int?) {
        self.id = id
        self.name = name
        self.sequence = sequence
        self.container = container
        self.type = type
        self.document = document
        self.documentSmall = documentSmall
        self.vehicleOrder = vehicleOrder
        self.weightCapacity = weightCapacity
        self.documentID = documentID
        self.smallDocID = smallDocID
    }
}


class AvailableVehicleType {
    let id: Int?
    let name: String?
    let sequence: Double?
    let container: Bool?
    let type: String?
    let document: AvailableLogo
    let documentSmall: AvailableLogo?
    let vehicleOrder: Int?
    let weightCapacity, documentID, smallDocID : Int?
    init(id: Int?, name: String?, sequence: Double?, container: Bool?, type: String?, document: AvailableLogo, documentSmall: AvailableLogo?, vehicleOrder: Int?, weightCapacity: Int?, documentID: Int?, smallDocID: Int?) {
        self.id = id
        self.name = name
        self.sequence = sequence
        self.container = container
        self.type = type
        self.document = document
        self.documentSmall = documentSmall
        self.vehicleOrder = vehicleOrder
        self.weightCapacity = weightCapacity
        self.documentID = documentID
        self.smallDocID = smallDocID
    }
}
// MARK: - Document
class Document {
    let id: Int
    let mimeType: String
    
    init(id: Int, mimeType: String) {
        self.id = id
        self.mimeType = mimeType
    }
}
