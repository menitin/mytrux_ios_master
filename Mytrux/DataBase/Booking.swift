//
//  Booking.swift
//  Mytrux
//
//  Created by Mytrux on 20/09/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import Foundation


// MARK: - BookingElement
class BookingElement {
    let id: Int?
    let shipper: Shipper?
    let consignee, deliveryCustomer: Consignee?
    let loadOwner: LoadOwner1?
    let fleetOwner: FleetOwner1?
    let billingParty: String?
    let jobStatus: JobStatusClass?
    let loadType: LoadTypeClass?
    let cargoType: CargoTypeClass?
    let jobType: JobTypeClass?
    let bookingType: BookingTypeClass?
    let pickupDateTime, deliveryDateTime: Int?
    let freightTerms: FreightTermsClass?
    let bookingNo: String?
    let availableLoadBid, availableFleetBid: AvailableBid1?
    let loadBidQuotaion: LoadBidQuotaion?
    let pickUpLocation, destination: String?
    let createDate: Int?
    let vehicleContainerDetails: VehicleContainerDetails1?
    let user: NSNull?
    let bookingCust, shiper, consigneer, deliverCust: Bool?
    let exInvoiceGrossAmount, exInvoiceGrossAmountWithoutTax: Int?
    let exTaxType: String?
    let exTaxInfo : ExTaxInfo?
    let exInvoiceNo: String?
    let exTaxGroup: NSNull?
    let invoiceDate: Int?
    let financeYear: String?
    let salesBillFinal, purchaseBillFinal: Bool?
    let defaultBankAndBranch: String?
    let count, lrNo: Int?
    let isinvite: Bool?
    let eLrTerms, salesInvoiceTerms: String?
    let jobVehiclesDrivers, trips: [JobVehiclesDriver]?
    let fleetBidInvite: FleetBidInvite?
    
    init(id: Int?, shipper: Shipper?, consignee: Consignee?, deliveryCustomer: Consignee?, loadOwner: LoadOwner1?, fleetOwner: FleetOwner1?, billingParty: String?, jobStatus: JobStatusClass?, loadType: LoadTypeClass?, cargoType: CargoTypeClass?, jobType: JobTypeClass?, bookingType: BookingTypeClass?, pickupDateTime: Int?, deliveryDateTime: Int?, freightTerms: FreightTermsClass?, bookingNo: String?, availableLoadBid: AvailableBid1?, availableFleetBid: AvailableBid1?, loadBidQuotaion: LoadBidQuotaion?, pickUpLocation: String?, destination: String?, createDate: Int?, vehicleContainerDetails: VehicleContainerDetails1?, user: NSNull?, bookingCust: Bool?, shiper: Bool?, consigneer: Bool?, deliverCust: Bool?, exInvoiceGrossAmount: Int?, exInvoiceGrossAmountWithoutTax: Int?, exTaxType: String?, exTaxInfo: ExTaxInfo?, exInvoiceNo: String?, exTaxGroup: NSNull?, invoiceDate: Int?, financeYear: String?, salesBillFinal: Bool?, purchaseBillFinal: Bool?, defaultBankAndBranch: String?, count: Int?, lrNo: Int?, isinvite: Bool?, eLrTerms: String?, salesInvoiceTerms: String?, jobVehiclesDrivers: [JobVehiclesDriver]?, trips: [JobVehiclesDriver]?, fleetBidInvite: FleetBidInvite?) {
        self.id = id
        self.shipper = shipper
        self.consignee = consignee
        self.deliveryCustomer = deliveryCustomer
        self.loadOwner = loadOwner
        self.fleetOwner = fleetOwner
        self.billingParty = billingParty
        self.jobStatus = jobStatus
        self.loadType = loadType
        self.cargoType = cargoType
        self.jobType = jobType
        self.bookingType = bookingType
        self.pickupDateTime = pickupDateTime
        self.deliveryDateTime = deliveryDateTime
        self.freightTerms = freightTerms
        self.bookingNo = bookingNo
        self.availableLoadBid = availableLoadBid
        self.availableFleetBid = availableFleetBid
        self.loadBidQuotaion = loadBidQuotaion
        self.pickUpLocation = pickUpLocation
        self.destination = destination
        self.createDate = createDate
        self.vehicleContainerDetails = vehicleContainerDetails
        self.user = user
        self.bookingCust = bookingCust
        self.shiper = shiper
        self.consigneer = consigneer
        self.deliverCust = deliverCust
        self.exInvoiceGrossAmount = exInvoiceGrossAmount
        self.exInvoiceGrossAmountWithoutTax = exInvoiceGrossAmountWithoutTax
        self.exTaxType = exTaxType
        self.exTaxInfo = exTaxInfo
        self.exInvoiceNo = exInvoiceNo
        self.exTaxGroup = exTaxGroup
        self.invoiceDate = invoiceDate
        self.financeYear = financeYear
        self.salesBillFinal = salesBillFinal
        self.purchaseBillFinal = purchaseBillFinal
        self.defaultBankAndBranch = defaultBankAndBranch
        self.count = count
        self.lrNo = lrNo
        self.isinvite = isinvite
        self.eLrTerms = eLrTerms
        self.salesInvoiceTerms = salesInvoiceTerms
        self.jobVehiclesDrivers = jobVehiclesDrivers
        self.trips = trips
        self.fleetBidInvite = fleetBidInvite
    }
}

// MARK: - BookingElement
class BookingElement1 {
    let id: Int?
    let shipper: Shipper?
    let consignee, deliveryCustomer: Consignee?
    let loadOwner: LoadOwner1?
    let fleetOwner: FleetOwner1?
    let billingParty: String?
    let jobStatus: JobStatusClass?
    let loadType: LoadTypeClass?
    let cargoType: CargoTypeClass?
    let jobType: JobTypeClass?
    let bookingType: BookingTypeClass?
    let pickupDateTime, deliveryDateTime: Int?
    let freightTerms: FreightTermsClass?
    let bookingNo: String?
    let availableLoadBid, availableFleetBid: AvailableBid1?
    let loadBidQuotaion: LoadBidQuotaion?
    let pickUpLocation, destination: String?
    let createDate: Int?
    let vehicleContainerDetails: VehicleContainerDetails1?
    let user: NSNull?
    let bookingCust, shiper, consigneer, deliverCust: Bool?
    let exInvoiceGrossAmount, exInvoiceGrossAmountWithoutTax: Int?
    let exTaxType: String?
    let exTaxInfo : ExTaxInfo?
    let exInvoiceNo: String?
    let exTaxGroup: NSNull?
    let invoiceDate: Int?
    let financeYear: String?
    let salesBillFinal, purchaseBillFinal: Bool?
    let defaultBankAndBranch: String?
    let count, lrNo: Int?
    let isinvite: Bool?
    let eLrTerms, salesInvoiceTerms: String?
    let jobVehiclesDrivers, trips: [JobVehiclesDriver]?
    let fleetBidInvite: FleetBidInvite?
    
    init(id: Int?, shipper: Shipper?, consignee: Consignee?, deliveryCustomer: Consignee?, loadOwner: LoadOwner1?, fleetOwner: FleetOwner1?, billingParty: String?, jobStatus: JobStatusClass?, loadType: LoadTypeClass?, cargoType: CargoTypeClass?, jobType: JobTypeClass?, bookingType: BookingTypeClass?, pickupDateTime: Int?, deliveryDateTime: Int?, freightTerms: FreightTermsClass?, bookingNo: String?, availableLoadBid: AvailableBid1?, availableFleetBid: AvailableBid1?, loadBidQuotaion: LoadBidQuotaion?, pickUpLocation: String?, destination: String?, createDate: Int?, vehicleContainerDetails: VehicleContainerDetails1?, user: NSNull?, bookingCust: Bool?, shiper: Bool?, consigneer: Bool?, deliverCust: Bool?, exInvoiceGrossAmount: Int?, exInvoiceGrossAmountWithoutTax: Int?, exTaxType: String?, exTaxInfo: ExTaxInfo?, exInvoiceNo: String?, exTaxGroup: NSNull?, invoiceDate: Int?, financeYear: String?, salesBillFinal: Bool?, purchaseBillFinal: Bool?, defaultBankAndBranch: String?, count: Int?, lrNo: Int?, isinvite: Bool?, eLrTerms: String?, salesInvoiceTerms: String?, jobVehiclesDrivers: [JobVehiclesDriver]?, trips: [JobVehiclesDriver]?, fleetBidInvite: FleetBidInvite?) {
        self.id = id
        self.shipper = shipper
        self.consignee = consignee
        self.deliveryCustomer = deliveryCustomer
        self.loadOwner = loadOwner
        self.fleetOwner = fleetOwner
        self.billingParty = billingParty
        self.jobStatus = jobStatus
        self.loadType = loadType
        self.cargoType = cargoType
        self.jobType = jobType
        self.bookingType = bookingType
        self.pickupDateTime = pickupDateTime
        self.deliveryDateTime = deliveryDateTime
        self.freightTerms = freightTerms
        self.bookingNo = bookingNo
        self.availableLoadBid = availableLoadBid
        self.availableFleetBid = availableFleetBid
        self.loadBidQuotaion = loadBidQuotaion
        self.pickUpLocation = pickUpLocation
        self.destination = destination
        self.createDate = createDate
        self.vehicleContainerDetails = vehicleContainerDetails
        self.user = user
        self.bookingCust = bookingCust
        self.shiper = shiper
        self.consigneer = consigneer
        self.deliverCust = deliverCust
        self.exInvoiceGrossAmount = exInvoiceGrossAmount
        self.exInvoiceGrossAmountWithoutTax = exInvoiceGrossAmountWithoutTax
        self.exTaxType = exTaxType
        self.exTaxInfo = exTaxInfo
        self.exInvoiceNo = exInvoiceNo
        self.exTaxGroup = exTaxGroup
        self.invoiceDate = invoiceDate
        self.financeYear = financeYear
        self.salesBillFinal = salesBillFinal
        self.purchaseBillFinal = purchaseBillFinal
        self.defaultBankAndBranch = defaultBankAndBranch
        self.count = count
        self.lrNo = lrNo
        self.isinvite = isinvite
        self.eLrTerms = eLrTerms
        self.salesInvoiceTerms = salesInvoiceTerms
        self.jobVehiclesDrivers = jobVehiclesDrivers
        self.trips = trips
        self.fleetBidInvite = fleetBidInvite
    }
}


// MARK: - AvailableBid
class AvailableBid1 {
    let id: Int?
    let bidCode: String?
    let creationDate, closedBidTime: Int?
    let availableLocation, destinationLocation: String?
    let comments: String?
    let availableLatitude, availableLongitude, destinationLatitude, destinationLongitude: Double?
    let vehicleType: VehicleType?
    let noOfVehicles, availableDateTime: Int?
    let cargoType: CargoTypeClass?
    let capacityWeight: Int?
    let capacityWeightUnit: WeightUnit?
    let loadType: LoadTypeClass?
    let expectedFreight: Int?
    let currencyMaster: CurrencyMaster?
    let status: String?
    let registrationNo: String?
    let postType: String?
    let bidInviteCount, bidQuotationCount: Int?
    let containerNo: String?
    let sealNo: String?
    let pinNo: String?
    let packgs: Int?
    
    init(id: Int?, bidCode: String?, creationDate: Int?, closedBidTime: Int?, availableLocation: String?, destinationLocation: String?, comments: String?, availableLatitude: Double?, availableLongitude: Double?, destinationLatitude: Double?, destinationLongitude: Double?, vehicleType: VehicleType?, noOfVehicles: Int?, availableDateTime: Int?, cargoType: CargoTypeClass?, capacityWeight: Int?, capacityWeightUnit: WeightUnit?, loadType: LoadTypeClass?, expectedFreight: Int?, currencyMaster: CurrencyMaster?, status: String?, registrationNo: String?, postType: String?, bidInviteCount: Int?, bidQuotationCount: Int?, containerNo: String?, sealNo: String?, pinNo: String?, packgs: Int?) {
        self.id = id
        self.bidCode = bidCode
        self.creationDate = creationDate
        self.closedBidTime = closedBidTime
        self.availableLocation = availableLocation
        self.destinationLocation = destinationLocation
        self.comments = comments
        self.availableLatitude = availableLatitude
        self.availableLongitude = availableLongitude
        self.destinationLatitude = destinationLatitude
        self.destinationLongitude = destinationLongitude
        self.vehicleType = vehicleType
        self.noOfVehicles = noOfVehicles
        self.availableDateTime = availableDateTime
        self.cargoType = cargoType
        self.capacityWeight = capacityWeight
        self.capacityWeightUnit = capacityWeightUnit
        self.loadType = loadType
        self.expectedFreight = expectedFreight
        self.currencyMaster = currencyMaster
        self.status = status
        self.registrationNo = registrationNo
        self.postType = postType
        self.bidInviteCount = bidInviteCount
        self.bidQuotationCount = bidQuotationCount
        self.containerNo = containerNo
        self.sealNo = sealNo
        self.pinNo = pinNo
        self.packgs = packgs
    }
}



// MARK: - BookingTypeClass
class BookingTypeClass {
    let id: Int?
    let bookingType: String?
    
    init(id: Int?, bookingType: String?) {
        self.id = id
        self.bookingType = bookingType
    }
}


// MARK: - Consignee
class Consignee {
    let id: Int?
    let name, address, location: String?
    let state: String?
    let contactPerson, contactNo: String?
    let loadOwner: LoadOwner1?
    let gstNo, pinNo, country: String?
    let createDate, updateDate: Int?
    let secAddress, contactEmail, cinNo, panNo: String?
    
    init(id: Int?, name: String?, address: String?, location: String?, state: String?, contactPerson: String?, contactNo: String?, loadOwner: LoadOwner1?, gstNo: String?, pinNo: String?, country: String?, createDate: Int?, updateDate: Int?, secAddress: String?, contactEmail: String?, cinNo: String?, panNo: String?) {
        self.id = id
        self.name = name
        self.address = address
        self.location = location
        self.state = state
        self.contactPerson = contactPerson
        self.contactNo = contactNo
        self.loadOwner = loadOwner
        self.gstNo = gstNo
        self.pinNo = pinNo
        self.country = country
        self.createDate = createDate
        self.updateDate = updateDate
        self.secAddress = secAddress
        self.contactEmail = contactEmail
        self.cinNo = cinNo
        self.panNo = panNo
    }
}

// MARK: - LoadOwner
class LoadOwner1 {
    let id: Int?
    let name: String?
    let registrationNo: String?
    let companyCode: String?
    let telephone, fax: String?
    let address, typeOfCompany, logo: NSNull?
    let gstTax: Double?
    let gstTaxRegNo: String?
    let jurisdiction: String?
    let timeZone: String?
    let panNo: String?
    let vatNo: String?
    let iecNo: String?
    let cinNo: String?
    let cstNo: String?
    let registrationDate, subscriptionStartDate, subscriptionEndDate, currentBalance: Int?
    let smsNotification: String?
    let companyAccess: CompanyAccess?
    let loadAdditionalInformation: NSNull?
    let status: String?
    let expiredType: String?
    let activeUserCount, maxUser: Int?
    let maxUserType: String?
    let exchangeBookingCount, exWalletValidFrom, exWalletValidTill, exWalletValidDays: Int?
    let exPlanType: String?
    let goCount, proCount, profileMailCount: Int?
    let ewbFlagStr: String?
    let planType: String?
    let exSuwalletAmount, exSuhubMaxUser, exSuallowTransaction, exSuremainsTr: Int?
    let hubPlanID, exSuhubTransactionCost: Int?
    let exPackType: String?
    
    init(id: Int?, name: String?, registrationNo: String?, companyCode: String?, telephone: String?, fax: String?, address: NSNull?, typeOfCompany: NSNull?, logo: NSNull?, gstTax: Double?, gstTaxRegNo: String?, jurisdiction: String?, timeZone: String?, panNo: String?, vatNo: String?, iecNo: String?, cinNo: String?, cstNo: String?, registrationDate: Int?, subscriptionStartDate: Int?, subscriptionEndDate: Int?, currentBalance: Int?, smsNotification: String?, companyAccess: CompanyAccess?, loadAdditionalInformation: NSNull?, status: String?, expiredType: String?, activeUserCount: Int?, maxUser: Int?, maxUserType: String?, exchangeBookingCount: Int?, exWalletValidFrom: Int?, exWalletValidTill: Int?, exWalletValidDays: Int?, exPlanType: String?, goCount: Int?, proCount: Int?, profileMailCount: Int?, ewbFlagStr: String?, planType: String?, exSuwalletAmount: Int?, exSuhubMaxUser: Int?, exSuallowTransaction: Int?, exSuremainsTr: Int?, hubPlanID: Int?, exSuhubTransactionCost: Int?, exPackType: String?) {
        self.id = id
        self.name = name
        self.registrationNo = registrationNo
        self.companyCode = companyCode
        self.telephone = telephone
        self.fax = fax
        self.address = address
        self.typeOfCompany = typeOfCompany
        self.logo = logo
        self.gstTax = gstTax
        self.gstTaxRegNo = gstTaxRegNo
        self.jurisdiction = jurisdiction
        self.timeZone = timeZone
        self.panNo = panNo
        self.vatNo = vatNo
        self.iecNo = iecNo
        self.cinNo = cinNo
        self.cstNo = cstNo
        self.registrationDate = registrationDate
        self.subscriptionStartDate = subscriptionStartDate
        self.subscriptionEndDate = subscriptionEndDate
        self.currentBalance = currentBalance
        self.smsNotification = smsNotification
        self.companyAccess = companyAccess
        self.loadAdditionalInformation = loadAdditionalInformation
        self.status = status
        self.expiredType = expiredType
        self.activeUserCount = activeUserCount
        self.maxUser = maxUser
        self.maxUserType = maxUserType
        self.exchangeBookingCount = exchangeBookingCount
        self.exWalletValidFrom = exWalletValidFrom
        self.exWalletValidTill = exWalletValidTill
        self.exWalletValidDays = exWalletValidDays
        self.exPlanType = exPlanType
        self.goCount = goCount
        self.proCount = proCount
        self.profileMailCount = profileMailCount
        self.ewbFlagStr = ewbFlagStr
        self.planType = planType
        self.exSuwalletAmount = exSuwalletAmount
        self.exSuhubMaxUser = exSuhubMaxUser
        self.exSuallowTransaction = exSuallowTransaction
        self.exSuremainsTr = exSuremainsTr
        self.hubPlanID = hubPlanID
        self.exSuhubTransactionCost = exSuhubTransactionCost
        self.exPackType = exPackType
    }
}









// MARK: - FleetBidInvite
class FleetBidInvite {
    let id: Int?
    let availableFleetBid: AvailableBid1?
    let loadOwner: LoadOwner?
    let inviteDate: Int?
    let availableLoadBid: AvailableBid1?
    let inviteStatus: String?
    
    init(id: Int?, availableFleetBid: AvailableBid1?, loadOwner: LoadOwner?, inviteDate: Int?, availableLoadBid: AvailableBid1?, inviteStatus: String?) {
        self.id = id
        self.availableFleetBid = availableFleetBid
        self.loadOwner = loadOwner
        self.inviteDate = inviteDate
        self.availableLoadBid = availableLoadBid
        self.inviteStatus = inviteStatus
    }
}

// MARK: - FleetOwner
class FleetOwner1 {
    let id: Int?
    let name: String?
    let registrationNo: String?
    let panNo: String?
    let fax, telephone: String?
    let typeOfCompany, fleetAdditionalInformation, address: NSNull?
    let userCount, activeUserCount: Int?
    let companyCode: String?
    let logo: NSNull?
    let gstTax: Int?
    let gstTaxRegNo: String?
    let jurisdiction: String?
    let timeZone: String?
    let vatNo: String?
    let iecNo: String?
    let cinNo: String?
    let cstNo: String?
    let registrationDate: Int?
    let expiredType: String?
    let subscriptionStartDate, subscriptionEndDate, perTransactionFee, smsCharge: Int?
    let currentBalance, bookingCount, purchasePaymentCount, purchaseReceiptCount: Int?
    let routeCount: Int?
    let smsNotification: String?
    let maxUser: Int?
    let maxUserType: String?
    let status: String?
    let contractMasterCount, goCount, proCount, profileMailCount: Int?
    let ewbFlagStr: String?
    let walletAmount, hubMaxUser, allowTransaction, remainsTr: Int?
    let hubPlanID, hubTransactionCost, thirdPartyAuthDateTime, exPurchasePayCount: Int?
    let exPurchaseReceiptCount, exWalletValidFrom, exWalletValidTill, exWalletValidDays: Int?
    let exPlanType: String?
    let exPackType: String?
    let userCounts: Int?
    
    init(id: Int?, name: String?, registrationNo: String?, panNo: String?, fax: String?, telephone: String?, typeOfCompany: NSNull?, fleetAdditionalInformation: NSNull?, address: NSNull?, userCount: Int?, activeUserCount: Int?, companyCode: String?, logo: NSNull?, gstTax: Int?, gstTaxRegNo: String?, jurisdiction: String?, timeZone: String?, vatNo: String?, iecNo: String?, cinNo: String?, cstNo: String?, registrationDate: Int?, expiredType: String?, subscriptionStartDate: Int?, subscriptionEndDate: Int?, perTransactionFee: Int?, smsCharge: Int?, currentBalance: Int?, bookingCount: Int?, purchasePaymentCount: Int?, purchaseReceiptCount: Int?, routeCount: Int?, smsNotification: String?, maxUser: Int?, maxUserType: String?, status: String?, contractMasterCount: Int?, goCount: Int?, proCount: Int?, profileMailCount: Int?, ewbFlagStr: String?, walletAmount: Int?, hubMaxUser: Int?, allowTransaction: Int?, remainsTr: Int?, hubPlanID: Int?, hubTransactionCost: Int?, thirdPartyAuthDateTime: Int?, exPurchasePayCount: Int?, exPurchaseReceiptCount: Int?, exWalletValidFrom: Int?, exWalletValidTill: Int?, exWalletValidDays: Int?, exPlanType: String?, exPackType: String?, userCounts: Int?) {
        self.id = id
        self.name = name
        self.registrationNo = registrationNo
        self.panNo = panNo
        self.fax = fax
        self.telephone = telephone
        self.typeOfCompany = typeOfCompany
        self.fleetAdditionalInformation = fleetAdditionalInformation
        self.address = address
        self.userCount = userCount
        self.activeUserCount = activeUserCount
        self.companyCode = companyCode
        self.logo = logo
        self.gstTax = gstTax
        self.gstTaxRegNo = gstTaxRegNo
        self.jurisdiction = jurisdiction
        self.timeZone = timeZone
        self.vatNo = vatNo
        self.iecNo = iecNo
        self.cinNo = cinNo
        self.cstNo = cstNo
        self.registrationDate = registrationDate
        self.expiredType = expiredType
        self.subscriptionStartDate = subscriptionStartDate
        self.subscriptionEndDate = subscriptionEndDate
        self.perTransactionFee = perTransactionFee
        self.smsCharge = smsCharge
        self.currentBalance = currentBalance
        self.bookingCount = bookingCount
        self.purchasePaymentCount = purchasePaymentCount
        self.purchaseReceiptCount = purchaseReceiptCount
        self.routeCount = routeCount
        self.smsNotification = smsNotification
        self.maxUser = maxUser
        self.maxUserType = maxUserType
        self.status = status
        self.contractMasterCount = contractMasterCount
        self.goCount = goCount
        self.proCount = proCount
        self.profileMailCount = profileMailCount
        self.ewbFlagStr = ewbFlagStr
        self.walletAmount = walletAmount
        self.hubMaxUser = hubMaxUser
        self.allowTransaction = allowTransaction
        self.remainsTr = remainsTr
        self.hubPlanID = hubPlanID
        self.hubTransactionCost = hubTransactionCost
        self.thirdPartyAuthDateTime = thirdPartyAuthDateTime
        self.exPurchasePayCount = exPurchasePayCount
        self.exPurchaseReceiptCount = exPurchaseReceiptCount
        self.exWalletValidFrom = exWalletValidFrom
        self.exWalletValidTill = exWalletValidTill
        self.exWalletValidDays = exWalletValidDays
        self.exPlanType = exPlanType
        self.exPackType = exPackType
        self.userCounts = userCounts
    }
}

// MARK: - FreightTermsClass
class FreightTermsClass {
    let id: Int?
    let freightTerms: String?
    
    init(id: Int?, freightTerms: String?) {
        self.id = id
        self.freightTerms = freightTerms
    }
}



// MARK: - JobTypeClass
class JobTypeClass {
    let id: Int?
    let jobType: String?
    
    init(id: Int?, jobType: String?) {
        self.id = id
        self.jobType = jobType
    }
}



// MARK: - JobVehiclesDriver
class JobVehiclesDriver {
    let id: Int?
    let availableFleetBid: AvailableBid1?
    let vehicleRegistrationNo, driverFullName, mobileNo: String?
    let startKM, endKM: Int?
    let user: User1?
    let allocationType: String?
    let createTime: Int?
    let vehicleType: VehicleType?
    let jobStatus: JobStatusClass?
    let lrNo, uploadPodTime: Int?
    let podID: PodID?
    let exManualLrNo: String?
    let exManualLrDate: Int?
    
    init(id: Int?, availableFleetBid: AvailableBid1?, vehicleRegistrationNo: String?, driverFullName: String?, mobileNo: String?, startKM: Int?, endKM: Int?, user: User1?, allocationType: String?, createTime: Int?, vehicleType: VehicleType?, jobStatus: JobStatusClass?, lrNo: Int?, uploadPodTime: Int?, podID: PodID?, exManualLrNo: String?, exManualLrDate: Int?) {
        self.id = id
        self.availableFleetBid = availableFleetBid
        self.vehicleRegistrationNo = vehicleRegistrationNo
        self.driverFullName = driverFullName
        self.mobileNo = mobileNo
        self.startKM = startKM
        self.endKM = endKM
        self.user = user
        self.allocationType = allocationType
        self.createTime = createTime
        self.vehicleType = vehicleType
        self.jobStatus = jobStatus
        self.lrNo = lrNo
        self.uploadPodTime = uploadPodTime
        self.podID = podID
        self.exManualLrNo = exManualLrNo
        self.exManualLrDate = exManualLrDate
    }
}



// MARK: - PodID
class PodID {
    let id: Int?
    let mimeType: String?
    
    init(id: Int?, mimeType: String?) {
        self.id = id
        self.mimeType = mimeType
    }
}

// MARK: - User
class User1 {
    let id: Int?
    let email: String?
    let userID: String?
    let creationDate, updationDate: Int?
    let password: String?
    let firstName: String?
    let surname: String?
    let designation: String?
    let deviceID, fcmToken: String?
    let status, appStatus: String?
    let otp: String?
    let otpTime: Int?
    let fleetDevicePassword: String?
    let userType: String?
    let profilePic: Document?
    let location, branch: NSNull?
    let empCode: String?
    let mobileNo: String?
    let appActivated: String?
    let erpStatus, hubStatus: String?
    
    init(id: Int?, email: String?, userID: String?, creationDate: Int?, updationDate: Int?, password: String?, firstName: String?, surname: String?, designation: String?, deviceID: String?, fcmToken: String?, status: String?, appStatus: String?, otp: String?, otpTime: Int?, fleetDevicePassword: String?, userType: String?, profilePic: Document?, location: NSNull?, branch: NSNull?, empCode: String?, mobileNo: String?, appActivated: String?, erpStatus: String?, hubStatus: String?) {
        self.id = id
        self.email = email
        self.userID = userID
        self.creationDate = creationDate
        self.updationDate = updationDate
        self.password = password
        self.firstName = firstName
        self.surname = surname
        self.designation = designation
        self.deviceID = deviceID
        self.fcmToken = fcmToken
        self.status = status
        self.appStatus = appStatus
        self.otp = otp
        self.otpTime = otpTime
        self.fleetDevicePassword = fleetDevicePassword
        self.userType = userType
        self.profilePic = profilePic
        self.location = location
        self.branch = branch
        self.empCode = empCode
        self.mobileNo = mobileNo
        self.appActivated = appActivated
        self.erpStatus = erpStatus
        self.hubStatus = hubStatus
    }
}


// MARK: - LoadBidQuotaion
class LoadBidQuotaion {
    let id: Int?
    let availableLoadBid: AvailableBid1?
    let fleetOwner: FleetOwner?
    let quotationAmount: Int?
    let currencyMaster: CurrencyMaster?
    let validDateTime, quotationEntryDate: Int?
    let quoteID, status: String?
    let availableFleetBid: AvailableBid1?
    let updateCount: String?
    let quotationUpdatedDate: Int?
    
    init(id: Int?, availableLoadBid: AvailableBid1?, fleetOwner: FleetOwner?, quotationAmount: Int?, currencyMaster: CurrencyMaster?, validDateTime: Int?, quotationEntryDate: Int?, quoteID: String?, status: String?, availableFleetBid: AvailableBid1?, updateCount: String?, quotationUpdatedDate: Int?) {
        self.id = id
        self.availableLoadBid = availableLoadBid
        self.fleetOwner = fleetOwner
        self.quotationAmount = quotationAmount
        self.currencyMaster = currencyMaster
        self.validDateTime = validDateTime
        self.quotationEntryDate = quotationEntryDate
        self.quoteID = quoteID
        self.status = status
        self.availableFleetBid = availableFleetBid
        self.updateCount = updateCount
        self.quotationUpdatedDate = quotationUpdatedDate
    }
}

// MARK: - Shipper
class Shipper {
    let id: Int?
    let name: String?
    let address: String?
    let location: String?
    let state: String?
    let contactPerson: String?
    let contactNo: String?
    let loadOwner: LoadOwner1?
    
    init(id: Int?, name: String?, address: String?, location: String?, state: String?, contactPerson: String?, contactNo: String?, loadOwner: LoadOwner1?) {
        self.id = id
        self.name = name
        self.address = address
        self.location = location
        self.state = state
        self.contactPerson = contactPerson
        self.contactNo = contactNo
        self.loadOwner = loadOwner
    }
}


class ExTaxInfo {
    let taxDetails: [TaxDetail]?
    let taxGroup: TaxGroup?
    let taxs: [Tax]?
    let taxType: String?
    
    init(taxDetails: [TaxDetail]?, taxGroup: TaxGroup?, taxs: [Tax]?, taxType: String?) {
        self.taxDetails = taxDetails
        self.taxGroup = taxGroup
        self.taxs = taxs
        self.taxType = taxType
    }
}

// MARK: - TaxDetail
class TaxDetail {
    let taxAmount: Int?
    let taxName: String?
    let taxRate: Double?
    let taxType: String?
    
    init(taxAmount: Int?, taxName: String?, taxRate: Double?, taxType: String?) {
        self.taxAmount = taxAmount
        self.taxName = taxName
        self.taxRate = taxRate
        self.taxType = taxType
    }
}

// MARK: - TaxGroup
class TaxGroup {
    let taxGroupDescription: String?
    let tax, id: Int?
    let status: Bool?
    let defaultType: String?
    
    init(taxGroupDescription: String?, tax: Int?, id: Int?, status: Bool?, defaultType: String?) {
        self.taxGroupDescription = taxGroupDescription
        self.tax = tax
        self.id = id
        self.status = status
        self.defaultType = defaultType
    }
}

// MARK: - Tax
class Tax {
    let taxMaster: TaxMaster?
    let taxType: String?
    let id: Int?
    let tax: Double?
    let status: Bool?
    let taxName: String?
    
    init(taxMaster: TaxMaster?, taxType: String?, id: Int?, tax: Double?, status: Bool?, taxName: String?) {
        self.taxMaster = taxMaster
        self.taxType = taxType
        self.id = id
        self.tax = tax
        self.status = status
        self.taxName = taxName
    }
}

// MARK: - TaxMaster
class TaxMaster {
    let id: Int?
    let taxName, taxType: String?
    
    init(id: Int?, taxName: String?, taxType: String?) {
        self.id = id
        self.taxName = taxName
        self.taxType = taxType
    }
}




// MARK: - ExTaxInfo

class ExTaxInfo1 {
    let id: Int?
    let billRecordType: String?
    let status: Bool?
    let sacCode: String?

    init(id: Int?, billRecordType: String?, status: Bool?, sacCode: String?) {
        self.id = id
        self.billRecordType = billRecordType
        self.status = status
        self.sacCode = sacCode
    }
}










// MARK: - DataClass
class ExchangeBillrecords {
    let billRecords: [BillRecord]?
    let invoiceDate: Int?
    let invoiceNo: String?
    let invoiceGrossAmount: Int?

    init(billRecords: [BillRecord]?, invoiceDate: Int?, invoiceNo: String?, invoiceGrossAmount: Int?) {
        self.billRecords = billRecords
        self.invoiceDate = invoiceDate
        self.invoiceNo = invoiceNo
        self.invoiceGrossAmount = invoiceGrossAmount
    }
}

// MARK: - BillRecord
class BillRecord {
    let id: Int?
    let billRecordType: BillRecordType?
    let currencyMaster: CurrencyMaster1?
    let exchangeRate, units, amount: Int?
    var taxable: Bool?
    let recordType, sacCode, billRecordDescription, taxDetail: String?
    let fleet: Fleet?

    init(id: Int?, billRecordType: BillRecordType?, currencyMaster: CurrencyMaster1?, exchangeRate: Int?, units: Int?, amount: Int?, taxable: Bool?, recordType: String?, sacCode: String?, billRecordDescription: String?, taxDetail: String?, fleet: Fleet?) {
        self.id = id
        self.billRecordType = billRecordType
        self.currencyMaster = currencyMaster
        self.exchangeRate = exchangeRate
        self.units = units
        self.amount = amount
        self.taxable = taxable
        self.recordType = recordType
        self.sacCode = sacCode
        self.billRecordDescription = billRecordDescription
        self.taxDetail = taxDetail
        self.fleet = fleet
    }
}

// MARK: - BillRecordType
class BillRecordType {
    let id: Int?
    let billRecordType: String?
    let status: Bool?
    let sacCode: String?

    init(id: Int?, billRecordType: String?, status: Bool?, sacCode: String?) {
        self.id = id
        self.billRecordType = billRecordType
        self.status = status
        self.sacCode = sacCode
    }
}

class CurrencyMaster1 {
    let id: Int?
    let currencyName, currencyCountry, currencyCode: String?

    init(id: Int?, currencyName: String?, currencyCountry: String?, currencyCode: String?) {
        self.id = id
        self.currencyName = currencyName
        self.currencyCountry = currencyCountry
        self.currencyCode = currencyCode
    }
}

// MARK: - Fleet
class Fleet {
    let id: Int?
    let name, registrationNo, panNo, fax: String?
    let telephone: String?
    let userCount, activeUserCount: Int?
    let companyCode: String?
    let gstTax: Int?
    let gstTaxRegNo, jurisdiction, timeZone, vatNo: String?
    let iecNo, cinNo, cstNo: String?
    let registrationDate: Int?
    let expiredType: String?
    let subscriptionStartDate, subscriptionEndDate, perTransactionFee, smsCharge: Int?
    let currentBalance, bookingCount, purchasePaymentCount, purchaseReceiptCount: Int?
    let routeCount: Int?
    let smsNotification: String?
    let maxUser: Int?
    let maxUserType, status: String?
    let contractMasterCount, goCount, proCount, profileMailCount: Int?
    let ewbFlagStr: String?
    let walletAmount, hubMaxUser, allowTransaction, remainsTr: Int?
    let hubPlanID, hubTransactionCost, thirdPartyAuthDateTime, exPurchasePayCount: Int?
    let exPurchaseReceiptCount, exWalletValidFrom, exWalletValidTill, exWalletValidDays: Int?
    let exPlanType, exPackType: String?
    let userCounts: Int?

    init(id: Int?, name: String?, registrationNo: String?, panNo: String?, fax: String?, telephone: String?, userCount: Int?, activeUserCount: Int?, companyCode: String?, gstTax: Int?, gstTaxRegNo: String?, jurisdiction: String?, timeZone: String?, vatNo: String?, iecNo: String?, cinNo: String?, cstNo: String?, registrationDate: Int?, expiredType: String?, subscriptionStartDate: Int?, subscriptionEndDate: Int?, perTransactionFee: Int?, smsCharge: Int?, currentBalance: Int?, bookingCount: Int?, purchasePaymentCount: Int?, purchaseReceiptCount: Int?, routeCount: Int?, smsNotification: String?, maxUser: Int?, maxUserType: String?, status: String?, contractMasterCount: Int?, goCount: Int?, proCount: Int?, profileMailCount: Int?, ewbFlagStr: String?, walletAmount: Int?, hubMaxUser: Int?, allowTransaction: Int?, remainsTr: Int?, hubPlanID: Int?, hubTransactionCost: Int?, thirdPartyAuthDateTime: Int?, exPurchasePayCount: Int?, exPurchaseReceiptCount: Int?, exWalletValidFrom: Int?, exWalletValidTill: Int?, exWalletValidDays: Int?, exPlanType: String?, exPackType: String?, userCounts: Int?) {
        self.id = id
        self.name = name
        self.registrationNo = registrationNo
        self.panNo = panNo
        self.fax = fax
        self.telephone = telephone
        self.userCount = userCount
        self.activeUserCount = activeUserCount
        self.companyCode = companyCode
        self.gstTax = gstTax
        self.gstTaxRegNo = gstTaxRegNo
        self.jurisdiction = jurisdiction
        self.timeZone = timeZone
        self.vatNo = vatNo
        self.iecNo = iecNo
        self.cinNo = cinNo
        self.cstNo = cstNo
        self.registrationDate = registrationDate
        self.expiredType = expiredType
        self.subscriptionStartDate = subscriptionStartDate
        self.subscriptionEndDate = subscriptionEndDate
        self.perTransactionFee = perTransactionFee
        self.smsCharge = smsCharge
        self.currentBalance = currentBalance
        self.bookingCount = bookingCount
        self.purchasePaymentCount = purchasePaymentCount
        self.purchaseReceiptCount = purchaseReceiptCount
        self.routeCount = routeCount
        self.smsNotification = smsNotification
        self.maxUser = maxUser
        self.maxUserType = maxUserType
        self.status = status
        self.contractMasterCount = contractMasterCount
        self.goCount = goCount
        self.proCount = proCount
        self.profileMailCount = profileMailCount
        self.ewbFlagStr = ewbFlagStr
        self.walletAmount = walletAmount
        self.hubMaxUser = hubMaxUser
        self.allowTransaction = allowTransaction
        self.remainsTr = remainsTr
        self.hubPlanID = hubPlanID
        self.hubTransactionCost = hubTransactionCost
        self.thirdPartyAuthDateTime = thirdPartyAuthDateTime
        self.exPurchasePayCount = exPurchasePayCount
        self.exPurchaseReceiptCount = exPurchaseReceiptCount
        self.exWalletValidFrom = exWalletValidFrom
        self.exWalletValidTill = exWalletValidTill
        self.exWalletValidDays = exWalletValidDays
        self.exPlanType = exPlanType
        self.exPackType = exPackType
        self.userCounts = userCounts
    }
}




// MARK: - MileStoneElement
class MileStoneElement {
    let id, createdDate: Int?
    let latitude, longitude, speed: Double?
    let distance, jobStatusID: Int?
    let bearing: Double?

    init(id: Int?, createdDate: Int?, latitude: Double?, longitude: Double?, speed: Double?, distance: Int?, jobStatusID: Int?, bearing: Double?) {
        self.id = id
        self.createdDate = createdDate
        self.latitude = latitude
        self.longitude = longitude
        self.speed = speed
        self.distance = distance
        self.jobStatusID = jobStatusID
        self.bearing = bearing
    }
}
