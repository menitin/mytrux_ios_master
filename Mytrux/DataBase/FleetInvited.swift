//
//  FleetInvited.swift
//  Mytrux
//
//  Created by Mytrux on 05/09/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//


import Foundation

// MARK: - FleetInvited
class FleetInvited {
    let status: Bool
    let data: [FleetInvitedDatum]
    
    init(status: Bool, data: [FleetInvitedDatum]) {
        self.status = status
        self.data = data
    }
}

// MARK: - Datum
class FleetInvitedDatum {
    let id: Int
    let availableFleetBid: AvailableBid
    let loadOwner: LoadOwnerInvited
    let inviteDate: Int?
    let availableLoadBid: AvailableBid
    let inviteStatus: String?
    let count: Int?
      let availableFleetBidInvite: AvailableFleetBidInvite?
    init(id: Int, availableFleetBid: AvailableBid, loadOwner: LoadOwnerInvited, inviteDate: Int?, availableLoadBid: AvailableBid, inviteStatus: String?, count: Int?,availableFleetBidInvite:AvailableFleetBidInvite?) {
        self.id = id
        self.availableFleetBid = availableFleetBid
        self.loadOwner = loadOwner
        self.inviteDate = inviteDate
        self.availableLoadBid = availableLoadBid
        self.inviteStatus = inviteStatus
        self.count = count
        self.availableFleetBidInvite = availableFleetBidInvite
    }
}


class AvailableFleetBidInvite {
    let id: Int?
    let availableFleetBid: AvailableBid?
    let loadOwner: LoadOwner?
    let inviteDate: Int?
    let availableLoadBid: AvailableBid?
    let inviteStatus: String?
    
    init(id: Int?, availableFleetBid: AvailableBid?, loadOwner: LoadOwner?, inviteDate: Int?, availableLoadBid: AvailableBid?, inviteStatus: String?) {
        self.id = id
        self.availableFleetBid = availableFleetBid
        self.loadOwner = loadOwner
        self.inviteDate = inviteDate
        self.availableLoadBid = availableLoadBid
        self.inviteStatus = inviteStatus
    }
}

// MARK: - AvailableBid
class AvailableBid {
    let id: Int
    let bidCode: String
    let creationDate, closedBidTime: Int
    let availableLocation : String
    let comments, destinationLocation : String?
    let availableLatitude, availableLongitude, destinationLatitude, destinationLongitude: Double?
    let bidInviteCount: Int?
    let vehicleType: VehicleType
    let noOfVehicles, availableDateTime: Int
    let cargoType: CargoType
    let capacityWeight: Int
    let capacityWeightUnit: CapacityWeightUnit
    let loadType: LoadType1
    let expectedFreight: Int?
    let currencyMaster: CurrencyMaster?
    let status: String
    let registrationNo: String?
    let packgs: Int?
    let containerNo: String?
    
    init(id: Int, bidCode: String, creationDate: Int, closedBidTime: Int, availableLocation: String, destinationLocation: String?, comments: String?, availableLatitude: Double?, availableLongitude: Double?, destinationLatitude: Double?, destinationLongitude: Double?, bidInviteCount: Int?, vehicleType: VehicleType, noOfVehicles: Int, availableDateTime: Int, cargoType: CargoType, capacityWeight: Int, capacityWeightUnit: CapacityWeightUnit, loadType: LoadType1, expectedFreight: Int?, currencyMaster: CurrencyMaster?, status: String, registrationNo: String?, packgs: Int?, containerNo: String?) {
        self.id = id
        self.bidCode = bidCode
        self.creationDate = creationDate
        self.closedBidTime = closedBidTime
        self.availableLocation = availableLocation
        self.destinationLocation = destinationLocation
        self.comments = comments
        self.availableLatitude = availableLatitude
        self.availableLongitude = availableLongitude
        self.destinationLatitude = destinationLatitude
        self.destinationLongitude = destinationLongitude
        self.bidInviteCount = bidInviteCount
        self.vehicleType = vehicleType
        self.noOfVehicles = noOfVehicles
        self.availableDateTime = availableDateTime
        self.cargoType = cargoType
        self.capacityWeight = capacityWeight
        self.capacityWeightUnit = capacityWeightUnit
        self.loadType = loadType
        self.expectedFreight = expectedFreight
        self.currencyMaster = currencyMaster
        self.status = status
        self.registrationNo = registrationNo
        self.packgs = packgs
        self.containerNo = containerNo
    }
}


// MARK: - LoadOwner
class LoadOwnerInvited {
    let id: Int
    let name, registrationNo, companyCode, telephone: String
    let fax: String
    let gstTax: Double
    let gstTaxRegNo, jurisdiction, timeZone, panNo: String
    let vatNo, iecNo, cinNo, cstNo: String
    let registrationDate, subscriptionStartDate, subscriptionEndDate, currentBalance: Int
    let smsNotification: String
    let contactPersons: [ContactPerson]
    let companyAccess: CompanyAccess
    let status, expiredType: String
    let activeUserCount, maxUser: Int
    let maxUserType: String
    let exchangeBookingCount: Int
    let exPlanType: String
    let goCount, proCount, profileMailCount: Int
    let ewbFlagStr, planType: String
    let exSuwalletAmount, exSuhubMaxUser, exSuallowTransaction, exSuremainsTr: Int
    let hubPlanID, exSuhubTransactionCost: Int
    let exPackType: String
    
    init(id: Int, name: String, registrationNo: String, companyCode: String, telephone: String, fax: String, gstTax: Double, gstTaxRegNo: String, jurisdiction: String, timeZone: String, panNo: String, vatNo: String, iecNo: String, cinNo: String, cstNo: String, registrationDate: Int, subscriptionStartDate: Int, subscriptionEndDate: Int, currentBalance: Int, smsNotification: String, contactPersons: [ContactPerson], companyAccess: CompanyAccess, status: String, expiredType: String, activeUserCount: Int, maxUser: Int, maxUserType: String, exchangeBookingCount: Int, exPlanType: String, goCount: Int, proCount: Int, profileMailCount: Int, ewbFlagStr: String, planType: String, exSuwalletAmount: Int, exSuhubMaxUser: Int, exSuallowTransaction: Int, exSuremainsTr: Int, hubPlanID: Int, exSuhubTransactionCost: Int, exPackType: String) {
        self.id = id
        self.name = name
        self.registrationNo = registrationNo
        self.companyCode = companyCode
        self.telephone = telephone
        self.fax = fax
        self.gstTax = gstTax
        self.gstTaxRegNo = gstTaxRegNo
        self.jurisdiction = jurisdiction
        self.timeZone = timeZone
        self.panNo = panNo
        self.vatNo = vatNo
        self.iecNo = iecNo
        self.cinNo = cinNo
        self.cstNo = cstNo
        self.registrationDate = registrationDate
        self.subscriptionStartDate = subscriptionStartDate
        self.subscriptionEndDate = subscriptionEndDate
        self.currentBalance = currentBalance
        self.smsNotification = smsNotification
        self.contactPersons = contactPersons
        self.companyAccess = companyAccess
        self.status = status
        self.expiredType = expiredType
        self.activeUserCount = activeUserCount
        self.maxUser = maxUser
        self.maxUserType = maxUserType
        self.exchangeBookingCount = exchangeBookingCount
        self.exPlanType = exPlanType
        self.goCount = goCount
        self.proCount = proCount
        self.profileMailCount = profileMailCount
        self.ewbFlagStr = ewbFlagStr
        self.planType = planType
        self.exSuwalletAmount = exSuwalletAmount
        self.exSuhubMaxUser = exSuhubMaxUser
        self.exSuallowTransaction = exSuallowTransaction
        self.exSuremainsTr = exSuremainsTr
        self.hubPlanID = hubPlanID
        self.exSuhubTransactionCost = exSuhubTransactionCost
        self.exPackType = exPackType
    }
}


// MARK: - ContactPerson
class ContactPerson {
    let id: Int
    let fullName, contactNo, email: String
    let main: Bool
    
    init(id: Int, fullName: String, contactNo: String, email: String, main: Bool) {
        self.id = id
        self.fullName = fullName
        self.contactNo = contactNo
        self.email = email
        self.main = main
    }
}
