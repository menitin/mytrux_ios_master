//
//  NotificationCenter.swift
//  Mytrux
//
//  Created by Mytrux on 20/08/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

extension Notification.Name {
    
    static let selectedTime = Notification.Name("SelectedTime")
    static let selectedDate = Notification.Name("selectedDate")
    
    //post fleet
    static let postFleetFrom = Notification.Name("postFleetFrom")
    static let postFleetFromDate = Notification.Name("postFleetFromDate")
    static let postFleetTill = Notification.Name("postFleetTill")
    static let postFleetTillDate = Notification.Name("postFleetTillDate")
    static let postFleetUnitType = Notification.Name("postFleetUnitType")
    static let postFleetCargoType = Notification.Name("postFleetCargoType")
    static let refreshViewPostFleet = Notification.Name("refreshViewPostFleet")
    
    //wallet
    static let paymentPlan = Notification.Name("paymentPlan")
     static let paymentState = Notification.Name("paymentState")
     static let paymentCity = Notification.Name("paymentCity")
     static let paymentSelectedState = Notification.Name("paymentSelectedState")
    
    //Legal
     static let legal = Notification.Name("legal")
 
    
    
    //booking
    static let eLR = Notification.Name("eLR")
    static let makeInvoice = Notification.Name("makeInvoice")
    static let viewInvoice = Notification.Name("viewInvoice")
    static let uploadPod = Notification.Name("uploadPod")
    static let track = Notification.Name("track")
    static let uploadSignature = Notification.Name("uploadSignature")
    static let uploadPhoto = Notification.Name("uploadPhoto")
    static let search = Notification.Name("search")
    static let searchData = Notification.Name("searchData")
    
    
    // loads Quoted
    static let loadsQuotedFromDate = Notification.Name("loadsQuotedFromDate")
    static let loadsQuotedToDate = Notification.Name("loadsQuotedToDate")
    
    // fleet posted
    static let fleetPostedFromDate = Notification.Name("fleetPostedFromDate")
    static let fleetPostedToDate = Notification.Name("fleetPostedToDate")
    static let editFleetPosted = Notification.Name("editFleetPosted")
    static let refreshViewFleetPost = Notification.Name("refreshViewFleetPost")
    static let fleetInvited = Notification.Name("fleetInvited")
    
    
    //Dashboard
    static let assign = Notification.Name("assign")
    
    
    // FleetInvited
    static let fleetInvitedFromDate = Notification.Name("fleetInvitedFromDate")
    static let fleetInvitedToDate = Notification.Name("fleetInvitedToDate")
    
    //MakeInvoice
    static let billingCustomer = Notification.Name("billingCustomer")
    static let billingDetails = Notification.Name("billingDetails")
    static let billingPreview = Notification.Name("billingPreview")
    static let backClicked = Notification.Name("backClicked")
    static let billPreviewBackClicked = Notification.Name("billPreviewBackClicked")
    static let billingParty = Notification.Name("billingParty")
    static let billingCustomerDate = Notification.Name("billingCustomerDate")
    static let billingHead = Notification.Name("billingHead")
    static let taxView = Notification.Name("taxView")
    
    
    
    
    // Fleet map View
    static let fleetDetailsMapView = Notification.Name("fleetDetailsMapView")
    static let fleetDetailsBottom = Notification.Name("fleetDetailsBottom")
    static let refreshViewFleetMap = Notification.Name("refreshViewFleetMap")
    static let activityIndicator = Notification.Name("activityIndicator")
    
    
    // Dashboard
    static let dashboardActivityIndicator = Notification.Name("dashboardActivityIndicator")
     
        
    // MapViewCon
    static let pannelCollapsed = Notification.Name("PannelCollapsed")
    static let sumbitQuoteView = Notification.Name("sumbitQuoteView")
    static let inviteFleetClicked = Notification.Name("inviteFleetClicked")
    static let loadObj = Notification.Name("loadObj")
    static let searchLoadCollapsed1 = Notification.Name("searchLoadCollapsed1")
    static let dashboardCollapsed = Notification.Name("dashboardCollapsed")
    static let searchLoadCollapsed = Notification.Name("searchLoadCollapsed")
   
    // trackView
        static let bookingDetailController = Notification.Name("bookingDetailController")
    // Customer
       static let customerDetail = Notification.Name("customerDetail")
    
    static let LoadsQuotedClicked = Notification.Name("LoadsQuotedClicked")
    static let spNotificationClicked = Notification.Name("spNotificationClicked")
    static let suNotificationClicked = Notification.Name("suNotificationClicked")
    static let spCustomerViewClicked = Notification.Name("spCustomerViewClicked")
    
    static let fleetsPostedClicked = Notification.Name("fleetsPostedClicked")
    static let bookingClicked = Notification.Name("bookingClicked")
    static let exchangeClicked = Notification.Name("exchangeClicked")
    static let postFleetClicked = Notification.Name("postFleetClicked")
    static let walletClicked = Notification.Name("walletClicked")
    static let financialYear = Notification.Name("financialYear")
    static let legalClicked = Notification.Name("legalClicked")
    
    static let FleetInfoView = Notification.Name("FleetInfoView")
    static let Dashboard = Notification.Name("Dashboard")
    static let SearchLoad = Notification.Name("SearchLoad")
    static let BackClicked = Notification.Name("BackClicked")
    static let submitQuote = Notification.Name("submitQuote")
    static let confirmSubmitQuote = Notification.Name("confirmSubmitQuote")
    static let inviteFleet = Notification.Name("inviteFleet")
    static let sliderValueChanged = Notification.Name("sliderValueChanged")
    static let refreshView = Notification.Name("refreshView")
 
    //    static let selectedTimeType = Notification.Name("SelectedTimeType")
    //    static let selectedTimeType = Notification.Name("SelectedTimeType")
    //    static let selectedTimeType = Notification.Name("SelectedTimeType")
    //    static let selectedTimeType = Notification.Name("SelectedTimeType")
    //    static let selectedTimeType = Notification.Name("SelectedTimeType")
    //    static let selectedTimeType = Notification.Name("SelectedTimeType")
    //    static let selectedTimeType = Notification.Name("SelectedTimeType")
    //    static let selectedTimeType = Notification.Name("SelectedTimeType")
    //    static let selectedTimeType = Notification.Name("SelectedTimeType")
    //    static let selectedTimeType = Notification.Name("SelectedTimeType")
    
}
