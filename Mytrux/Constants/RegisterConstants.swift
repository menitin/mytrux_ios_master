//
//  RegisterConstants.swift
//  Mytrux
//
//  Created by Mukta Bhuyar Punjabi on 29/05/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import Foundation
class RegisterConstants{ 
    static let FirstName = "firstName"
    static let Surname = "surname"
    static let MobileNo = "mobileNo"
    static let Email = "email"
    static let serviceType = "serviceType"
    static let appActivated = "appActivated"
    static let passward = "passward"
    static let confirmPassword = "confirmPassword"
    static let city = "city"
    static let address = "address"
    static let country = "country"
    static let state = "state"
    static let pincode = "pincode"
    static let compCode = "companyCode"
    static let id = "id"
    static let companyType = "companyType"
    static let companyCategories = "companyCategories"
    static let CompanyName = "name"
    static let jurisdiction = "jurisdiction"
    static let panNo = "panNo"
    static let profileImage = "profileImage"
    static let documentImage = "documentImage"
}
