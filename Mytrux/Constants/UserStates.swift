//
//  UserStates.swift
//  Mytrux
//
//  Created by Mukta Bhuyar Punjabi on 21/05/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import Foundation
class UserStates{
    static let HUB = "HUB",
     ERP = "ERP",
     DRIVER = "DRIVER",
     HUB_LOAD = "HUB LOAD OWNER",
     HUB_TRUCK = "HUB TRUCK OWNER",
     ERP_SU = "ERP_SU",
     ERP_SP = "ERP_SP",
     DRIVER_GO = "DRIVER_GO",
     DRIVER_PRO = "DRIVER_PRO",
     GO = "GO",
     PRO = "PRO",
     ALL = "ALL",
     SERVICE_USER = "SERVICE_USER",
     SERVICE_PROVIDER = "SERVICE_PROVIDER",
    NEW = "NEW",
    ASSIGN = "ASSIGN",
    COMPLETE = "COMPLETE",
    WITHOUTNEW = "WITHOUTNEW"
    
}


