//
//  UrlStatics.swift
//  Mytrux
//
//  Created by Mukta Bhuyar Punjabi on 21/05/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import Foundation

class URLStatics{
    
    
   static let baseURL = "https://www.mytrux.com/"
    
   // static let baseURL = "http://192.168.0.17:8080/"
    //  static let baseURL = "http://ec2-13-127-80-255.ap-south-1.compute.amazonaws.com:8082/"
    //static let baseURL = "http://mytruxdev.southeastasia.cloudapp.azure.com:8082/"
    
    static let googleApiKey = "AIzaSyA1Q1QXw0CaAqUc5lUCj-u5zvsnQYzWU9Q"
    
    static let placesList = "https://maps.googleapis.com/maps/api/place/autocomplete/json?"
    static let typeOfCompany = baseURL+"mytrux/company/typeofcompany"
    static let hubRegister = "\(baseURL)"+"mytrux/company/app/registration"
    static let resendOTP = "\(baseURL)"+"mytrux/user/tracker/otp/resend"
    static let verifyOTP = "\(baseURL)"+"mytrux/company/registration/verify"
    static let hubLogin = "\(baseURL)"+"mytrux/user/tracker/login"
    static let vehicleList = "\(baseURL)"+"mytrux/fleet/vehicletype"
    static let vehicleImage = "\(baseURL)"+"mytrux/document/"
    static let loadType = "\(baseURL)"+"mytrux/booking/loadtype"
    static let cargotype = "\(baseURL)"+"mytrux/booking/cargotype"
    static let unittype = "\(baseURL)"+"mytrux/booking/unittype/weight"
    
    
    static let findavailableloadforbidlist = baseURL+"mytrux/ep/dashboard/findavailableloadforbidlist/OPEN?"
    static let findavailablefleetforbidlist = baseURL+"mytrux/ep/dashboard/findavailablefleetforbidlist/OPEN?"
    static let postloadquotation = baseURL+"mytrux/ep/availableloadforbid/postloadquotation/"
    static let postFleet = baseURL+"mytrux/ep/availablefleetforbid"
    static let fleetPosted = baseURL + "mytrux/ep/dashboard/findavailablefleetforbidlist/"
    static let inviteList = baseURL+"mytrux/ep/dashboard/invitelist/"
    static let assignBookingList = baseURL+"mytrux/exchangebooking/assignBookingList?"
    static let quotations = baseURL+"mytrux/ep/dashboard/quotations?"
    static let inviteStatus = baseURL+"mytrux/ep/availablefleetforbid/postvehicleinvitestatus/"
    static let rejectToAvailableInvite = baseURL+"mytrux/ep/dashboard/rejectToAvailableInvite?"
    static let assignDriver = baseURL+"mytrux/exchangebooking/booking/assigndriver/"
    static let bookingDashboardCount = baseURL+"mytrux/exchangebooking/dashboard/count?"
    static let bookingList = baseURL+"mytrux/exchangebooking/bookinglist?"
    
    static let bookingCounter = baseURL+"mytrux/exchangebooking/booking/counter"
    static let report = baseURL+"mytrux/report/hub/"
    static let triplist = baseURL+"mytrux/exchangebooking/triplist/"
    static let uploadPod = baseURL+"mytrux/exchangebooking/pod/"
    static let updatebilling = baseURL+"mytrux/exchangebooking/updatebilling/"
    
    static let exBillMasterList = baseURL+"mytrux/exTaxResource/exBillMasterList"
    static let taxgroup = baseURL+"mytrux/tax/taxgroup?"
    static let exchangeBillrecords = baseURL+"mytrux/exchangebooking/exchangeBillrecords?"
    static let finalize = baseURL+"mytrux/exchangebooking/billrecord/finalize?"
    static let qrCode = baseURL+"hubIndex.html#/HSIQR/"
    static let trackinglist = baseURL+"mytrux/hubTrackingResource/trackinglist/"
    static let tripId = baseURL+"mytrux/exchangebooking/booking/"
    static let getAmount = baseURL+"mytrux/addPayment/getremaining/amount"
    static let paymentPlan = baseURL+"mytrux/suser/paymentplan"
    static let addPayment = baseURL+"mytrux/addPayment/checkbalance/"
    static let hubtransactions = baseURL+"mytrux/addPayment/hubtransactions"
     static let paymentReceipt = "https://www.mytrux.com/mytrux/report/paymentreceipthub/"
    
   static let transactionReceipt = "https://www.mytrux.com/mytrux/report/transaction/"
    static let companyDetails = baseURL+"mytrux/company/"
    static let addToWallet = baseURL+"mytrux/addPayment/addToWallet"
    
     static let disclaimer = baseURL+"/website/disclaimer.html"
     static let cookies = baseURL+"/website/cookies-policy.html"
     static let privacyPolicy = baseURL+"/website/privacy-policy.html"
     static let termsCondition = baseURL+"/website/terms-condition.html"
    static let customerOther = baseURL+"mytrux/exchangebooking/customerList"
    static let customer = baseURL+"mytrux/company/favourite/hub/"
    static let searchCustomer = baseURL+"mytrux/company/"
    static let inviteCustomer = baseURL+"mytrux/company/hub/invite/"
    static let addToFavCustomer = baseURL + "mytrux/company/hubFavourite/"
    //https://www.mytrux.com/mytrux/company/aboli.pasekar@mytrux.com?financialYear=2019-2020
}
