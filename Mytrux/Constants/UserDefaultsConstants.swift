//
//  UserDefaultsConstants.swift
//  Mytrux
//
//  Created by Mukta Bhuyar Punjabi on 22/05/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import Foundation
class UserDefaultsContants{

    static let MTX_STATE = "mtx_state"
    static let MTX_Load_STATE = "mtx_load_truck_state"
    static let SELECTED_CAMERA_IMAGE = "selected_Camera_Image"
    static let SELECTED_GALLERY_IMAGE = "selected_Gallery_Image"
    static let SELECTED_STATE = "selected_state"
    static let SELECTED_MODE = "selected_mode"
    static let SELECTED_SERVICE = "selected_service"
    static let CompanyType = "companyType"
    static let SERVICE_STATE = "service_state"
    static let LOGIN_USER_TYPE = "loginUserType"
    static let USER_ID = "user_id"
    static let OTP = "otp"
    
}
