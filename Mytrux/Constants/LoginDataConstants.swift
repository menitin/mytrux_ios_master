//
//  LoginData.swift
//  Mytrux
//
//  Created by Aboli on 04/06/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import Foundation

class LoginDataConstants {
    static let financeYear = "financeYear"
    static let firstName = "firstName"
    static let profilePic = "profilePic"
    static let id = "id"
    static let fleetName = "fleetName"
    static let serviceType = "serviceType"
    static let hubStatus = "hubStatus"
    static let fleetId = "fleetId"
    static let token = "token"
    static let emailId = "emailId"
    static let companyCode = "companyCode"
    static let designation = "designation"
    static let lastName = "lastName"
    static let userType = "userType"
    static let companyLogo = "companyLogo"
    static let mobileNo = "mobileNo"
    static let erpStatus = "erpStatus"
    static let appActivated = "appActivated"
    
    
}

