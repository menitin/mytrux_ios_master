//
//  ColorConstants.swift
//  Mytrux
//
//  Created by Aboli on 21/06/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import Foundation
class ColorConstants{
    
    static let DARK_BLUE = "#0F2541"
    static let RED = "#DD0B51"
    static let LIGHT_BLUE = "#559AFF"
    static let GREEN = "#66BA2E"
    static let LIGHT_GRAY = "C0C6CC"
    static let YELLOW = "FFB800"
   
    
}
