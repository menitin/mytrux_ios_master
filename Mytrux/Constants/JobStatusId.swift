//
//  JobStatusId.swift
//  Mytrux
//
//  Created by Mytrux on 15/10/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import Foundation
class JobStatusId {
 static let ASSIGNED5 = 5,
ASSIGNED = 2,
ACKNOWLEDGED = 6,
LEFT_FOR_PICKUP = 7,
REACHED_PICKUP = 8,
LEFT_FOR_DESTINATION = 9,
ARRIVED_AT_DESTINATION = 10,
UPLOADED_POD = 11,
NEW_BOOKING = 1

}
