//
//  SelectServiceVC.swift
//  Mytrux
//
//  Created by Mukta Bhuyar Punjabi on 15/05/19.
//  Copyright © 2019 Mytrux. All rights reserved.
//

import UIKit

class SelectServiceVC: BaseViewController {

    @IBOutlet weak var backButton: UIView!
    @IBOutlet weak var driverSubtitle: UILabel!
    @IBOutlet weak var erpSubtitle: UILabel!
    @IBOutlet weak var hubSubtitle: UILabel!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var erpView: UIView!
    @IBOutlet weak var hubView: UIView!
    @IBOutlet weak var driverView: UIView!
    @IBOutlet weak var selectServiceLabel: UILabel!
    var cardViewArray = [UIView]()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cardViewArray = [hubView,erpView,driverView]
        cardViewArray(arrView: cardViewArray)
        topBarCard(topBar: topBar)
        let state = UserDefaults.standard.value(forKey: UserDefaultsContants.MTX_STATE)! as! String
        print("state = \(state)")
        UserDefaults.standard.removeObject(forKey: RegisterConstants.companyType)
        UserDefaults.standard.removeObject(forKey: RegisterConstants.FirstName)
        UserDefaults.standard.removeObject(forKey: RegisterConstants.Surname)
        UserDefaults.standard.removeObject(forKey: RegisterConstants.passward)
        UserDefaults.standard.removeObject(forKey: RegisterConstants.confirmPassword)
        UserDefaults.standard.removeObject(forKey: RegisterConstants.Email)
        hubSubtitle.adjustsFontSizeToFitWidth = true
        hubSubtitle.minimumScaleFactor = 0.5
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        hubView.addGestureRecognizer(tap)
        hubView.isUserInteractionEnabled = true
        self.view.addSubview(hubView)
        
        let states = appDelegate.getAllStateList()
        let cities = appDelegate.getCity()
       
        for ci in cities!{
             print("city \(String(describing: ci.state))")
             print("cityy \(String(describing: ci.city))")
        }
        for st in states {
            print("staeee = \(st)")
        }
        
        let backtap = UITapGestureRecognizer(target: self, action: #selector(self.handleBackButtonTap(_:)))
        backButton.addGestureRecognizer(backtap)
        backButton.isUserInteractionEnabled = true
        self.topBar.addSubview(backButton)
        
        
        erpSubtitle.adjustsFontSizeToFitWidth = true
        erpSubtitle.minimumScaleFactor = 0.2
        driverSubtitle.adjustsFontSizeToFitWidth = true
        driverSubtitle.minimumScaleFactor = 0.2
        
    }
    
   
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        UserDefaults.standard.set(UserStates.HUB, forKey: UserDefaultsContants.SELECTED_SERVICE)
        UserDefaults.standard.synchronize()
         if (UserDefaults.standard.string(forKey: "name")) == nil {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "pageVC")
            controller.modalPresentationStyle = .fullScreen
         transitionVc(vc: controller, duration: 0.5, type: .fromRight)
            UserDefaults.standard.set("name", forKey: "name")
            CoreDataManager.setUserState(state: State(state: "hub"))
           
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier :"loadsAndTrucksVC")
            viewController.modalPresentationStyle = .fullScreen
            transitionVc(vc: viewController, duration: 0.5, type: .fromRight)
        }
        
    }
   
    
    @objc func handleBackButtonTap(_ sender: UITapGestureRecognizer) {
        
    }

    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
